// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.model.stream.HttpGlideUrlLoader;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.resource.bitmap.VideoDecoder;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder;
import android.graphics.Bitmap$CompressFormat;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.load.resource.gif.GifOptions;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import android.graphics.Bitmap;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.util.CachedHashCodeArrayMap;
import com.bumptech.glide.signature.EmptySignature;
import com.bumptech.glide.load.Transformation;
import java.util.Map;
import android.content.res.Resources$Theme;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Options;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public abstract class BaseRequestOptions<T extends BaseRequestOptions<T>> implements Cloneable
{
    private static final int DISK_CACHE_STRATEGY = 4;
    private static final int ERROR_ID = 32;
    private static final int ERROR_PLACEHOLDER = 16;
    private static final int FALLBACK = 8192;
    private static final int FALLBACK_ID = 16384;
    private static final int IS_CACHEABLE = 256;
    private static final int ONLY_RETRIEVE_FROM_CACHE = 524288;
    private static final int OVERRIDE = 512;
    private static final int PLACEHOLDER = 64;
    private static final int PLACEHOLDER_ID = 128;
    private static final int PRIORITY = 8;
    private static final int RESOURCE_CLASS = 4096;
    private static final int SIGNATURE = 1024;
    private static final int SIZE_MULTIPLIER = 2;
    private static final int THEME = 32768;
    private static final int TRANSFORMATION = 2048;
    private static final int TRANSFORMATION_ALLOWED = 65536;
    private static final int TRANSFORMATION_REQUIRED = 131072;
    private static final int UNSET = -1;
    private static final int USE_ANIMATION_POOL = 1048576;
    private static final int USE_UNLIMITED_SOURCE_GENERATORS_POOL = 262144;
    private DiskCacheStrategy diskCacheStrategy;
    private int errorId;
    private Drawable errorPlaceholder;
    private Drawable fallbackDrawable;
    private int fallbackId;
    private int fields;
    private boolean isAutoCloneEnabled;
    private boolean isCacheable;
    private boolean isLocked;
    private boolean isScaleOnlyOrNoTransform;
    private boolean isTransformationAllowed;
    private boolean isTransformationRequired;
    private boolean onlyRetrieveFromCache;
    private Options options;
    private int overrideHeight;
    private int overrideWidth;
    private Drawable placeholderDrawable;
    private int placeholderId;
    private Priority priority;
    private Class<?> resourceClass;
    private Key signature;
    private float sizeMultiplier;
    private Resources$Theme theme;
    private Map<Class<?>, Transformation<?>> transformations;
    private boolean useAnimationPool;
    private boolean useUnlimitedSourceGeneratorsPool;
    
    public BaseRequestOptions() {
        this.sizeMultiplier = 1.0f;
        this.diskCacheStrategy = DiskCacheStrategy.AUTOMATIC;
        this.priority = Priority.NORMAL;
        this.isCacheable = true;
        this.overrideHeight = -1;
        this.overrideWidth = -1;
        this.signature = EmptySignature.obtain();
        this.isTransformationAllowed = true;
        this.options = new Options();
        this.transformations = new CachedHashCodeArrayMap<Class<?>, Transformation<?>>();
        this.resourceClass = Object.class;
        this.isScaleOnlyOrNoTransform = true;
    }
    
    private boolean isSet(final int n) {
        return isSet(this.fields, n);
    }
    
    private static boolean isSet(final int n, final int n2) {
        return (n & n2) != 0x0;
    }
    
    private T optionalScaleOnlyTransform(final DownsampleStrategy downsampleStrategy, final Transformation<Bitmap> transformation) {
        return this.scaleOnlyTransform(downsampleStrategy, transformation, false);
    }
    
    private T scaleOnlyTransform(final DownsampleStrategy downsampleStrategy, final Transformation<Bitmap> transformation) {
        return this.scaleOnlyTransform(downsampleStrategy, transformation, true);
    }
    
    private T scaleOnlyTransform(final DownsampleStrategy downsampleStrategy, final Transformation<Bitmap> transformation, final boolean b) {
        BaseRequestOptions<T> baseRequestOptions;
        if (b) {
            baseRequestOptions = (BaseRequestOptions<T>)this.transform(downsampleStrategy, transformation);
        }
        else {
            baseRequestOptions = (BaseRequestOptions<T>)this.optionalTransform(downsampleStrategy, transformation);
        }
        baseRequestOptions.isScaleOnlyOrNoTransform = true;
        return (T)baseRequestOptions;
    }
    
    private T self() {
        return (T)this;
    }
    
    public T apply(final BaseRequestOptions<?> p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.apply:(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_1        
        //    17: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    20: iconst_2       
        //    21: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //    24: ifeq            35
        //    27: aload_0        
        //    28: aload_1        
        //    29: getfield        com/bumptech/glide/request/BaseRequestOptions.sizeMultiplier:F
        //    32: putfield        com/bumptech/glide/request/BaseRequestOptions.sizeMultiplier:F
        //    35: aload_1        
        //    36: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    39: ldc             262144
        //    41: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //    44: ifeq            55
        //    47: aload_0        
        //    48: aload_1        
        //    49: getfield        com/bumptech/glide/request/BaseRequestOptions.useUnlimitedSourceGeneratorsPool:Z
        //    52: putfield        com/bumptech/glide/request/BaseRequestOptions.useUnlimitedSourceGeneratorsPool:Z
        //    55: aload_1        
        //    56: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    59: ldc             1048576
        //    61: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //    64: ifeq            75
        //    67: aload_0        
        //    68: aload_1        
        //    69: getfield        com/bumptech/glide/request/BaseRequestOptions.useAnimationPool:Z
        //    72: putfield        com/bumptech/glide/request/BaseRequestOptions.useAnimationPool:Z
        //    75: aload_1        
        //    76: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    79: iconst_4       
        //    80: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //    83: ifeq            94
        //    86: aload_0        
        //    87: aload_1        
        //    88: getfield        com/bumptech/glide/request/BaseRequestOptions.diskCacheStrategy:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;
        //    91: putfield        com/bumptech/glide/request/BaseRequestOptions.diskCacheStrategy:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;
        //    94: aload_1        
        //    95: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    98: bipush          8
        //   100: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   103: ifeq            114
        //   106: aload_0        
        //   107: aload_1        
        //   108: getfield        com/bumptech/glide/request/BaseRequestOptions.priority:Lcom/bumptech/glide/Priority;
        //   111: putfield        com/bumptech/glide/request/BaseRequestOptions.priority:Lcom/bumptech/glide/Priority;
        //   114: aload_1        
        //   115: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   118: bipush          16
        //   120: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   123: ifeq            150
        //   126: aload_0        
        //   127: aload_1        
        //   128: getfield        com/bumptech/glide/request/BaseRequestOptions.errorPlaceholder:Landroid/graphics/drawable/Drawable;
        //   131: putfield        com/bumptech/glide/request/BaseRequestOptions.errorPlaceholder:Landroid/graphics/drawable/Drawable;
        //   134: aload_0        
        //   135: iconst_0       
        //   136: putfield        com/bumptech/glide/request/BaseRequestOptions.errorId:I
        //   139: aload_0        
        //   140: aload_0        
        //   141: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   144: bipush          -33
        //   146: iand           
        //   147: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   150: aload_1        
        //   151: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   154: bipush          32
        //   156: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   159: ifeq            186
        //   162: aload_0        
        //   163: aload_1        
        //   164: getfield        com/bumptech/glide/request/BaseRequestOptions.errorId:I
        //   167: putfield        com/bumptech/glide/request/BaseRequestOptions.errorId:I
        //   170: aload_0        
        //   171: aconst_null    
        //   172: putfield        com/bumptech/glide/request/BaseRequestOptions.errorPlaceholder:Landroid/graphics/drawable/Drawable;
        //   175: aload_0        
        //   176: aload_0        
        //   177: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   180: bipush          -17
        //   182: iand           
        //   183: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   186: aload_1        
        //   187: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   190: bipush          64
        //   192: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   195: ifeq            223
        //   198: aload_0        
        //   199: aload_1        
        //   200: getfield        com/bumptech/glide/request/BaseRequestOptions.placeholderDrawable:Landroid/graphics/drawable/Drawable;
        //   203: putfield        com/bumptech/glide/request/BaseRequestOptions.placeholderDrawable:Landroid/graphics/drawable/Drawable;
        //   206: aload_0        
        //   207: iconst_0       
        //   208: putfield        com/bumptech/glide/request/BaseRequestOptions.placeholderId:I
        //   211: aload_0        
        //   212: aload_0        
        //   213: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   216: sipush          -129
        //   219: iand           
        //   220: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   223: aload_1        
        //   224: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   227: sipush          128
        //   230: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   233: ifeq            260
        //   236: aload_0        
        //   237: aload_1        
        //   238: getfield        com/bumptech/glide/request/BaseRequestOptions.placeholderId:I
        //   241: putfield        com/bumptech/glide/request/BaseRequestOptions.placeholderId:I
        //   244: aload_0        
        //   245: aconst_null    
        //   246: putfield        com/bumptech/glide/request/BaseRequestOptions.placeholderDrawable:Landroid/graphics/drawable/Drawable;
        //   249: aload_0        
        //   250: aload_0        
        //   251: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   254: bipush          -65
        //   256: iand           
        //   257: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   260: aload_1        
        //   261: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   264: sipush          256
        //   267: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   270: ifeq            281
        //   273: aload_0        
        //   274: aload_1        
        //   275: getfield        com/bumptech/glide/request/BaseRequestOptions.isCacheable:Z
        //   278: putfield        com/bumptech/glide/request/BaseRequestOptions.isCacheable:Z
        //   281: aload_1        
        //   282: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   285: sipush          512
        //   288: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   291: ifeq            310
        //   294: aload_0        
        //   295: aload_1        
        //   296: getfield        com/bumptech/glide/request/BaseRequestOptions.overrideWidth:I
        //   299: putfield        com/bumptech/glide/request/BaseRequestOptions.overrideWidth:I
        //   302: aload_0        
        //   303: aload_1        
        //   304: getfield        com/bumptech/glide/request/BaseRequestOptions.overrideHeight:I
        //   307: putfield        com/bumptech/glide/request/BaseRequestOptions.overrideHeight:I
        //   310: aload_1        
        //   311: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   314: sipush          1024
        //   317: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   320: ifeq            331
        //   323: aload_0        
        //   324: aload_1        
        //   325: getfield        com/bumptech/glide/request/BaseRequestOptions.signature:Lcom/bumptech/glide/load/Key;
        //   328: putfield        com/bumptech/glide/request/BaseRequestOptions.signature:Lcom/bumptech/glide/load/Key;
        //   331: aload_1        
        //   332: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   335: sipush          4096
        //   338: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   341: ifeq            352
        //   344: aload_0        
        //   345: aload_1        
        //   346: getfield        com/bumptech/glide/request/BaseRequestOptions.resourceClass:Ljava/lang/Class;
        //   349: putfield        com/bumptech/glide/request/BaseRequestOptions.resourceClass:Ljava/lang/Class;
        //   352: aload_1        
        //   353: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   356: sipush          8192
        //   359: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   362: ifeq            390
        //   365: aload_0        
        //   366: aload_1        
        //   367: getfield        com/bumptech/glide/request/BaseRequestOptions.fallbackDrawable:Landroid/graphics/drawable/Drawable;
        //   370: putfield        com/bumptech/glide/request/BaseRequestOptions.fallbackDrawable:Landroid/graphics/drawable/Drawable;
        //   373: aload_0        
        //   374: iconst_0       
        //   375: putfield        com/bumptech/glide/request/BaseRequestOptions.fallbackId:I
        //   378: aload_0        
        //   379: aload_0        
        //   380: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   383: sipush          -16385
        //   386: iand           
        //   387: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   390: aload_1        
        //   391: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   394: sipush          16384
        //   397: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   400: ifeq            428
        //   403: aload_0        
        //   404: aload_1        
        //   405: getfield        com/bumptech/glide/request/BaseRequestOptions.fallbackId:I
        //   408: putfield        com/bumptech/glide/request/BaseRequestOptions.fallbackId:I
        //   411: aload_0        
        //   412: aconst_null    
        //   413: putfield        com/bumptech/glide/request/BaseRequestOptions.fallbackDrawable:Landroid/graphics/drawable/Drawable;
        //   416: aload_0        
        //   417: aload_0        
        //   418: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   421: sipush          -8193
        //   424: iand           
        //   425: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   428: aload_1        
        //   429: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   432: ldc             32768
        //   434: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   437: ifeq            448
        //   440: aload_0        
        //   441: aload_1        
        //   442: getfield        com/bumptech/glide/request/BaseRequestOptions.theme:Landroid/content/res/Resources$Theme;
        //   445: putfield        com/bumptech/glide/request/BaseRequestOptions.theme:Landroid/content/res/Resources$Theme;
        //   448: aload_1        
        //   449: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   452: ldc             65536
        //   454: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   457: ifeq            468
        //   460: aload_0        
        //   461: aload_1        
        //   462: getfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationAllowed:Z
        //   465: putfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationAllowed:Z
        //   468: aload_1        
        //   469: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   472: ldc             131072
        //   474: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   477: ifeq            488
        //   480: aload_0        
        //   481: aload_1        
        //   482: getfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationRequired:Z
        //   485: putfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationRequired:Z
        //   488: aload_1        
        //   489: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   492: sipush          2048
        //   495: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   498: ifeq            522
        //   501: aload_0        
        //   502: getfield        com/bumptech/glide/request/BaseRequestOptions.transformations:Ljava/util/Map;
        //   505: aload_1        
        //   506: getfield        com/bumptech/glide/request/BaseRequestOptions.transformations:Ljava/util/Map;
        //   509: invokeinterface java/util/Map.putAll:(Ljava/util/Map;)V
        //   514: aload_0        
        //   515: aload_1        
        //   516: getfield        com/bumptech/glide/request/BaseRequestOptions.isScaleOnlyOrNoTransform:Z
        //   519: putfield        com/bumptech/glide/request/BaseRequestOptions.isScaleOnlyOrNoTransform:Z
        //   522: aload_1        
        //   523: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   526: ldc             524288
        //   528: invokestatic    com/bumptech/glide/request/BaseRequestOptions.isSet:(II)Z
        //   531: ifeq            542
        //   534: aload_0        
        //   535: aload_1        
        //   536: getfield        com/bumptech/glide/request/BaseRequestOptions.onlyRetrieveFromCache:Z
        //   539: putfield        com/bumptech/glide/request/BaseRequestOptions.onlyRetrieveFromCache:Z
        //   542: aload_0        
        //   543: getfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationAllowed:Z
        //   546: ifne            585
        //   549: aload_0        
        //   550: getfield        com/bumptech/glide/request/BaseRequestOptions.transformations:Ljava/util/Map;
        //   553: invokeinterface java/util/Map.clear:()V
        //   558: aload_0        
        //   559: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   562: istore_2       
        //   563: aload_0        
        //   564: iconst_0       
        //   565: putfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationRequired:Z
        //   568: aload_0        
        //   569: iload_2        
        //   570: sipush          -2049
        //   573: iand           
        //   574: ldc             -131073
        //   576: iand           
        //   577: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   580: aload_0        
        //   581: iconst_1       
        //   582: putfield        com/bumptech/glide/request/BaseRequestOptions.isScaleOnlyOrNoTransform:Z
        //   585: aload_0        
        //   586: aload_0        
        //   587: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   590: aload_1        
        //   591: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   594: ior            
        //   595: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //   598: aload_0        
        //   599: getfield        com/bumptech/glide/request/BaseRequestOptions.options:Lcom/bumptech/glide/load/Options;
        //   602: aload_1        
        //   603: getfield        com/bumptech/glide/request/BaseRequestOptions.options:Lcom/bumptech/glide/load/Options;
        //   606: invokevirtual   com/bumptech/glide/load/Options.putAll:(Lcom/bumptech/glide/load/Options;)V
        //   609: aload_0        
        //   610: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //   613: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/request/BaseRequestOptions<*>;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T autoClone() {
        if (this.isLocked && !this.isAutoCloneEnabled) {
            throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
        }
        this.isAutoCloneEnabled = true;
        return this.lock();
    }
    
    public T centerCrop() {
        return this.transform(DownsampleStrategy.CENTER_OUTSIDE, new CenterCrop());
    }
    
    public T centerInside() {
        return this.scaleOnlyTransform(DownsampleStrategy.CENTER_INSIDE, new CenterInside());
    }
    
    public T circleCrop() {
        return this.transform(DownsampleStrategy.CENTER_INSIDE, new CircleCrop());
    }
    
    public T clone() {
        try {
            final BaseRequestOptions baseRequestOptions = (BaseRequestOptions)super.clone();
            (baseRequestOptions.options = new Options()).putAll(this.options);
            (baseRequestOptions.transformations = new CachedHashCodeArrayMap<Class<?>, Transformation<?>>()).putAll(this.transformations);
            baseRequestOptions.isLocked = false;
            baseRequestOptions.isAutoCloneEnabled = false;
            return (T)baseRequestOptions;
        }
        catch (final CloneNotSupportedException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    public T decode(final Class<?> p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.decode:(Ljava/lang/Class;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: aload_1        
        //    18: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    21: checkcast       Ljava/lang/Class;
        //    24: putfield        com/bumptech/glide/request/BaseRequestOptions.resourceClass:Ljava/lang/Class;
        //    27: aload_0        
        //    28: aload_0        
        //    29: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    32: sipush          4096
        //    35: ior            
        //    36: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    39: aload_0        
        //    40: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    43: areturn        
        //    Signature:
        //  (Ljava/lang/Class<*>;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T disallowHardwareConfig() {
        return this.set(Downsampler.ALLOW_HARDWARE_CONFIG, false);
    }
    
    public T diskCacheStrategy(final DiskCacheStrategy p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.diskCacheStrategy:(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: aload_1        
        //    18: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    21: checkcast       Lcom/bumptech/glide/load/engine/DiskCacheStrategy;
        //    24: putfield        com/bumptech/glide/request/BaseRequestOptions.diskCacheStrategy:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;
        //    27: aload_0        
        //    28: aload_0        
        //    29: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    32: iconst_4       
        //    33: ior            
        //    34: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    37: aload_0        
        //    38: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    41: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T dontAnimate() {
        return this.set(GifOptions.DISABLE_ANIMATION, true);
    }
    
    public T dontTransform() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            15
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.dontTransform:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    14: areturn        
        //    15: aload_0        
        //    16: getfield        com/bumptech/glide/request/BaseRequestOptions.transformations:Ljava/util/Map;
        //    19: invokeinterface java/util/Map.clear:()V
        //    24: aload_0        
        //    25: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    28: istore_1       
        //    29: aload_0        
        //    30: iconst_0       
        //    31: putfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationRequired:Z
        //    34: aload_0        
        //    35: iconst_0       
        //    36: putfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationAllowed:Z
        //    39: aload_0        
        //    40: iload_1        
        //    41: sipush          -2049
        //    44: iand           
        //    45: ldc             -131073
        //    47: iand           
        //    48: ldc             65536
        //    50: ior            
        //    51: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    54: aload_0        
        //    55: iconst_1       
        //    56: putfield        com/bumptech/glide/request/BaseRequestOptions.isScaleOnlyOrNoTransform:Z
        //    59: aload_0        
        //    60: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    63: areturn        
        //    Signature:
        //  ()TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T downsample(final DownsampleStrategy downsampleStrategy) {
        return this.set(DownsampleStrategy.OPTION, (DownsampleStrategy)Preconditions.checkNotNull((Y)downsampleStrategy));
    }
    
    public T encodeFormat(final Bitmap$CompressFormat bitmap$CompressFormat) {
        return this.set(BitmapEncoder.COMPRESSION_FORMAT, (Bitmap$CompressFormat)Preconditions.checkNotNull((Y)bitmap$CompressFormat));
    }
    
    public T encodeQuality(final int i) {
        return this.set(BitmapEncoder.COMPRESSION_QUALITY, i);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof BaseRequestOptions;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final BaseRequestOptions baseRequestOptions = (BaseRequestOptions)o;
            b3 = b2;
            if (Float.compare(baseRequestOptions.sizeMultiplier, this.sizeMultiplier) == 0) {
                b3 = b2;
                if (this.errorId == baseRequestOptions.errorId) {
                    b3 = b2;
                    if (Util.bothNullOrEqual(this.errorPlaceholder, baseRequestOptions.errorPlaceholder)) {
                        b3 = b2;
                        if (this.placeholderId == baseRequestOptions.placeholderId) {
                            b3 = b2;
                            if (Util.bothNullOrEqual(this.placeholderDrawable, baseRequestOptions.placeholderDrawable)) {
                                b3 = b2;
                                if (this.fallbackId == baseRequestOptions.fallbackId) {
                                    b3 = b2;
                                    if (Util.bothNullOrEqual(this.fallbackDrawable, baseRequestOptions.fallbackDrawable)) {
                                        b3 = b2;
                                        if (this.isCacheable == baseRequestOptions.isCacheable) {
                                            b3 = b2;
                                            if (this.overrideHeight == baseRequestOptions.overrideHeight) {
                                                b3 = b2;
                                                if (this.overrideWidth == baseRequestOptions.overrideWidth) {
                                                    b3 = b2;
                                                    if (this.isTransformationRequired == baseRequestOptions.isTransformationRequired) {
                                                        b3 = b2;
                                                        if (this.isTransformationAllowed == baseRequestOptions.isTransformationAllowed) {
                                                            b3 = b2;
                                                            if (this.useUnlimitedSourceGeneratorsPool == baseRequestOptions.useUnlimitedSourceGeneratorsPool) {
                                                                b3 = b2;
                                                                if (this.onlyRetrieveFromCache == baseRequestOptions.onlyRetrieveFromCache) {
                                                                    b3 = b2;
                                                                    if (this.diskCacheStrategy.equals(baseRequestOptions.diskCacheStrategy)) {
                                                                        b3 = b2;
                                                                        if (this.priority == baseRequestOptions.priority) {
                                                                            b3 = b2;
                                                                            if (this.options.equals(baseRequestOptions.options)) {
                                                                                b3 = b2;
                                                                                if (this.transformations.equals(baseRequestOptions.transformations)) {
                                                                                    b3 = b2;
                                                                                    if (this.resourceClass.equals(baseRequestOptions.resourceClass)) {
                                                                                        b3 = b2;
                                                                                        if (Util.bothNullOrEqual(this.signature, baseRequestOptions.signature)) {
                                                                                            b3 = b2;
                                                                                            if (Util.bothNullOrEqual(this.theme, baseRequestOptions.theme)) {
                                                                                                b3 = true;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    public T error(final int p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: iload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.error:(I)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: iload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.errorId:I
        //    21: aload_0        
        //    22: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    25: istore_1       
        //    26: aload_0        
        //    27: aconst_null    
        //    28: putfield        com/bumptech/glide/request/BaseRequestOptions.errorPlaceholder:Landroid/graphics/drawable/Drawable;
        //    31: aload_0        
        //    32: iload_1        
        //    33: bipush          32
        //    35: ior            
        //    36: bipush          -17
        //    38: iand           
        //    39: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    42: aload_0        
        //    43: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    46: areturn        
        //    Signature:
        //  (I)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T error(final Drawable p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.error:(Landroid/graphics/drawable/Drawable;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: aload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.errorPlaceholder:Landroid/graphics/drawable/Drawable;
        //    21: aload_0        
        //    22: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    25: istore_2       
        //    26: aload_0        
        //    27: iconst_0       
        //    28: putfield        com/bumptech/glide/request/BaseRequestOptions.errorId:I
        //    31: aload_0        
        //    32: iload_2        
        //    33: bipush          16
        //    35: ior            
        //    36: bipush          -33
        //    38: iand           
        //    39: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    42: aload_0        
        //    43: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    46: areturn        
        //    Signature:
        //  (Landroid/graphics/drawable/Drawable;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T fallback(final int p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: iload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.fallback:(I)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: iload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.fallbackId:I
        //    21: aload_0        
        //    22: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    25: istore_1       
        //    26: aload_0        
        //    27: aconst_null    
        //    28: putfield        com/bumptech/glide/request/BaseRequestOptions.fallbackDrawable:Landroid/graphics/drawable/Drawable;
        //    31: aload_0        
        //    32: iload_1        
        //    33: sipush          16384
        //    36: ior            
        //    37: sipush          -8193
        //    40: iand           
        //    41: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    44: aload_0        
        //    45: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    48: areturn        
        //    Signature:
        //  (I)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T fallback(final Drawable p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.fallback:(Landroid/graphics/drawable/Drawable;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: aload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.fallbackDrawable:Landroid/graphics/drawable/Drawable;
        //    21: aload_0        
        //    22: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    25: istore_2       
        //    26: aload_0        
        //    27: iconst_0       
        //    28: putfield        com/bumptech/glide/request/BaseRequestOptions.fallbackId:I
        //    31: aload_0        
        //    32: iload_2        
        //    33: sipush          8192
        //    36: ior            
        //    37: sipush          -16385
        //    40: iand           
        //    41: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    44: aload_0        
        //    45: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    48: areturn        
        //    Signature:
        //  (Landroid/graphics/drawable/Drawable;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T fitCenter() {
        return this.scaleOnlyTransform(DownsampleStrategy.FIT_CENTER, new FitCenter());
    }
    
    public T format(final DecodeFormat p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //     4: pop            
        //     5: aload_0        
        //     6: getstatic       com/bumptech/glide/load/resource/bitmap/Downsampler.DECODE_FORMAT:Lcom/bumptech/glide/load/Option;
        //     9: aload_1        
        //    10: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.set:(Lcom/bumptech/glide/load/Option;Ljava/lang/Object;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    13: getstatic       com/bumptech/glide/load/resource/gif/GifOptions.DECODE_FORMAT:Lcom/bumptech/glide/load/Option;
        //    16: aload_1        
        //    17: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.set:(Lcom/bumptech/glide/load/Option;Ljava/lang/Object;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    20: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/load/DecodeFormat;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T frame(final long l) {
        return this.set(VideoDecoder.TARGET_FRAME, l);
    }
    
    public final DiskCacheStrategy getDiskCacheStrategy() {
        return this.diskCacheStrategy;
    }
    
    public final int getErrorId() {
        return this.errorId;
    }
    
    public final Drawable getErrorPlaceholder() {
        return this.errorPlaceholder;
    }
    
    public final Drawable getFallbackDrawable() {
        return this.fallbackDrawable;
    }
    
    public final int getFallbackId() {
        return this.fallbackId;
    }
    
    public final boolean getOnlyRetrieveFromCache() {
        return this.onlyRetrieveFromCache;
    }
    
    public final Options getOptions() {
        return this.options;
    }
    
    public final int getOverrideHeight() {
        return this.overrideHeight;
    }
    
    public final int getOverrideWidth() {
        return this.overrideWidth;
    }
    
    public final Drawable getPlaceholderDrawable() {
        return this.placeholderDrawable;
    }
    
    public final int getPlaceholderId() {
        return this.placeholderId;
    }
    
    public final Priority getPriority() {
        return this.priority;
    }
    
    public final Class<?> getResourceClass() {
        return this.resourceClass;
    }
    
    public final Key getSignature() {
        return this.signature;
    }
    
    public final float getSizeMultiplier() {
        return this.sizeMultiplier;
    }
    
    public final Resources$Theme getTheme() {
        return this.theme;
    }
    
    public final Map<Class<?>, Transformation<?>> getTransformations() {
        return this.transformations;
    }
    
    public final boolean getUseAnimationPool() {
        return this.useAnimationPool;
    }
    
    public final boolean getUseUnlimitedSourceGeneratorsPool() {
        return this.useUnlimitedSourceGeneratorsPool;
    }
    
    @Override
    public int hashCode() {
        return Util.hashCode(this.theme, Util.hashCode(this.signature, Util.hashCode(this.resourceClass, Util.hashCode(this.transformations, Util.hashCode(this.options, Util.hashCode(this.priority, Util.hashCode(this.diskCacheStrategy, Util.hashCode(this.onlyRetrieveFromCache, Util.hashCode(this.useUnlimitedSourceGeneratorsPool, Util.hashCode(this.isTransformationAllowed, Util.hashCode(this.isTransformationRequired, Util.hashCode(this.overrideWidth, Util.hashCode(this.overrideHeight, Util.hashCode(this.isCacheable, Util.hashCode(this.fallbackDrawable, Util.hashCode(this.fallbackId, Util.hashCode(this.placeholderDrawable, Util.hashCode(this.placeholderId, Util.hashCode(this.errorPlaceholder, Util.hashCode(this.errorId, Util.hashCode(this.sizeMultiplier)))))))))))))))))))));
    }
    
    protected final boolean isAutoCloneEnabled() {
        return this.isAutoCloneEnabled;
    }
    
    public final boolean isDiskCacheStrategySet() {
        return this.isSet(4);
    }
    
    public final boolean isLocked() {
        return this.isLocked;
    }
    
    public final boolean isMemoryCacheable() {
        return this.isCacheable;
    }
    
    public final boolean isPrioritySet() {
        return this.isSet(8);
    }
    
    boolean isScaleOnlyOrNoTransform() {
        return this.isScaleOnlyOrNoTransform;
    }
    
    public final boolean isSkipMemoryCacheSet() {
        return this.isSet(256);
    }
    
    public final boolean isTransformationAllowed() {
        return this.isTransformationAllowed;
    }
    
    public final boolean isTransformationRequired() {
        return this.isTransformationRequired;
    }
    
    public final boolean isTransformationSet() {
        return this.isSet(2048);
    }
    
    public final boolean isValidOverride() {
        return Util.isValidDimensions(this.overrideWidth, this.overrideHeight);
    }
    
    public T lock() {
        this.isLocked = true;
        return this.self();
    }
    
    public T onlyRetrieveFromCache(final boolean p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: iload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.onlyRetrieveFromCache:(Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: iload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.onlyRetrieveFromCache:Z
        //    21: aload_0        
        //    22: aload_0        
        //    23: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    26: ldc             524288
        //    28: ior            
        //    29: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    32: aload_0        
        //    33: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    36: areturn        
        //    Signature:
        //  (Z)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T optionalCenterCrop() {
        return this.optionalTransform(DownsampleStrategy.CENTER_OUTSIDE, new CenterCrop());
    }
    
    public T optionalCenterInside() {
        return this.optionalScaleOnlyTransform(DownsampleStrategy.CENTER_INSIDE, new CenterInside());
    }
    
    public T optionalCircleCrop() {
        return this.optionalTransform(DownsampleStrategy.CENTER_OUTSIDE, new CircleCrop());
    }
    
    public T optionalFitCenter() {
        return this.optionalScaleOnlyTransform(DownsampleStrategy.FIT_CENTER, new FitCenter());
    }
    
    public T optionalTransform(final Transformation<Bitmap> transformation) {
        return this.transform(transformation, false);
    }
    
    final T optionalTransform(final DownsampleStrategy p0, final Transformation<Bitmap> p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            17
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: aload_2        
        //    13: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.optionalTransform:(Lcom/bumptech/glide/load/resource/bitmap/DownsampleStrategy;Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    16: areturn        
        //    17: aload_0        
        //    18: aload_1        
        //    19: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.downsample:(Lcom/bumptech/glide/load/resource/bitmap/DownsampleStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    22: pop            
        //    23: aload_0        
        //    24: aload_2        
        //    25: iconst_0       
        //    26: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Lcom/bumptech/glide/load/Transformation;Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    29: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/load/resource/bitmap/DownsampleStrategy;Lcom/bumptech/glide/load/Transformation<Landroid/graphics/Bitmap;>;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public <Y> T optionalTransform(final Class<Y> clazz, final Transformation<Y> transformation) {
        return this.transform(clazz, transformation, false);
    }
    
    public T override(final int n) {
        return this.override(n, n);
    }
    
    public T override(final int p0, final int p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            17
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: iload_1        
        //    12: iload_2        
        //    13: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.override:(II)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    16: areturn        
        //    17: aload_0        
        //    18: iload_1        
        //    19: putfield        com/bumptech/glide/request/BaseRequestOptions.overrideWidth:I
        //    22: aload_0        
        //    23: iload_2        
        //    24: putfield        com/bumptech/glide/request/BaseRequestOptions.overrideHeight:I
        //    27: aload_0        
        //    28: aload_0        
        //    29: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    32: sipush          512
        //    35: ior            
        //    36: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    39: aload_0        
        //    40: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    43: areturn        
        //    Signature:
        //  (II)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T placeholder(final int p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: iload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.placeholder:(I)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: iload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.placeholderId:I
        //    21: aload_0        
        //    22: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    25: istore_1       
        //    26: aload_0        
        //    27: aconst_null    
        //    28: putfield        com/bumptech/glide/request/BaseRequestOptions.placeholderDrawable:Landroid/graphics/drawable/Drawable;
        //    31: aload_0        
        //    32: iload_1        
        //    33: sipush          128
        //    36: ior            
        //    37: bipush          -65
        //    39: iand           
        //    40: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    43: aload_0        
        //    44: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    47: areturn        
        //    Signature:
        //  (I)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T placeholder(final Drawable p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.placeholder:(Landroid/graphics/drawable/Drawable;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: aload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.placeholderDrawable:Landroid/graphics/drawable/Drawable;
        //    21: aload_0        
        //    22: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    25: istore_2       
        //    26: aload_0        
        //    27: iconst_0       
        //    28: putfield        com/bumptech/glide/request/BaseRequestOptions.placeholderId:I
        //    31: aload_0        
        //    32: iload_2        
        //    33: bipush          64
        //    35: ior            
        //    36: sipush          -129
        //    39: iand           
        //    40: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    43: aload_0        
        //    44: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    47: areturn        
        //    Signature:
        //  (Landroid/graphics/drawable/Drawable;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T priority(final Priority p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.priority:(Lcom/bumptech/glide/Priority;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: aload_1        
        //    18: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    21: checkcast       Lcom/bumptech/glide/Priority;
        //    24: putfield        com/bumptech/glide/request/BaseRequestOptions.priority:Lcom/bumptech/glide/Priority;
        //    27: aload_0        
        //    28: aload_0        
        //    29: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    32: bipush          8
        //    34: ior            
        //    35: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    38: aload_0        
        //    39: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    42: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/Priority;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    T removeOption(final Option<?> p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.removeOption:(Lcom/bumptech/glide/load/Option;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: getfield        com/bumptech/glide/request/BaseRequestOptions.options:Lcom/bumptech/glide/load/Options;
        //    20: aload_1        
        //    21: invokevirtual   com/bumptech/glide/load/Options.remove:(Lcom/bumptech/glide/load/Option;)Lcom/bumptech/glide/load/Options;
        //    24: pop            
        //    25: aload_0        
        //    26: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    29: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/load/Option<*>;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected final T selfOrThrowIfLocked() {
        if (!this.isLocked) {
            return this.self();
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }
    
    public <Y> T set(final Option<Y> p0, final Y p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            17
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: aload_2        
        //    13: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.set:(Lcom/bumptech/glide/load/Option;Ljava/lang/Object;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    16: areturn        
        //    17: aload_1        
        //    18: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    21: pop            
        //    22: aload_2        
        //    23: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    26: pop            
        //    27: aload_0        
        //    28: getfield        com/bumptech/glide/request/BaseRequestOptions.options:Lcom/bumptech/glide/load/Options;
        //    31: aload_1        
        //    32: aload_2        
        //    33: invokevirtual   com/bumptech/glide/load/Options.set:(Lcom/bumptech/glide/load/Option;Ljava/lang/Object;)Lcom/bumptech/glide/load/Options;
        //    36: pop            
        //    37: aload_0        
        //    38: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    41: areturn        
        //    Signature:
        //  <Y:Ljava/lang/Object;>(Lcom/bumptech/glide/load/Option<TY;>;TY;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T signature(final Key p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.signature:(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: aload_1        
        //    18: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    21: checkcast       Lcom/bumptech/glide/load/Key;
        //    24: putfield        com/bumptech/glide/request/BaseRequestOptions.signature:Lcom/bumptech/glide/load/Key;
        //    27: aload_0        
        //    28: aload_0        
        //    29: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    32: sipush          1024
        //    35: ior            
        //    36: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    39: aload_0        
        //    40: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    43: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/load/Key;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T sizeMultiplier(final float p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: fload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.sizeMultiplier:(F)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: fload_1        
        //    17: fconst_0       
        //    18: fcmpg          
        //    19: iflt            48
        //    22: fload_1        
        //    23: fconst_1       
        //    24: fcmpl          
        //    25: ifgt            48
        //    28: aload_0        
        //    29: fload_1        
        //    30: putfield        com/bumptech/glide/request/BaseRequestOptions.sizeMultiplier:F
        //    33: aload_0        
        //    34: aload_0        
        //    35: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    38: iconst_2       
        //    39: ior            
        //    40: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    43: aload_0        
        //    44: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    47: areturn        
        //    48: new             Ljava/lang/IllegalArgumentException;
        //    51: dup            
        //    52: ldc_w           "sizeMultiplier must be between 0 and 1"
        //    55: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //    58: athrow         
        //    Signature:
        //  (F)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T skipMemoryCache(final boolean p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: iconst_1       
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.skipMemoryCache:(Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: iload_1        
        //    18: iconst_1       
        //    19: ixor           
        //    20: putfield        com/bumptech/glide/request/BaseRequestOptions.isCacheable:Z
        //    23: aload_0        
        //    24: aload_0        
        //    25: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    28: sipush          256
        //    31: ior            
        //    32: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    35: aload_0        
        //    36: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    39: areturn        
        //    Signature:
        //  (Z)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T theme(final Resources$Theme p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.theme:(Landroid/content/res/Resources$Theme;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: aload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.theme:Landroid/content/res/Resources$Theme;
        //    21: aload_1        
        //    22: ifnull          45
        //    25: aload_0        
        //    26: aload_0        
        //    27: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    30: ldc             32768
        //    32: ior            
        //    33: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    36: aload_0        
        //    37: getstatic       com/bumptech/glide/load/resource/drawable/ResourceDrawableDecoder.THEME:Lcom/bumptech/glide/load/Option;
        //    40: aload_1        
        //    41: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.set:(Lcom/bumptech/glide/load/Option;Ljava/lang/Object;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    44: areturn        
        //    45: aload_0        
        //    46: aload_0        
        //    47: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    50: ldc_w           -32769
        //    53: iand           
        //    54: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    57: aload_0        
        //    58: getstatic       com/bumptech/glide/load/resource/drawable/ResourceDrawableDecoder.THEME:Lcom/bumptech/glide/load/Option;
        //    61: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.removeOption:(Lcom/bumptech/glide/load/Option;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    64: areturn        
        //    Signature:
        //  (Landroid/content/res/Resources$Theme;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T timeout(final int i) {
        return this.set(HttpGlideUrlLoader.TIMEOUT, i);
    }
    
    public T transform(final Transformation<Bitmap> transformation) {
        return this.transform(transformation, true);
    }
    
    T transform(final Transformation<Bitmap> p0, final boolean p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: ifeq            17
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: iload_2        
        //    13: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Lcom/bumptech/glide/load/Transformation;Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    16: areturn        
        //    17: new             Lcom/bumptech/glide/load/resource/bitmap/DrawableTransformation;
        //    20: dup            
        //    21: aload_1        
        //    22: iload_2        
        //    23: invokespecial   com/bumptech/glide/load/resource/bitmap/DrawableTransformation.<init>:(Lcom/bumptech/glide/load/Transformation;Z)V
        //    26: astore_3       
        //    27: aload_0        
        //    28: ldc_w           Landroid/graphics/Bitmap;.class
        //    31: aload_1        
        //    32: iload_2        
        //    33: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Ljava/lang/Class;Lcom/bumptech/glide/load/Transformation;Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    36: pop            
        //    37: aload_0        
        //    38: ldc_w           Landroid/graphics/drawable/Drawable;.class
        //    41: aload_3        
        //    42: iload_2        
        //    43: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Ljava/lang/Class;Lcom/bumptech/glide/load/Transformation;Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    46: pop            
        //    47: aload_0        
        //    48: ldc_w           Landroid/graphics/drawable/BitmapDrawable;.class
        //    51: aload_3        
        //    52: invokevirtual   com/bumptech/glide/load/resource/bitmap/DrawableTransformation.asBitmapDrawable:()Lcom/bumptech/glide/load/Transformation;
        //    55: iload_2        
        //    56: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Ljava/lang/Class;Lcom/bumptech/glide/load/Transformation;Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    59: pop            
        //    60: aload_0        
        //    61: ldc_w           Lcom/bumptech/glide/load/resource/gif/GifDrawable;.class
        //    64: new             Lcom/bumptech/glide/load/resource/gif/GifDrawableTransformation;
        //    67: dup            
        //    68: aload_1        
        //    69: invokespecial   com/bumptech/glide/load/resource/gif/GifDrawableTransformation.<init>:(Lcom/bumptech/glide/load/Transformation;)V
        //    72: iload_2        
        //    73: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Ljava/lang/Class;Lcom/bumptech/glide/load/Transformation;Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    76: pop            
        //    77: aload_0        
        //    78: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    81: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/load/Transformation<Landroid/graphics/Bitmap;>;Z)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.invalidateDependentExpressions(TypeAnalysis.java:771)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1022)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:782)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:778)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2535)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:109)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    final T transform(final DownsampleStrategy p0, final Transformation<Bitmap> p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            17
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: aload_2        
        //    13: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Lcom/bumptech/glide/load/resource/bitmap/DownsampleStrategy;Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    16: areturn        
        //    17: aload_0        
        //    18: aload_1        
        //    19: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.downsample:(Lcom/bumptech/glide/load/resource/bitmap/DownsampleStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    22: pop            
        //    23: aload_0        
        //    24: aload_2        
        //    25: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    28: areturn        
        //    Signature:
        //  (Lcom/bumptech/glide/load/resource/bitmap/DownsampleStrategy;Lcom/bumptech/glide/load/Transformation<Landroid/graphics/Bitmap;>;)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public <Y> T transform(final Class<Y> clazz, final Transformation<Y> transformation) {
        return this.transform(clazz, transformation, true);
    }
    
     <Y> T transform(final Class<Y> p0, final Transformation<Y> p1, final boolean p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: ifeq            18
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: aload_1        
        //    12: aload_2        
        //    13: iload_3        
        //    14: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.transform:(Ljava/lang/Class;Lcom/bumptech/glide/load/Transformation;Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    17: areturn        
        //    18: aload_1        
        //    19: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    22: pop            
        //    23: aload_2        
        //    24: invokestatic    com/bumptech/glide/util/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    27: pop            
        //    28: aload_0        
        //    29: getfield        com/bumptech/glide/request/BaseRequestOptions.transformations:Ljava/util/Map;
        //    32: aload_1        
        //    33: aload_2        
        //    34: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //    39: pop            
        //    40: aload_0        
        //    41: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    44: istore          4
        //    46: aload_0        
        //    47: iconst_1       
        //    48: putfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationAllowed:Z
        //    51: iload           4
        //    53: sipush          2048
        //    56: ior            
        //    57: ldc             65536
        //    59: ior            
        //    60: istore          4
        //    62: aload_0        
        //    63: iload           4
        //    65: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    68: aload_0        
        //    69: iconst_0       
        //    70: putfield        com/bumptech/glide/request/BaseRequestOptions.isScaleOnlyOrNoTransform:Z
        //    73: iload_3        
        //    74: ifeq            91
        //    77: aload_0        
        //    78: iload           4
        //    80: ldc             131072
        //    82: ior            
        //    83: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    86: aload_0        
        //    87: iconst_1       
        //    88: putfield        com/bumptech/glide/request/BaseRequestOptions.isTransformationRequired:Z
        //    91: aload_0        
        //    92: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    95: areturn        
        //    Signature:
        //  <Y:Ljava/lang/Object;>(Ljava/lang/Class<TY;>;Lcom/bumptech/glide/load/Transformation<TY;>;Z)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T transform(final Transformation<Bitmap>... array) {
        if (array.length > 1) {
            return this.transform(new MultiTransformation<Bitmap>(array), true);
        }
        if (array.length == 1) {
            return this.transform(array[0]);
        }
        return this.selfOrThrowIfLocked();
    }
    
    @Deprecated
    public T transforms(final Transformation<Bitmap>... array) {
        return this.transform(new MultiTransformation<Bitmap>(array), true);
    }
    
    public T useAnimationPool(final boolean p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: iload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.useAnimationPool:(Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: iload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.useAnimationPool:Z
        //    21: aload_0        
        //    22: aload_0        
        //    23: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    26: ldc             1048576
        //    28: ior            
        //    29: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    32: aload_0        
        //    33: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    36: areturn        
        //    Signature:
        //  (Z)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public T useUnlimitedSourceGeneratorsPool(final boolean p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/BaseRequestOptions.isAutoCloneEnabled:Z
        //     4: ifeq            16
        //     7: aload_0        
        //     8: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.clone:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    11: iload_1        
        //    12: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.useUnlimitedSourceGeneratorsPool:(Z)Lcom/bumptech/glide/request/BaseRequestOptions;
        //    15: areturn        
        //    16: aload_0        
        //    17: iload_1        
        //    18: putfield        com/bumptech/glide/request/BaseRequestOptions.useUnlimitedSourceGeneratorsPool:Z
        //    21: aload_0        
        //    22: aload_0        
        //    23: getfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    26: ldc             262144
        //    28: ior            
        //    29: putfield        com/bumptech/glide/request/BaseRequestOptions.fields:I
        //    32: aload_0        
        //    33: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.selfOrThrowIfLocked:()Lcom/bumptech/glide/request/BaseRequestOptions;
        //    36: areturn        
        //    Signature:
        //  (Z)TT;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException: The requested operation is not supported.
        //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
        //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
        //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
        //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
        //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:314)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:790)
        //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1670)
        //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
        //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:373)
        //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:344)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

import java.io.Serializable;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.util.pool.GlideTrace;
import com.bumptech.glide.load.engine.GlideException;
import android.content.res.Resources$Theme;
import com.bumptech.glide.load.resource.drawable.DrawableDecoderCompat;
import java.util.Iterator;
import com.bumptech.glide.GlideExperiments;
import com.bumptech.glide.GlideBuilder;
import android.util.Log;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.util.pool.StateVerifier;
import com.bumptech.glide.load.engine.Resource;
import java.util.List;
import com.bumptech.glide.Priority;
import com.bumptech.glide.GlideContext;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.Engine;
import android.content.Context;
import java.util.concurrent.Executor;
import com.bumptech.glide.request.transition.TransitionFactory;
import com.bumptech.glide.request.target.SizeReadyCallback;

public final class SingleRequest<R> implements Request, SizeReadyCallback, ResourceCallback
{
    private static final String GLIDE_TAG = "Glide";
    private static final boolean IS_VERBOSE_LOGGABLE;
    private static final String TAG = "GlideRequest";
    private final TransitionFactory<? super R> animationFactory;
    private final Executor callbackExecutor;
    private final Context context;
    private int cookie;
    private volatile Engine engine;
    private Drawable errorDrawable;
    private Drawable fallbackDrawable;
    private final GlideContext glideContext;
    private int height;
    private boolean isCallingCallbacks;
    private Engine.LoadStatus loadStatus;
    private final Object model;
    private final int overrideHeight;
    private final int overrideWidth;
    private Drawable placeholderDrawable;
    private final Priority priority;
    private final RequestCoordinator requestCoordinator;
    private final List<RequestListener<R>> requestListeners;
    private final Object requestLock;
    private final BaseRequestOptions<?> requestOptions;
    private RuntimeException requestOrigin;
    private Resource<R> resource;
    private long startTime;
    private final StateVerifier stateVerifier;
    private Status status;
    private final String tag;
    private final Target<R> target;
    private final RequestListener<R> targetListener;
    private final Class<R> transcodeClass;
    private int width;
    
    static {
        IS_VERBOSE_LOGGABLE = Log.isLoggable("GlideRequest", 2);
    }
    
    private SingleRequest(final Context context, final GlideContext glideContext, final Object requestLock, final Object model, final Class<R> transcodeClass, final BaseRequestOptions<?> requestOptions, final int overrideWidth, final int overrideHeight, final Priority priority, final Target<R> target, final RequestListener<R> targetListener, final List<RequestListener<R>> requestListeners, final RequestCoordinator requestCoordinator, final Engine engine, final TransitionFactory<? super R> animationFactory, final Executor callbackExecutor) {
        String value;
        if (SingleRequest.IS_VERBOSE_LOGGABLE) {
            value = String.valueOf(super.hashCode());
        }
        else {
            value = null;
        }
        this.tag = value;
        this.stateVerifier = StateVerifier.newInstance();
        this.requestLock = requestLock;
        this.context = context;
        this.glideContext = glideContext;
        this.model = model;
        this.transcodeClass = transcodeClass;
        this.requestOptions = requestOptions;
        this.overrideWidth = overrideWidth;
        this.overrideHeight = overrideHeight;
        this.priority = priority;
        this.target = target;
        this.targetListener = targetListener;
        this.requestListeners = requestListeners;
        this.requestCoordinator = requestCoordinator;
        this.engine = engine;
        this.animationFactory = animationFactory;
        this.callbackExecutor = callbackExecutor;
        this.status = Status.PENDING;
        if (this.requestOrigin == null && glideContext.getExperiments().isEnabled((Class<? extends GlideExperiments.Experiment>)GlideBuilder.LogRequestOrigins.class)) {
            this.requestOrigin = new RuntimeException("Glide request origin trace");
        }
    }
    
    private void assertNotCallingCallbacks() {
        if (!this.isCallingCallbacks) {
            return;
        }
        throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you're trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead.");
    }
    
    private boolean canNotifyCleared() {
        final RequestCoordinator requestCoordinator = this.requestCoordinator;
        return requestCoordinator == null || requestCoordinator.canNotifyCleared(this);
    }
    
    private boolean canNotifyStatusChanged() {
        final RequestCoordinator requestCoordinator = this.requestCoordinator;
        return requestCoordinator == null || requestCoordinator.canNotifyStatusChanged(this);
    }
    
    private boolean canSetResource() {
        final RequestCoordinator requestCoordinator = this.requestCoordinator;
        return requestCoordinator == null || requestCoordinator.canSetImage(this);
    }
    
    private void cancel() {
        this.assertNotCallingCallbacks();
        this.stateVerifier.throwIfRecycled();
        this.target.removeCallback(this);
        final Engine.LoadStatus loadStatus = this.loadStatus;
        if (loadStatus != null) {
            loadStatus.cancel();
            this.loadStatus = null;
        }
    }
    
    private void experimentalNotifyRequestStarted(final Object o) {
        final List<RequestListener<R>> requestListeners = this.requestListeners;
        if (requestListeners == null) {
            return;
        }
        for (final RequestListener requestListener : requestListeners) {
            if (requestListener instanceof ExperimentalRequestListener) {
                ((ExperimentalRequestListener)requestListener).onRequestStarted(o);
            }
        }
    }
    
    private Drawable getErrorDrawable() {
        if (this.errorDrawable == null && (this.errorDrawable = this.requestOptions.getErrorPlaceholder()) == null && this.requestOptions.getErrorId() > 0) {
            this.errorDrawable = this.loadDrawable(this.requestOptions.getErrorId());
        }
        return this.errorDrawable;
    }
    
    private Drawable getFallbackDrawable() {
        if (this.fallbackDrawable == null && (this.fallbackDrawable = this.requestOptions.getFallbackDrawable()) == null && this.requestOptions.getFallbackId() > 0) {
            this.fallbackDrawable = this.loadDrawable(this.requestOptions.getFallbackId());
        }
        return this.fallbackDrawable;
    }
    
    private Drawable getPlaceholderDrawable() {
        if (this.placeholderDrawable == null && (this.placeholderDrawable = this.requestOptions.getPlaceholderDrawable()) == null && this.requestOptions.getPlaceholderId() > 0) {
            this.placeholderDrawable = this.loadDrawable(this.requestOptions.getPlaceholderId());
        }
        return this.placeholderDrawable;
    }
    
    private boolean isFirstReadyResource() {
        final RequestCoordinator requestCoordinator = this.requestCoordinator;
        return requestCoordinator == null || !requestCoordinator.getRoot().isAnyResourceSet();
    }
    
    private Drawable loadDrawable(final int n) {
        Resources$Theme resources$Theme;
        if (this.requestOptions.getTheme() != null) {
            resources$Theme = this.requestOptions.getTheme();
        }
        else {
            resources$Theme = this.context.getTheme();
        }
        return DrawableDecoderCompat.getDrawable(this.context, n, resources$Theme);
    }
    
    private void logV(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" this: ");
        sb.append(this.tag);
        Log.v("GlideRequest", sb.toString());
    }
    
    private static int maybeApplySizeMultiplier(int round, final float n) {
        if (round != Integer.MIN_VALUE) {
            round = Math.round(n * round);
        }
        return round;
    }
    
    private void notifyRequestCoordinatorLoadFailed() {
        final RequestCoordinator requestCoordinator = this.requestCoordinator;
        if (requestCoordinator != null) {
            requestCoordinator.onRequestFailed(this);
        }
    }
    
    private void notifyRequestCoordinatorLoadSucceeded() {
        final RequestCoordinator requestCoordinator = this.requestCoordinator;
        if (requestCoordinator != null) {
            requestCoordinator.onRequestSuccess(this);
        }
    }
    
    public static <R> SingleRequest<R> obtain(final Context context, final GlideContext glideContext, final Object o, final Object o2, final Class<R> clazz, final BaseRequestOptions<?> baseRequestOptions, final int n, final int n2, final Priority priority, final Target<R> target, final RequestListener<R> requestListener, final List<RequestListener<R>> list, final RequestCoordinator requestCoordinator, final Engine engine, final TransitionFactory<? super R> transitionFactory, final Executor executor) {
        return new SingleRequest<R>(context, glideContext, o, o2, clazz, baseRequestOptions, n, n2, priority, target, requestListener, list, requestCoordinator, engine, transitionFactory, executor);
    }
    
    private void onLoadFailed(final GlideException ex, int n) {
        this.stateVerifier.throwIfRecycled();
        synchronized (this.requestLock) {
            ex.setOrigin(this.requestOrigin);
            final int logLevel = this.glideContext.getLogLevel();
            if (logLevel <= n) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Load failed for [");
                sb.append(this.model);
                sb.append("] with dimensions [");
                sb.append(this.width);
                sb.append("x");
                sb.append(this.height);
                sb.append("]");
                Log.w("Glide", sb.toString(), (Throwable)ex);
                if (logLevel <= 4) {
                    ex.logRootCauses("Glide");
                }
            }
            this.loadStatus = null;
            this.status = Status.FAILED;
            this.notifyRequestCoordinatorLoadFailed();
            final int n2 = 1;
            this.isCallingCallbacks = true;
            try {
                final List<RequestListener<R>> requestListeners = this.requestListeners;
                int n3;
                if (requestListeners != null) {
                    final Iterator<RequestListener<R>> iterator = requestListeners.iterator();
                    n = 0;
                    while (true) {
                        n3 = n;
                        if (!iterator.hasNext()) {
                            break;
                        }
                        n |= (iterator.next().onLoadFailed(ex, this.model, this.target, this.isFirstReadyResource()) ? 1 : 0);
                    }
                }
                else {
                    n3 = 0;
                }
                final RequestListener<R> targetListener = this.targetListener;
                if (targetListener != null && targetListener.onLoadFailed(ex, this.model, this.target, this.isFirstReadyResource())) {
                    n = n2;
                }
                else {
                    n = 0;
                }
                if ((n3 | n) == 0x0) {
                    this.setErrorPlaceholder();
                }
                this.isCallingCallbacks = false;
                GlideTrace.endSectionAsync("GlideRequest", this.cookie);
            }
            finally {
                this.isCallingCallbacks = false;
            }
        }
    }
    
    private void onResourceReady(final Resource<R> resource, final R r, final DataSource obj, final boolean b) {
        final boolean firstReadyResource = this.isFirstReadyResource();
        this.status = Status.COMPLETE;
        this.resource = resource;
        if (this.glideContext.getLogLevel() <= 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Finished loading ");
            sb.append(r.getClass().getSimpleName());
            sb.append(" from ");
            sb.append(obj);
            sb.append(" for ");
            sb.append(this.model);
            sb.append(" with size [");
            sb.append(this.width);
            sb.append("x");
            sb.append(this.height);
            sb.append("] in ");
            sb.append(LogTime.getElapsedMillis(this.startTime));
            sb.append(" ms");
            Log.d("Glide", sb.toString());
        }
        this.notifyRequestCoordinatorLoadSucceeded();
        final int n = 1;
        this.isCallingCallbacks = true;
        try {
            final List<RequestListener<R>> requestListeners = this.requestListeners;
            int n3;
            if (requestListeners != null) {
                final Iterator<RequestListener<R>> iterator = requestListeners.iterator();
                int n2 = 0;
                while (true) {
                    n3 = n2;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    n2 |= (iterator.next().onResourceReady(r, this.model, this.target, obj, firstReadyResource) ? 1 : 0);
                }
            }
            else {
                n3 = 0;
            }
            final RequestListener<R> targetListener = this.targetListener;
            int n4;
            if (targetListener != null && targetListener.onResourceReady(r, this.model, this.target, obj, firstReadyResource)) {
                n4 = n;
            }
            else {
                n4 = 0;
            }
            if ((n4 | n3) == 0x0) {
                this.target.onResourceReady(r, this.animationFactory.build(obj, firstReadyResource));
            }
            this.isCallingCallbacks = false;
            GlideTrace.endSectionAsync("GlideRequest", this.cookie);
        }
        finally {
            this.isCallingCallbacks = false;
        }
    }
    
    private void setErrorPlaceholder() {
        if (!this.canNotifyStatusChanged()) {
            return;
        }
        Drawable fallbackDrawable = null;
        if (this.model == null) {
            fallbackDrawable = this.getFallbackDrawable();
        }
        Drawable errorDrawable;
        if ((errorDrawable = fallbackDrawable) == null) {
            errorDrawable = this.getErrorDrawable();
        }
        Drawable placeholderDrawable;
        if ((placeholderDrawable = errorDrawable) == null) {
            placeholderDrawable = this.getPlaceholderDrawable();
        }
        this.target.onLoadFailed(placeholderDrawable);
    }
    
    @Override
    public void begin() {
        synchronized (this.requestLock) {
            this.assertNotCallingCallbacks();
            this.stateVerifier.throwIfRecycled();
            this.startTime = LogTime.getLogTime();
            if (this.model == null) {
                if (Util.isValidDimensions(this.overrideWidth, this.overrideHeight)) {
                    this.width = this.overrideWidth;
                    this.height = this.overrideHeight;
                }
                int n;
                if (this.getFallbackDrawable() == null) {
                    n = 5;
                }
                else {
                    n = 3;
                }
                this.onLoadFailed(new GlideException("Received null model"), n);
                return;
            }
            if (this.status == Status.RUNNING) {
                throw new IllegalArgumentException("Cannot restart a running request");
            }
            if (this.status == Status.COMPLETE) {
                this.onResourceReady(this.resource, DataSource.MEMORY_CACHE, false);
                return;
            }
            this.experimentalNotifyRequestStarted(this.model);
            this.cookie = GlideTrace.beginSectionAsync("GlideRequest");
            this.status = Status.WAITING_FOR_SIZE;
            if (Util.isValidDimensions(this.overrideWidth, this.overrideHeight)) {
                this.onSizeReady(this.overrideWidth, this.overrideHeight);
            }
            else {
                this.target.getSize(this);
            }
            if ((this.status == Status.RUNNING || this.status == Status.WAITING_FOR_SIZE) && this.canNotifyStatusChanged()) {
                this.target.onLoadStarted(this.getPlaceholderDrawable());
            }
            if (SingleRequest.IS_VERBOSE_LOGGABLE) {
                final StringBuilder sb = new StringBuilder();
                sb.append("finished run method in ");
                sb.append(LogTime.getElapsedMillis(this.startTime));
                this.logV(sb.toString());
            }
        }
    }
    
    @Override
    public void clear() {
        synchronized (this.requestLock) {
            this.assertNotCallingCallbacks();
            this.stateVerifier.throwIfRecycled();
            if (this.status == Status.CLEARED) {
                return;
            }
            this.cancel();
            Resource<R> resource = this.resource;
            if (resource != null) {
                this.resource = null;
            }
            else {
                resource = null;
            }
            if (this.canNotifyCleared()) {
                this.target.onLoadCleared(this.getPlaceholderDrawable());
            }
            GlideTrace.endSectionAsync("GlideRequest", this.cookie);
            this.status = Status.CLEARED;
            monitorexit(this.requestLock);
            if (resource != null) {
                this.engine.release(resource);
            }
        }
    }
    
    @Override
    public Object getLock() {
        this.stateVerifier.throwIfRecycled();
        return this.requestLock;
    }
    
    @Override
    public boolean isAnyResourceSet() {
        synchronized (this.requestLock) {
            return this.status == Status.COMPLETE;
        }
    }
    
    @Override
    public boolean isCleared() {
        synchronized (this.requestLock) {
            return this.status == Status.CLEARED;
        }
    }
    
    @Override
    public boolean isComplete() {
        synchronized (this.requestLock) {
            return this.status == Status.COMPLETE;
        }
    }
    
    @Override
    public boolean isEquivalentTo(final Request request) {
        if (!(request instanceof SingleRequest)) {
            return false;
        }
        Object obj = this.requestLock;
        synchronized (obj) {
            final int overrideWidth = this.overrideWidth;
            final int overrideHeight = this.overrideHeight;
            final Object model = this.model;
            final Class<R> transcodeClass = this.transcodeClass;
            final BaseRequestOptions<?> requestOptions = this.requestOptions;
            final Priority priority = this.priority;
            final List<RequestListener<R>> requestListeners = this.requestListeners;
            int size;
            if (requestListeners != null) {
                size = requestListeners.size();
            }
            else {
                size = 0;
            }
            monitorexit(obj);
            final SingleRequest singleRequest = (SingleRequest)request;
            synchronized (singleRequest.requestLock) {
                final int overrideWidth2 = singleRequest.overrideWidth;
                final int overrideHeight2 = singleRequest.overrideHeight;
                final Object model2 = singleRequest.model;
                obj = singleRequest.transcodeClass;
                final BaseRequestOptions<?> requestOptions2 = singleRequest.requestOptions;
                final Priority priority2 = singleRequest.priority;
                final List<RequestListener<R>> requestListeners2 = singleRequest.requestListeners;
                int size2;
                if (requestListeners2 != null) {
                    size2 = requestListeners2.size();
                }
                else {
                    size2 = 0;
                }
                monitorexit(singleRequest.requestLock);
                return overrideWidth == overrideWidth2 && overrideHeight == overrideHeight2 && Util.bothModelsNullEquivalentOrEquals(model, model2) && transcodeClass.equals(obj) && requestOptions.equals(requestOptions2) && priority == priority2 && size == size2;
            }
        }
    }
    
    @Override
    public boolean isRunning() {
        synchronized (this.requestLock) {
            return this.status == Status.RUNNING || this.status == Status.WAITING_FOR_SIZE;
        }
    }
    
    @Override
    public void onLoadFailed(final GlideException ex) {
        this.onLoadFailed(ex, 5);
    }
    
    @Override
    public void onResourceReady(final Resource<?> obj, final DataSource dataSource, final boolean b) {
        this.stateVerifier.throwIfRecycled();
        final Resource<?> resource = null;
        final Resource<?> resource2 = null;
        Object o = resource;
        try {
            final Object requestLock = this.requestLock;
            o = resource;
            monitorenter(requestLock);
            o = resource2;
            StringBuilder sb2 = null;
            try {
                this.loadStatus = null;
                if (obj == null) {
                    o = resource2;
                    o = resource2;
                    o = resource2;
                    final StringBuilder sb = new StringBuilder();
                    o = resource2;
                    sb.append("Expected to receive a Resource<R> with an object of ");
                    o = resource2;
                    sb.append(this.transcodeClass);
                    o = resource2;
                    sb.append(" inside, but instead got null.");
                    o = resource2;
                    final GlideException ex = new GlideException(sb.toString());
                    o = resource2;
                    this.onLoadFailed(ex);
                    o = resource2;
                    monitorexit(requestLock);
                    return;
                }
                o = resource2;
                final R value = obj.get();
                Label_0245: {
                    if (value == null) {
                        break Label_0245;
                    }
                    o = resource2;
                    if (!this.transcodeClass.isAssignableFrom(value.getClass())) {
                        break Label_0245;
                    }
                    o = resource2;
                    Label_0224: {
                        if (this.canSetResource()) {
                            break Label_0224;
                        }
                        try {
                            this.resource = null;
                            this.status = Status.COMPLETE;
                            GlideTrace.endSectionAsync("GlideRequest", this.cookie);
                            monitorexit(requestLock);
                            return;
                            String str = null;
                        Label_0376:
                            while (true) {
                                Serializable class1 = null;
                                Label_0311: {
                                    while (true) {
                                        class1 = value.getClass();
                                        break Label_0311;
                                        this.resource = null;
                                        o = new StringBuilder();
                                        ((StringBuilder)o).append("Expected to receive an object of ");
                                        ((StringBuilder)o).append(this.transcodeClass);
                                        ((StringBuilder)o).append(" but instead got ");
                                        iftrue(Label_0307:)(value == null);
                                        continue;
                                    }
                                    str = "";
                                    break Label_0376;
                                    o = resource2;
                                    this.onResourceReady((Resource<R>)obj, value, dataSource, b);
                                    o = resource2;
                                    monitorexit(requestLock);
                                    return;
                                    Label_0372: {
                                        str = " To indicate failure return a null Resource object, rather than a Resource object containing null data.";
                                    }
                                    break Label_0376;
                                    Label_0307:
                                    class1 = "";
                                }
                                ((StringBuilder)o).append(class1);
                                ((StringBuilder)o).append("{");
                                ((StringBuilder)o).append(value);
                                ((StringBuilder)o).append("} inside Resource{");
                                ((StringBuilder)o).append(obj);
                                ((StringBuilder)o).append("}.");
                                iftrue(Label_0372:)(value == null);
                                continue;
                            }
                            ((StringBuilder)o).append(str);
                            this.onLoadFailed(new GlideException(((StringBuilder)o).toString()));
                            monitorexit(requestLock);
                            return;
                        }
                        finally {}
                    }
                }
            }
            finally {
                sb2 = (StringBuilder)o;
            }
            o = sb2;
            monitorexit(requestLock);
            o = sb2;
        }
        finally {
            if (o != null) {
                this.engine.release((Resource<?>)o);
            }
        }
    }
    
    @Override
    public void onSizeReady(final int p0, final int p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/bumptech/glide/request/SingleRequest.stateVerifier:Lcom/bumptech/glide/util/pool/StateVerifier;
        //     4: invokevirtual   com/bumptech/glide/util/pool/StateVerifier.throwIfRecycled:()V
        //     7: aload_0        
        //     8: getfield        com/bumptech/glide/request/SingleRequest.requestLock:Ljava/lang/Object;
        //    11: astore          11
        //    13: aload           11
        //    15: monitorenter   
        //    16: getstatic       com/bumptech/glide/request/SingleRequest.IS_VERBOSE_LOGGABLE:Z
        //    19: istore          5
        //    21: iload           5
        //    23: ifeq            67
        //    26: new             Ljava/lang/StringBuilder;
        //    29: astore          12
        //    31: aload           12
        //    33: invokespecial   java/lang/StringBuilder.<init>:()V
        //    36: aload           12
        //    38: ldc_w           "Got onSizeReady in "
        //    41: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    44: pop            
        //    45: aload           12
        //    47: aload_0        
        //    48: getfield        com/bumptech/glide/request/SingleRequest.startTime:J
        //    51: invokestatic    com/bumptech/glide/util/LogTime.getElapsedMillis:(J)D
        //    54: invokevirtual   java/lang/StringBuilder.append:(D)Ljava/lang/StringBuilder;
        //    57: pop            
        //    58: aload_0        
        //    59: aload           12
        //    61: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    64: invokespecial   com/bumptech/glide/request/SingleRequest.logV:(Ljava/lang/String;)V
        //    67: aload_0        
        //    68: getfield        com/bumptech/glide/request/SingleRequest.status:Lcom/bumptech/glide/request/SingleRequest$Status;
        //    71: getstatic       com/bumptech/glide/request/SingleRequest$Status.WAITING_FOR_SIZE:Lcom/bumptech/glide/request/SingleRequest$Status;
        //    74: if_acmpeq       81
        //    77: aload           11
        //    79: monitorexit    
        //    80: return         
        //    81: aload_0        
        //    82: getstatic       com/bumptech/glide/request/SingleRequest$Status.RUNNING:Lcom/bumptech/glide/request/SingleRequest$Status;
        //    85: putfield        com/bumptech/glide/request/SingleRequest.status:Lcom/bumptech/glide/request/SingleRequest$Status;
        //    88: aload_0        
        //    89: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //    92: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getSizeMultiplier:()F
        //    95: fstore_3       
        //    96: aload_0        
        //    97: iload_1        
        //    98: fload_3        
        //    99: invokestatic    com/bumptech/glide/request/SingleRequest.maybeApplySizeMultiplier:(IF)I
        //   102: putfield        com/bumptech/glide/request/SingleRequest.width:I
        //   105: aload_0        
        //   106: iload_2        
        //   107: fload_3        
        //   108: invokestatic    com/bumptech/glide/request/SingleRequest.maybeApplySizeMultiplier:(IF)I
        //   111: putfield        com/bumptech/glide/request/SingleRequest.height:I
        //   114: iload           5
        //   116: ifeq            160
        //   119: new             Ljava/lang/StringBuilder;
        //   122: astore          12
        //   124: aload           12
        //   126: invokespecial   java/lang/StringBuilder.<init>:()V
        //   129: aload           12
        //   131: ldc_w           "finished setup for calling load in "
        //   134: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   137: pop            
        //   138: aload           12
        //   140: aload_0        
        //   141: getfield        com/bumptech/glide/request/SingleRequest.startTime:J
        //   144: invokestatic    com/bumptech/glide/util/LogTime.getElapsedMillis:(J)D
        //   147: invokevirtual   java/lang/StringBuilder.append:(D)Ljava/lang/StringBuilder;
        //   150: pop            
        //   151: aload_0        
        //   152: aload           12
        //   154: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   157: invokespecial   com/bumptech/glide/request/SingleRequest.logV:(Ljava/lang/String;)V
        //   160: aload_0        
        //   161: getfield        com/bumptech/glide/request/SingleRequest.engine:Lcom/bumptech/glide/load/engine/Engine;
        //   164: astore          12
        //   166: aload_0        
        //   167: getfield        com/bumptech/glide/request/SingleRequest.glideContext:Lcom/bumptech/glide/GlideContext;
        //   170: astore          17
        //   172: aload_0        
        //   173: getfield        com/bumptech/glide/request/SingleRequest.model:Ljava/lang/Object;
        //   176: astore          19
        //   178: aload_0        
        //   179: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   182: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getSignature:()Lcom/bumptech/glide/load/Key;
        //   185: astore          22
        //   187: aload_0        
        //   188: getfield        com/bumptech/glide/request/SingleRequest.width:I
        //   191: istore_2       
        //   192: aload_0        
        //   193: getfield        com/bumptech/glide/request/SingleRequest.height:I
        //   196: istore_1       
        //   197: aload_0        
        //   198: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   201: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getResourceClass:()Ljava/lang/Class;
        //   204: astore          13
        //   206: aload_0        
        //   207: getfield        com/bumptech/glide/request/SingleRequest.transcodeClass:Ljava/lang/Class;
        //   210: astore          15
        //   212: aload_0        
        //   213: getfield        com/bumptech/glide/request/SingleRequest.priority:Lcom/bumptech/glide/Priority;
        //   216: astore          20
        //   218: aload_0        
        //   219: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   222: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getDiskCacheStrategy:()Lcom/bumptech/glide/load/engine/DiskCacheStrategy;
        //   225: astore          21
        //   227: aload_0        
        //   228: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   231: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getTransformations:()Ljava/util/Map;
        //   234: astore          16
        //   236: aload_0        
        //   237: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   240: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.isTransformationRequired:()Z
        //   243: istore          7
        //   245: aload_0        
        //   246: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   249: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.isScaleOnlyOrNoTransform:()Z
        //   252: istore          8
        //   254: aload_0        
        //   255: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   258: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getOptions:()Lcom/bumptech/glide/load/Options;
        //   261: astore          14
        //   263: aload_0        
        //   264: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   267: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.isMemoryCacheable:()Z
        //   270: istore          4
        //   272: aload_0        
        //   273: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   276: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getUseUnlimitedSourceGeneratorsPool:()Z
        //   279: istore          10
        //   281: aload_0        
        //   282: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   285: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getUseAnimationPool:()Z
        //   288: istore          9
        //   290: aload_0        
        //   291: getfield        com/bumptech/glide/request/SingleRequest.requestOptions:Lcom/bumptech/glide/request/BaseRequestOptions;
        //   294: invokevirtual   com/bumptech/glide/request/BaseRequestOptions.getOnlyRetrieveFromCache:()Z
        //   297: istore          6
        //   299: aload_0        
        //   300: getfield        com/bumptech/glide/request/SingleRequest.callbackExecutor:Ljava/util/concurrent/Executor;
        //   303: astore          18
        //   305: aload           12
        //   307: aload           17
        //   309: aload           19
        //   311: aload           22
        //   313: iload_2        
        //   314: iload_1        
        //   315: aload           13
        //   317: aload           15
        //   319: aload           20
        //   321: aload           21
        //   323: aload           16
        //   325: iload           7
        //   327: iload           8
        //   329: aload           14
        //   331: iload           4
        //   333: iload           10
        //   335: iload           9
        //   337: iload           6
        //   339: aload_0        
        //   340: aload           18
        //   342: invokevirtual   com/bumptech/glide/load/engine/Engine.load:(Lcom/bumptech/glide/GlideContext;Ljava/lang/Object;Lcom/bumptech/glide/load/Key;IILjava/lang/Class;Ljava/lang/Class;Lcom/bumptech/glide/Priority;Lcom/bumptech/glide/load/engine/DiskCacheStrategy;Ljava/util/Map;ZZLcom/bumptech/glide/load/Options;ZZZZLcom/bumptech/glide/request/ResourceCallback;Ljava/util/concurrent/Executor;)Lcom/bumptech/glide/load/engine/Engine$LoadStatus;
        //   345: astore          12
        //   347: aload           11
        //   349: astore          13
        //   351: aload_0        
        //   352: aload           12
        //   354: putfield        com/bumptech/glide/request/SingleRequest.loadStatus:Lcom/bumptech/glide/load/engine/Engine$LoadStatus;
        //   357: aload           11
        //   359: astore          13
        //   361: aload_0        
        //   362: getfield        com/bumptech/glide/request/SingleRequest.status:Lcom/bumptech/glide/request/SingleRequest$Status;
        //   365: getstatic       com/bumptech/glide/request/SingleRequest$Status.RUNNING:Lcom/bumptech/glide/request/SingleRequest$Status;
        //   368: if_acmpeq       380
        //   371: aload           11
        //   373: astore          13
        //   375: aload_0        
        //   376: aconst_null    
        //   377: putfield        com/bumptech/glide/request/SingleRequest.loadStatus:Lcom/bumptech/glide/load/engine/Engine$LoadStatus;
        //   380: iload           5
        //   382: ifeq            446
        //   385: aload           11
        //   387: astore          13
        //   389: new             Ljava/lang/StringBuilder;
        //   392: astore          12
        //   394: aload           11
        //   396: astore          13
        //   398: aload           12
        //   400: invokespecial   java/lang/StringBuilder.<init>:()V
        //   403: aload           11
        //   405: astore          13
        //   407: aload           12
        //   409: ldc_w           "finished onSizeReady in "
        //   412: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   415: pop            
        //   416: aload           11
        //   418: astore          13
        //   420: aload           12
        //   422: aload_0        
        //   423: getfield        com/bumptech/glide/request/SingleRequest.startTime:J
        //   426: invokestatic    com/bumptech/glide/util/LogTime.getElapsedMillis:(J)D
        //   429: invokevirtual   java/lang/StringBuilder.append:(D)Ljava/lang/StringBuilder;
        //   432: pop            
        //   433: aload           11
        //   435: astore          13
        //   437: aload_0        
        //   438: aload           12
        //   440: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   443: invokespecial   com/bumptech/glide/request/SingleRequest.logV:(Ljava/lang/String;)V
        //   446: aload           11
        //   448: astore          13
        //   450: aload           11
        //   452: monitorexit    
        //   453: return         
        //   454: astore          12
        //   456: goto            461
        //   459: astore          12
        //   461: aload           11
        //   463: astore          13
        //   465: aload           11
        //   467: monitorexit    
        //   468: aload           12
        //   470: athrow         
        //   471: astore          12
        //   473: aload           13
        //   475: astore          11
        //   477: goto            461
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  16     21     459    461    Any
        //  26     67     459    461    Any
        //  67     80     459    461    Any
        //  81     114    459    461    Any
        //  119    160    459    461    Any
        //  160    305    459    461    Any
        //  305    347    454    459    Any
        //  351    357    471    480    Any
        //  361    371    471    480    Any
        //  375    380    471    480    Any
        //  389    394    471    480    Any
        //  398    403    471    480    Any
        //  407    416    471    480    Any
        //  420    433    471    480    Any
        //  437    446    471    480    Any
        //  450    453    471    480    Any
        //  465    468    471    480    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0380:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void pause() {
        synchronized (this.requestLock) {
            if (this.isRunning()) {
                this.clear();
            }
        }
    }
    
    @Override
    public String toString() {
        Object requestLock = this.requestLock;
        synchronized (requestLock) {
            final Object model = this.model;
            final Class<R> transcodeClass = this.transcodeClass;
            monitorexit(requestLock);
            requestLock = new StringBuilder();
            ((StringBuilder)requestLock).append(super.toString());
            ((StringBuilder)requestLock).append("[model=");
            ((StringBuilder)requestLock).append(model);
            ((StringBuilder)requestLock).append(", transcodeClass=");
            ((StringBuilder)requestLock).append(transcodeClass);
            ((StringBuilder)requestLock).append("]");
            return ((StringBuilder)requestLock).toString();
        }
    }
    
    private enum Status
    {
        private static final Status[] $VALUES;
        
        CLEARED, 
        COMPLETE, 
        FAILED, 
        PENDING, 
        RUNNING, 
        WAITING_FOR_SIZE;
    }
}

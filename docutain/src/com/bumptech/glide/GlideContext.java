// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import java.util.Iterator;
import com.bumptech.glide.request.target.ViewTarget;
import android.widget.ImageView;
import android.content.Context;
import com.bumptech.glide.util.GlideSuppliers;
import com.bumptech.glide.request.target.ImageViewTargetFactory;
import com.bumptech.glide.load.engine.Engine;
import java.util.Map;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.RequestListener;
import java.util.List;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import android.content.ContextWrapper;

public class GlideContext extends ContextWrapper
{
    static final TransitionOptions<?, ?> DEFAULT_TRANSITION_OPTIONS;
    private final ArrayPool arrayPool;
    private final List<RequestListener<Object>> defaultRequestListeners;
    private RequestOptions defaultRequestOptions;
    private final Glide.RequestOptionsFactory defaultRequestOptionsFactory;
    private final Map<Class<?>, TransitionOptions<?, ?>> defaultTransitionOptions;
    private final Engine engine;
    private final GlideExperiments experiments;
    private final ImageViewTargetFactory imageViewTargetFactory;
    private final int logLevel;
    private final GlideSuppliers.GlideSupplier<Registry> registry;
    
    static {
        DEFAULT_TRANSITION_OPTIONS = new GenericTransitionOptions<Object>();
    }
    
    public GlideContext(final Context context, final ArrayPool arrayPool, final GlideSuppliers.GlideSupplier<Registry> glideSupplier, final ImageViewTargetFactory imageViewTargetFactory, final Glide.RequestOptionsFactory defaultRequestOptionsFactory, final Map<Class<?>, TransitionOptions<?, ?>> defaultTransitionOptions, final List<RequestListener<Object>> defaultRequestListeners, final Engine engine, final GlideExperiments experiments, final int logLevel) {
        super(context.getApplicationContext());
        this.arrayPool = arrayPool;
        this.imageViewTargetFactory = imageViewTargetFactory;
        this.defaultRequestOptionsFactory = defaultRequestOptionsFactory;
        this.defaultRequestListeners = defaultRequestListeners;
        this.defaultTransitionOptions = defaultTransitionOptions;
        this.engine = engine;
        this.experiments = experiments;
        this.logLevel = logLevel;
        this.registry = GlideSuppliers.memorize(glideSupplier);
    }
    
    public <X> ViewTarget<ImageView, X> buildImageViewTarget(final ImageView imageView, final Class<X> clazz) {
        return this.imageViewTargetFactory.buildTarget(imageView, clazz);
    }
    
    public ArrayPool getArrayPool() {
        return this.arrayPool;
    }
    
    public List<RequestListener<Object>> getDefaultRequestListeners() {
        return this.defaultRequestListeners;
    }
    
    public RequestOptions getDefaultRequestOptions() {
        synchronized (this) {
            if (this.defaultRequestOptions == null) {
                this.defaultRequestOptions = this.defaultRequestOptionsFactory.build().lock();
            }
            return this.defaultRequestOptions;
        }
    }
    
    public <T> TransitionOptions<?, T> getDefaultTransitionOptions(final Class<T> clazz) {
        TransitionOptions transitionOptions2;
        TransitionOptions transitionOptions = transitionOptions2 = this.defaultTransitionOptions.get(clazz);
        if (transitionOptions == null) {
            final Iterator<Map.Entry<Class<?>, TransitionOptions<?, ?>>> iterator = this.defaultTransitionOptions.entrySet().iterator();
            while (true) {
                transitionOptions2 = transitionOptions;
                if (!iterator.hasNext()) {
                    break;
                }
                final Map.Entry<Class, V> entry = iterator.next();
                if (!entry.getKey().isAssignableFrom(clazz)) {
                    continue;
                }
                transitionOptions = (TransitionOptions)entry.getValue();
            }
        }
        TransitionOptions<?, ?> default_TRANSITION_OPTIONS;
        if ((default_TRANSITION_OPTIONS = transitionOptions2) == null) {
            default_TRANSITION_OPTIONS = GlideContext.DEFAULT_TRANSITION_OPTIONS;
        }
        return (TransitionOptions<?, T>)default_TRANSITION_OPTIONS;
    }
    
    public Engine getEngine() {
        return this.engine;
    }
    
    public GlideExperiments getExperiments() {
        return this.experiments;
    }
    
    public int getLogLevel() {
        return this.logLevel;
    }
    
    public Registry getRegistry() {
        return this.registry.get();
    }
}

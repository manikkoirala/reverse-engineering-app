// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.provider;

import com.bumptech.glide.load.ResourceEncoder;
import java.util.ArrayList;
import java.util.List;

public class ResourceEncoderRegistry
{
    private final List<Entry<?>> encoders;
    
    public ResourceEncoderRegistry() {
        this.encoders = new ArrayList<Entry<?>>();
    }
    
    public <Z> void append(final Class<Z> clazz, final ResourceEncoder<Z> resourceEncoder) {
        synchronized (this) {
            this.encoders.add(new Entry<Object>((Class<Object>)clazz, (ResourceEncoder<Object>)resourceEncoder));
        }
    }
    
    public <Z> ResourceEncoder<Z> get(final Class<Z> clazz) {
        monitorenter(this);
        int i = 0;
        try {
            while (i < this.encoders.size()) {
                final Entry entry = this.encoders.get(i);
                if (entry.handles(clazz)) {
                    return (ResourceEncoder<Z>)entry.encoder;
                }
                ++i;
            }
            return null;
        }
        finally {
            monitorexit(this);
        }
    }
    
    public <Z> void prepend(final Class<Z> clazz, final ResourceEncoder<Z> resourceEncoder) {
        synchronized (this) {
            this.encoders.add(0, new Entry<Object>((Class<Object>)clazz, (ResourceEncoder<Object>)resourceEncoder));
        }
    }
    
    private static final class Entry<T>
    {
        final ResourceEncoder<T> encoder;
        private final Class<T> resourceClass;
        
        Entry(final Class<T> resourceClass, final ResourceEncoder<T> encoder) {
            this.resourceClass = resourceClass;
            this.encoder = encoder;
        }
        
        boolean handles(final Class<?> clazz) {
            return this.resourceClass.isAssignableFrom(clazz);
        }
    }
}

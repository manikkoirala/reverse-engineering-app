// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.provider;

import java.util.List;
import androidx.core.util.Pools;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.load.engine.DecodePath;
import com.bumptech.glide.load.resource.transcode.UnitTranscoder;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;
import com.bumptech.glide.util.MultiClassKey;
import androidx.collection.ArrayMap;
import com.bumptech.glide.load.engine.LoadPath;

public class LoadPathCache
{
    private static final LoadPath<?, ?, ?> NO_PATHS_SIGNAL;
    private final ArrayMap<MultiClassKey, LoadPath<?, ?, ?>> cache;
    private final AtomicReference<MultiClassKey> keyRef;
    
    static {
        NO_PATHS_SIGNAL = new LoadPath<Object, Object, Object>(Object.class, Object.class, Object.class, Collections.singletonList((DecodePath<?, ?, ?>)new DecodePath<Object, Object, Object>(Object.class, Object.class, Object.class, Collections.emptyList(), new UnitTranscoder<Object>(), null)), null);
    }
    
    public LoadPathCache() {
        this.cache = new ArrayMap<MultiClassKey, LoadPath<?, ?, ?>>();
        this.keyRef = new AtomicReference<MultiClassKey>();
    }
    
    private MultiClassKey getKey(final Class<?> clazz, final Class<?> clazz2, final Class<?> clazz3) {
        MultiClassKey multiClassKey;
        if ((multiClassKey = this.keyRef.getAndSet(null)) == null) {
            multiClassKey = new MultiClassKey();
        }
        multiClassKey.set(clazz, clazz2, clazz3);
        return multiClassKey;
    }
    
    public <Data, TResource, Transcode> LoadPath<Data, TResource, Transcode> get(final Class<Data> clazz, final Class<TResource> clazz2, final Class<Transcode> clazz3) {
        final MultiClassKey key = this.getKey(clazz, clazz2, clazz3);
        synchronized (this.cache) {
            final LoadPath loadPath = this.cache.get(key);
            monitorexit(this.cache);
            this.keyRef.set(key);
            return loadPath;
        }
    }
    
    public boolean isEmptyLoadPath(final LoadPath<?, ?, ?> obj) {
        return LoadPathCache.NO_PATHS_SIGNAL.equals(obj);
    }
    
    public void put(final Class<?> clazz, final Class<?> clazz2, final Class<?> clazz3, LoadPath<?, ?, ?> no_PATHS_SIGNAL) {
        synchronized (this.cache) {
            final ArrayMap<MultiClassKey, LoadPath<?, ?, ?>> cache = this.cache;
            final MultiClassKey multiClassKey = new MultiClassKey(clazz, clazz2, clazz3);
            if (no_PATHS_SIGNAL == null) {
                no_PATHS_SIGNAL = LoadPathCache.NO_PATHS_SIGNAL;
            }
            cache.put(multiClassKey, no_PATHS_SIGNAL);
        }
    }
}

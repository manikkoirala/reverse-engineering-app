// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.provider;

import java.util.concurrent.atomic.AtomicReference;
import java.util.List;
import com.bumptech.glide.util.MultiClassKey;
import androidx.collection.ArrayMap;

public class ModelToResourceClassCache
{
    private final ArrayMap<MultiClassKey, List<Class<?>>> registeredResourceClassCache;
    private final AtomicReference<MultiClassKey> resourceClassKeyRef;
    
    public ModelToResourceClassCache() {
        this.resourceClassKeyRef = new AtomicReference<MultiClassKey>();
        this.registeredResourceClassCache = new ArrayMap<MultiClassKey, List<Class<?>>>();
    }
    
    public void clear() {
        synchronized (this.registeredResourceClassCache) {
            this.registeredResourceClassCache.clear();
        }
    }
    
    public List<Class<?>> get(final Class<?> clazz, final Class<?> clazz2, final Class<?> clazz3) {
        final MultiClassKey multiClassKey = this.resourceClassKeyRef.getAndSet(null);
        MultiClassKey newValue;
        if (multiClassKey == null) {
            newValue = new MultiClassKey(clazz, clazz2, clazz3);
        }
        else {
            multiClassKey.set(clazz, clazz2, clazz3);
            newValue = multiClassKey;
        }
        synchronized (this.registeredResourceClassCache) {
            final List list = this.registeredResourceClassCache.get(newValue);
            monitorexit(this.registeredResourceClassCache);
            this.resourceClassKeyRef.set(newValue);
            return list;
        }
    }
    
    public void put(final Class<?> clazz, final Class<?> clazz2, final Class<?> clazz3, final List<Class<?>> list) {
        synchronized (this.registeredResourceClassCache) {
            this.registeredResourceClassCache.put(new MultiClassKey(clazz, clazz2, clazz3), list);
        }
    }
}

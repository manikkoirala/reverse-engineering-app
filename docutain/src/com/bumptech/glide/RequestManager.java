// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.request.target.CustomViewTarget;
import java.util.Iterator;
import android.content.res.Configuration;
import java.net.URL;
import android.net.Uri;
import java.util.List;
import android.view.View;
import java.io.File;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.target.Target;
import java.util.Collection;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import android.graphics.Bitmap;
import com.bumptech.glide.manager.RequestManagerTreeNode;
import com.bumptech.glide.manager.TargetTracker;
import com.bumptech.glide.manager.RequestTracker;
import com.bumptech.glide.manager.Lifecycle;
import com.bumptech.glide.request.RequestListener;
import java.util.concurrent.CopyOnWriteArrayList;
import android.content.Context;
import com.bumptech.glide.manager.ConnectivityMonitor;
import com.bumptech.glide.request.RequestOptions;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.manager.LifecycleListener;
import android.content.ComponentCallbacks2;

public class RequestManager implements ComponentCallbacks2, LifecycleListener, ModelTypes<RequestBuilder<Drawable>>
{
    private static final RequestOptions DECODE_TYPE_BITMAP;
    private static final RequestOptions DECODE_TYPE_GIF;
    private static final RequestOptions DOWNLOAD_ONLY_OPTIONS;
    private final Runnable addSelfToLifecycle;
    private final ConnectivityMonitor connectivityMonitor;
    protected final Context context;
    private final CopyOnWriteArrayList<RequestListener<Object>> defaultRequestListeners;
    protected final Glide glide;
    final Lifecycle lifecycle;
    private boolean pauseAllRequestsOnTrimMemoryModerate;
    private RequestOptions requestOptions;
    private final RequestTracker requestTracker;
    private final TargetTracker targetTracker;
    private final RequestManagerTreeNode treeNode;
    
    static {
        DECODE_TYPE_BITMAP = RequestOptions.decodeTypeOf(Bitmap.class).lock();
        DECODE_TYPE_GIF = RequestOptions.decodeTypeOf(GifDrawable.class).lock();
        DOWNLOAD_ONLY_OPTIONS = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.DATA).priority(Priority.LOW).skipMemoryCache(true);
    }
    
    public RequestManager(final Glide glide, final Lifecycle lifecycle, final RequestManagerTreeNode requestManagerTreeNode, final Context context) {
        this(glide, lifecycle, requestManagerTreeNode, new RequestTracker(), glide.getConnectivityMonitorFactory(), context);
    }
    
    RequestManager(final Glide glide, final Lifecycle lifecycle, final RequestManagerTreeNode treeNode, final RequestTracker requestTracker, final ConnectivityMonitorFactory connectivityMonitorFactory, final Context context) {
        this.targetTracker = new TargetTracker();
        final Runnable addSelfToLifecycle = new Runnable() {
            final RequestManager this$0;
            
            @Override
            public void run() {
                this.this$0.lifecycle.addListener(this.this$0);
            }
        };
        this.addSelfToLifecycle = addSelfToLifecycle;
        this.glide = glide;
        this.lifecycle = lifecycle;
        this.treeNode = treeNode;
        this.requestTracker = requestTracker;
        this.context = context;
        final ConnectivityMonitor build = connectivityMonitorFactory.build(context.getApplicationContext(), new RequestManagerConnectivityListener(requestTracker));
        this.connectivityMonitor = build;
        glide.registerRequestManager(this);
        if (Util.isOnBackgroundThread()) {
            Util.postOnUiThread(addSelfToLifecycle);
        }
        else {
            lifecycle.addListener(this);
        }
        lifecycle.addListener(build);
        this.defaultRequestListeners = new CopyOnWriteArrayList<RequestListener<Object>>(glide.getGlideContext().getDefaultRequestListeners());
        this.setRequestOptions(glide.getGlideContext().getDefaultRequestOptions());
    }
    
    private void untrackOrDelegate(final Target<?> target) {
        final boolean untrack = this.untrack(target);
        final Request request = target.getRequest();
        if (!untrack && !this.glide.removeFromManagers(target) && request != null) {
            target.setRequest(null);
            request.clear();
        }
    }
    
    private void updateRequestOptions(final RequestOptions requestOptions) {
        synchronized (this) {
            this.requestOptions = this.requestOptions.apply(requestOptions);
        }
    }
    
    public RequestManager addDefaultRequestListener(final RequestListener<Object> e) {
        this.defaultRequestListeners.add(e);
        return this;
    }
    
    public RequestManager applyDefaultRequestOptions(final RequestOptions requestOptions) {
        synchronized (this) {
            this.updateRequestOptions(requestOptions);
            return this;
        }
    }
    
    public <ResourceType> RequestBuilder<ResourceType> as(final Class<ResourceType> clazz) {
        return new RequestBuilder<ResourceType>(this.glide, this, clazz, this.context);
    }
    
    public RequestBuilder<Bitmap> asBitmap() {
        return this.as(Bitmap.class).apply(RequestManager.DECODE_TYPE_BITMAP);
    }
    
    public RequestBuilder<Drawable> asDrawable() {
        return this.as(Drawable.class);
    }
    
    public RequestBuilder<File> asFile() {
        return this.as(File.class).apply(RequestOptions.skipMemoryCacheOf(true));
    }
    
    public RequestBuilder<GifDrawable> asGif() {
        return this.as(GifDrawable.class).apply(RequestManager.DECODE_TYPE_GIF);
    }
    
    public void clear(final View view) {
        this.clear(new ClearTarget(view));
    }
    
    public void clear(final Target<?> target) {
        if (target == null) {
            return;
        }
        this.untrackOrDelegate(target);
    }
    
    public RequestBuilder<File> download(final Object o) {
        return this.downloadOnly().load(o);
    }
    
    public RequestBuilder<File> downloadOnly() {
        return this.as(File.class).apply(RequestManager.DOWNLOAD_ONLY_OPTIONS);
    }
    
    List<RequestListener<Object>> getDefaultRequestListeners() {
        return this.defaultRequestListeners;
    }
    
    RequestOptions getDefaultRequestOptions() {
        synchronized (this) {
            return this.requestOptions;
        }
    }
    
     <T> TransitionOptions<?, T> getDefaultTransitionOptions(final Class<T> clazz) {
        return this.glide.getGlideContext().getDefaultTransitionOptions(clazz);
    }
    
    public boolean isPaused() {
        synchronized (this) {
            return this.requestTracker.isPaused();
        }
    }
    
    public RequestBuilder<Drawable> load(final Bitmap bitmap) {
        return this.asDrawable().load(bitmap);
    }
    
    public RequestBuilder<Drawable> load(final Drawable drawable) {
        return this.asDrawable().load(drawable);
    }
    
    public RequestBuilder<Drawable> load(final Uri uri) {
        return this.asDrawable().load(uri);
    }
    
    public RequestBuilder<Drawable> load(final File file) {
        return this.asDrawable().load(file);
    }
    
    public RequestBuilder<Drawable> load(final Integer n) {
        return this.asDrawable().load(n);
    }
    
    public RequestBuilder<Drawable> load(final Object o) {
        return this.asDrawable().load(o);
    }
    
    public RequestBuilder<Drawable> load(final String s) {
        return this.asDrawable().load(s);
    }
    
    @Deprecated
    public RequestBuilder<Drawable> load(final URL url) {
        return this.asDrawable().load(url);
    }
    
    public RequestBuilder<Drawable> load(final byte[] array) {
        return this.asDrawable().load(array);
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
    }
    
    public void onDestroy() {
        synchronized (this) {
            this.targetTracker.onDestroy();
            final Iterator<Target<?>> iterator = this.targetTracker.getAll().iterator();
            while (iterator.hasNext()) {
                this.clear(iterator.next());
            }
            this.targetTracker.clear();
            this.requestTracker.clearRequests();
            this.lifecycle.removeListener(this);
            this.lifecycle.removeListener(this.connectivityMonitor);
            Util.removeCallbacksOnUiThread(this.addSelfToLifecycle);
            this.glide.unregisterRequestManager(this);
        }
    }
    
    public void onLowMemory() {
    }
    
    public void onStart() {
        synchronized (this) {
            this.resumeRequests();
            this.targetTracker.onStart();
        }
    }
    
    public void onStop() {
        synchronized (this) {
            this.pauseRequests();
            this.targetTracker.onStop();
        }
    }
    
    public void onTrimMemory(final int n) {
        if (n == 60 && this.pauseAllRequestsOnTrimMemoryModerate) {
            this.pauseAllRequestsRecursive();
        }
    }
    
    public void pauseAllRequests() {
        synchronized (this) {
            this.requestTracker.pauseAllRequests();
        }
    }
    
    public void pauseAllRequestsRecursive() {
        synchronized (this) {
            this.pauseAllRequests();
            final Iterator<RequestManager> iterator = this.treeNode.getDescendants().iterator();
            while (iterator.hasNext()) {
                iterator.next().pauseAllRequests();
            }
        }
    }
    
    public void pauseRequests() {
        synchronized (this) {
            this.requestTracker.pauseRequests();
        }
    }
    
    public void pauseRequestsRecursive() {
        synchronized (this) {
            this.pauseRequests();
            final Iterator<RequestManager> iterator = this.treeNode.getDescendants().iterator();
            while (iterator.hasNext()) {
                iterator.next().pauseRequests();
            }
        }
    }
    
    public void resumeRequests() {
        synchronized (this) {
            this.requestTracker.resumeRequests();
        }
    }
    
    public void resumeRequestsRecursive() {
        synchronized (this) {
            Util.assertMainThread();
            this.resumeRequests();
            final Iterator<RequestManager> iterator = this.treeNode.getDescendants().iterator();
            while (iterator.hasNext()) {
                iterator.next().resumeRequests();
            }
        }
    }
    
    public RequestManager setDefaultRequestOptions(final RequestOptions requestOptions) {
        synchronized (this) {
            this.setRequestOptions(requestOptions);
            return this;
        }
    }
    
    public void setPauseAllRequestsOnTrimMemoryModerate(final boolean pauseAllRequestsOnTrimMemoryModerate) {
        this.pauseAllRequestsOnTrimMemoryModerate = pauseAllRequestsOnTrimMemoryModerate;
    }
    
    protected void setRequestOptions(final RequestOptions requestOptions) {
        synchronized (this) {
            this.requestOptions = requestOptions.clone().autoClone();
        }
    }
    
    @Override
    public String toString() {
        synchronized (this) {
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append("{tracker=");
            sb.append(this.requestTracker);
            sb.append(", treeNode=");
            sb.append(this.treeNode);
            sb.append("}");
            return sb.toString();
        }
    }
    
    void track(final Target<?> target, final Request request) {
        synchronized (this) {
            this.targetTracker.track(target);
            this.requestTracker.runRequest(request);
        }
    }
    
    boolean untrack(final Target<?> target) {
        synchronized (this) {
            final Request request = target.getRequest();
            if (request == null) {
                return true;
            }
            if (this.requestTracker.clearAndRemove(request)) {
                this.targetTracker.untrack(target);
                target.setRequest(null);
                return true;
            }
            return false;
        }
    }
    
    private static class ClearTarget extends CustomViewTarget<View, Object>
    {
        ClearTarget(final View view) {
            super(view);
        }
        
        @Override
        public void onLoadFailed(final Drawable drawable) {
        }
        
        @Override
        protected void onResourceCleared(final Drawable drawable) {
        }
        
        @Override
        public void onResourceReady(final Object o, final Transition<? super Object> transition) {
        }
    }
    
    private class RequestManagerConnectivityListener implements ConnectivityListener
    {
        private final RequestTracker requestTracker;
        final RequestManager this$0;
        
        RequestManagerConnectivityListener(final RequestManager this$0, final RequestTracker requestTracker) {
            this.this$0 = this$0;
            this.requestTracker = requestTracker;
        }
        
        @Override
        public void onConnectivityChanged(final boolean b) {
            if (b) {
                synchronized (this.this$0) {
                    this.requestTracker.restartRequests();
                }
            }
        }
    }
}

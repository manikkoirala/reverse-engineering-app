// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import androidx.tracing.Trace;
import com.bumptech.glide.util.GlideSuppliers;
import java.util.Iterator;
import android.content.ContentResolver;
import android.content.res.Resources;
import com.bumptech.glide.load.resource.transcode.DrawableBytesTranscoder;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import com.bumptech.glide.load.resource.transcode.BitmapDrawableTranscoder;
import com.bumptech.glide.load.resource.drawable.UnitDrawableDecoder;
import com.bumptech.glide.load.model.ByteArrayLoader;
import com.bumptech.glide.load.model.stream.HttpGlideUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.MediaStoreFileLoader;
import com.bumptech.glide.load.model.stream.UrlLoader;
import java.net.URL;
import com.bumptech.glide.load.model.UrlUriLoader;
import com.bumptech.glide.load.model.UriLoader;
import com.bumptech.glide.load.model.stream.QMediaStoreUriLoader;
import com.bumptech.glide.load.model.stream.MediaStoreVideoThumbLoader;
import com.bumptech.glide.load.model.stream.MediaStoreImageThumbLoader;
import com.bumptech.glide.load.model.AssetUriLoader;
import com.bumptech.glide.load.model.StringLoader;
import com.bumptech.glide.load.model.DataUrlLoader;
import com.bumptech.glide.load.model.ResourceLoader;
import com.bumptech.glide.load.model.ResourceUriLoader;
import com.bumptech.glide.load.model.DirectResourceLoader;
import com.bumptech.glide.load.data.InputStreamRewinder;
import com.bumptech.glide.load.resource.file.FileDecoder;
import com.bumptech.glide.load.model.FileLoader;
import com.bumptech.glide.load.model.ByteBufferFileLoader;
import java.io.File;
import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.load.resource.bytes.ByteBufferRewinder;
import com.bumptech.glide.load.resource.bitmap.ResourceBitmapDecoder;
import android.net.Uri;
import com.bumptech.glide.load.resource.gif.GifFrameResourceDecoder;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.resource.gif.GifDrawableEncoder;
import com.bumptech.glide.load.resource.gif.StreamGifDecoder;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.load.resource.bitmap.BitmapDrawableEncoder;
import com.bumptech.glide.load.resource.bitmap.BitmapDrawableDecoder;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.resource.bitmap.UnitBitmapDecoder;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.UnitModelLoader;
import android.content.res.AssetFileDescriptor;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.resource.bitmap.ParcelFileDescriptorBitmapDecoder;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import android.graphics.Bitmap;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.model.ByteBufferEncoder;
import com.bumptech.glide.load.resource.transcode.GifDrawableBytesTranscoder;
import com.bumptech.glide.load.resource.transcode.BitmapBytesTranscoder;
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder;
import com.bumptech.glide.load.resource.drawable.ResourceDrawableDecoder;
import java.nio.ByteBuffer;
import com.bumptech.glide.load.resource.drawable.AnimatedImageDecoder;
import android.graphics.drawable.Drawable;
import java.io.InputStream;
import com.bumptech.glide.load.resource.bitmap.StreamBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.ByteBufferBitmapDecoder;
import com.bumptech.glide.load.resource.bitmap.ByteBufferBitmapImageDecoderResourceDecoder;
import com.bumptech.glide.load.resource.bitmap.InputStreamBitmapImageDecoderResourceDecoder;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.resource.bitmap.VideoDecoder;
import com.bumptech.glide.load.resource.gif.ByteBufferGifDecoder;
import com.bumptech.glide.load.resource.bitmap.ExifInterfaceImageHeaderParser;
import android.os.Build$VERSION;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.resource.bitmap.DefaultImageHeaderParser;
import android.content.Context;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.module.GlideModule;
import java.util.List;

final class RegistryFactory
{
    private RegistryFactory() {
    }
    
    static Registry createAndInitRegistry(final Glide glide, final List<GlideModule> list, final AppGlideModule appGlideModule) {
        final BitmapPool bitmapPool = glide.getBitmapPool();
        final ArrayPool arrayPool = glide.getArrayPool();
        final Context applicationContext = glide.getGlideContext().getApplicationContext();
        final GlideExperiments experiments = glide.getGlideContext().getExperiments();
        final Registry registry = new Registry();
        initializeDefaults(applicationContext, registry, bitmapPool, arrayPool, experiments);
        initializeModules(applicationContext, glide, registry, list, appGlideModule);
        return registry;
    }
    
    private static void initializeDefaults(final Context context, final Registry registry, final BitmapPool bitmapPool, final ArrayPool arrayPool, final GlideExperiments glideExperiments) {
        registry.register(new DefaultImageHeaderParser());
        if (Build$VERSION.SDK_INT >= 27) {
            registry.register(new ExifInterfaceImageHeaderParser());
        }
        final Resources resources = context.getResources();
        final List<ImageHeaderParser> imageHeaderParsers = registry.getImageHeaderParsers();
        final ByteBufferGifDecoder byteBufferGifDecoder = new ByteBufferGifDecoder(context, imageHeaderParsers, bitmapPool, arrayPool);
        final ResourceDecoder<ParcelFileDescriptor, Bitmap> parcel = VideoDecoder.parcel(bitmapPool);
        final Downsampler downsampler = new Downsampler(registry.getImageHeaderParsers(), resources.getDisplayMetrics(), bitmapPool, arrayPool);
        ResourceDecoder<InputStream, Bitmap> resourceDecoder;
        ResourceDecoder<ByteBuffer, Bitmap> resourceDecoder2;
        if (Build$VERSION.SDK_INT >= 28 && glideExperiments.isEnabled((Class<? extends GlideExperiments.Experiment>)GlideBuilder.EnableImageDecoderForBitmaps.class)) {
            resourceDecoder = new InputStreamBitmapImageDecoderResourceDecoder();
            resourceDecoder2 = new ByteBufferBitmapImageDecoderResourceDecoder();
        }
        else {
            resourceDecoder2 = new ByteBufferBitmapDecoder(downsampler);
            resourceDecoder = new StreamBitmapDecoder(downsampler, arrayPool);
        }
        if (Build$VERSION.SDK_INT >= 28) {
            registry.append("Animation", InputStream.class, Drawable.class, AnimatedImageDecoder.streamDecoder(imageHeaderParsers, arrayPool));
            registry.append("Animation", ByteBuffer.class, Drawable.class, AnimatedImageDecoder.byteBufferDecoder(imageHeaderParsers, arrayPool));
        }
        final ResourceDrawableDecoder resourceDrawableDecoder = new ResourceDrawableDecoder(context);
        final BitmapEncoder bitmapEncoder = new BitmapEncoder(arrayPool);
        final BitmapBytesTranscoder bitmapBytesTranscoder = new BitmapBytesTranscoder();
        final GifDrawableBytesTranscoder gifDrawableBytesTranscoder = new GifDrawableBytesTranscoder();
        final ContentResolver contentResolver = context.getContentResolver();
        registry.append(ByteBuffer.class, new ByteBufferEncoder()).append(InputStream.class, new StreamEncoder(arrayPool)).append("Bitmap", ByteBuffer.class, Bitmap.class, resourceDecoder2).append("Bitmap", InputStream.class, Bitmap.class, resourceDecoder);
        if (ParcelFileDescriptorRewinder.isSupported()) {
            registry.append("Bitmap", ParcelFileDescriptor.class, Bitmap.class, new ParcelFileDescriptorBitmapDecoder(downsampler));
        }
        registry.append("Bitmap", ParcelFileDescriptor.class, Bitmap.class, parcel).append("Bitmap", AssetFileDescriptor.class, Bitmap.class, VideoDecoder.asset(bitmapPool)).append(Bitmap.class, Bitmap.class, (ModelLoaderFactory<Bitmap, Bitmap>)UnitModelLoader.Factory.getInstance()).append("Bitmap", Bitmap.class, Bitmap.class, new UnitBitmapDecoder()).append(Bitmap.class, bitmapEncoder).append("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new BitmapDrawableDecoder<ByteBuffer>(resources, resourceDecoder2)).append("BitmapDrawable", InputStream.class, BitmapDrawable.class, new BitmapDrawableDecoder<InputStream>(resources, resourceDecoder)).append("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new BitmapDrawableDecoder<ParcelFileDescriptor>(resources, parcel)).append(BitmapDrawable.class, new BitmapDrawableEncoder(bitmapPool, bitmapEncoder)).append("Animation", InputStream.class, GifDrawable.class, new StreamGifDecoder(imageHeaderParsers, byteBufferGifDecoder, arrayPool)).append("Animation", ByteBuffer.class, GifDrawable.class, byteBufferGifDecoder).append(GifDrawable.class, new GifDrawableEncoder()).append(GifDecoder.class, GifDecoder.class, (ModelLoaderFactory<GifDecoder, GifDecoder>)UnitModelLoader.Factory.getInstance()).append("Bitmap", GifDecoder.class, Bitmap.class, new GifFrameResourceDecoder(bitmapPool)).append(Uri.class, Drawable.class, resourceDrawableDecoder).append(Uri.class, Bitmap.class, new ResourceBitmapDecoder(resourceDrawableDecoder, bitmapPool)).register(new ByteBufferRewinder.Factory()).append(File.class, ByteBuffer.class, new ByteBufferFileLoader.Factory()).append(File.class, InputStream.class, new FileLoader.StreamFactory()).append(File.class, File.class, new FileDecoder()).append(File.class, ParcelFileDescriptor.class, new FileLoader.FileDescriptorFactory()).append(File.class, File.class, (ModelLoaderFactory<File, File>)UnitModelLoader.Factory.getInstance()).register(new InputStreamRewinder.Factory(arrayPool));
        if (ParcelFileDescriptorRewinder.isSupported()) {
            registry.register(new ParcelFileDescriptorRewinder.Factory());
        }
        final ModelLoaderFactory<Integer, InputStream> inputStreamFactory = DirectResourceLoader.inputStreamFactory(context);
        final ModelLoaderFactory<Integer, AssetFileDescriptor> assetFileDescriptorFactory = DirectResourceLoader.assetFileDescriptorFactory(context);
        final ModelLoaderFactory<Integer, Drawable> drawableFactory = DirectResourceLoader.drawableFactory(context);
        registry.append(Integer.TYPE, InputStream.class, inputStreamFactory).append(Integer.class, InputStream.class, inputStreamFactory).append(Integer.TYPE, AssetFileDescriptor.class, assetFileDescriptorFactory).append(Integer.class, AssetFileDescriptor.class, assetFileDescriptorFactory).append(Integer.TYPE, Drawable.class, drawableFactory).append(Integer.class, Drawable.class, drawableFactory).append(Uri.class, InputStream.class, ResourceUriLoader.newStreamFactory(context)).append(Uri.class, AssetFileDescriptor.class, ResourceUriLoader.newAssetFileDescriptorFactory(context));
        final ResourceLoader.UriFactory uriFactory = new ResourceLoader.UriFactory(resources);
        final ResourceLoader.AssetFileDescriptorFactory assetFileDescriptorFactory2 = new ResourceLoader.AssetFileDescriptorFactory(resources);
        final ResourceLoader.StreamFactory streamFactory = new ResourceLoader.StreamFactory(resources);
        registry.append(Integer.class, Uri.class, uriFactory).append(Integer.TYPE, Uri.class, uriFactory).append(Integer.class, AssetFileDescriptor.class, assetFileDescriptorFactory2).append(Integer.TYPE, AssetFileDescriptor.class, assetFileDescriptorFactory2).append(Integer.class, InputStream.class, streamFactory).append(Integer.TYPE, InputStream.class, streamFactory);
        registry.append(String.class, InputStream.class, new DataUrlLoader.StreamFactory<String>()).append(Uri.class, InputStream.class, new DataUrlLoader.StreamFactory<Uri>()).append(String.class, InputStream.class, new StringLoader.StreamFactory()).append(String.class, ParcelFileDescriptor.class, new StringLoader.FileDescriptorFactory()).append(String.class, AssetFileDescriptor.class, new StringLoader.AssetFileDescriptorFactory()).append(Uri.class, InputStream.class, new AssetUriLoader.StreamFactory(context.getAssets())).append(Uri.class, AssetFileDescriptor.class, new AssetUriLoader.FileDescriptorFactory(context.getAssets())).append(Uri.class, InputStream.class, new MediaStoreImageThumbLoader.Factory(context)).append(Uri.class, InputStream.class, new MediaStoreVideoThumbLoader.Factory(context));
        if (Build$VERSION.SDK_INT >= 29) {
            registry.append(Uri.class, InputStream.class, new QMediaStoreUriLoader.InputStreamFactory(context));
            registry.append(Uri.class, ParcelFileDescriptor.class, new QMediaStoreUriLoader.FileDescriptorFactory(context));
        }
        registry.append(Uri.class, InputStream.class, new UriLoader.StreamFactory(contentResolver)).append(Uri.class, ParcelFileDescriptor.class, new UriLoader.FileDescriptorFactory(contentResolver)).append(Uri.class, AssetFileDescriptor.class, new UriLoader.AssetFileDescriptorFactory(contentResolver)).append(Uri.class, InputStream.class, new UrlUriLoader.StreamFactory()).append(URL.class, InputStream.class, new UrlLoader.StreamFactory()).append(Uri.class, File.class, new MediaStoreFileLoader.Factory(context)).append(GlideUrl.class, InputStream.class, new HttpGlideUrlLoader.Factory()).append(byte[].class, ByteBuffer.class, new ByteArrayLoader.ByteBufferFactory()).append(byte[].class, InputStream.class, new ByteArrayLoader.StreamFactory()).append(Uri.class, Uri.class, (ModelLoaderFactory<Uri, Uri>)UnitModelLoader.Factory.getInstance()).append(Drawable.class, Drawable.class, (ModelLoaderFactory<Drawable, Drawable>)UnitModelLoader.Factory.getInstance()).append(Drawable.class, Drawable.class, new UnitDrawableDecoder()).register(Bitmap.class, BitmapDrawable.class, new BitmapDrawableTranscoder(resources)).register(Bitmap.class, byte[].class, bitmapBytesTranscoder).register(Drawable.class, byte[].class, new DrawableBytesTranscoder(bitmapPool, bitmapBytesTranscoder, gifDrawableBytesTranscoder)).register(GifDrawable.class, byte[].class, gifDrawableBytesTranscoder);
        if (Build$VERSION.SDK_INT >= 23) {
            final ResourceDecoder<ByteBuffer, Bitmap> byteBuffer = VideoDecoder.byteBuffer(bitmapPool);
            registry.append(ByteBuffer.class, Bitmap.class, byteBuffer);
            registry.append(ByteBuffer.class, BitmapDrawable.class, new BitmapDrawableDecoder<ByteBuffer>(resources, (ResourceDecoder<Object, Bitmap>)byteBuffer));
        }
    }
    
    private static void initializeModules(final Context context, final Glide glide, final Registry registry, List<GlideModule> glideModule, final AppGlideModule appGlideModule) {
        final Iterator<GlideModule> iterator = ((List<GlideModule>)glideModule).iterator();
        while (iterator.hasNext()) {
            glideModule = iterator.next();
            try {
                glideModule.registerComponents(context, glide, registry);
                continue;
            }
            catch (final AbstractMethodError cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: ");
                sb.append(((List<GlideModule>)glideModule).getClass().getName());
                throw new IllegalStateException(sb.toString(), cause);
            }
            break;
        }
        if (appGlideModule != null) {
            appGlideModule.registerComponents(context, glide, registry);
        }
    }
    
    static GlideSuppliers.GlideSupplier<Registry> lazilyCreateAndInitializeRegistry(final Glide glide, final List<GlideModule> list, final AppGlideModule appGlideModule) {
        return new GlideSuppliers.GlideSupplier<Registry>(glide, list, appGlideModule) {
            private boolean isInitializing;
            final AppGlideModule val$annotationGeneratedModule;
            final Glide val$glide;
            final List val$manifestModules;
            
            public Registry get() {
                if (!this.isInitializing) {
                    Trace.beginSection("Glide registry");
                    this.isInitializing = true;
                    try {
                        return RegistryFactory.createAndInitRegistry(this.val$glide, this.val$manifestModules, this.val$annotationGeneratedModule);
                    }
                    finally {
                        this.isInitializing = false;
                        Trace.endSection();
                    }
                }
                throw new IllegalStateException("Recursive Registry initialization! In your AppGlideModule and LibraryGlideModules, Make sure you're using the provided Registry rather calling glide.getRegistry()!");
            }
        };
    }
}

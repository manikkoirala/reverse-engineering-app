// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import com.bumptech.glide.manager.RequestManagerRetriever;
import java.util.HashSet;
import java.util.Set;
import com.bumptech.glide.module.AppGlideModule;

abstract class GeneratedAppGlideModule extends AppGlideModule
{
    Set<Class<?>> getExcludedModuleClasses() {
        return new HashSet<Class<?>>();
    }
    
    RequestManagerRetriever.RequestManagerFactory getRequestManagerFactory() {
        return null;
    }
}

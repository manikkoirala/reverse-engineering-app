// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import java.util.Arrays;
import com.bumptech.glide.request.RequestFutureTarget;
import com.bumptech.glide.request.target.PreloadTarget;
import java.net.URL;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;
import com.bumptech.glide.request.target.ViewTarget;
import android.widget.ImageView;
import com.bumptech.glide.util.Executors;
import java.util.Objects;
import java.io.File;
import com.bumptech.glide.request.FutureTarget;
import java.util.Collection;
import java.util.ArrayList;
import com.bumptech.glide.request.SingleRequest;
import android.net.Uri;
import com.bumptech.glide.util.Preconditions;
import java.util.Iterator;
import com.bumptech.glide.request.ThumbnailRequestCoordinator;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.request.ErrorRequestCoordinator;
import com.bumptech.glide.request.RequestCoordinator;
import com.bumptech.glide.request.Request;
import java.util.concurrent.Executor;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.AndroidResourceSignature;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import java.util.List;
import android.content.Context;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.BaseRequestOptions;

public class RequestBuilder<TranscodeType> extends BaseRequestOptions<RequestBuilder<TranscodeType>> implements Cloneable, ModelTypes<RequestBuilder<TranscodeType>>
{
    protected static final RequestOptions DOWNLOAD_ONLY_OPTIONS;
    private final Context context;
    private RequestBuilder<TranscodeType> errorBuilder;
    private final Glide glide;
    private final GlideContext glideContext;
    private boolean isDefaultTransitionOptionsSet;
    private boolean isModelSet;
    private boolean isThumbnailBuilt;
    private Object model;
    private List<RequestListener<TranscodeType>> requestListeners;
    private final RequestManager requestManager;
    private Float thumbSizeMultiplier;
    private RequestBuilder<TranscodeType> thumbnailBuilder;
    private final Class<TranscodeType> transcodeClass;
    private TransitionOptions<?, ? super TranscodeType> transitionOptions;
    
    static {
        DOWNLOAD_ONLY_OPTIONS = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.DATA).priority(Priority.LOW).skipMemoryCache(true);
    }
    
    protected RequestBuilder(final Glide glide, final RequestManager requestManager, final Class<TranscodeType> transcodeClass, final Context context) {
        this.isDefaultTransitionOptionsSet = true;
        this.glide = glide;
        this.requestManager = requestManager;
        this.transcodeClass = transcodeClass;
        this.context = context;
        this.transitionOptions = requestManager.getDefaultTransitionOptions(transcodeClass);
        this.glideContext = glide.getGlideContext();
        this.initRequestListeners(requestManager.getDefaultRequestListeners());
        this.apply(requestManager.getDefaultRequestOptions());
    }
    
    protected RequestBuilder(final Class<TranscodeType> clazz, final RequestBuilder<?> requestBuilder) {
        this(requestBuilder.glide, requestBuilder.requestManager, clazz, requestBuilder.context);
        this.model = requestBuilder.model;
        this.isModelSet = requestBuilder.isModelSet;
        this.apply(requestBuilder);
    }
    
    private RequestBuilder<TranscodeType> applyResourceThemeAndSignature(final RequestBuilder<TranscodeType> requestBuilder) {
        return (RequestBuilder<TranscodeType>)requestBuilder.theme(this.context.getTheme()).signature(AndroidResourceSignature.obtain(this.context));
    }
    
    private Request buildRequest(final Target<TranscodeType> target, final RequestListener<TranscodeType> requestListener, final BaseRequestOptions<?> baseRequestOptions, final Executor executor) {
        return this.buildRequestRecursive(new Object(), target, requestListener, null, this.transitionOptions, baseRequestOptions.getPriority(), baseRequestOptions.getOverrideWidth(), baseRequestOptions.getOverrideHeight(), baseRequestOptions, executor);
    }
    
    private Request buildRequestRecursive(final Object o, final Target<TranscodeType> target, final RequestListener<TranscodeType> requestListener, final RequestCoordinator requestCoordinator, final TransitionOptions<?, ? super TranscodeType> transitionOptions, final Priority priority, final int n, final int n2, final BaseRequestOptions<?> baseRequestOptions, final Executor executor) {
        ErrorRequestCoordinator errorRequestCoordinator;
        RequestCoordinator requestCoordinator2;
        if (this.errorBuilder != null) {
            requestCoordinator2 = (errorRequestCoordinator = new ErrorRequestCoordinator(o, requestCoordinator));
        }
        else {
            final ErrorRequestCoordinator errorRequestCoordinator2 = null;
            requestCoordinator2 = requestCoordinator;
            errorRequestCoordinator = errorRequestCoordinator2;
        }
        final Request buildThumbnailRequestRecursive = this.buildThumbnailRequestRecursive(o, target, requestListener, requestCoordinator2, transitionOptions, priority, n, n2, baseRequestOptions, executor);
        if (errorRequestCoordinator == null) {
            return buildThumbnailRequestRecursive;
        }
        final int overrideWidth = this.errorBuilder.getOverrideWidth();
        final int overrideHeight = this.errorBuilder.getOverrideHeight();
        int overrideWidth2 = overrideWidth;
        int overrideHeight2 = overrideHeight;
        if (Util.isValidDimensions(n, n2)) {
            overrideWidth2 = overrideWidth;
            overrideHeight2 = overrideHeight;
            if (!this.errorBuilder.isValidOverride()) {
                overrideWidth2 = baseRequestOptions.getOverrideWidth();
                overrideHeight2 = baseRequestOptions.getOverrideHeight();
            }
        }
        final RequestBuilder<TranscodeType> errorBuilder = this.errorBuilder;
        errorRequestCoordinator.setRequests(buildThumbnailRequestRecursive, errorBuilder.buildRequestRecursive(o, target, requestListener, errorRequestCoordinator, errorBuilder.transitionOptions, errorBuilder.getPriority(), overrideWidth2, overrideHeight2, this.errorBuilder, executor));
        return errorRequestCoordinator;
    }
    
    private Request buildThumbnailRequestRecursive(final Object o, final Target<TranscodeType> target, final RequestListener<TranscodeType> requestListener, final RequestCoordinator requestCoordinator, final TransitionOptions<?, ? super TranscodeType> transitionOptions, final Priority priority, final int n, final int n2, final BaseRequestOptions<?> baseRequestOptions, final Executor executor) {
        final RequestBuilder<TranscodeType> thumbnailBuilder = this.thumbnailBuilder;
        if (thumbnailBuilder != null) {
            if (!this.isThumbnailBuilt) {
                TransitionOptions<?, ? super TranscodeType> transitionOptions2 = thumbnailBuilder.transitionOptions;
                if (thumbnailBuilder.isDefaultTransitionOptionsSet) {
                    transitionOptions2 = transitionOptions;
                }
                Priority priority2;
                if (thumbnailBuilder.isPrioritySet()) {
                    priority2 = this.thumbnailBuilder.getPriority();
                }
                else {
                    priority2 = this.getThumbnailPriority(priority);
                }
                final int overrideWidth = this.thumbnailBuilder.getOverrideWidth();
                final int overrideHeight = this.thumbnailBuilder.getOverrideHeight();
                int overrideWidth2 = overrideWidth;
                int overrideHeight2 = overrideHeight;
                if (Util.isValidDimensions(n, n2)) {
                    overrideWidth2 = overrideWidth;
                    overrideHeight2 = overrideHeight;
                    if (!this.thumbnailBuilder.isValidOverride()) {
                        overrideWidth2 = baseRequestOptions.getOverrideWidth();
                        overrideHeight2 = baseRequestOptions.getOverrideHeight();
                    }
                }
                final ThumbnailRequestCoordinator thumbnailRequestCoordinator = new ThumbnailRequestCoordinator(o, requestCoordinator);
                final Request obtainRequest = this.obtainRequest(o, target, requestListener, baseRequestOptions, thumbnailRequestCoordinator, transitionOptions, priority, n, n2, executor);
                this.isThumbnailBuilt = true;
                final RequestBuilder<TranscodeType> thumbnailBuilder2 = this.thumbnailBuilder;
                final Request buildRequestRecursive = thumbnailBuilder2.buildRequestRecursive(o, target, requestListener, thumbnailRequestCoordinator, transitionOptions2, priority2, overrideWidth2, overrideHeight2, thumbnailBuilder2, executor);
                this.isThumbnailBuilt = false;
                thumbnailRequestCoordinator.setRequests(obtainRequest, buildRequestRecursive);
                return thumbnailRequestCoordinator;
            }
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        }
        else {
            if (this.thumbSizeMultiplier != null) {
                final ThumbnailRequestCoordinator thumbnailRequestCoordinator2 = new ThumbnailRequestCoordinator(o, requestCoordinator);
                thumbnailRequestCoordinator2.setRequests(this.obtainRequest(o, target, requestListener, baseRequestOptions, thumbnailRequestCoordinator2, transitionOptions, priority, n, n2, executor), this.obtainRequest(o, target, requestListener, baseRequestOptions.clone().sizeMultiplier(this.thumbSizeMultiplier), thumbnailRequestCoordinator2, transitionOptions, this.getThumbnailPriority(priority), n, n2, executor));
                return thumbnailRequestCoordinator2;
            }
            return this.obtainRequest(o, target, requestListener, baseRequestOptions, requestCoordinator, transitionOptions, priority, n, n2, executor);
        }
    }
    
    private RequestBuilder<TranscodeType> cloneWithNullErrorAndThumbnail() {
        final RequestBuilder<TranscodeType> clone = this.clone();
        final RequestBuilder requestBuilder = null;
        return clone.error((RequestBuilder)null).thumbnail((RequestBuilder)null);
    }
    
    private Priority getThumbnailPriority(final Priority priority) {
        final int n = RequestBuilder$1.$SwitchMap$com$bumptech$glide$Priority[priority.ordinal()];
        if (n == 1) {
            return Priority.NORMAL;
        }
        if (n == 2) {
            return Priority.HIGH;
        }
        if (n != 3 && n != 4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("unknown priority: ");
            sb.append(this.getPriority());
            throw new IllegalArgumentException(sb.toString());
        }
        return Priority.IMMEDIATE;
    }
    
    private void initRequestListeners(final List<RequestListener<Object>> list) {
        final Iterator<RequestListener<Object>> iterator = list.iterator();
        while (iterator.hasNext()) {
            this.addListener((RequestListener<TranscodeType>)iterator.next());
        }
    }
    
    private <Y extends Target<TranscodeType>> Y into(final Y y, final RequestListener<TranscodeType> requestListener, final BaseRequestOptions<?> baseRequestOptions, final Executor executor) {
        Preconditions.checkNotNull(y);
        if (!this.isModelSet) {
            throw new IllegalArgumentException("You must call #load() before calling #into()");
        }
        final Request buildRequest = this.buildRequest(y, requestListener, baseRequestOptions, executor);
        final Request request = y.getRequest();
        if (buildRequest.isEquivalentTo(request) && !this.isSkipMemoryCacheWithCompletePreviousRequest(baseRequestOptions, request)) {
            if (!Preconditions.checkNotNull(request).isRunning()) {
                request.begin();
            }
            return y;
        }
        this.requestManager.clear(y);
        y.setRequest(buildRequest);
        this.requestManager.track(y, buildRequest);
        return y;
    }
    
    private boolean isSkipMemoryCacheWithCompletePreviousRequest(final BaseRequestOptions<?> baseRequestOptions, final Request request) {
        return !baseRequestOptions.isMemoryCacheable() && request.isComplete();
    }
    
    private RequestBuilder<TranscodeType> loadGeneric(final Object model) {
        if (this.isAutoCloneEnabled()) {
            return this.clone().loadGeneric(model);
        }
        this.model = model;
        this.isModelSet = true;
        return this.selfOrThrowIfLocked();
    }
    
    private RequestBuilder<TranscodeType> maybeApplyOptionsResourceUri(final Uri uri, final RequestBuilder<TranscodeType> requestBuilder) {
        if (uri != null && "android.resource".equals(uri.getScheme())) {
            return this.applyResourceThemeAndSignature(requestBuilder);
        }
        return requestBuilder;
    }
    
    private Request obtainRequest(final Object o, final Target<TranscodeType> target, final RequestListener<TranscodeType> requestListener, final BaseRequestOptions<?> baseRequestOptions, final RequestCoordinator requestCoordinator, final TransitionOptions<?, ? super TranscodeType> transitionOptions, final Priority priority, final int n, final int n2, final Executor executor) {
        final Context context = this.context;
        final GlideContext glideContext = this.glideContext;
        return SingleRequest.obtain(context, glideContext, o, this.model, this.transcodeClass, baseRequestOptions, n, n2, priority, target, requestListener, this.requestListeners, requestCoordinator, glideContext.getEngine(), transitionOptions.getTransitionFactory(), executor);
    }
    
    public RequestBuilder<TranscodeType> addListener(final RequestListener<TranscodeType> requestListener) {
        if (this.isAutoCloneEnabled()) {
            return this.clone().addListener(requestListener);
        }
        if (requestListener != null) {
            if (this.requestListeners == null) {
                this.requestListeners = new ArrayList<RequestListener<TranscodeType>>();
            }
            this.requestListeners.add(requestListener);
        }
        return this.selfOrThrowIfLocked();
    }
    
    @Override
    public RequestBuilder<TranscodeType> apply(final BaseRequestOptions<?> baseRequestOptions) {
        Preconditions.checkNotNull(baseRequestOptions);
        return super.apply(baseRequestOptions);
    }
    
    @Override
    public RequestBuilder<TranscodeType> clone() {
        final RequestBuilder requestBuilder = super.clone();
        requestBuilder.transitionOptions = (TransitionOptions<?, ? super TranscodeType>)requestBuilder.transitionOptions.clone();
        if (requestBuilder.requestListeners != null) {
            requestBuilder.requestListeners = new ArrayList<RequestListener<TranscodeType>>(requestBuilder.requestListeners);
        }
        final RequestBuilder<TranscodeType> thumbnailBuilder = requestBuilder.thumbnailBuilder;
        if (thumbnailBuilder != null) {
            requestBuilder.thumbnailBuilder = thumbnailBuilder.clone();
        }
        final RequestBuilder<TranscodeType> errorBuilder = requestBuilder.errorBuilder;
        if (errorBuilder != null) {
            requestBuilder.errorBuilder = errorBuilder.clone();
        }
        return requestBuilder;
    }
    
    @Deprecated
    public FutureTarget<File> downloadOnly(final int n, final int n2) {
        return this.getDownloadOnlyRequest().submit(n, n2);
    }
    
    @Deprecated
    public <Y extends Target<File>> Y downloadOnly(final Y y) {
        return (Y)this.getDownloadOnlyRequest().into((Target)y);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof RequestBuilder;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final RequestBuilder requestBuilder = (RequestBuilder)o;
            b3 = b2;
            if (super.equals(requestBuilder)) {
                b3 = b2;
                if (Objects.equals(this.transcodeClass, requestBuilder.transcodeClass)) {
                    b3 = b2;
                    if (this.transitionOptions.equals(requestBuilder.transitionOptions)) {
                        b3 = b2;
                        if (Objects.equals(this.model, requestBuilder.model)) {
                            b3 = b2;
                            if (Objects.equals(this.requestListeners, requestBuilder.requestListeners)) {
                                b3 = b2;
                                if (Objects.equals(this.thumbnailBuilder, requestBuilder.thumbnailBuilder)) {
                                    b3 = b2;
                                    if (Objects.equals(this.errorBuilder, requestBuilder.errorBuilder)) {
                                        b3 = b2;
                                        if (Objects.equals(this.thumbSizeMultiplier, requestBuilder.thumbSizeMultiplier)) {
                                            b3 = b2;
                                            if (this.isDefaultTransitionOptionsSet == requestBuilder.isDefaultTransitionOptionsSet) {
                                                b3 = b2;
                                                if (this.isModelSet == requestBuilder.isModelSet) {
                                                    b3 = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    public RequestBuilder<TranscodeType> error(final RequestBuilder<TranscodeType> errorBuilder) {
        if (this.isAutoCloneEnabled()) {
            return this.clone().error(errorBuilder);
        }
        this.errorBuilder = errorBuilder;
        return this.selfOrThrowIfLocked();
    }
    
    public RequestBuilder<TranscodeType> error(final Object o) {
        if (o == null) {
            final RequestBuilder requestBuilder = null;
            return this.error((RequestBuilder<TranscodeType>)null);
        }
        return this.error(this.cloneWithNullErrorAndThumbnail().load(o));
    }
    
    protected RequestBuilder<File> getDownloadOnlyRequest() {
        return new RequestBuilder<File>(File.class, this).apply(RequestBuilder.DOWNLOAD_ONLY_OPTIONS);
    }
    
    RequestManager getRequestManager() {
        return this.requestManager;
    }
    
    @Override
    public int hashCode() {
        return Util.hashCode(this.isModelSet, Util.hashCode(this.isDefaultTransitionOptionsSet, Util.hashCode(this.thumbSizeMultiplier, Util.hashCode(this.errorBuilder, Util.hashCode(this.thumbnailBuilder, Util.hashCode(this.requestListeners, Util.hashCode(this.model, Util.hashCode(this.transitionOptions, Util.hashCode(this.transcodeClass, super.hashCode())))))))));
    }
    
    @Deprecated
    public FutureTarget<TranscodeType> into(final int n, final int n2) {
        return this.submit(n, n2);
    }
    
    public <Y extends Target<TranscodeType>> Y into(final Y y) {
        return this.into(y, null, Executors.mainThreadExecutor());
    }
    
     <Y extends Target<TranscodeType>> Y into(final Y y, final RequestListener<TranscodeType> requestListener, final Executor executor) {
        return this.into(y, requestListener, this, executor);
    }
    
    public ViewTarget<ImageView, TranscodeType> into(final ImageView imageView) {
        Util.assertMainThread();
        Preconditions.checkNotNull(imageView);
        if (!this.isTransformationSet() && this.isTransformationAllowed() && imageView.getScaleType() != null) {
            switch (RequestBuilder$1.$SwitchMap$android$widget$ImageView$ScaleType[imageView.getScaleType().ordinal()]) {
                case 6: {
                    final BaseRequestOptions<TranscodeType> baseRequestOptions = this.clone().optionalCenterInside();
                    return this.into(this.glideContext.buildImageViewTarget(imageView, this.transcodeClass), null, baseRequestOptions, Executors.mainThreadExecutor());
                }
                case 3:
                case 4:
                case 5: {
                    final BaseRequestOptions<TranscodeType> baseRequestOptions = this.clone().optionalFitCenter();
                    return this.into(this.glideContext.buildImageViewTarget(imageView, this.transcodeClass), null, baseRequestOptions, Executors.mainThreadExecutor());
                }
                case 2: {
                    final BaseRequestOptions<TranscodeType> baseRequestOptions = this.clone().optionalCenterInside();
                    return this.into(this.glideContext.buildImageViewTarget(imageView, this.transcodeClass), null, baseRequestOptions, Executors.mainThreadExecutor());
                }
                case 1: {
                    final BaseRequestOptions<TranscodeType> baseRequestOptions = this.clone().optionalCenterCrop();
                    return this.into(this.glideContext.buildImageViewTarget(imageView, this.transcodeClass), null, baseRequestOptions, Executors.mainThreadExecutor());
                }
            }
        }
        final BaseRequestOptions<TranscodeType> baseRequestOptions = (BaseRequestOptions<TranscodeType>)this;
        return this.into(this.glideContext.buildImageViewTarget(imageView, this.transcodeClass), null, baseRequestOptions, Executors.mainThreadExecutor());
    }
    
    public RequestBuilder<TranscodeType> listener(final RequestListener<TranscodeType> requestListener) {
        if (this.isAutoCloneEnabled()) {
            return this.clone().listener(requestListener);
        }
        this.requestListeners = null;
        return this.addListener(requestListener);
    }
    
    @Override
    public RequestBuilder<TranscodeType> load(final Bitmap bitmap) {
        return this.loadGeneric(bitmap).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE));
    }
    
    @Override
    public RequestBuilder<TranscodeType> load(final Drawable drawable) {
        return this.loadGeneric(drawable).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE));
    }
    
    @Override
    public RequestBuilder<TranscodeType> load(final Uri uri) {
        return this.maybeApplyOptionsResourceUri(uri, this.loadGeneric(uri));
    }
    
    @Override
    public RequestBuilder<TranscodeType> load(final File file) {
        return this.loadGeneric(file);
    }
    
    @Override
    public RequestBuilder<TranscodeType> load(final Integer n) {
        return this.applyResourceThemeAndSignature(this.loadGeneric(n));
    }
    
    @Override
    public RequestBuilder<TranscodeType> load(final Object o) {
        return this.loadGeneric(o);
    }
    
    @Override
    public RequestBuilder<TranscodeType> load(final String s) {
        return this.loadGeneric(s);
    }
    
    @Deprecated
    @Override
    public RequestBuilder<TranscodeType> load(final URL url) {
        return this.loadGeneric(url);
    }
    
    @Override
    public RequestBuilder<TranscodeType> load(final byte[] array) {
        RequestBuilder requestBuilder2;
        final RequestBuilder<TranscodeType> requestBuilder = requestBuilder2 = this.loadGeneric(array);
        if (!requestBuilder.isDiskCacheStrategySet()) {
            requestBuilder2 = requestBuilder.apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE));
        }
        RequestBuilder apply = requestBuilder2;
        if (!requestBuilder2.isSkipMemoryCacheSet()) {
            apply = requestBuilder2.apply(RequestOptions.skipMemoryCacheOf(true));
        }
        return apply;
    }
    
    public Target<TranscodeType> preload() {
        return this.preload(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }
    
    public Target<TranscodeType> preload(final int n, final int n2) {
        return this.into((Target<TranscodeType>)PreloadTarget.obtain(this.requestManager, n, n2));
    }
    
    public FutureTarget<TranscodeType> submit() {
        return this.submit(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }
    
    public FutureTarget<TranscodeType> submit(final int n, final int n2) {
        final RequestFutureTarget requestFutureTarget = new RequestFutureTarget(n, n2);
        return this.into(requestFutureTarget, requestFutureTarget, Executors.directExecutor());
    }
    
    @Deprecated
    public RequestBuilder<TranscodeType> thumbnail(final float f) {
        if (this.isAutoCloneEnabled()) {
            return this.clone().thumbnail(f);
        }
        if (f >= 0.0f && f <= 1.0f) {
            this.thumbSizeMultiplier = f;
            return this.selfOrThrowIfLocked();
        }
        throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
    }
    
    public RequestBuilder<TranscodeType> thumbnail(final RequestBuilder<TranscodeType> thumbnailBuilder) {
        if (this.isAutoCloneEnabled()) {
            return this.clone().thumbnail(thumbnailBuilder);
        }
        this.thumbnailBuilder = thumbnailBuilder;
        return this.selfOrThrowIfLocked();
    }
    
    public RequestBuilder<TranscodeType> thumbnail(final List<RequestBuilder<TranscodeType>> list) {
        RequestBuilder thumbnail = null;
        if (list != null && !list.isEmpty()) {
            for (int i = list.size() - 1; i >= 0; --i) {
                final RequestBuilder requestBuilder = list.get(i);
                if (requestBuilder != null) {
                    if (thumbnail == null) {
                        thumbnail = requestBuilder;
                    }
                    else {
                        thumbnail = requestBuilder.thumbnail(thumbnail);
                    }
                }
            }
            return this.thumbnail((RequestBuilder<TranscodeType>)thumbnail);
        }
        final RequestBuilder requestBuilder2 = null;
        return this.thumbnail((RequestBuilder<TranscodeType>)null);
    }
    
    public RequestBuilder<TranscodeType> thumbnail(RequestBuilder<TranscodeType>... a) {
        if (a != null && a.length != 0) {
            return this.thumbnail(Arrays.asList((RequestBuilder<TranscodeType>[])(Object)a));
        }
        a = null;
        return this.thumbnail((RequestBuilder<TranscodeType>)null);
    }
    
    public RequestBuilder<TranscodeType> transition(final TransitionOptions<?, ? super TranscodeType> transitionOptions) {
        if (this.isAutoCloneEnabled()) {
            return this.clone().transition(transitionOptions);
        }
        this.transitionOptions = Preconditions.checkNotNull(transitionOptions);
        this.isDefaultTransitionOptionsSet = false;
        return this.selfOrThrowIfLocked();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.disklrucache;

import java.io.UnsupportedEncodingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.io.Closeable;

class StrictLineReader implements Closeable
{
    private static final byte CR = 13;
    private static final byte LF = 10;
    private byte[] buf;
    private final Charset charset;
    private int end;
    private final InputStream in;
    private int pos;
    
    public StrictLineReader(final InputStream in, final int n, final Charset charset) {
        if (in == null || charset == null) {
            throw null;
        }
        if (n < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        }
        if (charset.equals(Util.US_ASCII)) {
            this.in = in;
            this.charset = charset;
            this.buf = new byte[n];
            return;
        }
        throw new IllegalArgumentException("Unsupported encoding");
    }
    
    public StrictLineReader(final InputStream inputStream, final Charset charset) {
        this(inputStream, 8192, charset);
    }
    
    private void fillBuf() throws IOException {
        final InputStream in = this.in;
        final byte[] buf = this.buf;
        final int read = in.read(buf, 0, buf.length);
        if (read != -1) {
            this.pos = 0;
            this.end = read;
            return;
        }
        throw new EOFException();
    }
    
    @Override
    public void close() throws IOException {
        synchronized (this.in) {
            if (this.buf != null) {
                this.buf = null;
                this.in.close();
            }
        }
    }
    
    public boolean hasUnterminatedLine() {
        return this.end == -1;
    }
    
    public String readLine() throws IOException {
        synchronized (this.in) {
            if (this.buf != null) {
                if (this.pos >= this.end) {
                    this.fillBuf();
                }
                for (int i = this.pos; i != this.end; ++i) {
                    final byte[] buf = this.buf;
                    if (buf[i] == 10) {
                        final int pos = this.pos;
                        int n = 0;
                        Label_0087: {
                            if (i != pos) {
                                n = i - 1;
                                if (buf[n] == 13) {
                                    break Label_0087;
                                }
                            }
                            n = i;
                        }
                        final String s = new String(buf, pos, n - pos, this.charset.name());
                        this.pos = i + 1;
                        return s;
                    }
                }
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(this, this.end - this.pos + 80) {
                    final StrictLineReader this$0;
                    
                    @Override
                    public String toString() {
                        int count;
                        if (this.count > 0 && this.buf[this.count - 1] == 13) {
                            count = this.count - 1;
                        }
                        else {
                            count = this.count;
                        }
                        try {
                            return new String(this.buf, 0, count, this.this$0.charset.name());
                        }
                        catch (final UnsupportedEncodingException detailMessage) {
                            throw new AssertionError((Object)detailMessage);
                        }
                    }
                };
                int j = 0;
                byte[] buf3 = null;
            Block_10:
                while (true) {
                    final byte[] buf2 = this.buf;
                    final int pos2 = this.pos;
                    byteArrayOutputStream.write(buf2, pos2, this.end - pos2);
                    this.end = -1;
                    this.fillBuf();
                    for (j = this.pos; j != this.end; ++j) {
                        buf3 = this.buf;
                        if (buf3[j] == 10) {
                            break Block_10;
                        }
                    }
                }
                final int pos3 = this.pos;
                if (j != pos3) {
                    byteArrayOutputStream.write(buf3, pos3, j - pos3);
                }
                this.pos = j + 1;
                return byteArrayOutputStream.toString();
            }
            throw new IOException("LineReader is closed");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util;

import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

public final class ByteBufferUtil
{
    private static final AtomicReference<byte[]> BUFFER_REF;
    private static final int BUFFER_SIZE = 16384;
    
    static {
        BUFFER_REF = new AtomicReference<byte[]>();
    }
    
    private ByteBufferUtil() {
    }
    
    public static ByteBuffer fromFile(final File p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          5
        //     3: aconst_null    
        //     4: astore          4
        //     6: aload_0        
        //     7: invokevirtual   java/io/File.length:()J
        //    10: lstore_1       
        //    11: lload_1        
        //    12: ldc2_w          2147483647
        //    15: lcmp           
        //    16: ifgt            103
        //    19: lload_1        
        //    20: lconst_0       
        //    21: lcmp           
        //    22: ifeq            91
        //    25: new             Ljava/io/RandomAccessFile;
        //    28: astore_3       
        //    29: aload_3        
        //    30: aload_0        
        //    31: ldc             "r"
        //    33: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    36: aload           4
        //    38: astore_0       
        //    39: aload_3        
        //    40: invokevirtual   java/io/RandomAccessFile.getChannel:()Ljava/nio/channels/FileChannel;
        //    43: astore          4
        //    45: aload           4
        //    47: astore_0       
        //    48: aload           4
        //    50: getstatic       java/nio/channels/FileChannel$MapMode.READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;
        //    53: lconst_0       
        //    54: lload_1        
        //    55: invokevirtual   java/nio/channels/FileChannel.map:(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
        //    58: invokevirtual   java/nio/MappedByteBuffer.load:()Ljava/nio/MappedByteBuffer;
        //    61: astore          5
        //    63: aload           4
        //    65: ifnull          73
        //    68: aload           4
        //    70: invokevirtual   java/nio/channels/FileChannel.close:()V
        //    73: aload_3        
        //    74: invokevirtual   java/io/RandomAccessFile.close:()V
        //    77: aload           5
        //    79: areturn        
        //    80: astore          4
        //    82: aload_0        
        //    83: astore          5
        //    85: aload           4
        //    87: astore_0       
        //    88: goto            118
        //    91: new             Ljava/io/IOException;
        //    94: astore_0       
        //    95: aload_0        
        //    96: ldc             "File unsuitable for memory mapping"
        //    98: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   101: aload_0        
        //   102: athrow         
        //   103: new             Ljava/io/IOException;
        //   106: astore_0       
        //   107: aload_0        
        //   108: ldc             "File too large to map into memory"
        //   110: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   113: aload_0        
        //   114: athrow         
        //   115: astore_0       
        //   116: aconst_null    
        //   117: astore_3       
        //   118: aload           5
        //   120: ifnull          133
        //   123: aload           5
        //   125: invokevirtual   java/nio/channels/FileChannel.close:()V
        //   128: goto            133
        //   131: astore          4
        //   133: aload_3        
        //   134: ifnull          141
        //   137: aload_3        
        //   138: invokevirtual   java/io/RandomAccessFile.close:()V
        //   141: aload_0        
        //   142: athrow         
        //   143: astore_0       
        //   144: goto            73
        //   147: astore_0       
        //   148: goto            77
        //   151: astore_3       
        //   152: goto            141
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  6      11     115    118    Any
        //  25     36     115    118    Any
        //  39     45     80     91     Any
        //  48     63     80     91     Any
        //  68     73     143    147    Ljava/io/IOException;
        //  73     77     147    151    Ljava/io/IOException;
        //  91     103    115    118    Any
        //  103    115    115    118    Any
        //  123    128    131    133    Ljava/io/IOException;
        //  137    141    151    155    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 84 out of bounds for length 84
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static ByteBuffer fromStream(final InputStream inputStream) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        byte[] newValue;
        if ((newValue = ByteBufferUtil.BUFFER_REF.getAndSet(null)) == null) {
            newValue = new byte[16384];
        }
        while (true) {
            final int read = inputStream.read(newValue);
            if (read < 0) {
                break;
            }
            byteArrayOutputStream.write(newValue, 0, read);
        }
        ByteBufferUtil.BUFFER_REF.set(newValue);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        return rewind(ByteBuffer.allocateDirect(byteArray.length).put(byteArray));
    }
    
    private static SafeArray getSafeArray(final ByteBuffer byteBuffer) {
        if (!byteBuffer.isReadOnly() && byteBuffer.hasArray()) {
            return new SafeArray(byteBuffer.array(), byteBuffer.arrayOffset(), byteBuffer.limit());
        }
        return null;
    }
    
    public static ByteBuffer rewind(final ByteBuffer byteBuffer) {
        return (ByteBuffer)byteBuffer.position(0);
    }
    
    public static byte[] toBytes(final ByteBuffer byteBuffer) {
        final SafeArray safeArray = getSafeArray(byteBuffer);
        byte[] array;
        if (safeArray != null && safeArray.offset == 0 && safeArray.limit == safeArray.data.length) {
            array = byteBuffer.array();
        }
        else {
            final ByteBuffer readOnlyBuffer = byteBuffer.asReadOnlyBuffer();
            array = new byte[readOnlyBuffer.limit()];
            rewind(readOnlyBuffer);
            readOnlyBuffer.get(array);
        }
        return array;
    }
    
    public static void toFile(final ByteBuffer p0, final File p1) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokestatic    com/bumptech/glide/util/ByteBufferUtil.rewind:(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
        //     4: pop            
        //     5: aconst_null    
        //     6: astore_2       
        //     7: aconst_null    
        //     8: astore          4
        //    10: new             Ljava/io/RandomAccessFile;
        //    13: astore_3       
        //    14: aload_3        
        //    15: aload_1        
        //    16: ldc             "rw"
        //    18: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    21: aload           4
        //    23: astore_1       
        //    24: aload_3        
        //    25: invokevirtual   java/io/RandomAccessFile.getChannel:()Ljava/nio/channels/FileChannel;
        //    28: astore_2       
        //    29: aload_2        
        //    30: astore_1       
        //    31: aload_2        
        //    32: aload_0        
        //    33: invokevirtual   java/nio/channels/FileChannel.write:(Ljava/nio/ByteBuffer;)I
        //    36: pop            
        //    37: aload_2        
        //    38: astore_1       
        //    39: aload_2        
        //    40: iconst_0       
        //    41: invokevirtual   java/nio/channels/FileChannel.force:(Z)V
        //    44: aload_2        
        //    45: astore_1       
        //    46: aload_2        
        //    47: invokevirtual   java/nio/channels/FileChannel.close:()V
        //    50: aload_2        
        //    51: astore_1       
        //    52: aload_3        
        //    53: invokevirtual   java/io/RandomAccessFile.close:()V
        //    56: aload_2        
        //    57: ifnull          64
        //    60: aload_2        
        //    61: invokevirtual   java/nio/channels/FileChannel.close:()V
        //    64: aload_3        
        //    65: invokevirtual   java/io/RandomAccessFile.close:()V
        //    68: return         
        //    69: astore_0       
        //    70: aload_1        
        //    71: astore_2       
        //    72: aload_3        
        //    73: astore_1       
        //    74: goto            80
        //    77: astore_0       
        //    78: aconst_null    
        //    79: astore_1       
        //    80: aload_2        
        //    81: ifnull          92
        //    84: aload_2        
        //    85: invokevirtual   java/nio/channels/FileChannel.close:()V
        //    88: goto            92
        //    91: astore_2       
        //    92: aload_1        
        //    93: ifnull          100
        //    96: aload_1        
        //    97: invokevirtual   java/io/RandomAccessFile.close:()V
        //   100: aload_0        
        //   101: athrow         
        //   102: astore_0       
        //   103: goto            64
        //   106: astore_0       
        //   107: goto            68
        //   110: astore_1       
        //   111: goto            100
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  10     21     77     80     Any
        //  24     29     69     77     Any
        //  31     37     69     77     Any
        //  39     44     69     77     Any
        //  46     50     69     77     Any
        //  52     56     69     77     Any
        //  60     64     102    106    Ljava/io/IOException;
        //  64     68     106    110    Ljava/io/IOException;
        //  84     88     91     92     Ljava/io/IOException;
        //  96     100    110    114    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 71 out of bounds for length 71
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static InputStream toStream(final ByteBuffer byteBuffer) {
        return new ByteBufferStream(byteBuffer);
    }
    
    public static void toStream(final ByteBuffer byteBuffer, final OutputStream outputStream) throws IOException {
        final SafeArray safeArray = getSafeArray(byteBuffer);
        if (safeArray != null) {
            outputStream.write(safeArray.data, safeArray.offset, safeArray.offset + safeArray.limit);
        }
        else {
            byte[] newValue;
            if ((newValue = ByteBufferUtil.BUFFER_REF.getAndSet(null)) == null) {
                newValue = new byte[16384];
            }
            while (byteBuffer.remaining() > 0) {
                final int min = Math.min(byteBuffer.remaining(), newValue.length);
                byteBuffer.get(newValue, 0, min);
                outputStream.write(newValue, 0, min);
            }
            ByteBufferUtil.BUFFER_REF.set(newValue);
        }
    }
    
    private static class ByteBufferStream extends InputStream
    {
        private static final int UNSET = -1;
        private final ByteBuffer byteBuffer;
        private int markPos;
        
        ByteBufferStream(final ByteBuffer byteBuffer) {
            this.markPos = -1;
            this.byteBuffer = byteBuffer;
        }
        
        @Override
        public int available() {
            return this.byteBuffer.remaining();
        }
        
        @Override
        public void mark(final int n) {
            synchronized (this) {
                this.markPos = this.byteBuffer.position();
            }
        }
        
        @Override
        public boolean markSupported() {
            return true;
        }
        
        @Override
        public int read() {
            if (!this.byteBuffer.hasRemaining()) {
                return -1;
            }
            return this.byteBuffer.get() & 0xFF;
        }
        
        @Override
        public int read(final byte[] dst, final int offset, int min) {
            if (!this.byteBuffer.hasRemaining()) {
                return -1;
            }
            min = Math.min(min, this.available());
            this.byteBuffer.get(dst, offset, min);
            return min;
        }
        
        @Override
        public void reset() throws IOException {
            synchronized (this) {
                final int markPos = this.markPos;
                if (markPos != -1) {
                    this.byteBuffer.position(markPos);
                    return;
                }
                throw new IOException("Cannot reset to unset mark position");
            }
        }
        
        @Override
        public long skip(long min) {
            if (!this.byteBuffer.hasRemaining()) {
                return -1L;
            }
            min = Math.min(min, this.available());
            final ByteBuffer byteBuffer = this.byteBuffer;
            byteBuffer.position((int)(byteBuffer.position() + min));
            return min;
        }
    }
    
    static final class SafeArray
    {
        final byte[] data;
        final int limit;
        final int offset;
        
        SafeArray(final byte[] data, final int offset, final int limit) {
            this.data = data;
            this.offset = offset;
            this.limit = limit;
        }
    }
}

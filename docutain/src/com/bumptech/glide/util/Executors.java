// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executor;

public final class Executors
{
    private static final Executor DIRECT_EXECUTOR;
    private static final Executor MAIN_THREAD_EXECUTOR;
    
    static {
        MAIN_THREAD_EXECUTOR = new Executor() {
            @Override
            public void execute(final Runnable runnable) {
                Util.postOnUiThread(runnable);
            }
        };
        DIRECT_EXECUTOR = new Executor() {
            @Override
            public void execute(final Runnable runnable) {
                runnable.run();
            }
        };
    }
    
    private Executors() {
    }
    
    public static Executor directExecutor() {
        return Executors.DIRECT_EXECUTOR;
    }
    
    public static Executor mainThreadExecutor() {
        return Executors.MAIN_THREAD_EXECUTOR;
    }
    
    public static void shutdownAndAwaitTermination(final ExecutorService executorService) {
        executorService.shutdownNow();
        try {
            if (!executorService.awaitTermination(5L, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
                if (!executorService.awaitTermination(5L, TimeUnit.SECONDS)) {
                    throw new RuntimeException("Failed to shutdown");
                }
            }
        }
        catch (final InterruptedException cause) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
            throw new RuntimeException(cause);
        }
    }
}

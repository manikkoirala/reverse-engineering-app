// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util;

import android.os.SystemClock;
import android.os.Build$VERSION;

public final class LogTime
{
    private static final double MILLIS_MULTIPLIER;
    
    static {
        final int sdk_INT = Build$VERSION.SDK_INT;
        double millis_MULTIPLIER = 1.0;
        if (sdk_INT >= 17) {
            millis_MULTIPLIER = 1.0 / Math.pow(10.0, 6.0);
        }
        MILLIS_MULTIPLIER = millis_MULTIPLIER;
    }
    
    private LogTime() {
    }
    
    public static double getElapsedMillis(final long n) {
        return (getLogTime() - n) * LogTime.MILLIS_MULTIPLIER;
    }
    
    public static long getLogTime() {
        if (Build$VERSION.SDK_INT >= 17) {
            return SystemClock.elapsedRealtimeNanos();
        }
        return SystemClock.uptimeMillis();
    }
}

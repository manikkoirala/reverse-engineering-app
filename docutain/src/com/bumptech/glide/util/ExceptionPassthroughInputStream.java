// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util;

import java.io.IOException;
import java.util.Queue;
import java.io.InputStream;

public final class ExceptionPassthroughInputStream extends InputStream
{
    private static final Queue<ExceptionPassthroughInputStream> POOL;
    private IOException exception;
    private InputStream wrapped;
    
    static {
        POOL = Util.createQueue(0);
    }
    
    ExceptionPassthroughInputStream() {
    }
    
    static void clearQueue() {
        synchronized (ExceptionPassthroughInputStream.POOL) {
            while (true) {
                final Queue<ExceptionPassthroughInputStream> pool = ExceptionPassthroughInputStream.POOL;
                if (pool.isEmpty()) {
                    break;
                }
                pool.remove();
            }
        }
    }
    
    public static ExceptionPassthroughInputStream obtain(final InputStream inputStream) {
        Object pool = ExceptionPassthroughInputStream.POOL;
        synchronized (pool) {
            final ExceptionPassthroughInputStream exceptionPassthroughInputStream = ((Queue<ExceptionPassthroughInputStream>)pool).poll();
            monitorexit(pool);
            pool = exceptionPassthroughInputStream;
            if (exceptionPassthroughInputStream == null) {
                pool = new ExceptionPassthroughInputStream();
            }
            ((ExceptionPassthroughInputStream)pool).setInputStream(inputStream);
            return (ExceptionPassthroughInputStream)pool;
        }
    }
    
    @Override
    public int available() throws IOException {
        return this.wrapped.available();
    }
    
    @Override
    public void close() throws IOException {
        this.wrapped.close();
    }
    
    public IOException getException() {
        return this.exception;
    }
    
    @Override
    public void mark(final int readlimit) {
        this.wrapped.mark(readlimit);
    }
    
    @Override
    public boolean markSupported() {
        return this.wrapped.markSupported();
    }
    
    @Override
    public int read() throws IOException {
        try {
            return this.wrapped.read();
        }
        catch (final IOException exception) {
            throw this.exception = exception;
        }
    }
    
    @Override
    public int read(final byte[] b) throws IOException {
        try {
            return this.wrapped.read(b);
        }
        catch (final IOException exception) {
            throw this.exception = exception;
        }
    }
    
    @Override
    public int read(final byte[] b, int read, final int len) throws IOException {
        try {
            read = this.wrapped.read(b, read, len);
            return read;
        }
        catch (final IOException exception) {
            throw this.exception = exception;
        }
    }
    
    public void release() {
        this.exception = null;
        this.wrapped = null;
        final Queue<ExceptionPassthroughInputStream> pool = ExceptionPassthroughInputStream.POOL;
        synchronized (pool) {
            pool.offer(this);
        }
    }
    
    @Override
    public void reset() throws IOException {
        synchronized (this) {
            this.wrapped.reset();
        }
    }
    
    void setInputStream(final InputStream wrapped) {
        this.wrapped = wrapped;
    }
    
    @Override
    public long skip(long skip) throws IOException {
        try {
            skip = this.wrapped.skip(skip);
            return skip;
        }
        catch (final IOException exception) {
            throw this.exception = exception;
        }
    }
}

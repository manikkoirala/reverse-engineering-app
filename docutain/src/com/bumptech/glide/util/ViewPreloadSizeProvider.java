// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util;

import com.bumptech.glide.request.transition.Transition;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.request.target.CustomViewTarget;
import java.util.Arrays;
import android.view.View;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.ListPreloader;

public class ViewPreloadSizeProvider<T> implements PreloadSizeProvider<T>, SizeReadyCallback
{
    private int[] size;
    private SizeViewTarget viewTarget;
    
    public ViewPreloadSizeProvider() {
    }
    
    public ViewPreloadSizeProvider(final View view) {
        (this.viewTarget = new SizeViewTarget(view)).getSize(this);
    }
    
    @Override
    public int[] getPreloadSize(final T t, final int n, final int n2) {
        final int[] size = this.size;
        if (size == null) {
            return null;
        }
        return Arrays.copyOf(size, size.length);
    }
    
    @Override
    public void onSizeReady(final int n, final int n2) {
        this.size = new int[] { n, n2 };
        this.viewTarget = null;
    }
    
    public void setView(final View view) {
        if (this.size == null) {
            if (this.viewTarget == null) {
                (this.viewTarget = new SizeViewTarget(view)).getSize(this);
            }
        }
    }
    
    static final class SizeViewTarget extends CustomViewTarget<View, Object>
    {
        SizeViewTarget(final View view) {
            super(view);
        }
        
        @Override
        public void onLoadFailed(final Drawable drawable) {
        }
        
        @Override
        protected void onResourceCleared(final Drawable drawable) {
        }
        
        @Override
        public void onResourceReady(final Object o, final Transition<? super Object> transition) {
        }
    }
}

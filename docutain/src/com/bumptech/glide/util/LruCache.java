// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<T, Y>
{
    private final Map<T, Entry<Y>> cache;
    private long currentSize;
    private final long initialMaxSize;
    private long maxSize;
    
    public LruCache(final long n) {
        this.cache = new LinkedHashMap<T, Entry<Y>>(100, 0.75f, true);
        this.initialMaxSize = n;
        this.maxSize = n;
    }
    
    private void evict() {
        this.trimToSize(this.maxSize);
    }
    
    public void clearMemory() {
        this.trimToSize(0L);
    }
    
    public boolean contains(final T t) {
        synchronized (this) {
            return this.cache.containsKey(t);
        }
    }
    
    public Y get(final T t) {
        synchronized (this) {
            final Entry entry = this.cache.get(t);
            Object value;
            if (entry != null) {
                value = entry.value;
            }
            else {
                value = null;
            }
            return (Y)value;
        }
    }
    
    protected int getCount() {
        synchronized (this) {
            return this.cache.size();
        }
    }
    
    public long getCurrentSize() {
        synchronized (this) {
            return this.currentSize;
        }
    }
    
    public long getMaxSize() {
        synchronized (this) {
            return this.maxSize;
        }
    }
    
    protected int getSize(final Y y) {
        return 1;
    }
    
    protected void onItemEvicted(final T t, final Y y) {
    }
    
    public Y put(final T t, final Y obj) {
        synchronized (this) {
            final int size = this.getSize(obj);
            final long n = size;
            final long maxSize = this.maxSize;
            final Y y = null;
            if (n >= maxSize) {
                this.onItemEvicted(t, obj);
                return null;
            }
            if (obj != null) {
                this.currentSize += n;
            }
            final Map<T, Entry<Y>> cache = this.cache;
            Entry entry;
            if (obj == null) {
                entry = null;
            }
            else {
                entry = new Entry(obj, size);
            }
            final Entry entry2 = cache.put(t, entry);
            if (entry2 != null) {
                this.currentSize -= entry2.size;
                if (!entry2.value.equals(obj)) {
                    this.onItemEvicted(t, entry2.value);
                }
            }
            this.evict();
            Object value = y;
            if (entry2 != null) {
                value = entry2.value;
            }
            return (Y)value;
        }
    }
    
    public Y remove(final T t) {
        synchronized (this) {
            final Entry entry = this.cache.remove(t);
            if (entry == null) {
                return null;
            }
            this.currentSize -= entry.size;
            return (Y)entry.value;
        }
    }
    
    public void setSizeMultiplier(final float n) {
        monitorenter(this);
        if (n >= 0.0f) {
            Label_0046: {
                try {
                    this.maxSize = Math.round(this.initialMaxSize * n);
                    this.evict();
                    monitorexit(this);
                    return;
                }
                finally {
                    break Label_0046;
                }
                throw new IllegalArgumentException("Multiplier must be >= 0");
            }
            monitorexit(this);
        }
        throw new IllegalArgumentException("Multiplier must be >= 0");
    }
    
    protected void trimToSize(final long n) {
        synchronized (this) {
            while (this.currentSize > n) {
                final Iterator<Map.Entry<T, Entry<Y>>> iterator = this.cache.entrySet().iterator();
                final Map.Entry<K, Entry> entry = (Map.Entry<K, Entry>)iterator.next();
                final Entry entry2 = entry.getValue();
                this.currentSize -= entry2.size;
                final K key = entry.getKey();
                iterator.remove();
                this.onItemEvicted((T)key, entry2.value);
            }
        }
    }
    
    static final class Entry<Y>
    {
        final int size;
        final Y value;
        
        Entry(final Y value, final int size) {
            this.value = value;
            this.size = size;
        }
    }
}

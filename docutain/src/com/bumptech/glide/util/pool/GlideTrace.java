// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util.pool;

import java.util.concurrent.atomic.AtomicInteger;

public final class GlideTrace
{
    private static final AtomicInteger COOKIE_CREATOR;
    private static final int MAX_LENGTH = 127;
    private static final boolean TRACING_ENABLED = false;
    
    private GlideTrace() {
    }
    
    public static void beginSection(final String s) {
    }
    
    public static int beginSectionAsync(final String s) {
        return -1;
    }
    
    public static void beginSectionFormat(final String s, final Object o) {
    }
    
    public static void beginSectionFormat(final String s, final Object o, final Object o2) {
    }
    
    public static void beginSectionFormat(final String s, final Object o, final Object o2, final Object o3) {
    }
    
    public static void endSection() {
    }
    
    public static void endSectionAsync(final String s, final int n) {
    }
    
    private static String truncateTag(final String s) {
        String substring = s;
        if (s.length() > 127) {
            substring = s.substring(0, 126);
        }
        return substring;
    }
}

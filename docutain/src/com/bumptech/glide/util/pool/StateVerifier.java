// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util.pool;

public abstract class StateVerifier
{
    private static final boolean DEBUG = false;
    
    private StateVerifier() {
    }
    
    public static StateVerifier newInstance() {
        return new DefaultStateVerifier();
    }
    
    abstract void setRecycled(final boolean p0);
    
    public abstract void throwIfRecycled();
    
    private static class DebugStateVerifier extends StateVerifier
    {
        private volatile RuntimeException recycledAtStackTraceException;
        
        DebugStateVerifier() {
            super(null);
        }
        
        @Override
        void setRecycled(final boolean b) {
            if (b) {
                this.recycledAtStackTraceException = new RuntimeException("Released");
            }
            else {
                this.recycledAtStackTraceException = null;
            }
        }
        
        @Override
        public void throwIfRecycled() {
            if (this.recycledAtStackTraceException == null) {
                return;
            }
            throw new IllegalStateException("Already released", this.recycledAtStackTraceException);
        }
    }
    
    private static class DefaultStateVerifier extends StateVerifier
    {
        private volatile boolean isReleased;
        
        DefaultStateVerifier() {
            super(null);
        }
        
        public void setRecycled(final boolean isReleased) {
            this.isReleased = isReleased;
        }
        
        @Override
        public void throwIfRecycled() {
            if (!this.isReleased) {
                return;
            }
            throw new IllegalStateException("Already released");
        }
    }
}

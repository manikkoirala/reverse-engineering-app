// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util;

public final class GlideSuppliers
{
    private GlideSuppliers() {
    }
    
    public static <T> GlideSupplier<T> memorize(final GlideSupplier<T> glideSupplier) {
        return (GlideSupplier<T>)new GlideSupplier<T>(glideSupplier) {
            private volatile T instance;
            final GlideSupplier val$supplier;
            
            @Override
            public T get() {
                if (this.instance == null) {
                    synchronized (this) {
                        if (this.instance == null) {
                            this.instance = Preconditions.checkNotNull(this.val$supplier.get());
                        }
                    }
                }
                return this.instance;
            }
        };
    }
    
    public interface GlideSupplier<T>
    {
        T get();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.util;

import androidx.collection.SimpleArrayMap;
import androidx.collection.ArrayMap;

public final class CachedHashCodeArrayMap<K, V> extends ArrayMap<K, V>
{
    private int hashCode;
    
    @Override
    public void clear() {
        this.hashCode = 0;
        super.clear();
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            this.hashCode = super.hashCode();
        }
        return this.hashCode;
    }
    
    @Override
    public V put(final K k, final V v) {
        this.hashCode = 0;
        return super.put(k, v);
    }
    
    @Override
    public void putAll(final SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.hashCode = 0;
        super.putAll(simpleArrayMap);
    }
    
    @Override
    public V removeAt(final int n) {
        this.hashCode = 0;
        return super.removeAt(n);
    }
    
    @Override
    public V setValueAt(final int n, final V v) {
        this.hashCode = 0;
        return super.setValueAt(n, v);
    }
}

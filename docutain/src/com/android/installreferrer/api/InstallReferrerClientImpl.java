// 
// Decompiled by Procyon v0.6.0
// 

package com.android.installreferrer.api;

import com.google.android.finsky.externalreferrer.IGetInstallReferrerService$Stub;
import android.os.IBinder;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.List;
import android.content.pm.ResolveInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.os.RemoteException;
import android.os.Bundle;
import com.android.installreferrer.commons.InstallReferrerCommons;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.ServiceConnection;
import com.google.android.finsky.externalreferrer.IGetInstallReferrerService;
import android.content.Context;

class InstallReferrerClientImpl extends InstallReferrerClient
{
    private static final int PLAY_STORE_MIN_APP_VER = 80837300;
    private static final String SERVICE_ACTION_NAME = "com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE";
    private static final String SERVICE_NAME = "com.google.android.finsky.externalreferrer.GetInstallReferrerService";
    private static final String SERVICE_PACKAGE_NAME = "com.android.vending";
    private static final String TAG = "InstallReferrerClient";
    private int clientState;
    private final Context mApplicationContext;
    private IGetInstallReferrerService service;
    private ServiceConnection serviceConnection;
    
    public InstallReferrerClientImpl(final Context context) {
        this.clientState = 0;
        this.mApplicationContext = context.getApplicationContext();
    }
    
    private boolean isPlayStoreCompatible() {
        final PackageManager packageManager = this.mApplicationContext.getPackageManager();
        try {
            if (packageManager.getPackageInfo("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            }
            return false;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    @Override
    public void endConnection() {
        this.clientState = 3;
        if (this.serviceConnection != null) {
            InstallReferrerCommons.logVerbose("InstallReferrerClient", "Unbinding from service.");
            this.mApplicationContext.unbindService(this.serviceConnection);
            this.serviceConnection = null;
        }
        this.service = null;
    }
    
    @Override
    public ReferrerDetails getInstallReferrer() throws RemoteException {
        if (this.isReady()) {
            final Bundle bundle = new Bundle();
            bundle.putString("package_name", this.mApplicationContext.getPackageName());
            try {
                return new ReferrerDetails(this.service.c(bundle));
            }
            catch (final RemoteException ex) {
                InstallReferrerCommons.logWarn("InstallReferrerClient", "RemoteException getting install referrer information");
                this.clientState = 0;
                throw ex;
            }
        }
        throw new IllegalStateException("Service not connected. Please start a connection before using the service.");
    }
    
    @Override
    public boolean isReady() {
        return this.clientState == 2 && this.service != null && this.serviceConnection != null;
    }
    
    @Override
    public void startConnection(final InstallReferrerStateListener installReferrerStateListener) {
        if (this.isReady()) {
            InstallReferrerCommons.logVerbose("InstallReferrerClient", "Service connection is valid. No need to re-initialize.");
            installReferrerStateListener.onInstallReferrerSetupFinished(0);
            return;
        }
        final int clientState = this.clientState;
        if (clientState == 1) {
            InstallReferrerCommons.logWarn("InstallReferrerClient", "Client is already in the process of connecting to the service.");
            installReferrerStateListener.onInstallReferrerSetupFinished(3);
            return;
        }
        if (clientState == 3) {
            InstallReferrerCommons.logWarn("InstallReferrerClient", "Client was already closed and can't be reused. Please create another instance.");
            installReferrerStateListener.onInstallReferrerSetupFinished(3);
            return;
        }
        InstallReferrerCommons.logVerbose("InstallReferrerClient", "Starting install referrer service setup.");
        final Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        final List queryIntentServices = this.mApplicationContext.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
            final ResolveInfo resolveInfo = queryIntentServices.get(0);
            if (resolveInfo.serviceInfo != null) {
                final String packageName = resolveInfo.serviceInfo.packageName;
                final String name = resolveInfo.serviceInfo.name;
                if ("com.android.vending".equals(packageName) && name != null && this.isPlayStoreCompatible()) {
                    final Intent intent2 = new Intent(intent);
                    final InstallReferrerServiceConnection serviceConnection = new InstallReferrerServiceConnection(installReferrerStateListener);
                    this.serviceConnection = (ServiceConnection)serviceConnection;
                    try {
                        if (this.mApplicationContext.bindService(intent2, (ServiceConnection)serviceConnection, 1)) {
                            InstallReferrerCommons.logVerbose("InstallReferrerClient", "Service was bonded successfully.");
                            return;
                        }
                        InstallReferrerCommons.logWarn("InstallReferrerClient", "Connection to service is blocked.");
                        this.clientState = 0;
                        installReferrerStateListener.onInstallReferrerSetupFinished(1);
                        return;
                    }
                    catch (final SecurityException ex) {
                        InstallReferrerCommons.logWarn("InstallReferrerClient", "No permission to connect to service.");
                        this.clientState = 0;
                        installReferrerStateListener.onInstallReferrerSetupFinished(4);
                        return;
                    }
                }
                InstallReferrerCommons.logWarn("InstallReferrerClient", "Play Store missing or incompatible. Version 8.3.73 or later required.");
                this.clientState = 0;
                installReferrerStateListener.onInstallReferrerSetupFinished(2);
                return;
            }
        }
        this.clientState = 0;
        InstallReferrerCommons.logVerbose("InstallReferrerClient", "Install Referrer service unavailable on device.");
        installReferrerStateListener.onInstallReferrerSetupFinished(2);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ClientState {
        public static final int CLOSED = 3;
        public static final int CONNECTED = 2;
        public static final int CONNECTING = 1;
        public static final int DISCONNECTED = 0;
    }
    
    private final class InstallReferrerServiceConnection implements ServiceConnection
    {
        private final InstallReferrerStateListener mListener;
        final InstallReferrerClientImpl this$0;
        
        private InstallReferrerServiceConnection(final InstallReferrerClientImpl this$0, final InstallReferrerStateListener mListener) {
            this.this$0 = this$0;
            if (mListener != null) {
                this.mListener = mListener;
                return;
            }
            throw new RuntimeException("Please specify a listener to know when setup is done.");
        }
        
        public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
            InstallReferrerCommons.logVerbose("InstallReferrerClient", "Install Referrer service connected.");
            this.this$0.service = IGetInstallReferrerService$Stub.b(binder);
            this.this$0.clientState = 2;
            this.mListener.onInstallReferrerSetupFinished(0);
        }
        
        public void onServiceDisconnected(final ComponentName componentName) {
            InstallReferrerCommons.logWarn("InstallReferrerClient", "Install Referrer service disconnected.");
            this.this$0.service = null;
            this.this$0.clientState = 0;
            this.mListener.onInstallReferrerServiceDisconnected();
        }
    }
}

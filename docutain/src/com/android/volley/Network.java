// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

public interface Network
{
    NetworkResponse performRequest(final Request<?> p0) throws VolleyError;
}

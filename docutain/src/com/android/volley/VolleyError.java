// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

public class VolleyError extends Exception
{
    public final NetworkResponse networkResponse;
    private long networkTimeMs;
    
    public VolleyError() {
        this.networkResponse = null;
    }
    
    public VolleyError(final NetworkResponse networkResponse) {
        this.networkResponse = networkResponse;
    }
    
    public VolleyError(final String message) {
        super(message);
        this.networkResponse = null;
    }
    
    public VolleyError(final String message, final Throwable cause) {
        super(message, cause);
        this.networkResponse = null;
    }
    
    public VolleyError(final Throwable cause) {
        super(cause);
        this.networkResponse = null;
    }
    
    public long getNetworkTimeMs() {
        return this.networkTimeMs;
    }
    
    void setNetworkTimeMs(final long networkTimeMs) {
        this.networkTimeMs = networkTimeMs;
    }
}

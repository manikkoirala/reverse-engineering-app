// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import android.text.TextUtils;

public final class Header
{
    private final String mName;
    private final String mValue;
    
    public Header(final String mName, final String mValue) {
        this.mName = mName;
        this.mValue = mValue;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final Header header = (Header)o;
            if (!TextUtils.equals((CharSequence)this.mName, (CharSequence)header.mName) || !TextUtils.equals((CharSequence)this.mValue, (CharSequence)header.mValue)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public final String getName() {
        return this.mName;
    }
    
    public final String getValue() {
        return this.mValue;
    }
    
    @Override
    public int hashCode() {
        return this.mName.hashCode() * 31 + this.mValue.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Header[name=");
        sb.append(this.mName);
        sb.append(",value=");
        sb.append(this.mValue);
        sb.append("]");
        return sb.toString();
    }
}

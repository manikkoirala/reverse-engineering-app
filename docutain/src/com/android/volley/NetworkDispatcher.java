// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import android.os.Process;
import android.os.SystemClock;
import android.net.TrafficStats;
import android.os.Build$VERSION;
import java.util.concurrent.BlockingQueue;

public class NetworkDispatcher extends Thread
{
    private final Cache mCache;
    private final ResponseDelivery mDelivery;
    private final Network mNetwork;
    private final BlockingQueue<Request<?>> mQueue;
    private volatile boolean mQuit;
    
    public NetworkDispatcher(final BlockingQueue<Request<?>> mQueue, final Network mNetwork, final Cache mCache, final ResponseDelivery mDelivery) {
        this.mQuit = false;
        this.mQueue = mQueue;
        this.mNetwork = mNetwork;
        this.mCache = mCache;
        this.mDelivery = mDelivery;
    }
    
    private void addTrafficStatsTag(final Request<?> request) {
        if (Build$VERSION.SDK_INT >= 14) {
            TrafficStats.setThreadStatsTag(request.getTrafficStatsTag());
        }
    }
    
    private void parseAndDeliverNetworkError(final Request<?> request, VolleyError networkError) {
        networkError = request.parseNetworkError(networkError);
        this.mDelivery.postError(request, networkError);
    }
    
    private void processRequest() throws InterruptedException {
        this.processRequest(this.mQueue.take());
    }
    
    void processRequest(final Request<?> request) {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            request.addMarker("network-queue-take");
            if (request.isCanceled()) {
                request.finish("network-discard-cancelled");
                request.notifyListenerResponseNotUsable();
                return;
            }
            this.addTrafficStatsTag(request);
            final NetworkResponse performRequest = this.mNetwork.performRequest(request);
            request.addMarker("network-http-complete");
            if (performRequest.notModified && request.hasHadResponseDelivered()) {
                request.finish("not-modified");
                request.notifyListenerResponseNotUsable();
                return;
            }
            final Response networkResponse = request.parseNetworkResponse(performRequest);
            request.addMarker("network-parse-complete");
            if (request.shouldCache() && networkResponse.cacheEntry != null) {
                this.mCache.put(request.getCacheKey(), networkResponse.cacheEntry);
                request.addMarker("network-cache-written");
            }
            request.markDelivered();
            this.mDelivery.postResponse(request, networkResponse);
            request.notifyListenerResponseReceived(networkResponse);
        }
        catch (final Exception ex) {
            VolleyLog.e(ex, "Unhandled exception %s", ex.toString());
            final VolleyError volleyError = new VolleyError(ex);
            volleyError.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.mDelivery.postError(request, volleyError);
            request.notifyListenerResponseNotUsable();
        }
        catch (final VolleyError volleyError2) {
            volleyError2.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.parseAndDeliverNetworkError(request, volleyError2);
            request.notifyListenerResponseNotUsable();
        }
    }
    
    public void quit() {
        this.mQuit = true;
        this.interrupt();
    }
    
    @Override
    public void run() {
        Process.setThreadPriority(10);
    Label_0005_Outer:
        while (true) {
            while (true) {
                try {
                    while (true) {
                        this.processRequest();
                    }
                }
                catch (final InterruptedException ex) {
                    if (this.mQuit) {
                        Thread.currentThread().interrupt();
                        return;
                    }
                    VolleyLog.e("Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it", new Object[0]);
                    continue Label_0005_Outer;
                }
                continue;
            }
        }
    }
}

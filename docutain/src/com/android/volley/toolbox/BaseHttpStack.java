// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.io.InputStream;
import java.util.Iterator;
import org.apache.http.HttpEntity;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import com.android.volley.Header;
import java.util.ArrayList;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.apache.http.ProtocolVersion;
import com.android.volley.AuthFailureError;
import java.io.IOException;
import java.util.Map;
import com.android.volley.Request;

public abstract class BaseHttpStack implements HttpStack
{
    public abstract HttpResponse executeRequest(final Request<?> p0, final Map<String, String> p1) throws IOException, AuthFailureError;
    
    @Deprecated
    @Override
    public final org.apache.http.HttpResponse performRequest(final Request<?> request, final Map<String, String> map) throws IOException, AuthFailureError {
        final HttpResponse executeRequest = this.executeRequest(request, map);
        final BasicHttpResponse basicHttpResponse = new BasicHttpResponse((StatusLine)new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), executeRequest.getStatusCode(), ""));
        final ArrayList list = new ArrayList();
        for (final Header header : executeRequest.getHeaders()) {
            list.add(new BasicHeader(header.getName(), header.getValue()));
        }
        basicHttpResponse.setHeaders((org.apache.http.Header[])list.toArray(new org.apache.http.Header[list.size()]));
        final InputStream content = executeRequest.getContent();
        if (content != null) {
            final BasicHttpEntity entity = new BasicHttpEntity();
            entity.setContent(content);
            entity.setContentLength((long)executeRequest.getContentLength());
            basicHttpResponse.setEntity((HttpEntity)entity);
        }
        return (org.apache.http.HttpResponse)basicHttpResponse;
    }
}

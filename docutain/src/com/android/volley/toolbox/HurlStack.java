// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.io.FilterInputStream;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.ArrayList;
import com.android.volley.Header;
import java.util.List;
import java.util.Map;
import com.android.volley.AuthFailureError;
import java.io.IOException;
import java.io.DataOutputStream;
import com.android.volley.Request;
import java.io.InputStream;
import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;

public class HurlStack extends BaseHttpStack
{
    private static final int HTTP_CONTINUE = 100;
    private final SSLSocketFactory mSslSocketFactory;
    private final UrlRewriter mUrlRewriter;
    
    public HurlStack() {
        this(null);
    }
    
    public HurlStack(final UrlRewriter urlRewriter) {
        this(urlRewriter, null);
    }
    
    public HurlStack(final UrlRewriter mUrlRewriter, final SSLSocketFactory mSslSocketFactory) {
        this.mUrlRewriter = mUrlRewriter;
        this.mSslSocketFactory = mSslSocketFactory;
    }
    
    private static void addBody(final HttpURLConnection httpURLConnection, final Request<?> request, final byte[] b) throws IOException {
        httpURLConnection.setDoOutput(true);
        if (!httpURLConnection.getRequestProperties().containsKey("Content-Type")) {
            httpURLConnection.setRequestProperty("Content-Type", request.getBodyContentType());
        }
        final DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.write(b);
        dataOutputStream.close();
    }
    
    private static void addBodyIfExists(final HttpURLConnection httpURLConnection, final Request<?> request) throws IOException, AuthFailureError {
        final byte[] body = request.getBody();
        if (body != null) {
            addBody(httpURLConnection, request, body);
        }
    }
    
    static List<Header> convertHeaders(final Map<String, List<String>> map) {
        final ArrayList list = new ArrayList(map.size());
        for (final Map.Entry entry : map.entrySet()) {
            if (entry.getKey() != null) {
                final Iterator iterator2 = ((List)entry.getValue()).iterator();
                while (iterator2.hasNext()) {
                    list.add(new Header((String)entry.getKey(), (String)iterator2.next()));
                }
            }
        }
        return list;
    }
    
    private static boolean hasResponseBody(final int n, final int n2) {
        return n != 4 && (100 > n2 || n2 >= 200) && n2 != 204 && n2 != 304;
    }
    
    private static InputStream inputStreamFromConnection(HttpURLConnection httpURLConnection) {
        try {
            httpURLConnection = (HttpURLConnection)httpURLConnection.getInputStream();
        }
        catch (final IOException ex) {
            httpURLConnection = (HttpURLConnection)httpURLConnection.getErrorStream();
        }
        return (InputStream)httpURLConnection;
    }
    
    private HttpURLConnection openConnection(final URL url, final Request<?> request) throws IOException {
        final HttpURLConnection connection = this.createConnection(url);
        final int timeoutMs = request.getTimeoutMs();
        connection.setConnectTimeout(timeoutMs);
        connection.setReadTimeout(timeoutMs);
        connection.setUseCaches(false);
        connection.setDoInput(true);
        if ("https".equals(url.getProtocol())) {
            final SSLSocketFactory mSslSocketFactory = this.mSslSocketFactory;
            if (mSslSocketFactory != null) {
                ((HttpsURLConnection)connection).setSSLSocketFactory(mSslSocketFactory);
            }
        }
        return connection;
    }
    
    static void setConnectionParametersForRequest(final HttpURLConnection httpURLConnection, final Request<?> request) throws IOException, AuthFailureError {
        switch (request.getMethod()) {
            default: {
                throw new IllegalStateException("Unknown method type.");
            }
            case 7: {
                httpURLConnection.setRequestMethod("PATCH");
                addBodyIfExists(httpURLConnection, request);
                break;
            }
            case 6: {
                httpURLConnection.setRequestMethod("TRACE");
                break;
            }
            case 5: {
                httpURLConnection.setRequestMethod("OPTIONS");
                break;
            }
            case 4: {
                httpURLConnection.setRequestMethod("HEAD");
                break;
            }
            case 3: {
                httpURLConnection.setRequestMethod("DELETE");
                break;
            }
            case 2: {
                httpURLConnection.setRequestMethod("PUT");
                addBodyIfExists(httpURLConnection, request);
                break;
            }
            case 1: {
                httpURLConnection.setRequestMethod("POST");
                addBodyIfExists(httpURLConnection, request);
                break;
            }
            case 0: {
                httpURLConnection.setRequestMethod("GET");
                break;
            }
            case -1: {
                final byte[] postBody = request.getPostBody();
                if (postBody != null) {
                    httpURLConnection.setRequestMethod("POST");
                    addBody(httpURLConnection, request, postBody);
                    break;
                }
                break;
            }
        }
    }
    
    protected HttpURLConnection createConnection(final URL url) throws IOException {
        final HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        return httpURLConnection;
    }
    
    @Override
    public HttpResponse executeRequest(final Request<?> request, Map<String, String> openConnection) throws IOException, AuthFailureError {
        final String url = request.getUrl();
        final HashMap hashMap = new HashMap();
        hashMap.putAll((Map)openConnection);
        hashMap.putAll(request.getHeaders());
        final UrlRewriter mUrlRewriter = this.mUrlRewriter;
        String rewriteUrl = url;
        if (mUrlRewriter != null) {
            rewriteUrl = mUrlRewriter.rewriteUrl(url);
            if (rewriteUrl == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("URL blocked by rewriter: ");
                sb.append(url);
                throw new IOException(sb.toString());
            }
        }
        openConnection = this.openConnection(new URL(rewriteUrl), request);
        int n2;
        final int n = n2 = 0;
        try {
            final Iterator iterator = hashMap.keySet().iterator();
            while (true) {
                n2 = n;
                if (!iterator.hasNext()) {
                    break;
                }
                n2 = n;
                final String s = (String)iterator.next();
                n2 = n;
                openConnection.setRequestProperty(s, (String)hashMap.get(s));
            }
            n2 = n;
            setConnectionParametersForRequest(openConnection, request);
            n2 = n;
            final int responseCode = openConnection.getResponseCode();
            if (responseCode == -1) {
                n2 = n;
                n2 = n;
                final IOException ex = new IOException("Could not retrieve response code from HttpUrlConnection.");
                n2 = n;
                throw ex;
            }
            n2 = n;
            if (!hasResponseBody(request.getMethod(), responseCode)) {
                n2 = n;
                final HttpResponse httpResponse = new HttpResponse(responseCode, convertHeaders(openConnection.getHeaderFields()));
                openConnection.disconnect();
                return httpResponse;
            }
            final int n3 = n2 = 1;
            final List<Header> convertHeaders = convertHeaders(openConnection.getHeaderFields());
            n2 = n3;
            final int contentLength = openConnection.getContentLength();
            n2 = n3;
            n2 = n3;
            final UrlConnectionInputStream urlConnectionInputStream = new UrlConnectionInputStream(openConnection);
            n2 = n3;
            return new HttpResponse(responseCode, convertHeaders, contentLength, urlConnectionInputStream);
        }
        finally {
            if (n2 == 0) {
                openConnection.disconnect();
            }
        }
    }
    
    static class UrlConnectionInputStream extends FilterInputStream
    {
        private final HttpURLConnection mConnection;
        
        UrlConnectionInputStream(final HttpURLConnection mConnection) {
            super(inputStreamFromConnection(mConnection));
            this.mConnection = mConnection;
        }
        
        @Override
        public void close() throws IOException {
            super.close();
            this.mConnection.disconnect();
        }
    }
    
    public interface UrlRewriter
    {
        String rewriteUrl(final String p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.io.FilterInputStream;
import java.io.BufferedOutputStream;
import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.Collections;
import com.android.volley.Header;
import java.util.List;
import java.io.IOException;
import java.io.EOFException;
import java.io.InputStream;
import java.util.Iterator;
import android.os.SystemClock;
import com.android.volley.VolleyLog;
import java.util.LinkedHashMap;
import java.io.File;
import java.util.Map;
import com.android.volley.Cache;

public class DiskBasedCache implements Cache
{
    private static final int CACHE_MAGIC = 538247942;
    private static final int DEFAULT_DISK_USAGE_BYTES = 5242880;
    private static final float HYSTERESIS_FACTOR = 0.9f;
    private final Map<String, CacheHeader> mEntries;
    private final int mMaxCacheSizeInBytes;
    private final File mRootDirectory;
    private long mTotalSize;
    
    public DiskBasedCache(final File file) {
        this(file, 5242880);
    }
    
    public DiskBasedCache(final File mRootDirectory, final int mMaxCacheSizeInBytes) {
        this.mEntries = new LinkedHashMap<String, CacheHeader>(16, 0.75f, true);
        this.mTotalSize = 0L;
        this.mRootDirectory = mRootDirectory;
        this.mMaxCacheSizeInBytes = mMaxCacheSizeInBytes;
    }
    
    private String getFilenameForKey(final String s) {
        final int n = s.length() / 2;
        final int hashCode = s.substring(0, n).hashCode();
        final StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(hashCode));
        sb.append(String.valueOf(s.substring(n).hashCode()));
        return sb.toString();
    }
    
    private void pruneIfNeeded(int n) {
        final long mTotalSize = this.mTotalSize;
        final long n2 = n;
        if (mTotalSize + n2 < this.mMaxCacheSizeInBytes) {
            return;
        }
        if (VolleyLog.DEBUG) {
            VolleyLog.v("Pruning old cache entries.", new Object[0]);
        }
        final long mTotalSize2 = this.mTotalSize;
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final Iterator<Map.Entry<String, CacheHeader>> iterator = this.mEntries.entrySet().iterator();
        n = 0;
        int i;
        while (true) {
            i = n;
            if (!iterator.hasNext()) {
                break;
            }
            final CacheHeader cacheHeader = iterator.next().getValue();
            if (this.getFileForKey(cacheHeader.key).delete()) {
                this.mTotalSize -= cacheHeader.size;
            }
            else {
                VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", cacheHeader.key, this.getFilenameForKey(cacheHeader.key));
            }
            iterator.remove();
            ++n;
            if (this.mTotalSize + n2 < this.mMaxCacheSizeInBytes * 0.9f) {
                i = n;
                break;
            }
        }
        if (VolleyLog.DEBUG) {
            VolleyLog.v("pruned %d files, %d bytes, %d ms", i, this.mTotalSize - mTotalSize2, SystemClock.elapsedRealtime() - elapsedRealtime);
        }
    }
    
    private void putEntry(final String s, final CacheHeader cacheHeader) {
        if (!this.mEntries.containsKey(s)) {
            this.mTotalSize += cacheHeader.size;
        }
        else {
            this.mTotalSize += cacheHeader.size - this.mEntries.get(s).size;
        }
        this.mEntries.put(s, cacheHeader);
    }
    
    private static int read(final InputStream inputStream) throws IOException {
        final int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }
    
    static List<Header> readHeaderList(final CountingInputStream countingInputStream) throws IOException {
        final int int1 = readInt(countingInputStream);
        if (int1 >= 0) {
            List<Object> emptyList;
            if (int1 == 0) {
                emptyList = Collections.emptyList();
            }
            else {
                emptyList = new ArrayList<Object>();
            }
            for (int i = 0; i < int1; ++i) {
                emptyList.add(new Header(readString(countingInputStream).intern(), readString(countingInputStream).intern()));
            }
            return (List<Header>)emptyList;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("readHeaderList size=");
        sb.append(int1);
        throw new IOException(sb.toString());
    }
    
    static int readInt(final InputStream inputStream) throws IOException {
        return read(inputStream) << 24 | (read(inputStream) << 0 | 0x0 | read(inputStream) << 8 | read(inputStream) << 16);
    }
    
    static long readLong(final InputStream inputStream) throws IOException {
        return ((long)read(inputStream) & 0xFFL) << 0 | 0x0L | ((long)read(inputStream) & 0xFFL) << 8 | ((long)read(inputStream) & 0xFFL) << 16 | ((long)read(inputStream) & 0xFFL) << 24 | ((long)read(inputStream) & 0xFFL) << 32 | ((long)read(inputStream) & 0xFFL) << 40 | ((long)read(inputStream) & 0xFFL) << 48 | (0xFFL & (long)read(inputStream)) << 56;
    }
    
    static String readString(final CountingInputStream countingInputStream) throws IOException {
        return new String(streamToBytes(countingInputStream, readLong(countingInputStream)), "UTF-8");
    }
    
    private void removeEntry(final String s) {
        final CacheHeader cacheHeader = this.mEntries.remove(s);
        if (cacheHeader != null) {
            this.mTotalSize -= cacheHeader.size;
        }
    }
    
    static byte[] streamToBytes(final CountingInputStream in, final long lng) throws IOException {
        final long bytesRemaining = in.bytesRemaining();
        if (lng >= 0L && lng <= bytesRemaining) {
            final int n = (int)lng;
            if (n == lng) {
                final byte[] b = new byte[n];
                new DataInputStream(in).readFully(b);
                return b;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("streamToBytes length=");
        sb.append(lng);
        sb.append(", maxLength=");
        sb.append(bytesRemaining);
        throw new IOException(sb.toString());
    }
    
    static void writeHeaderList(final List<Header> list, final OutputStream outputStream) throws IOException {
        if (list != null) {
            writeInt(outputStream, list.size());
            for (final Header header : list) {
                writeString(outputStream, header.getName());
                writeString(outputStream, header.getValue());
            }
        }
        else {
            writeInt(outputStream, 0);
        }
    }
    
    static void writeInt(final OutputStream outputStream, final int n) throws IOException {
        outputStream.write(n >> 0 & 0xFF);
        outputStream.write(n >> 8 & 0xFF);
        outputStream.write(n >> 16 & 0xFF);
        outputStream.write(n >> 24 & 0xFF);
    }
    
    static void writeLong(final OutputStream outputStream, final long n) throws IOException {
        outputStream.write((byte)(n >>> 0));
        outputStream.write((byte)(n >>> 8));
        outputStream.write((byte)(n >>> 16));
        outputStream.write((byte)(n >>> 24));
        outputStream.write((byte)(n >>> 32));
        outputStream.write((byte)(n >>> 40));
        outputStream.write((byte)(n >>> 48));
        outputStream.write((byte)(n >>> 56));
    }
    
    static void writeString(final OutputStream outputStream, final String s) throws IOException {
        final byte[] bytes = s.getBytes("UTF-8");
        writeLong(outputStream, bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }
    
    @Override
    public void clear() {
        synchronized (this) {
            final File[] listFiles = this.mRootDirectory.listFiles();
            if (listFiles != null) {
                for (int length = listFiles.length, i = 0; i < length; ++i) {
                    listFiles[i].delete();
                }
            }
            this.mEntries.clear();
            this.mTotalSize = 0L;
            VolleyLog.d("Cache cleared.", new Object[0]);
        }
    }
    
    InputStream createInputStream(final File file) throws FileNotFoundException {
        return new FileInputStream(file);
    }
    
    OutputStream createOutputStream(final File file) throws FileNotFoundException {
        return new FileOutputStream(file);
    }
    
    @Override
    public Entry get(final String s) {
        synchronized (this) {
            final CacheHeader cacheHeader = this.mEntries.get(s);
            if (cacheHeader == null) {
                return null;
            }
            final File fileForKey = this.getFileForKey(s);
            try {
                final CountingInputStream countingInputStream = new CountingInputStream(new BufferedInputStream(this.createInputStream(fileForKey)), fileForKey.length());
                try {
                    final CacheHeader header = CacheHeader.readHeader(countingInputStream);
                    if (!TextUtils.equals((CharSequence)s, (CharSequence)header.key)) {
                        VolleyLog.d("%s: key=%s, found=%s", fileForKey.getAbsolutePath(), s, header.key);
                        this.removeEntry(s);
                        return null;
                    }
                    return cacheHeader.toCacheEntry(streamToBytes(countingInputStream, countingInputStream.bytesRemaining()));
                }
                finally {
                    countingInputStream.close();
                }
            }
            catch (final IOException ex) {
                VolleyLog.d("%s: %s", fileForKey.getAbsolutePath(), ex.toString());
                this.remove(s);
                return null;
            }
        }
    }
    
    public File getFileForKey(final String s) {
        return new File(this.mRootDirectory, this.getFilenameForKey(s));
    }
    
    @Override
    public void initialize() {
        synchronized (this) {
            final boolean exists = this.mRootDirectory.exists();
            int i = 0;
            if (!exists) {
                if (!this.mRootDirectory.mkdirs()) {
                    VolleyLog.e("Unable to create cache dir %s", this.mRootDirectory.getAbsolutePath());
                }
                return;
            }
            final File[] listFiles = this.mRootDirectory.listFiles();
            if (listFiles == null) {
                return;
            }
            while (i < listFiles.length) {
                final File file = listFiles[i];
                try {
                    final long length = file.length();
                    final CountingInputStream countingInputStream = new CountingInputStream(new BufferedInputStream(this.createInputStream(file)), length);
                    try {
                        final CacheHeader header = CacheHeader.readHeader(countingInputStream);
                        header.size = length;
                        this.putEntry(header.key, header);
                    }
                    finally {
                        countingInputStream.close();
                    }
                }
                catch (final IOException ex) {
                    file.delete();
                }
                ++i;
            }
        }
    }
    
    @Override
    public void invalidate(final String s, final boolean b) {
        synchronized (this) {
            final Entry value = this.get(s);
            if (value != null) {
                value.softTtl = 0L;
                if (b) {
                    value.ttl = 0L;
                }
                this.put(s, value);
            }
        }
    }
    
    @Override
    public void put(final String s, final Entry entry) {
        synchronized (this) {
            this.pruneIfNeeded(entry.data.length);
            final File fileForKey = this.getFileForKey(s);
            try {
                final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(this.createOutputStream(fileForKey));
                final CacheHeader cacheHeader = new CacheHeader(s, entry);
                if (cacheHeader.writeHeader(bufferedOutputStream)) {
                    bufferedOutputStream.write(entry.data);
                    bufferedOutputStream.close();
                    this.putEntry(s, cacheHeader);
                    return;
                }
                bufferedOutputStream.close();
                VolleyLog.d("Failed to write header for %s", fileForKey.getAbsolutePath());
                throw new IOException();
            }
            catch (final IOException ex) {
                if (!fileForKey.delete()) {
                    VolleyLog.d("Could not clean up file %s", fileForKey.getAbsolutePath());
                }
            }
        }
    }
    
    @Override
    public void remove(final String s) {
        synchronized (this) {
            final boolean delete = this.getFileForKey(s).delete();
            this.removeEntry(s);
            if (!delete) {
                VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", s, this.getFilenameForKey(s));
            }
        }
    }
    
    static class CacheHeader
    {
        final List<Header> allResponseHeaders;
        final String etag;
        final String key;
        final long lastModified;
        final long serverDate;
        long size;
        final long softTtl;
        final long ttl;
        
        CacheHeader(final String s, final Entry entry) {
            this(s, entry.etag, entry.serverDate, entry.lastModified, entry.ttl, entry.softTtl, getAllResponseHeaders(entry));
            this.size = entry.data.length;
        }
        
        private CacheHeader(String s, final String anObject, final long serverDate, final long lastModified, final long ttl, final long softTtl, final List<Header> allResponseHeaders) {
            this.key = s;
            s = anObject;
            if ("".equals(anObject)) {
                s = null;
            }
            this.etag = s;
            this.serverDate = serverDate;
            this.lastModified = lastModified;
            this.ttl = ttl;
            this.softTtl = softTtl;
            this.allResponseHeaders = allResponseHeaders;
        }
        
        private static List<Header> getAllResponseHeaders(final Entry entry) {
            if (entry.allResponseHeaders != null) {
                return entry.allResponseHeaders;
            }
            return HttpHeaderParser.toAllHeaderList(entry.responseHeaders);
        }
        
        static CacheHeader readHeader(final CountingInputStream countingInputStream) throws IOException {
            if (DiskBasedCache.readInt(countingInputStream) == 538247942) {
                return new CacheHeader(DiskBasedCache.readString(countingInputStream), DiskBasedCache.readString(countingInputStream), DiskBasedCache.readLong(countingInputStream), DiskBasedCache.readLong(countingInputStream), DiskBasedCache.readLong(countingInputStream), DiskBasedCache.readLong(countingInputStream), DiskBasedCache.readHeaderList(countingInputStream));
            }
            throw new IOException();
        }
        
        Entry toCacheEntry(final byte[] data) {
            final Entry entry = new Entry();
            entry.data = data;
            entry.etag = this.etag;
            entry.serverDate = this.serverDate;
            entry.lastModified = this.lastModified;
            entry.ttl = this.ttl;
            entry.softTtl = this.softTtl;
            entry.responseHeaders = HttpHeaderParser.toHeaderMap(this.allResponseHeaders);
            entry.allResponseHeaders = Collections.unmodifiableList((List<? extends Header>)this.allResponseHeaders);
            return entry;
        }
        
        boolean writeHeader(final OutputStream outputStream) {
            try {
                DiskBasedCache.writeInt(outputStream, 538247942);
                DiskBasedCache.writeString(outputStream, this.key);
                String etag;
                if ((etag = this.etag) == null) {
                    etag = "";
                }
                DiskBasedCache.writeString(outputStream, etag);
                DiskBasedCache.writeLong(outputStream, this.serverDate);
                DiskBasedCache.writeLong(outputStream, this.lastModified);
                DiskBasedCache.writeLong(outputStream, this.ttl);
                DiskBasedCache.writeLong(outputStream, this.softTtl);
                DiskBasedCache.writeHeaderList(this.allResponseHeaders, outputStream);
                outputStream.flush();
                return true;
            }
            catch (final IOException ex) {
                VolleyLog.d("%s", ex.toString());
                return false;
            }
        }
    }
    
    static class CountingInputStream extends FilterInputStream
    {
        private long bytesRead;
        private final long length;
        
        CountingInputStream(final InputStream in, final long length) {
            super(in);
            this.length = length;
        }
        
        long bytesRead() {
            return this.bytesRead;
        }
        
        long bytesRemaining() {
            return this.length - this.bytesRead;
        }
        
        @Override
        public int read() throws IOException {
            final int read = super.read();
            if (read != -1) {
                ++this.bytesRead;
            }
            return read;
        }
        
        @Override
        public int read(final byte[] b, int read, final int len) throws IOException {
            read = super.read(b, read, len);
            if (read != -1) {
                this.bytesRead += read;
            }
            return read;
        }
    }
}

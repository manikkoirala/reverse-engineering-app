// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.io.UnsupportedEncodingException;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.Request;

public class StringRequest extends Request<String>
{
    private Response.Listener<String> mListener;
    private final Object mLock;
    
    public StringRequest(final int n, final String s, final Response.Listener<String> mListener, final Response.ErrorListener errorListener) {
        super(n, s, errorListener);
        this.mLock = new Object();
        this.mListener = mListener;
    }
    
    public StringRequest(final String s, final Response.Listener<String> listener, final Response.ErrorListener errorListener) {
        this(0, s, listener, errorListener);
    }
    
    @Override
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }
    
    @Override
    protected void deliverResponse(final String s) {
        synchronized (this.mLock) {
            final Response.Listener<String> mListener = this.mListener;
            monitorexit(this.mLock);
            if (mListener != null) {
                mListener.onResponse(s);
            }
        }
    }
    
    @Override
    protected Response<String> parseNetworkResponse(final NetworkResponse networkResponse) {
        String s;
        try {
            s = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
        }
        catch (final UnsupportedEncodingException ex) {
            s = new String(networkResponse.data);
        }
        return Response.success(s, HttpHeaderParser.parseCacheHeaders(networkResponse));
    }
}

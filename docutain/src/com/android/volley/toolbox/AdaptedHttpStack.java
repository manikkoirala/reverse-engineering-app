// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import com.android.volley.AuthFailureError;
import org.apache.http.conn.ConnectTimeoutException;
import java.net.SocketTimeoutException;
import java.io.IOException;
import java.util.List;
import com.android.volley.Header;
import java.util.ArrayList;
import java.util.Map;
import com.android.volley.Request;

class AdaptedHttpStack extends BaseHttpStack
{
    private final HttpStack mHttpStack;
    
    AdaptedHttpStack(final HttpStack mHttpStack) {
        this.mHttpStack = mHttpStack;
    }
    
    @Override
    public HttpResponse executeRequest(final Request<?> request, final Map<String, String> map) throws IOException, AuthFailureError {
        try {
            final org.apache.http.HttpResponse performRequest = this.mHttpStack.performRequest(request, map);
            final int statusCode = performRequest.getStatusLine().getStatusCode();
            final org.apache.http.Header[] allHeaders = performRequest.getAllHeaders();
            final ArrayList list = new ArrayList(allHeaders.length);
            for (final org.apache.http.Header header : allHeaders) {
                list.add((Object)new Header(header.getName(), header.getValue()));
            }
            if (performRequest.getEntity() == null) {
                return new HttpResponse(statusCode, (List<Header>)list);
            }
            final long contentLength = performRequest.getEntity().getContentLength();
            if ((int)contentLength == contentLength) {
                return new HttpResponse(statusCode, (List<Header>)list, (int)performRequest.getEntity().getContentLength(), performRequest.getEntity().getContent());
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Response too large: ");
            sb.append(contentLength);
            throw new IOException(sb.toString());
        }
        catch (final ConnectTimeoutException ex) {
            throw new SocketTimeoutException(ex.getMessage());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import android.content.pm.PackageInfo;
import org.apache.http.client.HttpClient;
import android.net.http.AndroidHttpClient;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.Build$VERSION;
import com.android.volley.Cache;
import java.io.File;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import android.content.Context;

public class Volley
{
    private static final String DEFAULT_CACHE_DIR = "volley";
    
    public static RequestQueue newRequestQueue(final Context context) {
        final BaseHttpStack baseHttpStack = null;
        return newRequestQueue(context, (BaseHttpStack)null);
    }
    
    private static RequestQueue newRequestQueue(final Context context, final Network network) {
        final RequestQueue requestQueue = new RequestQueue(new DiskBasedCache(new File(context.getCacheDir(), "volley")), network);
        requestQueue.start();
        return requestQueue;
    }
    
    public static RequestQueue newRequestQueue(final Context context, final BaseHttpStack baseHttpStack) {
        BasicNetwork basicNetwork;
        if (baseHttpStack == null) {
            if (Build$VERSION.SDK_INT >= 9) {
                basicNetwork = new BasicNetwork(new HurlStack());
            }
            else {
                String string;
                try {
                    final String packageName = context.getPackageName();
                    final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                    final StringBuilder sb = new StringBuilder();
                    sb.append(packageName);
                    sb.append("/");
                    sb.append(packageInfo.versionCode);
                    string = sb.toString();
                }
                catch (final PackageManager$NameNotFoundException ex) {
                    string = "volley/0";
                }
                basicNetwork = new BasicNetwork(new HttpClientStack((HttpClient)AndroidHttpClient.newInstance(string)));
            }
        }
        else {
            basicNetwork = new BasicNetwork(baseHttpStack);
        }
        return newRequestQueue(context, basicNetwork);
    }
    
    @Deprecated
    public static RequestQueue newRequestQueue(final Context context, final HttpStack httpStack) {
        if (httpStack == null) {
            final BaseHttpStack baseHttpStack = null;
            return newRequestQueue(context, (BaseHttpStack)null);
        }
        return newRequestQueue(context, new BasicNetwork(httpStack));
    }
}

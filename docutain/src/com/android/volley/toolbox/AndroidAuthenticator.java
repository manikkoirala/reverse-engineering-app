// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import android.accounts.AccountManagerFuture;
import com.android.volley.AuthFailureError;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.accounts.AccountManagerCallback;
import android.content.Context;
import android.accounts.AccountManager;
import android.accounts.Account;

public class AndroidAuthenticator implements Authenticator
{
    private final Account mAccount;
    private final AccountManager mAccountManager;
    private final String mAuthTokenType;
    private final boolean mNotifyAuthFailure;
    
    AndroidAuthenticator(final AccountManager mAccountManager, final Account mAccount, final String mAuthTokenType, final boolean mNotifyAuthFailure) {
        this.mAccountManager = mAccountManager;
        this.mAccount = mAccount;
        this.mAuthTokenType = mAuthTokenType;
        this.mNotifyAuthFailure = mNotifyAuthFailure;
    }
    
    public AndroidAuthenticator(final Context context, final Account account, final String s) {
        this(context, account, s, false);
    }
    
    public AndroidAuthenticator(final Context context, final Account account, final String s, final boolean b) {
        this(AccountManager.get(context), account, s, b);
    }
    
    public Account getAccount() {
        return this.mAccount;
    }
    
    @Override
    public String getAuthToken() throws AuthFailureError {
        final AccountManagerFuture authToken = this.mAccountManager.getAuthToken(this.mAccount, this.mAuthTokenType, this.mNotifyAuthFailure, (AccountManagerCallback)null, (Handler)null);
        try {
            final Bundle bundle = (Bundle)authToken.getResult();
            String string;
            final String s = string = null;
            if (authToken.isDone()) {
                string = s;
                if (!authToken.isCancelled()) {
                    if (bundle.containsKey("intent")) {
                        throw new AuthFailureError((Intent)bundle.getParcelable("intent"));
                    }
                    string = bundle.getString("authtoken");
                }
            }
            if (string != null) {
                return string;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Got null auth token for type: ");
            sb.append(this.mAuthTokenType);
            throw new AuthFailureError(sb.toString());
        }
        catch (final Exception ex) {
            throw new AuthFailureError("Error while retrieving auth token", ex);
        }
    }
    
    public String getAuthTokenType() {
        return this.mAuthTokenType;
    }
    
    @Override
    public void invalidateAuthToken(final String s) {
        this.mAccountManager.invalidateAuthToken(this.mAccount.type, s);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import com.android.volley.NetworkResponse;
import java.io.UnsupportedEncodingException;
import com.android.volley.VolleyLog;
import com.android.volley.Response;
import com.android.volley.Request;

public abstract class JsonRequest<T> extends Request<T>
{
    protected static final String PROTOCOL_CHARSET = "utf-8";
    private static final String PROTOCOL_CONTENT_TYPE;
    private Response.Listener<T> mListener;
    private final Object mLock;
    private final String mRequestBody;
    
    static {
        PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", "utf-8");
    }
    
    public JsonRequest(final int n, final String s, final String mRequestBody, final Response.Listener<T> mListener, final Response.ErrorListener errorListener) {
        super(n, s, errorListener);
        this.mLock = new Object();
        this.mListener = mListener;
        this.mRequestBody = mRequestBody;
    }
    
    @Deprecated
    public JsonRequest(final String s, final String s2, final Response.Listener<T> listener, final Response.ErrorListener errorListener) {
        this(-1, s, s2, listener, errorListener);
    }
    
    @Override
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }
    
    @Override
    protected void deliverResponse(final T t) {
        synchronized (this.mLock) {
            final Response.Listener<T> mListener = this.mListener;
            monitorexit(this.mLock);
            if (mListener != null) {
                mListener.onResponse(t);
            }
        }
    }
    
    @Override
    public byte[] getBody() {
        byte[] bytes = null;
        try {
            final String mRequestBody = this.mRequestBody;
            if (mRequestBody != null) {
                bytes = mRequestBody.getBytes("utf-8");
            }
            return bytes;
        }
        catch (final UnsupportedEncodingException ex) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", this.mRequestBody, "utf-8");
            return null;
        }
    }
    
    @Override
    public String getBodyContentType() {
        return JsonRequest.PROTOCOL_CONTENT_TYPE;
    }
    
    @Deprecated
    @Override
    public byte[] getPostBody() {
        return this.getBody();
    }
    
    @Deprecated
    @Override
    public String getPostBodyContentType() {
        return this.getBodyContentType();
    }
    
    @Override
    protected abstract Response<T> parseNetworkResponse(final NetworkResponse p0);
}

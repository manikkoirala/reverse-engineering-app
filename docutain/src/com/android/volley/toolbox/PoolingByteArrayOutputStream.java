// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.io.IOException;
import java.io.ByteArrayOutputStream;

public class PoolingByteArrayOutputStream extends ByteArrayOutputStream
{
    private static final int DEFAULT_SIZE = 256;
    private final ByteArrayPool mPool;
    
    public PoolingByteArrayOutputStream(final ByteArrayPool byteArrayPool) {
        this(byteArrayPool, 256);
    }
    
    public PoolingByteArrayOutputStream(final ByteArrayPool mPool, final int a) {
        this.mPool = mPool;
        this.buf = mPool.getBuf(Math.max(a, 256));
    }
    
    private void expand(final int n) {
        if (this.count + n <= this.buf.length) {
            return;
        }
        final byte[] buf = this.mPool.getBuf((this.count + n) * 2);
        System.arraycopy(this.buf, 0, buf, 0, this.count);
        this.mPool.returnBuf(this.buf);
        this.buf = buf;
    }
    
    @Override
    public void close() throws IOException {
        this.mPool.returnBuf(this.buf);
        this.buf = null;
        super.close();
    }
    
    public void finalize() {
        this.mPool.returnBuf(this.buf);
    }
    
    @Override
    public void write(final int b) {
        synchronized (this) {
            this.expand(1);
            super.write(b);
        }
    }
    
    @Override
    public void write(final byte[] b, final int off, final int len) {
        synchronized (this) {
            this.expand(len);
            super.write(b, off, len);
        }
    }
}

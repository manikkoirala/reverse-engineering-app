// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.util.Collections;
import com.android.volley.Header;
import java.util.List;
import java.io.InputStream;

public final class HttpResponse
{
    private final InputStream mContent;
    private final int mContentLength;
    private final List<Header> mHeaders;
    private final int mStatusCode;
    
    public HttpResponse(final int n, final List<Header> list) {
        this(n, list, -1, null);
    }
    
    public HttpResponse(final int mStatusCode, final List<Header> mHeaders, final int mContentLength, final InputStream mContent) {
        this.mStatusCode = mStatusCode;
        this.mHeaders = mHeaders;
        this.mContentLength = mContentLength;
        this.mContent = mContent;
    }
    
    public final InputStream getContent() {
        return this.mContent;
    }
    
    public final int getContentLength() {
        return this.mContentLength;
    }
    
    public final List<Header> getHeaders() {
        return Collections.unmodifiableList((List<? extends Header>)this.mHeaders);
    }
    
    public final int getStatusCode() {
        return this.mStatusCode;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.net.URI;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.HttpResponse;
import java.io.IOException;
import java.util.Iterator;
import org.apache.http.message.BasicNameValuePair;
import java.util.ArrayList;
import org.apache.http.NameValuePair;
import java.util.List;
import com.android.volley.AuthFailureError;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import java.util.Map;
import com.android.volley.Request;
import org.apache.http.client.HttpClient;

@Deprecated
public class HttpClientStack implements HttpStack
{
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    protected final HttpClient mClient;
    
    public HttpClientStack(final HttpClient mClient) {
        this.mClient = mClient;
    }
    
    static HttpUriRequest createHttpRequest(final Request<?> request, final Map<String, String> map) throws AuthFailureError {
        switch (request.getMethod()) {
            default: {
                throw new IllegalStateException("Unknown request method.");
            }
            case 7: {
                final HttpPatch httpPatch = new HttpPatch(request.getUrl());
                httpPatch.addHeader("Content-Type", request.getBodyContentType());
                setEntityIfNonEmptyBody(httpPatch, request);
                return (HttpUriRequest)httpPatch;
            }
            case 6: {
                return (HttpUriRequest)new HttpTrace(request.getUrl());
            }
            case 5: {
                return (HttpUriRequest)new HttpOptions(request.getUrl());
            }
            case 4: {
                return (HttpUriRequest)new HttpHead(request.getUrl());
            }
            case 3: {
                return (HttpUriRequest)new HttpDelete(request.getUrl());
            }
            case 2: {
                final HttpPut httpPut = new HttpPut(request.getUrl());
                httpPut.addHeader("Content-Type", request.getBodyContentType());
                setEntityIfNonEmptyBody((HttpEntityEnclosingRequestBase)httpPut, request);
                return (HttpUriRequest)httpPut;
            }
            case 1: {
                final HttpPost httpPost = new HttpPost(request.getUrl());
                httpPost.addHeader("Content-Type", request.getBodyContentType());
                setEntityIfNonEmptyBody((HttpEntityEnclosingRequestBase)httpPost, request);
                return (HttpUriRequest)httpPost;
            }
            case 0: {
                return (HttpUriRequest)new HttpGet(request.getUrl());
            }
            case -1: {
                final byte[] postBody = request.getPostBody();
                if (postBody != null) {
                    final HttpPost httpPost2 = new HttpPost(request.getUrl());
                    httpPost2.addHeader("Content-Type", request.getPostBodyContentType());
                    httpPost2.setEntity((HttpEntity)new ByteArrayEntity(postBody));
                    return (HttpUriRequest)httpPost2;
                }
                return (HttpUriRequest)new HttpGet(request.getUrl());
            }
        }
    }
    
    private static List<NameValuePair> getPostParameterPairs(final Map<String, String> map) {
        final ArrayList list = new ArrayList(map.size());
        for (final String s : map.keySet()) {
            list.add(new BasicNameValuePair(s, (String)map.get(s)));
        }
        return list;
    }
    
    private static void setEntityIfNonEmptyBody(final HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, final Request<?> request) throws AuthFailureError {
        final byte[] body = request.getBody();
        if (body != null) {
            httpEntityEnclosingRequestBase.setEntity((HttpEntity)new ByteArrayEntity(body));
        }
    }
    
    private static void setHeaders(final HttpUriRequest httpUriRequest, final Map<String, String> map) {
        for (final String s : map.keySet()) {
            httpUriRequest.setHeader(s, (String)map.get(s));
        }
    }
    
    protected void onPrepareRequest(final HttpUriRequest httpUriRequest) throws IOException {
    }
    
    @Override
    public HttpResponse performRequest(final Request<?> request, final Map<String, String> map) throws IOException, AuthFailureError {
        final HttpUriRequest httpRequest = createHttpRequest(request, map);
        setHeaders(httpRequest, map);
        setHeaders(httpRequest, request.getHeaders());
        this.onPrepareRequest(httpRequest);
        final HttpParams params = httpRequest.getParams();
        final int timeoutMs = request.getTimeoutMs();
        HttpConnectionParams.setConnectionTimeout(params, 5000);
        HttpConnectionParams.setSoTimeout(params, timeoutMs);
        return this.mClient.execute(httpRequest);
    }
    
    public static final class HttpPatch extends HttpEntityEnclosingRequestBase
    {
        public static final String METHOD_NAME = "PATCH";
        
        public HttpPatch() {
        }
        
        public HttpPatch(final String str) {
            this.setURI(URI.create(str));
        }
        
        public HttpPatch(final URI uri) {
            this.setURI(uri);
        }
        
        public String getMethod() {
            return "PATCH";
        }
    }
}

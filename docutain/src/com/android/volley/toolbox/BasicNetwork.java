// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.net.SocketTimeoutException;
import com.android.volley.TimeoutError;
import java.net.MalformedURLException;
import com.android.volley.NetworkError;
import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NoConnectionError;
import com.android.volley.NetworkResponse;
import android.os.SystemClock;
import java.lang.constant.Constable;
import com.android.volley.ServerError;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Collections;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;
import com.android.volley.Cache;
import com.android.volley.Header;
import java.util.List;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.Network;

public class BasicNetwork implements Network
{
    protected static final boolean DEBUG;
    private static final int DEFAULT_POOL_SIZE = 4096;
    private static final int SLOW_REQUEST_THRESHOLD_MS = 3000;
    private final BaseHttpStack mBaseHttpStack;
    @Deprecated
    protected final HttpStack mHttpStack;
    protected final ByteArrayPool mPool;
    
    static {
        DEBUG = VolleyLog.DEBUG;
    }
    
    public BasicNetwork(final BaseHttpStack baseHttpStack) {
        this(baseHttpStack, new ByteArrayPool(4096));
    }
    
    public BasicNetwork(final BaseHttpStack baseHttpStack, final ByteArrayPool mPool) {
        this.mBaseHttpStack = baseHttpStack;
        this.mHttpStack = baseHttpStack;
        this.mPool = mPool;
    }
    
    @Deprecated
    public BasicNetwork(final HttpStack httpStack) {
        this(httpStack, new ByteArrayPool(4096));
    }
    
    @Deprecated
    public BasicNetwork(final HttpStack mHttpStack, final ByteArrayPool mPool) {
        this.mHttpStack = mHttpStack;
        this.mBaseHttpStack = new AdaptedHttpStack(mHttpStack);
        this.mPool = mPool;
    }
    
    private static void attemptRetryOnException(final String s, final Request<?> request, final VolleyError volleyError) throws VolleyError {
        final RetryPolicy retryPolicy = request.getRetryPolicy();
        final int timeoutMs = request.getTimeoutMs();
        try {
            retryPolicy.retry(volleyError);
            request.addMarker(String.format("%s-retry [timeout=%s]", s, timeoutMs));
        }
        catch (final VolleyError volleyError) {
            request.addMarker(String.format("%s-timeout-giveup [timeout=%s]", s, timeoutMs));
            throw volleyError;
        }
    }
    
    private static List<Header> combineHeaders(final List<Header> c, final Cache.Entry entry) {
        final TreeSet set = new TreeSet((Comparator<? super E>)String.CASE_INSENSITIVE_ORDER);
        if (!c.isEmpty()) {
            final Iterator iterator = c.iterator();
            while (iterator.hasNext()) {
                set.add(((Header)iterator.next()).getName());
            }
        }
        final ArrayList list = new ArrayList(c);
        if (entry.allResponseHeaders != null) {
            if (!entry.allResponseHeaders.isEmpty()) {
                for (final Header header : entry.allResponseHeaders) {
                    if (!set.contains(header.getName())) {
                        list.add(header);
                    }
                }
            }
        }
        else if (!entry.responseHeaders.isEmpty()) {
            for (final Map.Entry entry2 : entry.responseHeaders.entrySet()) {
                if (!set.contains(entry2.getKey())) {
                    list.add(new Header((String)entry2.getKey(), (String)entry2.getValue()));
                }
            }
        }
        return list;
    }
    
    @Deprecated
    protected static Map<String, String> convertHeaders(final Header[] array) {
        final TreeMap treeMap = new TreeMap((Comparator<? super K>)String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < array.length; ++i) {
            treeMap.put(array[i].getName(), array[i].getValue());
        }
        return treeMap;
    }
    
    private Map<String, String> getCacheHeaders(final Cache.Entry entry) {
        if (entry == null) {
            return Collections.emptyMap();
        }
        final HashMap hashMap = new HashMap();
        if (entry.etag != null) {
            hashMap.put("If-None-Match", entry.etag);
        }
        if (entry.lastModified > 0L) {
            hashMap.put("If-Modified-Since", HttpHeaderParser.formatEpochAsRfc1123(entry.lastModified));
        }
        return hashMap;
    }
    
    private byte[] inputStreamToBytes(final InputStream ex, int read) throws IOException, ServerError {
        final PoolingByteArrayOutputStream poolingByteArrayOutputStream = new PoolingByteArrayOutputStream(this.mPool, read);
        final byte[] array = null;
        Label_0119: {
            if (ex == null) {
                break Label_0119;
            }
            byte[] array2 = array;
            try {
                final byte[] buf = this.mPool.getBuf(1024);
                while (true) {
                    array2 = buf;
                    read = ((InputStream)ex).read(buf);
                    if (read == -1) {
                        break;
                    }
                    array2 = buf;
                    poolingByteArrayOutputStream.write(buf, 0, read);
                }
                array2 = buf;
                final byte[] byteArray = poolingByteArrayOutputStream.toByteArray();
                if (ex != null) {
                    try {
                        ((InputStream)ex).close();
                    }
                    catch (final IOException ex) {
                        VolleyLog.v("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.mPool.returnBuf(buf);
                poolingByteArrayOutputStream.close();
                return byteArray;
            }
            finally {
                if (ex != null) {
                    try {
                        ((InputStream)ex).close();
                    }
                    catch (final IOException ex2) {
                        VolleyLog.v("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.mPool.returnBuf(array2);
                poolingByteArrayOutputStream.close();
                throw new ServerError();
            }
        }
    }
    
    private void logSlowRequests(final long l, final Request<?> request, final byte[] array, final int i) {
        if (BasicNetwork.DEBUG || l > 3000L) {
            Constable value;
            if (array != null) {
                value = array.length;
            }
            else {
                value = "null";
            }
            VolleyLog.d("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", request, l, value, i, request.getRetryPolicy().getCurrentRetryCount());
        }
    }
    
    protected void logError(final String s, final String s2, final long n) {
        VolleyLog.v("HTTP ERROR(%s) %d ms to fetch %s", s, SystemClock.elapsedRealtime() - n, s2);
    }
    
    @Override
    public NetworkResponse performRequest(final Request<?> request) throws VolleyError {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            Object o = Collections.emptyList();
            Object headers = null;
            try {
                Object executeRequest = null;
                Object cacheEntry = null;
                Label_0329: {
                    try {
                        executeRequest = this.mBaseHttpStack.executeRequest(request, this.getCacheHeaders(request.getCacheEntry()));
                        try {
                            final int statusCode = ((HttpResponse)executeRequest).getStatusCode();
                            headers = ((HttpResponse)executeRequest).getHeaders();
                            if (statusCode == 304) {
                                try {
                                    cacheEntry = request.getCacheEntry();
                                    if (cacheEntry == null) {
                                        return new NetworkResponse(304, null, true, SystemClock.elapsedRealtime() - elapsedRealtime, (List<Header>)headers);
                                    }
                                    return new NetworkResponse(304, ((Cache.Entry)cacheEntry).data, true, SystemClock.elapsedRealtime() - elapsedRealtime, combineHeaders((List<Header>)headers, (Cache.Entry)cacheEntry));
                                }
                                catch (IOException o) {
                                    final Cache.Entry entry = null;
                                    final HttpResponse httpResponse = (HttpResponse)executeRequest;
                                    cacheEntry = headers;
                                    executeRequest = o;
                                    headers = httpResponse;
                                    o = entry;
                                    break Label_0329;
                                }
                            }
                            try {
                                o = ((HttpResponse)executeRequest).getContent();
                                if (o != null) {
                                    o = this.inputStreamToBytes((InputStream)o, ((HttpResponse)executeRequest).getContentLength());
                                }
                                else {
                                    o = new byte[0];
                                }
                                try {
                                    this.logSlowRequests(SystemClock.elapsedRealtime() - elapsedRealtime, request, (byte[])o, statusCode);
                                    Label_0236: {
                                        if (statusCode < 200 || statusCode > 299) {
                                            break Label_0236;
                                        }
                                        final long elapsedRealtime2 = SystemClock.elapsedRealtime();
                                        try {
                                            return new NetworkResponse(statusCode, (byte[])o, false, elapsedRealtime2 - elapsedRealtime, (List<Header>)headers);
                                            throw new IOException();
                                        }
                                        catch (final IOException cacheEntry) {}
                                    }
                                }
                                catch (final IOException ex) {}
                                final List<Header> list = (List<Header>)headers;
                                headers = executeRequest;
                                executeRequest = cacheEntry;
                                cacheEntry = list;
                            }
                            catch (final IOException cacheEntry) {
                                o = headers;
                                headers = cacheEntry;
                            }
                        }
                        catch (final IOException ex2) {}
                        cacheEntry = o;
                        final byte[] array = null;
                        o = executeRequest;
                        executeRequest = headers;
                        headers = o;
                        o = array;
                    }
                    catch (final IOException executeRequest) {
                        final byte[] array2 = null;
                        cacheEntry = o;
                        o = array2;
                    }
                }
                if (headers == null) {
                    throw new NoConnectionError((Throwable)executeRequest);
                }
                final int statusCode2 = ((HttpResponse)headers).getStatusCode();
                VolleyLog.e("Unexpected response code %d for %s", statusCode2, request.getUrl());
                if (o != null) {
                    final NetworkResponse networkResponse = new NetworkResponse(statusCode2, (byte[])o, false, SystemClock.elapsedRealtime() - elapsedRealtime, (List<Header>)cacheEntry);
                    if (statusCode2 != 401 && statusCode2 != 403) {
                        if (statusCode2 >= 400 && statusCode2 <= 499) {
                            throw new ClientError(networkResponse);
                        }
                        if (statusCode2 < 500 || statusCode2 > 599) {
                            throw new ServerError(networkResponse);
                        }
                        if (!request.shouldRetryServerErrors()) {
                            throw new ServerError(networkResponse);
                        }
                        attemptRetryOnException("server", request, new ServerError(networkResponse));
                    }
                    else {
                        attemptRetryOnException("auth", request, new AuthFailureError(networkResponse));
                    }
                }
                else {
                    attemptRetryOnException("network", request, new NetworkError());
                }
            }
            catch (final MalformedURLException cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Bad URL ");
                sb.append(request.getUrl());
                throw new RuntimeException(sb.toString(), cause);
            }
            catch (final SocketTimeoutException ex3) {
                attemptRetryOnException("socket", request, new TimeoutError());
            }
        }
    }
}

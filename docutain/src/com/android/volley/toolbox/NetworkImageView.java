// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import android.view.ViewGroup$LayoutParams;
import android.widget.ImageView$ScaleType;
import com.android.volley.VolleyError;
import android.text.TextUtils;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.ImageView;

public class NetworkImageView extends ImageView
{
    private int mDefaultImageId;
    private int mErrorImageId;
    private ImageLoader.ImageContainer mImageContainer;
    private ImageLoader mImageLoader;
    private String mUrl;
    
    public NetworkImageView(final Context context) {
        this(context, null);
    }
    
    public NetworkImageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public NetworkImageView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    private void setDefaultImageOrNull() {
        final int mDefaultImageId = this.mDefaultImageId;
        if (mDefaultImageId != 0) {
            this.setImageResource(mDefaultImageId);
        }
        else {
            this.setImageBitmap((Bitmap)null);
        }
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        this.invalidate();
    }
    
    void loadImageIfNecessary(final boolean b) {
        final int width = this.getWidth();
        final int height = this.getHeight();
        final ImageView$ScaleType scaleType = this.getScaleType();
        final ViewGroup$LayoutParams layoutParams = this.getLayoutParams();
        boolean b2 = true;
        boolean b4 = false;
        int n2 = 0;
        Label_0080: {
            if (layoutParams != null) {
                final boolean b3 = b4 = (this.getLayoutParams().width == -2);
                if (this.getLayoutParams().height == -2) {
                    final int n = 1;
                    b4 = b3;
                    n2 = n;
                    break Label_0080;
                }
            }
            else {
                b4 = false;
            }
            n2 = 0;
        }
        if (!b4 || n2 == 0) {
            b2 = false;
        }
        if (width == 0 && height == 0 && !b2) {
            return;
        }
        if (TextUtils.isEmpty((CharSequence)this.mUrl)) {
            final ImageLoader.ImageContainer mImageContainer = this.mImageContainer;
            if (mImageContainer != null) {
                mImageContainer.cancelRequest();
                this.mImageContainer = null;
            }
            this.setDefaultImageOrNull();
            return;
        }
        final ImageLoader.ImageContainer mImageContainer2 = this.mImageContainer;
        if (mImageContainer2 != null && mImageContainer2.getRequestUrl() != null) {
            if (this.mImageContainer.getRequestUrl().equals(this.mUrl)) {
                return;
            }
            this.mImageContainer.cancelRequest();
            this.setDefaultImageOrNull();
        }
        int n3 = width;
        if (b4) {
            n3 = 0;
        }
        int n4;
        if (n2 != 0) {
            n4 = 0;
        }
        else {
            n4 = height;
        }
        this.mImageContainer = this.mImageLoader.get(this.mUrl, (ImageLoader.ImageListener)new ImageLoader.ImageListener(this, b) {
            final NetworkImageView this$0;
            final boolean val$isInLayoutPass;
            
            @Override
            public void onErrorResponse(final VolleyError volleyError) {
                if (this.this$0.mErrorImageId != 0) {
                    final NetworkImageView this$0 = this.this$0;
                    this$0.setImageResource(this$0.mErrorImageId);
                }
            }
            
            @Override
            public void onResponse(final ImageContainer imageContainer, final boolean b) {
                if (b && this.val$isInLayoutPass) {
                    this.this$0.post((Runnable)new Runnable(this, imageContainer) {
                        final NetworkImageView$1 this$1;
                        final ImageContainer val$response;
                        
                        @Override
                        public void run() {
                            this.this$1.onResponse(this.val$response, false);
                        }
                    });
                    return;
                }
                if (imageContainer.getBitmap() != null) {
                    this.this$0.setImageBitmap(imageContainer.getBitmap());
                }
                else if (this.this$0.mDefaultImageId != 0) {
                    final NetworkImageView this$0 = this.this$0;
                    this$0.setImageResource(this$0.mDefaultImageId);
                }
            }
        }, n3, n4, scaleType);
    }
    
    protected void onDetachedFromWindow() {
        final ImageLoader.ImageContainer mImageContainer = this.mImageContainer;
        if (mImageContainer != null) {
            mImageContainer.cancelRequest();
            this.setImageBitmap((Bitmap)null);
            this.mImageContainer = null;
        }
        super.onDetachedFromWindow();
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        this.loadImageIfNecessary(true);
    }
    
    public void setDefaultImageResId(final int mDefaultImageId) {
        this.mDefaultImageId = mDefaultImageId;
    }
    
    public void setErrorImageResId(final int mErrorImageId) {
        this.mErrorImageId = mErrorImageId;
    }
    
    public void setImageUrl(final String mUrl, final ImageLoader mImageLoader) {
        Threads.throwIfNotOnMainThread();
        this.mUrl = mUrl;
        this.mImageLoader = mImageLoader;
        this.loadImageIfNecessary(false);
    }
}

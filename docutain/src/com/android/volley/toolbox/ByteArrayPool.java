// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;

public class ByteArrayPool
{
    protected static final Comparator<byte[]> BUF_COMPARATOR;
    private final List<byte[]> mBuffersByLastUse;
    private final List<byte[]> mBuffersBySize;
    private int mCurrentSize;
    private final int mSizeLimit;
    
    static {
        BUF_COMPARATOR = new Comparator<byte[]>() {
            @Override
            public int compare(final byte[] array, final byte[] array2) {
                return array.length - array2.length;
            }
        };
    }
    
    public ByteArrayPool(final int mSizeLimit) {
        this.mBuffersByLastUse = new ArrayList<byte[]>();
        this.mBuffersBySize = new ArrayList<byte[]>(64);
        this.mCurrentSize = 0;
        this.mSizeLimit = mSizeLimit;
    }
    
    private void trim() {
        synchronized (this) {
            while (this.mCurrentSize > this.mSizeLimit) {
                final byte[] array = this.mBuffersByLastUse.remove(0);
                this.mBuffersBySize.remove(array);
                this.mCurrentSize -= array.length;
            }
        }
    }
    
    public byte[] getBuf(final int n) {
        monitorenter(this);
        int i = 0;
        try {
            while (i < this.mBuffersBySize.size()) {
                final byte[] array = this.mBuffersBySize.get(i);
                if (array.length >= n) {
                    this.mCurrentSize -= array.length;
                    this.mBuffersBySize.remove(i);
                    this.mBuffersByLastUse.remove(array);
                    return array;
                }
                ++i;
            }
            return new byte[n];
        }
        finally {
            monitorexit(this);
        }
    }
    
    public void returnBuf(final byte[] key) {
        monitorenter(this);
        if (key != null) {
            try {
                if (key.length <= this.mSizeLimit) {
                    this.mBuffersByLastUse.add(key);
                    final int binarySearch = Collections.binarySearch(this.mBuffersBySize, key, ByteArrayPool.BUF_COMPARATOR);
                    int n;
                    if ((n = binarySearch) < 0) {
                        n = -binarySearch - 1;
                    }
                    this.mBuffersBySize.add(n, key);
                    this.mCurrentSize += key.length;
                    this.trim();
                    return;
                }
            }
            finally {
                monitorexit(this);
            }
        }
        monitorexit(this);
    }
}

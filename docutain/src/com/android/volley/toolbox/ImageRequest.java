// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import com.android.volley.VolleyError;
import com.android.volley.ParseError;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory$Options;
import com.android.volley.NetworkResponse;
import com.android.volley.RetryPolicy;
import com.android.volley.DefaultRetryPolicy;
import android.widget.ImageView$ScaleType;
import com.android.volley.Response;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import com.android.volley.Request;

public class ImageRequest extends Request<Bitmap>
{
    public static final float DEFAULT_IMAGE_BACKOFF_MULT = 2.0f;
    public static final int DEFAULT_IMAGE_MAX_RETRIES = 2;
    public static final int DEFAULT_IMAGE_TIMEOUT_MS = 1000;
    private static final Object sDecodeLock;
    private final Bitmap$Config mDecodeConfig;
    private Response.Listener<Bitmap> mListener;
    private final Object mLock;
    private final int mMaxHeight;
    private final int mMaxWidth;
    private final ImageView$ScaleType mScaleType;
    
    static {
        sDecodeLock = new Object();
    }
    
    @Deprecated
    public ImageRequest(final String s, final Response.Listener<Bitmap> listener, final int n, final int n2, final Bitmap$Config bitmap$Config, final Response.ErrorListener errorListener) {
        this(s, listener, n, n2, ImageView$ScaleType.CENTER_INSIDE, bitmap$Config, errorListener);
    }
    
    public ImageRequest(final String s, final Response.Listener<Bitmap> mListener, final int mMaxWidth, final int mMaxHeight, final ImageView$ScaleType mScaleType, final Bitmap$Config mDecodeConfig, final Response.ErrorListener errorListener) {
        super(0, s, errorListener);
        this.mLock = new Object();
        this.setRetryPolicy(new DefaultRetryPolicy(1000, 2, 2.0f));
        this.mListener = mListener;
        this.mDecodeConfig = mDecodeConfig;
        this.mMaxWidth = mMaxWidth;
        this.mMaxHeight = mMaxHeight;
        this.mScaleType = mScaleType;
    }
    
    private Response<Bitmap> doParse(final NetworkResponse networkResponse) {
        final byte[] data = networkResponse.data;
        final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
        Bitmap bitmap = null;
        Label_0205: {
            if (this.mMaxWidth == 0 && this.mMaxHeight == 0) {
                bitmapFactory$Options.inPreferredConfig = this.mDecodeConfig;
                bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, bitmapFactory$Options);
            }
            else {
                bitmapFactory$Options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(data, 0, data.length, bitmapFactory$Options);
                final int outWidth = bitmapFactory$Options.outWidth;
                final int outHeight = bitmapFactory$Options.outHeight;
                final int resizedDimension = getResizedDimension(this.mMaxWidth, this.mMaxHeight, outWidth, outHeight, this.mScaleType);
                final int resizedDimension2 = getResizedDimension(this.mMaxHeight, this.mMaxWidth, outHeight, outWidth, this.mScaleType);
                bitmapFactory$Options.inJustDecodeBounds = false;
                bitmapFactory$Options.inSampleSize = findBestSampleSize(outWidth, outHeight, resizedDimension, resizedDimension2);
                final Bitmap decodeByteArray = BitmapFactory.decodeByteArray(data, 0, data.length, bitmapFactory$Options);
                if ((bitmap = decodeByteArray) != null) {
                    if (decodeByteArray.getWidth() <= resizedDimension) {
                        bitmap = decodeByteArray;
                        if (decodeByteArray.getHeight() <= resizedDimension2) {
                            break Label_0205;
                        }
                    }
                    bitmap = Bitmap.createScaledBitmap(decodeByteArray, resizedDimension, resizedDimension2, true);
                    decodeByteArray.recycle();
                }
            }
        }
        if (bitmap == null) {
            return Response.error(new ParseError(networkResponse));
        }
        return Response.success(bitmap, HttpHeaderParser.parseCacheHeaders(networkResponse));
    }
    
    static int findBestSampleSize(final int n, final int n2, final int n3, final int n4) {
        final double min = Math.min(n / (double)n3, n2 / (double)n4);
        float n5 = 1.0f;
        while (true) {
            final float n6 = 2.0f * n5;
            if (n6 > min) {
                break;
            }
            n5 = n6;
        }
        return (int)n5;
    }
    
    private static int getResizedDimension(int n, final int n2, final int n3, final int n4, final ImageView$ScaleType imageView$ScaleType) {
        if (n == 0 && n2 == 0) {
            return n3;
        }
        if (imageView$ScaleType == ImageView$ScaleType.FIT_XY) {
            if (n == 0) {
                return n3;
            }
            return n;
        }
        else {
            if (n == 0) {
                return (int)(n3 * (n2 / (double)n4));
            }
            if (n2 == 0) {
                return n;
            }
            final double n5 = n4 / (double)n3;
            if (imageView$ScaleType == ImageView$ScaleType.CENTER_CROP) {
                final double n6 = n;
                final double n7 = n2;
                if (n6 * n5 < n7) {
                    n = (int)(n7 / n5);
                }
                return n;
            }
            final double n8 = n;
            final double n9 = n2;
            if (n8 * n5 > n9) {
                n = (int)(n9 / n5);
            }
            return n;
        }
    }
    
    @Override
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }
    
    @Override
    protected void deliverResponse(final Bitmap bitmap) {
        synchronized (this.mLock) {
            final Response.Listener<Bitmap> mListener = this.mListener;
            monitorexit(this.mLock);
            if (mListener != null) {
                mListener.onResponse(bitmap);
            }
        }
    }
    
    @Override
    public Priority getPriority() {
        return Priority.LOW;
    }
    
    @Override
    protected Response<Bitmap> parseNetworkResponse(final NetworkResponse networkResponse) {
        final Object sDecodeLock = ImageRequest.sDecodeLock;
        monitorenter(sDecodeLock);
        try {
            try {
                final Response<Bitmap> doParse = this.doParse(networkResponse);
                monitorexit(sDecodeLock);
                return doParse;
            }
            finally {
                monitorexit(sDecodeLock);
            }
        }
        catch (final OutOfMemoryError outOfMemoryError) {}
    }
}

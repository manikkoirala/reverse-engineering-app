// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.util.Comparator;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.ArrayList;
import com.android.volley.Header;
import java.util.List;
import java.text.ParseException;
import com.android.volley.VolleyLog;
import java.util.Map;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import java.util.TimeZone;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HttpHeaderParser
{
    private static final String DEFAULT_CONTENT_CHARSET = "ISO-8859-1";
    static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String RFC1123_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
    
    static String formatEpochAsRfc1123(final long date) {
        return newRfc1123Formatter().format(new Date(date));
    }
    
    private static SimpleDateFormat newRfc1123Formatter() {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat;
    }
    
    public static Cache.Entry parseCacheHeaders(final NetworkResponse p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: lstore          13
        //     5: aload_0        
        //     6: getfield        com/android/volley/NetworkResponse.headers:Ljava/util/Map;
        //     9: astore          15
        //    11: aload           15
        //    13: ldc             "Date"
        //    15: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    20: checkcast       Ljava/lang/String;
        //    23: astore          16
        //    25: aload           16
        //    27: ifnull          40
        //    30: aload           16
        //    32: invokestatic    com/android/volley/toolbox/HttpHeaderParser.parseDateAsEpoch:(Ljava/lang/String;)J
        //    35: lstore          7
        //    37: goto            43
        //    40: lconst_0       
        //    41: lstore          7
        //    43: aload           15
        //    45: ldc             "Cache-Control"
        //    47: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    52: checkcast       Ljava/lang/String;
        //    55: astore          16
        //    57: iconst_0       
        //    58: istore_1       
        //    59: iconst_0       
        //    60: istore_2       
        //    61: aload           16
        //    63: ifnull          235
        //    66: aload           16
        //    68: ldc             ","
        //    70: iconst_0       
        //    71: invokevirtual   java/lang/String.split:(Ljava/lang/String;I)[Ljava/lang/String;
        //    74: astore          16
        //    76: iconst_0       
        //    77: istore_1       
        //    78: lconst_0       
        //    79: lstore          5
        //    81: lconst_0       
        //    82: lstore_3       
        //    83: iload_2        
        //    84: aload           16
        //    86: arraylength    
        //    87: if_icmpge       230
        //    90: aload           16
        //    92: iload_2        
        //    93: aaload         
        //    94: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //    97: astore          17
        //    99: aload           17
        //   101: ldc             "no-cache"
        //   103: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   106: ifne            228
        //   109: aload           17
        //   111: ldc             "no-store"
        //   113: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   116: ifeq            122
        //   119: goto            228
        //   122: aload           17
        //   124: ldc             "max-age="
        //   126: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   129: ifeq            150
        //   132: aload           17
        //   134: bipush          8
        //   136: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   139: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   142: lstore          9
        //   144: lload_3        
        //   145: lstore          11
        //   147: goto            215
        //   150: aload           17
        //   152: ldc             "stale-while-revalidate="
        //   154: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   157: ifeq            179
        //   160: aload           17
        //   162: bipush          23
        //   164: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   167: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   170: lstore          11
        //   172: lload           5
        //   174: lstore          9
        //   176: goto            215
        //   179: aload           17
        //   181: ldc             "must-revalidate"
        //   183: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   186: ifne            206
        //   189: lload           5
        //   191: lstore          9
        //   193: lload_3        
        //   194: lstore          11
        //   196: aload           17
        //   198: ldc             "proxy-revalidate"
        //   200: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   203: ifeq            215
        //   206: iconst_1       
        //   207: istore_1       
        //   208: lload_3        
        //   209: lstore          11
        //   211: lload           5
        //   213: lstore          9
        //   215: iinc            2, 1
        //   218: lload           9
        //   220: lstore          5
        //   222: lload           11
        //   224: lstore_3       
        //   225: goto            83
        //   228: aconst_null    
        //   229: areturn        
        //   230: iconst_1       
        //   231: istore_2       
        //   232: goto            242
        //   235: iconst_0       
        //   236: istore_2       
        //   237: lconst_0       
        //   238: lstore          5
        //   240: lconst_0       
        //   241: lstore_3       
        //   242: aload           15
        //   244: ldc             "Expires"
        //   246: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   251: checkcast       Ljava/lang/String;
        //   254: astore          16
        //   256: aload           16
        //   258: ifnull          271
        //   261: aload           16
        //   263: invokestatic    com/android/volley/toolbox/HttpHeaderParser.parseDateAsEpoch:(Ljava/lang/String;)J
        //   266: lstore          11
        //   268: goto            274
        //   271: lconst_0       
        //   272: lstore          11
        //   274: aload           15
        //   276: ldc             "Last-Modified"
        //   278: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   283: checkcast       Ljava/lang/String;
        //   286: astore          16
        //   288: aload           16
        //   290: ifnull          303
        //   293: aload           16
        //   295: invokestatic    com/android/volley/toolbox/HttpHeaderParser.parseDateAsEpoch:(Ljava/lang/String;)J
        //   298: lstore          9
        //   300: goto            306
        //   303: lconst_0       
        //   304: lstore          9
        //   306: aload           15
        //   308: ldc             "ETag"
        //   310: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   315: checkcast       Ljava/lang/String;
        //   318: astore          17
        //   320: iload_2        
        //   321: ifeq            372
        //   324: lload           13
        //   326: lload           5
        //   328: ldc2_w          1000
        //   331: lmul           
        //   332: ladd           
        //   333: lstore          5
        //   335: iload_1        
        //   336: ifeq            345
        //   339: lload           5
        //   341: lstore_3       
        //   342: goto            359
        //   345: lload_3        
        //   346: invokestatic    java/lang/Long.signum:(J)I
        //   349: pop            
        //   350: lload_3        
        //   351: ldc2_w          1000
        //   354: lmul           
        //   355: lload           5
        //   357: ladd           
        //   358: lstore_3       
        //   359: lload_3        
        //   360: lstore          11
        //   362: lload           5
        //   364: lstore_3       
        //   365: lload           11
        //   367: lstore          5
        //   369: goto            407
        //   372: lconst_0       
        //   373: lstore          5
        //   375: lload           7
        //   377: lconst_0       
        //   378: lcmp           
        //   379: ifle            405
        //   382: lload           11
        //   384: lload           7
        //   386: lcmp           
        //   387: iflt            405
        //   390: lload           13
        //   392: lload           11
        //   394: lload           7
        //   396: lsub           
        //   397: ladd           
        //   398: lstore_3       
        //   399: lload_3        
        //   400: lstore          5
        //   402: goto            407
        //   405: lconst_0       
        //   406: lstore_3       
        //   407: new             Lcom/android/volley/Cache$Entry;
        //   410: dup            
        //   411: invokespecial   com/android/volley/Cache$Entry.<init>:()V
        //   414: astore          16
        //   416: aload           16
        //   418: aload_0        
        //   419: getfield        com/android/volley/NetworkResponse.data:[B
        //   422: putfield        com/android/volley/Cache$Entry.data:[B
        //   425: aload           16
        //   427: aload           17
        //   429: putfield        com/android/volley/Cache$Entry.etag:Ljava/lang/String;
        //   432: aload           16
        //   434: lload_3        
        //   435: putfield        com/android/volley/Cache$Entry.softTtl:J
        //   438: aload           16
        //   440: lload           5
        //   442: putfield        com/android/volley/Cache$Entry.ttl:J
        //   445: aload           16
        //   447: lload           7
        //   449: putfield        com/android/volley/Cache$Entry.serverDate:J
        //   452: aload           16
        //   454: lload           9
        //   456: putfield        com/android/volley/Cache$Entry.lastModified:J
        //   459: aload           16
        //   461: aload           15
        //   463: putfield        com/android/volley/Cache$Entry.responseHeaders:Ljava/util/Map;
        //   466: aload           16
        //   468: aload_0        
        //   469: getfield        com/android/volley/NetworkResponse.allHeaders:Ljava/util/List;
        //   472: putfield        com/android/volley/Cache$Entry.allResponseHeaders:Ljava/util/List;
        //   475: aload           16
        //   477: areturn        
        //   478: astore          17
        //   480: lload           5
        //   482: lstore          9
        //   484: lload_3        
        //   485: lstore          11
        //   487: goto            215
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  132    144    478    490    Ljava/lang/Exception;
        //  160    172    478    490    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String parseCharset(final Map<String, String> map) {
        return parseCharset(map, "ISO-8859-1");
    }
    
    public static String parseCharset(final Map<String, String> map, final String s) {
        final String s2 = map.get("Content-Type");
        if (s2 != null) {
            final String[] split = s2.split(";", 0);
            for (int i = 1; i < split.length; ++i) {
                final String[] split2 = split[i].trim().split("=", 0);
                if (split2.length == 2 && split2[0].equals("charset")) {
                    return split2[1];
                }
            }
        }
        return s;
    }
    
    public static long parseDateAsEpoch(final String source) {
        try {
            return newRfc1123Formatter().parse(source).getTime();
        }
        catch (final ParseException ex) {
            VolleyLog.e(ex, "Unable to parse dateStr: %s, falling back to 0", source);
            return 0L;
        }
    }
    
    static List<Header> toAllHeaderList(final Map<String, String> map) {
        final ArrayList list = new ArrayList(map.size());
        for (final Map.Entry entry : map.entrySet()) {
            list.add(new Header((String)entry.getKey(), (String)entry.getValue()));
        }
        return list;
    }
    
    static Map<String, String> toHeaderMap(final List<Header> list) {
        final TreeMap treeMap = new TreeMap((Comparator<? super K>)String.CASE_INSENSITIVE_ORDER);
        for (final Header header : list) {
            treeMap.put(header.getName(), header.getValue());
        }
        return treeMap;
    }
}

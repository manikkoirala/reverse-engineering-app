// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import android.os.SystemClock;
import java.util.concurrent.ExecutionException;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.Response;
import java.util.concurrent.Future;

public class RequestFuture<T> implements Future<T>, Listener<T>, ErrorListener
{
    private VolleyError mException;
    private Request<?> mRequest;
    private T mResult;
    private boolean mResultReceived;
    
    private RequestFuture() {
        this.mResultReceived = false;
    }
    
    private T doGet(final Long n) throws InterruptedException, ExecutionException, TimeoutException {
        synchronized (this) {
            if (this.mException != null) {
                throw new ExecutionException(this.mException);
            }
            if (this.mResultReceived) {
                return this.mResult;
            }
            if (n == null) {
                while (!this.isDone()) {
                    this.wait(0L);
                }
            }
            else if (n > 0L) {
                for (long n2 = SystemClock.uptimeMillis(), n3 = n + n2; !this.isDone() && n2 < n3; n2 = SystemClock.uptimeMillis()) {
                    this.wait(n3 - n2);
                }
            }
            if (this.mException != null) {
                throw new ExecutionException(this.mException);
            }
            if (this.mResultReceived) {
                return this.mResult;
            }
            throw new TimeoutException();
        }
    }
    
    public static <E> RequestFuture<E> newFuture() {
        return new RequestFuture<E>();
    }
    
    @Override
    public boolean cancel(final boolean b) {
        synchronized (this) {
            if (this.mRequest == null) {
                return false;
            }
            if (!this.isDone()) {
                this.mRequest.cancel();
                return true;
            }
            return false;
        }
    }
    
    @Override
    public T get() throws InterruptedException, ExecutionException {
        try {
            return this.doGet(null);
        }
        catch (final TimeoutException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    @Override
    public T get(final long sourceDuration, final TimeUnit sourceUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.doGet(TimeUnit.MILLISECONDS.convert(sourceDuration, sourceUnit));
    }
    
    @Override
    public boolean isCancelled() {
        final Request<?> mRequest = this.mRequest;
        return mRequest != null && mRequest.isCanceled();
    }
    
    @Override
    public boolean isDone() {
        synchronized (this) {
            return this.mResultReceived || this.mException != null || this.isCancelled();
        }
    }
    
    @Override
    public void onErrorResponse(final VolleyError mException) {
        synchronized (this) {
            this.mException = mException;
            this.notifyAll();
        }
    }
    
    @Override
    public void onResponse(final T mResult) {
        synchronized (this) {
            this.mResultReceived = true;
            this.mResult = mResult;
            this.notifyAll();
        }
    }
    
    public void setRequest(final Request<?> mRequest) {
        this.mRequest = mRequest;
    }
}

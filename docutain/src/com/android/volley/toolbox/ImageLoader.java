// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley.toolbox;

import java.util.ArrayList;
import java.util.List;
import android.graphics.Bitmap$Config;
import com.android.volley.Response;
import com.android.volley.Request;
import android.graphics.Bitmap;
import com.android.volley.VolleyError;
import android.widget.ImageView;
import android.widget.ImageView$ScaleType;
import java.util.Iterator;
import android.os.Looper;
import com.android.volley.RequestQueue;
import android.os.Handler;
import java.util.HashMap;

public class ImageLoader
{
    private int mBatchResponseDelayMs;
    private final HashMap<String, BatchedImageRequest> mBatchedResponses;
    private final ImageCache mCache;
    private final Handler mHandler;
    private final HashMap<String, BatchedImageRequest> mInFlightRequests;
    private final RequestQueue mRequestQueue;
    private Runnable mRunnable;
    
    public ImageLoader(final RequestQueue mRequestQueue, final ImageCache mCache) {
        this.mBatchResponseDelayMs = 100;
        this.mInFlightRequests = new HashMap<String, BatchedImageRequest>();
        this.mBatchedResponses = new HashMap<String, BatchedImageRequest>();
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mRequestQueue = mRequestQueue;
        this.mCache = mCache;
    }
    
    private void batchResponse(final String key, final BatchedImageRequest value) {
        this.mBatchedResponses.put(key, value);
        if (this.mRunnable == null) {
            final Runnable mRunnable = new Runnable(this) {
                final ImageLoader this$0;
                
                @Override
                public void run() {
                    for (final BatchedImageRequest batchedImageRequest : this.this$0.mBatchedResponses.values()) {
                        for (final ImageContainer imageContainer : batchedImageRequest.mContainers) {
                            if (imageContainer.mListener == null) {
                                continue;
                            }
                            if (batchedImageRequest.getError() == null) {
                                imageContainer.mBitmap = batchedImageRequest.mResponseBitmap;
                                imageContainer.mListener.onResponse(imageContainer, false);
                            }
                            else {
                                ((Response.ErrorListener)imageContainer.mListener).onErrorResponse(batchedImageRequest.getError());
                            }
                        }
                    }
                    this.this$0.mBatchedResponses.clear();
                    this.this$0.mRunnable = null;
                }
            };
            this.mRunnable = mRunnable;
            this.mHandler.postDelayed((Runnable)mRunnable, (long)this.mBatchResponseDelayMs);
        }
    }
    
    private static String getCacheKey(final String str, final int i, final int j, final ImageView$ScaleType imageView$ScaleType) {
        final StringBuilder sb = new StringBuilder(str.length() + 12);
        sb.append("#W");
        sb.append(i);
        sb.append("#H");
        sb.append(j);
        sb.append("#S");
        sb.append(imageView$ScaleType.ordinal());
        sb.append(str);
        return sb.toString();
    }
    
    public static ImageListener getImageListener(final ImageView imageView, final int n, final int n2) {
        return (ImageListener)new ImageListener(n2, imageView, n) {
            final int val$defaultImageResId;
            final int val$errorImageResId;
            final ImageView val$view;
            
            @Override
            public void onErrorResponse(final VolleyError volleyError) {
                final int val$errorImageResId = this.val$errorImageResId;
                if (val$errorImageResId != 0) {
                    this.val$view.setImageResource(val$errorImageResId);
                }
            }
            
            @Override
            public void onResponse(final ImageContainer imageContainer, final boolean b) {
                if (imageContainer.getBitmap() != null) {
                    this.val$view.setImageBitmap(imageContainer.getBitmap());
                }
                else {
                    final int val$defaultImageResId = this.val$defaultImageResId;
                    if (val$defaultImageResId != 0) {
                        this.val$view.setImageResource(val$defaultImageResId);
                    }
                }
            }
        };
    }
    
    public ImageContainer get(final String s, final ImageListener imageListener) {
        return this.get(s, imageListener, 0, 0);
    }
    
    public ImageContainer get(final String s, final ImageListener imageListener, final int n, final int n2) {
        return this.get(s, imageListener, n, n2, ImageView$ScaleType.CENTER_INSIDE);
    }
    
    public ImageContainer get(final String s, final ImageListener imageListener, final int n, final int n2, final ImageView$ScaleType imageView$ScaleType) {
        Threads.throwIfNotOnMainThread();
        final String cacheKey = getCacheKey(s, n, n2, imageView$ScaleType);
        final Bitmap bitmap = this.mCache.getBitmap(cacheKey);
        if (bitmap != null) {
            final ImageContainer imageContainer = new ImageContainer(bitmap, s, null, null);
            imageListener.onResponse(imageContainer, true);
            return imageContainer;
        }
        final ImageContainer imageContainer2 = new ImageContainer(null, s, cacheKey, imageListener);
        imageListener.onResponse(imageContainer2, true);
        final BatchedImageRequest batchedImageRequest = this.mInFlightRequests.get(cacheKey);
        if (batchedImageRequest != null) {
            batchedImageRequest.addContainer(imageContainer2);
            return imageContainer2;
        }
        final Request<Bitmap> imageRequest = this.makeImageRequest(s, n, n2, imageView$ScaleType, cacheKey);
        this.mRequestQueue.add((Request<Object>)imageRequest);
        this.mInFlightRequests.put(cacheKey, new BatchedImageRequest(imageRequest, imageContainer2));
        return imageContainer2;
    }
    
    public boolean isCached(final String s, final int n, final int n2) {
        return this.isCached(s, n, n2, ImageView$ScaleType.CENTER_INSIDE);
    }
    
    public boolean isCached(String cacheKey, final int n, final int n2, final ImageView$ScaleType imageView$ScaleType) {
        Threads.throwIfNotOnMainThread();
        cacheKey = getCacheKey(cacheKey, n, n2, imageView$ScaleType);
        return this.mCache.getBitmap(cacheKey) != null;
    }
    
    protected Request<Bitmap> makeImageRequest(final String s, final int n, final int n2, final ImageView$ScaleType imageView$ScaleType, final String s2) {
        return new ImageRequest(s, new Response.Listener<Bitmap>(this, s2) {
            final ImageLoader this$0;
            final String val$cacheKey;
            
            public void onResponse(final Bitmap bitmap) {
                this.this$0.onGetImageSuccess(this.val$cacheKey, bitmap);
            }
        }, n, n2, imageView$ScaleType, Bitmap$Config.RGB_565, new Response.ErrorListener(this, s2) {
            final ImageLoader this$0;
            final String val$cacheKey;
            
            @Override
            public void onErrorResponse(final VolleyError volleyError) {
                this.this$0.onGetImageError(this.val$cacheKey, volleyError);
            }
        });
    }
    
    protected void onGetImageError(final String key, final VolleyError error) {
        final BatchedImageRequest batchedImageRequest = this.mInFlightRequests.remove(key);
        if (batchedImageRequest != null) {
            batchedImageRequest.setError(error);
            this.batchResponse(key, batchedImageRequest);
        }
    }
    
    protected void onGetImageSuccess(final String key, final Bitmap bitmap) {
        this.mCache.putBitmap(key, bitmap);
        final BatchedImageRequest batchedImageRequest = this.mInFlightRequests.remove(key);
        if (batchedImageRequest != null) {
            batchedImageRequest.mResponseBitmap = bitmap;
            this.batchResponse(key, batchedImageRequest);
        }
    }
    
    public void setBatchedResponseDelay(final int mBatchResponseDelayMs) {
        this.mBatchResponseDelayMs = mBatchResponseDelayMs;
    }
    
    private static class BatchedImageRequest
    {
        private final List<ImageContainer> mContainers;
        private VolleyError mError;
        private final Request<?> mRequest;
        private Bitmap mResponseBitmap;
        
        public BatchedImageRequest(final Request<?> mRequest, final ImageContainer imageContainer) {
            final ArrayList mContainers = new ArrayList();
            this.mContainers = mContainers;
            this.mRequest = mRequest;
            mContainers.add(imageContainer);
        }
        
        public void addContainer(final ImageContainer imageContainer) {
            this.mContainers.add(imageContainer);
        }
        
        public VolleyError getError() {
            return this.mError;
        }
        
        public boolean removeContainerAndCancelIfNecessary(final ImageContainer imageContainer) {
            this.mContainers.remove(imageContainer);
            if (this.mContainers.size() == 0) {
                this.mRequest.cancel();
                return true;
            }
            return false;
        }
        
        public void setError(final VolleyError mError) {
            this.mError = mError;
        }
    }
    
    public interface ImageCache
    {
        Bitmap getBitmap(final String p0);
        
        void putBitmap(final String p0, final Bitmap p1);
    }
    
    public class ImageContainer
    {
        private Bitmap mBitmap;
        private final String mCacheKey;
        private final ImageListener mListener;
        private final String mRequestUrl;
        final ImageLoader this$0;
        
        public ImageContainer(final ImageLoader this$0, final Bitmap mBitmap, final String mRequestUrl, final String mCacheKey, final ImageListener mListener) {
            this.this$0 = this$0;
            this.mBitmap = mBitmap;
            this.mRequestUrl = mRequestUrl;
            this.mCacheKey = mCacheKey;
            this.mListener = mListener;
        }
        
        public void cancelRequest() {
            Threads.throwIfNotOnMainThread();
            if (this.mListener == null) {
                return;
            }
            final BatchedImageRequest batchedImageRequest = this.this$0.mInFlightRequests.get(this.mCacheKey);
            if (batchedImageRequest != null) {
                if (batchedImageRequest.removeContainerAndCancelIfNecessary(this)) {
                    this.this$0.mInFlightRequests.remove(this.mCacheKey);
                }
            }
            else {
                final BatchedImageRequest batchedImageRequest2 = this.this$0.mBatchedResponses.get(this.mCacheKey);
                if (batchedImageRequest2 != null) {
                    batchedImageRequest2.removeContainerAndCancelIfNecessary(this);
                    if (batchedImageRequest2.mContainers.size() == 0) {
                        this.this$0.mBatchedResponses.remove(this.mCacheKey);
                    }
                }
            }
        }
        
        public Bitmap getBitmap() {
            return this.mBitmap;
        }
        
        public String getRequestUrl() {
            return this.mRequestUrl;
        }
    }
    
    public interface ImageListener extends ErrorListener
    {
        void onResponse(final ImageContainer p0, final boolean p1);
    }
}

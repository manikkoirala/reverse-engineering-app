// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.Collections;
import android.os.Handler;
import android.os.Looper;
import android.net.Uri;
import android.text.TextUtils;
import java.util.Iterator;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public abstract class Request<T> implements Comparable<Request<T>>
{
    private static final String DEFAULT_PARAMS_ENCODING = "UTF-8";
    private Cache.Entry mCacheEntry;
    private boolean mCanceled;
    private final int mDefaultTrafficStatsTag;
    private Response.ErrorListener mErrorListener;
    private final VolleyLog.MarkerLog mEventLog;
    private final Object mLock;
    private final int mMethod;
    private NetworkRequestCompleteListener mRequestCompleteListener;
    private RequestQueue mRequestQueue;
    private boolean mResponseDelivered;
    private RetryPolicy mRetryPolicy;
    private Integer mSequence;
    private boolean mShouldCache;
    private boolean mShouldRetryServerErrors;
    private Object mTag;
    private final String mUrl;
    
    public Request(final int mMethod, final String mUrl, final Response.ErrorListener mErrorListener) {
        VolleyLog.MarkerLog mEventLog;
        if (VolleyLog.MarkerLog.ENABLED) {
            mEventLog = new VolleyLog.MarkerLog();
        }
        else {
            mEventLog = null;
        }
        this.mEventLog = mEventLog;
        this.mLock = new Object();
        this.mShouldCache = true;
        this.mCanceled = false;
        this.mResponseDelivered = false;
        this.mShouldRetryServerErrors = false;
        this.mCacheEntry = null;
        this.mMethod = mMethod;
        this.mUrl = mUrl;
        this.mErrorListener = mErrorListener;
        this.setRetryPolicy(new DefaultRetryPolicy());
        this.mDefaultTrafficStatsTag = findDefaultTrafficStatsTag(mUrl);
    }
    
    @Deprecated
    public Request(final String s, final Response.ErrorListener errorListener) {
        this(-1, s, errorListener);
    }
    
    private byte[] encodeParameters(final Map<String, String> map, final String s) {
        final StringBuilder sb = new StringBuilder();
        try {
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                if (entry.getKey() == null || entry.getValue() == null) {
                    throw new IllegalArgumentException(String.format("Request#getParams() or Request#getPostParams() returned a map containing a null key or value: (%s, %s). All keys and values must be non-null.", entry.getKey(), entry.getValue()));
                }
                sb.append(URLEncoder.encode(entry.getKey(), s));
                sb.append('=');
                sb.append(URLEncoder.encode((String)entry.getValue(), s));
                sb.append('&');
            }
            return sb.toString().getBytes(s);
        }
        catch (final UnsupportedEncodingException cause) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Encoding not supported: ");
            sb2.append(s);
            throw new RuntimeException(sb2.toString(), cause);
        }
    }
    
    private static int findDefaultTrafficStatsTag(String host) {
        if (!TextUtils.isEmpty((CharSequence)host)) {
            final Uri parse = Uri.parse(host);
            if (parse != null) {
                host = parse.getHost();
                if (host != null) {
                    return host.hashCode();
                }
            }
        }
        return 0;
    }
    
    public void addMarker(final String s) {
        if (VolleyLog.MarkerLog.ENABLED) {
            this.mEventLog.add(s, Thread.currentThread().getId());
        }
    }
    
    public void cancel() {
        synchronized (this.mLock) {
            this.mCanceled = true;
            this.mErrorListener = null;
        }
    }
    
    @Override
    public int compareTo(final Request<T> request) {
        final Priority priority = this.getPriority();
        final Priority priority2 = request.getPriority();
        int n;
        if (priority == priority2) {
            n = this.mSequence - request.mSequence;
        }
        else {
            n = priority2.ordinal() - priority.ordinal();
        }
        return n;
    }
    
    public void deliverError(final VolleyError volleyError) {
        synchronized (this.mLock) {
            final Response.ErrorListener mErrorListener = this.mErrorListener;
            monitorexit(this.mLock);
            if (mErrorListener != null) {
                mErrorListener.onErrorResponse(volleyError);
            }
        }
    }
    
    protected abstract void deliverResponse(final T p0);
    
    void finish(final String s) {
        final RequestQueue mRequestQueue = this.mRequestQueue;
        if (mRequestQueue != null) {
            mRequestQueue.finish((Request<Object>)this);
        }
        if (VolleyLog.MarkerLog.ENABLED) {
            final long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, s, id) {
                    final Request this$0;
                    final String val$tag;
                    final long val$threadId;
                    
                    @Override
                    public void run() {
                        this.this$0.mEventLog.add(this.val$tag, this.val$threadId);
                        this.this$0.mEventLog.finish(this.this$0.toString());
                    }
                });
                return;
            }
            this.mEventLog.add(s, id);
            this.mEventLog.finish(this.toString());
        }
    }
    
    public byte[] getBody() throws AuthFailureError {
        final Map<String, String> params = this.getParams();
        if (params != null && params.size() > 0) {
            return this.encodeParameters(params, this.getParamsEncoding());
        }
        return null;
    }
    
    public String getBodyContentType() {
        final StringBuilder sb = new StringBuilder();
        sb.append("application/x-www-form-urlencoded; charset=");
        sb.append(this.getParamsEncoding());
        return sb.toString();
    }
    
    public Cache.Entry getCacheEntry() {
        return this.mCacheEntry;
    }
    
    public String getCacheKey() {
        final String url = this.getUrl();
        final int method = this.getMethod();
        String string = url;
        if (method != 0) {
            if (method == -1) {
                string = url;
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(Integer.toString(method));
                sb.append('-');
                sb.append(url);
                string = sb.toString();
            }
        }
        return string;
    }
    
    public Response.ErrorListener getErrorListener() {
        synchronized (this.mLock) {
            return this.mErrorListener;
        }
    }
    
    public Map<String, String> getHeaders() throws AuthFailureError {
        return Collections.emptyMap();
    }
    
    public int getMethod() {
        return this.mMethod;
    }
    
    protected Map<String, String> getParams() throws AuthFailureError {
        return null;
    }
    
    protected String getParamsEncoding() {
        return "UTF-8";
    }
    
    @Deprecated
    public byte[] getPostBody() throws AuthFailureError {
        final Map<String, String> postParams = this.getPostParams();
        if (postParams != null && postParams.size() > 0) {
            return this.encodeParameters(postParams, this.getPostParamsEncoding());
        }
        return null;
    }
    
    @Deprecated
    public String getPostBodyContentType() {
        return this.getBodyContentType();
    }
    
    @Deprecated
    protected Map<String, String> getPostParams() throws AuthFailureError {
        return this.getParams();
    }
    
    @Deprecated
    protected String getPostParamsEncoding() {
        return this.getParamsEncoding();
    }
    
    public Priority getPriority() {
        return Priority.NORMAL;
    }
    
    public RetryPolicy getRetryPolicy() {
        return this.mRetryPolicy;
    }
    
    public final int getSequence() {
        final Integer mSequence = this.mSequence;
        if (mSequence != null) {
            return mSequence;
        }
        throw new IllegalStateException("getSequence called before setSequence");
    }
    
    public Object getTag() {
        return this.mTag;
    }
    
    public final int getTimeoutMs() {
        return this.getRetryPolicy().getCurrentTimeout();
    }
    
    public int getTrafficStatsTag() {
        return this.mDefaultTrafficStatsTag;
    }
    
    public String getUrl() {
        return this.mUrl;
    }
    
    public boolean hasHadResponseDelivered() {
        synchronized (this.mLock) {
            return this.mResponseDelivered;
        }
    }
    
    public boolean isCanceled() {
        synchronized (this.mLock) {
            return this.mCanceled;
        }
    }
    
    public void markDelivered() {
        synchronized (this.mLock) {
            this.mResponseDelivered = true;
        }
    }
    
    void notifyListenerResponseNotUsable() {
        synchronized (this.mLock) {
            final NetworkRequestCompleteListener mRequestCompleteListener = this.mRequestCompleteListener;
            monitorexit(this.mLock);
            if (mRequestCompleteListener != null) {
                mRequestCompleteListener.onNoUsableResponseReceived(this);
            }
        }
    }
    
    void notifyListenerResponseReceived(final Response<?> response) {
        synchronized (this.mLock) {
            final NetworkRequestCompleteListener mRequestCompleteListener = this.mRequestCompleteListener;
            monitorexit(this.mLock);
            if (mRequestCompleteListener != null) {
                mRequestCompleteListener.onResponseReceived(this, response);
            }
        }
    }
    
    protected VolleyError parseNetworkError(final VolleyError volleyError) {
        return volleyError;
    }
    
    protected abstract Response<T> parseNetworkResponse(final NetworkResponse p0);
    
    public Request<?> setCacheEntry(final Cache.Entry mCacheEntry) {
        this.mCacheEntry = mCacheEntry;
        return this;
    }
    
    void setNetworkRequestCompleteListener(final NetworkRequestCompleteListener mRequestCompleteListener) {
        synchronized (this.mLock) {
            this.mRequestCompleteListener = mRequestCompleteListener;
        }
    }
    
    public Request<?> setRequestQueue(final RequestQueue mRequestQueue) {
        this.mRequestQueue = mRequestQueue;
        return this;
    }
    
    public Request<?> setRetryPolicy(final RetryPolicy mRetryPolicy) {
        this.mRetryPolicy = mRetryPolicy;
        return this;
    }
    
    public final Request<?> setSequence(final int i) {
        this.mSequence = i;
        return this;
    }
    
    public final Request<?> setShouldCache(final boolean mShouldCache) {
        this.mShouldCache = mShouldCache;
        return this;
    }
    
    public final Request<?> setShouldRetryServerErrors(final boolean mShouldRetryServerErrors) {
        this.mShouldRetryServerErrors = mShouldRetryServerErrors;
        return this;
    }
    
    public Request<?> setTag(final Object mTag) {
        this.mTag = mTag;
        return this;
    }
    
    public final boolean shouldCache() {
        return this.mShouldCache;
    }
    
    public final boolean shouldRetryServerErrors() {
        return this.mShouldRetryServerErrors;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("0x");
        sb.append(Integer.toHexString(this.getTrafficStatsTag()));
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        String str;
        if (this.isCanceled()) {
            str = "[X] ";
        }
        else {
            str = "[ ] ";
        }
        sb2.append(str);
        sb2.append(this.getUrl());
        sb2.append(" ");
        sb2.append(string);
        sb2.append(" ");
        sb2.append(this.getPriority());
        sb2.append(" ");
        sb2.append(this.mSequence);
        return sb2.toString();
    }
    
    public interface Method
    {
        public static final int DELETE = 3;
        public static final int DEPRECATED_GET_OR_POST = -1;
        public static final int GET = 0;
        public static final int HEAD = 4;
        public static final int OPTIONS = 5;
        public static final int PATCH = 7;
        public static final int POST = 1;
        public static final int PUT = 2;
        public static final int TRACE = 6;
    }
    
    interface NetworkRequestCompleteListener
    {
        void onNoUsableResponseReceived(final Request<?> p0);
        
        void onResponseReceived(final Request<?> p0, final Response<?> p1);
    }
    
    public enum Priority
    {
        private static final Priority[] $VALUES;
        
        HIGH, 
        IMMEDIATE, 
        LOW, 
        NORMAL;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.Iterator;
import android.os.SystemClock;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.util.Log;

public class VolleyLog
{
    private static final String CLASS_NAME;
    public static boolean DEBUG = false;
    public static String TAG = "Volley";
    
    static {
        VolleyLog.DEBUG = Log.isLoggable("Volley", 2);
        CLASS_NAME = VolleyLog.class.getName();
    }
    
    private static String buildMessage(String format, final Object... args) {
        if (args != null) {
            format = String.format(Locale.US, format, args);
        }
        final StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        for (int i = 2; i < stackTrace.length; ++i) {
            if (!stackTrace[i].getClassName().equals(VolleyLog.CLASS_NAME)) {
                final String className = stackTrace[i].getClassName();
                final String substring = className.substring(className.lastIndexOf(46) + 1);
                final String substring2 = substring.substring(substring.lastIndexOf(36) + 1);
                final StringBuilder sb = new StringBuilder();
                sb.append(substring2);
                sb.append(".");
                sb.append(stackTrace[i].getMethodName());
                final String string = sb.toString();
                return String.format(Locale.US, "[%d] %s: %s", Thread.currentThread().getId(), string, format);
            }
        }
        final String string = "<unknown>";
        return String.format(Locale.US, "[%d] %s: %s", Thread.currentThread().getId(), string, format);
    }
    
    public static void d(final String s, final Object... array) {
        Log.d(VolleyLog.TAG, buildMessage(s, array));
    }
    
    public static void e(final String s, final Object... array) {
        Log.e(VolleyLog.TAG, buildMessage(s, array));
    }
    
    public static void e(final Throwable t, final String s, final Object... array) {
        Log.e(VolleyLog.TAG, buildMessage(s, array), t);
    }
    
    public static void setTag(final String tag) {
        d("Changing log tag to %s", tag);
        VolleyLog.TAG = tag;
        VolleyLog.DEBUG = Log.isLoggable(tag, 2);
    }
    
    public static void v(final String s, final Object... array) {
        if (VolleyLog.DEBUG) {
            Log.v(VolleyLog.TAG, buildMessage(s, array));
        }
    }
    
    public static void wtf(final String s, final Object... array) {
        Log.wtf(VolleyLog.TAG, buildMessage(s, array));
    }
    
    public static void wtf(final Throwable t, final String s, final Object... array) {
        Log.wtf(VolleyLog.TAG, buildMessage(s, array), t);
    }
    
    static class MarkerLog
    {
        public static final boolean ENABLED;
        private static final long MIN_DURATION_FOR_LOGGING_MS = 0L;
        private boolean mFinished;
        private final List<Marker> mMarkers;
        
        static {
            ENABLED = VolleyLog.DEBUG;
        }
        
        MarkerLog() {
            this.mMarkers = new ArrayList<Marker>();
            this.mFinished = false;
        }
        
        private long getTotalDuration() {
            if (this.mMarkers.size() == 0) {
                return 0L;
            }
            final long time = this.mMarkers.get(0).time;
            final List<Marker> mMarkers = this.mMarkers;
            return ((Marker)mMarkers.get(mMarkers.size() - 1)).time - time;
        }
        
        public void add(final String s, final long n) {
            synchronized (this) {
                if (!this.mFinished) {
                    this.mMarkers.add(new Marker(s, n, SystemClock.elapsedRealtime()));
                    return;
                }
                throw new IllegalStateException("Marker added to finished log");
            }
        }
        
        @Override
        protected void finalize() throws Throwable {
            if (!this.mFinished) {
                this.finish("Request on the loose");
                VolleyLog.e("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }
        
        public void finish(final String s) {
            synchronized (this) {
                this.mFinished = true;
                final long totalDuration = this.getTotalDuration();
                if (totalDuration <= 0L) {
                    return;
                }
                long time = this.mMarkers.get(0).time;
                VolleyLog.d("(%-4d ms) %s", totalDuration, s);
                for (final Marker marker : this.mMarkers) {
                    final long time2 = marker.time;
                    VolleyLog.d("(+%-4d) [%2d] %s", time2 - time, marker.thread, marker.name);
                    time = time2;
                }
            }
        }
        
        private static class Marker
        {
            public final String name;
            public final long thread;
            public final long time;
            
            public Marker(final String name, final long thread, final long time) {
                this.name = name;
                this.thread = thread;
                this.time = time;
            }
        }
    }
}

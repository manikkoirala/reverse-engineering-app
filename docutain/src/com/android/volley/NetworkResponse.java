// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.Comparator;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.List;

public class NetworkResponse
{
    public final List<Header> allHeaders;
    public final byte[] data;
    public final Map<String, String> headers;
    public final long networkTimeMs;
    public final boolean notModified;
    public final int statusCode;
    
    private NetworkResponse(final int statusCode, final byte[] data, final Map<String, String> headers, final List<Header> list, final boolean notModified, final long networkTimeMs) {
        this.statusCode = statusCode;
        this.data = data;
        this.headers = headers;
        if (list == null) {
            this.allHeaders = null;
        }
        else {
            this.allHeaders = Collections.unmodifiableList((List<? extends Header>)list);
        }
        this.notModified = notModified;
        this.networkTimeMs = networkTimeMs;
    }
    
    @Deprecated
    public NetworkResponse(final int n, final byte[] array, final Map<String, String> map, final boolean b) {
        this(n, array, map, b, 0L);
    }
    
    @Deprecated
    public NetworkResponse(final int n, final byte[] array, final Map<String, String> map, final boolean b, final long n2) {
        this(n, array, map, toAllHeaderList(map), b, n2);
    }
    
    public NetworkResponse(final int n, final byte[] array, final boolean b, final long n2, final List<Header> list) {
        this(n, array, toHeaderMap(list), list, b, n2);
    }
    
    public NetworkResponse(final byte[] array) {
        this(200, array, false, 0L, Collections.emptyList());
    }
    
    @Deprecated
    public NetworkResponse(final byte[] array, final Map<String, String> map) {
        this(200, array, map, false, 0L);
    }
    
    private static List<Header> toAllHeaderList(final Map<String, String> map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return Collections.emptyList();
        }
        final ArrayList list = new ArrayList(map.size());
        for (final Map.Entry entry : map.entrySet()) {
            list.add(new Header((String)entry.getKey(), (String)entry.getValue()));
        }
        return list;
    }
    
    private static Map<String, String> toHeaderMap(final List<Header> list) {
        if (list == null) {
            return null;
        }
        if (list.isEmpty()) {
            return Collections.emptyMap();
        }
        final TreeMap treeMap = new TreeMap((Comparator<? super K>)String.CASE_INSENSITIVE_ORDER);
        for (final Header header : list) {
            treeMap.put(header.getName(), header.getValue());
        }
        return treeMap;
    }
}

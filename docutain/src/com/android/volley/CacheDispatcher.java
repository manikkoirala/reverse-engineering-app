// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.os.Process;
import java.util.concurrent.BlockingQueue;

public class CacheDispatcher extends Thread
{
    private static final boolean DEBUG;
    private final Cache mCache;
    private final BlockingQueue<Request<?>> mCacheQueue;
    private final ResponseDelivery mDelivery;
    private final BlockingQueue<Request<?>> mNetworkQueue;
    private volatile boolean mQuit;
    private final WaitingRequestManager mWaitingRequestManager;
    
    static {
        DEBUG = VolleyLog.DEBUG;
    }
    
    public CacheDispatcher(final BlockingQueue<Request<?>> mCacheQueue, final BlockingQueue<Request<?>> mNetworkQueue, final Cache mCache, final ResponseDelivery mDelivery) {
        this.mQuit = false;
        this.mCacheQueue = mCacheQueue;
        this.mNetworkQueue = mNetworkQueue;
        this.mCache = mCache;
        this.mDelivery = mDelivery;
        this.mWaitingRequestManager = new WaitingRequestManager(this);
    }
    
    private void processRequest() throws InterruptedException {
        this.processRequest(this.mCacheQueue.take());
    }
    
    void processRequest(final Request<?> request) throws InterruptedException {
        request.addMarker("cache-queue-take");
        if (request.isCanceled()) {
            request.finish("cache-discard-canceled");
            return;
        }
        final Cache.Entry value = this.mCache.get(request.getCacheKey());
        if (value == null) {
            request.addMarker("cache-miss");
            if (!this.mWaitingRequestManager.maybeAddToWaitingRequests(request)) {
                this.mNetworkQueue.put(request);
            }
            return;
        }
        if (value.isExpired()) {
            request.addMarker("cache-hit-expired");
            request.setCacheEntry(value);
            if (!this.mWaitingRequestManager.maybeAddToWaitingRequests(request)) {
                this.mNetworkQueue.put(request);
            }
            return;
        }
        request.addMarker("cache-hit");
        final Response networkResponse = request.parseNetworkResponse(new NetworkResponse(value.data, value.responseHeaders));
        request.addMarker("cache-hit-parsed");
        if (!value.refreshNeeded()) {
            this.mDelivery.postResponse(request, networkResponse);
        }
        else {
            request.addMarker("cache-hit-refresh-needed");
            request.setCacheEntry(value);
            networkResponse.intermediate = true;
            if (!this.mWaitingRequestManager.maybeAddToWaitingRequests(request)) {
                this.mDelivery.postResponse(request, networkResponse, new Runnable(this, request) {
                    final CacheDispatcher this$0;
                    final Request val$request;
                    
                    @Override
                    public void run() {
                        try {
                            this.this$0.mNetworkQueue.put(this.val$request);
                        }
                        catch (final InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                    }
                });
            }
            else {
                this.mDelivery.postResponse(request, networkResponse);
            }
        }
    }
    
    public void quit() {
        this.mQuit = true;
        this.interrupt();
    }
    
    @Override
    public void run() {
        if (CacheDispatcher.DEBUG) {
            VolleyLog.v("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.mCache.initialize();
    Label_0029_Outer:
        while (true) {
            while (true) {
                try {
                    while (true) {
                        this.processRequest();
                    }
                }
                catch (final InterruptedException ex) {
                    if (this.mQuit) {
                        Thread.currentThread().interrupt();
                        return;
                    }
                    VolleyLog.e("Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it", new Object[0]);
                    continue Label_0029_Outer;
                }
                continue;
            }
        }
    }
    
    private static class WaitingRequestManager implements NetworkRequestCompleteListener
    {
        private final CacheDispatcher mCacheDispatcher;
        private final Map<String, List<Request<?>>> mWaitingRequests;
        
        WaitingRequestManager(final CacheDispatcher mCacheDispatcher) {
            this.mWaitingRequests = new HashMap<String, List<Request<?>>>();
            this.mCacheDispatcher = mCacheDispatcher;
        }
        
        private boolean maybeAddToWaitingRequests(final Request<?> request) {
            synchronized (this) {
                final String cacheKey = request.getCacheKey();
                if (this.mWaitingRequests.containsKey(cacheKey)) {
                    List list;
                    if ((list = this.mWaitingRequests.get(cacheKey)) == null) {
                        list = new ArrayList();
                    }
                    request.addMarker("waiting-for-response");
                    list.add(request);
                    this.mWaitingRequests.put(cacheKey, list);
                    if (VolleyLog.DEBUG) {
                        VolleyLog.d("Request for cacheKey=%s is in flight, putting on hold.", cacheKey);
                    }
                    return true;
                }
                this.mWaitingRequests.put(cacheKey, null);
                request.setNetworkRequestCompleteListener((Request.NetworkRequestCompleteListener)this);
                if (VolleyLog.DEBUG) {
                    VolleyLog.d("new request, sending to network %s", cacheKey);
                }
                return false;
            }
        }
        
        @Override
        public void onNoUsableResponseReceived(final Request<?> request) {
            synchronized (this) {
                final String cacheKey = request.getCacheKey();
                final List list = this.mWaitingRequests.remove(cacheKey);
                if (list != null && !list.isEmpty()) {
                    if (VolleyLog.DEBUG) {
                        VolleyLog.v("%d waiting requests for cacheKey=%s; resend to network", list.size(), cacheKey);
                    }
                    final Request request2 = (Request)list.remove(0);
                    this.mWaitingRequests.put(cacheKey, list);
                    request2.setNetworkRequestCompleteListener((Request.NetworkRequestCompleteListener)this);
                    try {
                        this.mCacheDispatcher.mNetworkQueue.put(request2);
                    }
                    catch (final InterruptedException ex) {
                        VolleyLog.e("Couldn't add request to queue. %s", ex.toString());
                        Thread.currentThread().interrupt();
                        this.mCacheDispatcher.quit();
                    }
                }
            }
        }
        
        @Override
        public void onResponseReceived(final Request<?> request, final Response<?> response) {
            if (response.cacheEntry != null) {
                if (!response.cacheEntry.isExpired()) {
                    final String cacheKey = request.getCacheKey();
                    synchronized (this) {
                        final List list = this.mWaitingRequests.remove(cacheKey);
                        monitorexit(this);
                        if (list != null) {
                            if (VolleyLog.DEBUG) {
                                VolleyLog.v("Releasing %d waiting requests for cacheKey=%s.", list.size(), cacheKey);
                            }
                            final Iterator iterator = list.iterator();
                            while (iterator.hasNext()) {
                                this.mCacheDispatcher.mDelivery.postResponse((Request<?>)iterator.next(), response);
                            }
                        }
                        return;
                    }
                }
            }
            this.onNoUsableResponseReceived(request);
        }
    }
}

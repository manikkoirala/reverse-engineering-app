// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.concurrent.BlockingQueue;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashSet;
import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;

public class RequestQueue
{
    private static final int DEFAULT_NETWORK_THREAD_POOL_SIZE = 4;
    private final Cache mCache;
    private CacheDispatcher mCacheDispatcher;
    private final PriorityBlockingQueue<Request<?>> mCacheQueue;
    private final Set<Request<?>> mCurrentRequests;
    private final ResponseDelivery mDelivery;
    private final NetworkDispatcher[] mDispatchers;
    private final List<RequestFinishedListener> mFinishedListeners;
    private final Network mNetwork;
    private final PriorityBlockingQueue<Request<?>> mNetworkQueue;
    private final AtomicInteger mSequenceGenerator;
    
    public RequestQueue(final Cache cache, final Network network) {
        this(cache, network, 4);
    }
    
    public RequestQueue(final Cache cache, final Network network, final int n) {
        this(cache, network, n, new ExecutorDelivery(new Handler(Looper.getMainLooper())));
    }
    
    public RequestQueue(final Cache mCache, final Network mNetwork, final int n, final ResponseDelivery mDelivery) {
        this.mSequenceGenerator = new AtomicInteger();
        this.mCurrentRequests = new HashSet<Request<?>>();
        this.mCacheQueue = new PriorityBlockingQueue<Request<?>>();
        this.mNetworkQueue = new PriorityBlockingQueue<Request<?>>();
        this.mFinishedListeners = new ArrayList<RequestFinishedListener>();
        this.mCache = mCache;
        this.mNetwork = mNetwork;
        this.mDispatchers = new NetworkDispatcher[n];
        this.mDelivery = mDelivery;
    }
    
    public <T> Request<T> add(final Request<T> request) {
        request.setRequestQueue(this);
        synchronized (this.mCurrentRequests) {
            this.mCurrentRequests.add(request);
            monitorexit(this.mCurrentRequests);
            request.setSequence(this.getSequenceNumber());
            request.addMarker("add-to-queue");
            if (!request.shouldCache()) {
                this.mNetworkQueue.add(request);
                return request;
            }
            this.mCacheQueue.add(request);
            return request;
        }
    }
    
    public <T> void addRequestFinishedListener(final RequestFinishedListener<T> requestFinishedListener) {
        synchronized (this.mFinishedListeners) {
            this.mFinishedListeners.add(requestFinishedListener);
        }
    }
    
    public void cancelAll(final RequestFilter requestFilter) {
        synchronized (this.mCurrentRequests) {
            for (final Request request : this.mCurrentRequests) {
                if (requestFilter.apply(request)) {
                    request.cancel();
                }
            }
        }
    }
    
    public void cancelAll(final Object o) {
        if (o != null) {
            this.cancelAll((RequestFilter)new RequestFilter(this, o) {
                final RequestQueue this$0;
                final Object val$tag;
                
                @Override
                public boolean apply(final Request<?> request) {
                    return request.getTag() == this.val$tag;
                }
            });
            return;
        }
        throw new IllegalArgumentException("Cannot cancelAll with a null tag");
    }
    
     <T> void finish(final Request<T> request) {
        synchronized (this.mCurrentRequests) {
            this.mCurrentRequests.remove(request);
            monitorexit(this.mCurrentRequests);
            final List<RequestFinishedListener> mFinishedListeners = this.mFinishedListeners;
            synchronized (this.mCurrentRequests) {
                final Iterator<RequestFinishedListener> iterator = (Iterator<RequestFinishedListener>)this.mFinishedListeners.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onRequestFinished(request);
                }
            }
        }
    }
    
    public Cache getCache() {
        return this.mCache;
    }
    
    public int getSequenceNumber() {
        return this.mSequenceGenerator.incrementAndGet();
    }
    
    public <T> void removeRequestFinishedListener(final RequestFinishedListener<T> requestFinishedListener) {
        synchronized (this.mFinishedListeners) {
            this.mFinishedListeners.remove(requestFinishedListener);
        }
    }
    
    public void start() {
        this.stop();
        (this.mCacheDispatcher = new CacheDispatcher(this.mCacheQueue, this.mNetworkQueue, this.mCache, this.mDelivery)).start();
        for (int i = 0; i < this.mDispatchers.length; ++i) {
            (this.mDispatchers[i] = new NetworkDispatcher(this.mNetworkQueue, this.mNetwork, this.mCache, this.mDelivery)).start();
        }
    }
    
    public void stop() {
        final CacheDispatcher mCacheDispatcher = this.mCacheDispatcher;
        if (mCacheDispatcher != null) {
            mCacheDispatcher.quit();
        }
        for (final NetworkDispatcher networkDispatcher : this.mDispatchers) {
            if (networkDispatcher != null) {
                networkDispatcher.quit();
            }
        }
    }
    
    public interface RequestFilter
    {
        boolean apply(final Request<?> p0);
    }
    
    public interface RequestFinishedListener<T>
    {
        void onRequestFinished(final Request<T> p0);
    }
}

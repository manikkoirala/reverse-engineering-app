// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.datatransport.Event;
import com.google.android.gms.internal.play_billing.zzb;
import com.google.android.datatransport.Transformer;
import com.google.android.datatransport.Encoding;
import com.google.android.gms.internal.play_billing.zzfz;
import com.google.android.datatransport.runtime.Destination;
import com.google.android.datatransport.cct.CCTDestination;
import com.google.android.datatransport.runtime.TransportRuntime;
import android.content.Context;
import com.google.android.datatransport.Transport;

final class zzay
{
    private boolean zza;
    private Transport zzb;
    
    zzay(final Context context) {
        try {
            TransportRuntime.initialize(context);
            this.zzb = TransportRuntime.getInstance().newFactory((Destination)CCTDestination.INSTANCE).getTransport("PLAY_BILLING_LIBRARY", (Class)zzfz.class, Encoding.of("proto"), (Transformer)zzax.zza);
        }
        finally {
            this.zza = true;
        }
    }
    
    public final void zza(final zzfz zzfz) {
        if (this.zza) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingLogger", "Skipping logging since initialization failed.");
            return;
        }
        try {
            this.zzb.send(Event.ofData((Object)zzfz));
        }
        finally {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingLogger", "logging failed.");
        }
    }
}

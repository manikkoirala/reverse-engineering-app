// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import com.google.android.gms.internal.play_billing.zzu;

public final class QueryProductDetailsParams
{
    private final zzu zza = builder.zza;
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    public final zzu zza() {
        return this.zza;
    }
    
    public final String zzb() {
        return ((Product)this.zza.get(0)).zzb();
    }
    
    public static class Builder
    {
        private zzu zza;
        
        private Builder() {
        }
        
        public QueryProductDetailsParams build() {
            return new QueryProductDetailsParams(this, null);
        }
        
        public Builder setProductList(final List<Product> list) {
            if (list == null || list.isEmpty()) {
                throw new IllegalArgumentException("Product list cannot be empty.");
            }
            final HashSet set = new HashSet();
            for (final Product product : list) {
                if (!"play_pass_subs".equals(product.zzb())) {
                    set.add(product.zzb());
                }
            }
            if (set.size() <= 1) {
                this.zza = zzu.zzj((Collection)list);
                return this;
            }
            throw new IllegalArgumentException("All products should be of the same product type.");
        }
    }
    
    public static class Product
    {
        private final String zza = builder.zza;
        private final String zzb = builder.zzb;
        
        public static Builder newBuilder() {
            return new Builder(null);
        }
        
        public final String zza() {
            return this.zza;
        }
        
        public final String zzb() {
            return this.zzb;
        }
        
        public static class Builder
        {
            private String zza;
            private String zzb;
            
            private Builder() {
            }
            
            public Product build() {
                if ("first_party".equals(this.zzb)) {
                    throw new IllegalArgumentException("Serialized doc id must be provided for first party products.");
                }
                if (this.zza == null) {
                    throw new IllegalArgumentException("Product id must be provided.");
                }
                if (this.zzb != null) {
                    return new Product(this, null);
                }
                throw new IllegalArgumentException("Product type must be provided.");
            }
            
            public Builder setProductId(final String zza) {
                this.zza = zza;
                return this;
            }
            
            public Builder setProductType(final String zzb) {
                this.zzb = zzb;
                return this;
            }
        }
    }
}

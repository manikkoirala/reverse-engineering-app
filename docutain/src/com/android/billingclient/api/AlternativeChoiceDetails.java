// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public final class AlternativeChoiceDetails
{
    private final String zza;
    private final JSONObject zzb;
    private final List zzc;
    
    AlternativeChoiceDetails(final String zza) throws JSONException {
        this.zza = zza;
        final JSONObject zzb = new JSONObject(zza);
        this.zzb = zzb;
        final JSONArray optJSONArray = zzb.optJSONArray("products");
        final ArrayList zzc = new ArrayList();
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); ++i) {
                final JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    zzc.add(new Product(optJSONObject, null));
                }
            }
        }
        this.zzc = zzc;
    }
    
    public String getExternalTransactionToken() {
        return this.zzb.optString("externalTransactionToken");
    }
    
    public String getOriginalExternalTransactionId() {
        String optString;
        if ((optString = this.zzb.optString("originalExternalTransactionId")).isEmpty()) {
            optString = null;
        }
        return optString;
    }
    
    public List<Product> getProducts() {
        return this.zzc;
    }
    
    public static class Product
    {
        private final String zza = jsonObject.optString("productId");
        private final String zzb = jsonObject.optString("productType");
        private final String zzc;
        
        @Override
        public final boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Product)) {
                return false;
            }
            final Product product = (Product)o;
            if (this.zza.equals(product.getId()) && this.zzb.equals(product.getType())) {
                final String zzc = this.zzc;
                final String offerToken = product.getOfferToken();
                if (zzc == offerToken || (zzc != null && zzc.equals(offerToken))) {
                    return true;
                }
            }
            return false;
        }
        
        public String getId() {
            return this.zza;
        }
        
        public String getOfferToken() {
            return this.zzc;
        }
        
        public String getType() {
            return this.zzb;
        }
        
        @Override
        public final int hashCode() {
            return Arrays.hashCode(new Object[] { this.zza, this.zzb, this.zzc });
        }
        
        @Override
        public final String toString() {
            return String.format("{id: %s, type: %s, offer token: %s}", this.zza, this.zzb, this.zzc);
        }
    }
}

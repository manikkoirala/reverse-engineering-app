// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.HashSet;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.Set;
import java.util.ArrayList;

public final class InAppMessageParams
{
    private final ArrayList zza = new ArrayList((Collection<? extends E>)Collections.unmodifiableList((List<?>)new ArrayList<Object>(c)));
    
    public static Builder newBuilder() {
        return new Builder();
    }
    
    final ArrayList zza() {
        return this.zza;
    }
    
    public static final class Builder
    {
        private final Set zza;
        
        public Builder() {
            this.zza = new HashSet();
        }
        
        public Builder addAllInAppMessageCategoriesToShow() {
            this.zza.add(2);
            return this;
        }
        
        public Builder addInAppMessageCategoryToShow(final int i) {
            this.zza.add(i);
            return this;
        }
        
        public InAppMessageParams build() {
            return new InAppMessageParams(this.zza, null);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface InAppMessageCategoryId {
        public static final int TRANSACTIONAL = 2;
        public static final int UNKNOWN_IN_APP_MESSAGE_CATEGORY_ID = 0;
    }
}

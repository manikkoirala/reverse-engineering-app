// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import org.json.JSONException;
import android.text.TextUtils;
import org.json.JSONObject;

@Deprecated
public class SkuDetails
{
    private final String zza;
    private final JSONObject zzb;
    
    public SkuDetails(final String zza) throws JSONException {
        this.zza = zza;
        final JSONObject zzb = new JSONObject(zza);
        this.zzb = zzb;
        if (TextUtils.isEmpty((CharSequence)zzb.optString("productId"))) {
            throw new IllegalArgumentException("SKU cannot be empty.");
        }
        if (!TextUtils.isEmpty((CharSequence)zzb.optString("type"))) {
            return;
        }
        throw new IllegalArgumentException("SkuType cannot be empty.");
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof SkuDetails && TextUtils.equals((CharSequence)this.zza, (CharSequence)((SkuDetails)o).zza));
    }
    
    public String getDescription() {
        return this.zzb.optString("description");
    }
    
    public String getFreeTrialPeriod() {
        return this.zzb.optString("freeTrialPeriod");
    }
    
    public String getIconUrl() {
        return this.zzb.optString("iconUrl");
    }
    
    public String getIntroductoryPrice() {
        return this.zzb.optString("introductoryPrice");
    }
    
    public long getIntroductoryPriceAmountMicros() {
        return this.zzb.optLong("introductoryPriceAmountMicros");
    }
    
    public int getIntroductoryPriceCycles() {
        return this.zzb.optInt("introductoryPriceCycles");
    }
    
    public String getIntroductoryPricePeriod() {
        return this.zzb.optString("introductoryPricePeriod");
    }
    
    public String getOriginalJson() {
        return this.zza;
    }
    
    public String getOriginalPrice() {
        if (this.zzb.has("original_price")) {
            return this.zzb.optString("original_price");
        }
        return this.getPrice();
    }
    
    public long getOriginalPriceAmountMicros() {
        if (this.zzb.has("original_price_micros")) {
            return this.zzb.optLong("original_price_micros");
        }
        return this.getPriceAmountMicros();
    }
    
    public String getPrice() {
        return this.zzb.optString("price");
    }
    
    public long getPriceAmountMicros() {
        return this.zzb.optLong("price_amount_micros");
    }
    
    public String getPriceCurrencyCode() {
        return this.zzb.optString("price_currency_code");
    }
    
    public String getSku() {
        return this.zzb.optString("productId");
    }
    
    public String getSubscriptionPeriod() {
        return this.zzb.optString("subscriptionPeriod");
    }
    
    public String getTitle() {
        return this.zzb.optString("title");
    }
    
    public String getType() {
        return this.zzb.optString("type");
    }
    
    @Override
    public int hashCode() {
        return this.zza.hashCode();
    }
    
    @Override
    public String toString() {
        return "SkuDetails: ".concat(String.valueOf(this.zza));
    }
    
    public int zza() {
        return this.zzb.optInt("offer_type");
    }
    
    public String zzb() {
        return this.zzb.optString("offer_id");
    }
    
    public String zzc() {
        String s;
        if ((s = this.zzb.optString("offerIdToken")).isEmpty()) {
            s = this.zzb.optString("offer_id_token");
        }
        return s;
    }
    
    public final String zzd() {
        return this.zzb.optString("packageName");
    }
    
    public String zze() {
        return this.zzb.optString("serializedDocid");
    }
    
    final String zzf() {
        return this.zzb.optString("skuDetailsToken");
    }
}

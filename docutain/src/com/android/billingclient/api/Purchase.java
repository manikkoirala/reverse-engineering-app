// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.List;
import android.text.TextUtils;
import org.json.JSONArray;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class Purchase
{
    private final String zza;
    private final String zzb;
    private final JSONObject zzc;
    
    public Purchase(final String zza, final String zzb) throws JSONException {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = new JSONObject(zza);
    }
    
    private final ArrayList zza() {
        final ArrayList list = new ArrayList();
        if (this.zzc.has("productIds")) {
            final JSONArray optJSONArray = this.zzc.optJSONArray("productIds");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    list.add(optJSONArray.optString(i));
                }
            }
        }
        else if (this.zzc.has("productId")) {
            list.add(this.zzc.optString("productId"));
        }
        return list;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Purchase)) {
            return false;
        }
        final Purchase purchase = (Purchase)o;
        return TextUtils.equals((CharSequence)this.zza, (CharSequence)purchase.getOriginalJson()) && TextUtils.equals((CharSequence)this.zzb, (CharSequence)purchase.getSignature());
    }
    
    public AccountIdentifiers getAccountIdentifiers() {
        final String optString = this.zzc.optString("obfuscatedAccountId");
        final String optString2 = this.zzc.optString("obfuscatedProfileId");
        if (optString == null && optString2 == null) {
            return null;
        }
        return new AccountIdentifiers(optString, optString2);
    }
    
    public String getDeveloperPayload() {
        return this.zzc.optString("developerPayload");
    }
    
    public String getOrderId() {
        String optString;
        if (TextUtils.isEmpty((CharSequence)(optString = this.zzc.optString("orderId")))) {
            optString = null;
        }
        return optString;
    }
    
    public String getOriginalJson() {
        return this.zza;
    }
    
    public String getPackageName() {
        return this.zzc.optString("packageName");
    }
    
    public List<String> getProducts() {
        return this.zza();
    }
    
    public int getPurchaseState() {
        if (this.zzc.optInt("purchaseState", 1) != 4) {
            return 1;
        }
        return 2;
    }
    
    public long getPurchaseTime() {
        return this.zzc.optLong("purchaseTime");
    }
    
    public String getPurchaseToken() {
        final JSONObject zzc = this.zzc;
        return zzc.optString("token", zzc.optString("purchaseToken"));
    }
    
    public int getQuantity() {
        return this.zzc.optInt("quantity", 1);
    }
    
    public String getSignature() {
        return this.zzb;
    }
    
    @Deprecated
    public ArrayList<String> getSkus() {
        return this.zza();
    }
    
    @Override
    public int hashCode() {
        return this.zza.hashCode();
    }
    
    public boolean isAcknowledged() {
        return this.zzc.optBoolean("acknowledged", true);
    }
    
    public boolean isAutoRenewing() {
        return this.zzc.optBoolean("autoRenewing");
    }
    
    @Override
    public String toString() {
        return "Purchase. Json: ".concat(String.valueOf(this.zza));
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface PurchaseState {
        public static final int PENDING = 2;
        public static final int PURCHASED = 1;
        public static final int UNSPECIFIED_STATE = 0;
    }
}

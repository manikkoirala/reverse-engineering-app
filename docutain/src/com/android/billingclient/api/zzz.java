// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;
import java.util.concurrent.Callable;

final class zzz implements Callable
{
    final String zza;
    final PurchaseHistoryResponseListener zzb;
    final BillingClientImpl zzc;
    
    zzz(final BillingClientImpl zzc, final String zza, final PurchaseHistoryResponseListener zzb) {
        this.zzc = zzc;
        this.zza = zza;
        this.zzb = zzb;
    }
}

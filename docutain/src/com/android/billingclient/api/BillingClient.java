// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.app.Activity;
import android.content.Context;

public abstract class BillingClient
{
    public static Builder newBuilder(final Context context) {
        return new Builder(context, null);
    }
    
    public abstract void acknowledgePurchase(final AcknowledgePurchaseParams p0, final AcknowledgePurchaseResponseListener p1);
    
    public abstract void consumeAsync(final ConsumeParams p0, final ConsumeResponseListener p1);
    
    public abstract void endConnection();
    
    public abstract int getConnectionState();
    
    public abstract BillingResult isFeatureSupported(final String p0);
    
    public abstract boolean isReady();
    
    public abstract BillingResult launchBillingFlow(final Activity p0, final BillingFlowParams p1);
    
    public abstract void queryProductDetailsAsync(final QueryProductDetailsParams p0, final ProductDetailsResponseListener p1);
    
    public abstract void queryPurchaseHistoryAsync(final QueryPurchaseHistoryParams p0, final PurchaseHistoryResponseListener p1);
    
    @Deprecated
    public abstract void queryPurchaseHistoryAsync(final String p0, final PurchaseHistoryResponseListener p1);
    
    public abstract void queryPurchasesAsync(final QueryPurchasesParams p0, final PurchasesResponseListener p1);
    
    @Deprecated
    public abstract void queryPurchasesAsync(final String p0, final PurchasesResponseListener p1);
    
    @Deprecated
    public abstract void querySkuDetailsAsync(final SkuDetailsParams p0, final SkuDetailsResponseListener p1);
    
    public abstract BillingResult showInAppMessages(final Activity p0, final InAppMessageParams p1, final InAppMessageResponseListener p2);
    
    public abstract void startConnection(final BillingClientStateListener p0);
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface BillingResponseCode {
        public static final int BILLING_UNAVAILABLE = 3;
        public static final int DEVELOPER_ERROR = 5;
        public static final int ERROR = 6;
        public static final int FEATURE_NOT_SUPPORTED = -2;
        public static final int ITEM_ALREADY_OWNED = 7;
        public static final int ITEM_NOT_OWNED = 8;
        public static final int ITEM_UNAVAILABLE = 4;
        public static final int NETWORK_ERROR = 12;
        public static final int OK = 0;
        public static final int SERVICE_DISCONNECTED = -1;
        @Deprecated
        public static final int SERVICE_TIMEOUT = -3;
        public static final int SERVICE_UNAVAILABLE = 2;
        public static final int USER_CANCELED = 1;
    }
    
    public static final class Builder
    {
        private volatile String zza;
        private volatile zzbe zzb;
        private final Context zzc = zzc;
        private volatile PurchasesUpdatedListener zzd;
        private volatile zzaz zze;
        private volatile zzar zzf;
        private volatile AlternativeBillingListener zzg;
        
        public BillingClient build() {
            if (this.zzc == null) {
                throw new IllegalArgumentException("Please provide a valid Context.");
            }
            if (this.zzd == null) {
                throw new IllegalArgumentException("Please provide a valid listener for purchases updates.");
            }
            if (this.zzb == null) {
                throw new IllegalArgumentException("Pending purchases for one-time products must be supported.");
            }
            if (this.zzd == null && this.zzg != null) {
                throw new IllegalArgumentException("Please provide a valid listener for Google Play Billing purchases updates when enabling Alternative Billing.");
            }
            if (this.zzd != null) {
                return new BillingClientImpl(null, this.zzb, this.zzc, this.zzd, this.zzg, null);
            }
            return new BillingClientImpl(null, this.zzb, this.zzc, null, null);
        }
        
        public Builder enableAlternativeBilling(final AlternativeBillingListener zzg) {
            this.zzg = zzg;
            return this;
        }
        
        public Builder enablePendingPurchases() {
            final zzbc zzbc = new zzbc(null);
            zzbc.zza();
            this.zzb = zzbc.zzb();
            return this;
        }
        
        public Builder setListener(final PurchasesUpdatedListener zzd) {
            this.zzd = zzd;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ConnectionState {
        public static final int CLOSED = 3;
        public static final int CONNECTED = 2;
        public static final int CONNECTING = 1;
        public static final int DISCONNECTED = 0;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface FeatureType {
        public static final String IN_APP_MESSAGING = "bbb";
        public static final String PRICE_CHANGE_CONFIRMATION = "priceChangeConfirmation";
        public static final String PRODUCT_DETAILS = "fff";
        public static final String SUBSCRIPTIONS = "subscriptions";
        public static final String SUBSCRIPTIONS_UPDATE = "subscriptionsUpdate";
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ProductType {
        public static final String INAPP = "inapp";
        public static final String SUBS = "subs";
    }
    
    @Deprecated
    @Retention(RetentionPolicy.SOURCE)
    public @interface SkuType {
        public static final String INAPP = "inapp";
        public static final String SUBS = "subs";
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.ArrayList;
import com.google.android.gms.internal.play_billing.zzb;
import android.os.Bundle;

final class zzbl
{
    static zzbk zza(final Bundle bundle, final String s, final String s2) {
        final BillingResult zzj = zzat.zzj;
        if (bundle == null) {
            zzb.zzj("BillingClient", String.format("%s got null owned items list", s2));
            return new zzbk(zzj, 54);
        }
        final int zzb = com.google.android.gms.internal.play_billing.zzb.zzb(bundle, "BillingClient");
        final String zzf = com.google.android.gms.internal.play_billing.zzb.zzf(bundle, "BillingClient");
        final BillingResult.Builder builder = BillingResult.newBuilder();
        builder.setResponseCode(zzb);
        builder.setDebugMessage(zzf);
        final BillingResult build = builder.build();
        if (zzb != 0) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", String.format("%s failed. Response code: %s", s2, zzb));
            return new zzbk(build, 23);
        }
        if (!bundle.containsKey("INAPP_PURCHASE_ITEM_LIST") || !bundle.containsKey("INAPP_PURCHASE_DATA_LIST") || !bundle.containsKey("INAPP_DATA_SIGNATURE_LIST")) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", String.format("Bundle returned from %s doesn't contain required fields.", s2));
            return new zzbk(zzj, 55);
        }
        final ArrayList stringArrayList = bundle.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
        final ArrayList stringArrayList2 = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
        final ArrayList stringArrayList3 = bundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
        if (stringArrayList == null) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", String.format("Bundle returned from %s contains null SKUs list.", s2));
            return new zzbk(zzj, 56);
        }
        if (stringArrayList2 == null) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", String.format("Bundle returned from %s contains null purchases list.", s2));
            return new zzbk(zzj, 57);
        }
        if (stringArrayList3 == null) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", String.format("Bundle returned from %s contains null signatures list.", s2));
            return new zzbk(zzj, 58);
        }
        return new zzbk(zzat.zzl, 1);
    }
}

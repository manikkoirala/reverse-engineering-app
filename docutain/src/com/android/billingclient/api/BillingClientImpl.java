// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzbx;
import com.google.android.gms.internal.play_billing.zzg;
import java.lang.ref.WeakReference;
import android.os.IBinder;
import android.view.View;
import android.os.ResultReceiver;
import androidx.core.app.BundleCompat;
import android.graphics.Rect;
import java.util.Iterator;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.CancellationException;
import android.os.Parcelable;
import android.app.PendingIntent;
import java.util.concurrent.TimeUnit;
import android.content.pm.PackageManager$NameNotFoundException;
import java.util.Collection;
import java.util.Arrays;
import android.os.RemoteException;
import com.google.android.gms.internal.play_billing.zzfe;
import com.google.android.gms.internal.play_billing.zzfu;
import com.google.android.gms.internal.play_billing.zzfh;
import com.google.android.gms.internal.play_billing.zzfa;
import com.google.android.gms.internal.play_billing.zzff;
import com.google.android.gms.internal.play_billing.zzfw;
import com.google.android.gms.internal.play_billing.zzfj;
import com.google.android.gms.internal.play_billing.zzfb;
import com.google.android.gms.internal.play_billing.zzu;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;
import android.os.Bundle;
import org.json.JSONException;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import com.google.android.gms.internal.play_billing.zzfl;
import com.google.android.gms.internal.play_billing.zzb;
import com.google.android.gms.internal.play_billing.zzfm;
import android.os.Looper;
import android.app.Activity;
import java.util.concurrent.ExecutorService;
import com.google.android.gms.internal.play_billing.zze;
import android.content.Context;
import android.os.Handler;

class BillingClientImpl extends BillingClient
{
    private volatile int zza;
    private final String zzb;
    private final Handler zzc;
    private volatile zzh zzd;
    private Context zze;
    private zzar zzf;
    private volatile zze zzg;
    private volatile zzaf zzh;
    private boolean zzi;
    private boolean zzj;
    private int zzk;
    private boolean zzl;
    private boolean zzm;
    private boolean zzn;
    private boolean zzo;
    private boolean zzp;
    private boolean zzq;
    private boolean zzr;
    private boolean zzs;
    private boolean zzt;
    private boolean zzu;
    private boolean zzv;
    private boolean zzw;
    private zzbe zzx;
    private boolean zzy;
    private ExecutorService zzz;
    
    private BillingClientImpl(final Activity activity, final zzbe zzbe, final String s) {
        this(activity.getApplicationContext(), zzbe, new zzaj(), s, null, null, null);
    }
    
    private BillingClientImpl(final Context context, final zzbe zzbe, final PurchasesUpdatedListener purchasesUpdatedListener, final String zzb, final String s, final AlternativeBillingListener alternativeBillingListener, final zzar zzar) {
        this.zza = 0;
        this.zzc = new Handler(Looper.getMainLooper());
        this.zzk = 0;
        this.initialize(context, purchasesUpdatedListener, zzbe, alternativeBillingListener, this.zzb = zzb, null);
    }
    
    private BillingClientImpl(final String zzb) {
        this.zza = 0;
        this.zzc = new Handler(Looper.getMainLooper());
        this.zzk = 0;
        this.zzb = zzb;
    }
    
    BillingClientImpl(final String s, final zzbe zzbe, final Context context, final PurchasesUpdatedListener purchasesUpdatedListener, final AlternativeBillingListener alternativeBillingListener, final zzar zzar) {
        this(context, zzbe, purchasesUpdatedListener, zzR(), null, alternativeBillingListener, null);
    }
    
    BillingClientImpl(final String s, final zzbe zzx, final Context context, final zzaz zzaz, final zzar zzar) {
        this.zza = 0;
        this.zzc = new Handler(Looper.getMainLooper());
        this.zzk = 0;
        this.zzb = zzR();
        this.zze = context.getApplicationContext();
        final zzfl zzv = zzfm.zzv();
        zzv.zzj(zzR());
        zzv.zzi(this.zze.getPackageName());
        this.zzf = new zzaw(this.zze, (zzfm)((zzbx)zzv).zzc());
        com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Billing client should have a valid listener but the provided is null.");
        this.zzd = new zzh(this.zze, null, this.zzf);
        this.zzx = zzx;
    }
    
    private void initialize(final Context context, final PurchasesUpdatedListener purchasesUpdatedListener, final zzbe zzx, final AlternativeBillingListener alternativeBillingListener, final String s, final zzar zzf) {
        this.zze = context.getApplicationContext();
        final zzfl zzv = zzfm.zzv();
        zzv.zzj(s);
        zzv.zzi(this.zze.getPackageName());
        if (zzf != null) {
            this.zzf = zzf;
        }
        else {
            this.zzf = new zzaw(this.zze, (zzfm)((zzbx)zzv).zzc());
        }
        if (purchasesUpdatedListener == null) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Billing client should have a valid listener but the provided is null.");
        }
        this.zzd = new zzh(this.zze, purchasesUpdatedListener, alternativeBillingListener, this.zzf);
        this.zzx = zzx;
        this.zzy = (alternativeBillingListener != null);
    }
    
    private int launchBillingFlowCpp(final Activity activity, final BillingFlowParams billingFlowParams) {
        return this.launchBillingFlow(activity, billingFlowParams).getResponseCode();
    }
    
    private void startConnection(final long n) {
        final zzaj zzaj = new zzaj(n);
        if (this.isReady()) {
            com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Service connection is valid. No need to re-initialize.");
            this.zzf.zzb(zzaq.zzb(6));
            zzaj.onBillingSetupFinished(zzat.zzl);
            return;
        }
        final int zza = this.zza;
        int n2 = 1;
        if (zza == 1) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Client is already in the process of connecting to billing service.");
            this.zzf.zza(zzaq.zza(37, 6, zzat.zzd));
            zzaj.onBillingSetupFinished(zzat.zzd);
            return;
        }
        if (this.zza == 3) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Client was already closed and can't be reused. Please create another instance.");
            this.zzf.zza(zzaq.zza(38, 6, zzat.zzm));
            zzaj.onBillingSetupFinished(zzat.zzm);
            return;
        }
        this.zza = 1;
        this.zzd.zze();
        com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Starting in-app billing setup.");
        this.zzh = new zzaf(this, zzaj, null);
        final Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        final List queryIntentServices = this.zze.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
            final ResolveInfo resolveInfo = queryIntentServices.get(0);
            if (resolveInfo.serviceInfo != null) {
                final String packageName = resolveInfo.serviceInfo.packageName;
                final String name = resolveInfo.serviceInfo.name;
                if ("com.android.vending".equals(packageName) && name != null) {
                    final ComponentName component = new ComponentName(packageName, name);
                    final Intent intent2 = new Intent(intent);
                    intent2.setComponent(component);
                    intent2.putExtra("playBillingLibraryVersion", this.zzb);
                    if (this.zze.bindService(intent2, (ServiceConnection)this.zzh, 1)) {
                        com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Service was bonded successfully.");
                        return;
                    }
                    com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Connection to Billing service is blocked.");
                    n2 = 39;
                }
                else {
                    com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "The device doesn't have valid Play Store.");
                    n2 = 40;
                }
            }
        }
        else {
            n2 = 41;
        }
        this.zza = 0;
        com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Billing service unavailable on device.");
        this.zzf.zza(zzaq.zza(n2, 6, zzat.zzc));
        zzaj.onBillingSetupFinished(zzat.zzc);
    }
    
    private final Handler zzO() {
        Handler zzc;
        if (Looper.myLooper() == null) {
            zzc = this.zzc;
        }
        else {
            zzc = new Handler(Looper.myLooper());
        }
        return zzc;
    }
    
    private final BillingResult zzP(final BillingResult billingResult) {
        if (Thread.interrupted()) {
            return billingResult;
        }
        this.zzc.post((Runnable)new zzx(this, billingResult));
        return billingResult;
    }
    
    private final BillingResult zzQ() {
        BillingResult billingResult;
        if (this.zza != 0 && this.zza != 3) {
            billingResult = zzat.zzj;
        }
        else {
            billingResult = zzat.zzm;
        }
        return billingResult;
    }
    
    private static String zzR() {
        try {
            return (String)Class.forName("com.android.billingclient.ktx.BuildConfig").getField("VERSION_NAME").get(null);
        }
        catch (final Exception ex) {
            return "6.0.1";
        }
    }
    
    private final Future zzS(final Callable callable, final long n, final Runnable runnable, final Handler handler) {
        if (this.zzz == null) {
            this.zzz = Executors.newFixedThreadPool(com.google.android.gms.internal.play_billing.zzb.zza, new zzab(this));
        }
        try {
            final Future<Object> submit = this.zzz.submit((Callable<Object>)callable);
            handler.postDelayed((Runnable)new zzw(submit, runnable), (long)(n * 0.95));
            return submit;
        }
        catch (final Exception ex) {
            com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", "Async task throws exception!", (Throwable)ex);
            return null;
        }
    }
    
    private final void zzT(final String s, final PurchaseHistoryResponseListener purchaseHistoryResponseListener) {
        if (!this.isReady()) {
            this.zzf.zza(zzaq.zza(2, 11, zzat.zzm));
            purchaseHistoryResponseListener.onPurchaseHistoryResponse(zzat.zzm, null);
            return;
        }
        if (this.zzS(new zzz(this, s, purchaseHistoryResponseListener), 30000L, new zzo(this, purchaseHistoryResponseListener), this.zzO()) == null) {
            final BillingResult zzQ = this.zzQ();
            this.zzf.zza(zzaq.zza(25, 11, zzQ));
            purchaseHistoryResponseListener.onPurchaseHistoryResponse(zzQ, null);
        }
    }
    
    private final void zzU(final String s, final PurchasesResponseListener purchasesResponseListener) {
        if (!this.isReady()) {
            this.zzf.zza(zzaq.zza(2, 9, zzat.zzm));
            purchasesResponseListener.onQueryPurchasesResponse(zzat.zzm, (List<Purchase>)com.google.android.gms.internal.play_billing.zzu.zzk());
            return;
        }
        if (TextUtils.isEmpty((CharSequence)s)) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Please provide a valid product type.");
            this.zzf.zza(zzaq.zza(50, 9, zzat.zzg));
            purchasesResponseListener.onQueryPurchasesResponse(zzat.zzg, (List<Purchase>)com.google.android.gms.internal.play_billing.zzu.zzk());
            return;
        }
        if (this.zzS(new zzy(this, s, purchasesResponseListener), 30000L, new com.android.billingclient.api.zzu(this, purchasesResponseListener), this.zzO()) == null) {
            final BillingResult zzQ = this.zzQ();
            this.zzf.zza(zzaq.zza(25, 9, zzQ));
            purchasesResponseListener.onQueryPurchasesResponse(zzQ, (List<Purchase>)com.google.android.gms.internal.play_billing.zzu.zzk());
        }
    }
    
    private final void zzV(final BillingResult billingResult, final int n, final int n2) {
        if (billingResult.getResponseCode() != 0) {
            final zzar zzf = this.zzf;
            final zzfa zzv = zzfb.zzv();
            final zzfh zzv2 = zzfj.zzv();
            zzv2.zzj(billingResult.getResponseCode());
            zzv2.zzi(billingResult.getDebugMessage());
            zzv2.zzk(n);
            zzv.zzi(zzv2);
            zzv.zzk(5);
            final zzfu zzv3 = zzfw.zzv();
            zzv3.zzi(n2);
            zzv.zzj((zzfw)((zzbx)zzv3).zzc());
            zzf.zza((zzfb)((zzbx)zzv).zzc());
            return;
        }
        final zzar zzf2 = this.zzf;
        final zzfe zzv4 = zzff.zzv();
        zzv4.zzj(5);
        final zzfu zzv5 = zzfw.zzv();
        zzv5.zzi(n2);
        zzv4.zzi((zzfw)((zzbx)zzv5).zzc());
        zzf2.zzb((zzff)((zzbx)zzv4).zzc());
    }
    
    @Override
    public final void acknowledgePurchase(final AcknowledgePurchaseParams acknowledgePurchaseParams, final AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener) {
        if (!this.isReady()) {
            this.zzf.zza(zzaq.zza(2, 3, zzat.zzm));
            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(zzat.zzm);
            return;
        }
        if (TextUtils.isEmpty((CharSequence)acknowledgePurchaseParams.getPurchaseToken())) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Please provide a valid purchase token.");
            this.zzf.zza(zzaq.zza(26, 3, zzat.zzi));
            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(zzat.zzi);
            return;
        }
        if (!this.zzn) {
            this.zzf.zza(zzaq.zza(27, 3, zzat.zzb));
            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(zzat.zzb);
            return;
        }
        if (this.zzS(new zzp(this, acknowledgePurchaseParams, acknowledgePurchaseResponseListener), 30000L, new zzq(this, acknowledgePurchaseResponseListener), this.zzO()) == null) {
            final BillingResult zzQ = this.zzQ();
            this.zzf.zza(zzaq.zza(25, 3, zzQ));
            acknowledgePurchaseResponseListener.onAcknowledgePurchaseResponse(zzQ);
        }
    }
    
    @Override
    public final void consumeAsync(final ConsumeParams consumeParams, final ConsumeResponseListener consumeResponseListener) {
        if (!this.isReady()) {
            this.zzf.zza(zzaq.zza(2, 4, zzat.zzm));
            consumeResponseListener.onConsumeResponse(zzat.zzm, consumeParams.getPurchaseToken());
            return;
        }
        if (this.zzS(new zzm(this, consumeParams, consumeResponseListener), 30000L, new zzn(this, consumeResponseListener, consumeParams), this.zzO()) == null) {
            final BillingResult zzQ = this.zzQ();
            this.zzf.zza(zzaq.zza(25, 4, zzQ));
            consumeResponseListener.onConsumeResponse(zzQ, consumeParams.getPurchaseToken());
        }
    }
    
    @Override
    public final void endConnection() {
        this.zzf.zzb(zzaq.zzb(12));
        try {
            try {
                this.zzd.zzd();
                if (this.zzh != null) {
                    this.zzh.zzc();
                }
                if (this.zzh != null && this.zzg != null) {
                    com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Unbinding from service.");
                    this.zze.unbindService((ServiceConnection)this.zzh);
                    this.zzh = null;
                }
                this.zzg = null;
                final ExecutorService zzz = this.zzz;
                if (zzz != null) {
                    zzz.shutdownNow();
                    this.zzz = null;
                }
                this.zza = 3;
                return;
            }
            finally {}
        }
        catch (final Exception ex) {
            com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", "There was an exception while ending connection!", (Throwable)ex);
            this.zza = 3;
            return;
        }
        this.zza = 3;
    }
    
    @Override
    public final int getConnectionState() {
        return this.zza;
    }
    
    @Override
    public final BillingResult isFeatureSupported(final String obj) {
        if (!this.isReady()) {
            final BillingResult zzm = zzat.zzm;
            if (zzm.getResponseCode() != 0) {
                this.zzf.zza(zzaq.zza(2, 5, zzm));
            }
            else {
                this.zzf.zzb(zzaq.zzb(5));
            }
            return zzat.zzm;
        }
        final int zzD = zzat.zzD;
        int n = 0;
        Label_0359: {
            switch (obj.hashCode()) {
                case 1987365622: {
                    if (obj.equals("subscriptions")) {
                        n = 0;
                        break Label_0359;
                    }
                    break;
                }
                case 207616302: {
                    if (obj.equals("priceChangeConfirmation")) {
                        n = 2;
                        break Label_0359;
                    }
                    break;
                }
                case 104265: {
                    if (obj.equals("iii")) {
                        n = 11;
                        break Label_0359;
                    }
                    break;
                }
                case 103272: {
                    if (obj.equals("hhh")) {
                        n = 10;
                        break Label_0359;
                    }
                    break;
                }
                case 102279: {
                    if (obj.equals("ggg")) {
                        n = 9;
                        break Label_0359;
                    }
                    break;
                }
                case 101286: {
                    if (obj.equals("fff")) {
                        n = 8;
                        break Label_0359;
                    }
                    break;
                }
                case 100293: {
                    if (obj.equals("eee")) {
                        n = 7;
                        break Label_0359;
                    }
                    break;
                }
                case 99300: {
                    if (obj.equals("ddd")) {
                        n = 5;
                        break Label_0359;
                    }
                    break;
                }
                case 98307: {
                    if (obj.equals("ccc")) {
                        n = 6;
                        break Label_0359;
                    }
                    break;
                }
                case 97314: {
                    if (obj.equals("bbb")) {
                        n = 3;
                        break Label_0359;
                    }
                    break;
                }
                case 96321: {
                    if (obj.equals("aaa")) {
                        n = 4;
                        break Label_0359;
                    }
                    break;
                }
                case -422092961: {
                    if (obj.equals("subscriptionsUpdate")) {
                        n = 1;
                        break Label_0359;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            default: {
                com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Unsupported feature: ".concat(String.valueOf(obj)));
                this.zzV(zzat.zzy, 34, 1);
                return zzat.zzy;
            }
            case 11: {
                BillingResult billingResult;
                if (this.zzw) {
                    billingResult = zzat.zzl;
                }
                else {
                    billingResult = zzat.zzC;
                }
                this.zzV(billingResult, 60, 13);
                return billingResult;
            }
            case 10: {
                BillingResult billingResult2;
                if (this.zzu) {
                    billingResult2 = zzat.zzl;
                }
                else {
                    billingResult2 = zzat.zzA;
                }
                this.zzV(billingResult2, 33, 12);
                return billingResult2;
            }
            case 9: {
                BillingResult billingResult3;
                if (this.zzu) {
                    billingResult3 = zzat.zzl;
                }
                else {
                    billingResult3 = zzat.zzz;
                }
                this.zzV(billingResult3, 32, 11);
                return billingResult3;
            }
            case 8: {
                BillingResult billingResult4;
                if (this.zzt) {
                    billingResult4 = zzat.zzl;
                }
                else {
                    billingResult4 = zzat.zzv;
                }
                this.zzV(billingResult4, 20, 10);
                return billingResult4;
            }
            case 7: {
                BillingResult billingResult5;
                if (this.zzs) {
                    billingResult5 = zzat.zzl;
                }
                else {
                    billingResult5 = zzat.zzt;
                }
                this.zzV(billingResult5, 61, 9);
                return billingResult5;
            }
            case 6: {
                BillingResult billingResult6;
                if (this.zzs) {
                    billingResult6 = zzat.zzl;
                }
                else {
                    billingResult6 = zzat.zzt;
                }
                this.zzV(billingResult6, 19, 8);
                return billingResult6;
            }
            case 5: {
                BillingResult billingResult7;
                if (this.zzq) {
                    billingResult7 = zzat.zzl;
                }
                else {
                    billingResult7 = zzat.zzu;
                }
                this.zzV(billingResult7, 21, 7);
                return billingResult7;
            }
            case 4: {
                BillingResult billingResult8;
                if (this.zzr) {
                    billingResult8 = zzat.zzl;
                }
                else {
                    billingResult8 = zzat.zzs;
                }
                this.zzV(billingResult8, 31, 6);
                return billingResult8;
            }
            case 3: {
                BillingResult billingResult9;
                if (this.zzp) {
                    billingResult9 = zzat.zzl;
                }
                else {
                    billingResult9 = zzat.zzw;
                }
                this.zzV(billingResult9, 30, 5);
                return billingResult9;
            }
            case 2: {
                BillingResult billingResult10;
                if (this.zzm) {
                    billingResult10 = zzat.zzl;
                }
                else {
                    billingResult10 = zzat.zzr;
                }
                this.zzV(billingResult10, 35, 4);
                return billingResult10;
            }
            case 1: {
                BillingResult billingResult11;
                if (this.zzj) {
                    billingResult11 = zzat.zzl;
                }
                else {
                    billingResult11 = zzat.zzp;
                }
                this.zzV(billingResult11, 10, 3);
                return billingResult11;
            }
            case 0: {
                BillingResult billingResult12;
                if (this.zzi) {
                    billingResult12 = zzat.zzl;
                }
                else {
                    billingResult12 = zzat.zzo;
                }
                this.zzV(billingResult12, 9, 2);
                return billingResult12;
            }
        }
    }
    
    @Override
    public final boolean isReady() {
        return this.zza == 2 && this.zzg != null && this.zzh != null;
    }
    
    @Override
    public final BillingResult launchBillingFlow(Activity ex, BillingFlowParams debugMessage) {
        if (!this.isReady()) {
            this.zzf.zza(zzaq.zza(2, 2, zzat.zzm));
            final BillingResult zzm = zzat.zzm;
            this.zzP(zzm);
            return zzm;
        }
        final ArrayList zzg = debugMessage.zzg();
        final List zzh = debugMessage.zzh();
        final SkuDetails skuDetails = (SkuDetails)com.google.android.gms.internal.play_billing.zzz.zza((Iterable)zzg, (Object)null);
        final BillingFlowParams.ProductDetailsParams productDetailsParams = (BillingFlowParams.ProductDetailsParams)com.google.android.gms.internal.play_billing.zzz.zza((Iterable)zzh, (Object)null);
        String s;
        String s2;
        if (skuDetails != null) {
            s = skuDetails.getSku();
            s2 = skuDetails.getType();
        }
        else {
            s = productDetailsParams.zza().getProductId();
            s2 = productDetailsParams.zza().getProductType();
        }
        if (s2.equals("subs") && !this.zzi) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Current client doesn't support subscriptions.");
            this.zzf.zza(zzaq.zza(9, 2, zzat.zzo));
            final BillingResult zzo = zzat.zzo;
            this.zzP(zzo);
            return zzo;
        }
        if (debugMessage.zzq() && !this.zzl) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Current client doesn't support extra params for buy intent.");
            this.zzf.zza(zzaq.zza(18, 2, zzat.zzh));
            final BillingResult zzh2 = zzat.zzh;
            this.zzP(zzh2);
            return zzh2;
        }
        if (zzg.size() > 1 && !this.zzs) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Current client doesn't support multi-item purchases.");
            this.zzf.zza(zzaq.zza(19, 2, zzat.zzt));
            final BillingResult zzt = zzat.zzt;
            this.zzP(zzt);
            return zzt;
        }
        if (!zzh.isEmpty() && !this.zzt) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Current client doesn't support purchases with ProductDetails.");
            this.zzf.zza(zzaq.zza(20, 2, zzat.zzv));
            final BillingResult zzv = zzat.zzv;
            this.zzP(zzv);
            return zzv;
        }
        Label_1723: {
            if (!this.zzl) {
                break Label_1723;
            }
            final boolean zzn = this.zzn;
            final boolean zzy = this.zzy;
            final String zzb = this.zzb;
            final Bundle bundle = new Bundle();
            bundle.putString("playBillingLibraryVersion", zzb);
            if (debugMessage.zzb() != 0) {
                bundle.putInt("prorationMode", debugMessage.zzb());
            }
            else if (debugMessage.zza() != 0) {
                bundle.putInt("prorationMode", debugMessage.zza());
            }
            if (!TextUtils.isEmpty((CharSequence)debugMessage.zzc())) {
                bundle.putString("accountId", debugMessage.zzc());
            }
            if (!TextUtils.isEmpty((CharSequence)debugMessage.zzd())) {
                bundle.putString("obfuscatedProfileId", debugMessage.zzd());
            }
            if (debugMessage.zzp()) {
                bundle.putBoolean("isOfferPersonalizedByDeveloper", true);
            }
            if (!TextUtils.isEmpty((CharSequence)null)) {
                bundle.putStringArrayList("skusToReplace", new ArrayList((Collection<? extends E>)Arrays.asList(null)));
            }
            if (!TextUtils.isEmpty((CharSequence)debugMessage.zze())) {
                bundle.putString("oldSkuPurchaseToken", debugMessage.zze());
            }
            if (!TextUtils.isEmpty((CharSequence)null)) {
                bundle.putString("oldSkuPurchaseId", (String)null);
            }
            if (!TextUtils.isEmpty((CharSequence)debugMessage.zzf())) {
                bundle.putString("originalExternalTransactionId", debugMessage.zzf());
            }
            if (!TextUtils.isEmpty((CharSequence)null)) {
                bundle.putString("paymentsPurchaseParams", (String)null);
            }
            if (zzn) {
                bundle.putBoolean("enablePendingPurchases", true);
            }
            if (zzy) {
                bundle.putBoolean("enableAlternativeBilling", true);
            }
            BillingFlowParams.ProductDetailsParams productDetailsParams2;
            SkuDetails skuDetails3;
            if (!zzg.isEmpty()) {
                final ArrayList list = new ArrayList();
                final ArrayList list2 = new ArrayList();
                final ArrayList list3 = new ArrayList();
                final ArrayList list4 = new ArrayList();
                final ArrayList list5 = new ArrayList();
                final Iterator iterator = zzg.iterator();
                boolean b = false;
                boolean b2 = false;
                boolean b3 = false;
                boolean b4 = false;
                while (iterator.hasNext()) {
                    final SkuDetails skuDetails2 = (SkuDetails)iterator.next();
                    if (!skuDetails2.zzf().isEmpty()) {
                        list.add(skuDetails2.zzf());
                    }
                    final String zzc = skuDetails2.zzc();
                    final String zzb2 = skuDetails2.zzb();
                    final int zza = skuDetails2.zza();
                    final String zze = skuDetails2.zze();
                    list2.add(zzc);
                    b |= (TextUtils.isEmpty((CharSequence)zzc) ^ true);
                    list3.add(zzb2);
                    final boolean b5 = b2 | (TextUtils.isEmpty((CharSequence)zzb2) ^ true);
                    list4.add(zza);
                    b3 |= (zza != 0);
                    b4 |= (TextUtils.isEmpty((CharSequence)zze) ^ true);
                    list5.add(zze);
                    b2 = b5;
                }
                if (!list.isEmpty()) {
                    bundle.putStringArrayList("skuDetailsTokens", list);
                }
                if (b) {
                    bundle.putStringArrayList("SKU_OFFER_ID_TOKEN_LIST", list2);
                }
                if (b2) {
                    bundle.putStringArrayList("SKU_OFFER_ID_LIST", list3);
                }
                if (b3) {
                    bundle.putIntegerArrayList("SKU_OFFER_TYPE_LIST", list4);
                }
                if (b4) {
                    bundle.putStringArrayList("SKU_SERIALIZED_DOCID_LIST", list5);
                }
                productDetailsParams2 = productDetailsParams;
                skuDetails3 = skuDetails;
                if (zzg.size() > 1) {
                    final ArrayList list6 = new ArrayList<String>(zzg.size() - 1);
                    final ArrayList list7 = new ArrayList<String>(zzg.size() - 1);
                    for (int i = 1; i < zzg.size(); ++i) {
                        list6.add(((SkuDetails)zzg.get(i)).getSku());
                        list7.add(((SkuDetails)zzg.get(i)).getType());
                    }
                    bundle.putStringArrayList("additionalSkus", list6);
                    bundle.putStringArrayList("additionalSkuTypes", list7);
                    productDetailsParams2 = productDetailsParams;
                    skuDetails3 = skuDetails;
                }
            }
            else {
                final ArrayList list8 = new ArrayList<String>(zzh.size() - 1);
                final ArrayList list9 = new ArrayList<String>(zzh.size() - 1);
                final ArrayList list10 = new ArrayList();
                final ArrayList list11 = new ArrayList();
                final ArrayList list12 = new ArrayList();
                for (int j = 0; j < zzh.size(); ++j) {
                    final BillingFlowParams.ProductDetailsParams productDetailsParams3 = zzh.get(j);
                    final ProductDetails zza2 = productDetailsParams3.zza();
                    if (!zza2.zzb().isEmpty()) {
                        list10.add(zza2.zzb());
                    }
                    list11.add(productDetailsParams3.zzb());
                    if (!TextUtils.isEmpty((CharSequence)zza2.zzc())) {
                        list12.add(zza2.zzc());
                    }
                    if (j > 0) {
                        list8.add(((BillingFlowParams.ProductDetailsParams)zzh.get(j)).zza().getProductId());
                        list9.add(((BillingFlowParams.ProductDetailsParams)zzh.get(j)).zza().getProductType());
                    }
                }
                bundle.putStringArrayList("SKU_OFFER_ID_TOKEN_LIST", list11);
                if (!list10.isEmpty()) {
                    bundle.putStringArrayList("skuDetailsTokens", list10);
                }
                if (!list12.isEmpty()) {
                    bundle.putStringArrayList("SKU_SERIALIZED_DOCID_LIST", list12);
                }
                productDetailsParams2 = productDetailsParams;
                skuDetails3 = skuDetails;
                if (!list8.isEmpty()) {
                    bundle.putStringArrayList("additionalSkus", list8);
                    bundle.putStringArrayList("additionalSkuTypes", list9);
                    skuDetails3 = skuDetails;
                    productDetailsParams2 = productDetailsParams;
                }
            }
            if (bundle.containsKey("SKU_OFFER_ID_TOKEN_LIST") && !this.zzq) {
                this.zzf.zza(zzaq.zza(21, 2, zzat.zzu));
                final BillingResult zzu = zzat.zzu;
                this.zzP(zzu);
                return zzu;
            }
            int zzb3 = 0;
            Label_1519: {
                if (skuDetails3 != null && !TextUtils.isEmpty((CharSequence)skuDetails3.zzd())) {
                    bundle.putString("skuPackageName", skuDetails3.zzd());
                }
                else {
                    if (productDetailsParams2 == null || TextUtils.isEmpty((CharSequence)productDetailsParams2.zza().zza())) {
                        zzb3 = 0;
                        break Label_1519;
                    }
                    bundle.putString("skuPackageName", productDetailsParams2.zza().zza());
                }
                zzb3 = 1;
            }
            if (!TextUtils.isEmpty((CharSequence)null)) {
                bundle.putString("accountName", (String)null);
            }
            final Intent intent = ((Activity)ex).getIntent();
            String stringExtra;
            String versionName = null;
            Bundle bundle2;
            String s3;
            String s4;
            Bundle bundle3;
            Block_61_Outer:Label_1624_Outer:
            while (true) {
                if (intent == null) {
                    com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Activity's intent is null.");
                    break Label_1635;
                }
                if (TextUtils.isEmpty((CharSequence)intent.getStringExtra("PROXY_PACKAGE"))) {
                    break Label_1635;
                }
                stringExtra = intent.getStringExtra("PROXY_PACKAGE");
                bundle.putString("proxyPackage", stringExtra);
                try {
                    versionName = this.zze.getPackageManager().getPackageInfo(stringExtra, 0).versionName;
                    bundle2 = bundle;
                    s3 = "proxyPackageVersion";
                    s4 = versionName;
                    bundle2.putString(s3, s4);
                    break Label_1635;
                }
                catch (final PackageManager$NameNotFoundException ex2) {}
                while (true) {
                    try {
                        bundle2 = bundle;
                        s3 = "proxyPackageVersion";
                        s4 = versionName;
                        bundle2.putString(s3, s4);
                        if (this.zzt && !zzh.isEmpty()) {
                            zzb3 = 17;
                        }
                        else if (this.zzr && zzb3 != 0) {
                            zzb3 = 15;
                        }
                        else if (this.zzn) {
                            zzb3 = 9;
                        }
                        else {
                            zzb3 = 6;
                        }
                        debugMessage = (BillingFlowParams)this.zzS(new zzs(this, zzb3, s, s2, debugMessage, bundle), 5000L, null, this.zzc);
                        while (true) {
                            try {
                                bundle3 = ((Future<Bundle>)debugMessage).get(5000L, TimeUnit.MILLISECONDS);
                                zzb3 = com.google.android.gms.internal.play_billing.zzb.zzb(bundle3, "BillingClient");
                                debugMessage = (BillingFlowParams)com.google.android.gms.internal.play_billing.zzb.zzf(bundle3, "BillingClient");
                                if (zzb3 != 0) {
                                    ex = (Exception)new StringBuilder();
                                    ((StringBuilder)ex).append("Unable to buy item, Error response code: ");
                                    ((StringBuilder)ex).append(zzb3);
                                    com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", ((StringBuilder)ex).toString());
                                    ex = (Exception)BillingResult.newBuilder();
                                    ((BillingResult.Builder)ex).setResponseCode(zzb3);
                                    ((BillingResult.Builder)ex).setDebugMessage((String)debugMessage);
                                    ex = (Exception)((BillingResult.Builder)ex).build();
                                    this.zzf.zza(zzaq.zza(3, 2, (BillingResult)ex));
                                    this.zzP((BillingResult)ex);
                                    return (BillingResult)ex;
                                }
                                debugMessage = (BillingFlowParams)new Intent((Context)ex, (Class)ProxyBillingActivity.class);
                                ((Intent)debugMessage).putExtra("BUY_INTENT", (Parcelable)bundle3.getParcelable("BUY_INTENT"));
                                ((Activity)ex).startActivity((Intent)debugMessage);
                                return zzat.zzl;
                            }
                            catch (Exception ex) {
                                com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", "Exception while launching billing flow. Try to reconnect", (Throwable)ex);
                                this.zzf.zza(zzaq.zza(5, 2, zzat.zzm));
                                ex = (Exception)zzat.zzm;
                                this.zzP((BillingResult)ex);
                                return (BillingResult)ex;
                            }
                            catch (final CancellationException ex) {}
                            catch (final TimeoutException ex3) {}
                            com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", "Time out while launching billing flow. Try to reconnect", (Throwable)ex);
                            this.zzf.zza(zzaq.zza(4, 2, zzat.zzn));
                            ex = (Exception)zzat.zzn;
                            this.zzP((BillingResult)ex);
                            return (BillingResult)ex;
                            bundle.putString("proxyPackageVersion", "package not found");
                            continue Block_61_Outer;
                            debugMessage = (BillingFlowParams)this.zzS(new zzt(this, s, s2), 5000L, null, this.zzc);
                            continue Label_1624_Outer;
                        }
                    }
                    catch (final PackageManager$NameNotFoundException ex4) {
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    @Override
    public final void queryProductDetailsAsync(final QueryProductDetailsParams queryProductDetailsParams, final ProductDetailsResponseListener productDetailsResponseListener) {
        if (!this.isReady()) {
            this.zzf.zza(zzaq.zza(2, 7, zzat.zzm));
            productDetailsResponseListener.onProductDetailsResponse(zzat.zzm, new ArrayList<ProductDetails>());
            return;
        }
        if (!this.zzt) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Querying product details is not supported.");
            this.zzf.zza(zzaq.zza(20, 7, zzat.zzv));
            productDetailsResponseListener.onProductDetailsResponse(zzat.zzv, new ArrayList<ProductDetails>());
            return;
        }
        if (this.zzS(new zzk(this, queryProductDetailsParams, productDetailsResponseListener), 30000L, new zzl(this, productDetailsResponseListener), this.zzO()) == null) {
            final BillingResult zzQ = this.zzQ();
            this.zzf.zza(zzaq.zza(25, 7, zzQ));
            productDetailsResponseListener.onProductDetailsResponse(zzQ, new ArrayList<ProductDetails>());
        }
    }
    
    @Override
    public final void queryPurchaseHistoryAsync(final QueryPurchaseHistoryParams queryPurchaseHistoryParams, final PurchaseHistoryResponseListener purchaseHistoryResponseListener) {
        this.zzT(queryPurchaseHistoryParams.zza(), purchaseHistoryResponseListener);
    }
    
    @Override
    public final void queryPurchaseHistoryAsync(final String s, final PurchaseHistoryResponseListener purchaseHistoryResponseListener) {
        this.zzT(s, purchaseHistoryResponseListener);
    }
    
    @Override
    public final void queryPurchasesAsync(final QueryPurchasesParams queryPurchasesParams, final PurchasesResponseListener purchasesResponseListener) {
        this.zzU(queryPurchasesParams.zza(), purchasesResponseListener);
    }
    
    @Override
    public final void queryPurchasesAsync(final String s, final PurchasesResponseListener purchasesResponseListener) {
        this.zzU(s, purchasesResponseListener);
    }
    
    @Override
    public final void querySkuDetailsAsync(final SkuDetailsParams skuDetailsParams, final SkuDetailsResponseListener skuDetailsResponseListener) {
        if (!this.isReady()) {
            this.zzf.zza(zzaq.zza(2, 8, zzat.zzm));
            skuDetailsResponseListener.onSkuDetailsResponse(zzat.zzm, null);
            return;
        }
        final String skuType = skuDetailsParams.getSkuType();
        final List<String> skusList = skuDetailsParams.getSkusList();
        if (TextUtils.isEmpty((CharSequence)skuType)) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Please fix the input params. SKU type can't be empty.");
            this.zzf.zza(zzaq.zza(49, 8, zzat.zzf));
            skuDetailsResponseListener.onSkuDetailsResponse(zzat.zzf, null);
            return;
        }
        if (skusList == null) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Please fix the input params. The list of SKUs can't be empty.");
            this.zzf.zza(zzaq.zza(48, 8, zzat.zze));
            skuDetailsResponseListener.onSkuDetailsResponse(zzat.zze, null);
            return;
        }
        if (this.zzS(new zzj(this, skuType, skusList, null, skuDetailsResponseListener), 30000L, new zzr(this, skuDetailsResponseListener), this.zzO()) == null) {
            final BillingResult zzQ = this.zzQ();
            this.zzf.zza(zzaq.zza(25, 8, zzQ));
            skuDetailsResponseListener.onSkuDetailsResponse(zzQ, null);
        }
    }
    
    @Override
    public final BillingResult showInAppMessages(final Activity activity, final InAppMessageParams inAppMessageParams, final InAppMessageResponseListener inAppMessageResponseListener) {
        if (!this.isReady()) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Service disconnected.");
            return zzat.zzm;
        }
        if (!this.zzp) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Current client doesn't support showing in-app messages.");
            return zzat.zzw;
        }
        final View viewById = activity.findViewById(16908290);
        final IBinder windowToken = viewById.getWindowToken();
        final Rect rect = new Rect();
        viewById.getGlobalVisibleRect(rect);
        final Bundle bundle = new Bundle();
        BundleCompat.putBinder(bundle, "KEY_WINDOW_TOKEN", windowToken);
        bundle.putInt("KEY_DIMEN_LEFT", rect.left);
        bundle.putInt("KEY_DIMEN_TOP", rect.top);
        bundle.putInt("KEY_DIMEN_RIGHT", rect.right);
        bundle.putInt("KEY_DIMEN_BOTTOM", rect.bottom);
        bundle.putString("playBillingLibraryVersion", this.zzb);
        bundle.putIntegerArrayList("KEY_CATEGORY_IDS", inAppMessageParams.zza());
        this.zzS(new zzv(this, bundle, activity, new zzaa(this, this.zzc, inAppMessageResponseListener)), 5000L, null, this.zzc);
        return zzat.zzl;
    }
    
    @Override
    public final void startConnection(final BillingClientStateListener billingClientStateListener) {
        if (this.isReady()) {
            com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Service connection is valid. No need to re-initialize.");
            this.zzf.zzb(zzaq.zzb(6));
            billingClientStateListener.onBillingSetupFinished(zzat.zzl);
            return;
        }
        final int zza = this.zza;
        int n = 1;
        if (zza == 1) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Client is already in the process of connecting to billing service.");
            this.zzf.zza(zzaq.zza(37, 6, zzat.zzd));
            billingClientStateListener.onBillingSetupFinished(zzat.zzd);
            return;
        }
        if (this.zza == 3) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Client was already closed and can't be reused. Please create another instance.");
            this.zzf.zza(zzaq.zza(38, 6, zzat.zzm));
            billingClientStateListener.onBillingSetupFinished(zzat.zzm);
            return;
        }
        this.zza = 1;
        this.zzd.zze();
        com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Starting in-app billing setup.");
        this.zzh = new zzaf(this, billingClientStateListener, null);
        final Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        final List queryIntentServices = this.zze.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
            final ResolveInfo resolveInfo = queryIntentServices.get(0);
            if (resolveInfo.serviceInfo != null) {
                final String packageName = resolveInfo.serviceInfo.packageName;
                final String name = resolveInfo.serviceInfo.name;
                if ("com.android.vending".equals(packageName) && name != null) {
                    final ComponentName component = new ComponentName(packageName, name);
                    final Intent intent2 = new Intent(intent);
                    intent2.setComponent(component);
                    intent2.putExtra("playBillingLibraryVersion", this.zzb);
                    if (this.zze.bindService(intent2, (ServiceConnection)this.zzh, 1)) {
                        com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Service was bonded successfully.");
                        return;
                    }
                    com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Connection to Billing service is blocked.");
                    n = 39;
                }
                else {
                    com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "The device doesn't have valid Play Store.");
                    n = 40;
                }
            }
        }
        else {
            n = 41;
        }
        this.zza = 0;
        com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Billing service unavailable on device.");
        this.zzf.zza(zzaq.zza(n, 6, zzat.zzc));
        billingClientStateListener.onBillingSetupFinished(zzat.zzc);
    }
}

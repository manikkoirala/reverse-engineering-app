// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;

@Deprecated
public interface SkuDetailsResponseListener
{
    void onSkuDetailsResponse(final BillingResult p0, final List<SkuDetails> p1);
}

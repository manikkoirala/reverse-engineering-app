// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzak;
import android.os.Parcelable;
import android.content.IntentSender$SendIntentException;
import android.app.PendingIntent;
import android.os.Bundle;
import com.google.android.gms.internal.play_billing.zzb;
import android.content.Intent;
import android.os.ResultReceiver;
import android.app.Activity;

public class ProxyBillingActivity extends Activity
{
    static final String KEY_IN_APP_MESSAGE_RESULT_RECEIVER = "in_app_message_result_receiver";
    static final String KEY_PRICE_CHANGE_RESULT_RECEIVER = "result_receiver";
    private static final String KEY_SEND_CANCELLED_BROADCAST_IF_FINISHED = "send_cancelled_broadcast_if_finished";
    private static final int REQUEST_CODE_FIRST_PARTY_PURCHASE_FLOW = 110;
    private static final int REQUEST_CODE_IN_APP_MESSAGE_FLOW = 101;
    private static final int REQUEST_CODE_LAUNCH_ACTIVITY = 100;
    private static final String TAG = "ProxyBillingActivity";
    private ResultReceiver inAppMessageResultReceiver;
    private boolean isFlowFromFirstPartyClient;
    private ResultReceiver priceChangeResultReceiver;
    private boolean sendCancelledBroadcastIfFinished;
    
    private Intent makeAlternativeBillingIntent(final String s) {
        final Intent intent = new Intent("com.android.vending.billing.ALTERNATIVE_BILLING");
        intent.setPackage(this.getApplicationContext().getPackageName());
        intent.putExtra("ALTERNATIVE_BILLING_USER_CHOICE_DATA", s);
        return intent;
    }
    
    private Intent makePurchasesUpdatedIntent() {
        final Intent intent = new Intent("com.android.vending.billing.PURCHASES_UPDATED");
        intent.setPackage(this.getApplicationContext().getPackageName());
        return intent;
    }
    
    protected void onActivityResult(int zza, int n, Intent intent) {
        super.onActivityResult(zza, n, intent);
        final Bundle bundle = null;
        final Bundle bundle2 = null;
        if (zza != 100 && zza != 110) {
            if (zza == 101) {
                zza = zzb.zza(intent, "ProxyBillingActivity");
                final ResultReceiver inAppMessageResultReceiver = this.inAppMessageResultReceiver;
                if (inAppMessageResultReceiver != null) {
                    Bundle extras;
                    if (intent == null) {
                        extras = bundle2;
                    }
                    else {
                        extras = intent.getExtras();
                    }
                    inAppMessageResultReceiver.send(zza, extras);
                }
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Got onActivityResult with wrong requestCode: ");
                sb.append(zza);
                sb.append("; skipping...");
                zzb.zzj("ProxyBillingActivity", sb.toString());
            }
        }
        else {
            final int responseCode = zzb.zzd(intent, "ProxyBillingActivity").getResponseCode();
            Label_0206: {
                int i;
                if ((i = n) == -1) {
                    if (responseCode == 0) {
                        n = 0;
                        break Label_0206;
                    }
                    i = -1;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Activity finished with resultCode ");
                sb2.append(i);
                sb2.append(" and billing's responseCode: ");
                sb2.append(responseCode);
                zzb.zzj("ProxyBillingActivity", sb2.toString());
                n = responseCode;
            }
            final ResultReceiver priceChangeResultReceiver = this.priceChangeResultReceiver;
            if (priceChangeResultReceiver != null) {
                Bundle extras2;
                if (intent == null) {
                    extras2 = bundle;
                }
                else {
                    extras2 = intent.getExtras();
                }
                priceChangeResultReceiver.send(n, extras2);
            }
            else {
                if (intent != null) {
                    if (intent.getExtras() != null) {
                        final String string = intent.getExtras().getString("ALTERNATIVE_BILLING_USER_CHOICE_DATA");
                        if (string != null) {
                            intent = this.makeAlternativeBillingIntent(string);
                            intent.putExtra("INTENT_SOURCE", "LAUNCH_BILLING_FLOW");
                        }
                        else {
                            final Intent purchasesUpdatedIntent = this.makePurchasesUpdatedIntent();
                            purchasesUpdatedIntent.putExtras(intent.getExtras());
                            purchasesUpdatedIntent.putExtra("INTENT_SOURCE", "LAUNCH_BILLING_FLOW");
                            intent = purchasesUpdatedIntent;
                        }
                    }
                    else {
                        intent = this.makePurchasesUpdatedIntent();
                        zzb.zzj("ProxyBillingActivity", "Got null bundle!");
                        intent.putExtra("RESPONSE_CODE", 6);
                        intent.putExtra("DEBUG_MESSAGE", "An internal error occurred.");
                        final BillingResult.Builder builder = BillingResult.newBuilder();
                        builder.setResponseCode(6);
                        builder.setDebugMessage("An internal error occurred.");
                        intent.putExtra("FAILURE_LOGGING_PAYLOAD", ((zzak)zzaq.zza(22, 2, builder.build())).zzc());
                        intent.putExtra("INTENT_SOURCE", "LAUNCH_BILLING_FLOW");
                    }
                }
                else {
                    intent = this.makePurchasesUpdatedIntent();
                }
                if (zza == 110) {
                    intent.putExtra("IS_FIRST_PARTY_PURCHASE", true);
                }
                this.sendBroadcast(intent);
            }
        }
        this.sendCancelledBroadcastIfFinished = false;
        this.finish();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            zzb.zzi("ProxyBillingActivity", "Launching Play Store billing flow");
            Label_0182: {
                PendingIntent pendingIntent2;
                if (this.getIntent().hasExtra("BUY_INTENT")) {
                    final PendingIntent pendingIntent = pendingIntent2 = (PendingIntent)this.getIntent().getParcelableExtra("BUY_INTENT");
                    if (this.getIntent().hasExtra("IS_FLOW_FROM_FIRST_PARTY_CLIENT")) {
                        pendingIntent2 = pendingIntent;
                        if (this.getIntent().getBooleanExtra("IS_FLOW_FROM_FIRST_PARTY_CLIENT", false)) {
                            this.isFlowFromFirstPartyClient = true;
                            final int n = 110;
                            pendingIntent2 = pendingIntent;
                            break Label_0182;
                        }
                    }
                }
                else if (this.getIntent().hasExtra("SUBS_MANAGEMENT_INTENT")) {
                    pendingIntent2 = (PendingIntent)this.getIntent().getParcelableExtra("SUBS_MANAGEMENT_INTENT");
                    this.priceChangeResultReceiver = (ResultReceiver)this.getIntent().getParcelableExtra("result_receiver");
                }
                else {
                    if (this.getIntent().hasExtra("IN_APP_MESSAGE_INTENT")) {
                        pendingIntent2 = (PendingIntent)this.getIntent().getParcelableExtra("IN_APP_MESSAGE_INTENT");
                        this.inAppMessageResultReceiver = (ResultReceiver)this.getIntent().getParcelableExtra("in_app_message_result_receiver");
                        final int n = 101;
                        break Label_0182;
                    }
                    pendingIntent2 = null;
                }
                final int n = 100;
                try {
                    this.sendCancelledBroadcastIfFinished = true;
                    this.startIntentSenderForResult(pendingIntent2.getIntentSender(), n, new Intent(), 0, 0, 0);
                    return;
                }
                catch (final IntentSender$SendIntentException ex) {
                    zzb.zzk("ProxyBillingActivity", "Got exception while trying to start a purchase flow.", (Throwable)ex);
                    final ResultReceiver priceChangeResultReceiver = this.priceChangeResultReceiver;
                    if (priceChangeResultReceiver != null) {
                        priceChangeResultReceiver.send(6, (Bundle)null);
                    }
                    else {
                        final ResultReceiver inAppMessageResultReceiver = this.inAppMessageResultReceiver;
                        if (inAppMessageResultReceiver != null) {
                            inAppMessageResultReceiver.send(0, (Bundle)null);
                        }
                        else {
                            final Intent purchasesUpdatedIntent = this.makePurchasesUpdatedIntent();
                            if (this.isFlowFromFirstPartyClient) {
                                purchasesUpdatedIntent.putExtra("IS_FIRST_PARTY_PURCHASE", true);
                            }
                            purchasesUpdatedIntent.putExtra("RESPONSE_CODE", 6);
                            purchasesUpdatedIntent.putExtra("DEBUG_MESSAGE", "An internal error occurred.");
                            this.sendBroadcast(purchasesUpdatedIntent);
                        }
                    }
                    this.sendCancelledBroadcastIfFinished = false;
                    this.finish();
                    return;
                }
            }
        }
        zzb.zzi("ProxyBillingActivity", "Launching Play Store billing flow from savedInstanceState");
        this.sendCancelledBroadcastIfFinished = bundle.getBoolean("send_cancelled_broadcast_if_finished", false);
        if (bundle.containsKey("result_receiver")) {
            this.priceChangeResultReceiver = (ResultReceiver)bundle.getParcelable("result_receiver");
        }
        else if (bundle.containsKey("in_app_message_result_receiver")) {
            this.inAppMessageResultReceiver = (ResultReceiver)bundle.getParcelable("in_app_message_result_receiver");
        }
        this.isFlowFromFirstPartyClient = bundle.getBoolean("IS_FLOW_FROM_FIRST_PARTY_CLIENT", false);
    }
    
    protected void onDestroy() {
        super.onDestroy();
        if (!this.isFinishing()) {
            return;
        }
        if (!this.sendCancelledBroadcastIfFinished) {
            return;
        }
        final Intent purchasesUpdatedIntent = this.makePurchasesUpdatedIntent();
        purchasesUpdatedIntent.putExtra("RESPONSE_CODE", 1);
        purchasesUpdatedIntent.putExtra("DEBUG_MESSAGE", "Billing dialog closed.");
        this.sendBroadcast(purchasesUpdatedIntent);
    }
    
    protected void onSaveInstanceState(final Bundle bundle) {
        final ResultReceiver priceChangeResultReceiver = this.priceChangeResultReceiver;
        if (priceChangeResultReceiver != null) {
            bundle.putParcelable("result_receiver", (Parcelable)priceChangeResultReceiver);
        }
        final ResultReceiver inAppMessageResultReceiver = this.inAppMessageResultReceiver;
        if (inAppMessageResultReceiver != null) {
            bundle.putParcelable("in_app_message_result_receiver", (Parcelable)inAppMessageResultReceiver);
        }
        bundle.putBoolean("send_cancelled_broadcast_if_finished", this.sendCancelledBroadcastIfFinished);
        bundle.putBoolean("IS_FLOW_FROM_FIRST_PARTY_CLIENT", this.isFlowFromFirstPartyClient);
    }
}

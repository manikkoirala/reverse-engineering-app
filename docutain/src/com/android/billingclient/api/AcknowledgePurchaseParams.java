// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

public final class AcknowledgePurchaseParams
{
    private String zza;
    
    private AcknowledgePurchaseParams() {
    }
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    public String getPurchaseToken() {
        return this.zza;
    }
    
    public static final class Builder
    {
        private String zza;
        
        private Builder() {
        }
        
        public AcknowledgePurchaseParams build() {
            final String zza = this.zza;
            if (zza != null) {
                final AcknowledgePurchaseParams acknowledgePurchaseParams = new AcknowledgePurchaseParams(null);
                AcknowledgePurchaseParams.zza(acknowledgePurchaseParams, zza);
                return acknowledgePurchaseParams;
            }
            throw new IllegalArgumentException("Purchase token must be set");
        }
        
        public Builder setPurchaseToken(final String zza) {
            this.zza = zza;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;

public final class zzbj
{
    private final List zza;
    private final BillingResult zzb;
    
    public zzbj(final BillingResult zzb, final List zza) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final BillingResult zza() {
        return this.zzb;
    }
    
    public final List zzb() {
        return this.zza;
    }
}

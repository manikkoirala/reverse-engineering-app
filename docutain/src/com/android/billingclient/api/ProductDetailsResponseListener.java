// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;

public interface ProductDetailsResponseListener
{
    void onProductDetailsResponse(final BillingResult p0, final List<ProductDetails> p1);
}

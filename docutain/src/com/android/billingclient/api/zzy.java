// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzu;
import java.util.List;
import java.util.concurrent.Callable;

final class zzy implements Callable
{
    final String zza;
    final PurchasesResponseListener zzb;
    final BillingClientImpl zzc;
    
    zzy(final BillingClientImpl zzc, final String zza, final PurchasesResponseListener zzb) {
        this.zzc = zzc;
        this.zza = zza;
        this.zzb = zzb;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.os.Build$VERSION;
import android.content.IntentFilter;
import org.json.JSONException;
import com.google.android.gms.internal.play_billing.zzu;
import java.util.List;
import android.content.Intent;
import android.content.Context;
import com.google.android.gms.internal.play_billing.zzb;
import com.google.android.gms.internal.play_billing.zzfb;
import com.google.android.gms.internal.play_billing.zzbn;
import android.os.Bundle;
import android.content.BroadcastReceiver;

final class zzg extends BroadcastReceiver
{
    final zzh zza;
    private final PurchasesUpdatedListener zzb = null;
    private final zzaz zzc = null;
    private final AlternativeBillingListener zzd = null;
    private final zzar zze = zze;
    private boolean zzf;
    
    private final void zze(final Bundle bundle, final BillingResult billingResult, final int n) {
        if (bundle.getByteArray("FAILURE_LOGGING_PAYLOAD") != null) {
            try {
                this.zze.zza(zzfb.zzx(bundle.getByteArray("FAILURE_LOGGING_PAYLOAD"), zzbn.zza()));
                return;
            }
            finally {
                com.google.android.gms.internal.play_billing.zzb.zzj("BillingBroadcastManager", "Failed parsing Api failure.");
                return;
            }
        }
        this.zze.zza(zzaq.zza(23, n, billingResult));
    }
    
    public final void onReceive(Context string, final Intent intent) {
        final Bundle extras = intent.getExtras();
        if (extras == null) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingBroadcastManager", "Bundle is null.");
            this.zze.zza(zzaq.zza(11, 1, zzat.zzj));
            final PurchasesUpdatedListener zzb = this.zzb;
            if (zzb != null) {
                zzb.onPurchasesUpdated(zzat.zzj, null);
            }
            return;
        }
        final BillingResult zzd = com.google.android.gms.internal.play_billing.zzb.zzd(intent, "BillingBroadcastManager");
        final String action = intent.getAction();
        final String string2 = extras.getString("INTENT_SOURCE");
        int n2;
        final int n = n2 = 2;
        if (string2 != "LAUNCH_BILLING_FLOW") {
            if (string2 != null && string2.equals("LAUNCH_BILLING_FLOW")) {
                n2 = n;
            }
            else {
                n2 = 1;
            }
        }
        if (action.equals("com.android.vending.billing.PURCHASES_UPDATED")) {
            final List zzh = com.google.android.gms.internal.play_billing.zzb.zzh(extras);
            if (zzd.getResponseCode() == 0) {
                this.zze.zzb(zzaq.zzb(n2));
            }
            else {
                this.zze(extras, zzd, n2);
            }
            this.zzb.onPurchasesUpdated(zzd, zzh);
            return;
        }
        if (action.equals("com.android.vending.billing.ALTERNATIVE_BILLING")) {
            if (zzd.getResponseCode() != 0) {
                this.zze(extras, zzd, n2);
                this.zzb.onPurchasesUpdated(zzd, (List<Purchase>)zzu.zzk());
                return;
            }
            if (this.zzd == null) {
                com.google.android.gms.internal.play_billing.zzb.zzj("BillingBroadcastManager", "AlternativeBillingListener is null.");
                this.zze.zza(zzaq.zza(15, n2, zzat.zzj));
                this.zzb.onPurchasesUpdated(zzat.zzj, (List<Purchase>)zzu.zzk());
                return;
            }
            string = (Context)extras.getString("ALTERNATIVE_BILLING_USER_CHOICE_DATA");
            if (string == null) {
                com.google.android.gms.internal.play_billing.zzb.zzj("BillingBroadcastManager", "Couldn't find alternative billing user choice data in bundle.");
                this.zze.zza(zzaq.zza(16, n2, zzat.zzj));
                this.zzb.onPurchasesUpdated(zzat.zzj, (List<Purchase>)zzu.zzk());
                return;
            }
            try {
                final AlternativeChoiceDetails alternativeChoiceDetails = new AlternativeChoiceDetails((String)string);
                this.zze.zzb(zzaq.zzb(n2));
                this.zzd.userSelectedAlternativeBilling(alternativeChoiceDetails);
            }
            catch (final JSONException ex) {
                com.google.android.gms.internal.play_billing.zzb.zzj("BillingBroadcastManager", String.format("Error when parsing invalid alternative choice data: [%s]", string));
                this.zze.zza(zzaq.zza(17, n2, zzat.zzj));
                this.zzb.onPurchasesUpdated(zzat.zzj, (List<Purchase>)zzu.zzk());
            }
        }
    }
    
    public final void zzc(final Context context, final IntentFilter intentFilter) {
        if (!this.zzf) {
            if (Build$VERSION.SDK_INT >= 33) {
                context.registerReceiver((BroadcastReceiver)zzh.zza(this.zza), intentFilter, 2);
            }
            else {
                context.registerReceiver((BroadcastReceiver)zzh.zza(this.zza), intentFilter);
            }
            this.zzf = true;
        }
    }
    
    public final void zzd(final Context context) {
        if (this.zzf) {
            context.unregisterReceiver((BroadcastReceiver)zzh.zza(this.zza));
            this.zzf = false;
            return;
        }
        com.google.android.gms.internal.play_billing.zzb.zzj("BillingBroadcastManager", "Receiver is not registered.");
    }
}

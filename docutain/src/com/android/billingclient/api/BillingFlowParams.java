// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.text.TextUtils;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import com.google.android.gms.internal.play_billing.zzm;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import com.google.android.gms.internal.play_billing.zzu;

public class BillingFlowParams
{
    public static final String EXTRA_PARAM_KEY_ACCOUNT_ID = "accountId";
    private boolean zza;
    private String zzb;
    private String zzc;
    private SubscriptionUpdateParams zzd;
    private zzu zze;
    private ArrayList zzf;
    private boolean zzg;
    
    private BillingFlowParams() {
    }
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    @Deprecated
    public final int zza() {
        return this.zzd.zza();
    }
    
    public final int zzb() {
        return this.zzd.zzb();
    }
    
    public final String zzc() {
        return this.zzb;
    }
    
    public final String zzd() {
        return this.zzc;
    }
    
    public final String zze() {
        return this.zzd.zzd();
    }
    
    public final String zzf() {
        return this.zzd.zze();
    }
    
    public final ArrayList zzg() {
        final ArrayList list = new ArrayList();
        list.addAll(this.zzf);
        return list;
    }
    
    public final List zzh() {
        return (List)this.zze;
    }
    
    public final boolean zzp() {
        return this.zzg;
    }
    
    final boolean zzq() {
        return this.zzb != null || this.zzc != null || this.zzd.zze() != null || this.zzd.zza() != 0 || this.zzd.zzb() != 0 || this.zza || this.zzg;
    }
    
    public static class Builder
    {
        private String zza;
        private String zzb;
        private List zzc;
        private ArrayList zzd;
        private boolean zze;
        private SubscriptionUpdateParams.Builder zzf;
        
        private Builder() {
            final SubscriptionUpdateParams.Builder builder = SubscriptionUpdateParams.newBuilder();
            SubscriptionUpdateParams.Builder.zza(builder);
            this.zzf = builder;
        }
        
        public BillingFlowParams build() {
            final ArrayList zzd = this.zzd;
            final boolean b = true;
            final boolean b2 = zzd != null && !zzd.isEmpty();
            final List zzc = this.zzc;
            final boolean b3 = zzc != null && !zzc.isEmpty();
            if (!b2 && !b3) {
                throw new IllegalArgumentException("Details of the products must be provided.");
            }
            if (b2 && b3) {
                throw new IllegalArgumentException("Set SkuDetails or ProductDetailsParams, not both.");
            }
            if (b2) {
                if (this.zzd.contains(null)) {
                    throw new IllegalArgumentException("SKU cannot be null.");
                }
                if (this.zzd.size() > 1) {
                    final SkuDetails skuDetails = this.zzd.get(0);
                    final String type = skuDetails.getType();
                    final ArrayList zzd2 = this.zzd;
                    for (int size = zzd2.size(), i = 0; i < size; ++i) {
                        final SkuDetails skuDetails2 = (SkuDetails)zzd2.get(i);
                        if (!type.equals("play_pass_subs") && !skuDetails2.getType().equals("play_pass_subs") && !type.equals(skuDetails2.getType())) {
                            throw new IllegalArgumentException("SKUs should have the same type.");
                        }
                    }
                    final String zzd3 = skuDetails.zzd();
                    final ArrayList zzd4 = this.zzd;
                    for (int size2 = zzd4.size(), j = 0; j < size2; ++j) {
                        final SkuDetails skuDetails3 = (SkuDetails)zzd4.get(j);
                        if (!type.equals("play_pass_subs") && !skuDetails3.getType().equals("play_pass_subs") && !zzd3.equals(skuDetails3.zzd())) {
                            throw new IllegalArgumentException("All SKUs must have the same package name.");
                        }
                    }
                }
            }
            else {
                final ProductDetailsParams productDetailsParams = this.zzc.get(0);
                for (int k = 0; k < this.zzc.size(); ++k) {
                    final ProductDetailsParams productDetailsParams2 = this.zzc.get(k);
                    if (productDetailsParams2 == null) {
                        throw new IllegalArgumentException("ProductDetailsParams cannot be null.");
                    }
                    if (k != 0 && !productDetailsParams2.zza().getProductType().equals(productDetailsParams.zza().getProductType()) && !productDetailsParams2.zza().getProductType().equals("play_pass_subs")) {
                        throw new IllegalArgumentException("All products should have same ProductType.");
                    }
                }
                final String zza = productDetailsParams.zza().zza();
                for (final ProductDetailsParams productDetailsParams3 : this.zzc) {
                    if (!productDetailsParams.zza().getProductType().equals("play_pass_subs") && !productDetailsParams3.zza().getProductType().equals("play_pass_subs")) {
                        if (zza.equals(productDetailsParams3.zza().zza())) {
                            continue;
                        }
                        throw new IllegalArgumentException("All products must have the same package name.");
                    }
                }
            }
            final BillingFlowParams billingFlowParams = new BillingFlowParams(null);
            boolean b4 = false;
            Label_0646: {
                if (b2) {
                    b4 = b;
                    if (!this.zzd.get(0).zzd().isEmpty()) {
                        break Label_0646;
                    }
                }
                b4 = (b3 && !((ProductDetailsParams)this.zzc.get(0)).zza().zza().isEmpty() && b);
            }
            BillingFlowParams.zzi(billingFlowParams, b4);
            BillingFlowParams.zzk(billingFlowParams, this.zza);
            BillingFlowParams.zzl(billingFlowParams, this.zzb);
            BillingFlowParams.zzo(billingFlowParams, this.zzf.build());
            final ArrayList zzd5 = this.zzd;
            ArrayList list;
            if (zzd5 != null) {
                list = new ArrayList(zzd5);
            }
            else {
                list = new ArrayList();
            }
            BillingFlowParams.zzn(billingFlowParams, (ArrayList)list);
            BillingFlowParams.zzj(billingFlowParams, this.zze);
            final List zzc2 = this.zzc;
            zzu zzu;
            if (zzc2 != null) {
                zzu = com.google.android.gms.internal.play_billing.zzu.zzj((Collection)zzc2);
            }
            else {
                zzu = com.google.android.gms.internal.play_billing.zzu.zzk();
            }
            BillingFlowParams.zzm(billingFlowParams, zzu);
            return billingFlowParams;
        }
        
        public Builder setIsOfferPersonalized(final boolean zze) {
            this.zze = zze;
            return this;
        }
        
        public Builder setObfuscatedAccountId(final String zza) {
            this.zza = zza;
            return this;
        }
        
        public Builder setObfuscatedProfileId(final String zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setProductDetailsParamsList(final List<ProductDetailsParams> c) {
            this.zzc = new ArrayList(c);
            return this;
        }
        
        @Deprecated
        public Builder setSkuDetails(final SkuDetails e) {
            final ArrayList zzd = new ArrayList();
            zzd.add(e);
            this.zzd = zzd;
            return this;
        }
        
        public Builder setSubscriptionUpdateParams(final SubscriptionUpdateParams subscriptionUpdateParams) {
            this.zzf = SubscriptionUpdateParams.zzc(subscriptionUpdateParams);
            return this;
        }
    }
    
    public static final class ProductDetailsParams
    {
        private final ProductDetails zza = builder.zza;
        private final String zzb = builder.zzb;
        
        public static Builder newBuilder() {
            return new Builder(null);
        }
        
        public final ProductDetails zza() {
            return this.zza;
        }
        
        public final String zzb() {
            return this.zzb;
        }
        
        public static class Builder
        {
            private ProductDetails zza;
            private String zzb;
            
            private Builder() {
            }
            
            public ProductDetailsParams build() {
                zzm.zzc((Object)this.zza, (Object)"ProductDetails is required for constructing ProductDetailsParams.");
                zzm.zzc((Object)this.zzb, (Object)"offerToken is required for constructing ProductDetailsParams.");
                return new ProductDetailsParams(this, null);
            }
            
            public Builder setOfferToken(final String zzb) {
                this.zzb = zzb;
                return this;
            }
            
            public Builder setProductDetails(final ProductDetails zza) {
                this.zza = zza;
                if (zza.getOneTimePurchaseOfferDetails() != null) {
                    zza.getOneTimePurchaseOfferDetails().getClass();
                    this.zzb = zza.getOneTimePurchaseOfferDetails().zza();
                }
                return this;
            }
        }
    }
    
    @Deprecated
    @Retention(RetentionPolicy.SOURCE)
    public @interface ProrationMode {
        public static final int DEFERRED = 4;
        public static final int IMMEDIATE_AND_CHARGE_FULL_PRICE = 5;
        public static final int IMMEDIATE_AND_CHARGE_PRORATED_PRICE = 2;
        public static final int IMMEDIATE_WITHOUT_PRORATION = 3;
        public static final int IMMEDIATE_WITH_TIME_PRORATION = 1;
        public static final int UNKNOWN_SUBSCRIPTION_UPGRADE_DOWNGRADE_POLICY = 0;
    }
    
    public static class SubscriptionUpdateParams
    {
        private String zza;
        private String zzb;
        private int zzc = 0;
        private int zzd = 0;
        
        private SubscriptionUpdateParams() {
            this.zzc = 0;
            this.zzd = 0;
        }
        
        public static Builder newBuilder() {
            return new Builder(null);
        }
        
        @Deprecated
        final int zza() {
            return this.zzc;
        }
        
        final int zzb() {
            return this.zzd;
        }
        
        final String zzd() {
            return this.zza;
        }
        
        final String zze() {
            return this.zzb;
        }
        
        public static class Builder
        {
            private String zza;
            private String zzb;
            private boolean zzc;
            private int zzd = 0;
            private int zze = 0;
            
            private Builder() {
                this.zzd = 0;
                this.zze = 0;
            }
            
            static /* synthetic */ Builder zza(final Builder builder) {
                builder.zzc = true;
                return builder;
            }
            
            public SubscriptionUpdateParams build() {
                final boolean b = !TextUtils.isEmpty((CharSequence)this.zza) || !TextUtils.isEmpty((CharSequence)null);
                final boolean b2 = true ^ TextUtils.isEmpty((CharSequence)this.zzb);
                if (b && b2) {
                    throw new IllegalArgumentException("Please provide Old SKU purchase information(token/id) or original external transaction id, not both.");
                }
                if (!this.zzc && !b && !b2) {
                    throw new IllegalArgumentException("Old SKU purchase information(token/id) or original external transaction id must be provided.");
                }
                final SubscriptionUpdateParams subscriptionUpdateParams = new SubscriptionUpdateParams(null);
                SubscriptionUpdateParams.zzf(subscriptionUpdateParams, this.zza);
                SubscriptionUpdateParams.zzh(subscriptionUpdateParams, this.zzd);
                SubscriptionUpdateParams.zzi(subscriptionUpdateParams, this.zze);
                SubscriptionUpdateParams.zzg(subscriptionUpdateParams, this.zzb);
                return subscriptionUpdateParams;
            }
            
            public Builder setOldPurchaseToken(final String zza) {
                this.zza = zza;
                return this;
            }
            
            @Deprecated
            public Builder setOldSkuPurchaseToken(final String zza) {
                this.zza = zza;
                return this;
            }
            
            public Builder setOriginalExternalTransactionId(final String zzb) {
                this.zzb = zzb;
                return this;
            }
            
            @Deprecated
            public Builder setReplaceProrationMode(final int zzd) {
                this.zzd = zzd;
                return this;
            }
            
            @Deprecated
            public Builder setReplaceSkusProrationMode(final int zzd) {
                this.zzd = zzd;
                return this;
            }
            
            public Builder setSubscriptionReplacementMode(final int zze) {
                this.zze = zze;
                return this;
            }
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface ReplacementMode {
            public static final int CHARGE_FULL_PRICE = 5;
            public static final int CHARGE_PRORATED_PRICE = 2;
            public static final int DEFERRED = 6;
            public static final int UNKNOWN_REPLACEMENT_MODE = 0;
            public static final int WITHOUT_PRORATION = 3;
            public static final int WITH_TIME_PRORATION = 1;
        }
    }
}

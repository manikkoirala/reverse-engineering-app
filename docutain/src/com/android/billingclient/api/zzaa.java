// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzb;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

final class zzaa extends ResultReceiver
{
    final InAppMessageResponseListener zza;
    
    zzaa(final BillingClientImpl billingClientImpl, final Handler handler, final InAppMessageResponseListener zza) {
        this.zza = zza;
        super(handler);
    }
    
    public final void onReceiveResult(final int n, final Bundle bundle) {
        this.zza.onInAppMessageResponse(zzb.zze(bundle, "BillingClient"));
    }
}

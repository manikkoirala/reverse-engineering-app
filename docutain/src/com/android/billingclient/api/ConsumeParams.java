// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

public final class ConsumeParams
{
    private String zza;
    
    private ConsumeParams() {
    }
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    public String getPurchaseToken() {
        return this.zza;
    }
    
    public static final class Builder
    {
        private String zza;
        
        private Builder() {
        }
        
        public ConsumeParams build() {
            final String zza = this.zza;
            if (zza != null) {
                final ConsumeParams consumeParams = new ConsumeParams(null);
                ConsumeParams.zza(consumeParams, zza);
                return consumeParams;
            }
            throw new IllegalArgumentException("Purchase token must be set");
        }
        
        public Builder setPurchaseToken(final String zza) {
            this.zza = zza;
            return this;
        }
    }
}

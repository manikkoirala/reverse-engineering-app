// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzb;

public final class BillingResult
{
    private int zza;
    private String zzb;
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    public String getDebugMessage() {
        return this.zzb;
    }
    
    public int getResponseCode() {
        return this.zza;
    }
    
    @Override
    public String toString() {
        final String zzg = com.google.android.gms.internal.play_billing.zzb.zzg(this.zza);
        final String zzb = this.zzb;
        final StringBuilder sb = new StringBuilder();
        sb.append("Response Code: ");
        sb.append(zzg);
        sb.append(", Debug Message: ");
        sb.append(zzb);
        return sb.toString();
    }
    
    public static class Builder
    {
        private int zza;
        private String zzb = "";
        
        private Builder() {
            this.zzb = "";
        }
        
        public BillingResult build() {
            final BillingResult billingResult = new BillingResult();
            BillingResult.zzb(billingResult, this.zza);
            BillingResult.zza(billingResult, this.zzb);
            return billingResult;
        }
        
        public Builder setDebugMessage(final String zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setResponseCode(final int zza) {
            this.zza = zza;
            return this;
        }
    }
}

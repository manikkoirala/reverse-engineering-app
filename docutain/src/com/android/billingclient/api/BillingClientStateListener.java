// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

public interface BillingClientStateListener
{
    void onBillingServiceDisconnected();
    
    void onBillingSetupFinished(final BillingResult p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;

public interface PurchasesUpdatedListener
{
    void onPurchasesUpdated(final BillingResult p0, final List<Purchase> p1);
}

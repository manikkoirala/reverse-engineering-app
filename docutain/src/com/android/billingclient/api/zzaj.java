// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.Collections;
import java.util.List;

final class zzaj implements AcknowledgePurchaseResponseListener, BillingClientStateListener, ConsumeResponseListener, PurchaseHistoryResponseListener, PurchasesResponseListener, PurchasesUpdatedListener, SkuDetailsResponseListener
{
    private final long zza;
    
    zzaj() {
        this.zza = 0L;
    }
    
    zzaj(final long zza) {
        this.zza = zza;
    }
    
    public static native void nativeOnAcknowledgePurchaseResponse(final int p0, final String p1, final long p2);
    
    public static native void nativeOnBillingServiceDisconnected();
    
    public static native void nativeOnBillingSetupFinished(final int p0, final String p1, final long p2);
    
    public static native void nativeOnConsumePurchaseResponse(final int p0, final String p1, final String p2, final long p3);
    
    public static native void nativeOnPriceChangeConfirmationResult(final int p0, final String p1, final long p2);
    
    public static native void nativeOnPurchaseHistoryResponse(final int p0, final String p1, final PurchaseHistoryRecord[] p2, final long p3);
    
    public static native void nativeOnPurchasesUpdated(final int p0, final String p1, final Purchase[] p2);
    
    public static native void nativeOnQueryPurchasesResponse(final int p0, final String p1, final Purchase[] p2, final long p3);
    
    public static native void nativeOnSkuDetailsResponse(final int p0, final String p1, final SkuDetails[] p2, final long p3);
    
    @Override
    public final void onAcknowledgePurchaseResponse(final BillingResult billingResult) {
        nativeOnAcknowledgePurchaseResponse(billingResult.getResponseCode(), billingResult.getDebugMessage(), this.zza);
    }
    
    @Override
    public final void onBillingServiceDisconnected() {
        nativeOnBillingServiceDisconnected();
    }
    
    @Override
    public final void onBillingSetupFinished(final BillingResult billingResult) {
        nativeOnBillingSetupFinished(billingResult.getResponseCode(), billingResult.getDebugMessage(), this.zza);
    }
    
    @Override
    public final void onConsumeResponse(final BillingResult billingResult, final String s) {
        nativeOnConsumePurchaseResponse(billingResult.getResponseCode(), billingResult.getDebugMessage(), s, this.zza);
    }
    
    @Override
    public final void onPurchaseHistoryResponse(final BillingResult billingResult, final List<PurchaseHistoryRecord> list) {
        Object emptyList = list;
        if (list == null) {
            emptyList = Collections.emptyList();
        }
        nativeOnPurchaseHistoryResponse(billingResult.getResponseCode(), billingResult.getDebugMessage(), (PurchaseHistoryRecord[])((List)emptyList).toArray(new PurchaseHistoryRecord[((List)emptyList).size()]), this.zza);
    }
    
    @Override
    public final void onPurchasesUpdated(final BillingResult billingResult, final List<Purchase> list) {
        Object emptyList = list;
        if (list == null) {
            emptyList = Collections.emptyList();
        }
        nativeOnPurchasesUpdated(billingResult.getResponseCode(), billingResult.getDebugMessage(), (Purchase[])((List)emptyList).toArray(new Purchase[((List)emptyList).size()]));
    }
    
    @Override
    public final void onQueryPurchasesResponse(final BillingResult billingResult, final List<Purchase> list) {
        nativeOnQueryPurchasesResponse(billingResult.getResponseCode(), billingResult.getDebugMessage(), list.toArray(new Purchase[list.size()]), this.zza);
    }
    
    @Override
    public final void onSkuDetailsResponse(final BillingResult billingResult, final List<SkuDetails> list) {
        Object emptyList = list;
        if (list == null) {
            emptyList = Collections.emptyList();
        }
        nativeOnSkuDetailsResponse(billingResult.getResponseCode(), billingResult.getDebugMessage(), (SkuDetails[])((List)emptyList).toArray(new SkuDetails[((List)emptyList).size()]), this.zza);
    }
}

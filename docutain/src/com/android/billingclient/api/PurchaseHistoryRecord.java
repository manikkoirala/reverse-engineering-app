// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;
import android.text.TextUtils;
import org.json.JSONArray;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class PurchaseHistoryRecord
{
    private final String zza;
    private final String zzb;
    private final JSONObject zzc;
    
    public PurchaseHistoryRecord(final String zza, final String zzb) throws JSONException {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = new JSONObject(zza);
    }
    
    private final ArrayList zza() {
        final ArrayList list = new ArrayList();
        if (this.zzc.has("productIds")) {
            final JSONArray optJSONArray = this.zzc.optJSONArray("productIds");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    list.add(optJSONArray.optString(i));
                }
            }
        }
        else if (this.zzc.has("productId")) {
            list.add(this.zzc.optString("productId"));
        }
        return list;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PurchaseHistoryRecord)) {
            return false;
        }
        final PurchaseHistoryRecord purchaseHistoryRecord = (PurchaseHistoryRecord)o;
        return TextUtils.equals((CharSequence)this.zza, (CharSequence)purchaseHistoryRecord.getOriginalJson()) && TextUtils.equals((CharSequence)this.zzb, (CharSequence)purchaseHistoryRecord.getSignature());
    }
    
    public String getDeveloperPayload() {
        return this.zzc.optString("developerPayload");
    }
    
    public String getOriginalJson() {
        return this.zza;
    }
    
    public List<String> getProducts() {
        return this.zza();
    }
    
    public long getPurchaseTime() {
        return this.zzc.optLong("purchaseTime");
    }
    
    public String getPurchaseToken() {
        final JSONObject zzc = this.zzc;
        return zzc.optString("token", zzc.optString("purchaseToken"));
    }
    
    public int getQuantity() {
        return this.zzc.optInt("quantity", 1);
    }
    
    public String getSignature() {
        return this.zzb;
    }
    
    @Deprecated
    public ArrayList<String> getSkus() {
        return this.zza();
    }
    
    @Override
    public int hashCode() {
        return this.zza.hashCode();
    }
    
    @Override
    public String toString() {
        return "PurchaseHistoryRecord. Json: ".concat(String.valueOf(this.zza));
    }
}

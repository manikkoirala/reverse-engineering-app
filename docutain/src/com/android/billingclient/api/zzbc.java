// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

public final class zzbc
{
    private boolean zza;
    
    private zzbc() {
    }
    
    public final zzbc zza() {
        this.zza = true;
        return this;
    }
    
    public final zzbe zzb() {
        if (this.zza) {
            return new zzbe(true, false, null);
        }
        throw new IllegalArgumentException("Pending purchases for one-time products must be supported.");
    }
}

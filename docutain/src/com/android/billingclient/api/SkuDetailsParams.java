// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class SkuDetailsParams
{
    private String zza;
    private List zzb;
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    public String getSkuType() {
        return this.zza;
    }
    
    public List<String> getSkusList() {
        return this.zzb;
    }
    
    public static class Builder
    {
        private String zza;
        private List zzb;
        
        private Builder() {
        }
        
        public SkuDetailsParams build() {
            final String zza = this.zza;
            if (zza == null) {
                throw new IllegalArgumentException("SKU type must be set");
            }
            if (this.zzb != null) {
                final SkuDetailsParams skuDetailsParams = new SkuDetailsParams();
                SkuDetailsParams.zza(skuDetailsParams, zza);
                SkuDetailsParams.zzb(skuDetailsParams, this.zzb);
                return skuDetailsParams;
            }
            throw new IllegalArgumentException("SKU list must be set");
        }
        
        public Builder setSkusList(final List<String> c) {
            this.zzb = new ArrayList(c);
            return this;
        }
        
        public Builder setType(final String zza) {
            this.zza = zza;
            return this;
        }
    }
}

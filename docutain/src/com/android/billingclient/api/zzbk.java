// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

final class zzbk
{
    private final BillingResult zza;
    private final int zzb;
    
    zzbk(final BillingResult zza, final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    final BillingResult zza() {
        return this.zza;
    }
    
    final int zzb() {
        return this.zzb;
    }
}

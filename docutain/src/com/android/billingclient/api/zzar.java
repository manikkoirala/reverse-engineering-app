// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzgd;
import com.google.android.gms.internal.play_billing.zzff;
import com.google.android.gms.internal.play_billing.zzfb;

interface zzar
{
    void zza(final zzfb p0);
    
    void zzb(final zzff p0);
    
    void zzc(final zzgd p0);
}

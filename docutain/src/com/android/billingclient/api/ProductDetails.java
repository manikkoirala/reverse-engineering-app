// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Collection;
import com.google.android.gms.internal.play_billing.zzu;
import org.json.JSONException;
import org.json.JSONArray;
import java.util.ArrayList;
import android.text.TextUtils;
import java.util.List;
import org.json.JSONObject;

public final class ProductDetails
{
    private final String zza;
    private final JSONObject zzb;
    private final String zzc;
    private final String zzd;
    private final String zze;
    private final String zzf;
    private final String zzg;
    private final String zzh;
    private final String zzi;
    private final String zzj;
    private final String zzk;
    private final List zzl;
    private final List zzm;
    
    ProductDetails(String optString) throws JSONException {
        this.zza = optString;
        final JSONObject zzb = new JSONObject(optString);
        this.zzb = zzb;
        final String optString2 = zzb.optString("productId");
        this.zzc = optString2;
        optString = zzb.optString("type");
        this.zzd = optString;
        if (TextUtils.isEmpty((CharSequence)optString2)) {
            throw new IllegalArgumentException("Product id cannot be empty.");
        }
        if (TextUtils.isEmpty((CharSequence)optString)) {
            throw new IllegalArgumentException("Product type cannot be empty.");
        }
        this.zze = zzb.optString("title");
        this.zzf = zzb.optString("name");
        this.zzg = zzb.optString("description");
        this.zzi = zzb.optString("packageDisplayName");
        this.zzj = zzb.optString("iconUrl");
        this.zzh = zzb.optString("skuDetailsToken");
        this.zzk = zzb.optString("serializedDocid");
        final JSONArray optJSONArray = zzb.optJSONArray("subscriptionOfferDetails");
        final int n = 0;
        if (optJSONArray != null) {
            final ArrayList zzl = new ArrayList();
            for (int i = 0; i < optJSONArray.length(); ++i) {
                zzl.add(new SubscriptionOfferDetails(optJSONArray.getJSONObject(i)));
            }
            this.zzl = zzl;
        }
        else {
            List zzl2;
            if (!optString.equals("subs") && !optString.equals("play_pass_subs")) {
                zzl2 = null;
            }
            else {
                zzl2 = new ArrayList();
            }
            this.zzl = zzl2;
        }
        final JSONObject optJSONObject = this.zzb.optJSONObject("oneTimePurchaseOfferDetails");
        final JSONArray optJSONArray2 = this.zzb.optJSONArray("oneTimePurchaseOfferDetailsList");
        final ArrayList list = new ArrayList();
        if (optJSONArray2 != null) {
            for (int j = n; j < optJSONArray2.length(); ++j) {
                list.add(new OneTimePurchaseOfferDetails(optJSONArray2.getJSONObject(j)));
            }
            this.zzm = list;
            return;
        }
        if (optJSONObject != null) {
            list.add(new OneTimePurchaseOfferDetails(optJSONObject));
            this.zzm = list;
            return;
        }
        this.zzm = null;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof ProductDetails && TextUtils.equals((CharSequence)this.zza, (CharSequence)((ProductDetails)o).zza));
    }
    
    public String getDescription() {
        return this.zzg;
    }
    
    public String getName() {
        return this.zzf;
    }
    
    public OneTimePurchaseOfferDetails getOneTimePurchaseOfferDetails() {
        final List zzm = this.zzm;
        if (zzm != null && !zzm.isEmpty()) {
            return this.zzm.get(0);
        }
        return null;
    }
    
    public String getProductId() {
        return this.zzc;
    }
    
    public String getProductType() {
        return this.zzd;
    }
    
    public List<SubscriptionOfferDetails> getSubscriptionOfferDetails() {
        return this.zzl;
    }
    
    public String getTitle() {
        return this.zze;
    }
    
    @Override
    public int hashCode() {
        return this.zza.hashCode();
    }
    
    @Override
    public String toString() {
        final String zza = this.zza;
        final String string = this.zzb.toString();
        final String zzc = this.zzc;
        final String zzd = this.zzd;
        final String zze = this.zze;
        final String zzh = this.zzh;
        final String value = String.valueOf(this.zzl);
        final StringBuilder sb = new StringBuilder();
        sb.append("ProductDetails{jsonString='");
        sb.append(zza);
        sb.append("', parsedJson=");
        sb.append(string);
        sb.append(", productId='");
        sb.append(zzc);
        sb.append("', productType='");
        sb.append(zzd);
        sb.append("', title='");
        sb.append(zze);
        sb.append("', productDetailsToken='");
        sb.append(zzh);
        sb.append("', subscriptionOfferDetails=");
        sb.append(value);
        sb.append("}");
        return sb.toString();
    }
    
    public final String zza() {
        return this.zzb.optString("packageName");
    }
    
    final String zzb() {
        return this.zzh;
    }
    
    public String zzc() {
        return this.zzk;
    }
    
    public static final class OneTimePurchaseOfferDetails
    {
        private final String zza;
        private final long zzb;
        private final String zzc;
        private final String zzd;
        private final String zze;
        private final zzu zzf;
        private final Long zzg;
        private final zzbg zzh;
        private final zzbi zzi;
        private final zzbh zzj;
        
        OneTimePurchaseOfferDetails(JSONObject optJSONObject) throws JSONException {
            this.zza = optJSONObject.optString("formattedPrice");
            this.zzb = optJSONObject.optLong("priceAmountMicros");
            this.zzc = optJSONObject.optString("priceCurrencyCode");
            this.zzd = optJSONObject.optString("offerIdToken");
            this.zze = optJSONObject.optString("offerId");
            optJSONObject.optInt("offerType");
            final JSONArray optJSONArray = optJSONObject.optJSONArray("offerTags");
            final ArrayList list = new ArrayList();
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    list.add(optJSONArray.getString(i));
                }
            }
            this.zzf = zzu.zzj((Collection)list);
            final boolean has = optJSONObject.has("fullPriceMicros");
            final zzbh zzbh = null;
            Long value;
            if (has) {
                value = optJSONObject.optLong("fullPriceMicros");
            }
            else {
                value = null;
            }
            this.zzg = value;
            final JSONObject optJSONObject2 = optJSONObject.optJSONObject("discountDisplayInfo");
            zzbg zzh;
            if (optJSONObject2 == null) {
                zzh = null;
            }
            else {
                zzh = new zzbg(optJSONObject2);
            }
            this.zzh = zzh;
            final JSONObject optJSONObject3 = optJSONObject.optJSONObject("validTimeWindow");
            zzbi zzi;
            if (optJSONObject3 == null) {
                zzi = null;
            }
            else {
                zzi = new zzbi(optJSONObject3);
            }
            this.zzi = zzi;
            optJSONObject = optJSONObject.optJSONObject("limitedQuantityInfo");
            zzbh zzj;
            if (optJSONObject == null) {
                zzj = zzbh;
            }
            else {
                zzj = new zzbh(optJSONObject);
            }
            this.zzj = zzj;
        }
        
        public String getFormattedPrice() {
            return this.zza;
        }
        
        public long getPriceAmountMicros() {
            return this.zzb;
        }
        
        public String getPriceCurrencyCode() {
            return this.zzc;
        }
        
        public final String zza() {
            return this.zzd;
        }
    }
    
    public static final class PricingPhase
    {
        private final String zza;
        private final long zzb;
        private final String zzc;
        private final String zzd;
        private final int zze;
        private final int zzf;
        
        PricingPhase(final JSONObject jsonObject) {
            this.zzd = jsonObject.optString("billingPeriod");
            this.zzc = jsonObject.optString("priceCurrencyCode");
            this.zza = jsonObject.optString("formattedPrice");
            this.zzb = jsonObject.optLong("priceAmountMicros");
            this.zzf = jsonObject.optInt("recurrenceMode");
            this.zze = jsonObject.optInt("billingCycleCount");
        }
        
        public int getBillingCycleCount() {
            return this.zze;
        }
        
        public String getBillingPeriod() {
            return this.zzd;
        }
        
        public String getFormattedPrice() {
            return this.zza;
        }
        
        public long getPriceAmountMicros() {
            return this.zzb;
        }
        
        public String getPriceCurrencyCode() {
            return this.zzc;
        }
        
        public int getRecurrenceMode() {
            return this.zzf;
        }
    }
    
    public static class PricingPhases
    {
        private final List zza;
        
        PricingPhases(final JSONArray jsonArray) {
            final ArrayList zza = new ArrayList();
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); ++i) {
                    final JSONObject optJSONObject = jsonArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        zza.add(new PricingPhase(optJSONObject));
                    }
                }
            }
            this.zza = zza;
        }
        
        public List<PricingPhase> getPricingPhaseList() {
            return this.zza;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface RecurrenceMode {
        public static final int FINITE_RECURRING = 2;
        public static final int INFINITE_RECURRING = 1;
        public static final int NON_RECURRING = 3;
    }
    
    public static final class SubscriptionOfferDetails
    {
        private final String zza;
        private final String zzb;
        private final String zzc;
        private final PricingPhases zzd;
        private final List zze;
        private final zzbf zzf;
        
        SubscriptionOfferDetails(final JSONObject jsonObject) throws JSONException {
            this.zza = jsonObject.optString("basePlanId");
            String optString = jsonObject.optString("offerId");
            final boolean empty = optString.isEmpty();
            final zzbf zzbf = null;
            if (empty) {
                optString = null;
            }
            this.zzb = optString;
            this.zzc = jsonObject.getString("offerIdToken");
            this.zzd = new PricingPhases(jsonObject.getJSONArray("pricingPhases"));
            final JSONObject optJSONObject = jsonObject.optJSONObject("installmentPlanDetails");
            zzbf zzf;
            if (optJSONObject == null) {
                zzf = zzbf;
            }
            else {
                zzf = new zzbf(optJSONObject);
            }
            this.zzf = zzf;
            final ArrayList zze = new ArrayList();
            final JSONArray optJSONArray = jsonObject.optJSONArray("offerTags");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    zze.add(optJSONArray.getString(i));
                }
            }
            this.zze = zze;
        }
        
        public String getBasePlanId() {
            return this.zza;
        }
        
        public String getOfferId() {
            return this.zzb;
        }
        
        public List<String> getOfferTags() {
            return this.zze;
        }
        
        public String getOfferToken() {
            return this.zzc;
        }
        
        public PricingPhases getPricingPhases() {
            return this.zzd;
        }
    }
}

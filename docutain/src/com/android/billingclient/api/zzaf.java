// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.internal.play_billing.zze;
import com.google.android.gms.internal.play_billing.zzgd;
import java.util.concurrent.Callable;
import com.google.android.gms.internal.play_billing.zzd;
import com.google.android.gms.internal.play_billing.zzb;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.ServiceConnection;

final class zzaf implements ServiceConnection
{
    final BillingClientImpl zza;
    private final Object zzb = new Object();
    private boolean zzc = false;
    private BillingClientStateListener zzd = zzd;
    
    private final void zzd(final BillingResult billingResult) {
        synchronized (this.zzb) {
            final BillingClientStateListener zzd = this.zzd;
            if (zzd != null) {
                zzd.onBillingSetupFinished(billingResult);
            }
        }
    }
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        com.google.android.gms.internal.play_billing.zzb.zzi("BillingClient", "Billing service connected.");
        BillingClientImpl.zzD(this.zza, com.google.android.gms.internal.play_billing.zzd.zzn(binder));
        final BillingClientImpl zza = this.zza;
        if (BillingClientImpl.zzo(zza, (Callable)new zzac(this), 30000L, (Runnable)new zzad(this), BillingClientImpl.zze(zza)) == null) {
            final BillingResult zzh = BillingClientImpl.zzh(this.zza);
            BillingClientImpl.zzg(this.zza).zza(zzaq.zza(25, 6, zzh));
            this.zzd(zzh);
        }
    }
    
    public final void onServiceDisconnected(final ComponentName componentName) {
        com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Billing service disconnected.");
        BillingClientImpl.zzg(this.zza).zzc(zzgd.zzw());
        BillingClientImpl.zzD(this.zza, (zze)null);
        BillingClientImpl.zzp(this.zza, 0);
        synchronized (this.zzb) {
            final BillingClientStateListener zzd = this.zzd;
            if (zzd != null) {
                zzd.onBillingServiceDisconnected();
            }
        }
    }
    
    final void zzc() {
        synchronized (this.zzb) {
            this.zzd = null;
            this.zzc = true;
        }
    }
}

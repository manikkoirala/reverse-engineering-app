// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzbx;
import com.google.android.gms.internal.play_billing.zzgd;
import com.google.android.gms.internal.play_billing.zzff;
import com.google.android.gms.internal.play_billing.zzfy;
import com.google.android.gms.internal.play_billing.zzb;
import com.google.android.gms.internal.play_billing.zzfz;
import com.google.android.gms.internal.play_billing.zzfb;
import android.content.Context;
import com.google.android.gms.internal.play_billing.zzfm;

final class zzaw implements zzar
{
    private final zzfm zza;
    private final zzay zzb;
    
    zzaw(final Context context, final zzfm zza) {
        this.zzb = new zzay(context);
        this.zza = zza;
    }
    
    @Override
    public final void zza(final zzfb zzfb) {
        try {
            final zzfy zzv = zzfz.zzv();
            final zzfm zza = this.zza;
            if (zza != null) {
                zzv.zzk(zza);
            }
            zzv.zzi(zzfb);
            this.zzb.zza((zzfz)((zzbx)zzv).zzc());
        }
        finally {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingLogger", "Unable to log.");
        }
    }
    
    @Override
    public final void zzb(final zzff zzff) {
        try {
            final zzfy zzv = zzfz.zzv();
            final zzfm zza = this.zza;
            if (zza != null) {
                zzv.zzk(zza);
            }
            zzv.zzj(zzff);
            this.zzb.zza((zzfz)((zzbx)zzv).zzc());
        }
        finally {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingLogger", "Unable to log.");
        }
    }
    
    @Override
    public final void zzc(final zzgd zzgd) {
        try {
            final zzfy zzv = zzfz.zzv();
            final zzfm zza = this.zza;
            if (zza != null) {
                zzv.zzk(zza);
            }
            zzv.zzl(zzgd);
            this.zzb.zza((zzfz)((zzbx)zzv).zzc());
        }
        finally {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingLogger", "Unable to log.");
        }
    }
}

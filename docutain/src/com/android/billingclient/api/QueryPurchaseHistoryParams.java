// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

public final class QueryPurchaseHistoryParams
{
    private final String zza = builder.zza;
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    public final String zza() {
        return this.zza;
    }
    
    public static class Builder
    {
        private String zza;
        
        private Builder() {
        }
        
        public QueryPurchaseHistoryParams build() {
            if (this.zza != null) {
                return new QueryPurchaseHistoryParams(this, null);
            }
            throw new IllegalArgumentException("Product type must be set");
        }
        
        public Builder setProductType(final String zza) {
            this.zza = zza;
            return this;
        }
    }
}

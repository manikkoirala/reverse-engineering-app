// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

public final class AccountIdentifiers
{
    private final String zza;
    private final String zzb;
    
    AccountIdentifiers(final String zza, final String zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public String getObfuscatedAccountId() {
        return this.zza;
    }
    
    public String getObfuscatedProfileId() {
        return this.zzb;
    }
}

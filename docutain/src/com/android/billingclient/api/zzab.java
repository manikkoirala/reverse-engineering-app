// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;

final class zzab implements ThreadFactory
{
    private final ThreadFactory zza;
    private final AtomicInteger zzb;
    
    zzab(final BillingClientImpl billingClientImpl) {
        this.zza = Executors.defaultThreadFactory();
        this.zzb = new AtomicInteger(1);
    }
    
    @Override
    public final Thread newThread(final Runnable runnable) {
        final Thread thread = this.zza.newThread(runnable);
        final int andIncrement = this.zzb.getAndIncrement();
        final StringBuilder sb = new StringBuilder();
        sb.append("PlayBillingLibrary-");
        sb.append(andIncrement);
        thread.setName(sb.toString());
        return thread;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

public final class QueryPurchasesParams
{
    private final String zza = builder.zza;
    
    public static Builder newBuilder() {
        return new Builder(null);
    }
    
    public final String zza() {
        return this.zza;
    }
    
    public static class Builder
    {
        private String zza;
        
        private Builder() {
        }
        
        public QueryPurchasesParams build() {
            if (this.zza != null) {
                return new QueryPurchasesParams(this, null);
            }
            throw new IllegalArgumentException("Product type must be set");
        }
        
        public Builder setProductType(final String zza) {
            this.zza = zza;
            return this;
        }
    }
}

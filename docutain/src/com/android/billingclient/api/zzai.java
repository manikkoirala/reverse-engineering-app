// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;

final class zzai
{
    private final List zza;
    private final BillingResult zzb;
    
    zzai(final BillingResult zzb, final List zza) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    final BillingResult zza() {
        return this.zzb;
    }
    
    final List zzb() {
        return this.zza;
    }
}

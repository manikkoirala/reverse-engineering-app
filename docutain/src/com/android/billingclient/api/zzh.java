// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.content.IntentFilter;
import android.content.Context;

final class zzh
{
    private final Context zza;
    private final zzg zzb;
    
    zzh(final Context zza, final PurchasesUpdatedListener purchasesUpdatedListener, final AlternativeBillingListener alternativeBillingListener, final zzar zzar) {
        this.zza = zza;
        this.zzb = new zzg(this, purchasesUpdatedListener, alternativeBillingListener, zzar, null);
    }
    
    zzh(final Context zza, final zzaz zzaz, final zzar zzar) {
        this.zza = zza;
        this.zzb = new zzg(this, null, zzar, null);
    }
    
    final zzaz zzb() {
        zzg.zza(this.zzb);
        return null;
    }
    
    final PurchasesUpdatedListener zzc() {
        return zzg.zzb(this.zzb);
    }
    
    final void zzd() {
        this.zzb.zzd(this.zza);
    }
    
    final void zze() {
        final IntentFilter intentFilter = new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED");
        intentFilter.addAction("com.android.vending.billing.ALTERNATIVE_BILLING");
        this.zzb.zzc(this.zza, intentFilter);
    }
}

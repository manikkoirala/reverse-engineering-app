// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

public final class InAppMessageResult
{
    private final int zza;
    private final String zzb;
    
    public InAppMessageResult(final int zza, final String zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public String getPurchaseToken() {
        return this.zzb;
    }
    
    public int getResponseCode() {
        return this.zza;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface InAppMessageResponseCode {
        public static final int NO_ACTION_NEEDED = 0;
        public static final int SUBSCRIPTION_STATUS_UPDATED = 1;
    }
}

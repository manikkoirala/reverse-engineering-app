// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.os.RemoteException;
import java.util.concurrent.CancellationException;
import android.os.Parcelable;
import android.content.Context;
import android.content.Intent;
import android.app.PendingIntent;
import android.app.Activity;
import com.google.android.gms.internal.play_billing.zzb;
import android.os.Bundle;
import android.os.ResultReceiver;
import java.lang.ref.WeakReference;
import com.google.android.gms.internal.play_billing.zzf;

final class zzah extends zzf
{
    final WeakReference zza = zza;
    final ResultReceiver zzb = zzb;
    
    public final void zza(final Bundle bundle) throws RemoteException {
        final ResultReceiver zzb = this.zzb;
        if (zzb == null) {
            com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Unable to send result for in-app messaging");
            return;
        }
        if (bundle == null) {
            zzb.send(0, (Bundle)null);
            return;
        }
        final Activity activity = (Activity)this.zza.get();
        final PendingIntent pendingIntent = (PendingIntent)bundle.getParcelable("KEY_LAUNCH_INTENT");
        if (activity != null) {
            if (pendingIntent != null) {
                try {
                    final Intent intent = new Intent((Context)activity, (Class)ProxyBillingActivity.class);
                    intent.putExtra("in_app_message_result_receiver", (Parcelable)this.zzb);
                    intent.putExtra("IN_APP_MESSAGE_INTENT", (Parcelable)pendingIntent);
                    activity.startActivity(intent);
                    return;
                }
                catch (final CancellationException ex) {
                    this.zzb.send(0, (Bundle)null);
                    com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", "Exception caught while launching intent for in-app messaging.", (Throwable)ex);
                    return;
                }
            }
        }
        this.zzb.send(0, (Bundle)null);
        com.google.android.gms.internal.play_billing.zzb.zzj("BillingClient", "Unable to launch intent for in-app messaging");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;

public interface PurchaseHistoryResponseListener
{
    void onPurchaseHistoryResponse(final BillingResult p0, final List<PurchaseHistoryRecord> p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker;

import java.io.File;
import android.content.Context;

public class ReLinker
{
    private ReLinker() {
    }
    
    public static ReLinkerInstance force() {
        return new ReLinkerInstance().force();
    }
    
    public static void loadLibrary(final Context context, final String s) {
        loadLibrary(context, s, null, null);
    }
    
    public static void loadLibrary(final Context context, final String s, final LoadListener loadListener) {
        loadLibrary(context, s, null, loadListener);
    }
    
    public static void loadLibrary(final Context context, final String s, final String s2) {
        loadLibrary(context, s, s2, null);
    }
    
    public static void loadLibrary(final Context context, final String s, final String s2, final LoadListener loadListener) {
        new ReLinkerInstance().loadLibrary(context, s, s2, loadListener);
    }
    
    public static ReLinkerInstance log(final Logger logger) {
        return new ReLinkerInstance().log(logger);
    }
    
    public static ReLinkerInstance recursively() {
        return new ReLinkerInstance().recursively();
    }
    
    public interface LibraryInstaller
    {
        void installLibrary(final Context p0, final String[] p1, final String p2, final File p3, final ReLinkerInstance p4);
    }
    
    public interface LibraryLoader
    {
        void loadLibrary(final String p0);
        
        void loadPath(final String p0);
        
        String mapLibraryName(final String p0);
        
        String[] supportedAbis();
        
        String unmapLibraryName(final String p0);
    }
    
    public interface LoadListener
    {
        void failure(final Throwable p0);
        
        void success();
    }
    
    public interface Logger
    {
        void log(final String p0);
    }
}

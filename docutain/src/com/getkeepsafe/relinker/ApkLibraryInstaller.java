// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker;

import android.content.pm.ApplicationInfo;
import android.os.Build$VERSION;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.File;
import android.content.Context;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.Closeable;

public class ApkLibraryInstaller implements LibraryInstaller
{
    private static final int COPY_BUFFER_SIZE = 4096;
    private static final int MAX_TRIES = 5;
    
    private void closeSilently(final Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        }
        catch (final IOException ex) {}
    }
    
    private long copy(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        final byte[] array = new byte[4096];
        long n = 0L;
        while (true) {
            final int read = inputStream.read(array);
            if (read == -1) {
                break;
            }
            outputStream.write(array, 0, read);
            n += read;
        }
        outputStream.flush();
        return n;
    }
    
    private ZipFileInZipEntry findAPKWithLibrary(final Context context, final String[] array, final String str, final ReLinkerInstance reLinkerInstance) {
        final String[] sourceDirectories = this.sourceDirectories(context);
        final int length = sourceDirectories.length;
        int n = 0;
        ZipFile zipFile2 = null;
        ZipEntry entry = null;
    Label_0228_Outer:
        while (true) {
            final ZipFile zipFile = null;
            Label_0234: {
                if (n >= length) {
                    break Label_0234;
                }
                final String pathname = sourceDirectories[n];
                int n2 = 0;
                while (true) {
                    zipFile2 = zipFile;
                    if (n2 < 5) {
                        try {
                            zipFile2 = new ZipFile(new File(pathname), 1);
                        }
                        catch (final IOException ex) {
                            ++n2;
                            continue Label_0228_Outer;
                        }
                        break;
                    }
                    break;
                }
                while (true) {
                    if (zipFile2 == null) {
                        break Label_0228;
                    }
                    for (int i = 0; i < 5; ++i) {
                        for (final String str2 : array) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("lib");
                            sb.append(File.separatorChar);
                            sb.append(str2);
                            sb.append(File.separatorChar);
                            sb.append(str);
                            final String string = sb.toString();
                            reLinkerInstance.log("Looking for %s in APK %s...", string, pathname);
                            entry = zipFile2.getEntry(string);
                            if (entry != null) {
                                break Label_0228_Outer;
                            }
                        }
                    }
                    try {
                        zipFile2.close();
                        ++n;
                        continue Label_0228_Outer;
                        return null;
                    }
                    catch (final IOException ex2) {
                        continue;
                    }
                    break;
                }
            }
        }
        return new ZipFileInZipEntry(zipFile2, entry);
    }
    
    private String[] getSupportedABIs(final Context p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/StringBuilder.<init>:()V
        //     7: astore          5
        //     9: aload           5
        //    11: ldc             "lib"
        //    13: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    16: pop            
        //    17: aload           5
        //    19: getstatic       java/io/File.separatorChar:C
        //    22: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    25: pop            
        //    26: aload           5
        //    28: ldc             "([^\\"
        //    30: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    33: pop            
        //    34: aload           5
        //    36: getstatic       java/io/File.separatorChar:C
        //    39: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    42: pop            
        //    43: aload           5
        //    45: ldc             "]*)"
        //    47: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    50: pop            
        //    51: aload           5
        //    53: getstatic       java/io/File.separatorChar:C
        //    56: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    59: pop            
        //    60: aload           5
        //    62: aload_2        
        //    63: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    66: pop            
        //    67: aload           5
        //    69: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    72: invokestatic    java/util/regex/Pattern.compile:(Ljava/lang/String;)Ljava/util/regex/Pattern;
        //    75: astore          5
        //    77: new             Ljava/util/HashSet;
        //    80: dup            
        //    81: invokespecial   java/util/HashSet.<init>:()V
        //    84: astore_2       
        //    85: aload_0        
        //    86: aload_1        
        //    87: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.sourceDirectories:(Landroid/content/Context;)[Ljava/lang/String;
        //    90: astore_1       
        //    91: aload_1        
        //    92: arraylength    
        //    93: istore          4
        //    95: iconst_0       
        //    96: istore_3       
        //    97: iload_3        
        //    98: iload           4
        //   100: if_icmpge       200
        //   103: aload_1        
        //   104: iload_3        
        //   105: aaload         
        //   106: astore          8
        //   108: new             Ljava/util/zip/ZipFile;
        //   111: astore          7
        //   113: new             Ljava/io/File;
        //   116: astore          6
        //   118: aload           6
        //   120: aload           8
        //   122: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   125: aload           7
        //   127: aload           6
        //   129: iconst_1       
        //   130: invokespecial   java/util/zip/ZipFile.<init>:(Ljava/io/File;I)V
        //   133: aload           7
        //   135: invokevirtual   java/util/zip/ZipFile.entries:()Ljava/util/Enumeration;
        //   138: astore          7
        //   140: aload           7
        //   142: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //   147: ifeq            194
        //   150: aload           5
        //   152: aload           7
        //   154: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //   159: checkcast       Ljava/util/zip/ZipEntry;
        //   162: invokevirtual   java/util/zip/ZipEntry.getName:()Ljava/lang/String;
        //   165: invokevirtual   java/util/regex/Pattern.matcher:(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
        //   168: astore          6
        //   170: aload           6
        //   172: invokevirtual   java/util/regex/Matcher.matches:()Z
        //   175: ifeq            140
        //   178: aload_2        
        //   179: aload           6
        //   181: iconst_1       
        //   182: invokevirtual   java/util/regex/Matcher.group:(I)Ljava/lang/String;
        //   185: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //   190: pop            
        //   191: goto            140
        //   194: iinc            3, 1
        //   197: goto            97
        //   200: aload_2        
        //   201: aload_2        
        //   202: invokeinterface java/util/Set.size:()I
        //   207: anewarray       Ljava/lang/String;
        //   210: invokeinterface java/util/Set.toArray:([Ljava/lang/Object;)[Ljava/lang/Object;
        //   215: checkcast       [Ljava/lang/String;
        //   218: areturn        
        //   219: astore          6
        //   221: goto            194
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  108    133    219    224    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private String[] sourceDirectories(final Context context) {
        final ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (Build$VERSION.SDK_INT >= 21 && applicationInfo.splitSourceDirs != null && applicationInfo.splitSourceDirs.length != 0) {
            final String[] array = new String[applicationInfo.splitSourceDirs.length + 1];
            array[0] = applicationInfo.sourceDir;
            System.arraycopy(applicationInfo.splitSourceDirs, 0, array, 1, applicationInfo.splitSourceDirs.length);
            return array;
        }
        return new String[] { applicationInfo.sourceDir };
    }
    
    @Override
    public void installLibrary(final Context p0, final String[] p1, final String p2, final File p3, final ReLinkerInstance p4) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          14
        //     3: aconst_null    
        //     4: astore          13
        //     6: aload_0        
        //     7: aload_1        
        //     8: aload_2        
        //     9: aload_3        
        //    10: aload           5
        //    12: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.findAPKWithLibrary:(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Lcom/getkeepsafe/relinker/ReLinkerInstance;)Lcom/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry;
        //    15: astore          12
        //    17: aload           12
        //    19: ifnull          299
        //    22: iconst_0       
        //    23: istore          6
        //    25: iload           6
        //    27: iconst_5       
        //    28: if_icmpge       270
        //    31: aload           5
        //    33: ldc             "Found %s! Extracting..."
        //    35: iconst_1       
        //    36: anewarray       Ljava/lang/Object;
        //    39: dup            
        //    40: iconst_0       
        //    41: aload_3        
        //    42: aastore        
        //    43: invokevirtual   com/getkeepsafe/relinker/ReLinkerInstance.log:(Ljava/lang/String;[Ljava/lang/Object;)V
        //    46: aload           4
        //    48: invokevirtual   java/io/File.exists:()Z
        //    51: ifne            69
        //    54: aload           4
        //    56: invokevirtual   java/io/File.createNewFile:()Z
        //    59: istore          11
        //    61: iload           11
        //    63: ifne            69
        //    66: goto            264
        //    69: aload           12
        //    71: getfield        com/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry.zipFile:Ljava/util/zip/ZipFile;
        //    74: aload           12
        //    76: getfield        com/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry.zipEntry:Ljava/util/zip/ZipEntry;
        //    79: invokevirtual   java/util/zip/ZipFile.getInputStream:(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
        //    82: astore_2       
        //    83: new             Ljava/io/FileOutputStream;
        //    86: astore_1       
        //    87: aload_1        
        //    88: aload           4
        //    90: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //    93: aload_0        
        //    94: aload_2        
        //    95: aload_1        
        //    96: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.copy:(Ljava/io/InputStream;Ljava/io/OutputStream;)J
        //    99: lstore          9
        //   101: aload_1        
        //   102: invokevirtual   java/io/FileOutputStream.getFD:()Ljava/io/FileDescriptor;
        //   105: invokevirtual   java/io/FileDescriptor.sync:()V
        //   108: aload           4
        //   110: invokevirtual   java/io/File.length:()J
        //   113: lstore          7
        //   115: lload           9
        //   117: lload           7
        //   119: lcmp           
        //   120: ifeq            136
        //   123: aload_0        
        //   124: aload_2        
        //   125: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.closeSilently:(Ljava/io/Closeable;)V
        //   128: aload_0        
        //   129: aload_1        
        //   130: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.closeSilently:(Ljava/io/Closeable;)V
        //   133: goto            264
        //   136: aload_0        
        //   137: aload_2        
        //   138: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.closeSilently:(Ljava/io/Closeable;)V
        //   141: aload_0        
        //   142: aload_1        
        //   143: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.closeSilently:(Ljava/io/Closeable;)V
        //   146: aload           4
        //   148: iconst_1       
        //   149: iconst_0       
        //   150: invokevirtual   java/io/File.setReadable:(ZZ)Z
        //   153: pop            
        //   154: aload           4
        //   156: iconst_1       
        //   157: iconst_0       
        //   158: invokevirtual   java/io/File.setExecutable:(ZZ)Z
        //   161: pop            
        //   162: aload           4
        //   164: iconst_1       
        //   165: invokevirtual   java/io/File.setWritable:(Z)Z
        //   168: pop            
        //   169: aload           12
        //   171: ifnull          190
        //   174: aload           12
        //   176: getfield        com/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry.zipFile:Ljava/util/zip/ZipFile;
        //   179: ifnull          190
        //   182: aload           12
        //   184: getfield        com/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry.zipFile:Ljava/util/zip/ZipFile;
        //   187: invokevirtual   java/util/zip/ZipFile.close:()V
        //   190: return         
        //   191: astore          4
        //   193: aload_1        
        //   194: astore_3       
        //   195: aload           4
        //   197: astore_1       
        //   198: goto            204
        //   201: astore_1       
        //   202: aconst_null    
        //   203: astore_3       
        //   204: goto            225
        //   207: astore_1       
        //   208: aconst_null    
        //   209: astore_1       
        //   210: goto            242
        //   213: astore_1       
        //   214: aconst_null    
        //   215: astore_1       
        //   216: goto            255
        //   219: astore_1       
        //   220: aconst_null    
        //   221: astore_3       
        //   222: aload           13
        //   224: astore_2       
        //   225: aload_0        
        //   226: aload_2        
        //   227: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.closeSilently:(Ljava/io/Closeable;)V
        //   230: aload_0        
        //   231: aload_3        
        //   232: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.closeSilently:(Ljava/io/Closeable;)V
        //   235: aload_1        
        //   236: athrow         
        //   237: astore_1       
        //   238: aconst_null    
        //   239: astore_2       
        //   240: aconst_null    
        //   241: astore_1       
        //   242: aload_0        
        //   243: aload_2        
        //   244: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.closeSilently:(Ljava/io/Closeable;)V
        //   247: goto            128
        //   250: astore_1       
        //   251: aconst_null    
        //   252: astore_2       
        //   253: aconst_null    
        //   254: astore_1       
        //   255: aload_0        
        //   256: aload_2        
        //   257: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.closeSilently:(Ljava/io/Closeable;)V
        //   260: goto            128
        //   263: astore_1       
        //   264: iinc            6, 1
        //   267: goto            25
        //   270: aload           5
        //   272: ldc             "FATAL! Couldn't extract the library from the APK!"
        //   274: invokevirtual   com/getkeepsafe/relinker/ReLinkerInstance.log:(Ljava/lang/String;)V
        //   277: aload           12
        //   279: ifnull          298
        //   282: aload           12
        //   284: getfield        com/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry.zipFile:Ljava/util/zip/ZipFile;
        //   287: ifnull          298
        //   290: aload           12
        //   292: getfield        com/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry.zipFile:Ljava/util/zip/ZipFile;
        //   295: invokevirtual   java/util/zip/ZipFile.close:()V
        //   298: return         
        //   299: aload_0        
        //   300: aload_1        
        //   301: aload_3        
        //   302: invokespecial   com/getkeepsafe/relinker/ApkLibraryInstaller.getSupportedABIs:(Landroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;
        //   305: astore_1       
        //   306: goto            329
        //   309: astore_1       
        //   310: aload           12
        //   312: astore_2       
        //   313: goto            349
        //   316: astore_1       
        //   317: iconst_1       
        //   318: anewarray       Ljava/lang/String;
        //   321: dup            
        //   322: iconst_0       
        //   323: aload_1        
        //   324: invokevirtual   java/lang/Exception.toString:()Ljava/lang/String;
        //   327: aastore        
        //   328: astore_1       
        //   329: new             Lcom/getkeepsafe/relinker/MissingLibraryException;
        //   332: astore          4
        //   334: aload           4
        //   336: aload_3        
        //   337: aload_2        
        //   338: aload_1        
        //   339: invokespecial   com/getkeepsafe/relinker/MissingLibraryException.<init>:(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
        //   342: aload           4
        //   344: athrow         
        //   345: astore_1       
        //   346: aload           14
        //   348: astore_2       
        //   349: aload_2        
        //   350: ifnull          367
        //   353: aload_2        
        //   354: getfield        com/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry.zipFile:Ljava/util/zip/ZipFile;
        //   357: ifnull          367
        //   360: aload_2        
        //   361: getfield        com/getkeepsafe/relinker/ApkLibraryInstaller$ZipFileInZipEntry.zipFile:Ljava/util/zip/ZipFile;
        //   364: invokevirtual   java/util/zip/ZipFile.close:()V
        //   367: aload_1        
        //   368: athrow         
        //   369: astore          14
        //   371: goto            255
        //   374: astore          14
        //   376: goto            242
        //   379: astore_1       
        //   380: goto            190
        //   383: astore_1       
        //   384: goto            298
        //   387: astore_2       
        //   388: goto            367
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  6      17     345    349    Any
        //  31     46     309    316    Any
        //  46     61     263    264    Ljava/io/IOException;
        //  46     61     309    316    Any
        //  69     83     250    255    Ljava/io/FileNotFoundException;
        //  69     83     237    242    Ljava/io/IOException;
        //  69     83     219    225    Any
        //  83     93     213    219    Ljava/io/FileNotFoundException;
        //  83     93     207    213    Ljava/io/IOException;
        //  83     93     201    204    Any
        //  93     115    369    374    Ljava/io/FileNotFoundException;
        //  93     115    374    379    Ljava/io/IOException;
        //  93     115    191    201    Any
        //  123    128    309    316    Any
        //  128    133    309    316    Any
        //  136    169    309    316    Any
        //  174    190    379    383    Ljava/io/IOException;
        //  225    237    309    316    Any
        //  242    247    309    316    Any
        //  255    260    309    316    Any
        //  270    277    309    316    Any
        //  282    298    383    387    Ljava/io/IOException;
        //  299    306    316    329    Ljava/lang/Exception;
        //  299    306    309    316    Any
        //  317    329    309    316    Any
        //  329    345    309    316    Any
        //  353    367    387    391    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 213 out of bounds for length 213
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static class ZipFileInZipEntry
    {
        public ZipEntry zipEntry;
        public ZipFile zipFile;
        
        public ZipFileInZipEntry(final ZipFile zipFile, final ZipEntry zipEntry) {
            this.zipFile = zipFile;
            this.zipEntry = zipEntry;
        }
    }
}

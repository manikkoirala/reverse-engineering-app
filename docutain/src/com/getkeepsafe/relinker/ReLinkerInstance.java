// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker;

import java.util.List;
import java.util.Locale;
import java.io.FilenameFilter;
import java.util.Iterator;
import java.io.File;
import java.io.IOException;
import com.getkeepsafe.relinker.elf.ElfParser;
import android.util.Log;
import android.content.Context;
import java.util.HashSet;
import java.util.Set;

public class ReLinkerInstance
{
    private static final String LIB_DIR = "lib";
    protected boolean force;
    protected final ReLinker.LibraryInstaller libraryInstaller;
    protected final ReLinker.LibraryLoader libraryLoader;
    protected final Set<String> loadedLibraries;
    protected ReLinker.Logger logger;
    protected boolean recursive;
    
    protected ReLinkerInstance() {
        this(new SystemLibraryLoader(), new ApkLibraryInstaller());
    }
    
    protected ReLinkerInstance(final ReLinker.LibraryLoader libraryLoader, final ReLinker.LibraryInstaller libraryInstaller) {
        this.loadedLibraries = new HashSet<String>();
        if (libraryLoader == null) {
            throw new IllegalArgumentException("Cannot pass null library loader");
        }
        if (libraryInstaller != null) {
            this.libraryLoader = libraryLoader;
            this.libraryInstaller = libraryInstaller;
            return;
        }
        throw new IllegalArgumentException("Cannot pass null library installer");
    }
    
    private void loadLibraryInternal(final Context context, final String s, final String s2) {
        if (this.loadedLibraries.contains(s) && !this.force) {
            this.log("%s already loaded previously!", s);
            return;
        }
        try {
            this.libraryLoader.loadLibrary(s);
            this.loadedLibraries.add(s);
            this.log("%s (%s) was loaded normally!", s, s2);
        }
        catch (final UnsatisfiedLinkError unsatisfiedLinkError) {
            this.log("Loading the library normally failed: %s", Log.getStackTraceString((Throwable)unsatisfiedLinkError));
            this.log("%s (%s) was not loaded normally, re-linking...", s, s2);
            final File workaroundLibFile = this.getWorkaroundLibFile(context, s, s2);
            if (!workaroundLibFile.exists() || this.force) {
                if (this.force) {
                    this.log("Forcing a re-link of %s (%s)...", s, s2);
                }
                this.cleanupOldLibFiles(context, s, s2);
                this.libraryInstaller.installLibrary(context, this.libraryLoader.supportedAbis(), this.libraryLoader.mapLibraryName(s), workaroundLibFile, this);
            }
            try {
                if (this.recursive) {
                    Object o = null;
                    Iterator<String> iterator = null;
                    try {
                        final ElfParser elfParser = new ElfParser(workaroundLibFile);
                        try {
                            o = elfParser.parseNeededDependencies();
                            elfParser.close();
                            o = ((List<String>)o).iterator();
                            while (((Iterator)o).hasNext()) {
                                this.loadLibrary(context, this.libraryLoader.unmapLibraryName(((Iterator<String>)o).next()));
                            }
                        }
                        finally {}
                    }
                    finally {
                        iterator = (Iterator<String>)o;
                    }
                    if (iterator != null) {
                        ((ElfParser)iterator).close();
                    }
                }
                this.libraryLoader.loadPath(workaroundLibFile.getAbsolutePath());
                this.loadedLibraries.add(s);
                this.log("%s (%s) was re-linked!", s, s2);
            }
            catch (final IOException ex) {}
        }
    }
    
    protected void cleanupOldLibFiles(final Context context, final String s, final String s2) {
        final File workaroundLibDir = this.getWorkaroundLibDir(context);
        final File workaroundLibFile = this.getWorkaroundLibFile(context, s, s2);
        final File[] listFiles = workaroundLibDir.listFiles(new FilenameFilter(this, this.libraryLoader.mapLibraryName(s)) {
            final ReLinkerInstance this$0;
            final String val$mappedLibraryName;
            
            @Override
            public boolean accept(final File file, final String s) {
                return s.startsWith(this.val$mappedLibraryName);
            }
        });
        if (listFiles == null) {
            return;
        }
        for (final File file : listFiles) {
            if (this.force || !file.getAbsolutePath().equals(workaroundLibFile.getAbsolutePath())) {
                file.delete();
            }
        }
    }
    
    public ReLinkerInstance force() {
        this.force = true;
        return this;
    }
    
    protected File getWorkaroundLibDir(final Context context) {
        return context.getDir("lib", 0);
    }
    
    protected File getWorkaroundLibFile(final Context context, String mapLibraryName, final String str) {
        mapLibraryName = this.libraryLoader.mapLibraryName(mapLibraryName);
        if (TextUtils.isEmpty(str)) {
            return new File(this.getWorkaroundLibDir(context), mapLibraryName);
        }
        final File workaroundLibDir = this.getWorkaroundLibDir(context);
        final StringBuilder sb = new StringBuilder();
        sb.append(mapLibraryName);
        sb.append(".");
        sb.append(str);
        return new File(workaroundLibDir, sb.toString());
    }
    
    public void loadLibrary(final Context context, final String s) {
        this.loadLibrary(context, s, null, null);
    }
    
    public void loadLibrary(final Context context, final String s, final ReLinker.LoadListener loadListener) {
        this.loadLibrary(context, s, null, loadListener);
    }
    
    public void loadLibrary(final Context context, final String s, final String s2) {
        this.loadLibrary(context, s, s2, null);
    }
    
    public void loadLibrary(final Context context, final String s, final String s2, final ReLinker.LoadListener loadListener) {
        if (context == null) {
            throw new IllegalArgumentException("Given context is null");
        }
        if (!TextUtils.isEmpty(s)) {
            this.log("Beginning load of %s...", s);
            if (loadListener == null) {
                this.loadLibraryInternal(context, s, s2);
            }
            else {
                new Thread(new Runnable(this, context, s, s2, loadListener) {
                    final ReLinkerInstance this$0;
                    final Context val$context;
                    final String val$library;
                    final ReLinker.LoadListener val$listener;
                    final String val$version;
                    
                    @Override
                    public void run() {
                        try {
                            this.this$0.loadLibraryInternal(this.val$context, this.val$library, this.val$version);
                            this.val$listener.success();
                        }
                        catch (final MissingLibraryException ex) {
                            this.val$listener.failure(ex);
                        }
                        catch (final UnsatisfiedLinkError unsatisfiedLinkError) {
                            this.val$listener.failure(unsatisfiedLinkError);
                        }
                    }
                }).start();
            }
            return;
        }
        throw new IllegalArgumentException("Given library is either null or empty");
    }
    
    public ReLinkerInstance log(final ReLinker.Logger logger) {
        this.logger = logger;
        return this;
    }
    
    public void log(final String s) {
        final ReLinker.Logger logger = this.logger;
        if (logger != null) {
            logger.log(s);
        }
    }
    
    public void log(final String format, final Object... args) {
        this.log(String.format(Locale.US, format, args));
    }
    
    public ReLinkerInstance recursively() {
        this.recursive = true;
        return this;
    }
}

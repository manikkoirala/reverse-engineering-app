// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker.elf;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class Section64Header extends SectionHeader
{
    public Section64Header(final ElfParser elfParser, final Header header, final int n) throws IOException {
        final ByteBuffer allocate = ByteBuffer.allocate(8);
        ByteOrder bo;
        if (header.bigEndian) {
            bo = ByteOrder.BIG_ENDIAN;
        }
        else {
            bo = ByteOrder.LITTLE_ENDIAN;
        }
        allocate.order(bo);
        this.info = elfParser.readWord(allocate, header.shoff + n * header.shentsize + 44L);
    }
}

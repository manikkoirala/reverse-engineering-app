// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker.elf;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class Elf32Header extends Header
{
    private final ElfParser parser;
    
    public Elf32Header(final boolean bigEndian, final ElfParser parser) throws IOException {
        this.bigEndian = bigEndian;
        this.parser = parser;
        final ByteBuffer allocate = ByteBuffer.allocate(4);
        ByteOrder bo;
        if (bigEndian) {
            bo = ByteOrder.BIG_ENDIAN;
        }
        else {
            bo = ByteOrder.LITTLE_ENDIAN;
        }
        allocate.order(bo);
        this.type = parser.readHalf(allocate, 16L);
        this.phoff = parser.readWord(allocate, 28L);
        this.shoff = parser.readWord(allocate, 32L);
        this.phentsize = parser.readHalf(allocate, 42L);
        this.phnum = parser.readHalf(allocate, 44L);
        this.shentsize = parser.readHalf(allocate, 46L);
        this.shnum = parser.readHalf(allocate, 48L);
        this.shstrndx = parser.readHalf(allocate, 50L);
    }
    
    @Override
    public DynamicStructure getDynamicStructure(final long n, final int n2) throws IOException {
        return new Dynamic32Structure(this.parser, this, n, n2);
    }
    
    @Override
    public ProgramHeader getProgramHeader(final long n) throws IOException {
        return new Program32Header(this.parser, this, n);
    }
    
    @Override
    public SectionHeader getSectionHeader(final int n) throws IOException {
        return new Section32Header(this.parser, this, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker.elf;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class Program64Header extends ProgramHeader
{
    public Program64Header(final ElfParser elfParser, final Header header, long n) throws IOException {
        final ByteBuffer allocate = ByteBuffer.allocate(8);
        ByteOrder bo;
        if (header.bigEndian) {
            bo = ByteOrder.BIG_ENDIAN;
        }
        else {
            bo = ByteOrder.LITTLE_ENDIAN;
        }
        allocate.order(bo);
        n = header.phoff + n * header.phentsize;
        this.type = elfParser.readWord(allocate, n);
        this.offset = elfParser.readLong(allocate, 8L + n);
        this.vaddr = elfParser.readLong(allocate, 16L + n);
        this.memsz = elfParser.readLong(allocate, n + 40L);
    }
}

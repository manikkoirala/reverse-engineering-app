// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker.elf;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class Dynamic32Structure extends DynamicStructure
{
    public Dynamic32Structure(final ElfParser elfParser, final Header header, long n, final int n2) throws IOException {
        final ByteBuffer allocate = ByteBuffer.allocate(4);
        ByteOrder bo;
        if (header.bigEndian) {
            bo = ByteOrder.BIG_ENDIAN;
        }
        else {
            bo = ByteOrder.LITTLE_ENDIAN;
        }
        allocate.order(bo);
        n += n2 * 8;
        this.tag = elfParser.readWord(allocate, n);
        this.val = elfParser.readWord(allocate, n + 4L);
    }
}

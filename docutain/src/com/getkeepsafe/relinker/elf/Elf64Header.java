// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker.elf;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class Elf64Header extends Header
{
    private final ElfParser parser;
    
    public Elf64Header(final boolean bigEndian, final ElfParser parser) throws IOException {
        this.bigEndian = bigEndian;
        this.parser = parser;
        final ByteBuffer allocate = ByteBuffer.allocate(8);
        ByteOrder bo;
        if (bigEndian) {
            bo = ByteOrder.BIG_ENDIAN;
        }
        else {
            bo = ByteOrder.LITTLE_ENDIAN;
        }
        allocate.order(bo);
        this.type = parser.readHalf(allocate, 16L);
        this.phoff = parser.readLong(allocate, 32L);
        this.shoff = parser.readLong(allocate, 40L);
        this.phentsize = parser.readHalf(allocate, 54L);
        this.phnum = parser.readHalf(allocate, 56L);
        this.shentsize = parser.readHalf(allocate, 58L);
        this.shnum = parser.readHalf(allocate, 60L);
        this.shstrndx = parser.readHalf(allocate, 62L);
    }
    
    @Override
    public DynamicStructure getDynamicStructure(final long n, final int n2) throws IOException {
        return new Dynamic64Structure(this.parser, this, n, n2);
    }
    
    @Override
    public ProgramHeader getProgramHeader(final long n) throws IOException {
        return new Program64Header(this.parser, this, n);
    }
    
    @Override
    public SectionHeader getSectionHeader(final int n) throws IOException {
        return new Section64Header(this.parser, this, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker.elf;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class Program32Header extends ProgramHeader
{
    public Program32Header(final ElfParser elfParser, final Header header, long n) throws IOException {
        final ByteBuffer allocate = ByteBuffer.allocate(4);
        ByteOrder bo;
        if (header.bigEndian) {
            bo = ByteOrder.BIG_ENDIAN;
        }
        else {
            bo = ByteOrder.LITTLE_ENDIAN;
        }
        allocate.order(bo);
        n = header.phoff + n * header.phentsize;
        this.type = elfParser.readWord(allocate, n);
        this.offset = elfParser.readWord(allocate, 4L + n);
        this.vaddr = elfParser.readWord(allocate, 8L + n);
        this.memsz = elfParser.readWord(allocate, n + 20L);
    }
}

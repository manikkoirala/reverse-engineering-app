// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.relinker;

final class TextUtils
{
    private TextUtils() {
    }
    
    public static boolean isEmpty(final CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }
}

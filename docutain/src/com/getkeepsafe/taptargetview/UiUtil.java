// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import android.content.res.Resources$Theme;
import android.util.TypedValue;
import android.content.Context;

class UiUtil
{
    static int dp(final Context context, final int n) {
        return (int)TypedValue.applyDimension(1, (float)n, context.getResources().getDisplayMetrics());
    }
    
    static int setAlpha(final int n, final float n2) {
        float n3;
        if (n2 > 1.0f) {
            n3 = 1.0f;
        }
        else {
            n3 = n2;
            if (n2 <= 0.0f) {
                n3 = 0.0f;
            }
        }
        return (n & 0xFFFFFF) | (int)((n >>> 24) * n3) << 24;
    }
    
    static int sp(final Context context, final int n) {
        return (int)TypedValue.applyDimension(2, (float)n, context.getResources().getDisplayMetrics());
    }
    
    static int themeIntAttr(final Context context, final String s) {
        final Resources$Theme theme = context.getTheme();
        if (theme == null) {
            return -1;
        }
        final TypedValue typedValue = new TypedValue();
        final int identifier = context.getResources().getIdentifier(s, "attr", context.getPackageName());
        if (identifier == 0) {
            return -1;
        }
        theme.resolveAttribute(identifier, typedValue, true);
        return typedValue.data;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import android.view.View;
import android.widget.Toolbar;
import androidx.core.content.ContextCompat;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.Typeface;
import android.graphics.Rect;

public class TapTarget
{
    Rect bounds;
    boolean cancelable;
    final CharSequence description;
    float descriptionTextAlpha;
    private Integer descriptionTextColor;
    private int descriptionTextColorRes;
    private int descriptionTextDimen;
    private int descriptionTextSize;
    Typeface descriptionTypeface;
    private Integer dimColor;
    private int dimColorRes;
    boolean drawShadow;
    Drawable icon;
    int id;
    float outerCircleAlpha;
    private Integer outerCircleColor;
    private int outerCircleColorRes;
    private Integer targetCircleColor;
    private int targetCircleColorRes;
    int targetRadius;
    boolean tintTarget;
    final CharSequence title;
    private Integer titleTextColor;
    private int titleTextColorRes;
    private int titleTextDimen;
    private int titleTextSize;
    Typeface titleTypeface;
    boolean transparentTarget;
    
    protected TapTarget(final Rect bounds, final CharSequence charSequence, final CharSequence charSequence2) {
        this(charSequence, charSequence2);
        if (bounds != null) {
            this.bounds = bounds;
            return;
        }
        throw new IllegalArgumentException("Cannot pass null bounds or title");
    }
    
    protected TapTarget(final CharSequence title, final CharSequence description) {
        this.outerCircleAlpha = 0.96f;
        this.targetRadius = 44;
        this.outerCircleColorRes = -1;
        this.targetCircleColorRes = -1;
        this.dimColorRes = -1;
        this.titleTextColorRes = -1;
        this.descriptionTextColorRes = -1;
        this.outerCircleColor = null;
        this.targetCircleColor = null;
        this.dimColor = null;
        this.titleTextColor = null;
        this.descriptionTextColor = null;
        this.titleTextDimen = -1;
        this.descriptionTextDimen = -1;
        this.titleTextSize = 20;
        this.descriptionTextSize = 18;
        this.id = -1;
        this.drawShadow = false;
        this.cancelable = true;
        this.tintTarget = true;
        this.transparentTarget = false;
        this.descriptionTextAlpha = 0.54f;
        if (title != null) {
            this.title = title;
            this.description = description;
            return;
        }
        throw new IllegalArgumentException("Cannot pass null title");
    }
    
    private Integer colorResOrInt(final Context context, final Integer n, final int n2) {
        if (n2 != -1) {
            return ContextCompat.getColor(context, n2);
        }
        return n;
    }
    
    private int dimenOrSize(final Context context, final int n, final int n2) {
        if (n2 != -1) {
            return context.getResources().getDimensionPixelSize(n2);
        }
        return UiUtil.sp(context, n);
    }
    
    public static TapTarget forBounds(final Rect rect, final CharSequence charSequence) {
        return forBounds(rect, charSequence, null);
    }
    
    public static TapTarget forBounds(final Rect rect, final CharSequence charSequence, final CharSequence charSequence2) {
        return new TapTarget(rect, charSequence, charSequence2);
    }
    
    public static TapTarget forToolbarMenuItem(final Toolbar toolbar, final int n, final CharSequence charSequence) {
        return forToolbarMenuItem(toolbar, n, charSequence, null);
    }
    
    public static TapTarget forToolbarMenuItem(final Toolbar toolbar, final int n, final CharSequence charSequence, final CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, n, charSequence, charSequence2);
    }
    
    public static TapTarget forToolbarMenuItem(final androidx.appcompat.widget.Toolbar toolbar, final int n, final CharSequence charSequence) {
        return forToolbarMenuItem(toolbar, n, charSequence, null);
    }
    
    public static TapTarget forToolbarMenuItem(final androidx.appcompat.widget.Toolbar toolbar, final int n, final CharSequence charSequence, final CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, n, charSequence, charSequence2);
    }
    
    public static TapTarget forToolbarNavigationIcon(final Toolbar toolbar, final CharSequence charSequence) {
        return forToolbarNavigationIcon(toolbar, charSequence, null);
    }
    
    public static TapTarget forToolbarNavigationIcon(final Toolbar toolbar, final CharSequence charSequence, final CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, true, charSequence, charSequence2);
    }
    
    public static TapTarget forToolbarNavigationIcon(final androidx.appcompat.widget.Toolbar toolbar, final CharSequence charSequence) {
        return forToolbarNavigationIcon(toolbar, charSequence, null);
    }
    
    public static TapTarget forToolbarNavigationIcon(final androidx.appcompat.widget.Toolbar toolbar, final CharSequence charSequence, final CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, true, charSequence, charSequence2);
    }
    
    public static TapTarget forToolbarOverflow(final Toolbar toolbar, final CharSequence charSequence) {
        return forToolbarOverflow(toolbar, charSequence, null);
    }
    
    public static TapTarget forToolbarOverflow(final Toolbar toolbar, final CharSequence charSequence, final CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, false, charSequence, charSequence2);
    }
    
    public static TapTarget forToolbarOverflow(final androidx.appcompat.widget.Toolbar toolbar, final CharSequence charSequence) {
        return forToolbarOverflow(toolbar, charSequence, null);
    }
    
    public static TapTarget forToolbarOverflow(final androidx.appcompat.widget.Toolbar toolbar, final CharSequence charSequence, final CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, false, charSequence, charSequence2);
    }
    
    public static TapTarget forView(final View view, final CharSequence charSequence) {
        return forView(view, charSequence, null);
    }
    
    public static TapTarget forView(final View view, final CharSequence charSequence, final CharSequence charSequence2) {
        return new ViewTapTarget(view, charSequence, charSequence2);
    }
    
    public Rect bounds() {
        final Rect bounds = this.bounds;
        if (bounds != null) {
            return bounds;
        }
        throw new IllegalStateException("Requesting bounds that are not set! Make sure your target is ready");
    }
    
    public TapTarget cancelable(final boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }
    
    public TapTarget descriptionTextAlpha(final float n) {
        if (n >= 0.0f && n <= 1.0f) {
            this.descriptionTextAlpha = n;
            return this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Given an invalid alpha value: ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public TapTarget descriptionTextColor(final int descriptionTextColorRes) {
        this.descriptionTextColorRes = descriptionTextColorRes;
        return this;
    }
    
    public TapTarget descriptionTextColorInt(final int i) {
        this.descriptionTextColor = i;
        return this;
    }
    
    Integer descriptionTextColorInt(final Context context) {
        return this.colorResOrInt(context, this.descriptionTextColor, this.descriptionTextColorRes);
    }
    
    public TapTarget descriptionTextDimen(final int descriptionTextDimen) {
        this.descriptionTextDimen = descriptionTextDimen;
        return this;
    }
    
    public TapTarget descriptionTextSize(final int descriptionTextSize) {
        if (descriptionTextSize >= 0) {
            this.descriptionTextSize = descriptionTextSize;
            return this;
        }
        throw new IllegalArgumentException("Given negative text size");
    }
    
    int descriptionTextSizePx(final Context context) {
        return this.dimenOrSize(context, this.descriptionTextSize, this.descriptionTextDimen);
    }
    
    public TapTarget descriptionTypeface(final Typeface descriptionTypeface) {
        if (descriptionTypeface != null) {
            this.descriptionTypeface = descriptionTypeface;
            return this;
        }
        throw new IllegalArgumentException("Cannot use a null typeface");
    }
    
    public TapTarget dimColor(final int dimColorRes) {
        this.dimColorRes = dimColorRes;
        return this;
    }
    
    public TapTarget dimColorInt(final int i) {
        this.dimColor = i;
        return this;
    }
    
    Integer dimColorInt(final Context context) {
        return this.colorResOrInt(context, this.dimColor, this.dimColorRes);
    }
    
    public TapTarget drawShadow(final boolean drawShadow) {
        this.drawShadow = drawShadow;
        return this;
    }
    
    public TapTarget icon(final Drawable drawable) {
        return this.icon(drawable, false);
    }
    
    public TapTarget icon(final Drawable icon, final boolean b) {
        if (icon != null) {
            this.icon = icon;
            if (!b) {
                icon.setBounds(new Rect(0, 0, this.icon.getIntrinsicWidth(), this.icon.getIntrinsicHeight()));
            }
            return this;
        }
        throw new IllegalArgumentException("Cannot use null drawable");
    }
    
    public int id() {
        return this.id;
    }
    
    public TapTarget id(final int id) {
        this.id = id;
        return this;
    }
    
    public void onReady(final Runnable runnable) {
        runnable.run();
    }
    
    public TapTarget outerCircleAlpha(final float n) {
        if (n >= 0.0f && n <= 1.0f) {
            this.outerCircleAlpha = n;
            return this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Given an invalid alpha value: ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public TapTarget outerCircleColor(final int outerCircleColorRes) {
        this.outerCircleColorRes = outerCircleColorRes;
        return this;
    }
    
    public TapTarget outerCircleColorInt(final int i) {
        this.outerCircleColor = i;
        return this;
    }
    
    Integer outerCircleColorInt(final Context context) {
        return this.colorResOrInt(context, this.outerCircleColor, this.outerCircleColorRes);
    }
    
    public TapTarget targetCircleColor(final int targetCircleColorRes) {
        this.targetCircleColorRes = targetCircleColorRes;
        return this;
    }
    
    public TapTarget targetCircleColorInt(final int i) {
        this.targetCircleColor = i;
        return this;
    }
    
    Integer targetCircleColorInt(final Context context) {
        return this.colorResOrInt(context, this.targetCircleColor, this.targetCircleColorRes);
    }
    
    public TapTarget targetRadius(final int targetRadius) {
        this.targetRadius = targetRadius;
        return this;
    }
    
    public TapTarget textColor(final int n) {
        this.titleTextColorRes = n;
        this.descriptionTextColorRes = n;
        return this;
    }
    
    public TapTarget textColorInt(final int n) {
        this.titleTextColor = n;
        this.descriptionTextColor = n;
        return this;
    }
    
    public TapTarget textTypeface(final Typeface typeface) {
        if (typeface != null) {
            this.titleTypeface = typeface;
            this.descriptionTypeface = typeface;
            return this;
        }
        throw new IllegalArgumentException("Cannot use a null typeface");
    }
    
    public TapTarget tintTarget(final boolean tintTarget) {
        this.tintTarget = tintTarget;
        return this;
    }
    
    public TapTarget titleTextColor(final int titleTextColorRes) {
        this.titleTextColorRes = titleTextColorRes;
        return this;
    }
    
    public TapTarget titleTextColorInt(final int i) {
        this.titleTextColor = i;
        return this;
    }
    
    Integer titleTextColorInt(final Context context) {
        return this.colorResOrInt(context, this.titleTextColor, this.titleTextColorRes);
    }
    
    public TapTarget titleTextDimen(final int titleTextDimen) {
        this.titleTextDimen = titleTextDimen;
        return this;
    }
    
    public TapTarget titleTextSize(final int titleTextSize) {
        if (titleTextSize >= 0) {
            this.titleTextSize = titleTextSize;
            return this;
        }
        throw new IllegalArgumentException("Given negative text size");
    }
    
    int titleTextSizePx(final Context context) {
        return this.dimenOrSize(context, this.titleTextSize, this.titleTextDimen);
    }
    
    public TapTarget titleTypeface(final Typeface titleTypeface) {
        if (titleTypeface != null) {
            this.titleTypeface = titleTypeface;
            return this;
        }
        throw new IllegalArgumentException("Cannot use a null typeface");
    }
    
    public TapTarget transparentTarget(final boolean transparentTarget) {
        this.transparentTarget = transparentTarget;
        return this;
    }
}

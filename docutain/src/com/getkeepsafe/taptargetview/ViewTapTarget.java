// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.graphics.Rect;
import android.view.View;

class ViewTapTarget extends TapTarget
{
    final View view;
    
    ViewTapTarget(final View view, final CharSequence charSequence, final CharSequence charSequence2) {
        super(charSequence, charSequence2);
        if (view != null) {
            this.view = view;
            return;
        }
        throw new IllegalArgumentException("Given null view to target");
    }
    
    @Override
    public void onReady(final Runnable runnable) {
        ViewUtil.onLaidOut(this.view, new Runnable(this, runnable) {
            final ViewTapTarget this$0;
            final Runnable val$runnable;
            
            @Override
            public void run() {
                final int[] array = new int[2];
                this.this$0.view.getLocationOnScreen(array);
                final ViewTapTarget this$0 = this.this$0;
                final int n = array[0];
                this$0.bounds = new Rect(n, array[1], this.this$0.view.getWidth() + n, array[1] + this.this$0.view.getHeight());
                if (this.this$0.icon == null && this.this$0.view.getWidth() > 0 && this.this$0.view.getHeight() > 0) {
                    final Bitmap bitmap = Bitmap.createBitmap(this.this$0.view.getWidth(), this.this$0.view.getHeight(), Bitmap$Config.ARGB_8888);
                    this.this$0.view.draw(new Canvas(bitmap));
                    (this.this$0.icon = (Drawable)new BitmapDrawable(this.this$0.view.getContext().getResources(), bitmap)).setBounds(0, 0, this.this$0.icon.getIntrinsicWidth(), this.this$0.icon.getIntrinsicHeight());
                }
                this.val$runnable.run();
            }
        });
    }
}

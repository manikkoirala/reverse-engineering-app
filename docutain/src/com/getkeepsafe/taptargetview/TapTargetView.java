// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import android.view.MotionEvent;
import android.view.KeyEvent;
import android.graphics.Region$Op;
import android.graphics.drawable.Drawable;
import android.graphics.ColorFilter;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Bitmap$Config;
import android.text.Layout$Alignment;
import android.graphics.Canvas;
import android.content.res.Resources$Theme;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff$Mode;
import android.graphics.Outline;
import android.view.WindowManager$LayoutParams;
import android.app.Dialog;
import android.view.ViewGroup$LayoutParams;
import android.view.View$OnLongClickListener;
import android.view.View$OnClickListener;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.app.Activity;
import android.os.Build$VERSION;
import android.graphics.Paint$Style;
import android.graphics.Typeface;
import android.animation.TimeInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.graphics.Path$Direction;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.ViewManager;
import android.view.ViewOutlineProvider;
import android.graphics.Path;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.graphics.Rect;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.SpannableStringBuilder;
import android.graphics.Paint;
import android.text.DynamicLayout;
import android.view.ViewGroup;
import android.animation.ValueAnimator;
import android.view.View;

public class TapTargetView extends View
{
    final int CIRCLE_PADDING;
    final int GUTTER_DIM;
    final int SHADOW_DIM;
    final int SHADOW_JITTER_DIM;
    final int TARGET_PADDING;
    final int TARGET_PULSE_RADIUS;
    final int TARGET_RADIUS;
    final int TEXT_MAX_WIDTH;
    final int TEXT_PADDING;
    final int TEXT_POSITIONING_BIAS;
    final int TEXT_SPACING;
    private ValueAnimator[] animators;
    int bottomBoundary;
    final ViewGroup boundingParent;
    int calculatedOuterCircleRadius;
    boolean cancelable;
    boolean debug;
    DynamicLayout debugLayout;
    Paint debugPaint;
    SpannableStringBuilder debugStringBuilder;
    TextPaint debugTextPaint;
    CharSequence description;
    StaticLayout descriptionLayout;
    final TextPaint descriptionPaint;
    int dimColor;
    final ValueAnimator dismissAnimation;
    private final ValueAnimator dismissConfirmAnimation;
    Rect drawingBounds;
    final ValueAnimator expandAnimation;
    final FloatValueAnimatorBuilder.UpdateListener expandContractUpdateListener;
    private final ViewTreeObserver$OnGlobalLayoutListener globalLayoutListener;
    boolean isDark;
    private boolean isDismissed;
    private boolean isDismissing;
    private boolean isInteractable;
    float lastTouchX;
    float lastTouchY;
    Listener listener;
    int outerCircleAlpha;
    int[] outerCircleCenter;
    final Paint outerCirclePaint;
    Path outerCirclePath;
    float outerCircleRadius;
    final Paint outerCircleShadowPaint;
    ViewOutlineProvider outlineProvider;
    final ViewManager parent;
    final ValueAnimator pulseAnimation;
    boolean shouldDrawShadow;
    boolean shouldTintTarget;
    final TapTarget target;
    final Rect targetBounds;
    int targetCircleAlpha;
    final Paint targetCirclePaint;
    int targetCirclePulseAlpha;
    final Paint targetCirclePulsePaint;
    float targetCirclePulseRadius;
    float targetCircleRadius;
    int textAlpha;
    Rect textBounds;
    Bitmap tintedTarget;
    CharSequence title;
    StaticLayout titleLayout;
    final TextPaint titlePaint;
    int topBoundary;
    boolean visible;
    
    public TapTargetView(final Context context, final ViewManager parent, final ViewGroup boundingParent, final TapTarget target, Listener listener) {
        super(context);
        boolean b = false;
        this.isDismissed = false;
        this.isDismissing = false;
        this.isInteractable = true;
        this.expandContractUpdateListener = new FloatValueAnimatorBuilder.UpdateListener() {
            final TapTargetView this$0;
            
            @Override
            public void onUpdate(final float n) {
                final float outerCircleRadius = this.this$0.calculatedOuterCircleRadius * n;
                final boolean b = outerCircleRadius > this.this$0.outerCircleRadius;
                if (!b) {
                    this.this$0.calculateDrawingBounds();
                }
                final float a = this.this$0.target.outerCircleAlpha * 255.0f;
                this.this$0.outerCircleRadius = outerCircleRadius;
                final TapTargetView this$0 = this.this$0;
                final float b2 = 1.5f * n;
                this$0.outerCircleAlpha = (int)Math.min(a, b2 * a);
                this.this$0.outerCirclePath.reset();
                this.this$0.outerCirclePath.addCircle((float)this.this$0.outerCircleCenter[0], (float)this.this$0.outerCircleCenter[1], this.this$0.outerCircleRadius, Path$Direction.CW);
                this.this$0.targetCircleAlpha = (int)Math.min(255.0f, b2 * 255.0f);
                if (b) {
                    final TapTargetView this$2 = this.this$0;
                    this$2.targetCircleRadius = this$2.TARGET_RADIUS * Math.min(1.0f, b2);
                }
                else {
                    final TapTargetView this$3 = this.this$0;
                    this$3.targetCircleRadius = this$3.TARGET_RADIUS * n;
                    final TapTargetView this$4 = this.this$0;
                    this$4.targetCirclePulseRadius *= n;
                }
                final TapTargetView this$5 = this.this$0;
                this$5.textAlpha = (int)(this$5.delayedLerp(n, 0.7f) * 255.0f);
                if (b) {
                    this.this$0.calculateDrawingBounds();
                }
                final TapTargetView this$6 = this.this$0;
                this$6.invalidateViewAndOutline(this$6.drawingBounds);
            }
        };
        final ValueAnimator build = new FloatValueAnimatorBuilder().duration(250L).delayBy(250L).interpolator((TimeInterpolator)new AccelerateDecelerateInterpolator()).onUpdate((FloatValueAnimatorBuilder.UpdateListener)new FloatValueAnimatorBuilder.UpdateListener() {
            final TapTargetView this$0;
            
            @Override
            public void onUpdate(final float n) {
                this.this$0.expandContractUpdateListener.onUpdate(n);
            }
        }).onEnd((FloatValueAnimatorBuilder.EndListener)new FloatValueAnimatorBuilder.EndListener() {
            final TapTargetView this$0;
            
            @Override
            public void onEnd() {
                this.this$0.pulseAnimation.start();
                this.this$0.isInteractable = true;
            }
        }).build();
        this.expandAnimation = build;
        final ValueAnimator build2 = new FloatValueAnimatorBuilder().duration(1000L).repeat(-1).interpolator((TimeInterpolator)new AccelerateDecelerateInterpolator()).onUpdate((FloatValueAnimatorBuilder.UpdateListener)new FloatValueAnimatorBuilder.UpdateListener() {
            final TapTargetView this$0;
            
            @Override
            public void onUpdate(final float n) {
                final float delayedLerp = this.this$0.delayedLerp(n, 0.5f);
                final TapTargetView this$0 = this.this$0;
                this$0.targetCirclePulseRadius = (delayedLerp + 1.0f) * this$0.TARGET_RADIUS;
                this.this$0.targetCirclePulseAlpha = (int)((1.0f - delayedLerp) * 255.0f);
                final TapTargetView this$2 = this.this$0;
                this$2.targetCircleRadius = this$2.TARGET_RADIUS + this.this$0.halfwayLerp(n) * this.this$0.TARGET_PULSE_RADIUS;
                if (this.this$0.outerCircleRadius != this.this$0.calculatedOuterCircleRadius) {
                    final TapTargetView this$3 = this.this$0;
                    this$3.outerCircleRadius = (float)this$3.calculatedOuterCircleRadius;
                }
                this.this$0.calculateDrawingBounds();
                final TapTargetView this$4 = this.this$0;
                this$4.invalidateViewAndOutline(this$4.drawingBounds);
            }
        }).build();
        this.pulseAnimation = build2;
        final ValueAnimator build3 = new FloatValueAnimatorBuilder(true).duration(250L).interpolator((TimeInterpolator)new AccelerateDecelerateInterpolator()).onUpdate((FloatValueAnimatorBuilder.UpdateListener)new FloatValueAnimatorBuilder.UpdateListener() {
            final TapTargetView this$0;
            
            @Override
            public void onUpdate(final float n) {
                this.this$0.expandContractUpdateListener.onUpdate(n);
            }
        }).onEnd((FloatValueAnimatorBuilder.EndListener)new FloatValueAnimatorBuilder.EndListener() {
            final TapTargetView this$0;
            
            @Override
            public void onEnd() {
                this.this$0.finishDismiss(true);
            }
        }).build();
        this.dismissAnimation = build3;
        final ValueAnimator build4 = new FloatValueAnimatorBuilder().duration(250L).interpolator((TimeInterpolator)new AccelerateDecelerateInterpolator()).onUpdate((FloatValueAnimatorBuilder.UpdateListener)new FloatValueAnimatorBuilder.UpdateListener() {
            final TapTargetView this$0;
            
            @Override
            public void onUpdate(final float n) {
                final float min = Math.min(1.0f, 2.0f * n);
                final TapTargetView this$0 = this.this$0;
                this$0.outerCircleRadius = this$0.calculatedOuterCircleRadius * (0.2f * min + 1.0f);
                final TapTargetView this$2 = this.this$0;
                final float n2 = 1.0f - min;
                this$2.outerCircleAlpha = (int)(this$2.target.outerCircleAlpha * n2 * 255.0f);
                this.this$0.outerCirclePath.reset();
                this.this$0.outerCirclePath.addCircle((float)this.this$0.outerCircleCenter[0], (float)this.this$0.outerCircleCenter[1], this.this$0.outerCircleRadius, Path$Direction.CW);
                final TapTargetView this$3 = this.this$0;
                final float n3 = 1.0f - n;
                this$3.targetCircleRadius = this$3.TARGET_RADIUS * n3;
                this.this$0.targetCircleAlpha = (int)(n3 * 255.0f);
                final TapTargetView this$4 = this.this$0;
                this$4.targetCirclePulseRadius = (n + 1.0f) * this$4.TARGET_RADIUS;
                final TapTargetView this$5 = this.this$0;
                this$5.targetCirclePulseAlpha *= (int)n3;
                this.this$0.textAlpha = (int)(n2 * 255.0f);
                this.this$0.calculateDrawingBounds();
                final TapTargetView this$6 = this.this$0;
                this$6.invalidateViewAndOutline(this$6.drawingBounds);
            }
        }).onEnd((FloatValueAnimatorBuilder.EndListener)new FloatValueAnimatorBuilder.EndListener() {
            final TapTargetView this$0;
            
            @Override
            public void onEnd() {
                this.this$0.finishDismiss(true);
            }
        }).build();
        this.dismissConfirmAnimation = build4;
        this.animators = new ValueAnimator[] { build, build2, build4, build3 };
        if (target != null) {
            this.target = target;
            this.parent = parent;
            this.boundingParent = boundingParent;
            if (listener == null) {
                listener = new Listener();
            }
            this.listener = listener;
            this.title = target.title;
            this.description = target.description;
            this.TARGET_PADDING = UiUtil.dp(context, 20);
            this.CIRCLE_PADDING = UiUtil.dp(context, 40);
            final int dp = UiUtil.dp(context, target.targetRadius);
            this.TARGET_RADIUS = dp;
            this.TEXT_PADDING = UiUtil.dp(context, 40);
            this.TEXT_SPACING = UiUtil.dp(context, 8);
            this.TEXT_MAX_WIDTH = UiUtil.dp(context, 360);
            this.TEXT_POSITIONING_BIAS = UiUtil.dp(context, 20);
            this.GUTTER_DIM = UiUtil.dp(context, 88);
            this.SHADOW_DIM = UiUtil.dp(context, 8);
            final int dp2 = UiUtil.dp(context, 1);
            this.SHADOW_JITTER_DIM = dp2;
            this.TARGET_PULSE_RADIUS = (int)(dp * 0.1f);
            this.outerCirclePath = new Path();
            this.targetBounds = new Rect();
            this.drawingBounds = new Rect();
            final TextPaint titlePaint = new TextPaint();
            (this.titlePaint = titlePaint).setTextSize((float)target.titleTextSizePx(context));
            titlePaint.setTypeface(Typeface.create("sans-serif-medium", 0));
            titlePaint.setAntiAlias(true);
            final TextPaint descriptionPaint = new TextPaint();
            (this.descriptionPaint = descriptionPaint).setTextSize((float)target.descriptionTextSizePx(context));
            descriptionPaint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 0));
            descriptionPaint.setAntiAlias(true);
            descriptionPaint.setAlpha(137);
            final Paint outerCirclePaint = new Paint();
            (this.outerCirclePaint = outerCirclePaint).setAntiAlias(true);
            outerCirclePaint.setAlpha((int)(target.outerCircleAlpha * 255.0f));
            final Paint outerCircleShadowPaint = new Paint();
            (this.outerCircleShadowPaint = outerCircleShadowPaint).setAntiAlias(true);
            outerCircleShadowPaint.setAlpha(50);
            outerCircleShadowPaint.setStyle(Paint$Style.STROKE);
            outerCircleShadowPaint.setStrokeWidth((float)dp2);
            outerCircleShadowPaint.setColor(-16777216);
            (this.targetCirclePaint = new Paint()).setAntiAlias(true);
            (this.targetCirclePulsePaint = new Paint()).setAntiAlias(true);
            this.applyTargetOptions(context);
            final boolean b2 = Build$VERSION.SDK_INT >= 19;
            boolean b6;
            boolean b7;
            if (context instanceof Activity) {
                final int flags = ((Activity)context).getWindow().getAttributes().flags;
                final boolean b3 = b2 && (0x4000000 & flags) != 0x0;
                final boolean b4 = b2 && (0x8000000 & flags) != 0x0;
                if ((flags & 0x200) != 0x0) {
                    b = true;
                }
                final boolean b5 = b4;
                b6 = b3;
                b7 = b5;
            }
            else {
                b6 = false;
                b7 = false;
                b = false;
            }
            final ViewTreeObserver$OnGlobalLayoutListener globalLayoutListener = (ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this, target, boundingParent, context, b6, b7, b) {
                final TapTargetView this$0;
                final ViewGroup val$boundingParent;
                final Context val$context;
                final boolean val$layoutNoLimits;
                final TapTarget val$target;
                final boolean val$translucentNavigationBar;
                final boolean val$translucentStatusBar;
                
                public void onGlobalLayout() {
                    if (this.this$0.isDismissing) {
                        return;
                    }
                    this.this$0.updateTextLayouts();
                    this.val$target.onReady(new Runnable(this) {
                        final TapTargetView$9 this$1;
                        
                        @Override
                        public void run() {
                            final int[] array = new int[2];
                            this.this$1.this$0.targetBounds.set(this.this$1.val$target.bounds());
                            this.this$1.this$0.getLocationOnScreen(array);
                            this.this$1.this$0.targetBounds.offset(-array[0], -array[1]);
                            if (this.this$1.val$boundingParent != null) {
                                final WindowManager windowManager = (WindowManager)this.this$1.val$context.getSystemService("window");
                                final DisplayMetrics displayMetrics = new DisplayMetrics();
                                windowManager.getDefaultDisplay().getMetrics(displayMetrics);
                                final Rect rect = new Rect();
                                this.this$1.val$boundingParent.getWindowVisibleDisplayFrame(rect);
                                final int[] array2 = new int[2];
                                this.this$1.val$boundingParent.getLocationInWindow(array2);
                                if (this.this$1.val$translucentStatusBar) {
                                    rect.top = array2[1];
                                }
                                if (this.this$1.val$translucentNavigationBar) {
                                    rect.bottom = array2[1] + this.this$1.val$boundingParent.getHeight();
                                }
                                if (this.this$1.val$layoutNoLimits) {
                                    this.this$1.this$0.topBoundary = Math.max(0, rect.top);
                                    this.this$1.this$0.bottomBoundary = Math.min(rect.bottom, displayMetrics.heightPixels);
                                }
                                else {
                                    this.this$1.this$0.topBoundary = rect.top;
                                    this.this$1.this$0.bottomBoundary = rect.bottom;
                                }
                            }
                            this.this$1.this$0.drawTintedTarget();
                            this.this$1.this$0.requestFocus();
                            this.this$1.this$0.calculateDimensions();
                            this.this$1.this$0.startExpandAnimation();
                        }
                    });
                }
            };
            this.globalLayoutListener = (ViewTreeObserver$OnGlobalLayoutListener)globalLayoutListener;
            this.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)globalLayoutListener);
            this.setFocusableInTouchMode(true);
            this.setClickable(true);
            this.setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
                final TapTargetView this$0;
                
                public void onClick(final View view) {
                    if (this.this$0.listener != null && this.this$0.outerCircleCenter != null) {
                        if (this.this$0.isInteractable) {
                            final TapTargetView this$0 = this.this$0;
                            final double distance = this$0.distance(this$0.targetBounds.centerX(), this.this$0.targetBounds.centerY(), (int)this.this$0.lastTouchX, (int)this.this$0.lastTouchY);
                            final double n = this.this$0.targetCircleRadius;
                            boolean b = true;
                            final boolean b2 = distance <= n;
                            final TapTargetView this$2 = this.this$0;
                            if (this$2.distance(this$2.outerCircleCenter[0], this.this$0.outerCircleCenter[1], (int)this.this$0.lastTouchX, (int)this.this$0.lastTouchY) > this.this$0.outerCircleRadius) {
                                b = false;
                            }
                            if (b2) {
                                this.this$0.isInteractable = false;
                                this.this$0.listener.onTargetClick(this.this$0);
                            }
                            else if (b) {
                                this.this$0.listener.onOuterCircleClick(this.this$0);
                            }
                            else if (this.this$0.cancelable) {
                                this.this$0.isInteractable = false;
                                this.this$0.listener.onTargetCancel(this.this$0);
                            }
                        }
                    }
                }
            });
            this.setOnLongClickListener((View$OnLongClickListener)new View$OnLongClickListener(this) {
                final TapTargetView this$0;
                
                public boolean onLongClick(final View view) {
                    if (this.this$0.listener == null) {
                        return false;
                    }
                    if (this.this$0.targetBounds.contains((int)this.this$0.lastTouchX, (int)this.this$0.lastTouchY)) {
                        this.this$0.listener.onTargetLongClick(this.this$0);
                        return true;
                    }
                    return false;
                }
            });
            return;
        }
        throw new IllegalArgumentException("Target cannot be null");
    }
    
    private void finishDismiss(final boolean b) {
        this.onDismiss(b);
        ViewUtil.removeView(this.parent, this);
    }
    
    public static TapTargetView showFor(final Activity activity, final TapTarget tapTarget) {
        return showFor(activity, tapTarget, null);
    }
    
    public static TapTargetView showFor(final Activity activity, final TapTarget tapTarget, final Listener listener) {
        if (activity != null) {
            final ViewGroup viewGroup = (ViewGroup)activity.getWindow().getDecorView();
            final ViewGroup$LayoutParams viewGroup$LayoutParams = new ViewGroup$LayoutParams(-1, -1);
            final TapTargetView tapTargetView = new TapTargetView((Context)activity, (ViewManager)viewGroup, (ViewGroup)viewGroup.findViewById(16908290), tapTarget, listener);
            viewGroup.addView((View)tapTargetView, viewGroup$LayoutParams);
            return tapTargetView;
        }
        throw new IllegalArgumentException("Activity is null");
    }
    
    public static TapTargetView showFor(final Dialog dialog, final TapTarget tapTarget) {
        return showFor(dialog, tapTarget, null);
    }
    
    public static TapTargetView showFor(final Dialog dialog, final TapTarget tapTarget, final Listener listener) {
        if (dialog != null) {
            final Context context = dialog.getContext();
            final WindowManager windowManager = (WindowManager)context.getSystemService("window");
            final WindowManager$LayoutParams windowManager$LayoutParams = new WindowManager$LayoutParams();
            windowManager$LayoutParams.type = 2;
            windowManager$LayoutParams.format = 1;
            windowManager$LayoutParams.flags = 0;
            windowManager$LayoutParams.gravity = 8388659;
            windowManager$LayoutParams.x = 0;
            windowManager$LayoutParams.y = 0;
            windowManager$LayoutParams.width = -1;
            windowManager$LayoutParams.height = -1;
            final TapTargetView tapTargetView = new TapTargetView(context, (ViewManager)windowManager, null, tapTarget, listener);
            windowManager.addView((View)tapTargetView, (ViewGroup$LayoutParams)windowManager$LayoutParams);
            return tapTargetView;
        }
        throw new IllegalArgumentException("Dialog is null");
    }
    
    private void startExpandAnimation() {
        if (!this.visible) {
            this.isInteractable = false;
            this.expandAnimation.start();
            this.visible = true;
        }
    }
    
    protected void applyTargetOptions(final Context context) {
        final boolean transparentTarget = this.target.transparentTarget;
        final boolean b = false;
        this.shouldTintTarget = (!transparentTarget && this.target.tintTarget);
        this.shouldDrawShadow = this.target.drawShadow;
        this.cancelable = this.target.cancelable;
        if (this.shouldDrawShadow && Build$VERSION.SDK_INT >= 21 && !this.target.transparentTarget) {
            this.setOutlineProvider(this.outlineProvider = new ViewOutlineProvider(this) {
                final TapTargetView this$0;
                
                public void getOutline(final View view, final Outline outline) {
                    if (this.this$0.outerCircleCenter == null) {
                        return;
                    }
                    outline.setOval((int)(this.this$0.outerCircleCenter[0] - this.this$0.outerCircleRadius), (int)(this.this$0.outerCircleCenter[1] - this.this$0.outerCircleRadius), (int)(this.this$0.outerCircleCenter[0] + this.this$0.outerCircleRadius), (int)(this.this$0.outerCircleCenter[1] + this.this$0.outerCircleRadius));
                    outline.setAlpha(this.this$0.outerCircleAlpha / 255.0f);
                    if (Build$VERSION.SDK_INT >= 22) {
                        outline.offset(0, this.this$0.SHADOW_DIM);
                    }
                }
            });
            this.setElevation((float)this.SHADOW_DIM);
        }
        if (this.shouldDrawShadow && this.outlineProvider == null && Build$VERSION.SDK_INT < 18) {
            this.setLayerType(1, (Paint)null);
        }
        else {
            this.setLayerType(2, (Paint)null);
        }
        final Resources$Theme theme = context.getTheme();
        boolean isDark = b;
        if (UiUtil.themeIntAttr(context, "isLightTheme") == 0) {
            isDark = true;
        }
        this.isDark = isDark;
        final Integer outerCircleColorInt = this.target.outerCircleColorInt(context);
        final int n = -1;
        if (outerCircleColorInt != null) {
            this.outerCirclePaint.setColor((int)outerCircleColorInt);
        }
        else if (theme != null) {
            this.outerCirclePaint.setColor(UiUtil.themeIntAttr(context, "colorPrimary"));
        }
        else {
            this.outerCirclePaint.setColor(-1);
        }
        final Integer targetCircleColorInt = this.target.targetCircleColorInt(context);
        if (targetCircleColorInt != null) {
            this.targetCirclePaint.setColor((int)targetCircleColorInt);
        }
        else {
            final Paint targetCirclePaint = this.targetCirclePaint;
            int color;
            if (this.isDark) {
                color = -16777216;
            }
            else {
                color = -1;
            }
            targetCirclePaint.setColor(color);
        }
        if (this.target.transparentTarget) {
            this.targetCirclePaint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.CLEAR));
        }
        this.targetCirclePulsePaint.setColor(this.targetCirclePaint.getColor());
        final Integer dimColorInt = this.target.dimColorInt(context);
        if (dimColorInt != null) {
            this.dimColor = UiUtil.setAlpha(dimColorInt, 0.3f);
        }
        else {
            this.dimColor = -1;
        }
        final Integer titleTextColorInt = this.target.titleTextColorInt(context);
        if (titleTextColorInt != null) {
            this.titlePaint.setColor((int)titleTextColorInt);
        }
        else {
            final TextPaint titlePaint = this.titlePaint;
            int color2 = n;
            if (this.isDark) {
                color2 = -16777216;
            }
            titlePaint.setColor(color2);
        }
        final Integer descriptionTextColorInt = this.target.descriptionTextColorInt(context);
        if (descriptionTextColorInt != null) {
            this.descriptionPaint.setColor((int)descriptionTextColorInt);
        }
        else {
            this.descriptionPaint.setColor(this.titlePaint.getColor());
        }
        if (this.target.titleTypeface != null) {
            this.titlePaint.setTypeface(this.target.titleTypeface);
        }
        if (this.target.descriptionTypeface != null) {
            this.descriptionPaint.setTypeface(this.target.descriptionTypeface);
        }
    }
    
    void calculateDimensions() {
        this.textBounds = this.getTextBounds();
        final int[] outerCircleCenterPoint = this.getOuterCircleCenterPoint();
        this.outerCircleCenter = outerCircleCenterPoint;
        this.calculatedOuterCircleRadius = this.getOuterCircleRadius(outerCircleCenterPoint[0], outerCircleCenterPoint[1], this.textBounds, this.targetBounds);
    }
    
    void calculateDrawingBounds() {
        final int[] outerCircleCenter = this.outerCircleCenter;
        if (outerCircleCenter == null) {
            return;
        }
        this.drawingBounds.left = (int)Math.max(0.0f, outerCircleCenter[0] - this.outerCircleRadius);
        this.drawingBounds.top = (int)Math.min(0.0f, this.outerCircleCenter[1] - this.outerCircleRadius);
        this.drawingBounds.right = (int)Math.min((float)this.getWidth(), this.outerCircleCenter[0] + this.outerCircleRadius + this.CIRCLE_PADDING);
        this.drawingBounds.bottom = (int)Math.min((float)this.getHeight(), this.outerCircleCenter[1] + this.outerCircleRadius + this.CIRCLE_PADDING);
    }
    
    float delayedLerp(final float n, final float n2) {
        if (n < n2) {
            return 0.0f;
        }
        return (n - n2) / (1.0f - n2);
    }
    
    public void dismiss(final boolean b) {
        this.isDismissing = true;
        this.pulseAnimation.cancel();
        this.expandAnimation.cancel();
        if (this.visible && this.outerCircleCenter != null) {
            if (b) {
                this.dismissConfirmAnimation.start();
            }
            else {
                this.dismissAnimation.start();
            }
            return;
        }
        this.finishDismiss(b);
    }
    
    double distance(final int n, final int n2, final int n3, final int n4) {
        return Math.sqrt(Math.pow(n3 - n, 2.0) + Math.pow(n4 - n2, 2.0));
    }
    
    void drawDebugInformation(final Canvas canvas) {
        if (this.debugPaint == null) {
            (this.debugPaint = new Paint()).setARGB(255, 255, 0, 0);
            this.debugPaint.setStyle(Paint$Style.STROKE);
            this.debugPaint.setStrokeWidth((float)UiUtil.dp(this.getContext(), 1));
        }
        if (this.debugTextPaint == null) {
            (this.debugTextPaint = new TextPaint()).setColor(-65536);
            this.debugTextPaint.setTextSize((float)UiUtil.sp(this.getContext(), 16));
        }
        this.debugPaint.setStyle(Paint$Style.STROKE);
        canvas.drawRect(this.textBounds, this.debugPaint);
        canvas.drawRect(this.targetBounds, this.debugPaint);
        final int[] outerCircleCenter = this.outerCircleCenter;
        canvas.drawCircle((float)outerCircleCenter[0], (float)outerCircleCenter[1], 10.0f, this.debugPaint);
        final int[] outerCircleCenter2 = this.outerCircleCenter;
        canvas.drawCircle((float)outerCircleCenter2[0], (float)outerCircleCenter2[1], (float)(this.calculatedOuterCircleRadius - this.CIRCLE_PADDING), this.debugPaint);
        canvas.drawCircle((float)this.targetBounds.centerX(), (float)this.targetBounds.centerY(), (float)(this.TARGET_RADIUS + this.TARGET_PADDING), this.debugPaint);
        this.debugPaint.setStyle(Paint$Style.FILL);
        final StringBuilder sb = new StringBuilder();
        sb.append("Text bounds: ");
        sb.append(this.textBounds.toShortString());
        sb.append("\nTarget bounds: ");
        sb.append(this.targetBounds.toShortString());
        sb.append("\nCenter: ");
        sb.append(this.outerCircleCenter[0]);
        sb.append(" ");
        sb.append(this.outerCircleCenter[1]);
        sb.append("\nView size: ");
        sb.append(this.getWidth());
        sb.append(" ");
        sb.append(this.getHeight());
        sb.append("\nTarget bounds: ");
        sb.append(this.targetBounds.toShortString());
        final String string = sb.toString();
        final SpannableStringBuilder debugStringBuilder = this.debugStringBuilder;
        if (debugStringBuilder == null) {
            this.debugStringBuilder = new SpannableStringBuilder((CharSequence)string);
        }
        else {
            debugStringBuilder.clear();
            this.debugStringBuilder.append((CharSequence)string);
        }
        if (this.debugLayout == null) {
            this.debugLayout = new DynamicLayout((CharSequence)string, this.debugTextPaint, this.getWidth(), Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        }
        final int save = canvas.save();
        this.debugPaint.setARGB(220, 0, 0, 0);
        canvas.translate(0.0f, (float)this.topBoundary);
        canvas.drawRect(0.0f, 0.0f, (float)this.debugLayout.getWidth(), (float)this.debugLayout.getHeight(), this.debugPaint);
        this.debugPaint.setARGB(255, 255, 0, 0);
        this.debugLayout.draw(canvas);
        canvas.restoreToCount(save);
    }
    
    void drawJitteredShadow(final Canvas canvas) {
        final float n = this.outerCircleAlpha * 0.2f;
        this.outerCircleShadowPaint.setStyle(Paint$Style.FILL_AND_STROKE);
        this.outerCircleShadowPaint.setAlpha((int)n);
        final int[] outerCircleCenter = this.outerCircleCenter;
        canvas.drawCircle((float)outerCircleCenter[0], (float)(outerCircleCenter[1] + this.SHADOW_DIM), this.outerCircleRadius, this.outerCircleShadowPaint);
        this.outerCircleShadowPaint.setStyle(Paint$Style.STROKE);
        for (int i = 6; i > 0; --i) {
            this.outerCircleShadowPaint.setAlpha((int)(i / 7.0f * n));
            final int[] outerCircleCenter2 = this.outerCircleCenter;
            canvas.drawCircle((float)outerCircleCenter2[0], (float)(outerCircleCenter2[1] + this.SHADOW_DIM), this.outerCircleRadius + (7 - i) * this.SHADOW_JITTER_DIM, this.outerCircleShadowPaint);
        }
    }
    
    void drawTintedTarget() {
        final Drawable icon = this.target.icon;
        if (!this.shouldTintTarget || icon == null) {
            this.tintedTarget = null;
            return;
        }
        if (this.tintedTarget != null) {
            return;
        }
        this.tintedTarget = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight(), Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(this.tintedTarget);
        icon.setColorFilter((ColorFilter)new PorterDuffColorFilter(this.outerCirclePaint.getColor(), PorterDuff$Mode.SRC_ATOP));
        icon.draw(canvas);
        icon.setColorFilter((ColorFilter)null);
    }
    
    int[] getOuterCircleCenterPoint() {
        if (this.inGutter(this.targetBounds.centerY())) {
            return new int[] { this.targetBounds.centerX(), this.targetBounds.centerY() };
        }
        final int n = Math.max(this.targetBounds.width(), this.targetBounds.height()) / 2 + this.TARGET_PADDING;
        final int totalTextHeight = this.getTotalTextHeight();
        final boolean b = this.targetBounds.centerY() - this.TARGET_RADIUS - this.TARGET_PADDING - totalTextHeight > 0;
        final int min = Math.min(this.textBounds.left, this.targetBounds.left - n);
        final int max = Math.max(this.textBounds.right, this.targetBounds.right + n);
        final StaticLayout titleLayout = this.titleLayout;
        int height;
        if (titleLayout == null) {
            height = 0;
        }
        else {
            height = titleLayout.getHeight();
        }
        int n2;
        if (b) {
            n2 = this.targetBounds.centerY() - this.TARGET_RADIUS - this.TARGET_PADDING - totalTextHeight + height;
        }
        else {
            n2 = this.targetBounds.centerY() + this.TARGET_RADIUS + this.TARGET_PADDING + height;
        }
        return new int[] { (min + max) / 2, n2 };
    }
    
    int getOuterCircleRadius(final int n, final int n2, final Rect rect, Rect rect2) {
        final int centerX = rect2.centerX();
        final int centerY = rect2.centerY();
        final int n3 = (int)(this.TARGET_RADIUS * 1.1f);
        rect2 = new Rect(centerX, centerY, centerX, centerY);
        final int n4 = -n3;
        rect2.inset(n4, n4);
        return Math.max(this.maxDistanceToPoints(n, n2, rect), this.maxDistanceToPoints(n, n2, rect2)) + this.CIRCLE_PADDING;
    }
    
    Rect getTextBounds() {
        final int totalTextHeight = this.getTotalTextHeight();
        final int totalTextWidth = this.getTotalTextWidth();
        int n = this.targetBounds.centerY() - this.TARGET_RADIUS - this.TARGET_PADDING - totalTextHeight;
        if (n <= this.topBoundary) {
            n = this.targetBounds.centerY() + this.TARGET_RADIUS + this.TARGET_PADDING;
        }
        int text_POSITIONING_BIAS;
        if (this.getWidth() / 2 - this.targetBounds.centerX() < 0) {
            text_POSITIONING_BIAS = -this.TEXT_POSITIONING_BIAS;
        }
        else {
            text_POSITIONING_BIAS = this.TEXT_POSITIONING_BIAS;
        }
        final int max = Math.max(this.TEXT_PADDING, this.targetBounds.centerX() - text_POSITIONING_BIAS - totalTextWidth);
        return new Rect(max, n, Math.min(this.getWidth() - this.TEXT_PADDING, totalTextWidth + max), totalTextHeight + n);
    }
    
    int getTotalTextHeight() {
        final StaticLayout titleLayout = this.titleLayout;
        if (titleLayout == null) {
            return 0;
        }
        int height;
        int n;
        if (this.descriptionLayout == null) {
            height = titleLayout.getHeight();
            n = this.TEXT_SPACING;
        }
        else {
            height = titleLayout.getHeight() + this.descriptionLayout.getHeight();
            n = this.TEXT_SPACING;
        }
        return height + n;
    }
    
    int getTotalTextWidth() {
        final StaticLayout titleLayout = this.titleLayout;
        if (titleLayout == null) {
            return 0;
        }
        if (this.descriptionLayout == null) {
            return titleLayout.getWidth();
        }
        return Math.max(titleLayout.getWidth(), this.descriptionLayout.getWidth());
    }
    
    float halfwayLerp(final float n) {
        if (n < 0.5f) {
            return n / 0.5f;
        }
        return (1.0f - n) / 0.5f;
    }
    
    boolean inGutter(final int n) {
        final int bottomBoundary = this.bottomBoundary;
        boolean b = false;
        final boolean b2 = false;
        if (bottomBoundary > 0) {
            final int gutter_DIM = this.GUTTER_DIM;
            if (n >= gutter_DIM) {
                final boolean b3 = b2;
                if (n <= bottomBoundary - gutter_DIM) {
                    return b3;
                }
            }
            return true;
        }
        if (n < this.GUTTER_DIM || n > this.getHeight() - this.GUTTER_DIM) {
            b = true;
        }
        return b;
    }
    
    void invalidateViewAndOutline(final Rect rect) {
        this.invalidate(rect);
        if (this.outlineProvider != null && Build$VERSION.SDK_INT >= 21) {
            this.invalidateOutline();
        }
    }
    
    public boolean isVisible() {
        return !this.isDismissed && this.visible;
    }
    
    int maxDistanceToPoints(final int n, final int n2, final Rect rect) {
        return (int)Math.max(this.distance(n, n2, rect.left, rect.top), Math.max(this.distance(n, n2, rect.right, rect.top), Math.max(this.distance(n, n2, rect.left, rect.bottom), this.distance(n, n2, rect.right, rect.bottom))));
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.onDismiss(false);
    }
    
    void onDismiss(final boolean b) {
        if (this.isDismissed) {
            return;
        }
        this.isDismissing = false;
        this.isDismissed = true;
        for (final ValueAnimator valueAnimator : this.animators) {
            valueAnimator.cancel();
            valueAnimator.removeAllUpdateListeners();
        }
        ViewUtil.removeOnGlobalLayoutListener(this.getViewTreeObserver(), this.globalLayoutListener);
        this.visible = false;
        final Listener listener = this.listener;
        if (listener != null) {
            listener.onTargetDismissed(this, b);
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        if (!this.isDismissed) {
            if (this.outerCircleCenter != null) {
                final int topBoundary = this.topBoundary;
                if (topBoundary > 0 && this.bottomBoundary > 0) {
                    canvas.clipRect(0, topBoundary, this.getWidth(), this.bottomBoundary);
                }
                final int dimColor = this.dimColor;
                if (dimColor != -1) {
                    canvas.drawColor(dimColor);
                }
                this.outerCirclePaint.setAlpha(this.outerCircleAlpha);
                if (this.shouldDrawShadow && this.outlineProvider == null) {
                    final int save = canvas.save();
                    canvas.clipPath(this.outerCirclePath, Region$Op.DIFFERENCE);
                    this.drawJitteredShadow(canvas);
                    canvas.restoreToCount(save);
                }
                final int[] outerCircleCenter = this.outerCircleCenter;
                canvas.drawCircle((float)outerCircleCenter[0], (float)outerCircleCenter[1], this.outerCircleRadius, this.outerCirclePaint);
                this.targetCirclePaint.setAlpha(this.targetCircleAlpha);
                final int targetCirclePulseAlpha = this.targetCirclePulseAlpha;
                if (targetCirclePulseAlpha > 0) {
                    this.targetCirclePulsePaint.setAlpha(targetCirclePulseAlpha);
                    canvas.drawCircle((float)this.targetBounds.centerX(), (float)this.targetBounds.centerY(), this.targetCirclePulseRadius, this.targetCirclePulsePaint);
                }
                canvas.drawCircle((float)this.targetBounds.centerX(), (float)this.targetBounds.centerY(), this.targetCircleRadius, this.targetCirclePaint);
                final int save2 = canvas.save();
                canvas.translate((float)this.textBounds.left, (float)this.textBounds.top);
                this.titlePaint.setAlpha(this.textAlpha);
                final StaticLayout titleLayout = this.titleLayout;
                if (titleLayout != null) {
                    titleLayout.draw(canvas);
                }
                if (this.descriptionLayout != null) {
                    final StaticLayout titleLayout2 = this.titleLayout;
                    if (titleLayout2 != null) {
                        canvas.translate(0.0f, (float)(titleLayout2.getHeight() + this.TEXT_SPACING));
                        this.descriptionPaint.setAlpha((int)(this.target.descriptionTextAlpha * this.textAlpha));
                        this.descriptionLayout.draw(canvas);
                    }
                }
                canvas.restoreToCount(save2);
                final int save3 = canvas.save();
                if (this.tintedTarget != null) {
                    canvas.translate((float)(this.targetBounds.centerX() - this.tintedTarget.getWidth() / 2), (float)(this.targetBounds.centerY() - this.tintedTarget.getHeight() / 2));
                    canvas.drawBitmap(this.tintedTarget, 0.0f, 0.0f, this.targetCirclePaint);
                }
                else if (this.target.icon != null) {
                    canvas.translate((float)(this.targetBounds.centerX() - this.target.icon.getBounds().width() / 2), (float)(this.targetBounds.centerY() - this.target.icon.getBounds().height() / 2));
                    this.target.icon.setAlpha(this.targetCirclePaint.getAlpha());
                    this.target.icon.draw(canvas);
                }
                canvas.restoreToCount(save3);
                if (this.debug) {
                    this.drawDebugInformation(canvas);
                }
            }
        }
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        if (this.isVisible() && this.cancelable && n == 4) {
            keyEvent.startTracking();
            return true;
        }
        return false;
    }
    
    public boolean onKeyUp(final int n, final KeyEvent keyEvent) {
        if (this.isVisible() && this.isInteractable && this.cancelable && n == 4 && keyEvent.isTracking() && !keyEvent.isCanceled()) {
            this.isInteractable = false;
            final Listener listener = this.listener;
            if (listener != null) {
                listener.onTargetCancel(this);
            }
            else {
                new Listener().onTargetCancel(this);
            }
            return true;
        }
        return false;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        this.lastTouchX = motionEvent.getX();
        this.lastTouchY = motionEvent.getY();
        return super.onTouchEvent(motionEvent);
    }
    
    public void setDrawDebug(final boolean debug) {
        if (this.debug != debug) {
            this.debug = debug;
            this.postInvalidate();
        }
    }
    
    void updateTextLayouts() {
        final int n = Math.min(this.getWidth(), this.TEXT_MAX_WIDTH) - this.TEXT_PADDING * 2;
        if (n <= 0) {
            return;
        }
        this.titleLayout = new StaticLayout(this.title, this.titlePaint, n, Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        if (this.description != null) {
            this.descriptionLayout = new StaticLayout(this.description, this.descriptionPaint, n, Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        }
        else {
            this.descriptionLayout = null;
        }
    }
    
    public static class Listener
    {
        public void onOuterCircleClick(final TapTargetView tapTargetView) {
        }
        
        public void onTargetCancel(final TapTargetView tapTargetView) {
            tapTargetView.dismiss(false);
        }
        
        public void onTargetClick(final TapTargetView tapTargetView) {
            tapTargetView.dismiss(true);
        }
        
        public void onTargetDismissed(final TapTargetView tapTargetView, final boolean b) {
        }
        
        public void onTargetLongClick(final TapTargetView tapTargetView) {
            this.onTargetClick(tapTargetView);
        }
    }
}

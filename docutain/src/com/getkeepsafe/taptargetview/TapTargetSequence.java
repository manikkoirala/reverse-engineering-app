// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import java.util.Collections;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.LinkedList;
import java.util.Queue;
import android.app.Dialog;
import android.app.Activity;

public class TapTargetSequence
{
    private boolean active;
    private final Activity activity;
    boolean considerOuterCircleCanceled;
    boolean continueOnCancel;
    private TapTargetView currentView;
    private final Dialog dialog;
    Listener listener;
    private final TapTargetView.Listener tapTargetListener;
    private final Queue<TapTarget> targets;
    
    public TapTargetSequence(final Activity activity) {
        this.tapTargetListener = new TapTargetView.Listener() {
            final TapTargetSequence this$0;
            
            @Override
            public void onOuterCircleClick(final TapTargetView tapTargetView) {
                if (this.this$0.considerOuterCircleCanceled) {
                    this.onTargetCancel(tapTargetView);
                }
            }
            
            @Override
            public void onTargetCancel(final TapTargetView tapTargetView) {
                super.onTargetCancel(tapTargetView);
                if (this.this$0.continueOnCancel) {
                    if (this.this$0.listener != null) {
                        this.this$0.listener.onSequenceStep(tapTargetView.target, false);
                    }
                    this.this$0.showNext();
                }
                else if (this.this$0.listener != null) {
                    this.this$0.listener.onSequenceCanceled(tapTargetView.target);
                }
            }
            
            @Override
            public void onTargetClick(final TapTargetView tapTargetView) {
                super.onTargetClick(tapTargetView);
                if (this.this$0.listener != null) {
                    this.this$0.listener.onSequenceStep(tapTargetView.target, true);
                }
                this.this$0.showNext();
            }
        };
        if (activity != null) {
            this.activity = activity;
            this.dialog = null;
            this.targets = new LinkedList<TapTarget>();
            return;
        }
        throw new IllegalArgumentException("Activity is null");
    }
    
    public TapTargetSequence(final Dialog dialog) {
        this.tapTargetListener = new TapTargetView.Listener() {
            final TapTargetSequence this$0;
            
            @Override
            public void onOuterCircleClick(final TapTargetView tapTargetView) {
                if (this.this$0.considerOuterCircleCanceled) {
                    this.onTargetCancel(tapTargetView);
                }
            }
            
            @Override
            public void onTargetCancel(final TapTargetView tapTargetView) {
                super.onTargetCancel(tapTargetView);
                if (this.this$0.continueOnCancel) {
                    if (this.this$0.listener != null) {
                        this.this$0.listener.onSequenceStep(tapTargetView.target, false);
                    }
                    this.this$0.showNext();
                }
                else if (this.this$0.listener != null) {
                    this.this$0.listener.onSequenceCanceled(tapTargetView.target);
                }
            }
            
            @Override
            public void onTargetClick(final TapTargetView tapTargetView) {
                super.onTargetClick(tapTargetView);
                if (this.this$0.listener != null) {
                    this.this$0.listener.onSequenceStep(tapTargetView.target, true);
                }
                this.this$0.showNext();
            }
        };
        if (dialog != null) {
            this.dialog = dialog;
            this.activity = null;
            this.targets = new LinkedList<TapTarget>();
            return;
        }
        throw new IllegalArgumentException("Given null Dialog");
    }
    
    public boolean cancel() {
        if (this.active) {
            final TapTargetView currentView = this.currentView;
            if (currentView != null) {
                if (currentView.cancelable) {
                    this.currentView.dismiss(false);
                    this.active = false;
                    this.targets.clear();
                    final Listener listener = this.listener;
                    if (listener != null) {
                        listener.onSequenceCanceled(this.currentView.target);
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    public TapTargetSequence considerOuterCircleCanceled(final boolean considerOuterCircleCanceled) {
        this.considerOuterCircleCanceled = considerOuterCircleCanceled;
        return this;
    }
    
    public TapTargetSequence continueOnCancel(final boolean continueOnCancel) {
        this.continueOnCancel = continueOnCancel;
        return this;
    }
    
    public TapTargetSequence listener(final Listener listener) {
        this.listener = listener;
        return this;
    }
    
    void showNext() {
        try {
            final TapTarget tapTarget = this.targets.remove();
            final Activity activity = this.activity;
            if (activity != null) {
                this.currentView = TapTargetView.showFor(activity, tapTarget, this.tapTargetListener);
            }
            else {
                this.currentView = TapTargetView.showFor(this.dialog, tapTarget, this.tapTargetListener);
            }
        }
        catch (final NoSuchElementException ex) {
            this.currentView = null;
            final Listener listener = this.listener;
            if (listener != null) {
                listener.onSequenceFinish();
            }
        }
    }
    
    public void start() {
        if (!this.targets.isEmpty()) {
            if (!this.active) {
                this.active = true;
                this.showNext();
            }
        }
    }
    
    public void startAt(final int n) {
        if (this.active) {
            return;
        }
        if (n < 0 || n >= this.targets.size()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Given invalid index ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        final int n2 = this.targets.size() - n;
        while (this.targets.peek() != null && this.targets.size() != n2) {
            this.targets.poll();
        }
        if (this.targets.size() == n2) {
            this.start();
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Given index ");
        sb2.append(n);
        sb2.append(" not in sequence");
        throw new IllegalStateException(sb2.toString());
    }
    
    public void startWith(final int i) {
        if (this.active) {
            return;
        }
        while (this.targets.peek() != null && this.targets.peek().id() != i) {
            this.targets.poll();
        }
        final TapTarget tapTarget = this.targets.peek();
        if (tapTarget != null && tapTarget.id() == i) {
            this.start();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Given target ");
        sb.append(i);
        sb.append(" not in sequence");
        throw new IllegalStateException(sb.toString());
    }
    
    public TapTargetSequence target(final TapTarget tapTarget) {
        this.targets.add(tapTarget);
        return this;
    }
    
    public TapTargetSequence targets(final List<TapTarget> list) {
        this.targets.addAll((Collection<?>)list);
        return this;
    }
    
    public TapTargetSequence targets(final TapTarget... elements) {
        Collections.addAll(this.targets, elements);
        return this;
    }
    
    public interface Listener
    {
        void onSequenceCanceled(final TapTarget p0);
        
        void onSequenceFinish();
        
        void onSequenceStep(final TapTarget p0, final boolean p1);
    }
}

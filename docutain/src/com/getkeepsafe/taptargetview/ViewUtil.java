// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import android.view.ViewManager;
import android.os.Build$VERSION;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import androidx.core.view.ViewCompat;
import android.view.View;

class ViewUtil
{
    private static boolean isLaidOut(final View view) {
        return ViewCompat.isLaidOut(view) && view.getWidth() > 0 && view.getHeight() > 0;
    }
    
    static void onLaidOut(final View view, final Runnable runnable) {
        if (isLaidOut(view)) {
            runnable.run();
            return;
        }
        final ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(viewTreeObserver, view, runnable) {
            final ViewTreeObserver val$observer;
            final Runnable val$runnable;
            final View val$view;
            
            public void onGlobalLayout() {
                ViewTreeObserver viewTreeObserver;
                if (this.val$observer.isAlive()) {
                    viewTreeObserver = this.val$observer;
                }
                else {
                    viewTreeObserver = this.val$view.getViewTreeObserver();
                }
                ViewUtil.removeOnGlobalLayoutListener(viewTreeObserver, (ViewTreeObserver$OnGlobalLayoutListener)this);
                this.val$runnable.run();
            }
        });
    }
    
    static void removeOnGlobalLayoutListener(final ViewTreeObserver viewTreeObserver, final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener) {
        if (Build$VERSION.SDK_INT >= 16) {
            viewTreeObserver.removeOnGlobalLayoutListener(viewTreeObserver$OnGlobalLayoutListener);
        }
        else {
            viewTreeObserver.removeGlobalOnLayoutListener(viewTreeObserver$OnGlobalLayoutListener);
        }
    }
    
    static void removeView(final ViewManager viewManager, final View view) {
        if (viewManager == null) {
            return;
        }
        if (view == null) {
            return;
        }
        try {
            viewManager.removeView(view);
        }
        catch (final Exception ex) {}
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import android.os.Build$VERSION;
import android.widget.ImageView;
import android.view.ViewGroup;
import java.util.Stack;
import android.graphics.drawable.Drawable;
import android.widget.ImageButton;
import java.util.ArrayList;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toolbar;

class ToolbarTapTarget extends ViewTapTarget
{
    ToolbarTapTarget(final Toolbar toolbar, final int n, final CharSequence charSequence, final CharSequence charSequence2) {
        super(toolbar.findViewById(n), charSequence, charSequence2);
    }
    
    ToolbarTapTarget(final Toolbar toolbar, final boolean b, final CharSequence charSequence, final CharSequence charSequence2) {
        View view;
        if (b) {
            view = findNavView(toolbar);
        }
        else {
            view = findOverflowView(toolbar);
        }
        super(view, charSequence, charSequence2);
    }
    
    ToolbarTapTarget(final androidx.appcompat.widget.Toolbar toolbar, final int n, final CharSequence charSequence, final CharSequence charSequence2) {
        super(toolbar.findViewById(n), charSequence, charSequence2);
    }
    
    ToolbarTapTarget(final androidx.appcompat.widget.Toolbar toolbar, final boolean b, final CharSequence charSequence, final CharSequence charSequence2) {
        View view;
        if (b) {
            view = findNavView(toolbar);
        }
        else {
            view = findOverflowView(toolbar);
        }
        super(view, charSequence, charSequence2);
    }
    
    private static View findNavView(final Object o) {
        final ToolbarProxy proxy = proxyOf(o);
        CharSequence navigationContentDescription = proxy.getNavigationContentDescription();
        final boolean b = TextUtils.isEmpty(navigationContentDescription) ^ true;
        if (!b) {
            navigationContentDescription = "taptarget-findme";
        }
        proxy.setNavigationContentDescription(navigationContentDescription);
        final ArrayList list = new ArrayList<View>(1);
        proxy.findViewsWithText(list, navigationContentDescription, 2);
        if (!b) {
            proxy.setNavigationContentDescription(null);
        }
        final int size = list.size();
        int i = 0;
        if (size > 0) {
            return (View)list.get(0);
        }
        final Drawable navigationIcon = proxy.getNavigationIcon();
        if (navigationIcon != null) {
            while (i < proxy.getChildCount()) {
                final View child = proxy.getChildAt(i);
                if (child instanceof ImageButton && ((ImageButton)child).getDrawable() == navigationIcon) {
                    return child;
                }
                ++i;
            }
            throw new IllegalStateException("Could not find navigation view for Toolbar!");
        }
        throw new IllegalStateException("Toolbar does not have a navigation view set!");
    }
    
    private static View findOverflowView(final Object o) {
        final ToolbarProxy proxy = proxyOf(o);
        final Drawable overflowIcon = proxy.getOverflowIcon();
        if (overflowIcon != null) {
            final Stack stack = new Stack();
            stack.push(proxy.internalToolbar());
            while (!stack.empty()) {
                final ViewGroup viewGroup = stack.pop();
                for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                    final View child = viewGroup.getChildAt(i);
                    if (child instanceof ViewGroup) {
                        stack.push(child);
                    }
                    else if (child instanceof ImageView && ((ImageView)child).getDrawable() == overflowIcon) {
                        return child;
                    }
                }
            }
        }
        try {
            return (View)ReflectUtil.getPrivateField(ReflectUtil.getPrivateField(ReflectUtil.getPrivateField(proxy.internalToolbar(), "mMenuView"), "mPresenter"), "mOverflowButton");
        }
        catch (final IllegalAccessException cause) {
            throw new IllegalStateException("Unable to access overflow view for Toolbar!", cause);
        }
        catch (final NoSuchFieldException cause2) {
            throw new IllegalStateException("Could not find overflow view for Toolbar!", cause2);
        }
    }
    
    private static ToolbarProxy proxyOf(final Object o) {
        if (o == null) {
            throw new IllegalArgumentException("Given null instance");
        }
        if (o instanceof androidx.appcompat.widget.Toolbar) {
            return (ToolbarProxy)new SupportToolbarProxy((androidx.appcompat.widget.Toolbar)o);
        }
        if (o instanceof Toolbar) {
            return (ToolbarProxy)new StandardToolbarProxy((Toolbar)o);
        }
        throw new IllegalStateException("Couldn't provide proper toolbar proxy instance");
    }
    
    private static class StandardToolbarProxy implements ToolbarProxy
    {
        private final Toolbar toolbar;
        
        StandardToolbarProxy(final Toolbar toolbar) {
            this.toolbar = toolbar;
        }
        
        @Override
        public void findViewsWithText(final ArrayList<View> list, final CharSequence charSequence, final int n) {
            this.toolbar.findViewsWithText((ArrayList)list, charSequence, n);
        }
        
        @Override
        public View getChildAt(final int n) {
            return this.toolbar.getChildAt(n);
        }
        
        @Override
        public int getChildCount() {
            return this.toolbar.getChildCount();
        }
        
        @Override
        public CharSequence getNavigationContentDescription() {
            return this.toolbar.getNavigationContentDescription();
        }
        
        @Override
        public Drawable getNavigationIcon() {
            return this.toolbar.getNavigationIcon();
        }
        
        @Override
        public Drawable getOverflowIcon() {
            if (Build$VERSION.SDK_INT >= 23) {
                return this.toolbar.getOverflowIcon();
            }
            return null;
        }
        
        @Override
        public Object internalToolbar() {
            return this.toolbar;
        }
        
        @Override
        public void setNavigationContentDescription(final CharSequence navigationContentDescription) {
            this.toolbar.setNavigationContentDescription(navigationContentDescription);
        }
    }
    
    private interface ToolbarProxy
    {
        void findViewsWithText(final ArrayList<View> p0, final CharSequence p1, final int p2);
        
        View getChildAt(final int p0);
        
        int getChildCount();
        
        CharSequence getNavigationContentDescription();
        
        Drawable getNavigationIcon();
        
        Drawable getOverflowIcon();
        
        Object internalToolbar();
        
        void setNavigationContentDescription(final CharSequence p0);
    }
    
    private static class SupportToolbarProxy implements ToolbarProxy
    {
        private final androidx.appcompat.widget.Toolbar toolbar;
        
        SupportToolbarProxy(final androidx.appcompat.widget.Toolbar toolbar) {
            this.toolbar = toolbar;
        }
        
        @Override
        public void findViewsWithText(final ArrayList<View> list, final CharSequence charSequence, final int n) {
            this.toolbar.findViewsWithText((ArrayList)list, charSequence, n);
        }
        
        @Override
        public View getChildAt(final int n) {
            return this.toolbar.getChildAt(n);
        }
        
        @Override
        public int getChildCount() {
            return this.toolbar.getChildCount();
        }
        
        @Override
        public CharSequence getNavigationContentDescription() {
            return this.toolbar.getNavigationContentDescription();
        }
        
        @Override
        public Drawable getNavigationIcon() {
            return this.toolbar.getNavigationIcon();
        }
        
        @Override
        public Drawable getOverflowIcon() {
            return this.toolbar.getOverflowIcon();
        }
        
        @Override
        public Object internalToolbar() {
            return this.toolbar;
        }
        
        @Override
        public void setNavigationContentDescription(final CharSequence navigationContentDescription) {
            this.toolbar.setNavigationContentDescription(navigationContentDescription);
        }
    }
}

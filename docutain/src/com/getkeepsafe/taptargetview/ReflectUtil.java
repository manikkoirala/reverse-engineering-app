// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import java.lang.reflect.Field;

class ReflectUtil
{
    static Object getPrivateField(final Object obj, final String name) throws NoSuchFieldException, IllegalAccessException {
        final Field declaredField = obj.getClass().getDeclaredField(name);
        declaredField.setAccessible(true);
        return declaredField.get(obj);
    }
}

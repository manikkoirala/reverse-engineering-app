// 
// Decompiled by Procyon v0.6.0
// 

package com.getkeepsafe.taptargetview;

import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.TimeInterpolator;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;

class FloatValueAnimatorBuilder
{
    final ValueAnimator animator;
    EndListener endListener;
    
    protected FloatValueAnimatorBuilder() {
        this(false);
    }
    
    protected FloatValueAnimatorBuilder(final boolean b) {
        if (b) {
            this.animator = ValueAnimator.ofFloat(new float[] { 1.0f, 0.0f });
        }
        else {
            this.animator = ValueAnimator.ofFloat(new float[] { 0.0f, 1.0f });
        }
    }
    
    public ValueAnimator build() {
        if (this.endListener != null) {
            this.animator.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this) {
                final FloatValueAnimatorBuilder this$0;
                
                public void onAnimationEnd(final Animator animator) {
                    this.this$0.endListener.onEnd();
                }
            });
        }
        return this.animator;
    }
    
    public FloatValueAnimatorBuilder delayBy(final long startDelay) {
        this.animator.setStartDelay(startDelay);
        return this;
    }
    
    public FloatValueAnimatorBuilder duration(final long duration) {
        this.animator.setDuration(duration);
        return this;
    }
    
    public FloatValueAnimatorBuilder interpolator(final TimeInterpolator interpolator) {
        this.animator.setInterpolator(interpolator);
        return this;
    }
    
    public FloatValueAnimatorBuilder onEnd(final EndListener endListener) {
        this.endListener = endListener;
        return this;
    }
    
    public FloatValueAnimatorBuilder onUpdate(final UpdateListener updateListener) {
        this.animator.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener(this, updateListener) {
            final FloatValueAnimatorBuilder this$0;
            final UpdateListener val$listener;
            
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                this.val$listener.onUpdate((float)valueAnimator.getAnimatedValue());
            }
        });
        return this;
    }
    
    public FloatValueAnimatorBuilder repeat(final int repeatCount) {
        this.animator.setRepeatCount(repeatCount);
        return this;
    }
    
    interface EndListener
    {
        void onEnd();
    }
    
    interface UpdateListener
    {
        void onUpdate(final float p0);
    }
}

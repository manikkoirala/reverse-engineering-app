// 
// Decompiled by Procyon v0.6.0
// 

package com.facebook.device.yearclass;

import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import android.content.Context;

public class YearClass
{
    public static final int CLASS_2008 = 2008;
    public static final int CLASS_2009 = 2009;
    public static final int CLASS_2010 = 2010;
    public static final int CLASS_2011 = 2011;
    public static final int CLASS_2012 = 2012;
    public static final int CLASS_2013 = 2013;
    public static final int CLASS_2014 = 2014;
    public static final int CLASS_2015 = 2015;
    public static final int CLASS_UNKNOWN = -1;
    private static final long MB = 1048576L;
    private static final int MHZ_IN_KHZ = 1000;
    private static volatile Integer mYearCategory;
    
    private static int categorizeByYear2014Method(final Context context) {
        final ArrayList list = new ArrayList();
        conditionallyAdd(list, getNumCoresYear());
        conditionallyAdd(list, getClockSpeedYear());
        conditionallyAdd(list, getRamYear(context));
        if (list.isEmpty()) {
            return -1;
        }
        Collections.sort((List<Comparable>)list);
        if ((list.size() & 0x1) == 0x1) {
            return (int)list.get(list.size() / 2);
        }
        final int n = list.size() / 2 - 1;
        return (int)list.get(n) + ((int)list.get(n + 1) - (int)list.get(n)) / 2;
    }
    
    private static int categorizeByYear2016Method(final Context context) {
        final long totalMemory = DeviceInfo.getTotalMemory(context);
        if (totalMemory == -1L) {
            return categorizeByYear2014Method(context);
        }
        if (totalMemory <= 805306368L) {
            int n;
            if (DeviceInfo.getNumberOfCPUCores() <= 1) {
                n = 2009;
            }
            else {
                n = 2010;
            }
            return n;
        }
        int n2 = 2012;
        if (totalMemory <= 1073741824L) {
            if (DeviceInfo.getCPUMaxFreqKHz() < 1300000) {
                n2 = 2011;
            }
            return n2;
        }
        if (totalMemory <= 1610612736L) {
            if (DeviceInfo.getCPUMaxFreqKHz() >= 1800000) {
                n2 = 2013;
            }
            return n2;
        }
        if (totalMemory <= 2147483648L) {
            return 2013;
        }
        int n3;
        if (totalMemory <= 3221225472L) {
            n3 = 2014;
        }
        else {
            n3 = 2015;
        }
        return n3;
    }
    
    private static void conditionallyAdd(final ArrayList<Integer> list, final int i) {
        if (i != -1) {
            list.add(i);
        }
    }
    
    public static int get(final Context context) {
        if (YearClass.mYearCategory == null) {
            synchronized (YearClass.class) {
                if (YearClass.mYearCategory == null) {
                    YearClass.mYearCategory = categorizeByYear2016Method(context);
                }
            }
        }
        return YearClass.mYearCategory;
    }
    
    private static int getClockSpeedYear() {
        final long n = DeviceInfo.getCPUMaxFreqKHz();
        if (n == -1L) {
            return -1;
        }
        if (n <= 528000L) {
            return 2008;
        }
        if (n <= 620000L) {
            return 2009;
        }
        if (n <= 1020000L) {
            return 2010;
        }
        if (n <= 1220000L) {
            return 2011;
        }
        if (n <= 1520000L) {
            return 2012;
        }
        if (n <= 2020000L) {
            return 2013;
        }
        return 2014;
    }
    
    private static int getNumCoresYear() {
        final int numberOfCPUCores = DeviceInfo.getNumberOfCPUCores();
        if (numberOfCPUCores < 1) {
            return -1;
        }
        if (numberOfCPUCores == 1) {
            return 2008;
        }
        if (numberOfCPUCores <= 3) {
            return 2011;
        }
        return 2012;
    }
    
    private static int getRamYear(final Context context) {
        final long totalMemory = DeviceInfo.getTotalMemory(context);
        if (totalMemory <= 0L) {
            return -1;
        }
        if (totalMemory <= 201326592L) {
            return 2008;
        }
        if (totalMemory <= 304087040L) {
            return 2009;
        }
        if (totalMemory <= 536870912L) {
            return 2010;
        }
        if (totalMemory <= 1073741824L) {
            return 2011;
        }
        if (totalMemory <= 1610612736L) {
            return 2012;
        }
        if (totalMemory <= 2147483648L) {
            return 2013;
        }
        return 2014;
    }
}

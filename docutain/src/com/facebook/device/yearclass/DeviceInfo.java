// 
// Decompiled by Procyon v0.6.0
// 

package com.facebook.device.yearclass;

import android.app.ActivityManager;
import android.app.ActivityManager$MemoryInfo;
import android.content.Context;
import android.os.Build$VERSION;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileFilter;

public class DeviceInfo
{
    private static final FileFilter CPU_FILTER;
    public static final int DEVICEINFO_UNKNOWN = -1;
    
    static {
        CPU_FILTER = new FileFilter() {
            @Override
            public boolean accept(final File file) {
                final String name = file.getName();
                if (name.startsWith("cpu")) {
                    for (int i = 3; i < name.length(); ++i) {
                        if (!Character.isDigit(name.charAt(i))) {
                            return false;
                        }
                    }
                    return true;
                }
                return false;
            }
        };
    }
    
    private static int extractValue(final byte[] ascii, int i) {
        while (i < ascii.length) {
            final byte codePoint = ascii[i];
            if (codePoint == 10) {
                break;
            }
            if (Character.isDigit(codePoint)) {
                int n;
                for (n = i + 1; n < ascii.length && Character.isDigit(ascii[n]); ++n) {}
                return Integer.parseInt(new String(ascii, 0, i, n - i));
            }
            ++i;
        }
        return -1;
    }
    
    public static int getCPUMaxFreqKHz() {
        final int i = 0;
        final int n = -1;
        try {
            if (i >= getNumberOfCPUCores()) {
                goto Label_0194;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("/sys/devices/system/cpu/cpu");
            sb.append(i);
            sb.append("/cpufreq/cpuinfo_max_freq");
            final File file = new File(sb.toString());
            int intValue = n;
            if (!file.exists()) {
                goto Label_0186;
            }
            final byte[] array = new byte[128];
            final FileInputStream fileInputStream = new FileInputStream(file);
            while (true) {
                try {
                    fileInputStream.read(array);
                    for (intValue = 0; Character.isDigit(array[intValue]) && intValue < 128; ++intValue) {}
                    final Integer value = Integer.parseInt(new String(array, 0, intValue));
                    if (value > (intValue = n)) {
                        intValue = value;
                        break Label_0168;
                    }
                    break Label_0168;
                    goto Label_0186;
                }
                catch (final NumberFormatException ex) {
                    intValue = n;
                    continue;
                }
                finally {
                    fileInputStream.close();
                }
                break;
            }
            try {
                final FileInputStream fileInputStream2;
                int fileForValue = parseFileForValue("cpu MHz", fileInputStream2);
                final int n2 = fileForValue * 1000;
                fileForValue = n;
                if (n2 > n) {
                    fileForValue = n2;
                }
            }
            finally {}
        }
        catch (final IOException ex2) {}
    }
    
    private static int getCoresFromCPUFileList() {
        return new File("/sys/devices/system/cpu/").listFiles(DeviceInfo.CPU_FILTER).length;
    }
    
    private static int getCoresFromFileInfo(final String name) {
        try {
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(name)));
            final String line = bufferedReader.readLine();
            bufferedReader.close();
            return getCoresFromFileString(line);
        }
        catch (final IOException ex) {
            return -1;
        }
    }
    
    static int getCoresFromFileString(final String s) {
        if (s != null && s.matches("0-[\\d]+$")) {
            return Integer.valueOf(s.substring(2)) + 1;
        }
        return -1;
    }
    
    public static int getNumberOfCPUCores() {
        if (Build$VERSION.SDK_INT <= 10) {
            return 1;
        }
        final int n = -1;
        try {
            int n2;
            if ((n2 = getCoresFromFileInfo("/sys/devices/system/cpu/possible")) == -1) {
                n2 = getCoresFromFileInfo("/sys/devices/system/cpu/present");
            }
            if (n2 == -1) {
                n2 = getCoresFromCPUFileList();
            }
            return n2;
        }
        catch (final SecurityException | NullPointerException ex) {
            return n;
        }
    }
    
    public static long getTotalMemory(Context context) {
        if (Build$VERSION.SDK_INT >= 16) {
            final ActivityManager$MemoryInfo activityManager$MemoryInfo = new ActivityManager$MemoryInfo();
            ((ActivityManager)context.getSystemService("activity")).getMemoryInfo(activityManager$MemoryInfo);
            return activityManager$MemoryInfo.totalMem;
        }
        long n2;
        long n = n2 = -1L;
        try {
            n2 = n;
            context = (Context)new FileInputStream("/proc/meminfo");
            try {
                n = parseFileForValue("MemTotal", (FileInputStream)context) * 1024L;
            }
            finally {
                n2 = n;
                ((FileInputStream)context).close();
                n2 = n;
            }
            return n2;
        }
        catch (final IOException ex) {
            return n2;
        }
    }
    
    private static int parseFileForValue(final String s, final FileInputStream fileInputStream) {
        final byte[] b = new byte[1024];
        try {
            int n;
            for (int read = fileInputStream.read(b), i = 0; i < read; i = n + 1) {
                final byte b2 = b[i];
                if (b2 == 10 || (n = i) == 0) {
                    int n2 = i;
                    if (b2 == 10) {
                        n2 = i + 1;
                    }
                    int n3 = n2;
                    while (true) {
                        n = n2;
                        if (n3 >= read) {
                            break;
                        }
                        final int index = n3 - n2;
                        if (b[n3] != s.charAt(index)) {
                            n = n2;
                            break;
                        }
                        if (index == s.length() - 1) {
                            return extractValue(b, n3);
                        }
                        ++n3;
                    }
                }
            }
            return -1;
        }
        catch (final IOException | NumberFormatException ex) {
            return -1;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation;

import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

final class zzc extends InterstitialAdLoadCallback
{
    final AbstractAdViewAdapter zza;
    final MediationInterstitialListener zzb;
    
    public zzc(final AbstractAdViewAdapter zza, final MediationInterstitialListener zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void onAdFailedToLoad(final LoadAdError loadAdError) {
        this.zzb.onAdFailedToLoad((MediationInterstitialAdapter)this.zza, (AdError)loadAdError);
    }
}

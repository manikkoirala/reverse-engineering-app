// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation;

import com.google.android.gms.ads.BaseAdView;
import android.app.Activity;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd$OnCustomClickListener;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd$OnCustomTemplateAdLoadedListener;
import com.google.android.gms.ads.formats.UnifiedNativeAd$OnUnifiedNativeAdLoadedListener;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.AdLoader$Builder;
import com.google.android.gms.ads.internal.client.zzdq;
import android.view.View;
import java.util.Iterator;
import java.util.Set;
import java.util.Date;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.ads.zzbzh;
import com.google.android.gms.ads.internal.client.zzay;
import com.google.android.gms.ads.AdRequest$Builder;
import com.google.android.gms.ads.AdRequest;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import android.content.Context;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.mediation.zza;
import com.google.android.gms.ads.mediation.OnImmersiveModeUpdatedListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;

public abstract class AbstractAdViewAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter, OnImmersiveModeUpdatedListener, zza
{
    public static final String AD_UNIT_ID_PARAMETER = "pubid";
    private AdLoader adLoader;
    protected AdView mAdView;
    protected InterstitialAd mInterstitialAd;
    
    AdRequest buildAdRequest(final Context context, final MediationAdRequest mediationAdRequest, final Bundle bundle, final Bundle bundle2) {
        final AdRequest$Builder adRequest$Builder = new AdRequest$Builder();
        final Date birthday = mediationAdRequest.getBirthday();
        if (birthday != null) {
            adRequest$Builder.zzb(birthday);
        }
        final int gender = mediationAdRequest.getGender();
        if (gender != 0) {
            adRequest$Builder.zzc(gender);
        }
        final Set keywords = mediationAdRequest.getKeywords();
        if (keywords != null) {
            final Iterator iterator = keywords.iterator();
            while (iterator.hasNext()) {
                adRequest$Builder.addKeyword((String)iterator.next());
            }
        }
        if (mediationAdRequest.isTesting()) {
            zzay.zzb();
            adRequest$Builder.zza(zzbzh.zzy(context));
        }
        if (mediationAdRequest.taggedForChildDirectedTreatment() != -1) {
            final int taggedForChildDirectedTreatment = mediationAdRequest.taggedForChildDirectedTreatment();
            boolean b = true;
            if (taggedForChildDirectedTreatment != 1) {
                b = false;
            }
            adRequest$Builder.zze(b);
        }
        adRequest$Builder.zzd(mediationAdRequest.isDesignedForFamilies());
        adRequest$Builder.addNetworkExtrasBundle((Class)AdMobAdapter.class, this.buildExtrasBundle(bundle, bundle2));
        return adRequest$Builder.build();
    }
    
    protected abstract Bundle buildExtrasBundle(final Bundle p0, final Bundle p1);
    
    public String getAdUnitId(final Bundle bundle) {
        return bundle.getString("pubid");
    }
    
    public View getBannerView() {
        return (View)this.mAdView;
    }
    
    InterstitialAd getInterstitialAd() {
        return this.mInterstitialAd;
    }
    
    public zzdq getVideoController() {
        final AdView mAdView = this.mAdView;
        if (mAdView != null) {
            return mAdView.zza().zza();
        }
        return null;
    }
    
    AdLoader$Builder newAdLoader(final Context context, final String s) {
        return new AdLoader$Builder(context, s);
    }
    
    public void onDestroy() {
        final AdView mAdView = this.mAdView;
        if (mAdView != null) {
            ((BaseAdView)mAdView).destroy();
            this.mAdView = null;
        }
        if (this.mInterstitialAd != null) {
            this.mInterstitialAd = null;
        }
        if (this.adLoader != null) {
            this.adLoader = null;
        }
    }
    
    public void onImmersiveModeUpdated(final boolean immersiveMode) {
        final InterstitialAd mInterstitialAd = this.mInterstitialAd;
        if (mInterstitialAd != null) {
            mInterstitialAd.setImmersiveMode(immersiveMode);
        }
    }
    
    public void onPause() {
        final AdView mAdView = this.mAdView;
        if (mAdView != null) {
            ((BaseAdView)mAdView).pause();
        }
    }
    
    public void onResume() {
        final AdView mAdView = this.mAdView;
        if (mAdView != null) {
            ((BaseAdView)mAdView).resume();
        }
    }
    
    public void requestBannerAd(final Context context, final MediationBannerListener mediationBannerListener, final Bundle bundle, final AdSize adSize, final MediationAdRequest mediationAdRequest, final Bundle bundle2) {
        ((BaseAdView)(this.mAdView = new AdView(context))).setAdSize(new AdSize(adSize.getWidth(), adSize.getHeight()));
        ((BaseAdView)this.mAdView).setAdUnitId(this.getAdUnitId(bundle));
        ((BaseAdView)this.mAdView).setAdListener((AdListener)new zzb(this, mediationBannerListener));
        ((BaseAdView)this.mAdView).loadAd(this.buildAdRequest(context, mediationAdRequest, bundle2, bundle));
    }
    
    public void requestInterstitialAd(final Context context, final MediationInterstitialListener mediationInterstitialListener, final Bundle bundle, final MediationAdRequest mediationAdRequest, final Bundle bundle2) {
        InterstitialAd.load(context, this.getAdUnitId(bundle), this.buildAdRequest(context, mediationAdRequest, bundle2, bundle), (InterstitialAdLoadCallback)new zzc(this, mediationInterstitialListener));
    }
    
    public void requestNativeAd(final Context context, final MediationNativeListener mediationNativeListener, final Bundle bundle, final NativeMediationAdRequest nativeMediationAdRequest, final Bundle bundle2) {
        final zze zze = new zze(this, mediationNativeListener);
        final AdLoader$Builder withAdListener = this.newAdLoader(context, bundle.getString("pubid")).withAdListener((AdListener)zze);
        withAdListener.withNativeAdOptions(nativeMediationAdRequest.getNativeAdOptions());
        withAdListener.withNativeAdOptions(nativeMediationAdRequest.getNativeAdRequestOptions());
        if (nativeMediationAdRequest.isUnifiedNativeAdRequested()) {
            withAdListener.forUnifiedNativeAd((UnifiedNativeAd$OnUnifiedNativeAdLoadedListener)zze);
        }
        if (nativeMediationAdRequest.zzb()) {
            for (final String s : nativeMediationAdRequest.zza().keySet()) {
                Object o;
                if (!(boolean)nativeMediationAdRequest.zza().get(s)) {
                    o = null;
                }
                else {
                    o = zze;
                }
                withAdListener.forCustomTemplateAd(s, (NativeCustomTemplateAd$OnCustomTemplateAdLoadedListener)zze, (NativeCustomTemplateAd$OnCustomClickListener)o);
            }
        }
        (this.adLoader = withAdListener.build()).loadAd(this.buildAdRequest(context, (MediationAdRequest)nativeMediationAdRequest, bundle2, bundle));
    }
    
    public void showInterstitial() {
        final InterstitialAd mInterstitialAd = this.mInterstitialAd;
        if (mInterstitialAd != null) {
            mInterstitialAd.show((Activity)null);
        }
    }
}

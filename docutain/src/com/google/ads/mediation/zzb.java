// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.admanager.AppEventListener;
import com.google.android.gms.ads.AdListener;

final class zzb extends AdListener implements AppEventListener, zza
{
    final AbstractAdViewAdapter zza;
    final MediationBannerListener zzb;
    
    public zzb(final AbstractAdViewAdapter zza, final MediationBannerListener zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void onAdClicked() {
        this.zzb.onAdClicked((MediationBannerAdapter)this.zza);
    }
    
    public final void onAdClosed() {
        this.zzb.onAdClosed((MediationBannerAdapter)this.zza);
    }
    
    public final void onAdFailedToLoad(final LoadAdError loadAdError) {
        this.zzb.onAdFailedToLoad((MediationBannerAdapter)this.zza, (AdError)loadAdError);
    }
    
    public final void onAdLoaded() {
        this.zzb.onAdLoaded((MediationBannerAdapter)this.zza);
    }
    
    public final void onAdOpened() {
        this.zzb.onAdOpened((MediationBannerAdapter)this.zza);
    }
    
    public final void onAppEvent(final String s, final String s2) {
        this.zzb.zzd((MediationBannerAdapter)this.zza, s, s2);
    }
}

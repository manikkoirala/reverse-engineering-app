// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation;

import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd$OnCustomClickListener;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd$OnCustomTemplateAdLoadedListener;
import com.google.android.gms.ads.formats.UnifiedNativeAd$OnUnifiedNativeAdLoadedListener;
import com.google.android.gms.ads.AdListener;

final class zze extends AdListener implements UnifiedNativeAd$OnUnifiedNativeAdLoadedListener, NativeCustomTemplateAd$OnCustomTemplateAdLoadedListener, NativeCustomTemplateAd$OnCustomClickListener
{
    final AbstractAdViewAdapter zza;
    final MediationNativeListener zzb;
    
    public zze(final AbstractAdViewAdapter zza, final MediationNativeListener zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void onAdClicked() {
        this.zzb.onAdClicked((MediationNativeAdapter)this.zza);
    }
    
    public final void onAdClosed() {
        this.zzb.onAdClosed((MediationNativeAdapter)this.zza);
    }
    
    public final void onAdFailedToLoad(final LoadAdError loadAdError) {
        this.zzb.onAdFailedToLoad((MediationNativeAdapter)this.zza, (AdError)loadAdError);
    }
    
    public final void onAdImpression() {
        this.zzb.onAdImpression((MediationNativeAdapter)this.zza);
    }
    
    public final void onAdLoaded() {
    }
    
    public final void onAdOpened() {
        this.zzb.onAdOpened((MediationNativeAdapter)this.zza);
    }
    
    public final void onCustomClick(final NativeCustomTemplateAd nativeCustomTemplateAd, final String s) {
        this.zzb.zze((MediationNativeAdapter)this.zza, nativeCustomTemplateAd, s);
    }
    
    public final void onCustomTemplateAdLoaded(final NativeCustomTemplateAd nativeCustomTemplateAd) {
        this.zzb.zzc((MediationNativeAdapter)this.zza, nativeCustomTemplateAd);
    }
    
    public final void onUnifiedNativeAdLoaded(final UnifiedNativeAd unifiedNativeAd) {
        this.zzb.onAdLoaded((MediationNativeAdapter)this.zza, (UnifiedNativeAdMapper)new zza(unifiedNativeAd));
    }
}

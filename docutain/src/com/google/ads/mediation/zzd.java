// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation;

import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.FullScreenContentCallback;

final class zzd extends FullScreenContentCallback
{
    final AbstractAdViewAdapter zza;
    final MediationInterstitialListener zzb;
    
    public zzd(final AbstractAdViewAdapter zza, final MediationInterstitialListener zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void onAdDismissedFullScreenContent() {
        this.zzb.onAdClosed((MediationInterstitialAdapter)this.zza);
    }
    
    public final void onAdShowedFullScreenContent() {
        this.zzb.onAdOpened((MediationInterstitialAdapter)this.zza);
    }
}

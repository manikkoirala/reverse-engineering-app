// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation;

import com.google.android.gms.ads.formats.zze;
import com.google.android.gms.ads.formats.zzg;
import java.util.Map;
import android.view.View;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;

final class zza extends UnifiedNativeAdMapper
{
    private final UnifiedNativeAd zza;
    
    public zza(final UnifiedNativeAd zza) {
        this.zza = zza;
        this.setHeadline(zza.getHeadline());
        this.setImages(zza.getImages());
        this.setBody(zza.getBody());
        this.setIcon(zza.getIcon());
        this.setCallToAction(zza.getCallToAction());
        this.setAdvertiser(zza.getAdvertiser());
        this.setStarRating(zza.getStarRating());
        this.setStore(zza.getStore());
        this.setPrice(zza.getPrice());
        this.zzd(zza.zza());
        this.setOverrideImpressionRecording(true);
        this.setOverrideClickHandling(true);
        this.zze(zza.getVideoController());
    }
    
    public final void trackViews(final View key, final Map<String, View> map, final Map<String, View> map2) {
        if (key instanceof zzg) {
            final zzg zzg = (zzg)key;
            throw null;
        }
        if (zze.zza.get(key) == null) {
            return;
        }
        throw null;
    }
}

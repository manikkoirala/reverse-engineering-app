// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads;

import android.content.Context;

@Deprecated
public final class AdSize
{
    public static final int AUTO_HEIGHT = -2;
    public static final AdSize BANNER;
    public static final int FULL_WIDTH = -1;
    public static final AdSize IAB_BANNER;
    public static final AdSize IAB_LEADERBOARD;
    public static final AdSize IAB_MRECT;
    public static final AdSize IAB_WIDE_SKYSCRAPER;
    public static final int LANDSCAPE_AD_HEIGHT = 32;
    public static final int LARGE_AD_HEIGHT = 90;
    public static final int PORTRAIT_AD_HEIGHT = 50;
    public static final AdSize SMART_BANNER;
    private final com.google.android.gms.ads.AdSize zza;
    
    static {
        SMART_BANNER = new AdSize(-1, -2);
        BANNER = new AdSize(320, 50);
        IAB_MRECT = new AdSize(300, 250);
        IAB_BANNER = new AdSize(468, 60);
        IAB_LEADERBOARD = new AdSize(728, 90);
        IAB_WIDE_SKYSCRAPER = new AdSize(160, 600);
    }
    
    public AdSize(final int n, final int n2) {
        this(new com.google.android.gms.ads.AdSize(n, n2));
    }
    
    public AdSize(final com.google.android.gms.ads.AdSize zza) {
        this.zza = zza;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof AdSize && this.zza.equals((Object)((AdSize)o).zza);
    }
    
    public AdSize findBestSize(final AdSize... array) {
        AdSize adSize = null;
        if (array == null) {
            return null;
        }
        final int width = this.getWidth();
        final int height = this.getHeight();
        int i = 0;
        float n = 0.0f;
        while (i < array.length) {
            final AdSize adSize2 = array[i];
            final int width2 = adSize2.getWidth();
            final int height2 = adSize2.getHeight();
            AdSize adSize3 = adSize;
            float n2 = n;
            if (this.isSizeAppropriate(width2, height2)) {
                float n4;
                final float n3 = n4 = width2 * height2 / (float)(width * height);
                if (n3 > 1.0f) {
                    n4 = 1.0f / n3;
                }
                adSize3 = adSize;
                n2 = n;
                if (n4 > n) {
                    adSize3 = adSize2;
                    n2 = n4;
                }
            }
            ++i;
            adSize = adSize3;
            n = n2;
        }
        return adSize;
    }
    
    public int getHeight() {
        return this.zza.getHeight();
    }
    
    public int getHeightInPixels(final Context context) {
        return this.zza.getHeightInPixels(context);
    }
    
    public int getWidth() {
        return this.zza.getWidth();
    }
    
    public int getWidthInPixels(final Context context) {
        return this.zza.getWidthInPixels(context);
    }
    
    @Override
    public int hashCode() {
        return this.zza.hashCode();
    }
    
    public boolean isAutoHeight() {
        return this.zza.isAutoHeight();
    }
    
    public boolean isCustomAdSize() {
        return false;
    }
    
    public boolean isFullWidth() {
        return this.zza.isFullWidth();
    }
    
    public boolean isSizeAppropriate(int height, final int n) {
        final float n2 = (float)this.getWidth();
        final float n3 = (float)height;
        height = this.getHeight();
        if (n3 <= n2 * 1.25f && n3 >= n2 * 0.8f) {
            final float n4 = (float)n;
            final float n5 = (float)height;
            if (n4 <= 1.25f * n5 && n4 >= n5 * 0.8f) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        return this.zza.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.a;

import android.os.RemoteException;
import android.os.Parcel;
import android.os.IBinder;
import android.os.IInterface;

public class a implements IInterface
{
    private final IBinder a;
    private final String b;
    
    protected a(final IBinder a) {
        this.a = a;
        this.b = "com.google.android.finsky.externalreferrer.IGetInstallReferrerService";
    }
    
    protected final Parcel a() {
        final Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.b);
        return obtain;
    }
    
    public final IBinder asBinder() {
        return this.a;
    }
    
    protected final Parcel b(final Parcel parcel) throws RemoteException {
        final Parcel obtain = Parcel.obtain();
        try {
            try {
                this.a.transact(1, parcel, obtain, 0);
                obtain.readException();
                parcel.recycle();
                return obtain;
            }
            finally {}
        }
        catch (final RuntimeException ex) {
            final Parcel parcel2;
            parcel2.recycle();
            throw ex;
        }
        parcel.recycle();
    }
}

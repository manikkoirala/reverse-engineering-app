// 
// Decompiled by Procyon v0.6.0
// 

package com.davemorrissey.labs.subscaleview;

public final class R
{
    public static final class attr
    {
        public static final int assetName = 2130968648;
        public static final int panEnabled = 2130969508;
        public static final int quickScaleEnabled = 2130969945;
        public static final int src = 2130970047;
        public static final int tileBackgroundColor = 2130970223;
        public static final int zoomEnabled = 2130970316;
    }
    
    public static final class styleable
    {
        public static final int[] SubsamplingScaleImageView;
        public static final int SubsamplingScaleImageView_assetName = 0;
        public static final int SubsamplingScaleImageView_panEnabled = 1;
        public static final int SubsamplingScaleImageView_quickScaleEnabled = 2;
        public static final int SubsamplingScaleImageView_src = 3;
        public static final int SubsamplingScaleImageView_tileBackgroundColor = 4;
        public static final int SubsamplingScaleImageView_zoomEnabled = 5;
        
        static {
            SubsamplingScaleImageView = new int[] { 2130968648, 2130969508, 2130969945, 2130970047, 2130970223, 2130970316 };
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.davemorrissey.labs.subscaleview.decoder;

import android.graphics.Point;
import android.net.Uri;
import android.content.Context;
import android.graphics.BitmapFactory$Options;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Build$VERSION;
import java.util.concurrent.locks.Lock;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReadWriteLock;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Bitmap$Config;

public class SkiaImageRegionDecoder implements ImageRegionDecoder
{
    private static final String ASSET_PREFIX = "file:///android_asset/";
    private static final String FILE_PREFIX = "file://";
    private static final String RESOURCE_PREFIX = "android.resource://";
    private final Bitmap$Config bitmapConfig;
    private BitmapRegionDecoder decoder;
    private final ReadWriteLock decoderLock;
    
    public SkiaImageRegionDecoder() {
        this(null);
    }
    
    public SkiaImageRegionDecoder(final Bitmap$Config bitmapConfig) {
        this.decoderLock = new ReentrantReadWriteLock(true);
        final Bitmap$Config preferredBitmapConfig = SubsamplingScaleImageView.getPreferredBitmapConfig();
        if (bitmapConfig != null) {
            this.bitmapConfig = bitmapConfig;
        }
        else if (preferredBitmapConfig != null) {
            this.bitmapConfig = preferredBitmapConfig;
        }
        else {
            this.bitmapConfig = Bitmap$Config.RGB_565;
        }
    }
    
    private Lock getDecodeLock() {
        if (Build$VERSION.SDK_INT < 21) {
            return this.decoderLock.writeLock();
        }
        return this.decoderLock.readLock();
    }
    
    @Override
    public Bitmap decodeRegion(final Rect rect, final int inSampleSize) {
        this.getDecodeLock().lock();
        try {
            final BitmapRegionDecoder decoder = this.decoder;
            if (decoder == null || decoder.isRecycled()) {
                throw new IllegalStateException("Cannot decode region after decoder has been recycled");
            }
            final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
            bitmapFactory$Options.inSampleSize = inSampleSize;
            bitmapFactory$Options.inPreferredConfig = this.bitmapConfig;
            final Bitmap decodeRegion = this.decoder.decodeRegion(rect, bitmapFactory$Options);
            if (decodeRegion != null) {
                return decodeRegion;
            }
            throw new RuntimeException("Skia image decoder returned null bitmap - image format may not be supported");
        }
        finally {
            this.getDecodeLock().unlock();
        }
    }
    
    @Override
    public Point init(final Context p0, final Uri p1) throws Exception {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   android/net/Uri.toString:()Ljava/lang/String;
        //     4: astore          4
        //     6: aload           4
        //     8: ldc             "android.resource://"
        //    10: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //    13: ifeq            171
        //    16: aload_2        
        //    17: invokevirtual   android/net/Uri.getAuthority:()Ljava/lang/String;
        //    20: astore          5
        //    22: aload_1        
        //    23: invokevirtual   android/content/Context.getPackageName:()Ljava/lang/String;
        //    26: aload           5
        //    28: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    31: ifeq            43
        //    34: aload_1        
        //    35: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //    38: astore          4
        //    40: goto            54
        //    43: aload_1        
        //    44: invokevirtual   android/content/Context.getPackageManager:()Landroid/content/pm/PackageManager;
        //    47: aload           5
        //    49: invokevirtual   android/content/pm/PackageManager.getResourcesForApplication:(Ljava/lang/String;)Landroid/content/res/Resources;
        //    52: astore          4
        //    54: aload_2        
        //    55: invokevirtual   android/net/Uri.getPathSegments:()Ljava/util/List;
        //    58: astore_2       
        //    59: aload_2        
        //    60: invokeinterface java/util/List.size:()I
        //    65: istore_3       
        //    66: iload_3        
        //    67: iconst_2       
        //    68: if_icmpne       112
        //    71: aload_2        
        //    72: iconst_0       
        //    73: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    78: checkcast       Ljava/lang/String;
        //    81: ldc             "drawable"
        //    83: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    86: ifeq            112
        //    89: aload           4
        //    91: aload_2        
        //    92: iconst_1       
        //    93: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    98: checkcast       Ljava/lang/String;
        //   101: ldc             "drawable"
        //   103: aload           5
        //   105: invokevirtual   android/content/res/Resources.getIdentifier:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //   108: istore_3       
        //   109: goto            152
        //   112: iload_3        
        //   113: iconst_1       
        //   114: if_icmpne       150
        //   117: aload_2        
        //   118: iconst_0       
        //   119: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   124: checkcast       Ljava/lang/CharSequence;
        //   127: invokestatic    android/text/TextUtils.isDigitsOnly:(Ljava/lang/CharSequence;)Z
        //   130: ifeq            150
        //   133: aload_2        
        //   134: iconst_0       
        //   135: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   140: checkcast       Ljava/lang/String;
        //   143: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   146: istore_3       
        //   147: goto            152
        //   150: iconst_0       
        //   151: istore_3       
        //   152: aload_0        
        //   153: aload_1        
        //   154: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //   157: iload_3        
        //   158: invokevirtual   android/content/res/Resources.openRawResource:(I)Ljava/io/InputStream;
        //   161: iconst_0       
        //   162: invokestatic    android/graphics/BitmapRegionDecoder.newInstance:(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
        //   165: putfield        com/davemorrissey/labs/subscaleview/decoder/SkiaImageRegionDecoder.decoder:Landroid/graphics/BitmapRegionDecoder;
        //   168: goto            269
        //   171: aload           4
        //   173: ldc             "file:///android_asset/"
        //   175: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   178: ifeq            209
        //   181: aload           4
        //   183: bipush          22
        //   185: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   188: astore_2       
        //   189: aload_0        
        //   190: aload_1        
        //   191: invokevirtual   android/content/Context.getAssets:()Landroid/content/res/AssetManager;
        //   194: aload_2        
        //   195: iconst_1       
        //   196: invokevirtual   android/content/res/AssetManager.open:(Ljava/lang/String;I)Ljava/io/InputStream;
        //   199: iconst_0       
        //   200: invokestatic    android/graphics/BitmapRegionDecoder.newInstance:(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
        //   203: putfield        com/davemorrissey/labs/subscaleview/decoder/SkiaImageRegionDecoder.decoder:Landroid/graphics/BitmapRegionDecoder;
        //   206: goto            269
        //   209: aload           4
        //   211: ldc             "file://"
        //   213: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   216: ifeq            237
        //   219: aload_0        
        //   220: aload           4
        //   222: bipush          7
        //   224: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   227: iconst_0       
        //   228: invokestatic    android/graphics/BitmapRegionDecoder.newInstance:(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;
        //   231: putfield        com/davemorrissey/labs/subscaleview/decoder/SkiaImageRegionDecoder.decoder:Landroid/graphics/BitmapRegionDecoder;
        //   234: goto            269
        //   237: aconst_null    
        //   238: astore          4
        //   240: aload_1        
        //   241: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
        //   244: aload_2        
        //   245: invokevirtual   android/content/ContentResolver.openInputStream:(Landroid/net/Uri;)Ljava/io/InputStream;
        //   248: astore_1       
        //   249: aload_1        
        //   250: astore          4
        //   252: aload_0        
        //   253: aload_1        
        //   254: iconst_0       
        //   255: invokestatic    android/graphics/BitmapRegionDecoder.newInstance:(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
        //   258: putfield        com/davemorrissey/labs/subscaleview/decoder/SkiaImageRegionDecoder.decoder:Landroid/graphics/BitmapRegionDecoder;
        //   261: aload_1        
        //   262: ifnull          269
        //   265: aload_1        
        //   266: invokevirtual   java/io/InputStream.close:()V
        //   269: new             Landroid/graphics/Point;
        //   272: dup            
        //   273: aload_0        
        //   274: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaImageRegionDecoder.decoder:Landroid/graphics/BitmapRegionDecoder;
        //   277: invokevirtual   android/graphics/BitmapRegionDecoder.getWidth:()I
        //   280: aload_0        
        //   281: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaImageRegionDecoder.decoder:Landroid/graphics/BitmapRegionDecoder;
        //   284: invokevirtual   android/graphics/BitmapRegionDecoder.getHeight:()I
        //   287: invokespecial   android/graphics/Point.<init>:(II)V
        //   290: areturn        
        //   291: astore_1       
        //   292: aload           4
        //   294: ifnull          302
        //   297: aload           4
        //   299: invokevirtual   java/io/InputStream.close:()V
        //   302: aload_1        
        //   303: athrow         
        //   304: astore_2       
        //   305: goto            150
        //   308: astore_1       
        //   309: goto            269
        //   312: astore_2       
        //   313: goto            302
        //    Exceptions:
        //  throws java.lang.Exception
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  133    147    304    308    Ljava/lang/NumberFormatException;
        //  240    249    291    304    Any
        //  252    261    291    304    Any
        //  265    269    308    312    Ljava/lang/Exception;
        //  297    302    312    316    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 148 out of bounds for length 148
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public boolean isReady() {
        synchronized (this) {
            final BitmapRegionDecoder decoder = this.decoder;
            return decoder != null && !decoder.isRecycled();
        }
    }
    
    @Override
    public void recycle() {
        synchronized (this) {
            this.decoderLock.writeLock().lock();
            try {
                this.decoder.recycle();
                this.decoder = null;
            }
            finally {
                this.decoderLock.writeLock().unlock();
            }
        }
    }
}

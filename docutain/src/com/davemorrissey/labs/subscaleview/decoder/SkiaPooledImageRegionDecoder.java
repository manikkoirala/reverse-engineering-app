// 
// Decompiled by Procyon v0.6.0
// 

package com.davemorrissey.labs.subscaleview.decoder;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.regex.Pattern;
import android.graphics.BitmapRegionDecoder;
import android.graphics.BitmapFactory$Options;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.app.ActivityManager$MemoryInfo;
import android.app.ActivityManager;
import android.os.Build$VERSION;
import java.io.FileFilter;
import java.io.File;
import android.util.Log;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import android.net.Uri;
import java.util.concurrent.atomic.AtomicBoolean;
import android.graphics.Point;
import java.util.concurrent.locks.ReadWriteLock;
import android.content.Context;
import android.graphics.Bitmap$Config;

public class SkiaPooledImageRegionDecoder implements ImageRegionDecoder
{
    private static final String ASSET_PREFIX = "file:///android_asset/";
    private static final String FILE_PREFIX = "file://";
    private static final String RESOURCE_PREFIX = "android.resource://";
    private static final String TAG = "SkiaPooledImageRegionDecoder";
    private static boolean debug = false;
    private final Bitmap$Config bitmapConfig;
    private Context context;
    private final ReadWriteLock decoderLock;
    private DecoderPool decoderPool;
    private long fileLength;
    private final Point imageDimensions;
    private final AtomicBoolean lazyInited;
    private Uri uri;
    
    public SkiaPooledImageRegionDecoder() {
        this(null);
    }
    
    public SkiaPooledImageRegionDecoder(final Bitmap$Config bitmapConfig) {
        this.decoderPool = new DecoderPool();
        this.decoderLock = new ReentrantReadWriteLock(true);
        this.fileLength = Long.MAX_VALUE;
        this.imageDimensions = new Point(0, 0);
        this.lazyInited = new AtomicBoolean(false);
        final Bitmap$Config preferredBitmapConfig = SubsamplingScaleImageView.getPreferredBitmapConfig();
        if (bitmapConfig != null) {
            this.bitmapConfig = bitmapConfig;
        }
        else if (preferredBitmapConfig != null) {
            this.bitmapConfig = preferredBitmapConfig;
        }
        else {
            this.bitmapConfig = Bitmap$Config.RGB_565;
        }
    }
    
    private void debug(final String s) {
        if (SkiaPooledImageRegionDecoder.debug) {
            Log.d(SkiaPooledImageRegionDecoder.TAG, s);
        }
    }
    
    private int getNumCoresOldPhones() {
        try {
            return new File("/sys/devices/system/cpu/").listFiles(new CpuFilter()).length;
        }
        catch (final Exception ex) {
            return 1;
        }
    }
    
    private int getNumberOfCores() {
        if (Build$VERSION.SDK_INT >= 17) {
            return Runtime.getRuntime().availableProcessors();
        }
        return this.getNumCoresOldPhones();
    }
    
    private void initialiseDecoder() throws Exception {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.uri:Landroid/net/Uri;
        //     4: invokevirtual   android/net/Uri.toString:()Ljava/lang/String;
        //     7: astore          8
        //     9: aload           8
        //    11: ldc             "android.resource://"
        //    13: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //    16: istore          6
        //    18: ldc2_w          9223372036854775807
        //    21: lstore_2       
        //    22: iload           6
        //    24: ifeq            223
        //    27: aload_0        
        //    28: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.uri:Landroid/net/Uri;
        //    31: invokevirtual   android/net/Uri.getAuthority:()Ljava/lang/String;
        //    34: astore          8
        //    36: aload_0        
        //    37: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.context:Landroid/content/Context;
        //    40: invokevirtual   android/content/Context.getPackageName:()Ljava/lang/String;
        //    43: aload           8
        //    45: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    48: ifeq            63
        //    51: aload_0        
        //    52: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.context:Landroid/content/Context;
        //    55: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //    58: astore          7
        //    60: goto            77
        //    63: aload_0        
        //    64: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.context:Landroid/content/Context;
        //    67: invokevirtual   android/content/Context.getPackageManager:()Landroid/content/pm/PackageManager;
        //    70: aload           8
        //    72: invokevirtual   android/content/pm/PackageManager.getResourcesForApplication:(Ljava/lang/String;)Landroid/content/res/Resources;
        //    75: astore          7
        //    77: aload_0        
        //    78: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.uri:Landroid/net/Uri;
        //    81: invokevirtual   android/net/Uri.getPathSegments:()Ljava/util/List;
        //    84: astore          9
        //    86: aload           9
        //    88: invokeinterface java/util/List.size:()I
        //    93: istore_1       
        //    94: iload_1        
        //    95: iconst_2       
        //    96: if_icmpne       142
        //    99: aload           9
        //   101: iconst_0       
        //   102: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   107: checkcast       Ljava/lang/String;
        //   110: ldc             "drawable"
        //   112: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   115: ifeq            142
        //   118: aload           7
        //   120: aload           9
        //   122: iconst_1       
        //   123: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   128: checkcast       Ljava/lang/String;
        //   131: ldc             "drawable"
        //   133: aload           8
        //   135: invokevirtual   android/content/res/Resources.getIdentifier:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //   138: istore_1       
        //   139: goto            184
        //   142: iload_1        
        //   143: iconst_1       
        //   144: if_icmpne       182
        //   147: aload           9
        //   149: iconst_0       
        //   150: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   155: checkcast       Ljava/lang/CharSequence;
        //   158: invokestatic    android/text/TextUtils.isDigitsOnly:(Ljava/lang/CharSequence;)Z
        //   161: ifeq            182
        //   164: aload           9
        //   166: iconst_0       
        //   167: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   172: checkcast       Ljava/lang/String;
        //   175: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   178: istore_1       
        //   179: goto            184
        //   182: iconst_0       
        //   183: istore_1       
        //   184: aload_0        
        //   185: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.context:Landroid/content/Context;
        //   188: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //   191: iload_1        
        //   192: invokevirtual   android/content/res/Resources.openRawResourceFd:(I)Landroid/content/res/AssetFileDescriptor;
        //   195: invokevirtual   android/content/res/AssetFileDescriptor.getLength:()J
        //   198: lstore          4
        //   200: lload           4
        //   202: lstore_2       
        //   203: aload_0        
        //   204: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.context:Landroid/content/Context;
        //   207: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //   210: iload_1        
        //   211: invokevirtual   android/content/res/Resources.openRawResource:(I)Ljava/io/InputStream;
        //   214: iconst_0       
        //   215: invokestatic    android/graphics/BitmapRegionDecoder.newInstance:(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
        //   218: astore          7
        //   220: goto            448
        //   223: aload           8
        //   225: ldc             "file:///android_asset/"
        //   227: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   230: ifeq            284
        //   233: aload           8
        //   235: bipush          22
        //   237: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   240: astore          7
        //   242: aload_0        
        //   243: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.context:Landroid/content/Context;
        //   246: invokevirtual   android/content/Context.getAssets:()Landroid/content/res/AssetManager;
        //   249: aload           7
        //   251: invokevirtual   android/content/res/AssetManager.openFd:(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
        //   254: invokevirtual   android/content/res/AssetFileDescriptor.getLength:()J
        //   257: lstore          4
        //   259: lload           4
        //   261: lstore_2       
        //   262: aload_0        
        //   263: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.context:Landroid/content/Context;
        //   266: invokevirtual   android/content/Context.getAssets:()Landroid/content/res/AssetManager;
        //   269: aload           7
        //   271: iconst_1       
        //   272: invokevirtual   android/content/res/AssetManager.open:(Ljava/lang/String;I)Ljava/io/InputStream;
        //   275: iconst_0       
        //   276: invokestatic    android/graphics/BitmapRegionDecoder.newInstance:(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
        //   279: astore          7
        //   281: goto            448
        //   284: aload           8
        //   286: ldc             "file://"
        //   288: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   291: ifeq            343
        //   294: aload           8
        //   296: bipush          7
        //   298: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   301: iconst_0       
        //   302: invokestatic    android/graphics/BitmapRegionDecoder.newInstance:(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;
        //   305: astore          7
        //   307: new             Ljava/io/File;
        //   310: astore          9
        //   312: aload           9
        //   314: aload           8
        //   316: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   319: lload_2        
        //   320: lstore          4
        //   322: aload           9
        //   324: invokevirtual   java/io/File.exists:()Z
        //   327: ifeq            337
        //   330: aload           9
        //   332: invokevirtual   java/io/File.length:()J
        //   335: lstore          4
        //   337: lload           4
        //   339: lstore_2       
        //   340: goto            448
        //   343: aconst_null    
        //   344: astore          8
        //   346: aload           8
        //   348: astore          7
        //   350: aload_0        
        //   351: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.context:Landroid/content/Context;
        //   354: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
        //   357: astore          10
        //   359: aload           8
        //   361: astore          7
        //   363: aload           10
        //   365: aload_0        
        //   366: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.uri:Landroid/net/Uri;
        //   369: invokevirtual   android/content/ContentResolver.openInputStream:(Landroid/net/Uri;)Ljava/io/InputStream;
        //   372: astore          9
        //   374: aload           9
        //   376: astore          7
        //   378: aload           9
        //   380: iconst_0       
        //   381: invokestatic    android/graphics/BitmapRegionDecoder.newInstance:(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
        //   384: astore          8
        //   386: aload           9
        //   388: astore          7
        //   390: aload           10
        //   392: aload_0        
        //   393: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.uri:Landroid/net/Uri;
        //   396: ldc_w           "r"
        //   399: invokevirtual   android/content/ContentResolver.openAssetFileDescriptor:(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
        //   402: astore          10
        //   404: lload_2        
        //   405: lstore          4
        //   407: aload           10
        //   409: ifnull          431
        //   412: aload           9
        //   414: astore          7
        //   416: aload           10
        //   418: invokevirtual   android/content/res/AssetFileDescriptor.getLength:()J
        //   421: lstore          4
        //   423: goto            431
        //   426: astore          7
        //   428: lload_2        
        //   429: lstore          4
        //   431: aload           9
        //   433: ifnull          441
        //   436: aload           9
        //   438: invokevirtual   java/io/InputStream.close:()V
        //   441: aload           8
        //   443: astore          7
        //   445: lload           4
        //   447: lstore_2       
        //   448: aload_0        
        //   449: lload_2        
        //   450: putfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.fileLength:J
        //   453: aload_0        
        //   454: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.imageDimensions:Landroid/graphics/Point;
        //   457: aload           7
        //   459: invokevirtual   android/graphics/BitmapRegionDecoder.getWidth:()I
        //   462: aload           7
        //   464: invokevirtual   android/graphics/BitmapRegionDecoder.getHeight:()I
        //   467: invokevirtual   android/graphics/Point.set:(II)V
        //   470: aload_0        
        //   471: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.decoderLock:Ljava/util/concurrent/locks/ReadWriteLock;
        //   474: invokeinterface java/util/concurrent/locks/ReadWriteLock.writeLock:()Ljava/util/concurrent/locks/Lock;
        //   479: invokeinterface java/util/concurrent/locks/Lock.lock:()V
        //   484: aload_0        
        //   485: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.decoderPool:Lcom/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder$DecoderPool;
        //   488: astore          8
        //   490: aload           8
        //   492: ifnull          502
        //   495: aload           8
        //   497: aload           7
        //   499: invokestatic    com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder$DecoderPool.access$600:(Lcom/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder$DecoderPool;Landroid/graphics/BitmapRegionDecoder;)V
        //   502: aload_0        
        //   503: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.decoderLock:Ljava/util/concurrent/locks/ReadWriteLock;
        //   506: invokeinterface java/util/concurrent/locks/ReadWriteLock.writeLock:()Ljava/util/concurrent/locks/Lock;
        //   511: invokeinterface java/util/concurrent/locks/Lock.unlock:()V
        //   516: return         
        //   517: astore          7
        //   519: aload_0        
        //   520: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaPooledImageRegionDecoder.decoderLock:Ljava/util/concurrent/locks/ReadWriteLock;
        //   523: invokeinterface java/util/concurrent/locks/ReadWriteLock.writeLock:()Ljava/util/concurrent/locks/Lock;
        //   528: invokeinterface java/util/concurrent/locks/Lock.unlock:()V
        //   533: aload           7
        //   535: athrow         
        //   536: astore          8
        //   538: aload           7
        //   540: ifnull          548
        //   543: aload           7
        //   545: invokevirtual   java/io/InputStream.close:()V
        //   548: aload           8
        //   550: athrow         
        //   551: astore          7
        //   553: goto            182
        //   556: astore          7
        //   558: goto            203
        //   561: astore          8
        //   563: goto            262
        //   566: astore          8
        //   568: lload_2        
        //   569: lstore          4
        //   571: goto            337
        //   574: astore          7
        //   576: goto            441
        //   579: astore          7
        //   581: goto            548
        //    Exceptions:
        //  throws java.lang.Exception
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  164    179    551    556    Ljava/lang/NumberFormatException;
        //  184    200    556    561    Ljava/lang/Exception;
        //  242    259    561    566    Ljava/lang/Exception;
        //  307    319    566    574    Ljava/lang/Exception;
        //  322    337    566    574    Ljava/lang/Exception;
        //  350    359    536    551    Any
        //  363    374    536    551    Any
        //  378    386    536    551    Any
        //  390    404    426    431    Ljava/lang/Exception;
        //  390    404    536    551    Any
        //  416    423    426    431    Ljava/lang/Exception;
        //  416    423    536    551    Any
        //  436    441    574    579    Ljava/lang/Exception;
        //  484    490    517    536    Any
        //  495    502    517    536    Any
        //  543    548    579    584    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 254 out of bounds for length 254
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private boolean isLowMemory() {
        final ActivityManager activityManager = (ActivityManager)this.context.getSystemService("activity");
        if (activityManager != null) {
            final ActivityManager$MemoryInfo activityManager$MemoryInfo = new ActivityManager$MemoryInfo();
            activityManager.getMemoryInfo(activityManager$MemoryInfo);
            return activityManager$MemoryInfo.lowMemory;
        }
        return true;
    }
    
    private void lazyInit() {
        if (this.lazyInited.compareAndSet(false, true) && this.fileLength < Long.MAX_VALUE) {
            this.debug("Starting lazy init of additional decoders");
            new Thread(this) {
                final SkiaPooledImageRegionDecoder this$0;
                
                @Override
                public void run() {
                    while (this.this$0.decoderPool != null) {
                        final SkiaPooledImageRegionDecoder this$0 = this.this$0;
                        if (!this$0.allowAdditionalDecoder(this$0.decoderPool.size(), this.this$0.fileLength)) {
                            break;
                        }
                        try {
                            if (this.this$0.decoderPool == null) {
                                continue;
                            }
                            final long currentTimeMillis = System.currentTimeMillis();
                            this.this$0.debug("Starting decoder");
                            this.this$0.initialiseDecoder();
                            final long currentTimeMillis2 = System.currentTimeMillis();
                            final SkiaPooledImageRegionDecoder this$2 = this.this$0;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Started decoder, took ");
                            sb.append(currentTimeMillis2 - currentTimeMillis);
                            sb.append("ms");
                            this$2.debug(sb.toString());
                        }
                        catch (final Exception ex) {
                            final SkiaPooledImageRegionDecoder this$3 = this.this$0;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Failed to start decoder: ");
                            sb2.append(ex.getMessage());
                            this$3.debug(sb2.toString());
                        }
                    }
                }
            }.start();
        }
    }
    
    public static void setDebug(final boolean debug) {
        SkiaPooledImageRegionDecoder.debug = debug;
    }
    
    protected boolean allowAdditionalDecoder(final int i, long n) {
        if (i >= 4) {
            this.debug("No additional decoders allowed, reached hard limit (4)");
            return false;
        }
        n *= i;
        if (n > 20971520L) {
            this.debug("No additional encoders allowed, reached hard memory limit (20Mb)");
            return false;
        }
        if (i >= this.getNumberOfCores()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No additional encoders allowed, limited by CPU cores (");
            sb.append(this.getNumberOfCores());
            sb.append(")");
            this.debug(sb.toString());
            return false;
        }
        if (this.isLowMemory()) {
            this.debug("No additional encoders allowed, memory is low");
            return false;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Additional decoder allowed, current count is ");
        sb2.append(i);
        sb2.append(", estimated native memory ");
        sb2.append(n / 1048576L);
        sb2.append("Mb");
        this.debug(sb2.toString());
        return true;
    }
    
    @Override
    public Bitmap decodeRegion(final Rect obj, final int inSampleSize) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Decode region ");
        sb.append(obj);
        sb.append(" on thread ");
        sb.append(Thread.currentThread().getName());
        this.debug(sb.toString());
        if (obj.width() < this.imageDimensions.x || obj.height() < this.imageDimensions.y) {
            this.lazyInit();
        }
        this.decoderLock.readLock().lock();
        try {
            final DecoderPool decoderPool = this.decoderPool;
            if (decoderPool != null) {
                final BitmapRegionDecoder access$700 = decoderPool.acquire();
                if (access$700 != null) {
                    try {
                        if (!access$700.isRecycled()) {
                            final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
                            bitmapFactory$Options.inSampleSize = inSampleSize;
                            bitmapFactory$Options.inPreferredConfig = this.bitmapConfig;
                            final Bitmap decodeRegion = access$700.decodeRegion(obj, bitmapFactory$Options);
                            if (decodeRegion != null) {
                                return decodeRegion;
                            }
                            throw new RuntimeException("Skia image decoder returned null bitmap - image format may not be supported");
                        }
                    }
                    finally {
                        if (access$700 != null) {
                            this.decoderPool.release(access$700);
                        }
                    }
                }
                if (access$700 != null) {
                    this.decoderPool.release(access$700);
                }
            }
            throw new IllegalStateException("Cannot decode region after decoder has been recycled");
        }
        finally {
            this.decoderLock.readLock().unlock();
        }
    }
    
    @Override
    public Point init(final Context context, final Uri uri) throws Exception {
        this.context = context;
        this.uri = uri;
        this.initialiseDecoder();
        return this.imageDimensions;
    }
    
    @Override
    public boolean isReady() {
        synchronized (this) {
            final DecoderPool decoderPool = this.decoderPool;
            return decoderPool != null && !decoderPool.isEmpty();
        }
    }
    
    @Override
    public void recycle() {
        synchronized (this) {
            this.decoderLock.writeLock().lock();
            try {
                final DecoderPool decoderPool = this.decoderPool;
                if (decoderPool != null) {
                    decoderPool.recycle();
                    this.decoderPool = null;
                    this.context = null;
                    this.uri = null;
                }
            }
            finally {
                this.decoderLock.writeLock().unlock();
            }
        }
    }
    
    class CpuFilter implements FileFilter
    {
        final SkiaPooledImageRegionDecoder this$0;
        
        CpuFilter(final SkiaPooledImageRegionDecoder this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean accept(final File file) {
            return Pattern.matches("cpu[0-9]+", file.getName());
        }
    }
    
    private static class DecoderPool
    {
        private final Semaphore available;
        private final Map<BitmapRegionDecoder, Boolean> decoders;
        
        private DecoderPool() {
            this.available = new Semaphore(0, true);
            this.decoders = new ConcurrentHashMap<BitmapRegionDecoder, Boolean>();
        }
        
        private BitmapRegionDecoder acquire() {
            this.available.acquireUninterruptibly();
            return this.getNextAvailable();
        }
        
        private void add(final BitmapRegionDecoder bitmapRegionDecoder) {
            synchronized (this) {
                this.decoders.put(bitmapRegionDecoder, false);
                this.available.release();
            }
        }
        
        private BitmapRegionDecoder getNextAvailable() {
            synchronized (this) {
                for (final Map.Entry<K, Boolean> entry : this.decoders.entrySet()) {
                    if (!entry.getValue()) {
                        entry.setValue(true);
                        return (BitmapRegionDecoder)entry.getKey();
                    }
                }
                return null;
            }
        }
        
        private boolean isEmpty() {
            synchronized (this) {
                return this.decoders.isEmpty();
            }
        }
        
        private boolean markAsUnused(final BitmapRegionDecoder bitmapRegionDecoder) {
            synchronized (this) {
                for (final Map.Entry entry : this.decoders.entrySet()) {
                    if (bitmapRegionDecoder == entry.getKey()) {
                        if (entry.getValue()) {
                            entry.setValue(false);
                            return true;
                        }
                        return false;
                    }
                }
                return false;
            }
        }
        
        private void recycle() {
            synchronized (this) {
                while (!this.decoders.isEmpty()) {
                    final BitmapRegionDecoder acquire = this.acquire();
                    acquire.recycle();
                    this.decoders.remove(acquire);
                }
            }
        }
        
        private void release(final BitmapRegionDecoder bitmapRegionDecoder) {
            if (this.markAsUnused(bitmapRegionDecoder)) {
                this.available.release();
            }
        }
        
        private int size() {
            synchronized (this) {
                return this.decoders.size();
            }
        }
    }
}

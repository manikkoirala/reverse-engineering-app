// 
// Decompiled by Procyon v0.6.0
// 

package com.davemorrissey.labs.subscaleview.decoder;

import android.graphics.Bitmap;
import android.net.Uri;
import android.content.Context;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import android.graphics.Bitmap$Config;

public class SkiaImageDecoder implements ImageDecoder
{
    private static final String ASSET_PREFIX = "file:///android_asset/";
    private static final String FILE_PREFIX = "file://";
    private static final String RESOURCE_PREFIX = "android.resource://";
    private final Bitmap$Config bitmapConfig;
    
    public SkiaImageDecoder() {
        this(null);
    }
    
    public SkiaImageDecoder(final Bitmap$Config bitmapConfig) {
        final Bitmap$Config preferredBitmapConfig = SubsamplingScaleImageView.getPreferredBitmapConfig();
        if (bitmapConfig != null) {
            this.bitmapConfig = bitmapConfig;
        }
        else if (preferredBitmapConfig != null) {
            this.bitmapConfig = preferredBitmapConfig;
        }
        else {
            this.bitmapConfig = Bitmap$Config.RGB_565;
        }
    }
    
    @Override
    public Bitmap decode(final Context p0, final Uri p1) throws Exception {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   android/net/Uri.toString:()Ljava/lang/String;
        //     4: astore          9
        //     6: new             Landroid/graphics/BitmapFactory$Options;
        //     9: dup            
        //    10: invokespecial   android/graphics/BitmapFactory$Options.<init>:()V
        //    13: astore          8
        //    15: aload           8
        //    17: aload_0        
        //    18: getfield        com/davemorrissey/labs/subscaleview/decoder/SkiaImageDecoder.bitmapConfig:Landroid/graphics/Bitmap$Config;
        //    21: putfield        android/graphics/BitmapFactory$Options.inPreferredConfig:Landroid/graphics/Bitmap$Config;
        //    24: aload           9
        //    26: ldc             "android.resource://"
        //    28: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //    31: ifeq            191
        //    34: aload_2        
        //    35: invokevirtual   android/net/Uri.getAuthority:()Ljava/lang/String;
        //    38: astore          9
        //    40: aload_1        
        //    41: invokevirtual   android/content/Context.getPackageName:()Ljava/lang/String;
        //    44: aload           9
        //    46: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    49: ifeq            61
        //    52: aload_1        
        //    53: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //    56: astore          7
        //    58: goto            72
        //    61: aload_1        
        //    62: invokevirtual   android/content/Context.getPackageManager:()Landroid/content/pm/PackageManager;
        //    65: aload           9
        //    67: invokevirtual   android/content/pm/PackageManager.getResourcesForApplication:(Ljava/lang/String;)Landroid/content/res/Resources;
        //    70: astore          7
        //    72: aload_2        
        //    73: invokevirtual   android/net/Uri.getPathSegments:()Ljava/util/List;
        //    76: astore_2       
        //    77: aload_2        
        //    78: invokeinterface java/util/List.size:()I
        //    83: istore          5
        //    85: iconst_0       
        //    86: istore          4
        //    88: iload           5
        //    90: iconst_2       
        //    91: if_icmpne       135
        //    94: aload_2        
        //    95: iconst_0       
        //    96: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   101: checkcast       Ljava/lang/String;
        //   104: ldc             "drawable"
        //   106: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   109: ifeq            135
        //   112: aload           7
        //   114: aload_2        
        //   115: iconst_1       
        //   116: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   121: checkcast       Ljava/lang/String;
        //   124: ldc             "drawable"
        //   126: aload           9
        //   128: invokevirtual   android/content/res/Resources.getIdentifier:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //   131: istore_3       
        //   132: goto            177
        //   135: iload           4
        //   137: istore_3       
        //   138: iload           5
        //   140: iconst_1       
        //   141: if_icmpne       177
        //   144: iload           4
        //   146: istore_3       
        //   147: aload_2        
        //   148: iconst_0       
        //   149: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   154: checkcast       Ljava/lang/CharSequence;
        //   157: invokestatic    android/text/TextUtils.isDigitsOnly:(Ljava/lang/CharSequence;)Z
        //   160: ifeq            177
        //   163: aload_2        
        //   164: iconst_0       
        //   165: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   170: checkcast       Ljava/lang/String;
        //   173: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   176: istore_3       
        //   177: aload_1        
        //   178: invokevirtual   android/content/Context.getResources:()Landroid/content/res/Resources;
        //   181: iload_3        
        //   182: aload           8
        //   184: invokestatic    android/graphics/BitmapFactory.decodeResource:(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //   187: astore_1       
        //   188: goto            289
        //   191: aload           9
        //   193: ldc             "file:///android_asset/"
        //   195: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   198: istore          6
        //   200: aconst_null    
        //   201: astore          7
        //   203: iload           6
        //   205: ifeq            234
        //   208: aload           9
        //   210: bipush          22
        //   212: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   215: astore_2       
        //   216: aload_1        
        //   217: invokevirtual   android/content/Context.getAssets:()Landroid/content/res/AssetManager;
        //   220: aload_2        
        //   221: invokevirtual   android/content/res/AssetManager.open:(Ljava/lang/String;)Ljava/io/InputStream;
        //   224: aconst_null    
        //   225: aload           8
        //   227: invokestatic    android/graphics/BitmapFactory.decodeStream:(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //   230: astore_1       
        //   231: goto            289
        //   234: aload           9
        //   236: ldc             "file://"
        //   238: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   241: ifeq            260
        //   244: aload           9
        //   246: bipush          7
        //   248: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   251: aload           8
        //   253: invokestatic    android/graphics/BitmapFactory.decodeFile:(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //   256: astore_1       
        //   257: goto            289
        //   260: aload_1        
        //   261: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
        //   264: aload_2        
        //   265: invokevirtual   android/content/ContentResolver.openInputStream:(Landroid/net/Uri;)Ljava/io/InputStream;
        //   268: astore_2       
        //   269: aload_2        
        //   270: aconst_null    
        //   271: aload           8
        //   273: invokestatic    android/graphics/BitmapFactory.decodeStream:(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //   276: astore_1       
        //   277: aload_2        
        //   278: ifnull          289
        //   281: aload_2        
        //   282: invokevirtual   java/io/InputStream.close:()V
        //   285: goto            289
        //   288: astore_2       
        //   289: aload_1        
        //   290: ifnull          295
        //   293: aload_1        
        //   294: areturn        
        //   295: new             Ljava/lang/RuntimeException;
        //   298: dup            
        //   299: ldc             "Skia image region decoder returned null bitmap - image format may not be supported"
        //   301: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;)V
        //   304: athrow         
        //   305: astore_1       
        //   306: goto            313
        //   309: astore_1       
        //   310: aload           7
        //   312: astore_2       
        //   313: aload_2        
        //   314: ifnull          321
        //   317: aload_2        
        //   318: invokevirtual   java/io/InputStream.close:()V
        //   321: aload_1        
        //   322: athrow         
        //   323: astore_2       
        //   324: iload           4
        //   326: istore_3       
        //   327: goto            177
        //   330: astore_2       
        //   331: goto            321
        //    Exceptions:
        //  throws java.lang.Exception
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  163    177    323    330    Ljava/lang/NumberFormatException;
        //  260    269    309    313    Any
        //  269    277    305    309    Any
        //  281    285    288    289    Ljava/lang/Exception;
        //  317    321    330    334    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0321:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

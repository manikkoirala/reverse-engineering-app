// 
// Decompiled by Procyon v0.6.0
// 

package com.davemorrissey.labs.subscaleview;

import java.lang.ref.WeakReference;
import android.view.View$MeasureSpec;
import java.util.Locale;
import android.view.GestureDetector$OnGestureListener;
import android.view.GestureDetector$SimpleOnGestureListener;
import android.view.ViewParent;
import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Iterator;
import android.graphics.Point;
import android.graphics.Canvas;
import android.database.Cursor;
import androidx.exifinterface.media.ExifInterface;
import android.util.Log;
import android.graphics.Paint$Style;
import android.util.DisplayMetrics;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.graphics.Color;
import android.os.Message;
import android.os.Handler$Callback;
import com.davemorrissey.labs.subscaleview.decoder.SkiaImageRegionDecoder;
import com.davemorrissey.labs.subscaleview.decoder.CompatDecoderFactory;
import com.davemorrissey.labs.subscaleview.decoder.SkiaImageDecoder;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.content.Context;
import java.util.Arrays;
import android.net.Uri;
import java.util.Map;
import android.graphics.RectF;
import android.graphics.PointF;
import android.graphics.Rect;
import android.view.View$OnLongClickListener;
import android.graphics.Matrix;
import android.os.Handler;
import java.util.concurrent.Executor;
import android.view.GestureDetector;
import java.util.concurrent.locks.ReadWriteLock;
import com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder;
import android.graphics.Paint;
import com.davemorrissey.labs.subscaleview.decoder.ImageDecoder;
import com.davemorrissey.labs.subscaleview.decoder.DecoderFactory;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import java.util.List;
import android.view.View;

public class SubsamplingScaleImageView extends View
{
    public static final int EASE_IN_OUT_QUAD = 2;
    public static final int EASE_OUT_QUAD = 1;
    private static final int MESSAGE_LONG_CLICK = 1;
    public static final int ORIENTATION_0 = 0;
    public static final int ORIENTATION_180 = 180;
    public static final int ORIENTATION_270 = 270;
    public static final int ORIENTATION_90 = 90;
    public static final int ORIENTATION_USE_EXIF = -1;
    public static final int ORIGIN_ANIM = 1;
    public static final int ORIGIN_DOUBLE_TAP_ZOOM = 4;
    public static final int ORIGIN_FLING = 3;
    public static final int ORIGIN_TOUCH = 2;
    public static final int PAN_LIMIT_CENTER = 3;
    public static final int PAN_LIMIT_INSIDE = 1;
    public static final int PAN_LIMIT_OUTSIDE = 2;
    public static final int SCALE_TYPE_CENTER_CROP = 2;
    public static final int SCALE_TYPE_CENTER_INSIDE = 1;
    public static final int SCALE_TYPE_CUSTOM = 3;
    public static final int SCALE_TYPE_START = 4;
    private static final String TAG = "SubsamplingScaleImageView";
    public static final int TILE_SIZE_AUTO = Integer.MAX_VALUE;
    private static final List<Integer> VALID_EASING_STYLES;
    private static final List<Integer> VALID_ORIENTATIONS;
    private static final List<Integer> VALID_PAN_LIMITS;
    private static final List<Integer> VALID_SCALE_TYPES;
    private static final List<Integer> VALID_ZOOM_STYLES;
    public static final int ZOOM_FOCUS_CENTER = 2;
    public static final int ZOOM_FOCUS_CENTER_IMMEDIATE = 3;
    public static final int ZOOM_FOCUS_FIXED = 1;
    private static Bitmap$Config preferredBitmapConfig;
    private Anim anim;
    private Bitmap bitmap;
    private DecoderFactory<? extends ImageDecoder> bitmapDecoderFactory;
    private boolean bitmapIsCached;
    private boolean bitmapIsPreview;
    private Paint bitmapPaint;
    private boolean debug;
    private Paint debugLinePaint;
    private Paint debugTextPaint;
    private ImageRegionDecoder decoder;
    private final ReadWriteLock decoderLock;
    private final float density;
    private GestureDetector detector;
    private int doubleTapZoomDuration;
    private float doubleTapZoomScale;
    private int doubleTapZoomStyle;
    private final float[] dstArray;
    private boolean eagerLoadingEnabled;
    private Executor executor;
    private int fullImageSampleSize;
    private final Handler handler;
    private boolean imageLoadedSent;
    private boolean isPanning;
    private boolean isQuickScaling;
    private boolean isZooming;
    private Matrix matrix;
    private float maxScale;
    private int maxTileHeight;
    private int maxTileWidth;
    private int maxTouchCount;
    private float minScale;
    private int minimumScaleType;
    private int minimumTileDpi;
    private OnImageEventListener onImageEventListener;
    private View$OnLongClickListener onLongClickListener;
    private OnStateChangedListener onStateChangedListener;
    private int orientation;
    private Rect pRegion;
    private boolean panEnabled;
    private int panLimit;
    private Float pendingScale;
    private boolean quickScaleEnabled;
    private float quickScaleLastDistance;
    private boolean quickScaleMoved;
    private PointF quickScaleSCenter;
    private final float quickScaleThreshold;
    private PointF quickScaleVLastPoint;
    private PointF quickScaleVStart;
    private boolean readySent;
    private DecoderFactory<? extends ImageRegionDecoder> regionDecoderFactory;
    private int sHeight;
    private int sOrientation;
    private PointF sPendingCenter;
    private RectF sRect;
    private Rect sRegion;
    private PointF sRequestedCenter;
    private int sWidth;
    private ScaleAndTranslate satTemp;
    private float scale;
    private float scaleStart;
    private GestureDetector singleDetector;
    private final float[] srcArray;
    private Paint tileBgPaint;
    private Map<Integer, List<Tile>> tileMap;
    private Uri uri;
    private PointF vCenterStart;
    private float vDistStart;
    private PointF vTranslate;
    private PointF vTranslateBefore;
    private PointF vTranslateStart;
    private boolean zoomEnabled;
    
    static {
        final Integer value = 1;
        final Integer value2 = 2;
        final Integer value3 = 3;
        VALID_ORIENTATIONS = Arrays.asList(0, 90, 180, 270, -1);
        VALID_ZOOM_STYLES = Arrays.asList(value, value2, value3);
        VALID_EASING_STYLES = Arrays.asList(value2, value);
        VALID_PAN_LIMITS = Arrays.asList(value, value2, value3);
        VALID_SCALE_TYPES = Arrays.asList(value2, value, value3, 4);
    }
    
    public SubsamplingScaleImageView(final Context context) {
        this(context, null);
    }
    
    public SubsamplingScaleImageView(final Context gestureDetector, final AttributeSet set) {
        super(gestureDetector, set);
        this.orientation = 0;
        this.maxScale = 2.0f;
        this.minScale = this.minScale();
        this.minimumTileDpi = -1;
        this.panLimit = 1;
        this.minimumScaleType = 1;
        this.maxTileWidth = Integer.MAX_VALUE;
        this.maxTileHeight = Integer.MAX_VALUE;
        this.executor = AsyncTask.THREAD_POOL_EXECUTOR;
        this.eagerLoadingEnabled = true;
        this.panEnabled = true;
        this.zoomEnabled = true;
        this.quickScaleEnabled = true;
        this.doubleTapZoomScale = 1.0f;
        this.doubleTapZoomStyle = 1;
        this.doubleTapZoomDuration = 500;
        this.decoderLock = new ReentrantReadWriteLock(true);
        this.bitmapDecoderFactory = new CompatDecoderFactory<ImageDecoder>(SkiaImageDecoder.class);
        this.regionDecoderFactory = new CompatDecoderFactory<ImageRegionDecoder>(SkiaImageRegionDecoder.class);
        this.srcArray = new float[8];
        this.dstArray = new float[8];
        this.density = this.getResources().getDisplayMetrics().density;
        this.setMinimumDpi(160);
        this.setDoubleTapZoomDpi(160);
        this.setMinimumTileDpi(320);
        this.setGestureDetector(gestureDetector);
        this.handler = new Handler((Handler$Callback)new Handler$Callback(this) {
            final SubsamplingScaleImageView this$0;
            
            public boolean handleMessage(final Message message) {
                if (message.what == 1 && this.this$0.onLongClickListener != null) {
                    this.this$0.maxTouchCount = 0;
                    final SubsamplingScaleImageView this$0 = this.this$0;
                    SubsamplingScaleImageView.access$201(this$0, this$0.onLongClickListener);
                    this.this$0.performLongClick();
                    SubsamplingScaleImageView.access$301(this.this$0, null);
                }
                return true;
            }
        });
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.SubsamplingScaleImageView);
            if (obtainStyledAttributes.hasValue(R.styleable.SubsamplingScaleImageView_assetName)) {
                final String string = obtainStyledAttributes.getString(R.styleable.SubsamplingScaleImageView_assetName);
                if (string != null && string.length() > 0) {
                    this.setImage(ImageSource.asset(string).tilingEnabled());
                }
            }
            if (obtainStyledAttributes.hasValue(R.styleable.SubsamplingScaleImageView_src)) {
                final int resourceId = obtainStyledAttributes.getResourceId(R.styleable.SubsamplingScaleImageView_src, 0);
                if (resourceId > 0) {
                    this.setImage(ImageSource.resource(resourceId).tilingEnabled());
                }
            }
            if (obtainStyledAttributes.hasValue(R.styleable.SubsamplingScaleImageView_panEnabled)) {
                this.setPanEnabled(obtainStyledAttributes.getBoolean(R.styleable.SubsamplingScaleImageView_panEnabled, true));
            }
            if (obtainStyledAttributes.hasValue(R.styleable.SubsamplingScaleImageView_zoomEnabled)) {
                this.setZoomEnabled(obtainStyledAttributes.getBoolean(R.styleable.SubsamplingScaleImageView_zoomEnabled, true));
            }
            if (obtainStyledAttributes.hasValue(R.styleable.SubsamplingScaleImageView_quickScaleEnabled)) {
                this.setQuickScaleEnabled(obtainStyledAttributes.getBoolean(R.styleable.SubsamplingScaleImageView_quickScaleEnabled, true));
            }
            if (obtainStyledAttributes.hasValue(R.styleable.SubsamplingScaleImageView_tileBackgroundColor)) {
                this.setTileBackgroundColor(obtainStyledAttributes.getColor(R.styleable.SubsamplingScaleImageView_tileBackgroundColor, Color.argb(0, 0, 0, 0)));
            }
            obtainStyledAttributes.recycle();
        }
        this.quickScaleThreshold = TypedValue.applyDimension(1, 20.0f, gestureDetector.getResources().getDisplayMetrics());
    }
    
    static /* synthetic */ void access$201(final SubsamplingScaleImageView subsamplingScaleImageView, final View$OnLongClickListener onLongClickListener) {
        subsamplingScaleImageView.setOnLongClickListener(onLongClickListener);
    }
    
    static /* synthetic */ void access$301(final SubsamplingScaleImageView subsamplingScaleImageView, final View$OnLongClickListener onLongClickListener) {
        subsamplingScaleImageView.setOnLongClickListener(onLongClickListener);
    }
    
    private int calculateInSampleSize(final float n) {
        float n2 = n;
        if (this.minimumTileDpi > 0) {
            final DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
            n2 = n * (this.minimumTileDpi / ((displayMetrics.xdpi + displayMetrics.ydpi) / 2.0f));
        }
        final int n3 = (int)(this.sWidth() * n2);
        final int n4 = (int)(this.sHeight() * n2);
        if (n3 != 0 && n4 != 0) {
            final int sHeight = this.sHeight();
            int n5 = 1;
            int round;
            if (sHeight <= n4 && this.sWidth() <= n3) {
                round = 1;
            }
            else {
                round = Math.round(this.sHeight() / (float)n4);
                final int round2 = Math.round(this.sWidth() / (float)n3);
                if (round >= round2) {
                    round = round2;
                }
            }
            while (true) {
                final int n6 = n5 * 2;
                if (n6 >= round) {
                    break;
                }
                n5 = n6;
            }
            return n5;
        }
        return 32;
    }
    
    private boolean checkImageLoaded() {
        final boolean baseLayerReady = this.isBaseLayerReady();
        if (!this.imageLoadedSent && baseLayerReady) {
            this.preDraw();
            this.imageLoadedSent = true;
            this.onImageLoaded();
            final OnImageEventListener onImageEventListener = this.onImageEventListener;
            if (onImageEventListener != null) {
                onImageEventListener.onImageLoaded();
            }
        }
        return baseLayerReady;
    }
    
    private boolean checkReady() {
        final boolean b = this.getWidth() > 0 && this.getHeight() > 0 && this.sWidth > 0 && this.sHeight > 0 && (this.bitmap != null || this.isBaseLayerReady());
        if (!this.readySent && b) {
            this.preDraw();
            this.readySent = true;
            this.onReady();
            final OnImageEventListener onImageEventListener = this.onImageEventListener;
            if (onImageEventListener != null) {
                onImageEventListener.onReady();
            }
        }
        return b;
    }
    
    private void createPaints() {
        if (this.bitmapPaint == null) {
            (this.bitmapPaint = new Paint()).setAntiAlias(true);
            this.bitmapPaint.setFilterBitmap(true);
            this.bitmapPaint.setDither(true);
        }
        if ((this.debugTextPaint == null || this.debugLinePaint == null) && this.debug) {
            (this.debugTextPaint = new Paint()).setTextSize((float)this.px(12));
            this.debugTextPaint.setColor(-65281);
            this.debugTextPaint.setStyle(Paint$Style.FILL);
            (this.debugLinePaint = new Paint()).setColor(-65281);
            this.debugLinePaint.setStyle(Paint$Style.STROKE);
            this.debugLinePaint.setStrokeWidth((float)this.px(1));
        }
    }
    
    private void debug(final String format, final Object... args) {
        if (this.debug) {
            Log.d(SubsamplingScaleImageView.TAG, String.format(format, args));
        }
    }
    
    private float distance(float n, float n2, final float n3, final float n4) {
        n -= n2;
        n2 = n3 - n4;
        return (float)Math.sqrt(n * n + n2 * n2);
    }
    
    private void doubleTapZoom(final PointF pointF, final PointF pointF2) {
        if (!this.panEnabled) {
            final PointF sRequestedCenter = this.sRequestedCenter;
            if (sRequestedCenter != null) {
                pointF.x = sRequestedCenter.x;
                pointF.y = this.sRequestedCenter.y;
            }
            else {
                pointF.x = (float)(this.sWidth() / 2);
                pointF.y = (float)(this.sHeight() / 2);
            }
        }
        float n = Math.min(this.maxScale, this.doubleTapZoomScale);
        final float scale = this.scale;
        final boolean b = scale <= n * 0.9 || scale == this.minScale;
        if (!b) {
            n = this.minScale();
        }
        final int doubleTapZoomStyle = this.doubleTapZoomStyle;
        if (doubleTapZoomStyle == 3) {
            this.setScaleAndCenter(n, pointF);
        }
        else if (doubleTapZoomStyle != 2 && b && this.panEnabled) {
            if (doubleTapZoomStyle == 1) {
                new AnimationBuilder(n, pointF, pointF2).withInterruptible(false).withDuration(this.doubleTapZoomDuration).withOrigin(4).start();
            }
        }
        else {
            new AnimationBuilder(n, pointF).withInterruptible(false).withDuration(this.doubleTapZoomDuration).withOrigin(4).start();
        }
        this.invalidate();
    }
    
    private float ease(final int i, final long n, final float n2, final float n3, final long n4) {
        if (i == 1) {
            return this.easeOutQuad(n, n2, n3, n4);
        }
        if (i == 2) {
            return this.easeInOutQuad(n, n2, n3, n4);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected easing type: ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }
    
    private float easeInOutQuad(final long n, final float n2, float n3, final long n4) {
        float n5 = n / (n4 / 2.0f);
        if (n5 < 1.0f) {
            n3 = n3 / 2.0f * n5;
        }
        else {
            final float n6 = n5 - 1.0f;
            n3 = -n3 / 2.0f;
            n5 = n6 * (n6 - 2.0f) - 1.0f;
        }
        return n3 * n5 + n2;
    }
    
    private float easeOutQuad(final long n, final float n2, final float n3, final long n4) {
        final float n5 = n / (float)n4;
        return -n3 * n5 * (n5 - 2.0f) + n2;
    }
    
    private void execute(final AsyncTask<Void, Void, ?> asyncTask) {
        asyncTask.executeOnExecutor(this.executor, (Object[])new Void[0]);
    }
    
    private void fileSRect(final Rect rect, final Rect rect2) {
        if (this.getRequiredRotation() == 0) {
            rect2.set(rect);
        }
        else if (this.getRequiredRotation() == 90) {
            rect2.set(rect.top, this.sHeight - rect.right, rect.bottom, this.sHeight - rect.left);
        }
        else if (this.getRequiredRotation() == 180) {
            rect2.set(this.sWidth - rect.right, this.sHeight - rect.bottom, this.sWidth - rect.left, this.sHeight - rect.top);
        }
        else {
            rect2.set(this.sWidth - rect.bottom, rect.left, this.sWidth - rect.top, rect.right);
        }
    }
    
    private void fitToBounds(final boolean b) {
        boolean b2;
        if (this.vTranslate == null) {
            b2 = true;
            this.vTranslate = new PointF(0.0f, 0.0f);
        }
        else {
            b2 = false;
        }
        if (this.satTemp == null) {
            this.satTemp = new ScaleAndTranslate(0.0f, new PointF(0.0f, 0.0f));
        }
        this.satTemp.scale = this.scale;
        this.satTemp.vTranslate.set(this.vTranslate);
        this.fitToBounds(b, this.satTemp);
        this.scale = this.satTemp.scale;
        this.vTranslate.set(this.satTemp.vTranslate);
        if (b2 && this.minimumScaleType != 4) {
            this.vTranslate.set(this.vTranslateForSCenter((float)(this.sWidth() / 2), (float)(this.sHeight() / 2), this.scale));
        }
    }
    
    private void fitToBounds(final boolean b, final ScaleAndTranslate scaleAndTranslate) {
        boolean b2 = b;
        if (this.panLimit == 2) {
            b2 = b;
            if (this.isReady()) {
                b2 = false;
            }
        }
        final PointF access$4800 = scaleAndTranslate.vTranslate;
        final float limitedScale = this.limitedScale(scaleAndTranslate.scale);
        final float n = this.sWidth() * limitedScale;
        final float n2 = this.sHeight() * limitedScale;
        if (this.panLimit == 3 && this.isReady()) {
            access$4800.x = Math.max(access$4800.x, this.getWidth() / 2 - n);
            access$4800.y = Math.max(access$4800.y, this.getHeight() / 2 - n2);
        }
        else if (b2) {
            access$4800.x = Math.max(access$4800.x, this.getWidth() - n);
            access$4800.y = Math.max(access$4800.y, this.getHeight() - n2);
        }
        else {
            access$4800.x = Math.max(access$4800.x, -n);
            access$4800.y = Math.max(access$4800.y, -n2);
        }
        final int paddingLeft = this.getPaddingLeft();
        float n3 = 0.5f;
        float n4;
        if (paddingLeft <= 0 && this.getPaddingRight() <= 0) {
            n4 = 0.5f;
        }
        else {
            n4 = this.getPaddingLeft() / (float)(this.getPaddingLeft() + this.getPaddingRight());
        }
        if (this.getPaddingTop() > 0 || this.getPaddingBottom() > 0) {
            n3 = this.getPaddingTop() / (float)(this.getPaddingTop() + this.getPaddingBottom());
        }
        float max2 = 0.0f;
        float b3 = 0.0f;
        Label_0405: {
            float n5;
            int n6;
            if (this.panLimit == 3 && this.isReady()) {
                n5 = (float)Math.max(0, this.getWidth() / 2);
                n6 = Math.max(0, this.getHeight() / 2);
            }
            else {
                if (b2) {
                    final float max = Math.max(0.0f, (this.getWidth() - n) * n4);
                    max2 = Math.max(0.0f, (this.getHeight() - n2) * n3);
                    b3 = max;
                    break Label_0405;
                }
                n5 = (float)Math.max(0, this.getWidth());
                n6 = Math.max(0, this.getHeight());
            }
            final float n7 = (float)n6;
            b3 = n5;
            max2 = n7;
        }
        access$4800.x = Math.min(access$4800.x, b3);
        access$4800.y = Math.min(access$4800.y, max2);
        scaleAndTranslate.scale = limitedScale;
    }
    
    private int getExifOrientation(final Context context, final String s) {
        final boolean startsWith = s.startsWith("content");
        final int n = 0;
        final int n2 = 0;
        int n3 = 0;
        if (startsWith) {
            Cursor cursor = null;
            Cursor cursor2 = null;
            try {
                try {
                    final Cursor query = context.getContentResolver().query(Uri.parse(s), new String[] { "orientation" }, (String)null, (String[])null, (String)null);
                    int int1 = n3;
                    if (query != null) {
                        int1 = n3;
                        cursor2 = query;
                        cursor = query;
                        if (query.moveToFirst()) {
                            cursor2 = query;
                            cursor = query;
                            int1 = query.getInt(0);
                            cursor2 = query;
                            cursor = query;
                            if (!SubsamplingScaleImageView.VALID_ORIENTATIONS.contains(int1) || int1 == -1) {
                                cursor2 = query;
                                cursor = query;
                                final String tag = SubsamplingScaleImageView.TAG;
                                cursor2 = query;
                                cursor = query;
                                cursor2 = query;
                                cursor = query;
                                final StringBuilder sb = new StringBuilder();
                                cursor2 = query;
                                cursor = query;
                                sb.append("Unsupported orientation: ");
                                cursor2 = query;
                                cursor = query;
                                sb.append(int1);
                                cursor2 = query;
                                cursor = query;
                                Log.w(tag, sb.toString());
                                int1 = n3;
                            }
                        }
                    }
                    n3 = int1;
                    if (query != null) {
                        n3 = int1;
                        query.close();
                        return n3;
                    }
                    return n3;
                }
                finally {
                    if (cursor2 != null) {
                        cursor2.close();
                    }
                    final Cursor query = cursor;
                    n3 = n;
                }
            }
            catch (final Exception ex) {}
        }
        n3 = n2;
        if (s.startsWith("file:///")) {
            n3 = n2;
            if (!s.startsWith("file:///android_asset/")) {
                try {
                    final int attributeInt = new ExifInterface(s.substring(7)).getAttributeInt("Orientation", 1);
                    n3 = n2;
                    if (attributeInt != 1) {
                        if (attributeInt == 0) {
                            n3 = n2;
                        }
                        else if (attributeInt == 6) {
                            n3 = 90;
                        }
                        else if (attributeInt == 3) {
                            n3 = 180;
                        }
                        else if (attributeInt == 8) {
                            n3 = 270;
                        }
                        else {
                            final String tag2 = SubsamplingScaleImageView.TAG;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Unsupported EXIF orientation: ");
                            sb2.append(attributeInt);
                            Log.w(tag2, sb2.toString());
                            n3 = n2;
                        }
                    }
                }
                catch (final Exception ex2) {
                    Log.w(SubsamplingScaleImageView.TAG, "Could not get EXIF orientation of image");
                    n3 = n2;
                }
            }
        }
        return n3;
    }
    
    private Point getMaxBitmapDimensions(final Canvas canvas) {
        return new Point(Math.min(canvas.getMaximumBitmapWidth(), this.maxTileWidth), Math.min(canvas.getMaximumBitmapHeight(), this.maxTileHeight));
    }
    
    public static Bitmap$Config getPreferredBitmapConfig() {
        return SubsamplingScaleImageView.preferredBitmapConfig;
    }
    
    private int getRequiredRotation() {
        int n;
        if ((n = this.orientation) == -1) {
            n = this.sOrientation;
        }
        return n;
    }
    
    private void initialiseBaseLayer(final Point point) {
        synchronized (this) {
            this.debug("initialiseBaseLayer maxTileDimensions=%dx%d", point.x, point.y);
            this.fitToBounds(true, this.satTemp = new ScaleAndTranslate(0.0f, new PointF(0.0f, 0.0f)));
            final int calculateInSampleSize = this.calculateInSampleSize(this.satTemp.scale);
            this.fullImageSampleSize = calculateInSampleSize;
            if (calculateInSampleSize > 1) {
                this.fullImageSampleSize = calculateInSampleSize / 2;
            }
            if (this.fullImageSampleSize == 1 && this.sRegion == null && this.sWidth() < point.x && this.sHeight() < point.y) {
                this.decoder.recycle();
                this.decoder = null;
                this.execute(new BitmapLoadTask(this, this.getContext(), this.bitmapDecoderFactory, this.uri, false));
            }
            else {
                this.initialiseTileMap(point);
                final Iterator iterator = this.tileMap.get(this.fullImageSampleSize).iterator();
                while (iterator.hasNext()) {
                    this.execute(new TileLoadTask(this, this.decoder, (Tile)iterator.next()));
                }
                this.refreshRequiredTiles(true);
            }
        }
    }
    
    private void initialiseTileMap(final Point point) {
        this.debug("initialiseTileMap maxTileDimensions=%dx%d", point.x, point.y);
        this.tileMap = new LinkedHashMap<Integer, List<Tile>>();
        int fullImageSampleSize = this.fullImageSampleSize;
        int n = 1;
        int n2 = 1;
        while (true) {
            int n3 = this.sWidth() / n;
            final int n4 = this.sHeight() / n2;
            int n5 = n3 / fullImageSampleSize;
            final int n6 = n4 / fullImageSampleSize;
            int n7;
            int n8;
            int n9;
            while (true) {
                if (n5 + n + 1 <= point.x) {
                    n7 = n2;
                    n8 = n4;
                    n9 = n6;
                    if (n5 <= this.getWidth() * 1.25) {
                        break;
                    }
                    n7 = n2;
                    n8 = n4;
                    n9 = n6;
                    if (fullImageSampleSize >= this.fullImageSampleSize) {
                        break;
                    }
                }
                ++n;
                n3 = this.sWidth() / n;
                n5 = n3 / fullImageSampleSize;
            }
            while (n9 + n7 + 1 > point.y || (n9 > this.getHeight() * 1.25 && fullImageSampleSize < this.fullImageSampleSize)) {
                ++n7;
                n8 = this.sHeight() / n7;
                n9 = n8 / fullImageSampleSize;
            }
            final ArrayList list = new ArrayList(n * n7);
            for (int i = 0; i < n; ++i) {
                for (int j = 0; j < n7; ++j) {
                    final Tile tile = new Tile();
                    tile.sampleSize = fullImageSampleSize;
                    tile.visible = (fullImageSampleSize == this.fullImageSampleSize);
                    int sWidth;
                    if (i == n - 1) {
                        sWidth = this.sWidth();
                    }
                    else {
                        sWidth = (i + 1) * n3;
                    }
                    int sHeight;
                    if (j == n7 - 1) {
                        sHeight = this.sHeight();
                    }
                    else {
                        sHeight = (j + 1) * n8;
                    }
                    tile.sRect = new Rect(i * n3, j * n8, sWidth, sHeight);
                    tile.vRect = new Rect(0, 0, 0, 0);
                    tile.fileSRect = new Rect(tile.sRect);
                    list.add((Object)tile);
                }
            }
            this.tileMap.put(fullImageSampleSize, (ArrayList)list);
            if (fullImageSampleSize == 1) {
                break;
            }
            fullImageSampleSize /= 2;
            n2 = n7;
        }
    }
    
    private boolean isBaseLayerReady() {
        final Bitmap bitmap = this.bitmap;
        boolean b = true;
        if (bitmap != null && !this.bitmapIsPreview) {
            return true;
        }
        final Map<Integer, List<Tile>> tileMap = this.tileMap;
        if (tileMap != null) {
            for (final Map.Entry<Integer, V> entry : tileMap.entrySet()) {
                if (entry.getKey() == this.fullImageSampleSize) {
                    final Iterator iterator2 = ((List)entry.getValue()).iterator();
                    boolean b2 = b;
                    while (true) {
                        b = b2;
                        if (!iterator2.hasNext()) {
                            break;
                        }
                        final Tile tile = (Tile)iterator2.next();
                        if (!tile.loading && tile.bitmap != null) {
                            continue;
                        }
                        b2 = false;
                    }
                }
            }
            return b;
        }
        return false;
    }
    
    private PointF limitedSCenter(final float n, final float n2, final float n3, final PointF pointF) {
        final PointF vTranslateForSCenter = this.vTranslateForSCenter(n, n2, n3);
        pointF.set((this.getPaddingLeft() + (this.getWidth() - this.getPaddingRight() - this.getPaddingLeft()) / 2 - vTranslateForSCenter.x) / n3, (this.getPaddingTop() + (this.getHeight() - this.getPaddingBottom() - this.getPaddingTop()) / 2 - vTranslateForSCenter.y) / n3);
        return pointF;
    }
    
    private float limitedScale(float max) {
        max = Math.max(this.minScale(), max);
        return Math.min(this.maxScale, max);
    }
    
    private float minScale() {
        final int n = this.getPaddingBottom() + this.getPaddingTop();
        final int n2 = this.getPaddingLeft() + this.getPaddingRight();
        final int minimumScaleType = this.minimumScaleType;
        if (minimumScaleType != 2 && minimumScaleType != 4) {
            if (minimumScaleType == 3) {
                final float minScale = this.minScale;
                if (minScale > 0.0f) {
                    return minScale;
                }
            }
            return Math.min((this.getWidth() - n2) / (float)this.sWidth(), (this.getHeight() - n) / (float)this.sHeight());
        }
        return Math.max((this.getWidth() - n2) / (float)this.sWidth(), (this.getHeight() - n) / (float)this.sHeight());
    }
    
    private void onImageLoaded(final Bitmap bitmap, final int sOrientation, final boolean bitmapIsCached) {
        synchronized (this) {
            this.debug("onImageLoaded", new Object[0]);
            final int sWidth = this.sWidth;
            if (sWidth > 0 && this.sHeight > 0 && (sWidth != bitmap.getWidth() || this.sHeight != bitmap.getHeight())) {
                this.reset(false);
            }
            final Bitmap bitmap2 = this.bitmap;
            if (bitmap2 != null && !this.bitmapIsCached) {
                bitmap2.recycle();
            }
            if (this.bitmap != null && this.bitmapIsCached) {
                final OnImageEventListener onImageEventListener = this.onImageEventListener;
                if (onImageEventListener != null) {
                    onImageEventListener.onPreviewReleased();
                }
            }
            this.bitmapIsPreview = false;
            this.bitmapIsCached = bitmapIsCached;
            this.bitmap = bitmap;
            this.sWidth = bitmap.getWidth();
            this.sHeight = bitmap.getHeight();
            this.sOrientation = sOrientation;
            final boolean checkReady = this.checkReady();
            final boolean checkImageLoaded = this.checkImageLoaded();
            if (checkReady || checkImageLoaded) {
                this.invalidate();
                this.requestLayout();
            }
        }
    }
    
    private void onPreviewLoaded(final Bitmap bitmap) {
        synchronized (this) {
            this.debug("onPreviewLoaded", new Object[0]);
            if (this.bitmap == null && !this.imageLoadedSent) {
                final Rect pRegion = this.pRegion;
                if (pRegion != null) {
                    this.bitmap = Bitmap.createBitmap(bitmap, pRegion.left, this.pRegion.top, this.pRegion.width(), this.pRegion.height());
                }
                else {
                    this.bitmap = bitmap;
                }
                this.bitmapIsPreview = true;
                if (this.checkReady()) {
                    this.invalidate();
                    this.requestLayout();
                }
                return;
            }
            bitmap.recycle();
        }
    }
    
    private void onTileLoaded() {
        synchronized (this) {
            this.debug("onTileLoaded", new Object[0]);
            this.checkReady();
            this.checkImageLoaded();
            if (this.isBaseLayerReady()) {
                final Bitmap bitmap = this.bitmap;
                if (bitmap != null) {
                    if (!this.bitmapIsCached) {
                        bitmap.recycle();
                    }
                    this.bitmap = null;
                    final OnImageEventListener onImageEventListener = this.onImageEventListener;
                    if (onImageEventListener != null && this.bitmapIsCached) {
                        onImageEventListener.onPreviewReleased();
                    }
                    this.bitmapIsPreview = false;
                    this.bitmapIsCached = false;
                }
            }
            this.invalidate();
        }
    }
    
    private void onTilesInited(final ImageRegionDecoder decoder, int n, final int n2, final int sOrientation) {
        synchronized (this) {
            this.debug("onTilesInited sWidth=%d, sHeight=%d, sOrientation=%d", n, n2, this.orientation);
            final int sWidth = this.sWidth;
            if (sWidth > 0) {
                final int sHeight = this.sHeight;
                if (sHeight > 0 && (sWidth != n || sHeight != n2)) {
                    this.reset(false);
                    final Bitmap bitmap = this.bitmap;
                    if (bitmap != null) {
                        if (!this.bitmapIsCached) {
                            bitmap.recycle();
                        }
                        this.bitmap = null;
                        final OnImageEventListener onImageEventListener = this.onImageEventListener;
                        if (onImageEventListener != null && this.bitmapIsCached) {
                            onImageEventListener.onPreviewReleased();
                        }
                        this.bitmapIsPreview = false;
                        this.bitmapIsCached = false;
                    }
                }
            }
            this.decoder = decoder;
            this.sWidth = n;
            this.sHeight = n2;
            this.sOrientation = sOrientation;
            this.checkReady();
            if (!this.checkImageLoaded()) {
                n = this.maxTileWidth;
                if (n > 0 && n != Integer.MAX_VALUE) {
                    n = this.maxTileHeight;
                    if (n > 0 && n != Integer.MAX_VALUE && this.getWidth() > 0 && this.getHeight() > 0) {
                        this.initialiseBaseLayer(new Point(this.maxTileWidth, this.maxTileHeight));
                    }
                }
            }
            this.invalidate();
            this.requestLayout();
        }
    }
    
    private boolean onTouchEventInternal(final MotionEvent motionEvent) {
        final int pointerCount = motionEvent.getPointerCount();
        final int action = motionEvent.getAction();
        Label_1862: {
            if (action != 0) {
                Label_1659: {
                    if (action != 1) {
                        if (action != 2) {
                            if (action == 5) {
                                break Label_1862;
                            }
                            if (action == 6) {
                                break Label_1659;
                            }
                            if (action == 261) {
                                break Label_1862;
                            }
                            if (action == 262) {
                                break Label_1659;
                            }
                        }
                        else {
                            boolean b7 = false;
                            Label_1638: {
                                Label_1635: {
                                    if (this.maxTouchCount > 0) {
                                        if (pointerCount >= 2) {
                                            final float distance = this.distance(motionEvent.getX(0), motionEvent.getX(1), motionEvent.getY(0), motionEvent.getY(1));
                                            final float n = (motionEvent.getX(0) + motionEvent.getX(1)) / 2.0f;
                                            final float n2 = (motionEvent.getY(0) + motionEvent.getY(1)) / 2.0f;
                                            if (!this.zoomEnabled || (this.distance(this.vCenterStart.x, n, this.vCenterStart.y, n2) <= 5.0f && Math.abs(distance - this.vDistStart) <= 5.0f && !this.isPanning)) {
                                                break Label_1635;
                                            }
                                            this.isZooming = true;
                                            this.isPanning = true;
                                            final double n3 = this.scale;
                                            final float min = Math.min(this.maxScale, distance / this.vDistStart * this.scaleStart);
                                            this.scale = min;
                                            if (min <= this.minScale()) {
                                                this.vDistStart = distance;
                                                this.scaleStart = this.minScale();
                                                this.vCenterStart.set(n, n2);
                                                this.vTranslateStart.set(this.vTranslate);
                                            }
                                            else if (this.panEnabled) {
                                                final float x = this.vCenterStart.x;
                                                final float x2 = this.vTranslateStart.x;
                                                final float y = this.vCenterStart.y;
                                                final float y2 = this.vTranslateStart.y;
                                                final float scale = this.scale;
                                                final float scaleStart = this.scaleStart;
                                                final float n4 = scale / scaleStart;
                                                final float n5 = scale / scaleStart;
                                                this.vTranslate.x = n - (x - x2) * n4;
                                                this.vTranslate.y = n2 - (y - y2) * n5;
                                                if ((this.sHeight() * n3 < this.getHeight() && this.scale * this.sHeight() >= this.getHeight()) || (n3 * this.sWidth() < this.getWidth() && this.scale * this.sWidth() >= this.getWidth())) {
                                                    this.fitToBounds(true);
                                                    this.vCenterStart.set(n, n2);
                                                    this.vTranslateStart.set(this.vTranslate);
                                                    this.scaleStart = this.scale;
                                                    this.vDistStart = distance;
                                                }
                                            }
                                            else if (this.sRequestedCenter != null) {
                                                this.vTranslate.x = this.getWidth() / 2 - this.scale * this.sRequestedCenter.x;
                                                this.vTranslate.y = this.getHeight() / 2 - this.scale * this.sRequestedCenter.y;
                                            }
                                            else {
                                                this.vTranslate.x = this.getWidth() / 2 - this.scale * (this.sWidth() / 2);
                                                this.vTranslate.y = this.getHeight() / 2 - this.scale * (this.sHeight() / 2);
                                            }
                                            this.fitToBounds(true);
                                            this.refreshRequiredTiles(this.eagerLoadingEnabled);
                                        }
                                        else if (this.isQuickScaling) {
                                            final float quickScaleLastDistance = Math.abs(this.quickScaleVStart.y - motionEvent.getY()) * 2.0f + this.quickScaleThreshold;
                                            if (this.quickScaleLastDistance == -1.0f) {
                                                this.quickScaleLastDistance = quickScaleLastDistance;
                                            }
                                            final boolean b = motionEvent.getY() > this.quickScaleVLastPoint.y;
                                            this.quickScaleVLastPoint.set(0.0f, motionEvent.getY());
                                            final float n6 = quickScaleLastDistance / this.quickScaleLastDistance;
                                            final float n7 = 1.0f;
                                            final float n8 = Math.abs(1.0f - n6) * 0.5f;
                                            float quickScaleLastDistance2 = 0.0f;
                                            Label_1197: {
                                                if (n8 <= 0.03f) {
                                                    quickScaleLastDistance2 = quickScaleLastDistance;
                                                    if (!this.quickScaleMoved) {
                                                        break Label_1197;
                                                    }
                                                }
                                                this.quickScaleMoved = true;
                                                float n9 = n7;
                                                if (this.quickScaleLastDistance > 0.0f) {
                                                    if (b) {
                                                        n9 = n8 + 1.0f;
                                                    }
                                                    else {
                                                        n9 = 1.0f - n8;
                                                    }
                                                }
                                                final double n10 = this.scale;
                                                this.scale = Math.max(this.minScale(), Math.min(this.maxScale, this.scale * n9));
                                                if (this.panEnabled) {
                                                    final float x3 = this.vCenterStart.x;
                                                    final float x4 = this.vTranslateStart.x;
                                                    final float y3 = this.vCenterStart.y;
                                                    final float y4 = this.vTranslateStart.y;
                                                    final float scale2 = this.scale;
                                                    final float scaleStart2 = this.scaleStart;
                                                    final float n11 = scale2 / scaleStart2;
                                                    final float n12 = scale2 / scaleStart2;
                                                    this.vTranslate.x = this.vCenterStart.x - (x3 - x4) * n11;
                                                    this.vTranslate.y = this.vCenterStart.y - (y3 - y4) * n12;
                                                    if (this.sHeight() * n10 >= this.getHeight() || this.scale * this.sHeight() < this.getHeight()) {
                                                        quickScaleLastDistance2 = quickScaleLastDistance;
                                                        if (n10 * this.sWidth() >= this.getWidth()) {
                                                            break Label_1197;
                                                        }
                                                        quickScaleLastDistance2 = quickScaleLastDistance;
                                                        if (this.scale * this.sWidth() < this.getWidth()) {
                                                            break Label_1197;
                                                        }
                                                    }
                                                    this.fitToBounds(true);
                                                    this.vCenterStart.set(this.sourceToViewCoord(this.quickScaleSCenter));
                                                    this.vTranslateStart.set(this.vTranslate);
                                                    this.scaleStart = this.scale;
                                                    quickScaleLastDistance2 = 0.0f;
                                                }
                                                else if (this.sRequestedCenter != null) {
                                                    this.vTranslate.x = this.getWidth() / 2 - this.scale * this.sRequestedCenter.x;
                                                    this.vTranslate.y = this.getHeight() / 2 - this.scale * this.sRequestedCenter.y;
                                                    quickScaleLastDistance2 = quickScaleLastDistance;
                                                }
                                                else {
                                                    this.vTranslate.x = this.getWidth() / 2 - this.scale * (this.sWidth() / 2);
                                                    this.vTranslate.y = this.getHeight() / 2 - this.scale * (this.sHeight() / 2);
                                                    quickScaleLastDistance2 = quickScaleLastDistance;
                                                }
                                            }
                                            this.quickScaleLastDistance = quickScaleLastDistance2;
                                            this.fitToBounds(true);
                                            this.refreshRequiredTiles(this.eagerLoadingEnabled);
                                        }
                                        else {
                                            if (this.isZooming) {
                                                break Label_1635;
                                            }
                                            final float abs = Math.abs(motionEvent.getX() - this.vCenterStart.x);
                                            final float abs2 = Math.abs(motionEvent.getY() - this.vCenterStart.y);
                                            final float n13 = this.density * 5.0f;
                                            final float n14 = fcmpl(abs, n13);
                                            if (n14 <= 0 && abs2 <= n13 && !this.isPanning) {
                                                break Label_1635;
                                            }
                                            this.vTranslate.x = this.vTranslateStart.x + (motionEvent.getX() - this.vCenterStart.x);
                                            this.vTranslate.y = this.vTranslateStart.y + (motionEvent.getY() - this.vCenterStart.y);
                                            final float x5 = this.vTranslate.x;
                                            final float y5 = this.vTranslate.y;
                                            this.fitToBounds(true);
                                            final boolean b2 = x5 != this.vTranslate.x;
                                            final boolean b3 = y5 != this.vTranslate.y;
                                            final boolean b4 = b2 && abs > abs2 && !this.isPanning;
                                            final boolean b5 = b3 && abs2 > abs && !this.isPanning;
                                            final boolean b6 = y5 == this.vTranslate.y && abs2 > 3.0f * n13;
                                            if (!b4 && !b5 && (!b2 || !b3 || b6 || this.isPanning)) {
                                                this.isPanning = true;
                                            }
                                            else if (n14 > 0 || abs2 > n13) {
                                                this.maxTouchCount = 0;
                                                this.handler.removeMessages(1);
                                                this.requestDisallowInterceptTouchEvent(false);
                                            }
                                            if (!this.panEnabled) {
                                                this.vTranslate.x = this.vTranslateStart.x;
                                                this.vTranslate.y = this.vTranslateStart.y;
                                                this.requestDisallowInterceptTouchEvent(false);
                                            }
                                            this.refreshRequiredTiles(this.eagerLoadingEnabled);
                                        }
                                        b7 = true;
                                        break Label_1638;
                                    }
                                }
                                b7 = false;
                            }
                            if (b7) {
                                this.handler.removeMessages(1);
                                this.invalidate();
                                return true;
                            }
                        }
                        return false;
                    }
                }
                this.handler.removeMessages(1);
                if (this.isQuickScaling) {
                    this.isQuickScaling = false;
                    if (!this.quickScaleMoved) {
                        this.doubleTapZoom(this.quickScaleSCenter, this.vCenterStart);
                    }
                }
                if (this.maxTouchCount > 0) {
                    final boolean isZooming = this.isZooming;
                    if (isZooming || this.isPanning) {
                        if (isZooming && pointerCount == 2) {
                            this.isPanning = true;
                            this.vTranslateStart.set(this.vTranslate.x, this.vTranslate.y);
                            if (motionEvent.getActionIndex() == 1) {
                                this.vCenterStart.set(motionEvent.getX(0), motionEvent.getY(0));
                            }
                            else {
                                this.vCenterStart.set(motionEvent.getX(1), motionEvent.getY(1));
                            }
                        }
                        if (pointerCount < 3) {
                            this.isZooming = false;
                        }
                        if (pointerCount < 2) {
                            this.isPanning = false;
                            this.maxTouchCount = 0;
                        }
                        this.refreshRequiredTiles(true);
                        return true;
                    }
                }
                if (pointerCount == 1) {
                    this.isZooming = false;
                    this.isPanning = false;
                    this.maxTouchCount = 0;
                }
                return true;
            }
        }
        this.anim = null;
        this.requestDisallowInterceptTouchEvent(true);
        this.maxTouchCount = Math.max(this.maxTouchCount, pointerCount);
        if (pointerCount >= 2) {
            if (this.zoomEnabled) {
                final float distance2 = this.distance(motionEvent.getX(0), motionEvent.getX(1), motionEvent.getY(0), motionEvent.getY(1));
                this.scaleStart = this.scale;
                this.vDistStart = distance2;
                this.vTranslateStart.set(this.vTranslate.x, this.vTranslate.y);
                this.vCenterStart.set((motionEvent.getX(0) + motionEvent.getX(1)) / 2.0f, (motionEvent.getY(0) + motionEvent.getY(1)) / 2.0f);
            }
            else {
                this.maxTouchCount = 0;
            }
            this.handler.removeMessages(1);
        }
        else if (!this.isQuickScaling) {
            this.vTranslateStart.set(this.vTranslate.x, this.vTranslate.y);
            this.vCenterStart.set(motionEvent.getX(), motionEvent.getY());
            this.handler.sendEmptyMessageDelayed(1, 600L);
        }
        return true;
    }
    
    private void preDraw() {
        if (this.getWidth() != 0 && this.getHeight() != 0 && this.sWidth > 0) {
            if (this.sHeight > 0) {
                if (this.sPendingCenter != null) {
                    final Float pendingScale = this.pendingScale;
                    if (pendingScale != null) {
                        this.scale = pendingScale;
                        if (this.vTranslate == null) {
                            this.vTranslate = new PointF();
                        }
                        this.vTranslate.x = this.getWidth() / 2 - this.scale * this.sPendingCenter.x;
                        this.vTranslate.y = this.getHeight() / 2 - this.scale * this.sPendingCenter.y;
                        this.sPendingCenter = null;
                        this.pendingScale = null;
                        this.fitToBounds(true);
                        this.refreshRequiredTiles(true);
                    }
                }
                this.fitToBounds(false);
            }
        }
    }
    
    private int px(final int n) {
        return (int)(this.density * n);
    }
    
    private void refreshRequiredTiles(final boolean b) {
        if (this.decoder != null) {
            if (this.tileMap != null) {
                final int min = Math.min(this.fullImageSampleSize, this.calculateInSampleSize(this.scale));
                final Iterator<Map.Entry<Integer, List<Tile>>> iterator = this.tileMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    for (final Tile tile : ((Map.Entry<K, List>)iterator.next()).getValue()) {
                        if (tile.sampleSize < min || (tile.sampleSize > min && tile.sampleSize != this.fullImageSampleSize)) {
                            tile.visible = false;
                            if (tile.bitmap != null) {
                                tile.bitmap.recycle();
                                tile.bitmap = null;
                            }
                        }
                        if (tile.sampleSize == min) {
                            if (this.tileVisible(tile)) {
                                tile.visible = true;
                                if (tile.loading || tile.bitmap != null || !b) {
                                    continue;
                                }
                                this.execute(new TileLoadTask(this, this.decoder, tile));
                            }
                            else {
                                if (tile.sampleSize == this.fullImageSampleSize) {
                                    continue;
                                }
                                tile.visible = false;
                                if (tile.bitmap == null) {
                                    continue;
                                }
                                tile.bitmap.recycle();
                                tile.bitmap = null;
                            }
                        }
                        else {
                            if (tile.sampleSize != this.fullImageSampleSize) {
                                continue;
                            }
                            tile.visible = true;
                        }
                    }
                }
            }
        }
    }
    
    private void requestDisallowInterceptTouchEvent(final boolean b) {
        final ViewParent parent = this.getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(b);
        }
    }
    
    private void reset(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("reset newImage=");
        sb.append(b);
        this.debug(sb.toString(), new Object[0]);
        this.scale = 0.0f;
        this.scaleStart = 0.0f;
        this.vTranslate = null;
        this.vTranslateStart = null;
        this.vTranslateBefore = null;
        this.pendingScale = 0.0f;
        this.sPendingCenter = null;
        this.sRequestedCenter = null;
        this.isZooming = false;
        this.isPanning = false;
        this.isQuickScaling = false;
        this.maxTouchCount = 0;
        this.fullImageSampleSize = 0;
        this.vCenterStart = null;
        this.vDistStart = 0.0f;
        this.quickScaleLastDistance = 0.0f;
        this.quickScaleMoved = false;
        this.quickScaleSCenter = null;
        this.quickScaleVLastPoint = null;
        this.quickScaleVStart = null;
        this.anim = null;
        this.satTemp = null;
        this.matrix = null;
        this.sRect = null;
        if (b) {
            this.uri = null;
            this.decoderLock.writeLock().lock();
            try {
                final ImageRegionDecoder decoder = this.decoder;
                if (decoder != null) {
                    decoder.recycle();
                    this.decoder = null;
                }
                this.decoderLock.writeLock().unlock();
                final Bitmap bitmap = this.bitmap;
                if (bitmap != null && !this.bitmapIsCached) {
                    bitmap.recycle();
                }
                if (this.bitmap != null && this.bitmapIsCached) {
                    final OnImageEventListener onImageEventListener = this.onImageEventListener;
                    if (onImageEventListener != null) {
                        onImageEventListener.onPreviewReleased();
                    }
                }
                this.sWidth = 0;
                this.sHeight = 0;
                this.sOrientation = 0;
                this.sRegion = null;
                this.pRegion = null;
                this.readySent = false;
                this.imageLoadedSent = false;
                this.bitmap = null;
                this.bitmapIsPreview = false;
                this.bitmapIsCached = false;
            }
            finally {
                this.decoderLock.writeLock().unlock();
            }
        }
        final Map<Integer, List<Tile>> tileMap = this.tileMap;
        if (tileMap != null) {
            final Iterator<Map.Entry<Integer, List<Tile>>> iterator = tileMap.entrySet().iterator();
            while (iterator.hasNext()) {
                for (final Tile tile : ((Map.Entry<K, List>)iterator.next()).getValue()) {
                    tile.visible = false;
                    if (tile.bitmap != null) {
                        tile.bitmap.recycle();
                        tile.bitmap = null;
                    }
                }
            }
            this.tileMap = null;
        }
        this.setGestureDetector(this.getContext());
    }
    
    private void restoreState(final ImageViewState imageViewState) {
        if (imageViewState != null && SubsamplingScaleImageView.VALID_ORIENTATIONS.contains(imageViewState.getOrientation())) {
            this.orientation = imageViewState.getOrientation();
            this.pendingScale = imageViewState.getScale();
            this.sPendingCenter = imageViewState.getCenter();
            this.invalidate();
        }
    }
    
    private int sHeight() {
        final int requiredRotation = this.getRequiredRotation();
        if (requiredRotation != 90 && requiredRotation != 270) {
            return this.sHeight;
        }
        return this.sWidth;
    }
    
    private int sWidth() {
        final int requiredRotation = this.getRequiredRotation();
        if (requiredRotation != 90 && requiredRotation != 270) {
            return this.sWidth;
        }
        return this.sHeight;
    }
    
    private void sendStateChanged(final float n, final PointF pointF, final int n2) {
        final OnStateChangedListener onStateChangedListener = this.onStateChangedListener;
        if (onStateChangedListener != null) {
            final float scale = this.scale;
            if (scale != n) {
                onStateChangedListener.onScaleChanged(scale, n2);
            }
        }
        if (this.onStateChangedListener != null && !this.vTranslate.equals((Object)pointF)) {
            this.onStateChangedListener.onCenterChanged(this.getCenter(), n2);
        }
    }
    
    private void setGestureDetector(final Context context) {
        this.detector = new GestureDetector(context, (GestureDetector$OnGestureListener)new GestureDetector$SimpleOnGestureListener(this, context) {
            final SubsamplingScaleImageView this$0;
            final Context val$context;
            
            public boolean onDoubleTap(final MotionEvent motionEvent) {
                if (!this.this$0.zoomEnabled || !this.this$0.readySent || this.this$0.vTranslate == null) {
                    return super.onDoubleTapEvent(motionEvent);
                }
                this.this$0.setGestureDetector(this.val$context);
                if (this.this$0.quickScaleEnabled) {
                    this.this$0.vCenterStart = new PointF(motionEvent.getX(), motionEvent.getY());
                    this.this$0.vTranslateStart = new PointF(this.this$0.vTranslate.x, this.this$0.vTranslate.y);
                    final SubsamplingScaleImageView this$0 = this.this$0;
                    this$0.scaleStart = this$0.scale;
                    this.this$0.isQuickScaling = true;
                    this.this$0.isZooming = true;
                    this.this$0.quickScaleLastDistance = -1.0f;
                    final SubsamplingScaleImageView this$2 = this.this$0;
                    this$2.quickScaleSCenter = this$2.viewToSourceCoord(this$2.vCenterStart);
                    this.this$0.quickScaleVStart = new PointF(motionEvent.getX(), motionEvent.getY());
                    this.this$0.quickScaleVLastPoint = new PointF(this.this$0.quickScaleSCenter.x, this.this$0.quickScaleSCenter.y);
                    this.this$0.quickScaleMoved = false;
                    return false;
                }
                final SubsamplingScaleImageView this$3 = this.this$0;
                this$3.doubleTapZoom(this$3.viewToSourceCoord(new PointF(motionEvent.getX(), motionEvent.getY())), new PointF(motionEvent.getX(), motionEvent.getY()));
                return true;
            }
            
            public boolean onFling(final MotionEvent motionEvent, final MotionEvent motionEvent2, float a, float a2) {
                if (this.this$0.panEnabled && this.this$0.readySent && this.this$0.vTranslate != null && motionEvent != null && motionEvent2 != null && (Math.abs(motionEvent.getX() - motionEvent2.getX()) > 50.0f || Math.abs(motionEvent.getY() - motionEvent2.getY()) > 50.0f) && (Math.abs(a) > 500.0f || Math.abs(a2) > 500.0f) && !this.this$0.isZooming) {
                    final PointF pointF = new PointF(this.this$0.vTranslate.x + a * 0.25f, this.this$0.vTranslate.y + a2 * 0.25f);
                    a = (this.this$0.getWidth() / 2 - pointF.x) / this.this$0.scale;
                    a2 = (this.this$0.getHeight() / 2 - pointF.y) / this.this$0.scale;
                    new AnimationBuilder(new PointF(a, a2)).withEasing(1).withPanLimited(false).withOrigin(3).start();
                    return true;
                }
                return super.onFling(motionEvent, motionEvent2, a, a2);
            }
            
            public boolean onSingleTapConfirmed(final MotionEvent motionEvent) {
                this.this$0.performClick();
                return true;
            }
        });
        this.singleDetector = new GestureDetector(context, (GestureDetector$OnGestureListener)new GestureDetector$SimpleOnGestureListener(this) {
            final SubsamplingScaleImageView this$0;
            
            public boolean onSingleTapConfirmed(final MotionEvent motionEvent) {
                this.this$0.performClick();
                return true;
            }
        });
    }
    
    private void setMatrixArray(final float[] array, final float n, final float n2, final float n3, final float n4, final float n5, final float n6, final float n7, final float n8) {
        array[0] = n;
        array[1] = n2;
        array[2] = n3;
        array[3] = n4;
        array[4] = n5;
        array[5] = n6;
        array[6] = n7;
        array[7] = n8;
    }
    
    public static void setPreferredBitmapConfig(final Bitmap$Config preferredBitmapConfig) {
        SubsamplingScaleImageView.preferredBitmapConfig = preferredBitmapConfig;
    }
    
    private void sourceToViewRect(final Rect rect, final Rect rect2) {
        rect2.set((int)this.sourceToViewX((float)rect.left), (int)this.sourceToViewY((float)rect.top), (int)this.sourceToViewX((float)rect.right), (int)this.sourceToViewY((float)rect.bottom));
    }
    
    private float sourceToViewX(final float n) {
        final PointF vTranslate = this.vTranslate;
        if (vTranslate == null) {
            return Float.NaN;
        }
        return n * this.scale + vTranslate.x;
    }
    
    private float sourceToViewY(final float n) {
        final PointF vTranslate = this.vTranslate;
        if (vTranslate == null) {
            return Float.NaN;
        }
        return n * this.scale + vTranslate.y;
    }
    
    private boolean tileVisible(final Tile tile) {
        final float viewToSourceX = this.viewToSourceX(0.0f);
        final float viewToSourceX2 = this.viewToSourceX((float)this.getWidth());
        final float viewToSourceY = this.viewToSourceY(0.0f);
        final float viewToSourceY2 = this.viewToSourceY((float)this.getHeight());
        return viewToSourceX <= tile.sRect.right && tile.sRect.left <= viewToSourceX2 && viewToSourceY <= tile.sRect.bottom && tile.sRect.top <= viewToSourceY2;
    }
    
    private PointF vTranslateForSCenter(final float n, final float n2, final float n3) {
        final int paddingLeft = this.getPaddingLeft();
        final int n4 = (this.getWidth() - this.getPaddingRight() - this.getPaddingLeft()) / 2;
        final int paddingTop = this.getPaddingTop();
        final int n5 = (this.getHeight() - this.getPaddingBottom() - this.getPaddingTop()) / 2;
        if (this.satTemp == null) {
            this.satTemp = new ScaleAndTranslate(0.0f, new PointF(0.0f, 0.0f));
        }
        this.satTemp.scale = n3;
        this.satTemp.vTranslate.set(paddingLeft + n4 - n * n3, paddingTop + n5 - n2 * n3);
        this.fitToBounds(true, this.satTemp);
        return this.satTemp.vTranslate;
    }
    
    private float viewToSourceX(final float n) {
        final PointF vTranslate = this.vTranslate;
        if (vTranslate == null) {
            return Float.NaN;
        }
        return (n - vTranslate.x) / this.scale;
    }
    
    private float viewToSourceY(final float n) {
        final PointF vTranslate = this.vTranslate;
        if (vTranslate == null) {
            return Float.NaN;
        }
        return (n - vTranslate.y) / this.scale;
    }
    
    public AnimationBuilder animateCenter(final PointF pointF) {
        if (!this.isReady()) {
            return null;
        }
        return new AnimationBuilder(pointF);
    }
    
    public AnimationBuilder animateScale(final float n) {
        if (!this.isReady()) {
            return null;
        }
        return new AnimationBuilder(n);
    }
    
    public AnimationBuilder animateScaleAndCenter(final float n, final PointF pointF) {
        if (!this.isReady()) {
            return null;
        }
        return new AnimationBuilder(n, pointF);
    }
    
    public final int getAppliedOrientation() {
        return this.getRequiredRotation();
    }
    
    public final PointF getCenter() {
        return this.viewToSourceCoord((float)(this.getWidth() / 2), (float)(this.getHeight() / 2));
    }
    
    public float getMaxScale() {
        return this.maxScale;
    }
    
    public final float getMinScale() {
        return this.minScale();
    }
    
    public final int getOrientation() {
        return this.orientation;
    }
    
    public final void getPanRemaining(final RectF rectF) {
        if (!this.isReady()) {
            return;
        }
        final float n = this.scale * this.sWidth();
        final float n2 = this.scale * this.sHeight();
        final int panLimit = this.panLimit;
        if (panLimit == 3) {
            rectF.top = Math.max(0.0f, -(this.vTranslate.y - this.getHeight() / 2));
            rectF.left = Math.max(0.0f, -(this.vTranslate.x - this.getWidth() / 2));
            rectF.bottom = Math.max(0.0f, this.vTranslate.y - (this.getHeight() / 2 - n2));
            rectF.right = Math.max(0.0f, this.vTranslate.x - (this.getWidth() / 2 - n));
        }
        else if (panLimit == 2) {
            rectF.top = Math.max(0.0f, -(this.vTranslate.y - this.getHeight()));
            rectF.left = Math.max(0.0f, -(this.vTranslate.x - this.getWidth()));
            rectF.bottom = Math.max(0.0f, this.vTranslate.y + n2);
            rectF.right = Math.max(0.0f, this.vTranslate.x + n);
        }
        else {
            rectF.top = Math.max(0.0f, -this.vTranslate.y);
            rectF.left = Math.max(0.0f, -this.vTranslate.x);
            rectF.bottom = Math.max(0.0f, n2 + this.vTranslate.y - this.getHeight());
            rectF.right = Math.max(0.0f, n + this.vTranslate.x - this.getWidth());
        }
    }
    
    public final int getSHeight() {
        return this.sHeight;
    }
    
    public final int getSWidth() {
        return this.sWidth;
    }
    
    public final float getScale() {
        return this.scale;
    }
    
    public final ImageViewState getState() {
        if (this.vTranslate != null && this.sWidth > 0 && this.sHeight > 0) {
            return new ImageViewState(this.getScale(), this.getCenter(), this.getOrientation());
        }
        return null;
    }
    
    public boolean hasImage() {
        return this.uri != null || this.bitmap != null;
    }
    
    public final boolean isImageLoaded() {
        return this.imageLoadedSent;
    }
    
    public final boolean isPanEnabled() {
        return this.panEnabled;
    }
    
    public final boolean isQuickScaleEnabled() {
        return this.quickScaleEnabled;
    }
    
    public final boolean isReady() {
        return this.readySent;
    }
    
    public final boolean isZoomEnabled() {
        return this.zoomEnabled;
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        this.createPaints();
        if (this.sWidth != 0 && this.sHeight != 0 && this.getWidth() != 0) {
            if (this.getHeight() != 0) {
                if (this.tileMap == null && this.decoder != null) {
                    this.initialiseBaseLayer(this.getMaxBitmapDimensions(canvas));
                }
                if (!this.checkReady()) {
                    return;
                }
                this.preDraw();
                final Anim anim = this.anim;
                if (anim != null && anim.vFocusStart != null) {
                    final float scale = this.scale;
                    if (this.vTranslateBefore == null) {
                        this.vTranslateBefore = new PointF(0.0f, 0.0f);
                    }
                    this.vTranslateBefore.set(this.vTranslate);
                    final long a = System.currentTimeMillis() - this.anim.time;
                    final boolean b = a > this.anim.duration;
                    final long min = Math.min(a, this.anim.duration);
                    this.scale = this.ease(this.anim.easing, min, this.anim.scaleStart, this.anim.scaleEnd - this.anim.scaleStart, this.anim.duration);
                    final float ease = this.ease(this.anim.easing, min, this.anim.vFocusStart.x, this.anim.vFocusEnd.x - this.anim.vFocusStart.x, this.anim.duration);
                    final float ease2 = this.ease(this.anim.easing, min, this.anim.vFocusStart.y, this.anim.vFocusEnd.y - this.anim.vFocusStart.y, this.anim.duration);
                    final PointF vTranslate = this.vTranslate;
                    vTranslate.x -= this.sourceToViewX(this.anim.sCenterEnd.x) - ease;
                    final PointF vTranslate2 = this.vTranslate;
                    vTranslate2.y -= this.sourceToViewY(this.anim.sCenterEnd.y) - ease2;
                    this.fitToBounds(b || this.anim.scaleStart == this.anim.scaleEnd);
                    this.sendStateChanged(scale, this.vTranslateBefore, this.anim.origin);
                    this.refreshRequiredTiles(b);
                    if (b) {
                        if (this.anim.listener != null) {
                            try {
                                this.anim.listener.onComplete();
                            }
                            catch (final Exception ex) {
                                Log.w(SubsamplingScaleImageView.TAG, "Error thrown by animation listener", (Throwable)ex);
                            }
                        }
                        this.anim = null;
                    }
                    this.invalidate();
                }
                if (this.tileMap != null && this.isBaseLayerReady()) {
                    final int min2 = Math.min(this.fullImageSampleSize, this.calculateInSampleSize(this.scale));
                    final Iterator<Map.Entry<Integer, List<Tile>>> iterator = this.tileMap.entrySet().iterator();
                    int n = 0;
                    while (iterator.hasNext()) {
                        final Map.Entry<Integer, V> entry = (Map.Entry<Integer, V>)iterator.next();
                        if (entry.getKey() == min2) {
                            final Iterator iterator2 = ((List)entry.getValue()).iterator();
                            int n2 = n;
                            while (true) {
                                n = n2;
                                if (!iterator2.hasNext()) {
                                    break;
                                }
                                final Tile tile = (Tile)iterator2.next();
                                if (!tile.visible || (!tile.loading && tile.bitmap != null)) {
                                    continue;
                                }
                                n2 = 1;
                            }
                        }
                    }
                    for (final Map.Entry<Integer, V> entry2 : this.tileMap.entrySet()) {
                        if (entry2.getKey() != min2 && n == 0) {
                            continue;
                        }
                        for (final Tile tile2 : (List)entry2.getValue()) {
                            this.sourceToViewRect(tile2.sRect, tile2.vRect);
                            if (!tile2.loading && tile2.bitmap != null) {
                                if (this.tileBgPaint != null) {
                                    canvas.drawRect(tile2.vRect, this.tileBgPaint);
                                }
                                if (this.matrix == null) {
                                    this.matrix = new Matrix();
                                }
                                this.matrix.reset();
                                this.setMatrixArray(this.srcArray, 0.0f, 0.0f, (float)tile2.bitmap.getWidth(), 0.0f, (float)tile2.bitmap.getWidth(), (float)tile2.bitmap.getHeight(), 0.0f, (float)tile2.bitmap.getHeight());
                                if (this.getRequiredRotation() == 0) {
                                    this.setMatrixArray(this.dstArray, (float)tile2.vRect.left, (float)tile2.vRect.top, (float)tile2.vRect.right, (float)tile2.vRect.top, (float)tile2.vRect.right, (float)tile2.vRect.bottom, (float)tile2.vRect.left, (float)tile2.vRect.bottom);
                                }
                                else if (this.getRequiredRotation() == 90) {
                                    this.setMatrixArray(this.dstArray, (float)tile2.vRect.right, (float)tile2.vRect.top, (float)tile2.vRect.right, (float)tile2.vRect.bottom, (float)tile2.vRect.left, (float)tile2.vRect.bottom, (float)tile2.vRect.left, (float)tile2.vRect.top);
                                }
                                else if (this.getRequiredRotation() == 180) {
                                    this.setMatrixArray(this.dstArray, (float)tile2.vRect.right, (float)tile2.vRect.bottom, (float)tile2.vRect.left, (float)tile2.vRect.bottom, (float)tile2.vRect.left, (float)tile2.vRect.top, (float)tile2.vRect.right, (float)tile2.vRect.top);
                                }
                                else if (this.getRequiredRotation() == 270) {
                                    this.setMatrixArray(this.dstArray, (float)tile2.vRect.left, (float)tile2.vRect.bottom, (float)tile2.vRect.left, (float)tile2.vRect.top, (float)tile2.vRect.right, (float)tile2.vRect.top, (float)tile2.vRect.right, (float)tile2.vRect.bottom);
                                }
                                this.matrix.setPolyToPoly(this.srcArray, 0, this.dstArray, 0, 4);
                                canvas.drawBitmap(tile2.bitmap, this.matrix, this.bitmapPaint);
                                if (this.debug) {
                                    canvas.drawRect(tile2.vRect, this.debugLinePaint);
                                }
                            }
                            else if (tile2.loading && this.debug) {
                                canvas.drawText("LOADING", (float)(tile2.vRect.left + this.px(5)), (float)(tile2.vRect.top + this.px(35)), this.debugTextPaint);
                            }
                            if (tile2.visible && this.debug) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("ISS ");
                                sb.append(tile2.sampleSize);
                                sb.append(" RECT ");
                                sb.append(tile2.sRect.top);
                                sb.append(",");
                                sb.append(tile2.sRect.left);
                                sb.append(",");
                                sb.append(tile2.sRect.bottom);
                                sb.append(",");
                                sb.append(tile2.sRect.right);
                                canvas.drawText(sb.toString(), (float)(tile2.vRect.left + this.px(5)), (float)(tile2.vRect.top + this.px(15)), this.debugTextPaint);
                            }
                        }
                    }
                }
                else {
                    final Bitmap bitmap = this.bitmap;
                    if (bitmap != null) {
                        float scale2 = this.scale;
                        float n3;
                        if (this.bitmapIsPreview) {
                            scale2 *= this.sWidth / (float)bitmap.getWidth();
                            n3 = this.scale * (this.sHeight / (float)this.bitmap.getHeight());
                        }
                        else {
                            n3 = scale2;
                        }
                        if (this.matrix == null) {
                            this.matrix = new Matrix();
                        }
                        this.matrix.reset();
                        this.matrix.postScale(scale2, n3);
                        this.matrix.postRotate((float)this.getRequiredRotation());
                        this.matrix.postTranslate(this.vTranslate.x, this.vTranslate.y);
                        if (this.getRequiredRotation() == 180) {
                            final Matrix matrix = this.matrix;
                            final float scale3 = this.scale;
                            matrix.postTranslate(this.sWidth * scale3, scale3 * this.sHeight);
                        }
                        else if (this.getRequiredRotation() == 90) {
                            this.matrix.postTranslate(this.scale * this.sHeight, 0.0f);
                        }
                        else if (this.getRequiredRotation() == 270) {
                            this.matrix.postTranslate(0.0f, this.scale * this.sWidth);
                        }
                        if (this.tileBgPaint != null) {
                            if (this.sRect == null) {
                                this.sRect = new RectF();
                            }
                            final RectF sRect = this.sRect;
                            int n4;
                            if (this.bitmapIsPreview) {
                                n4 = this.bitmap.getWidth();
                            }
                            else {
                                n4 = this.sWidth;
                            }
                            final float n5 = (float)n4;
                            int n6;
                            if (this.bitmapIsPreview) {
                                n6 = this.bitmap.getHeight();
                            }
                            else {
                                n6 = this.sHeight;
                            }
                            sRect.set(0.0f, 0.0f, n5, (float)n6);
                            this.matrix.mapRect(this.sRect);
                            canvas.drawRect(this.sRect, this.tileBgPaint);
                        }
                        canvas.drawBitmap(this.bitmap, this.matrix, this.bitmapPaint);
                    }
                }
                if (this.debug) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Scale: ");
                    sb2.append(String.format(Locale.ENGLISH, "%.2f", this.scale));
                    sb2.append(" (");
                    sb2.append(String.format(Locale.ENGLISH, "%.2f", this.minScale()));
                    sb2.append(" - ");
                    sb2.append(String.format(Locale.ENGLISH, "%.2f", this.maxScale));
                    sb2.append(")");
                    canvas.drawText(sb2.toString(), (float)this.px(5), (float)this.px(15), this.debugTextPaint);
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Translate: ");
                    sb3.append(String.format(Locale.ENGLISH, "%.2f", this.vTranslate.x));
                    sb3.append(":");
                    sb3.append(String.format(Locale.ENGLISH, "%.2f", this.vTranslate.y));
                    canvas.drawText(sb3.toString(), (float)this.px(5), (float)this.px(30), this.debugTextPaint);
                    final PointF center = this.getCenter();
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Source center: ");
                    sb4.append(String.format(Locale.ENGLISH, "%.2f", center.x));
                    sb4.append(":");
                    sb4.append(String.format(Locale.ENGLISH, "%.2f", center.y));
                    canvas.drawText(sb4.toString(), (float)this.px(5), (float)this.px(45), this.debugTextPaint);
                    final Anim anim2 = this.anim;
                    if (anim2 != null) {
                        final PointF sourceToViewCoord = this.sourceToViewCoord(anim2.sCenterStart);
                        final PointF sourceToViewCoord2 = this.sourceToViewCoord(this.anim.sCenterEndRequested);
                        final PointF sourceToViewCoord3 = this.sourceToViewCoord(this.anim.sCenterEnd);
                        canvas.drawCircle(sourceToViewCoord.x, sourceToViewCoord.y, (float)this.px(10), this.debugLinePaint);
                        this.debugLinePaint.setColor(-65536);
                        canvas.drawCircle(sourceToViewCoord2.x, sourceToViewCoord2.y, (float)this.px(20), this.debugLinePaint);
                        this.debugLinePaint.setColor(-16776961);
                        canvas.drawCircle(sourceToViewCoord3.x, sourceToViewCoord3.y, (float)this.px(25), this.debugLinePaint);
                        this.debugLinePaint.setColor(-16711681);
                        canvas.drawCircle((float)(this.getWidth() / 2), (float)(this.getHeight() / 2), (float)this.px(30), this.debugLinePaint);
                    }
                    if (this.vCenterStart != null) {
                        this.debugLinePaint.setColor(-65536);
                        canvas.drawCircle(this.vCenterStart.x, this.vCenterStart.y, (float)this.px(20), this.debugLinePaint);
                    }
                    if (this.quickScaleSCenter != null) {
                        this.debugLinePaint.setColor(-16776961);
                        canvas.drawCircle(this.sourceToViewX(this.quickScaleSCenter.x), this.sourceToViewY(this.quickScaleSCenter.y), (float)this.px(35), this.debugLinePaint);
                    }
                    if (this.quickScaleVStart != null && this.isQuickScaling) {
                        this.debugLinePaint.setColor(-16711681);
                        canvas.drawCircle(this.quickScaleVStart.x, this.quickScaleVStart.y, (float)this.px(30), this.debugLinePaint);
                    }
                    this.debugLinePaint.setColor(-65281);
                }
            }
        }
    }
    
    protected void onImageLoaded() {
    }
    
    protected void onMeasure(int sWidth, int sHeight) {
        final int mode = View$MeasureSpec.getMode(sWidth);
        final int mode2 = View$MeasureSpec.getMode(sHeight);
        final int size = View$MeasureSpec.getSize(sWidth);
        final int size2 = View$MeasureSpec.getSize(sHeight);
        boolean b = true;
        final boolean b2 = mode != 1073741824;
        if (mode2 == 1073741824) {
            b = false;
        }
        sWidth = size;
        sHeight = size2;
        if (this.sWidth > 0) {
            sWidth = size;
            sHeight = size2;
            if (this.sHeight > 0) {
                if (b2 && b) {
                    sWidth = this.sWidth();
                    sHeight = this.sHeight();
                }
                else if (b) {
                    sHeight = (int)(this.sHeight() / (double)this.sWidth() * size);
                    sWidth = size;
                }
                else {
                    sWidth = size;
                    sHeight = size2;
                    if (b2) {
                        sWidth = (int)(this.sWidth() / (double)this.sHeight() * size2);
                        sHeight = size2;
                    }
                }
            }
        }
        this.setMeasuredDimension(Math.max(sWidth, this.getSuggestedMinimumWidth()), Math.max(sHeight, this.getSuggestedMinimumHeight()));
    }
    
    protected void onReady() {
    }
    
    protected void onSizeChanged(final int i, final int j, final int k, final int l) {
        this.debug("onSizeChanged %dx%d -> %dx%d", k, l, i, j);
        final PointF center = this.getCenter();
        if (this.readySent && center != null) {
            this.anim = null;
            this.pendingScale = this.scale;
            this.sPendingCenter = center;
        }
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final Anim anim = this.anim;
        final boolean b = true;
        if (anim != null && !anim.interruptible) {
            this.requestDisallowInterceptTouchEvent(true);
            return true;
        }
        final Anim anim2 = this.anim;
        if (anim2 != null && anim2.listener != null) {
            try {
                this.anim.listener.onInterruptedByUser();
            }
            catch (final Exception ex) {
                Log.w(SubsamplingScaleImageView.TAG, "Error thrown by animation listener", (Throwable)ex);
            }
        }
        this.anim = null;
        if (this.vTranslate == null) {
            final GestureDetector singleDetector = this.singleDetector;
            if (singleDetector != null) {
                singleDetector.onTouchEvent(motionEvent);
            }
            return true;
        }
        if (!this.isQuickScaling) {
            final GestureDetector detector = this.detector;
            if (detector == null || detector.onTouchEvent(motionEvent)) {
                this.isZooming = false;
                this.isPanning = false;
                this.maxTouchCount = 0;
                return true;
            }
        }
        if (this.vTranslateStart == null) {
            this.vTranslateStart = new PointF(0.0f, 0.0f);
        }
        if (this.vTranslateBefore == null) {
            this.vTranslateBefore = new PointF(0.0f, 0.0f);
        }
        if (this.vCenterStart == null) {
            this.vCenterStart = new PointF(0.0f, 0.0f);
        }
        final float scale = this.scale;
        this.vTranslateBefore.set(this.vTranslate);
        final boolean onTouchEventInternal = this.onTouchEventInternal(motionEvent);
        this.sendStateChanged(scale, this.vTranslateBefore, 2);
        boolean b2 = b;
        if (!onTouchEventInternal) {
            b2 = (super.onTouchEvent(motionEvent) && b);
        }
        return b2;
    }
    
    public void recycle() {
        this.reset(true);
        this.bitmapPaint = null;
        this.debugTextPaint = null;
        this.debugLinePaint = null;
        this.tileBgPaint = null;
    }
    
    public final void resetScaleAndCenter() {
        this.anim = null;
        this.pendingScale = this.limitedScale(0.0f);
        if (this.isReady()) {
            this.sPendingCenter = new PointF((float)(this.sWidth() / 2), (float)(this.sHeight() / 2));
        }
        else {
            this.sPendingCenter = new PointF(0.0f, 0.0f);
        }
        this.invalidate();
    }
    
    public final void setBitmapDecoderClass(final Class<? extends ImageDecoder> clazz) {
        if (clazz != null) {
            this.bitmapDecoderFactory = new CompatDecoderFactory<ImageDecoder>(clazz);
            return;
        }
        throw new IllegalArgumentException("Decoder class cannot be set to null");
    }
    
    public final void setBitmapDecoderFactory(final DecoderFactory<? extends ImageDecoder> bitmapDecoderFactory) {
        if (bitmapDecoderFactory != null) {
            this.bitmapDecoderFactory = bitmapDecoderFactory;
            return;
        }
        throw new IllegalArgumentException("Decoder factory cannot be set to null");
    }
    
    public final void setDebug(final boolean debug) {
        this.debug = debug;
    }
    
    public final void setDoubleTapZoomDpi(final int n) {
        final DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        this.setDoubleTapZoomScale((displayMetrics.xdpi + displayMetrics.ydpi) / 2.0f / n);
    }
    
    public final void setDoubleTapZoomDuration(final int b) {
        this.doubleTapZoomDuration = Math.max(0, b);
    }
    
    public final void setDoubleTapZoomScale(final float doubleTapZoomScale) {
        this.doubleTapZoomScale = doubleTapZoomScale;
    }
    
    public final void setDoubleTapZoomStyle(final int i) {
        if (SubsamplingScaleImageView.VALID_ZOOM_STYLES.contains(i)) {
            this.doubleTapZoomStyle = i;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid zoom style: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void setEagerLoadingEnabled(final boolean eagerLoadingEnabled) {
        this.eagerLoadingEnabled = eagerLoadingEnabled;
    }
    
    public void setExecutor(final Executor executor) {
        if (executor != null) {
            this.executor = executor;
            return;
        }
        throw new NullPointerException("Executor must not be null");
    }
    
    public final void setImage(final ImageSource imageSource) {
        this.setImage(imageSource, null, null);
    }
    
    public final void setImage(final ImageSource imageSource, final ImageSource imageSource2) {
        this.setImage(imageSource, imageSource2, null);
    }
    
    public final void setImage(final ImageSource imageSource, final ImageSource imageSource2, final ImageViewState imageViewState) {
        if (imageSource != null) {
            this.reset(true);
            if (imageViewState != null) {
                this.restoreState(imageViewState);
            }
            if (imageSource2 != null) {
                if (imageSource.getBitmap() != null) {
                    throw new IllegalArgumentException("Preview image cannot be used when a bitmap is provided for the main image");
                }
                if (imageSource.getSWidth() <= 0 || imageSource.getSHeight() <= 0) {
                    throw new IllegalArgumentException("Preview image cannot be used unless dimensions are provided for the main image");
                }
                this.sWidth = imageSource.getSWidth();
                this.sHeight = imageSource.getSHeight();
                this.pRegion = imageSource2.getSRegion();
                if (imageSource2.getBitmap() != null) {
                    this.bitmapIsCached = imageSource2.isCached();
                    this.onPreviewLoaded(imageSource2.getBitmap());
                }
                else {
                    final Uri uri = imageSource2.getUri();
                    Uri parse;
                    if ((parse = uri) == null) {
                        parse = uri;
                        if (imageSource2.getResource() != null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("android.resource://");
                            sb.append(this.getContext().getPackageName());
                            sb.append("/");
                            sb.append(imageSource2.getResource());
                            parse = Uri.parse(sb.toString());
                        }
                    }
                    this.execute(new BitmapLoadTask(this, this.getContext(), this.bitmapDecoderFactory, parse, true));
                }
            }
            if (imageSource.getBitmap() != null && imageSource.getSRegion() != null) {
                this.onImageLoaded(Bitmap.createBitmap(imageSource.getBitmap(), imageSource.getSRegion().left, imageSource.getSRegion().top, imageSource.getSRegion().width(), imageSource.getSRegion().height()), 0, false);
            }
            else if (imageSource.getBitmap() != null) {
                this.onImageLoaded(imageSource.getBitmap(), 0, imageSource.isCached());
            }
            else {
                this.sRegion = imageSource.getSRegion();
                final Uri uri2 = imageSource.getUri();
                this.uri = uri2;
                if (uri2 == null && imageSource.getResource() != null) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("android.resource://");
                    sb2.append(this.getContext().getPackageName());
                    sb2.append("/");
                    sb2.append(imageSource.getResource());
                    this.uri = Uri.parse(sb2.toString());
                }
                if (!imageSource.getTile() && this.sRegion == null) {
                    this.execute(new BitmapLoadTask(this, this.getContext(), this.bitmapDecoderFactory, this.uri, false));
                }
                else {
                    this.execute(new TilesInitTask(this, this.getContext(), this.regionDecoderFactory, this.uri));
                }
            }
            return;
        }
        throw new NullPointerException("imageSource must not be null");
    }
    
    public final void setImage(final ImageSource imageSource, final ImageViewState imageViewState) {
        this.setImage(imageSource, null, imageViewState);
    }
    
    public final void setMaxScale(final float maxScale) {
        this.maxScale = maxScale;
    }
    
    public void setMaxTileSize(final int n) {
        this.maxTileWidth = n;
        this.maxTileHeight = n;
    }
    
    public void setMaxTileSize(final int maxTileWidth, final int maxTileHeight) {
        this.maxTileWidth = maxTileWidth;
        this.maxTileHeight = maxTileHeight;
    }
    
    public final void setMaximumDpi(final int n) {
        final DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        this.setMinScale((displayMetrics.xdpi + displayMetrics.ydpi) / 2.0f / n);
    }
    
    public final void setMinScale(final float minScale) {
        this.minScale = minScale;
    }
    
    public final void setMinimumDpi(final int n) {
        final DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        this.setMaxScale((displayMetrics.xdpi + displayMetrics.ydpi) / 2.0f / n);
    }
    
    public final void setMinimumScaleType(final int i) {
        if (SubsamplingScaleImageView.VALID_SCALE_TYPES.contains(i)) {
            this.minimumScaleType = i;
            if (this.isReady()) {
                this.fitToBounds(true);
                this.invalidate();
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid scale type: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void setMinimumTileDpi(final int n) {
        final DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        this.minimumTileDpi = (int)Math.min((displayMetrics.xdpi + displayMetrics.ydpi) / 2.0f, (float)n);
        if (this.isReady()) {
            this.reset(false);
            this.invalidate();
        }
    }
    
    public void setOnImageEventListener(final OnImageEventListener onImageEventListener) {
        this.onImageEventListener = onImageEventListener;
    }
    
    public void setOnLongClickListener(final View$OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }
    
    public void setOnStateChangedListener(final OnStateChangedListener onStateChangedListener) {
        this.onStateChangedListener = onStateChangedListener;
    }
    
    public final void setOrientation(final int i) {
        if (SubsamplingScaleImageView.VALID_ORIENTATIONS.contains(i)) {
            this.orientation = i;
            this.reset(false);
            this.invalidate();
            this.requestLayout();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid orientation: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void setPanEnabled(final boolean panEnabled) {
        if (!(this.panEnabled = panEnabled)) {
            final PointF vTranslate = this.vTranslate;
            if (vTranslate != null) {
                vTranslate.x = this.getWidth() / 2 - this.scale * (this.sWidth() / 2);
                this.vTranslate.y = this.getHeight() / 2 - this.scale * (this.sHeight() / 2);
                if (this.isReady()) {
                    this.refreshRequiredTiles(true);
                    this.invalidate();
                }
            }
        }
    }
    
    public final void setPanLimit(final int i) {
        if (SubsamplingScaleImageView.VALID_PAN_LIMITS.contains(i)) {
            this.panLimit = i;
            if (this.isReady()) {
                this.fitToBounds(true);
                this.invalidate();
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid pan limit: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void setQuickScaleEnabled(final boolean quickScaleEnabled) {
        this.quickScaleEnabled = quickScaleEnabled;
    }
    
    public final void setRegionDecoderClass(final Class<? extends ImageRegionDecoder> clazz) {
        if (clazz != null) {
            this.regionDecoderFactory = new CompatDecoderFactory<ImageRegionDecoder>(clazz);
            return;
        }
        throw new IllegalArgumentException("Decoder class cannot be set to null");
    }
    
    public final void setRegionDecoderFactory(final DecoderFactory<? extends ImageRegionDecoder> regionDecoderFactory) {
        if (regionDecoderFactory != null) {
            this.regionDecoderFactory = regionDecoderFactory;
            return;
        }
        throw new IllegalArgumentException("Decoder factory cannot be set to null");
    }
    
    public final void setScaleAndCenter(final float f, final PointF pointF) {
        this.anim = null;
        this.pendingScale = f;
        this.sPendingCenter = pointF;
        this.sRequestedCenter = pointF;
        this.invalidate();
    }
    
    public final void setTileBackgroundColor(final int color) {
        if (Color.alpha(color) == 0) {
            this.tileBgPaint = null;
        }
        else {
            (this.tileBgPaint = new Paint()).setStyle(Paint$Style.FILL);
            this.tileBgPaint.setColor(color);
        }
        this.invalidate();
    }
    
    public final void setZoomEnabled(final boolean zoomEnabled) {
        this.zoomEnabled = zoomEnabled;
    }
    
    public final PointF sourceToViewCoord(final float n, final float n2) {
        return this.sourceToViewCoord(n, n2, new PointF());
    }
    
    public final PointF sourceToViewCoord(final float n, final float n2, final PointF pointF) {
        if (this.vTranslate == null) {
            return null;
        }
        pointF.set(this.sourceToViewX(n), this.sourceToViewY(n2));
        return pointF;
    }
    
    public final PointF sourceToViewCoord(final PointF pointF) {
        return this.sourceToViewCoord(pointF.x, pointF.y, new PointF());
    }
    
    public final PointF sourceToViewCoord(final PointF pointF, final PointF pointF2) {
        return this.sourceToViewCoord(pointF.x, pointF.y, pointF2);
    }
    
    public void viewToFileRect(Rect sRegion, final Rect rect) {
        if (this.vTranslate != null) {
            if (this.readySent) {
                rect.set((int)this.viewToSourceX((float)sRegion.left), (int)this.viewToSourceY((float)sRegion.top), (int)this.viewToSourceX((float)sRegion.right), (int)this.viewToSourceY((float)sRegion.bottom));
                this.fileSRect(rect, rect);
                rect.set(Math.max(0, rect.left), Math.max(0, rect.top), Math.min(this.sWidth, rect.right), Math.min(this.sHeight, rect.bottom));
                sRegion = this.sRegion;
                if (sRegion != null) {
                    rect.offset(sRegion.left, this.sRegion.top);
                }
            }
        }
    }
    
    public final PointF viewToSourceCoord(final float n, final float n2) {
        return this.viewToSourceCoord(n, n2, new PointF());
    }
    
    public final PointF viewToSourceCoord(final float n, final float n2, final PointF pointF) {
        if (this.vTranslate == null) {
            return null;
        }
        pointF.set(this.viewToSourceX(n), this.viewToSourceY(n2));
        return pointF;
    }
    
    public final PointF viewToSourceCoord(final PointF pointF) {
        return this.viewToSourceCoord(pointF.x, pointF.y, new PointF());
    }
    
    public final PointF viewToSourceCoord(final PointF pointF, final PointF pointF2) {
        return this.viewToSourceCoord(pointF.x, pointF.y, pointF2);
    }
    
    public void visibleFileRect(final Rect rect) {
        if (this.vTranslate != null) {
            if (this.readySent) {
                rect.set(0, 0, this.getWidth(), this.getHeight());
                this.viewToFileRect(rect, rect);
            }
        }
    }
    
    private static class Anim
    {
        private long duration;
        private int easing;
        private boolean interruptible;
        private OnAnimationEventListener listener;
        private int origin;
        private PointF sCenterEnd;
        private PointF sCenterEndRequested;
        private PointF sCenterStart;
        private float scaleEnd;
        private float scaleStart;
        private long time;
        private PointF vFocusEnd;
        private PointF vFocusStart;
        
        private Anim() {
            this.duration = 500L;
            this.interruptible = true;
            this.easing = 2;
            this.origin = 1;
            this.time = System.currentTimeMillis();
        }
    }
    
    public final class AnimationBuilder
    {
        private long duration;
        private int easing;
        private boolean interruptible;
        private OnAnimationEventListener listener;
        private int origin;
        private boolean panLimited;
        private final PointF targetSCenter;
        private final float targetScale;
        final SubsamplingScaleImageView this$0;
        private final PointF vFocus;
        
        private AnimationBuilder(final SubsamplingScaleImageView this$0, final float targetScale) {
            this.this$0 = this$0;
            this.duration = 500L;
            this.easing = 2;
            this.origin = 1;
            this.interruptible = true;
            this.panLimited = true;
            this.targetScale = targetScale;
            this.targetSCenter = this$0.getCenter();
            this.vFocus = null;
        }
        
        private AnimationBuilder(final SubsamplingScaleImageView this$0, final float targetScale, final PointF targetSCenter) {
            this.this$0 = this$0;
            this.duration = 500L;
            this.easing = 2;
            this.origin = 1;
            this.interruptible = true;
            this.panLimited = true;
            this.targetScale = targetScale;
            this.targetSCenter = targetSCenter;
            this.vFocus = null;
        }
        
        private AnimationBuilder(final SubsamplingScaleImageView this$0, final float targetScale, final PointF targetSCenter, final PointF vFocus) {
            this.this$0 = this$0;
            this.duration = 500L;
            this.easing = 2;
            this.origin = 1;
            this.interruptible = true;
            this.panLimited = true;
            this.targetScale = targetScale;
            this.targetSCenter = targetSCenter;
            this.vFocus = vFocus;
        }
        
        private AnimationBuilder(final SubsamplingScaleImageView this$0, final PointF targetSCenter) {
            this.this$0 = this$0;
            this.duration = 500L;
            this.easing = 2;
            this.origin = 1;
            this.interruptible = true;
            this.panLimited = true;
            this.targetScale = this$0.scale;
            this.targetSCenter = targetSCenter;
            this.vFocus = null;
        }
        
        private AnimationBuilder withOrigin(final int origin) {
            this.origin = origin;
            return this;
        }
        
        private AnimationBuilder withPanLimited(final boolean panLimited) {
            this.panLimited = panLimited;
            return this;
        }
        
        public void start() {
            if (this.this$0.anim != null && this.this$0.anim.listener != null) {
                try {
                    this.this$0.anim.listener.onInterruptedByNewAnim();
                }
                catch (final Exception ex) {
                    Log.w(SubsamplingScaleImageView.TAG, "Error thrown by animation listener", (Throwable)ex);
                }
            }
            final int paddingLeft = this.this$0.getPaddingLeft();
            final int n = (this.this$0.getWidth() - this.this$0.getPaddingRight() - this.this$0.getPaddingLeft()) / 2;
            final int paddingTop = this.this$0.getPaddingTop();
            final int n2 = (this.this$0.getHeight() - this.this$0.getPaddingBottom() - this.this$0.getPaddingTop()) / 2;
            final float access$6500 = this.this$0.limitedScale(this.targetScale);
            PointF pointF;
            if (this.panLimited) {
                pointF = this.this$0.limitedSCenter(this.targetSCenter.x, this.targetSCenter.y, access$6500, new PointF());
            }
            else {
                pointF = this.targetSCenter;
            }
            this.this$0.anim = new Anim();
            this.this$0.anim.scaleStart = this.this$0.scale;
            this.this$0.anim.scaleEnd = access$6500;
            this.this$0.anim.time = System.currentTimeMillis();
            this.this$0.anim.sCenterEndRequested = pointF;
            this.this$0.anim.sCenterStart = this.this$0.getCenter();
            this.this$0.anim.sCenterEnd = pointF;
            this.this$0.anim.vFocusStart = this.this$0.sourceToViewCoord(pointF);
            this.this$0.anim.vFocusEnd = new PointF((float)(paddingLeft + n), (float)(paddingTop + n2));
            this.this$0.anim.duration = this.duration;
            this.this$0.anim.interruptible = this.interruptible;
            this.this$0.anim.easing = this.easing;
            this.this$0.anim.origin = this.origin;
            this.this$0.anim.time = System.currentTimeMillis();
            this.this$0.anim.listener = this.listener;
            final PointF vFocus = this.vFocus;
            if (vFocus != null) {
                final float n3 = vFocus.x - this.this$0.anim.sCenterStart.x * access$6500;
                final float n4 = this.vFocus.y - this.this$0.anim.sCenterStart.y * access$6500;
                final ScaleAndTranslate scaleAndTranslate = new ScaleAndTranslate(access$6500, new PointF(n3, n4));
                this.this$0.fitToBounds(true, scaleAndTranslate);
                this.this$0.anim.vFocusEnd = new PointF(this.vFocus.x + (scaleAndTranslate.vTranslate.x - n3), this.vFocus.y + (scaleAndTranslate.vTranslate.y - n4));
            }
            this.this$0.invalidate();
        }
        
        public AnimationBuilder withDuration(final long duration) {
            this.duration = duration;
            return this;
        }
        
        public AnimationBuilder withEasing(final int i) {
            if (SubsamplingScaleImageView.VALID_EASING_STYLES.contains(i)) {
                this.easing = i;
                return this;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown easing type: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public AnimationBuilder withInterruptible(final boolean interruptible) {
            this.interruptible = interruptible;
            return this;
        }
        
        public AnimationBuilder withOnAnimationEventListener(final OnAnimationEventListener listener) {
            this.listener = listener;
            return this;
        }
    }
    
    private static class BitmapLoadTask extends AsyncTask<Void, Void, Integer>
    {
        private Bitmap bitmap;
        private final WeakReference<Context> contextRef;
        private final WeakReference<DecoderFactory<? extends ImageDecoder>> decoderFactoryRef;
        private Exception exception;
        private final boolean preview;
        private final Uri source;
        private final WeakReference<SubsamplingScaleImageView> viewRef;
        
        BitmapLoadTask(final SubsamplingScaleImageView referent, final Context referent2, final DecoderFactory<? extends ImageDecoder> referent3, final Uri source, final boolean preview) {
            this.viewRef = new WeakReference<SubsamplingScaleImageView>(referent);
            this.contextRef = new WeakReference<Context>(referent2);
            this.decoderFactoryRef = new WeakReference<DecoderFactory<? extends ImageDecoder>>(referent3);
            this.source = source;
            this.preview = preview;
        }
        
        protected Integer doInBackground(final Void... array) {
            try {
                final String string = this.source.toString();
                final Context context = this.contextRef.get();
                final DecoderFactory decoderFactory = this.decoderFactoryRef.get();
                final SubsamplingScaleImageView subsamplingScaleImageView = this.viewRef.get();
                if (context != null && decoderFactory != null && subsamplingScaleImageView != null) {
                    subsamplingScaleImageView.debug("BitmapLoadTask.doInBackground", new Object[0]);
                    this.bitmap = ((ImageDecoder)decoderFactory.make()).decode(context, this.source);
                    return subsamplingScaleImageView.getExifOrientation(context, string);
                }
            }
            catch (final OutOfMemoryError cause) {
                Log.e(SubsamplingScaleImageView.TAG, "Failed to load bitmap - OutOfMemoryError", (Throwable)cause);
                this.exception = new RuntimeException(cause);
            }
            catch (final Exception exception) {
                Log.e(SubsamplingScaleImageView.TAG, "Failed to load bitmap", (Throwable)exception);
                this.exception = exception;
            }
            return null;
        }
        
        protected void onPostExecute(final Integer n) {
            final SubsamplingScaleImageView subsamplingScaleImageView = this.viewRef.get();
            if (subsamplingScaleImageView != null) {
                final Bitmap bitmap = this.bitmap;
                if (bitmap != null && n != null) {
                    if (this.preview) {
                        subsamplingScaleImageView.onPreviewLoaded(bitmap);
                    }
                    else {
                        subsamplingScaleImageView.onImageLoaded(bitmap, n, false);
                    }
                }
                else if (this.exception != null && subsamplingScaleImageView.onImageEventListener != null) {
                    if (this.preview) {
                        subsamplingScaleImageView.onImageEventListener.onPreviewLoadError(this.exception);
                    }
                    else {
                        subsamplingScaleImageView.onImageEventListener.onImageLoadError(this.exception);
                    }
                }
            }
        }
    }
    
    public static class DefaultOnAnimationEventListener implements OnAnimationEventListener
    {
        @Override
        public void onComplete() {
        }
        
        @Override
        public void onInterruptedByNewAnim() {
        }
        
        @Override
        public void onInterruptedByUser() {
        }
    }
    
    public interface OnAnimationEventListener
    {
        void onComplete();
        
        void onInterruptedByNewAnim();
        
        void onInterruptedByUser();
    }
    
    public static class DefaultOnImageEventListener implements OnImageEventListener
    {
        @Override
        public void onImageLoadError(final Exception ex) {
        }
        
        @Override
        public void onImageLoaded() {
        }
        
        @Override
        public void onPreviewLoadError(final Exception ex) {
        }
        
        @Override
        public void onPreviewReleased() {
        }
        
        @Override
        public void onReady() {
        }
        
        @Override
        public void onTileLoadError(final Exception ex) {
        }
    }
    
    public interface OnImageEventListener
    {
        void onImageLoadError(final Exception p0);
        
        void onImageLoaded();
        
        void onPreviewLoadError(final Exception p0);
        
        void onPreviewReleased();
        
        void onReady();
        
        void onTileLoadError(final Exception p0);
    }
    
    public static class DefaultOnStateChangedListener implements OnStateChangedListener
    {
        @Override
        public void onCenterChanged(final PointF pointF, final int n) {
        }
        
        @Override
        public void onScaleChanged(final float n, final int n2) {
        }
    }
    
    public interface OnStateChangedListener
    {
        void onCenterChanged(final PointF p0, final int p1);
        
        void onScaleChanged(final float p0, final int p1);
    }
    
    private static class ScaleAndTranslate
    {
        private float scale;
        private final PointF vTranslate;
        
        private ScaleAndTranslate(final float scale, final PointF vTranslate) {
            this.scale = scale;
            this.vTranslate = vTranslate;
        }
    }
    
    private static class Tile
    {
        private Bitmap bitmap;
        private Rect fileSRect;
        private boolean loading;
        private Rect sRect;
        private int sampleSize;
        private Rect vRect;
        private boolean visible;
    }
    
    private static class TileLoadTask extends AsyncTask<Void, Void, Bitmap>
    {
        private final WeakReference<ImageRegionDecoder> decoderRef;
        private Exception exception;
        private final WeakReference<Tile> tileRef;
        private final WeakReference<SubsamplingScaleImageView> viewRef;
        
        TileLoadTask(final SubsamplingScaleImageView referent, final ImageRegionDecoder referent2, final Tile referent3) {
            this.viewRef = new WeakReference<SubsamplingScaleImageView>(referent);
            this.decoderRef = new WeakReference<ImageRegionDecoder>(referent2);
            this.tileRef = new WeakReference<Tile>(referent3);
            referent3.loading = true;
        }
        
        protected Bitmap doInBackground(Void... array) {
            try {
                array = this.viewRef.get();
                final ImageRegionDecoder imageRegionDecoder = this.decoderRef.get();
                final Tile tile = this.tileRef.get();
                if (imageRegionDecoder != null && tile != null && array != null && imageRegionDecoder.isReady() && tile.visible) {
                    ((SubsamplingScaleImageView)(Object)array).debug("TileLoadTask.doInBackground, tile.sRect=%s, tile.sampleSize=%d", tile.sRect, tile.sampleSize);
                    ((SubsamplingScaleImageView)(Object)array).decoderLock.readLock().lock();
                    try {
                        if (imageRegionDecoder.isReady()) {
                            ((SubsamplingScaleImageView)(Object)array).fileSRect(tile.sRect, tile.fileSRect);
                            if (((SubsamplingScaleImageView)(Object)array).sRegion != null) {
                                tile.fileSRect.offset(((SubsamplingScaleImageView)(Object)array).sRegion.left, ((SubsamplingScaleImageView)(Object)array).sRegion.top);
                            }
                            return imageRegionDecoder.decodeRegion(tile.fileSRect, tile.sampleSize);
                        }
                        tile.loading = false;
                        return null;
                    }
                    finally {
                        ((SubsamplingScaleImageView)(Object)array).decoderLock.readLock().unlock();
                    }
                }
                if (tile != null) {
                    tile.loading = false;
                }
            }
            catch (final OutOfMemoryError cause) {
                Log.e(SubsamplingScaleImageView.TAG, "Failed to decode tile - OutOfMemoryError", (Throwable)cause);
                this.exception = new RuntimeException(cause);
            }
            catch (final Exception exception) {
                Log.e(SubsamplingScaleImageView.TAG, "Failed to decode tile", (Throwable)exception);
                this.exception = exception;
            }
            return null;
        }
        
        protected void onPostExecute(final Bitmap bitmap) {
            final SubsamplingScaleImageView subsamplingScaleImageView = this.viewRef.get();
            final Tile tile = this.tileRef.get();
            if (subsamplingScaleImageView != null && tile != null) {
                if (bitmap != null) {
                    tile.bitmap = bitmap;
                    tile.loading = false;
                    subsamplingScaleImageView.onTileLoaded();
                }
                else if (this.exception != null && subsamplingScaleImageView.onImageEventListener != null) {
                    subsamplingScaleImageView.onImageEventListener.onTileLoadError(this.exception);
                }
            }
        }
    }
    
    private static class TilesInitTask extends AsyncTask<Void, Void, int[]>
    {
        private final WeakReference<Context> contextRef;
        private ImageRegionDecoder decoder;
        private final WeakReference<DecoderFactory<? extends ImageRegionDecoder>> decoderFactoryRef;
        private Exception exception;
        private final Uri source;
        private final WeakReference<SubsamplingScaleImageView> viewRef;
        
        TilesInitTask(final SubsamplingScaleImageView referent, final Context referent2, final DecoderFactory<? extends ImageRegionDecoder> referent3, final Uri source) {
            this.viewRef = new WeakReference<SubsamplingScaleImageView>(referent);
            this.contextRef = new WeakReference<Context>(referent2);
            this.decoderFactoryRef = new WeakReference<DecoderFactory<? extends ImageRegionDecoder>>(referent3);
            this.source = source;
        }
        
        protected int[] doInBackground(final Void... array) {
            try {
                final String string = this.source.toString();
                final Context context = this.contextRef.get();
                final DecoderFactory decoderFactory = this.decoderFactoryRef.get();
                final SubsamplingScaleImageView subsamplingScaleImageView = this.viewRef.get();
                if (context != null && decoderFactory != null && subsamplingScaleImageView != null) {
                    subsamplingScaleImageView.debug("TilesInitTask.doInBackground", new Object[0]);
                    final ImageRegionDecoder decoder = (ImageRegionDecoder)decoderFactory.make();
                    this.decoder = decoder;
                    final Point init = decoder.init(context, this.source);
                    final int x = init.x;
                    final int y = init.y;
                    final int access$5200 = subsamplingScaleImageView.getExifOrientation(context, string);
                    int height = y;
                    int width = x;
                    if (subsamplingScaleImageView.sRegion != null) {
                        subsamplingScaleImageView.sRegion.left = Math.max(0, subsamplingScaleImageView.sRegion.left);
                        subsamplingScaleImageView.sRegion.top = Math.max(0, subsamplingScaleImageView.sRegion.top);
                        subsamplingScaleImageView.sRegion.right = Math.min(x, subsamplingScaleImageView.sRegion.right);
                        subsamplingScaleImageView.sRegion.bottom = Math.min(y, subsamplingScaleImageView.sRegion.bottom);
                        width = subsamplingScaleImageView.sRegion.width();
                        height = subsamplingScaleImageView.sRegion.height();
                    }
                    return new int[] { width, height, access$5200 };
                }
            }
            catch (final Exception exception) {
                Log.e(SubsamplingScaleImageView.TAG, "Failed to initialise bitmap decoder", (Throwable)exception);
                this.exception = exception;
            }
            return null;
        }
        
        protected void onPostExecute(final int[] array) {
            final SubsamplingScaleImageView subsamplingScaleImageView = this.viewRef.get();
            if (subsamplingScaleImageView != null) {
                final ImageRegionDecoder decoder = this.decoder;
                if (decoder != null && array != null && array.length == 3) {
                    subsamplingScaleImageView.onTilesInited(decoder, array[0], array[1], array[2]);
                }
                else if (this.exception != null && subsamplingScaleImageView.onImageEventListener != null) {
                    subsamplingScaleImageView.onImageEventListener.onImageLoadError(this.exception);
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.charts.PieChart;
import java.text.DecimalFormat;

public class PercentFormatter extends ValueFormatter
{
    public DecimalFormat mFormat;
    private PieChart pieChart;
    
    public PercentFormatter() {
        this.mFormat = new DecimalFormat("###,###,##0.0");
    }
    
    public PercentFormatter(final PieChart pieChart) {
        this();
        this.pieChart = pieChart;
    }
    
    @Override
    public String getFormattedValue(final float n) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mFormat.format(n));
        sb.append(" %");
        return sb.toString();
    }
    
    @Override
    public String getPieLabel(final float n, final PieEntry pieEntry) {
        final PieChart pieChart = this.pieChart;
        if (pieChart != null && pieChart.isUsePercentValuesEnabled()) {
            return this.getFormattedValue(n);
        }
        return this.mFormat.format(n);
    }
}

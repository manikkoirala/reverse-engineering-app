// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.BubbleEntry;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.components.AxisBase;

public abstract class ValueFormatter implements IAxisValueFormatter, IValueFormatter
{
    public String getAxisLabel(final float n, final AxisBase axisBase) {
        return this.getFormattedValue(n);
    }
    
    public String getBarLabel(final BarEntry barEntry) {
        return this.getFormattedValue(barEntry.getY());
    }
    
    public String getBarStackedLabel(final float n, final BarEntry barEntry) {
        return this.getFormattedValue(n);
    }
    
    public String getBubbleLabel(final BubbleEntry bubbleEntry) {
        return this.getFormattedValue(bubbleEntry.getSize());
    }
    
    public String getCandleLabel(final CandleEntry candleEntry) {
        return this.getFormattedValue(candleEntry.getHigh());
    }
    
    public String getFormattedValue(final float f) {
        return String.valueOf(f);
    }
    
    @Deprecated
    @Override
    public String getFormattedValue(final float n, final AxisBase axisBase) {
        return this.getFormattedValue(n);
    }
    
    @Deprecated
    @Override
    public String getFormattedValue(final float n, final Entry entry, final int n2, final ViewPortHandler viewPortHandler) {
        return this.getFormattedValue(n);
    }
    
    public String getPieLabel(final float n, final PieEntry pieEntry) {
        return this.getFormattedValue(n);
    }
    
    public String getPointLabel(final Entry entry) {
        return this.getFormattedValue(entry.getY());
    }
    
    public String getRadarLabel(final RadarEntry radarEntry) {
        return this.getFormattedValue(radarEntry.getY());
    }
}

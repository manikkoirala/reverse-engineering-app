// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.formatter;

import java.text.DecimalFormat;

public class LargeValueFormatter extends ValueFormatter
{
    private DecimalFormat mFormat;
    private int mMaxLength;
    private String[] mSuffix;
    private String mText;
    
    public LargeValueFormatter() {
        this.mSuffix = new String[] { "", "k", "m", "b", "t" };
        this.mMaxLength = 5;
        this.mText = "";
        this.mFormat = new DecimalFormat("###E00");
    }
    
    public LargeValueFormatter(final String mText) {
        this();
        this.mText = mText;
    }
    
    private String makePretty(final double number) {
        final String format = this.mFormat.format(number);
        final int numericValue = Character.getNumericValue(format.charAt(format.length() - 1));
        final int numericValue2 = Character.getNumericValue(format.charAt(format.length() - 2));
        final StringBuilder sb = new StringBuilder();
        sb.append(numericValue2);
        sb.append("");
        sb.append(numericValue);
        String s;
        StringBuilder sb2;
        for (s = format.replaceAll("E[0-9][0-9]", this.mSuffix[Integer.valueOf(sb.toString()) / 3]); s.length() > this.mMaxLength || s.matches("[0-9]+\\.[a-z]"); s = sb2.toString()) {
            sb2 = new StringBuilder();
            sb2.append(s.substring(0, s.length() - 2));
            sb2.append(s.substring(s.length() - 1));
        }
        return s;
    }
    
    public int getDecimalDigits() {
        return 0;
    }
    
    @Override
    public String getFormattedValue(final float n) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.makePretty(n));
        sb.append(this.mText);
        return sb.toString();
    }
    
    public void setAppendix(final String mText) {
        this.mText = mText;
    }
    
    public void setMaxLength(final int mMaxLength) {
        this.mMaxLength = mMaxLength;
    }
    
    public void setSuffix(final String[] mSuffix) {
        this.mSuffix = mSuffix;
    }
}

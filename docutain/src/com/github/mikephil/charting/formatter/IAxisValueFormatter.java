// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.components.AxisBase;

@Deprecated
public interface IAxisValueFormatter
{
    @Deprecated
    String getFormattedValue(final float p0, final AxisBase p1);
}

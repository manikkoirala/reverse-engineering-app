// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.data.BarEntry;
import java.text.DecimalFormat;

public class StackedValueFormatter extends ValueFormatter
{
    private boolean mDrawWholeStack;
    private DecimalFormat mFormat;
    private String mSuffix;
    
    public StackedValueFormatter(final boolean mDrawWholeStack, final String mSuffix, final int n) {
        this.mDrawWholeStack = mDrawWholeStack;
        this.mSuffix = mSuffix;
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < n; ++i) {
            if (i == 0) {
                sb.append(".");
            }
            sb.append("0");
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("###,###,###,##0");
        sb2.append(sb.toString());
        this.mFormat = new DecimalFormat(sb2.toString());
    }
    
    @Override
    public String getBarStackedLabel(final float n, final BarEntry barEntry) {
        if (!this.mDrawWholeStack) {
            final float[] yVals = barEntry.getYVals();
            if (yVals != null) {
                if (yVals[yVals.length - 1] == n) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.mFormat.format(barEntry.getY()));
                    sb.append(this.mSuffix);
                    return sb.toString();
                }
                return "";
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.mFormat.format(n));
        sb2.append(this.mSuffix);
        return sb2.toString();
    }
}

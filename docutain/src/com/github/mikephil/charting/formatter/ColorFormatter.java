// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.formatter;

import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.data.Entry;

public interface ColorFormatter
{
    int getColor(final int p0, final Entry p1, final IDataSet p2);
}

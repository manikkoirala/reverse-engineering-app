// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.formatter;

import java.text.DecimalFormat;

public class DefaultAxisValueFormatter extends ValueFormatter
{
    protected int digits;
    protected DecimalFormat mFormat;
    
    public DefaultAxisValueFormatter(final int digits) {
        this.digits = digits;
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digits; ++i) {
            if (i == 0) {
                sb.append(".");
            }
            sb.append("0");
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("###,###,###,##0");
        sb2.append(sb.toString());
        this.mFormat = new DecimalFormat(sb2.toString());
    }
    
    public int getDecimalDigits() {
        return this.digits;
    }
    
    @Override
    public String getFormattedValue(final float n) {
        return this.mFormat.format(n);
    }
}

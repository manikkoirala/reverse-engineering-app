// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.highlight;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;

public class BarHighlighter extends ChartHighlighter<BarDataProvider>
{
    public BarHighlighter(final BarDataProvider barDataProvider) {
        super(barDataProvider);
    }
    
    protected int getClosestStackIndex(final Range[] array, final float n) {
        int n3;
        final int n2 = n3 = 0;
        if (array != null) {
            if (array.length == 0) {
                n3 = n2;
            }
            else {
                final int length = array.length;
                int i = 0;
                int n4 = 0;
                while (i < length) {
                    if (array[i].contains(n)) {
                        return n4;
                    }
                    ++n4;
                    ++i;
                }
                final int max = Math.max(array.length - 1, 0);
                n3 = n2;
                if (n > array[max].to) {
                    n3 = max;
                }
            }
        }
        return n3;
    }
    
    @Override
    protected BarLineScatterCandleBubbleData getData() {
        return ((BarDataProvider)this.mChart).getBarData();
    }
    
    @Override
    protected float getDistance(final float n, final float n2, final float n3, final float n4) {
        return Math.abs(n - n3);
    }
    
    @Override
    public Highlight getHighlight(final float n, final float n2) {
        final Highlight highlight = super.getHighlight(n, n2);
        if (highlight == null) {
            return null;
        }
        final MPPointD valsForTouch = this.getValsForTouch(n, n2);
        final IBarDataSet set = ((BarDataProvider)this.mChart).getBarData().getDataSetByIndex(highlight.getDataSetIndex());
        if (set.isStacked()) {
            return this.getStackedHighlight(highlight, set, (float)valsForTouch.x, (float)valsForTouch.y);
        }
        MPPointD.recycleInstance(valsForTouch);
        return highlight;
    }
    
    public Highlight getStackedHighlight(Highlight highlight, final IBarDataSet set, final float n, final float n2) {
        final BarEntry barEntry = set.getEntryForXValue(n, n2);
        if (barEntry == null) {
            return null;
        }
        if (barEntry.getYVals() == null) {
            return highlight;
        }
        final Range[] ranges = barEntry.getRanges();
        if (ranges.length > 0) {
            final int closestStackIndex = this.getClosestStackIndex(ranges, n2);
            final MPPointD pixelForValues = this.mChart.getTransformer(set.getAxisDependency()).getPixelForValues(highlight.getX(), ranges[closestStackIndex].to);
            highlight = new Highlight(barEntry.getX(), barEntry.getY(), (float)pixelForValues.x, (float)pixelForValues.y, highlight.getDataSetIndex(), closestStackIndex, highlight.getAxis());
            MPPointD.recycleInstance(pixelForValues);
            return highlight;
        }
        return null;
    }
}

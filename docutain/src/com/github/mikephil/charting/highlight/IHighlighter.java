// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.highlight;

public interface IHighlighter
{
    Highlight getHighlight(final float p0, final float p1);
}

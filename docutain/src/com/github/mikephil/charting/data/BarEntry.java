// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.data;

import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.highlight.Range;

public class BarEntry extends Entry
{
    private float mNegativeSum;
    private float mPositiveSum;
    private Range[] mRanges;
    private float[] mYVals;
    
    public BarEntry(final float n, final float n2) {
        super(n, n2);
    }
    
    public BarEntry(final float n, final float n2, final Drawable drawable) {
        super(n, n2, drawable);
    }
    
    public BarEntry(final float n, final float n2, final Drawable drawable, final Object o) {
        super(n, n2, drawable, o);
    }
    
    public BarEntry(final float n, final float n2, final Object o) {
        super(n, n2, o);
    }
    
    public BarEntry(final float n, final float[] myVals) {
        super(n, calcSum(myVals));
        this.mYVals = myVals;
        this.calcPosNegSum();
        this.calcRanges();
    }
    
    public BarEntry(final float n, final float[] myVals, final Drawable drawable) {
        super(n, calcSum(myVals), drawable);
        this.mYVals = myVals;
        this.calcPosNegSum();
        this.calcRanges();
    }
    
    public BarEntry(final float n, final float[] myVals, final Drawable drawable, final Object o) {
        super(n, calcSum(myVals), drawable, o);
        this.mYVals = myVals;
        this.calcPosNegSum();
        this.calcRanges();
    }
    
    public BarEntry(final float n, final float[] myVals, final Object o) {
        super(n, calcSum(myVals), o);
        this.mYVals = myVals;
        this.calcPosNegSum();
        this.calcRanges();
    }
    
    private void calcPosNegSum() {
        final float[] myVals = this.mYVals;
        if (myVals == null) {
            this.mNegativeSum = 0.0f;
            this.mPositiveSum = 0.0f;
            return;
        }
        final int length = myVals.length;
        int i = 0;
        float mNegativeSum = 0.0f;
        float mPositiveSum = 0.0f;
        while (i < length) {
            final float a = myVals[i];
            if (a <= 0.0f) {
                mNegativeSum += Math.abs(a);
            }
            else {
                mPositiveSum += a;
            }
            ++i;
        }
        this.mNegativeSum = mNegativeSum;
        this.mPositiveSum = mPositiveSum;
    }
    
    private static float calcSum(final float[] array) {
        float n = 0.0f;
        if (array == null) {
            return 0.0f;
        }
        for (int length = array.length, i = 0; i < length; ++i) {
            n += array[i];
        }
        return n;
    }
    
    protected void calcRanges() {
        final float[] yVals = this.getYVals();
        if (yVals != null) {
            if (yVals.length != 0) {
                this.mRanges = new Range[yVals.length];
                float n = -this.getNegativeSum();
                int n2 = 0;
                float n3 = 0.0f;
                while (true) {
                    final Range[] mRanges = this.mRanges;
                    if (n2 >= mRanges.length) {
                        break;
                    }
                    final float n4 = yVals[n2];
                    if (n4 < 0.0f) {
                        final float n5 = n - n4;
                        mRanges[n2] = new Range(n, n5);
                        n = n5;
                    }
                    else {
                        final float n6 = n4 + n3;
                        mRanges[n2] = new Range(n3, n6);
                        n3 = n6;
                    }
                    ++n2;
                }
            }
        }
    }
    
    @Override
    public BarEntry copy() {
        final BarEntry barEntry = new BarEntry(this.getX(), this.getY(), this.getData());
        barEntry.setVals(this.mYVals);
        return barEntry;
    }
    
    @Deprecated
    public float getBelowSum(final int n) {
        return this.getSumBelow(n);
    }
    
    public float getNegativeSum() {
        return this.mNegativeSum;
    }
    
    public float getPositiveSum() {
        return this.mPositiveSum;
    }
    
    public Range[] getRanges() {
        return this.mRanges;
    }
    
    public float getSumBelow(final int n) {
        final float[] myVals = this.mYVals;
        float n2 = 0.0f;
        if (myVals == null) {
            return 0.0f;
        }
        for (int n3 = myVals.length - 1; n3 > n && n3 >= 0; --n3) {
            n2 += this.mYVals[n3];
        }
        return n2;
    }
    
    @Override
    public float getY() {
        return super.getY();
    }
    
    public float[] getYVals() {
        return this.mYVals;
    }
    
    public boolean isStacked() {
        return this.mYVals != null;
    }
    
    public void setVals(final float[] myVals) {
        this.setY(calcSum(myVals));
        this.mYVals = myVals;
        this.calcPosNegSum();
        this.calcRanges();
    }
}

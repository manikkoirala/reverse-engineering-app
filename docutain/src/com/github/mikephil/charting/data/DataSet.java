// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.data;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public abstract class DataSet<T extends Entry> extends BaseDataSet<T>
{
    protected List<T> mValues;
    protected float mXMax;
    protected float mXMin;
    protected float mYMax;
    protected float mYMin;
    
    public DataSet(final List<T> mValues, final String s) {
        super(s);
        this.mYMax = -3.4028235E38f;
        this.mYMin = Float.MAX_VALUE;
        this.mXMax = -3.4028235E38f;
        this.mXMin = Float.MAX_VALUE;
        this.mValues = mValues;
        if (mValues == null) {
            this.mValues = new ArrayList<T>();
        }
        this.calcMinMax();
    }
    
    @Override
    public boolean addEntry(final T t) {
        if (t == null) {
            return false;
        }
        List<T> values;
        if ((values = this.getValues()) == null) {
            values = new ArrayList<T>();
        }
        this.calcMinMax(t);
        return values.add(t);
    }
    
    @Override
    public void addEntryOrdered(final T t) {
        if (t == null) {
            return;
        }
        if (this.mValues == null) {
            this.mValues = new ArrayList<T>();
        }
        this.calcMinMax(t);
        if (this.mValues.size() > 0) {
            final List<T> mValues = this.mValues;
            if (((T)mValues.get(mValues.size() - 1)).getX() > t.getX()) {
                this.mValues.add(this.getEntryIndex(t.getX(), t.getY(), Rounding.UP), t);
                return;
            }
        }
        this.mValues.add(t);
    }
    
    @Override
    public void calcMinMax() {
        final List<T> mValues = this.mValues;
        if (mValues != null) {
            if (!mValues.isEmpty()) {
                this.mYMax = -3.4028235E38f;
                this.mYMin = Float.MAX_VALUE;
                this.mXMax = -3.4028235E38f;
                this.mXMin = Float.MAX_VALUE;
                final Iterator<T> iterator = this.mValues.iterator();
                while (iterator.hasNext()) {
                    this.calcMinMax(iterator.next());
                }
            }
        }
    }
    
    protected void calcMinMax(final T t) {
        if (t == null) {
            return;
        }
        this.calcMinMaxX(t);
        this.calcMinMaxY(t);
    }
    
    protected void calcMinMaxX(final T t) {
        if (t.getX() < this.mXMin) {
            this.mXMin = t.getX();
        }
        if (t.getX() > this.mXMax) {
            this.mXMax = t.getX();
        }
    }
    
    @Override
    public void calcMinMaxY(final float n, final float n2) {
        final List<T> mValues = this.mValues;
        if (mValues != null) {
            if (!mValues.isEmpty()) {
                this.mYMax = -3.4028235E38f;
                this.mYMin = Float.MAX_VALUE;
                for (int i = this.getEntryIndex(n, Float.NaN, Rounding.DOWN); i <= this.getEntryIndex(n2, Float.NaN, Rounding.UP); ++i) {
                    this.calcMinMaxY(this.mValues.get(i));
                }
            }
        }
    }
    
    protected void calcMinMaxY(final T t) {
        if (t.getY() < this.mYMin) {
            this.mYMin = t.getY();
        }
        if (t.getY() > this.mYMax) {
            this.mYMax = t.getY();
        }
    }
    
    @Override
    public void clear() {
        this.mValues.clear();
        this.notifyDataSetChanged();
    }
    
    public abstract DataSet<T> copy();
    
    protected void copy(final DataSet set) {
        super.copy(set);
    }
    
    @Override
    public List<T> getEntriesForXValue(final float n) {
        final ArrayList list = new ArrayList();
        int n2 = this.mValues.size() - 1;
        int i = 0;
        while (i <= n2) {
            final int n3 = (n2 + i) / 2;
            final Entry entry = this.mValues.get(n3);
            if (n == entry.getX()) {
                int j;
                for (j = n3; j > 0 && this.mValues.get(j - 1).getX() == n; --j) {}
                while (j < this.mValues.size()) {
                    final Entry entry2 = this.mValues.get(j);
                    if (entry2.getX() != n) {
                        break;
                    }
                    list.add(entry2);
                    ++j;
                }
                break;
            }
            if (n > entry.getX()) {
                i = n3 + 1;
            }
            else {
                n2 = n3 - 1;
            }
        }
        return list;
    }
    
    @Override
    public int getEntryCount() {
        return this.mValues.size();
    }
    
    @Override
    public T getEntryForIndex(final int n) {
        return this.mValues.get(n);
    }
    
    @Override
    public T getEntryForXValue(final float n, final float n2) {
        return this.getEntryForXValue(n, n2, Rounding.CLOSEST);
    }
    
    @Override
    public T getEntryForXValue(final float n, final float n2, final Rounding rounding) {
        final int entryIndex = this.getEntryIndex(n, n2, rounding);
        if (entryIndex > -1) {
            return this.mValues.get(entryIndex);
        }
        return null;
    }
    
    @Override
    public int getEntryIndex(float y, final float v, final Rounding rounding) {
        final List<T> mValues = this.mValues;
        if (mValues != null && !mValues.isEmpty()) {
            int i = 0;
            int n = this.mValues.size() - 1;
            while (i < n) {
                final int n2 = (i + n) / 2;
                final float a = this.mValues.get(n2).getX() - y;
                final List<T> mValues2 = this.mValues;
                final int n3 = n2 + 1;
                final float x = mValues2.get(n3).getX();
                final float abs = Math.abs(a);
                final float abs2 = Math.abs(x - y);
                Label_0130: {
                    if (abs2 >= abs) {
                        if (abs >= abs2) {
                            final double n4 = a;
                            if (n4 < 0.0) {
                                if (n4 < 0.0) {
                                    break Label_0130;
                                }
                                continue;
                            }
                        }
                        n = n2;
                        continue;
                    }
                }
                i = n3;
            }
            int n5;
            if ((n5 = n) != -1) {
                final float x2 = this.mValues.get(n).getX();
                int n6;
                if (rounding == Rounding.UP) {
                    n6 = n;
                    if (x2 < y && (n6 = n) < this.mValues.size() - 1) {
                        n6 = n + 1;
                    }
                }
                else {
                    n6 = n;
                    if (rounding == Rounding.DOWN) {
                        n6 = n;
                        if (x2 > y && (n6 = n) > 0) {
                            n6 = n - 1;
                        }
                    }
                }
                n5 = n6;
                if (!Float.isNaN(v)) {
                    while (n6 > 0 && this.mValues.get(n6 - 1).getX() == x2) {
                        --n6;
                    }
                    y = this.mValues.get(n6).getY();
                Label_0407:
                    while (true) {
                        int n7 = n6;
                        Entry entry;
                        int n8;
                        do {
                            n8 = n7 + 1;
                            if (n8 >= this.mValues.size()) {
                                break Label_0407;
                            }
                            entry = this.mValues.get(n8);
                            if (entry.getX() != x2) {
                                break Label_0407;
                            }
                            n7 = n8;
                        } while (Math.abs(entry.getY() - v) >= Math.abs(y - v));
                        y = v;
                        n6 = n8;
                    }
                    n5 = n6;
                }
            }
            return n5;
        }
        return -1;
    }
    
    @Override
    public int getEntryIndex(final Entry entry) {
        return this.mValues.indexOf(entry);
    }
    
    public List<T> getValues() {
        return this.mValues;
    }
    
    @Override
    public float getXMax() {
        return this.mXMax;
    }
    
    @Override
    public float getXMin() {
        return this.mXMin;
    }
    
    @Override
    public float getYMax() {
        return this.mYMax;
    }
    
    @Override
    public float getYMin() {
        return this.mYMin;
    }
    
    @Override
    public boolean removeEntry(final T t) {
        if (t == null) {
            return false;
        }
        final List<T> mValues = this.mValues;
        if (mValues == null) {
            return false;
        }
        final boolean remove = mValues.remove(t);
        if (remove) {
            this.calcMinMax();
        }
        return remove;
    }
    
    public void setValues(final List<T> mValues) {
        this.mValues = mValues;
        this.notifyDataSetChanged();
    }
    
    public String toSimpleString() {
        final StringBuffer sb = new StringBuffer();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("DataSet, label: ");
        String label;
        if (this.getLabel() == null) {
            label = "";
        }
        else {
            label = this.getLabel();
        }
        sb2.append(label);
        sb2.append(", entries: ");
        sb2.append(this.mValues.size());
        sb2.append("\n");
        sb.append(sb2.toString());
        return sb.toString();
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(this.toSimpleString());
        for (int i = 0; i < this.mValues.size(); ++i) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.mValues.get(i).toString());
            sb2.append(" ");
            sb.append(sb2.toString());
        }
        return sb.toString();
    }
    
    public enum Rounding
    {
        private static final Rounding[] $VALUES;
        
        CLOSEST, 
        DOWN, 
        UP;
    }
}

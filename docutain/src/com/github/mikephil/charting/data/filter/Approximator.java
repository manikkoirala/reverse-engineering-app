// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.data.filter;

import java.util.Arrays;

public class Approximator
{
    float[] concat(final float[]... array) {
        final int length = array.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            n += array[i].length;
            ++i;
        }
        final float[] array2 = new float[n];
        final int length2 = array.length;
        int j = 0;
        int n2 = 0;
        while (j < length2) {
            final float[] array3 = array[j];
            for (int length3 = array3.length, k = 0; k < length3; ++k) {
                array2[n2] = array3[k];
                ++n2;
            }
            ++j;
        }
        return array2;
    }
    
    public float[] reduceWithDouglasPeucker(float[] reduceWithDouglasPeucker, final float n) {
        final Line line = new Line(reduceWithDouglasPeucker[0], reduceWithDouglasPeucker[1], reduceWithDouglasPeucker[reduceWithDouglasPeucker.length - 2], reduceWithDouglasPeucker[reduceWithDouglasPeucker.length - 1]);
        float n2 = 0.0f;
        int i = 2;
        int from = 0;
        while (i < reduceWithDouglasPeucker.length - 2) {
            final float distance = line.distance(reduceWithDouglasPeucker[i], reduceWithDouglasPeucker[i + 1]);
            float n3 = n2;
            if (distance > n2) {
                from = i;
                n3 = distance;
            }
            i += 2;
            n2 = n3;
        }
        if (n2 > n) {
            final float[] reduceWithDouglasPeucker2 = this.reduceWithDouglasPeucker(Arrays.copyOfRange(reduceWithDouglasPeucker, 0, from + 2), n);
            reduceWithDouglasPeucker = this.reduceWithDouglasPeucker(Arrays.copyOfRange(reduceWithDouglasPeucker, from, reduceWithDouglasPeucker.length), n);
            return this.concat(new float[][] { reduceWithDouglasPeucker2, Arrays.copyOfRange(reduceWithDouglasPeucker, 2, reduceWithDouglasPeucker.length) });
        }
        return line.getPoints();
    }
    
    private class Line
    {
        private float dx;
        private float dy;
        private float exsy;
        private float length;
        private float[] points;
        private float sxey;
        final Approximator this$0;
        
        public Line(final Approximator this$0, final float n, final float n2, final float n3, final float n4) {
            this.this$0 = this$0;
            final float dx = n - n3;
            this.dx = dx;
            final float dy = n2 - n4;
            this.dy = dy;
            this.sxey = n * n4;
            this.exsy = n3 * n2;
            this.length = (float)Math.sqrt(dx * dx + dy * dy);
            this.points = new float[] { n, n2, n3, n4 };
        }
        
        public float distance(final float n, final float n2) {
            return Math.abs(this.dy * n - this.dx * n2 + this.sxey - this.exsy) / this.length;
        }
        
        public float[] getPoints() {
            return this.points;
        }
    }
}

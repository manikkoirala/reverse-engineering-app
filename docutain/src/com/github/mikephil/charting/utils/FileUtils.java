// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.utils;

import java.util.Iterator;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import android.util.Log;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.io.File;
import android.os.Environment;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.BarEntry;
import java.util.List;
import android.content.res.AssetManager;

public class FileUtils
{
    private static final String LOG = "MPChart-FileUtils";
    
    public static List<BarEntry> loadBarEntriesFromAssets(final AssetManager p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/util/ArrayList.<init>:()V
        //     7: astore          6
        //     9: aconst_null    
        //    10: astore          4
        //    12: aconst_null    
        //    13: astore          5
        //    15: aload           5
        //    17: astore_2       
        //    18: new             Ljava/io/BufferedReader;
        //    21: astore_3       
        //    22: aload           5
        //    24: astore_2       
        //    25: new             Ljava/io/InputStreamReader;
        //    28: astore          7
        //    30: aload           5
        //    32: astore_2       
        //    33: aload           7
        //    35: aload_0        
        //    36: aload_1        
        //    37: invokevirtual   android/content/res/AssetManager.open:(Ljava/lang/String;)Ljava/io/InputStream;
        //    40: ldc             "UTF-8"
        //    42: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    45: aload           5
        //    47: astore_2       
        //    48: aload_3        
        //    49: aload           7
        //    51: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    54: aload_3        
        //    55: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    58: astore_0       
        //    59: aload_0        
        //    60: ifnull          107
        //    63: aload_0        
        //    64: ldc             "#"
        //    66: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //    69: astore_1       
        //    70: new             Lcom/github/mikephil/charting/data/BarEntry;
        //    73: astore_0       
        //    74: aload_0        
        //    75: aload_1        
        //    76: iconst_1       
        //    77: aaload         
        //    78: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //    81: aload_1        
        //    82: iconst_0       
        //    83: aaload         
        //    84: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //    87: invokespecial   com/github/mikephil/charting/data/BarEntry.<init>:(FF)V
        //    90: aload           6
        //    92: aload_0        
        //    93: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //    98: pop            
        //    99: aload_3        
        //   100: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   103: astore_0       
        //   104: goto            59
        //   107: aload_3        
        //   108: invokevirtual   java/io/BufferedReader.close:()V
        //   111: goto            168
        //   114: astore_0       
        //   115: aload_3        
        //   116: astore_2       
        //   117: goto            171
        //   120: astore_1       
        //   121: aload_3        
        //   122: astore_0       
        //   123: goto            134
        //   126: astore_0       
        //   127: goto            171
        //   130: astore_1       
        //   131: aload           4
        //   133: astore_0       
        //   134: aload_0        
        //   135: astore_2       
        //   136: ldc             "MPChart-FileUtils"
        //   138: aload_1        
        //   139: invokevirtual   java/io/IOException.toString:()Ljava/lang/String;
        //   142: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   145: pop            
        //   146: aload_0        
        //   147: ifnull          168
        //   150: aload_0        
        //   151: invokevirtual   java/io/BufferedReader.close:()V
        //   154: goto            168
        //   157: astore_0       
        //   158: ldc             "MPChart-FileUtils"
        //   160: aload_0        
        //   161: invokevirtual   java/io/IOException.toString:()Ljava/lang/String;
        //   164: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   167: pop            
        //   168: aload           6
        //   170: areturn        
        //   171: aload_2        
        //   172: ifnull          193
        //   175: aload_2        
        //   176: invokevirtual   java/io/BufferedReader.close:()V
        //   179: goto            193
        //   182: astore_1       
        //   183: ldc             "MPChart-FileUtils"
        //   185: aload_1        
        //   186: invokevirtual   java/io/IOException.toString:()Ljava/lang/String;
        //   189: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   192: pop            
        //   193: aload_0        
        //   194: athrow         
        //    Signature:
        //  (Landroid/content/res/AssetManager;Ljava/lang/String;)Ljava/util/List<Lcom/github/mikephil/charting/data/BarEntry;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  18     22     130    134    Ljava/io/IOException;
        //  18     22     126    130    Any
        //  25     30     130    134    Ljava/io/IOException;
        //  25     30     126    130    Any
        //  33     45     130    134    Ljava/io/IOException;
        //  33     45     126    130    Any
        //  48     54     130    134    Ljava/io/IOException;
        //  48     54     126    130    Any
        //  54     59     120    126    Ljava/io/IOException;
        //  54     59     114    120    Any
        //  63     104    120    126    Ljava/io/IOException;
        //  63     104    114    120    Any
        //  107    111    157    168    Ljava/io/IOException;
        //  136    146    126    130    Any
        //  150    154    157    168    Ljava/io/IOException;
        //  175    179    182    193    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0107:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static List<Entry> loadEntriesFromAssets(final AssetManager p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/util/ArrayList.<init>:()V
        //     7: astore          8
        //     9: aconst_null    
        //    10: astore          6
        //    12: aconst_null    
        //    13: astore          7
        //    15: aload           7
        //    17: astore          4
        //    19: new             Ljava/io/BufferedReader;
        //    22: astore          5
        //    24: aload           7
        //    26: astore          4
        //    28: new             Ljava/io/InputStreamReader;
        //    31: astore          9
        //    33: aload           7
        //    35: astore          4
        //    37: aload           9
        //    39: aload_0        
        //    40: aload_1        
        //    41: invokevirtual   android/content/res/AssetManager.open:(Ljava/lang/String;)Ljava/io/InputStream;
        //    44: ldc             "UTF-8"
        //    46: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    49: aload           7
        //    51: astore          4
        //    53: aload           5
        //    55: aload           9
        //    57: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    60: aload           5
        //    62: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    65: astore_0       
        //    66: aload_0        
        //    67: ifnull          188
        //    70: aload_0        
        //    71: ldc             "#"
        //    73: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //    76: astore_0       
        //    77: aload_0        
        //    78: arraylength    
        //    79: istore_3       
        //    80: iconst_0       
        //    81: istore_2       
        //    82: iload_3        
        //    83: iconst_2       
        //    84: if_icmpgt       119
        //    87: new             Lcom/github/mikephil/charting/data/Entry;
        //    90: astore_1       
        //    91: aload_1        
        //    92: aload_0        
        //    93: iconst_1       
        //    94: aaload         
        //    95: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //    98: aload_0        
        //    99: iconst_0       
        //   100: aaload         
        //   101: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   104: invokespecial   com/github/mikephil/charting/data/Entry.<init>:(FF)V
        //   107: aload           8
        //   109: aload_1        
        //   110: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   115: pop            
        //   116: goto            179
        //   119: aload_0        
        //   120: arraylength    
        //   121: iconst_1       
        //   122: isub           
        //   123: istore_3       
        //   124: iload_3        
        //   125: newarray        F
        //   127: astore          4
        //   129: iload_2        
        //   130: iload_3        
        //   131: if_icmpge       150
        //   134: aload           4
        //   136: iload_2        
        //   137: aload_0        
        //   138: iload_2        
        //   139: aaload         
        //   140: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   143: fastore        
        //   144: iinc            2, 1
        //   147: goto            129
        //   150: new             Lcom/github/mikephil/charting/data/BarEntry;
        //   153: astore_1       
        //   154: aload_1        
        //   155: aload_0        
        //   156: aload_0        
        //   157: arraylength    
        //   158: iconst_1       
        //   159: isub           
        //   160: aaload         
        //   161: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   164: i2f            
        //   165: aload           4
        //   167: invokespecial   com/github/mikephil/charting/data/BarEntry.<init>:(F[F)V
        //   170: aload           8
        //   172: aload_1        
        //   173: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   178: pop            
        //   179: aload           5
        //   181: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   184: astore_0       
        //   185: goto            66
        //   188: aload           5
        //   190: invokevirtual   java/io/BufferedReader.close:()V
        //   193: goto            254
        //   196: astore_0       
        //   197: aload           5
        //   199: astore          4
        //   201: goto            257
        //   204: astore_1       
        //   205: aload           5
        //   207: astore_0       
        //   208: goto            219
        //   211: astore_0       
        //   212: goto            257
        //   215: astore_1       
        //   216: aload           6
        //   218: astore_0       
        //   219: aload_0        
        //   220: astore          4
        //   222: ldc             "MPChart-FileUtils"
        //   224: aload_1        
        //   225: invokevirtual   java/io/IOException.toString:()Ljava/lang/String;
        //   228: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   231: pop            
        //   232: aload_0        
        //   233: ifnull          254
        //   236: aload_0        
        //   237: invokevirtual   java/io/BufferedReader.close:()V
        //   240: goto            254
        //   243: astore_0       
        //   244: ldc             "MPChart-FileUtils"
        //   246: aload_0        
        //   247: invokevirtual   java/io/IOException.toString:()Ljava/lang/String;
        //   250: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   253: pop            
        //   254: aload           8
        //   256: areturn        
        //   257: aload           4
        //   259: ifnull          281
        //   262: aload           4
        //   264: invokevirtual   java/io/BufferedReader.close:()V
        //   267: goto            281
        //   270: astore_1       
        //   271: ldc             "MPChart-FileUtils"
        //   273: aload_1        
        //   274: invokevirtual   java/io/IOException.toString:()Ljava/lang/String;
        //   277: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   280: pop            
        //   281: aload_0        
        //   282: athrow         
        //    Signature:
        //  (Landroid/content/res/AssetManager;Ljava/lang/String;)Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  19     24     215    219    Ljava/io/IOException;
        //  19     24     211    215    Any
        //  28     33     215    219    Ljava/io/IOException;
        //  28     33     211    215    Any
        //  37     49     215    219    Ljava/io/IOException;
        //  37     49     211    215    Any
        //  53     60     215    219    Ljava/io/IOException;
        //  53     60     211    215    Any
        //  60     66     204    211    Ljava/io/IOException;
        //  60     66     196    204    Any
        //  70     80     204    211    Ljava/io/IOException;
        //  70     80     196    204    Any
        //  87     116    204    211    Ljava/io/IOException;
        //  87     116    196    204    Any
        //  119    129    204    211    Ljava/io/IOException;
        //  119    129    196    204    Any
        //  134    144    204    211    Ljava/io/IOException;
        //  134    144    196    204    Any
        //  150    179    204    211    Ljava/io/IOException;
        //  150    179    196    204    Any
        //  179    185    204    211    Ljava/io/IOException;
        //  179    185    196    204    Any
        //  188    193    243    254    Ljava/io/IOException;
        //  222    232    211    215    Any
        //  236    240    243    254    Ljava/io/IOException;
        //  262    267    270    281    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0188:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static List<Entry> loadEntriesFromFile(String child) {
        final File file = new File(Environment.getExternalStorageDirectory(), child);
        child = (String)new ArrayList();
        try {
            final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while (true) {
                final String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                final String[] split = line.split("#");
                final int length = split.length;
                int i = 0;
                if (length <= 2) {
                    ((List<Entry>)child).add(new Entry(Float.parseFloat(split[0]), (float)Integer.parseInt(split[1])));
                }
                else {
                    final int n = split.length - 1;
                    final float[] array = new float[n];
                    while (i < n) {
                        array[i] = Float.parseFloat(split[i]);
                        ++i;
                    }
                    ((List<BarEntry>)child).add(new BarEntry((float)Integer.parseInt(split[split.length - 1]), array));
                }
            }
        }
        catch (final IOException ex) {
            Log.e("MPChart-FileUtils", ex.toString());
        }
        return (List<Entry>)child;
    }
    
    public static void saveToSdCard(final List<Entry> list, final String child) {
        final File file = new File(Environment.getExternalStorageDirectory(), child);
        if (!file.exists()) {
            try {
                file.createNewFile();
            }
            catch (final IOException ex) {
                Log.e("MPChart-FileUtils", ex.toString());
            }
        }
        try {
            final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            for (final Entry entry : list) {
                final StringBuilder sb = new StringBuilder();
                sb.append(entry.getY());
                sb.append("#");
                sb.append(entry.getX());
                bufferedWriter.append((CharSequence)sb.toString());
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        }
        catch (final IOException ex2) {
            Log.e("MPChart-FileUtils", ex2.toString());
        }
    }
}

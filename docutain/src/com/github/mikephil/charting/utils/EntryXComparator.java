// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.utils;

import com.github.mikephil.charting.data.Entry;
import java.util.Comparator;

public class EntryXComparator implements Comparator<Entry>
{
    @Override
    public int compare(final Entry entry, final Entry entry2) {
        final float n = fcmpl(entry.getX() - entry2.getX(), 0.0f);
        if (n == 0) {
            return 0;
        }
        if (n > 0) {
            return 1;
        }
        return -1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.model;

public class GradientColor
{
    private int endColor;
    private int startColor;
    
    public GradientColor(final int startColor, final int endColor) {
        this.startColor = startColor;
        this.endColor = endColor;
    }
    
    public int getEndColor() {
        return this.endColor;
    }
    
    public int getStartColor() {
        return this.startColor;
    }
    
    public void setEndColor(final int endColor) {
        this.endColor = endColor;
    }
    
    public void setStartColor(final int startColor) {
        this.startColor = startColor;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.charts;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.TimeInterpolator;
import android.animation.ObjectAnimator;
import com.github.mikephil.charting.animation.Easing;
import android.view.View;
import android.view.MotionEvent;
import android.graphics.RectF;
import com.github.mikephil.charting.listener.PieRadarChartTouchListener;
import com.github.mikephil.charting.components.XAxis;
import android.util.Log;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.components.Legend;
import android.util.AttributeSet;
import android.content.Context;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.data.ChartData;

public abstract class PieRadarChartBase<T extends ChartData<? extends IDataSet<? extends Entry>>> extends Chart<T>
{
    protected float mMinOffset;
    private float mRawRotationAngle;
    protected boolean mRotateEnabled;
    private float mRotationAngle;
    
    public PieRadarChartBase(final Context context) {
        super(context);
        this.mRotationAngle = 270.0f;
        this.mRawRotationAngle = 270.0f;
        this.mRotateEnabled = true;
        this.mMinOffset = 0.0f;
    }
    
    public PieRadarChartBase(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRotationAngle = 270.0f;
        this.mRawRotationAngle = 270.0f;
        this.mRotateEnabled = true;
        this.mMinOffset = 0.0f;
    }
    
    public PieRadarChartBase(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mRotationAngle = 270.0f;
        this.mRawRotationAngle = 270.0f;
        this.mRotateEnabled = true;
        this.mMinOffset = 0.0f;
    }
    
    @Override
    protected void calcMinMax() {
    }
    
    public void calculateOffsets() {
        final Legend mLegend = this.mLegend;
        float n = 0.0f;
        final float n2 = 0.0f;
        final float n3 = 0.0f;
        final float n4 = 0.0f;
        final float n5 = 0.0f;
        float n18;
        float n20;
        float n21;
        float n22;
        if (mLegend != null && this.mLegend.isEnabled() && !this.mLegend.isDrawInsideEnabled()) {
            final float min = Math.min(this.mLegend.mNeededWidth, this.mViewPortHandler.getChartWidth() * this.mLegend.getMaxSizePercent());
            final int n6 = PieRadarChartBase$2.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendOrientation[this.mLegend.getOrientation().ordinal()];
            float n8 = 0.0f;
            float n14 = 0.0f;
            float n15 = 0.0f;
            Label_0621: {
                float min3 = 0.0f;
                Label_0221: {
                    while (true) {
                        float min4 = 0.0f;
                        Label_0211: {
                            Label_0203: {
                                Label_0201: {
                                    if (n6 != 1) {
                                        if (n6 != 2) {
                                            n = n5;
                                        }
                                        else {
                                            if (this.mLegend.getVerticalAlignment() != Legend.LegendVerticalAlignment.TOP) {
                                                n = n5;
                                                if (this.mLegend.getVerticalAlignment() != Legend.LegendVerticalAlignment.BOTTOM) {
                                                    break Label_0201;
                                                }
                                            }
                                            final float min2 = Math.min(this.mLegend.mNeededHeight + this.getRequiredLegendOffset(), this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent());
                                            final int n7 = PieRadarChartBase$2.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendVerticalAlignment[this.mLegend.getVerticalAlignment().ordinal()];
                                            min3 = min2;
                                            if (n7 == 1) {
                                                break Label_0221;
                                            }
                                            min4 = min2;
                                            if (n7 == 2) {
                                                break Label_0211;
                                            }
                                            n = n5;
                                        }
                                    }
                                    else {
                                        if (this.mLegend.getHorizontalAlignment() != Legend.LegendHorizontalAlignment.LEFT && this.mLegend.getHorizontalAlignment() != Legend.LegendHorizontalAlignment.RIGHT) {
                                            n8 = 0.0f;
                                        }
                                        else if (this.mLegend.getVerticalAlignment() == Legend.LegendVerticalAlignment.CENTER) {
                                            n8 = min + Utils.convertDpToPixel(13.0f);
                                        }
                                        else {
                                            final float n9 = min + Utils.convertDpToPixel(8.0f);
                                            final float mNeededHeight = this.mLegend.mNeededHeight;
                                            final float mTextHeightMax = this.mLegend.mTextHeightMax;
                                            final MPPointF center = this.getCenter();
                                            float n10;
                                            if (this.mLegend.getHorizontalAlignment() == Legend.LegendHorizontalAlignment.RIGHT) {
                                                n10 = this.getWidth() - n9 + 15.0f;
                                            }
                                            else {
                                                n10 = n9 - 15.0f;
                                            }
                                            final float n11 = mNeededHeight + mTextHeightMax + 15.0f;
                                            final float distanceToCenter = this.distanceToCenter(n10, n11);
                                            final MPPointF position = this.getPosition(center, this.getRadius(), this.getAngleForPoint(n10, n11));
                                            final float distanceToCenter2 = this.distanceToCenter(position.x, position.y);
                                            final float convertDpToPixel = Utils.convertDpToPixel(5.0f);
                                            if (n11 >= center.y && this.getHeight() - n9 > this.getWidth()) {
                                                n8 = n9;
                                            }
                                            else if (distanceToCenter < distanceToCenter2) {
                                                n8 = convertDpToPixel + (distanceToCenter2 - distanceToCenter);
                                            }
                                            else {
                                                n8 = 0.0f;
                                            }
                                            MPPointF.recycleInstance(center);
                                            MPPointF.recycleInstance(position);
                                        }
                                        final int n12 = PieRadarChartBase$2.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendHorizontalAlignment[this.mLegend.getHorizontalAlignment().ordinal()];
                                        if (n12 != 1) {
                                            if (n12 == 2) {
                                                break Label_0203;
                                            }
                                            if (n12 == 3) {
                                                final int n13 = PieRadarChartBase$2.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendVerticalAlignment[this.mLegend.getVerticalAlignment().ordinal()];
                                                if (n13 == 1) {
                                                    min3 = Math.min(this.mLegend.mNeededHeight, this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent());
                                                    break Label_0221;
                                                }
                                                if (n13 == 2) {
                                                    min4 = Math.min(this.mLegend.mNeededHeight, this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent());
                                                    break Label_0211;
                                                }
                                            }
                                            n = n5;
                                        }
                                        else {
                                            n = n8;
                                        }
                                    }
                                }
                                n8 = 0.0f;
                            }
                            n14 = 0.0f;
                            n15 = 0.0f;
                            break Label_0621;
                        }
                        n14 = min4;
                        n8 = 0.0f;
                        n = n2;
                        continue;
                    }
                }
                n15 = min3;
                n8 = 0.0f;
                final float n16 = 0.0f;
                n = n3;
                n14 = n16;
            }
            final float n17 = n + this.getRequiredBaseOffset();
            n18 = n8 + this.getRequiredBaseOffset();
            final float n19 = n15 + this.getRequiredBaseOffset();
            n20 = n14 + this.getRequiredBaseOffset();
            n21 = n17;
            n22 = n19;
        }
        else {
            n18 = 0.0f;
            n20 = 0.0f;
            n22 = 0.0f;
            n21 = n4;
        }
        float n23;
        final float a = n23 = Utils.convertDpToPixel(this.mMinOffset);
        if (this instanceof RadarChart) {
            final XAxis xAxis = this.getXAxis();
            n23 = a;
            if (xAxis.isEnabled()) {
                n23 = a;
                if (xAxis.isDrawLabelsEnabled()) {
                    n23 = Math.max(a, (float)xAxis.mLabelRotatedWidth);
                }
            }
        }
        final float extraTopOffset = this.getExtraTopOffset();
        final float extraRightOffset = this.getExtraRightOffset();
        final float extraBottomOffset = this.getExtraBottomOffset();
        final float max = Math.max(n23, n21 + this.getExtraLeftOffset());
        final float max2 = Math.max(n23, n22 + extraTopOffset);
        final float max3 = Math.max(n23, n18 + extraRightOffset);
        final float max4 = Math.max(n23, Math.max(this.getRequiredBaseOffset(), n20 + extraBottomOffset));
        this.mViewPortHandler.restrainViewPort(max, max2, max3, max4);
        if (this.mLogEnabled) {
            final StringBuilder sb = new StringBuilder();
            sb.append("offsetLeft: ");
            sb.append(max);
            sb.append(", offsetTop: ");
            sb.append(max2);
            sb.append(", offsetRight: ");
            sb.append(max3);
            sb.append(", offsetBottom: ");
            sb.append(max4);
            Log.i("MPAndroidChart", sb.toString());
        }
    }
    
    public void computeScroll() {
        if (this.mChartTouchListener instanceof PieRadarChartTouchListener) {
            ((PieRadarChartTouchListener)this.mChartTouchListener).computeScroll();
        }
    }
    
    public float distanceToCenter(float n, float n2) {
        final MPPointF centerOffsets = this.getCenterOffsets();
        if (n > centerOffsets.x) {
            n -= centerOffsets.x;
        }
        else {
            n = centerOffsets.x - n;
        }
        if (n2 > centerOffsets.y) {
            n2 -= centerOffsets.y;
        }
        else {
            n2 = centerOffsets.y - n2;
        }
        n = (float)Math.sqrt(Math.pow(n, 2.0) + Math.pow(n2, 2.0));
        MPPointF.recycleInstance(centerOffsets);
        return n;
    }
    
    public float getAngleForPoint(float n, float n2) {
        final MPPointF centerOffsets = this.getCenterOffsets();
        final double n3 = n - centerOffsets.x;
        final double n4 = n2 - centerOffsets.y;
        final float n5 = n2 = (float)Math.toDegrees(Math.acos(n4 / Math.sqrt(n3 * n3 + n4 * n4)));
        if (n > centerOffsets.x) {
            n2 = 360.0f - n5;
        }
        n2 = (n = n2 + 90.0f);
        if (n2 > 360.0f) {
            n = n2 - 360.0f;
        }
        MPPointF.recycleInstance(centerOffsets);
        return n;
    }
    
    public float getDiameter() {
        final RectF contentRect = this.mViewPortHandler.getContentRect();
        contentRect.left += this.getExtraLeftOffset();
        contentRect.top += this.getExtraTopOffset();
        contentRect.right -= this.getExtraRightOffset();
        contentRect.bottom -= this.getExtraBottomOffset();
        return Math.min(contentRect.width(), contentRect.height());
    }
    
    public abstract int getIndexForAngle(final float p0);
    
    public int getMaxVisibleCount() {
        return this.mData.getEntryCount();
    }
    
    public float getMinOffset() {
        return this.mMinOffset;
    }
    
    public MPPointF getPosition(final MPPointF mpPointF, final float n, final float n2) {
        final MPPointF instance = MPPointF.getInstance(0.0f, 0.0f);
        this.getPosition(mpPointF, n, n2, instance);
        return instance;
    }
    
    public void getPosition(final MPPointF mpPointF, final float n, final float n2, final MPPointF mpPointF2) {
        final double n3 = mpPointF.x;
        final double n4 = n;
        final double n5 = n2;
        mpPointF2.x = (float)(n3 + Math.cos(Math.toRadians(n5)) * n4);
        mpPointF2.y = (float)(mpPointF.y + n4 * Math.sin(Math.toRadians(n5)));
    }
    
    public abstract float getRadius();
    
    public float getRawRotationAngle() {
        return this.mRawRotationAngle;
    }
    
    protected abstract float getRequiredBaseOffset();
    
    protected abstract float getRequiredLegendOffset();
    
    public float getRotationAngle() {
        return this.mRotationAngle;
    }
    
    public float getYChartMax() {
        return 0.0f;
    }
    
    public float getYChartMin() {
        return 0.0f;
    }
    
    @Override
    protected void init() {
        super.init();
        this.mChartTouchListener = new PieRadarChartTouchListener(this);
    }
    
    public boolean isRotationEnabled() {
        return this.mRotateEnabled;
    }
    
    @Override
    public void notifyDataSetChanged() {
        if (this.mData == null) {
            return;
        }
        this.calcMinMax();
        if (this.mLegend != null) {
            this.mLegendRenderer.computeLegend(this.mData);
        }
        this.calculateOffsets();
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (this.mTouchEnabled && this.mChartTouchListener != null) {
            return this.mChartTouchListener.onTouch((View)this, motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }
    
    public void setMinOffset(final float mMinOffset) {
        this.mMinOffset = mMinOffset;
    }
    
    public void setRotationAngle(final float mRawRotationAngle) {
        this.mRawRotationAngle = mRawRotationAngle;
        this.mRotationAngle = Utils.getNormalizedAngle(mRawRotationAngle);
    }
    
    public void setRotationEnabled(final boolean mRotateEnabled) {
        this.mRotateEnabled = mRotateEnabled;
    }
    
    public void spin(final int n, final float rotationAngle, final float n2, final Easing.EasingFunction interpolator) {
        this.setRotationAngle(rotationAngle);
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat((Object)this, "rotationAngle", new float[] { rotationAngle, n2 });
        ofFloat.setDuration((long)n);
        ofFloat.setInterpolator((TimeInterpolator)interpolator);
        ofFloat.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener(this) {
            final PieRadarChartBase this$0;
            
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                this.this$0.postInvalidate();
            }
        });
        ofFloat.start();
    }
}

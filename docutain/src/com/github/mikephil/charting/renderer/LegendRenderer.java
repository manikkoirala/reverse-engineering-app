// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.renderer;

import com.github.mikephil.charting.utils.FSize;
import android.graphics.PathEffect;
import android.graphics.Canvas;
import android.graphics.Typeface;
import java.util.Collection;
import java.util.Collections;
import com.github.mikephil.charting.interfaces.datasets.ICandleDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import android.graphics.DashPathEffect;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.data.ChartData;
import android.graphics.Paint$Style;
import android.graphics.Paint$Align;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import com.github.mikephil.charting.utils.ViewPortHandler;
import android.graphics.Path;
import android.graphics.Paint;
import com.github.mikephil.charting.components.Legend;
import android.graphics.Paint$FontMetrics;
import com.github.mikephil.charting.components.LegendEntry;
import java.util.List;

public class LegendRenderer extends Renderer
{
    protected List<LegendEntry> computedEntries;
    protected Paint$FontMetrics legendFontMetrics;
    protected Legend mLegend;
    protected Paint mLegendFormPaint;
    protected Paint mLegendLabelPaint;
    private Path mLineFormPath;
    
    public LegendRenderer(final ViewPortHandler viewPortHandler, final Legend mLegend) {
        super(viewPortHandler);
        this.computedEntries = new ArrayList<LegendEntry>(16);
        this.legendFontMetrics = new Paint$FontMetrics();
        this.mLineFormPath = new Path();
        this.mLegend = mLegend;
        (this.mLegendLabelPaint = new Paint(1)).setTextSize(Utils.convertDpToPixel(9.0f));
        this.mLegendLabelPaint.setTextAlign(Paint$Align.LEFT);
        (this.mLegendFormPaint = new Paint(1)).setStyle(Paint$Style.FILL);
    }
    
    public void computeLegend(final ChartData<?> chartData) {
        ChartData<IBarDataSet> chartData2 = (ChartData<IBarDataSet>)chartData;
        if (!this.mLegend.isLegendCustom()) {
            this.computedEntries.clear();
            for (int i = 0; i < chartData.getDataSetCount(); ++i) {
                final IBarDataSet dataSetByIndex = chartData2.getDataSetByIndex(i);
                final List<Integer> colors = dataSetByIndex.getColors();
                final int entryCount = dataSetByIndex.getEntryCount();
                if (dataSetByIndex instanceof IBarDataSet) {
                    final IBarDataSet set = dataSetByIndex;
                    if (set.isStacked()) {
                        final String[] stackLabels = set.getStackLabels();
                        for (int n = 0; n < colors.size() && n < set.getStackSize(); ++n) {
                            this.computedEntries.add(new LegendEntry(stackLabels[n % stackLabels.length], dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), (int)colors.get(n)));
                        }
                        if (set.getLabel() != null) {
                            this.computedEntries.add(new LegendEntry(dataSetByIndex.getLabel(), Legend.LegendForm.NONE, Float.NaN, Float.NaN, null, 1122867));
                        }
                        continue;
                    }
                }
                Label_0669: {
                    if (dataSetByIndex instanceof IPieDataSet) {
                        final IPieDataSet set2 = (IPieDataSet)dataSetByIndex;
                        for (int n2 = 0; n2 < colors.size() && n2 < entryCount; ++n2) {
                            this.computedEntries.add(new LegendEntry(set2.getEntryForIndex(n2).getLabel(), dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), (int)colors.get(n2)));
                        }
                        if (set2.getLabel() != null) {
                            this.computedEntries.add(new LegendEntry(dataSetByIndex.getLabel(), Legend.LegendForm.NONE, Float.NaN, Float.NaN, null, 1122867));
                        }
                    }
                    else {
                        if (dataSetByIndex instanceof ICandleDataSet) {
                            final ICandleDataSet set3 = (ICandleDataSet)dataSetByIndex;
                            if (set3.getDecreasingColor() != 1122867) {
                                final int decreasingColor = set3.getDecreasingColor();
                                final int increasingColor = set3.getIncreasingColor();
                                this.computedEntries.add(new LegendEntry(null, dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), decreasingColor));
                                this.computedEntries.add(new LegendEntry(dataSetByIndex.getLabel(), dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), increasingColor));
                                break Label_0669;
                            }
                        }
                        for (int n3 = 0; n3 < colors.size() && n3 < entryCount; ++n3) {
                            String label;
                            if (n3 < colors.size() - 1 && n3 < entryCount - 1) {
                                label = null;
                            }
                            else {
                                label = chartData.getDataSetByIndex(i).getLabel();
                            }
                            this.computedEntries.add(new LegendEntry(label, dataSetByIndex.getForm(), dataSetByIndex.getFormSize(), dataSetByIndex.getFormLineWidth(), dataSetByIndex.getFormLineDashEffect(), colors.get(n3)));
                        }
                    }
                }
                chartData2 = (ChartData<IBarDataSet>)chartData;
            }
            if (this.mLegend.getExtraEntries() != null) {
                Collections.addAll(this.computedEntries, this.mLegend.getExtraEntries());
            }
            this.mLegend.setEntries(this.computedEntries);
        }
        final Typeface typeface = this.mLegend.getTypeface();
        if (typeface != null) {
            this.mLegendLabelPaint.setTypeface(typeface);
        }
        this.mLegendLabelPaint.setTextSize(this.mLegend.getTextSize());
        this.mLegendLabelPaint.setColor(this.mLegend.getTextColor());
        this.mLegend.calculateDimensions(this.mLegendLabelPaint, this.mViewPortHandler);
    }
    
    protected void drawForm(final Canvas canvas, final float n, final float n2, final LegendEntry legendEntry, final Legend legend) {
        if (legendEntry.formColor != 1122868 && legendEntry.formColor != 1122867) {
            if (legendEntry.formColor != 0) {
                final int save = canvas.save();
                Enum<Legend.LegendForm> enum1;
                if ((enum1 = legendEntry.form) == Legend.LegendForm.DEFAULT) {
                    enum1 = legend.getForm();
                }
                this.mLegendFormPaint.setColor(legendEntry.formColor);
                float n3;
                if (Float.isNaN(legendEntry.formSize)) {
                    n3 = legend.getFormSize();
                }
                else {
                    n3 = legendEntry.formSize;
                }
                final float convertDpToPixel = Utils.convertDpToPixel(n3);
                final float n4 = convertDpToPixel / 2.0f;
                final int n5 = LegendRenderer$1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendForm[enum1.ordinal()];
                if (n5 != 3 && n5 != 4) {
                    if (n5 != 5) {
                        if (n5 == 6) {
                            float n6;
                            if (Float.isNaN(legendEntry.formLineWidth)) {
                                n6 = legend.getFormLineWidth();
                            }
                            else {
                                n6 = legendEntry.formLineWidth;
                            }
                            final float convertDpToPixel2 = Utils.convertDpToPixel(n6);
                            DashPathEffect pathEffect;
                            if (legendEntry.formLineDashEffect == null) {
                                pathEffect = legend.getFormLineDashEffect();
                            }
                            else {
                                pathEffect = legendEntry.formLineDashEffect;
                            }
                            this.mLegendFormPaint.setStyle(Paint$Style.STROKE);
                            this.mLegendFormPaint.setStrokeWidth(convertDpToPixel2);
                            this.mLegendFormPaint.setPathEffect((PathEffect)pathEffect);
                            this.mLineFormPath.reset();
                            this.mLineFormPath.moveTo(n, n2);
                            this.mLineFormPath.lineTo(n + convertDpToPixel, n2);
                            canvas.drawPath(this.mLineFormPath, this.mLegendFormPaint);
                        }
                    }
                    else {
                        this.mLegendFormPaint.setStyle(Paint$Style.FILL);
                        canvas.drawRect(n, n2 - n4, n + convertDpToPixel, n2 + n4, this.mLegendFormPaint);
                    }
                }
                else {
                    this.mLegendFormPaint.setStyle(Paint$Style.FILL);
                    canvas.drawCircle(n + n4, n2, n4, this.mLegendFormPaint);
                }
                canvas.restoreToCount(save);
            }
        }
    }
    
    protected void drawLabel(final Canvas canvas, final float n, final float n2, final String s) {
        canvas.drawText(s, n, n2, this.mLegendLabelPaint);
    }
    
    public Paint getFormPaint() {
        return this.mLegendFormPaint;
    }
    
    public Paint getLabelPaint() {
        return this.mLegendLabelPaint;
    }
    
    public void renderLegend(final Canvas canvas) {
        if (!this.mLegend.isEnabled()) {
            return;
        }
        final Typeface typeface = this.mLegend.getTypeface();
        if (typeface != null) {
            this.mLegendLabelPaint.setTypeface(typeface);
        }
        this.mLegendLabelPaint.setTextSize(this.mLegend.getTextSize());
        this.mLegendLabelPaint.setColor(this.mLegend.getTextColor());
        final float lineHeight = Utils.getLineHeight(this.mLegendLabelPaint, this.legendFontMetrics);
        final float n = Utils.getLineSpacing(this.mLegendLabelPaint, this.legendFontMetrics) + Utils.convertDpToPixel(this.mLegend.getYEntrySpace());
        final float n2 = lineHeight - Utils.calcTextHeight(this.mLegendLabelPaint, "ABC") / 2.0f;
        final LegendEntry[] entries = this.mLegend.getEntries();
        final float convertDpToPixel = Utils.convertDpToPixel(this.mLegend.getFormToTextSpace());
        final float convertDpToPixel2 = Utils.convertDpToPixel(this.mLegend.getXEntrySpace());
        final Legend.LegendOrientation orientation = this.mLegend.getOrientation();
        final Legend.LegendHorizontalAlignment horizontalAlignment = this.mLegend.getHorizontalAlignment();
        final Legend.LegendVerticalAlignment verticalAlignment = this.mLegend.getVerticalAlignment();
        final Legend.LegendDirection direction = this.mLegend.getDirection();
        final float convertDpToPixel3 = Utils.convertDpToPixel(this.mLegend.getFormSize());
        final float convertDpToPixel4 = Utils.convertDpToPixel(this.mLegend.getStackSpace());
        final float yOffset = this.mLegend.getYOffset();
        float xOffset = this.mLegend.getXOffset();
        final int n3 = LegendRenderer$1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendHorizontalAlignment[horizontalAlignment.ordinal()];
        if (n3 != 1) {
            if (n3 != 2) {
                if (n3 != 3) {
                    xOffset = 0.0f;
                }
                else {
                    float n4;
                    if (orientation == Legend.LegendOrientation.VERTICAL) {
                        n4 = this.mViewPortHandler.getChartWidth() / 2.0f;
                    }
                    else {
                        n4 = this.mViewPortHandler.contentLeft() + this.mViewPortHandler.contentWidth() / 2.0f;
                    }
                    float n5;
                    if (direction == Legend.LegendDirection.LEFT_TO_RIGHT) {
                        n5 = xOffset;
                    }
                    else {
                        n5 = -xOffset;
                    }
                    final float n6 = n4 + n5;
                    if (orientation == Legend.LegendOrientation.VERTICAL) {
                        final double n7 = n6;
                        double n8;
                        if (direction == Legend.LegendDirection.LEFT_TO_RIGHT) {
                            n8 = -this.mLegend.mNeededWidth / 2.0 + xOffset;
                        }
                        else {
                            n8 = this.mLegend.mNeededWidth / 2.0 - xOffset;
                        }
                        xOffset = (float)(n7 + n8);
                    }
                    else {
                        xOffset = n6;
                    }
                }
            }
            else {
                float n9;
                if (orientation == Legend.LegendOrientation.VERTICAL) {
                    n9 = this.mViewPortHandler.getChartWidth();
                }
                else {
                    n9 = this.mViewPortHandler.contentRight();
                }
                final float n10 = xOffset = n9 - xOffset;
                if (direction == Legend.LegendDirection.LEFT_TO_RIGHT) {
                    xOffset = n10 - this.mLegend.mNeededWidth;
                }
            }
        }
        else {
            if (orientation != Legend.LegendOrientation.VERTICAL) {
                xOffset += this.mViewPortHandler.contentLeft();
            }
            if (direction == Legend.LegendDirection.RIGHT_TO_LEFT) {
                xOffset += this.mLegend.mNeededWidth;
            }
        }
        final int n11 = LegendRenderer$1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendOrientation[orientation.ordinal()];
        if (n11 != 1) {
            if (n11 == 2) {
                final int n12 = LegendRenderer$1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendVerticalAlignment[verticalAlignment.ordinal()];
                float n13;
                if (n12 != 1) {
                    if (n12 != 2) {
                        if (n12 != 3) {
                            n13 = 0.0f;
                        }
                        else {
                            n13 = this.mViewPortHandler.getChartHeight() / 2.0f - this.mLegend.mNeededHeight / 2.0f + this.mLegend.getYOffset();
                        }
                    }
                    else {
                        float n14;
                        if (horizontalAlignment == Legend.LegendHorizontalAlignment.CENTER) {
                            n14 = this.mViewPortHandler.getChartHeight();
                        }
                        else {
                            n14 = this.mViewPortHandler.contentBottom();
                        }
                        n13 = n14 - (this.mLegend.mNeededHeight + yOffset);
                    }
                }
                else {
                    float contentTop;
                    if (horizontalAlignment == Legend.LegendHorizontalAlignment.CENTER) {
                        contentTop = 0.0f;
                    }
                    else {
                        contentTop = this.mViewPortHandler.contentTop();
                    }
                    n13 = contentTop + yOffset;
                }
                int n15 = 0;
                int i = 0;
                float n16 = 0.0f;
                final float n17 = convertDpToPixel4;
                final float n18 = n2;
                float n19 = n13;
                final Legend.LegendDirection legendDirection = direction;
                while (i < entries.length) {
                    final LegendEntry legendEntry = entries[i];
                    final boolean b = legendEntry.form != Legend.LegendForm.NONE;
                    float convertDpToPixel5;
                    if (Float.isNaN(legendEntry.formSize)) {
                        convertDpToPixel5 = convertDpToPixel3;
                    }
                    else {
                        convertDpToPixel5 = Utils.convertDpToPixel(legendEntry.formSize);
                    }
                    float n22;
                    if (b) {
                        float n20;
                        if (legendDirection == Legend.LegendDirection.LEFT_TO_RIGHT) {
                            n20 = xOffset + n16;
                        }
                        else {
                            n20 = xOffset - (convertDpToPixel5 - n16);
                        }
                        final float n21 = n20;
                        this.drawForm(canvas, n21, n19 + n18, legendEntry, this.mLegend);
                        n22 = n21;
                        if (legendDirection == Legend.LegendDirection.LEFT_TO_RIGHT) {
                            n22 = n21 + convertDpToPixel5;
                        }
                    }
                    else {
                        n22 = xOffset;
                    }
                    float n25;
                    if (legendEntry.label != null) {
                        if (b && n15 == 0) {
                            float n23;
                            if (legendDirection == Legend.LegendDirection.LEFT_TO_RIGHT) {
                                n23 = convertDpToPixel;
                            }
                            else {
                                n23 = -convertDpToPixel;
                            }
                            n22 += n23;
                        }
                        else if (n15 != 0) {
                            n22 = xOffset;
                        }
                        float n24 = n22;
                        if (legendDirection == Legend.LegendDirection.RIGHT_TO_LEFT) {
                            n24 = n22 - Utils.calcTextWidth(this.mLegendLabelPaint, legendEntry.label);
                        }
                        if (n15 == 0) {
                            this.drawLabel(canvas, n24, n19 + lineHeight, legendEntry.label);
                        }
                        else {
                            n19 += lineHeight + n;
                            this.drawLabel(canvas, n24, n19 + lineHeight, legendEntry.label);
                        }
                        n19 += lineHeight + n;
                        n25 = 0.0f;
                    }
                    else {
                        n25 = n16 + (convertDpToPixel5 + n17);
                        n15 = 1;
                    }
                    ++i;
                    n16 = n25;
                }
            }
        }
        else {
            final List<FSize> calculatedLineSizes = this.mLegend.getCalculatedLineSizes();
            final List<FSize> calculatedLabelSizes = this.mLegend.getCalculatedLabelSizes();
            final List<Boolean> calculatedLabelBreakPoints = this.mLegend.getCalculatedLabelBreakPoints();
            final int n26 = LegendRenderer$1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendVerticalAlignment[verticalAlignment.ordinal()];
            float n27 = yOffset;
            if (n26 != 1) {
                if (n26 != 2) {
                    if (n26 != 3) {
                        n27 = 0.0f;
                    }
                    else {
                        n27 = yOffset + (this.mViewPortHandler.getChartHeight() - this.mLegend.mNeededHeight) / 2.0f;
                    }
                }
                else {
                    n27 = this.mViewPortHandler.getChartHeight() - yOffset - this.mLegend.mNeededHeight;
                }
            }
            final int length = entries.length;
            float n28 = xOffset;
            int j = 0;
            int n29 = 0;
            final float n30 = convertDpToPixel4;
            final float n31 = xOffset;
            while (j < length) {
                final LegendEntry legendEntry2 = entries[j];
                final boolean b2 = legendEntry2.form != Legend.LegendForm.NONE;
                float convertDpToPixel6;
                if (Float.isNaN(legendEntry2.formSize)) {
                    convertDpToPixel6 = convertDpToPixel3;
                }
                else {
                    convertDpToPixel6 = Utils.convertDpToPixel(legendEntry2.formSize);
                }
                float n32;
                if (j < calculatedLabelBreakPoints.size() && (boolean)calculatedLabelBreakPoints.get(j)) {
                    n27 += lineHeight + n;
                    n32 = n31;
                }
                else {
                    n32 = n28;
                }
                if (n32 == n31 && horizontalAlignment == Legend.LegendHorizontalAlignment.CENTER && n29 < calculatedLineSizes.size()) {
                    float width;
                    if (direction == Legend.LegendDirection.RIGHT_TO_LEFT) {
                        width = calculatedLineSizes.get(n29).width;
                    }
                    else {
                        width = -calculatedLineSizes.get(n29).width;
                    }
                    n32 += width / 2.0f;
                    ++n29;
                }
                final boolean b3 = legendEntry2.label == null;
                if (b2) {
                    float n33 = n32;
                    if (direction == Legend.LegendDirection.RIGHT_TO_LEFT) {
                        n33 = n32 - convertDpToPixel6;
                    }
                    this.drawForm(canvas, n33, n27 + n2, legendEntry2, this.mLegend);
                    if (direction == Legend.LegendDirection.LEFT_TO_RIGHT) {
                        n32 = n33 + convertDpToPixel6;
                    }
                    else {
                        n32 = n33;
                    }
                }
                float n39;
                if (!b3) {
                    float n34 = n32;
                    if (b2) {
                        float n35;
                        if (direction == Legend.LegendDirection.RIGHT_TO_LEFT) {
                            n35 = -convertDpToPixel;
                        }
                        else {
                            n35 = convertDpToPixel;
                        }
                        n34 = n32 + n35;
                    }
                    float n36 = n34;
                    if (direction == Legend.LegendDirection.RIGHT_TO_LEFT) {
                        n36 = n34 - calculatedLabelSizes.get(j).width;
                    }
                    this.drawLabel(canvas, n36, n27 + lineHeight, legendEntry2.label);
                    float n37 = n36;
                    if (direction == Legend.LegendDirection.LEFT_TO_RIGHT) {
                        n37 = n36 + calculatedLabelSizes.get(j).width;
                    }
                    float n38;
                    if (direction == Legend.LegendDirection.RIGHT_TO_LEFT) {
                        n38 = -convertDpToPixel2;
                    }
                    else {
                        n38 = convertDpToPixel2;
                    }
                    n39 = n37 + n38;
                }
                else {
                    float n40;
                    if (direction == Legend.LegendDirection.RIGHT_TO_LEFT) {
                        n40 = -n30;
                    }
                    else {
                        n40 = n30;
                    }
                    n39 = n32 + n40;
                }
                ++j;
                n28 = n39;
            }
        }
    }
}

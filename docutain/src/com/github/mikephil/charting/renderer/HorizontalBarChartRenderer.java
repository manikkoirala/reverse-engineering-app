// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.renderer;

import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.buffer.HorizontalBarBuffer;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.formatter.ValueFormatter;
import java.util.List;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.dataprovider.ChartInterface;
import com.github.mikephil.charting.buffer.BarBuffer;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import android.graphics.Canvas;
import android.graphics.Paint$Align;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import android.graphics.RectF;

public class HorizontalBarChartRenderer extends BarChartRenderer
{
    private RectF mBarShadowRectBuffer;
    
    public HorizontalBarChartRenderer(final BarDataProvider barDataProvider, final ChartAnimator chartAnimator, final ViewPortHandler viewPortHandler) {
        super(barDataProvider, chartAnimator, viewPortHandler);
        this.mBarShadowRectBuffer = new RectF();
        this.mValuePaint.setTextAlign(Paint$Align.LEFT);
    }
    
    @Override
    protected void drawDataSet(final Canvas canvas, final IBarDataSet set, int dataSet) {
        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
        this.mBarBorderPaint.setColor(set.getBarBorderColor());
        this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(set.getBarBorderWidth()));
        final float barBorderWidth = set.getBarBorderWidth();
        final int n = 0;
        final int n2 = 1;
        final boolean b = barBorderWidth > 0.0f;
        final float phaseX = this.mAnimator.getPhaseX();
        final float phaseY = this.mAnimator.getPhaseY();
        if (this.mChart.isDrawBarShadowEnabled()) {
            this.mShadowPaint.setColor(set.getBarShadowColor());
            final float n3 = this.mChart.getBarData().getBarWidth() / 2.0f;
            for (int min = Math.min((int)Math.ceil(set.getEntryCount() * phaseX), set.getEntryCount()), i = 0; i < min; ++i) {
                final float x = set.getEntryForIndex(i).getX();
                this.mBarShadowRectBuffer.top = x - n3;
                this.mBarShadowRectBuffer.bottom = x + n3;
                transformer.rectValueToPixel(this.mBarShadowRectBuffer);
                if (this.mViewPortHandler.isInBoundsTop(this.mBarShadowRectBuffer.bottom)) {
                    if (!this.mViewPortHandler.isInBoundsBottom(this.mBarShadowRectBuffer.top)) {
                        break;
                    }
                    this.mBarShadowRectBuffer.left = this.mViewPortHandler.contentLeft();
                    this.mBarShadowRectBuffer.right = this.mViewPortHandler.contentRight();
                    canvas.drawRect(this.mBarShadowRectBuffer, this.mShadowPaint);
                }
            }
        }
        final BarBuffer barBuffer = this.mBarBuffers[dataSet];
        barBuffer.setPhases(phaseX, phaseY);
        barBuffer.setDataSet(dataSet);
        barBuffer.setInverted(this.mChart.isInverted(set.getAxisDependency()));
        barBuffer.setBarWidth(this.mChart.getBarData().getBarWidth());
        barBuffer.feed(set);
        transformer.pointValuesToPixel(barBuffer.buffer);
        if (set.getColors().size() == 1) {
            dataSet = n2;
        }
        else {
            dataSet = 0;
        }
        int j = n;
        if (dataSet != 0) {
            this.mRenderPaint.setColor(set.getColor());
            j = n;
        }
        while (j < barBuffer.size()) {
            final ViewPortHandler mViewPortHandler = this.mViewPortHandler;
            final float[] buffer = barBuffer.buffer;
            final int n4 = j + 3;
            if (!mViewPortHandler.isInBoundsTop(buffer[n4])) {
                break;
            }
            final ViewPortHandler mViewPortHandler2 = this.mViewPortHandler;
            final float[] buffer2 = barBuffer.buffer;
            final int n5 = j + 1;
            if (mViewPortHandler2.isInBoundsBottom(buffer2[n5])) {
                if (dataSet == 0) {
                    this.mRenderPaint.setColor(set.getColor(j / 4));
                }
                final float n6 = barBuffer.buffer[j];
                final float n7 = barBuffer.buffer[n5];
                final float[] buffer3 = barBuffer.buffer;
                final int n8 = j + 2;
                canvas.drawRect(n6, n7, buffer3[n8], barBuffer.buffer[n4], this.mRenderPaint);
                if (b) {
                    canvas.drawRect(barBuffer.buffer[j], barBuffer.buffer[n5], barBuffer.buffer[n8], barBuffer.buffer[n4], this.mBarBorderPaint);
                }
            }
            j += 4;
        }
    }
    
    @Override
    public void drawValue(final Canvas canvas, final String s, final float n, final float n2, final int color) {
        this.mValuePaint.setColor(color);
        canvas.drawText(s, n, n2, this.mValuePaint);
    }
    
    @Override
    public void drawValues(final Canvas canvas) {
        if (this.isDrawingValuesAllowed(this.mChart)) {
            List<IBarDataSet> dataSets = this.mChart.getBarData().getDataSets();
            final float convertDpToPixel = Utils.convertDpToPixel(5.0f);
            final boolean drawValueAboveBarEnabled = this.mChart.isDrawValueAboveBarEnabled();
            for (int i = 0; i < this.mChart.getBarData().getDataSetCount(); ++i) {
                final IBarDataSet set = dataSets.get(i);
                if (this.shouldDrawValues(set)) {
                    final boolean inverted = this.mChart.isInverted(set.getAxisDependency());
                    this.applyValueTextStyle(set);
                    final float n = Utils.calcTextHeight(this.mValuePaint, "10") / 2.0f;
                    final ValueFormatter valueFormatter = set.getValueFormatter();
                    final BarBuffer barBuffer = this.mBarBuffers[i];
                    final float phaseY = this.mAnimator.getPhaseY();
                    final MPPointF instance = MPPointF.getInstance(set.getIconsOffset());
                    instance.x = Utils.convertDpToPixel(instance.x);
                    instance.y = Utils.convertDpToPixel(instance.y);
                    MPPointF mpPointF2;
                    if (!set.isStacked()) {
                        int n2 = 0;
                        final float n3 = n;
                        final ValueFormatter valueFormatter2 = valueFormatter;
                        final BarBuffer barBuffer2 = barBuffer;
                        final MPPointF mpPointF = instance;
                        while (n2 < barBuffer2.buffer.length * this.mAnimator.getPhaseX()) {
                            final float[] buffer = barBuffer2.buffer;
                            final int n4 = n2 + 1;
                            final float n5 = (buffer[n4] + barBuffer2.buffer[n2 + 3]) / 2.0f;
                            if (!this.mViewPortHandler.isInBoundsTop(barBuffer2.buffer[n4])) {
                                break;
                            }
                            if (this.mViewPortHandler.isInBoundsX(barBuffer2.buffer[n2]) && this.mViewPortHandler.isInBoundsBottom(barBuffer2.buffer[n4])) {
                                final BarEntry barEntry = set.getEntryForIndex(n2 / 4);
                                final float y = barEntry.getY();
                                final String barLabel = valueFormatter2.getBarLabel(barEntry);
                                final float n6 = (float)Utils.calcTextWidth(this.mValuePaint, barLabel);
                                float n7;
                                if (drawValueAboveBarEnabled) {
                                    n7 = convertDpToPixel;
                                }
                                else {
                                    n7 = -(n6 + convertDpToPixel);
                                }
                                float n8;
                                if (drawValueAboveBarEnabled) {
                                    n8 = -(n6 + convertDpToPixel);
                                }
                                else {
                                    n8 = convertDpToPixel;
                                }
                                float n9 = n7;
                                float n10 = n8;
                                if (inverted) {
                                    n9 = -n7 - n6;
                                    n10 = -n8 - n6;
                                }
                                float n11 = n9;
                                if (set.isDrawValuesEnabled()) {
                                    final float n12 = barBuffer2.buffer[n2 + 2];
                                    float n13;
                                    if (y >= 0.0f) {
                                        n13 = n11;
                                    }
                                    else {
                                        n13 = n10;
                                    }
                                    this.drawValue(canvas, barLabel, n12 + n13, n5 + n3, set.getValueTextColor(n2 / 2));
                                }
                                if (barEntry.getIcon() != null && set.isDrawIconsEnabled()) {
                                    final Drawable icon = barEntry.getIcon();
                                    final float n14 = barBuffer2.buffer[n2 + 2];
                                    if (y < 0.0f) {
                                        n11 = n10;
                                    }
                                    Utils.drawImage(canvas, icon, (int)(n14 + n11 + mpPointF.x), (int)(n5 + mpPointF.y), icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
                                }
                            }
                            n2 += 4;
                        }
                        mpPointF2 = mpPointF;
                    }
                    else {
                        final List<IBarDataSet> list = dataSets;
                        final int n15 = i;
                        final MPPointF mpPointF3 = instance;
                        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
                        int n16 = 0;
                        int n17 = 0;
                        while (true) {
                            mpPointF2 = mpPointF3;
                            dataSets = list;
                            i = n15;
                            if (n16 >= set.getEntryCount() * this.mAnimator.getPhaseX()) {
                                break;
                            }
                            final BarEntry barEntry2 = set.getEntryForIndex(n16);
                            final int valueTextColor = set.getValueTextColor(n16);
                            final float[] yVals = barEntry2.getYVals();
                            if (yVals == null) {
                                final ViewPortHandler mViewPortHandler = this.mViewPortHandler;
                                final float[] buffer2 = barBuffer.buffer;
                                final int n18 = n17 + 1;
                                if (!mViewPortHandler.isInBoundsTop(buffer2[n18])) {
                                    mpPointF2 = mpPointF3;
                                    dataSets = list;
                                    i = n15;
                                    break;
                                }
                                if (!this.mViewPortHandler.isInBoundsX(barBuffer.buffer[n17])) {
                                    continue;
                                }
                                if (!this.mViewPortHandler.isInBoundsBottom(barBuffer.buffer[n18])) {
                                    continue;
                                }
                                final String barLabel2 = valueFormatter.getBarLabel(barEntry2);
                                final float n19 = (float)Utils.calcTextWidth(this.mValuePaint, barLabel2);
                                float n20;
                                if (drawValueAboveBarEnabled) {
                                    n20 = convertDpToPixel;
                                }
                                else {
                                    n20 = -(n19 + convertDpToPixel);
                                }
                                float n21;
                                if (drawValueAboveBarEnabled) {
                                    n21 = -(n19 + convertDpToPixel);
                                }
                                else {
                                    n21 = convertDpToPixel;
                                }
                                float n22 = n20;
                                float n23 = n21;
                                if (inverted) {
                                    n22 = -n20 - n19;
                                    n23 = -n21 - n19;
                                }
                                float n24 = n22;
                                if (set.isDrawValuesEnabled()) {
                                    final float n25 = barBuffer.buffer[n17 + 2];
                                    float n26;
                                    if (barEntry2.getY() >= 0.0f) {
                                        n26 = n24;
                                    }
                                    else {
                                        n26 = n23;
                                    }
                                    this.drawValue(canvas, barLabel2, n25 + n26, barBuffer.buffer[n18] + n, valueTextColor);
                                }
                                if (barEntry2.getIcon() != null && set.isDrawIconsEnabled()) {
                                    final Drawable icon2 = barEntry2.getIcon();
                                    final float n27 = barBuffer.buffer[n17 + 2];
                                    if (barEntry2.getY() < 0.0f) {
                                        n24 = n23;
                                    }
                                    Utils.drawImage(canvas, icon2, (int)(n27 + n24 + mpPointF3.x), (int)(barBuffer.buffer[n18] + mpPointF3.y), icon2.getIntrinsicWidth(), icon2.getIntrinsicHeight());
                                }
                            }
                            else {
                                final int n28 = yVals.length * 2;
                                final float[] array = new float[n28];
                                float n29 = -barEntry2.getNegativeSum();
                                int j = 0;
                                int n30 = 0;
                                float n31 = 0.0f;
                                while (j < n28) {
                                    float n32 = yVals[n30];
                                    final float n33 = fcmpl(n32, 0.0f);
                                    float n34;
                                    if (n33 == 0 && (n31 == 0.0f || n29 == 0.0f)) {
                                        n34 = n31;
                                    }
                                    else if (n33 >= 0) {
                                        n34 = (n32 += n31);
                                    }
                                    else {
                                        final float n35 = n29 - n32;
                                        n34 = n31;
                                        n32 = n29;
                                        n29 = n35;
                                    }
                                    array[j] = n32 * phaseY;
                                    j += 2;
                                    ++n30;
                                    n31 = n34;
                                }
                                transformer.pointValuesToPixel(array);
                                for (int k = 0; k < n28; k += 2) {
                                    final float n36 = yVals[k / 2];
                                    final String barStackedLabel = valueFormatter.getBarStackedLabel(n36, barEntry2);
                                    final float n37 = (float)Utils.calcTextWidth(this.mValuePaint, barStackedLabel);
                                    float n38;
                                    if (drawValueAboveBarEnabled) {
                                        n38 = convertDpToPixel;
                                    }
                                    else {
                                        n38 = -(n37 + convertDpToPixel);
                                    }
                                    float n39;
                                    if (drawValueAboveBarEnabled) {
                                        n39 = -(n37 + convertDpToPixel);
                                    }
                                    else {
                                        n39 = convertDpToPixel;
                                    }
                                    float n40 = n38;
                                    float n41 = n39;
                                    if (inverted) {
                                        n40 = -n38 - n37;
                                        n41 = -n39 - n37;
                                    }
                                    final boolean b = (n36 == 0.0f && n29 == 0.0f && n31 > 0.0f) || n36 < 0.0f;
                                    final float n42 = array[k];
                                    if (b) {
                                        n40 = n41;
                                    }
                                    final float n43 = n42 + n40;
                                    final float n44 = (barBuffer.buffer[n17 + 1] + barBuffer.buffer[n17 + 3]) / 2.0f;
                                    if (!this.mViewPortHandler.isInBoundsTop(n44)) {
                                        break;
                                    }
                                    if (this.mViewPortHandler.isInBoundsX(n43) && this.mViewPortHandler.isInBoundsBottom(n44)) {
                                        if (set.isDrawValuesEnabled()) {
                                            this.drawValue(canvas, barStackedLabel, n43, n44 + n, valueTextColor);
                                        }
                                        if (barEntry2.getIcon() != null && set.isDrawIconsEnabled()) {
                                            final Drawable icon3 = barEntry2.getIcon();
                                            Utils.drawImage(canvas, icon3, (int)(n43 + mpPointF3.x), (int)(n44 + mpPointF3.y), icon3.getIntrinsicWidth(), icon3.getIntrinsicHeight());
                                        }
                                    }
                                }
                            }
                            int n45;
                            if (yVals == null) {
                                n45 = n17 + 4;
                            }
                            else {
                                n45 = n17 + yVals.length * 4;
                            }
                            ++n16;
                            n17 = n45;
                        }
                    }
                    MPPointF.recycleInstance(mpPointF2);
                }
            }
        }
    }
    
    @Override
    public void initBuffers() {
        final BarData barData = this.mChart.getBarData();
        this.mBarBuffers = new HorizontalBarBuffer[barData.getDataSetCount()];
        for (int i = 0; i < this.mBarBuffers.length; ++i) {
            final IBarDataSet set = barData.getDataSetByIndex(i);
            final BarBuffer[] mBarBuffers = this.mBarBuffers;
            final int entryCount = set.getEntryCount();
            int stackSize;
            if (set.isStacked()) {
                stackSize = set.getStackSize();
            }
            else {
                stackSize = 1;
            }
            mBarBuffers[i] = new HorizontalBarBuffer(entryCount * 4 * stackSize, barData.getDataSetCount(), set.isStacked());
        }
    }
    
    @Override
    protected boolean isDrawingValuesAllowed(final ChartInterface chartInterface) {
        return chartInterface.getData().getEntryCount() < chartInterface.getMaxVisibleCount() * this.mViewPortHandler.getScaleY();
    }
    
    @Override
    protected void prepareBarHighlight(final float n, final float n2, final float n3, final float n4, final Transformer transformer) {
        this.mBarRect.set(n2, n - n4, n3, n + n4);
        transformer.rectToPixelPhaseHorizontal(this.mBarRect, this.mAnimator.getPhaseY());
    }
    
    @Override
    protected void setHighlightDrawPos(final Highlight highlight, final RectF rectF) {
        highlight.setDraw(rectF.centerY(), rectF.right);
    }
}

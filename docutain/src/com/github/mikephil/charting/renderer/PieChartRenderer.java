// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.renderer;

import com.github.mikephil.charting.data.Entry;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.formatter.ValueFormatter;
import java.util.List;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import android.graphics.Color;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.data.PieEntry;
import java.util.Iterator;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.data.PieData;
import android.graphics.Bitmap$Config;
import android.graphics.Path$Direction;
import android.os.Build$VERSION;
import android.text.Layout$Alignment;
import com.github.mikephil.charting.utils.MPPointF;
import android.graphics.Paint$Align;
import com.github.mikephil.charting.utils.Utils;
import android.graphics.Paint$Style;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.github.mikephil.charting.animation.ChartAnimator;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Bitmap;
import java.lang.ref.WeakReference;
import com.github.mikephil.charting.charts.PieChart;
import android.text.TextPaint;
import android.text.StaticLayout;
import android.graphics.RectF;
import android.graphics.Canvas;

public class PieChartRenderer extends DataRenderer
{
    protected Canvas mBitmapCanvas;
    private RectF mCenterTextLastBounds;
    private CharSequence mCenterTextLastValue;
    private StaticLayout mCenterTextLayout;
    private TextPaint mCenterTextPaint;
    protected PieChart mChart;
    protected WeakReference<Bitmap> mDrawBitmap;
    protected Path mDrawCenterTextPathBuffer;
    protected RectF mDrawHighlightedRectF;
    private Paint mEntryLabelsPaint;
    private Path mHoleCirclePath;
    protected Paint mHolePaint;
    private RectF mInnerRectBuffer;
    private Path mPathBuffer;
    private RectF[] mRectBuffer;
    protected Paint mTransparentCirclePaint;
    protected Paint mValueLinePaint;
    
    public PieChartRenderer(final PieChart mChart, final ChartAnimator chartAnimator, final ViewPortHandler viewPortHandler) {
        super(chartAnimator, viewPortHandler);
        this.mCenterTextLastBounds = new RectF();
        this.mRectBuffer = new RectF[] { new RectF(), new RectF(), new RectF() };
        this.mPathBuffer = new Path();
        this.mInnerRectBuffer = new RectF();
        this.mHoleCirclePath = new Path();
        this.mDrawCenterTextPathBuffer = new Path();
        this.mDrawHighlightedRectF = new RectF();
        this.mChart = mChart;
        (this.mHolePaint = new Paint(1)).setColor(-1);
        this.mHolePaint.setStyle(Paint$Style.FILL);
        (this.mTransparentCirclePaint = new Paint(1)).setColor(-1);
        this.mTransparentCirclePaint.setStyle(Paint$Style.FILL);
        this.mTransparentCirclePaint.setAlpha(105);
        (this.mCenterTextPaint = new TextPaint(1)).setColor(-16777216);
        this.mCenterTextPaint.setTextSize(Utils.convertDpToPixel(12.0f));
        this.mValuePaint.setTextSize(Utils.convertDpToPixel(13.0f));
        this.mValuePaint.setColor(-1);
        this.mValuePaint.setTextAlign(Paint$Align.CENTER);
        (this.mEntryLabelsPaint = new Paint(1)).setColor(-1);
        this.mEntryLabelsPaint.setTextAlign(Paint$Align.CENTER);
        this.mEntryLabelsPaint.setTextSize(Utils.convertDpToPixel(13.0f));
        (this.mValueLinePaint = new Paint(1)).setStyle(Paint$Style.STROKE);
    }
    
    protected float calculateMinimumRadiusForSpacedSlice(final MPPointF mpPointF, final float n, final float n2, final float n3, final float n4, float n5, float n6) {
        final float n7 = n6 / 2.0f;
        final float x = mpPointF.x;
        final double n8 = (n5 + n6) * 0.017453292f;
        final float n9 = x + (float)Math.cos(n8) * n;
        n6 = mpPointF.y + (float)Math.sin(n8) * n;
        final float x2 = mpPointF.x;
        final double n10 = (n5 + n7) * 0.017453292f;
        final float n11 = (float)Math.cos(n10);
        final float y = mpPointF.y;
        n5 = (float)Math.sin(n10);
        return (float)(n - (float)(Math.sqrt(Math.pow(n9 - n3, 2.0) + Math.pow(n6 - n4, 2.0)) / 2.0 * Math.tan((180.0 - n2) / 2.0 * 0.017453292519943295)) - Math.sqrt(Math.pow(x2 + n11 * n - (n9 + n3) / 2.0f, 2.0) + Math.pow(y + n5 * n - (n6 + n4) / 2.0f, 2.0)));
    }
    
    protected void drawCenterText(final Canvas canvas) {
        final CharSequence centerText = this.mChart.getCenterText();
        if (this.mChart.isDrawCenterTextEnabled() && centerText != null) {
            final MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
            final MPPointF centerTextOffset = this.mChart.getCenterTextOffset();
            final float n = centerCircleBox.x + centerTextOffset.x;
            final float n2 = centerCircleBox.y + centerTextOffset.y;
            float radius;
            if (this.mChart.isDrawHoleEnabled() && !this.mChart.isDrawSlicesUnderHoleEnabled()) {
                radius = this.mChart.getRadius() * (this.mChart.getHoleRadius() / 100.0f);
            }
            else {
                radius = this.mChart.getRadius();
            }
            final RectF rectF = this.mRectBuffer[0];
            rectF.left = n - radius;
            rectF.top = n2 - radius;
            rectF.right = n + radius;
            rectF.bottom = n2 + radius;
            final RectF rectF2 = this.mRectBuffer[1];
            rectF2.set(rectF);
            final float n3 = this.mChart.getCenterTextRadiusPercent() / 100.0f;
            if (n3 > 0.0) {
                rectF2.inset((rectF2.width() - rectF2.width() * n3) / 2.0f, (rectF2.height() - rectF2.height() * n3) / 2.0f);
            }
            if (!centerText.equals(this.mCenterTextLastValue) || !rectF2.equals((Object)this.mCenterTextLastBounds)) {
                this.mCenterTextLastBounds.set(rectF2);
                this.mCenterTextLastValue = centerText;
                this.mCenterTextLayout = new StaticLayout(centerText, 0, centerText.length(), this.mCenterTextPaint, (int)Math.max(Math.ceil(this.mCenterTextLastBounds.width()), 1.0), Layout$Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
            }
            final float n4 = (float)this.mCenterTextLayout.getHeight();
            canvas.save();
            if (Build$VERSION.SDK_INT >= 18) {
                final Path mDrawCenterTextPathBuffer = this.mDrawCenterTextPathBuffer;
                mDrawCenterTextPathBuffer.reset();
                mDrawCenterTextPathBuffer.addOval(rectF, Path$Direction.CW);
                canvas.clipPath(mDrawCenterTextPathBuffer);
            }
            canvas.translate(rectF2.left, rectF2.top + (rectF2.height() - n4) / 2.0f);
            this.mCenterTextLayout.draw(canvas);
            canvas.restore();
            MPPointF.recycleInstance(centerCircleBox);
            MPPointF.recycleInstance(centerTextOffset);
        }
    }
    
    @Override
    public void drawData(final Canvas canvas) {
        final int n = (int)this.mViewPortHandler.getChartWidth();
        final int n2 = (int)this.mViewPortHandler.getChartHeight();
        final WeakReference<Bitmap> mDrawBitmap = this.mDrawBitmap;
        Bitmap bitmap;
        if (mDrawBitmap == null) {
            bitmap = null;
        }
        else {
            bitmap = mDrawBitmap.get();
        }
        Bitmap bitmap2 = null;
        Label_0116: {
            if (bitmap != null && bitmap.getWidth() == n) {
                bitmap2 = bitmap;
                if (bitmap.getHeight() == n2) {
                    break Label_0116;
                }
            }
            if (n <= 0 || n2 <= 0) {
                return;
            }
            bitmap2 = Bitmap.createBitmap(n, n2, Bitmap$Config.ARGB_4444);
            this.mDrawBitmap = new WeakReference<Bitmap>(bitmap2);
            this.mBitmapCanvas = new Canvas(bitmap2);
        }
        bitmap2.eraseColor(0);
        for (final IPieDataSet set : this.mChart.getData().getDataSets()) {
            if (set.isVisible() && set.getEntryCount() > 0) {
                this.drawDataSet(canvas, set);
            }
        }
    }
    
    protected void drawDataSet(final Canvas canvas, final IPieDataSet set) {
        PieChartRenderer pieChartRenderer = this;
        final float rotationAngle = pieChartRenderer.mChart.getRotationAngle();
        final float phaseX = pieChartRenderer.mAnimator.getPhaseX();
        final float phaseY = pieChartRenderer.mAnimator.getPhaseY();
        final RectF circleBox = pieChartRenderer.mChart.getCircleBox();
        final int entryCount = set.getEntryCount();
        final float[] drawAngles = pieChartRenderer.mChart.getDrawAngles();
        final MPPointF centerCircleBox = pieChartRenderer.mChart.getCenterCircleBox();
        final float radius = pieChartRenderer.mChart.getRadius();
        final boolean b = pieChartRenderer.mChart.isDrawHoleEnabled() && !pieChartRenderer.mChart.isDrawSlicesUnderHoleEnabled();
        float a;
        if (b) {
            a = pieChartRenderer.mChart.getHoleRadius() / 100.0f * radius;
        }
        else {
            a = 0.0f;
        }
        final float n = (radius - pieChartRenderer.mChart.getHoleRadius() * radius / 100.0f) / 2.0f;
        RectF rectF = new RectF();
        final boolean b2 = b && pieChartRenderer.mChart.isDrawRoundedSlicesEnabled();
        int i = 0;
        int n2 = 0;
        while (i < entryCount) {
            int n3 = n2;
            if (Math.abs(set.getEntryForIndex(i).getY()) > Utils.FLOAT_EPSILON) {
                n3 = n2 + 1;
            }
            ++i;
            n2 = n3;
        }
        float sliceSpace;
        if (n2 <= 1) {
            sliceSpace = 0.0f;
        }
        else {
            sliceSpace = pieChartRenderer.getSliceSpace(set);
        }
        int j = 0;
        float n4 = 0.0f;
        while (j < entryCount) {
            final float n5 = drawAngles[j];
            float n30;
            if (Math.abs(set.getEntryForIndex(j).getY()) > Utils.FLOAT_EPSILON && (!pieChartRenderer.mChart.needsHighlight(j) || b2)) {
                final boolean b3 = sliceSpace > 0.0f && n5 <= 180.0f;
                pieChartRenderer.mRenderPaint.setColor(set.getColor(j));
                float n6;
                if (n2 == 1) {
                    n6 = 0.0f;
                }
                else {
                    n6 = sliceSpace / (radius * 0.017453292f);
                }
                final float n7 = rotationAngle + (n4 + n6 / 2.0f) * phaseY;
                float n8;
                if ((n8 = (n5 - n6) * phaseY) < 0.0f) {
                    n8 = 0.0f;
                }
                pieChartRenderer.mPathBuffer.reset();
                if (b2) {
                    final float x = centerCircleBox.x;
                    final float n9 = radius - n;
                    final double n10 = n7 * 0.017453292f;
                    final float n11 = x + (float)Math.cos(n10) * n9;
                    final float n12 = centerCircleBox.y + n9 * (float)Math.sin(n10);
                    rectF.set(n11 - n, n12 - n, n11 + n, n12 + n);
                }
                final float x2 = centerCircleBox.x;
                final double n13 = n7 * 0.017453292f;
                final float n14 = x2 + (float)Math.cos(n13) * radius;
                final float n15 = centerCircleBox.y + (float)Math.sin(n13) * radius;
                final float n16 = fcmpl(n8, 360.0f);
                if (n16 >= 0 && n8 % 360.0f <= Utils.FLOAT_EPSILON) {
                    pieChartRenderer.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, radius, Path$Direction.CW);
                }
                else {
                    if (b2) {
                        pieChartRenderer.mPathBuffer.arcTo(rectF, n7 + 180.0f, -180.0f);
                    }
                    pieChartRenderer.mPathBuffer.arcTo(circleBox, n7, n8);
                }
                pieChartRenderer.mInnerRectBuffer.set(centerCircleBox.x - a, centerCircleBox.y - a, centerCircleBox.x + a, centerCircleBox.y + a);
                Label_1251: {
                    PieChartRenderer pieChartRenderer2;
                    if (b && (a > 0.0f || b3)) {
                        float max;
                        if (b3) {
                            float calculateMinimumRadiusForSpacedSlice;
                            final float n17 = calculateMinimumRadiusForSpacedSlice = this.calculateMinimumRadiusForSpacedSlice(centerCircleBox, radius, n5 * phaseY, n14, n15, n7, n8);
                            if (n17 < 0.0f) {
                                calculateMinimumRadiusForSpacedSlice = -n17;
                            }
                            max = Math.max(a, calculateMinimumRadiusForSpacedSlice);
                        }
                        else {
                            max = a;
                        }
                        final float n18 = max;
                        float n19;
                        if (n2 != 1 && n18 != 0.0f) {
                            n19 = sliceSpace / (n18 * 0.017453292f);
                        }
                        else {
                            n19 = 0.0f;
                        }
                        final float n20 = n19 / 2.0f;
                        float n21;
                        if ((n21 = (n5 - n19) * phaseY) < 0.0f) {
                            n21 = 0.0f;
                        }
                        final float n22 = rotationAngle + (n4 + n20) * phaseY + n21;
                        if (n16 >= 0 && n8 % 360.0f <= Utils.FLOAT_EPSILON) {
                            this.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, n18, Path$Direction.CCW);
                        }
                        else {
                            if (b2) {
                                final float x3 = centerCircleBox.x;
                                final float n23 = radius - n;
                                final double n24 = n22 * 0.017453292f;
                                final float n25 = x3 + (float)Math.cos(n24) * n23;
                                final float n26 = centerCircleBox.y + n23 * (float)Math.sin(n24);
                                rectF.set(n25 - n, n26 - n, n25 + n, n26 + n);
                                this.mPathBuffer.arcTo(rectF, n22, 180.0f);
                            }
                            else {
                                final Path mPathBuffer = this.mPathBuffer;
                                final float x4 = centerCircleBox.x;
                                final double n27 = n22 * 0.017453292f;
                                mPathBuffer.lineTo(x4 + (float)Math.cos(n27) * n18, centerCircleBox.y + n18 * (float)Math.sin(n27));
                            }
                            this.mPathBuffer.arcTo(this.mInnerRectBuffer, n22, -n21);
                        }
                        pieChartRenderer2 = this;
                    }
                    else {
                        final MPPointF mpPointF = centerCircleBox;
                        final RectF rectF2 = rectF;
                        pieChartRenderer2 = pieChartRenderer;
                        if (n8 % 360.0f > Utils.FLOAT_EPSILON) {
                            if (b3) {
                                final float n28 = n8 / 2.0f;
                                rectF = rectF2;
                                final float calculateMinimumRadiusForSpacedSlice2 = this.calculateMinimumRadiusForSpacedSlice(mpPointF, radius, n5 * phaseY, n14, n15, n7, n8);
                                final float x5 = mpPointF.x;
                                final double n29 = (n7 + n28) * 0.017453292f;
                                pieChartRenderer.mPathBuffer.lineTo(x5 + (float)Math.cos(n29) * calculateMinimumRadiusForSpacedSlice2, mpPointF.y + calculateMinimumRadiusForSpacedSlice2 * (float)Math.sin(n29));
                                break Label_1251;
                            }
                            rectF = rectF2;
                            pieChartRenderer.mPathBuffer.lineTo(mpPointF.x, mpPointF.y);
                            break Label_1251;
                        }
                    }
                    pieChartRenderer = pieChartRenderer2;
                }
                pieChartRenderer.mPathBuffer.close();
                pieChartRenderer.mBitmapCanvas.drawPath(pieChartRenderer.mPathBuffer, pieChartRenderer.mRenderPaint);
                n30 = n4 + n5 * phaseX;
            }
            else {
                n30 = n4 + n5 * phaseX;
            }
            ++j;
            n4 = n30;
        }
        MPPointF.recycleInstance(centerCircleBox);
    }
    
    protected void drawEntryLabel(final Canvas canvas, final String s, final float n, final float n2) {
        canvas.drawText(s, n, n2, this.mEntryLabelsPaint);
    }
    
    @Override
    public void drawExtras(final Canvas canvas) {
        this.drawHole(canvas);
        canvas.drawBitmap((Bitmap)this.mDrawBitmap.get(), 0.0f, 0.0f, (Paint)null);
        this.drawCenterText(canvas);
    }
    
    @Override
    public void drawHighlighted(final Canvas canvas, final Highlight[] array) {
        final boolean b = this.mChart.isDrawHoleEnabled() && !this.mChart.isDrawSlicesUnderHoleEnabled();
        if (b && this.mChart.isDrawRoundedSlicesEnabled()) {
            return;
        }
        final float phaseX = this.mAnimator.getPhaseX();
        final float phaseY = this.mAnimator.getPhaseY();
        final float rotationAngle = this.mChart.getRotationAngle();
        final float[] drawAngles = this.mChart.getDrawAngles();
        final float[] absoluteAngles = this.mChart.getAbsoluteAngles();
        final MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
        final float radius = this.mChart.getRadius();
        float a;
        if (b) {
            a = this.mChart.getHoleRadius() / 100.0f * radius;
        }
        else {
            a = 0.0f;
        }
        final RectF mDrawHighlightedRectF = this.mDrawHighlightedRectF;
        mDrawHighlightedRectF.set(0.0f, 0.0f, 0.0f, 0.0f);
        for (int i = 0; i < array.length; ++i) {
            final int n = (int)array[i].getX();
            if (n < drawAngles.length) {
                final IPieDataSet dataSetByIndex = this.mChart.getData().getDataSetByIndex(array[i].getDataSetIndex());
                if (dataSetByIndex != null) {
                    if (dataSetByIndex.isHighlightEnabled()) {
                        final int entryCount = dataSetByIndex.getEntryCount();
                        int j = 0;
                        int n2 = 0;
                        while (j < entryCount) {
                            int n3 = n2;
                            if (Math.abs(dataSetByIndex.getEntryForIndex(j).getY()) > Utils.FLOAT_EPSILON) {
                                n3 = n2 + 1;
                            }
                            ++j;
                            n2 = n3;
                        }
                        float n4;
                        if (n == 0) {
                            n4 = 0.0f;
                        }
                        else {
                            n4 = absoluteAngles[n - 1] * phaseX;
                        }
                        float sliceSpace;
                        if (n2 <= 1) {
                            sliceSpace = 0.0f;
                        }
                        else {
                            sliceSpace = dataSetByIndex.getSliceSpace();
                        }
                        final float n5 = drawAngles[n];
                        final float selectionShift = dataSetByIndex.getSelectionShift();
                        final float n6 = radius + selectionShift;
                        mDrawHighlightedRectF.set(this.mChart.getCircleBox());
                        final float n7 = -selectionShift;
                        mDrawHighlightedRectF.inset(n7, n7);
                        final boolean b2 = sliceSpace > 0.0f && n5 <= 180.0f;
                        this.mRenderPaint.setColor(dataSetByIndex.getColor(n));
                        float n8;
                        if (n2 == 1) {
                            n8 = 0.0f;
                        }
                        else {
                            n8 = sliceSpace / (radius * 0.017453292f);
                        }
                        float n9;
                        if (n2 == 1) {
                            n9 = 0.0f;
                        }
                        else {
                            n9 = sliceSpace / (n6 * 0.017453292f);
                        }
                        final float n10 = rotationAngle + (n8 / 2.0f + n4) * phaseY;
                        float n11 = (n5 - n8) * phaseY;
                        if (n11 < 0.0f) {
                            n11 = 0.0f;
                        }
                        final float n12 = (n9 / 2.0f + n4) * phaseY + rotationAngle;
                        float n13;
                        if ((n13 = (n5 - n9) * phaseY) < 0.0f) {
                            n13 = 0.0f;
                        }
                        this.mPathBuffer.reset();
                        final float n14 = fcmpl(n11, 360.0f);
                        if (n14 >= 0 && n11 % 360.0f <= Utils.FLOAT_EPSILON) {
                            this.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, n6, Path$Direction.CW);
                        }
                        else {
                            final Path mPathBuffer = this.mPathBuffer;
                            final float x = centerCircleBox.x;
                            final double n15 = n12 * 0.017453292f;
                            mPathBuffer.moveTo(x + (float)Math.cos(n15) * n6, centerCircleBox.y + n6 * (float)Math.sin(n15));
                            this.mPathBuffer.arcTo(mDrawHighlightedRectF, n12, n13);
                        }
                        float calculateMinimumRadiusForSpacedSlice;
                        if (b2) {
                            final float x2 = centerCircleBox.x;
                            final double n16 = n10 * 0.017453292f;
                            calculateMinimumRadiusForSpacedSlice = this.calculateMinimumRadiusForSpacedSlice(centerCircleBox, radius, n5 * phaseY, (float)Math.cos(n16) * radius + x2, centerCircleBox.y + (float)Math.sin(n16) * radius, n10, n11);
                        }
                        else {
                            calculateMinimumRadiusForSpacedSlice = 0.0f;
                        }
                        this.mInnerRectBuffer.set(centerCircleBox.x - a, centerCircleBox.y - a, centerCircleBox.x + a, centerCircleBox.y + a);
                        if (b && (a > 0.0f || b2)) {
                            float max;
                            if (b2) {
                                float b3 = calculateMinimumRadiusForSpacedSlice;
                                if (calculateMinimumRadiusForSpacedSlice < 0.0f) {
                                    b3 = -calculateMinimumRadiusForSpacedSlice;
                                }
                                max = Math.max(a, b3);
                            }
                            else {
                                max = a;
                            }
                            float n17;
                            if (n2 != 1 && max != 0.0f) {
                                n17 = sliceSpace / (max * 0.017453292f);
                            }
                            else {
                                n17 = 0.0f;
                            }
                            final float n18 = n17 / 2.0f;
                            float n19;
                            if ((n19 = (n5 - n17) * phaseY) < 0.0f) {
                                n19 = 0.0f;
                            }
                            final float n20 = (n4 + n18) * phaseY + rotationAngle + n19;
                            if (n14 >= 0 && n11 % 360.0f <= Utils.FLOAT_EPSILON) {
                                this.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, max, Path$Direction.CCW);
                            }
                            else {
                                final Path mPathBuffer2 = this.mPathBuffer;
                                final float x3 = centerCircleBox.x;
                                final double n21 = n20 * 0.017453292f;
                                mPathBuffer2.lineTo(x3 + (float)Math.cos(n21) * max, centerCircleBox.y + max * (float)Math.sin(n21));
                                this.mPathBuffer.arcTo(this.mInnerRectBuffer, n20, -n19);
                            }
                        }
                        else if (n11 % 360.0f > Utils.FLOAT_EPSILON) {
                            if (b2) {
                                final float n22 = n11 / 2.0f;
                                final float x4 = centerCircleBox.x;
                                final double n23 = (n10 + n22) * 0.017453292f;
                                this.mPathBuffer.lineTo(x4 + (float)Math.cos(n23) * calculateMinimumRadiusForSpacedSlice, centerCircleBox.y + calculateMinimumRadiusForSpacedSlice * (float)Math.sin(n23));
                            }
                            else {
                                this.mPathBuffer.lineTo(centerCircleBox.x, centerCircleBox.y);
                            }
                        }
                        this.mPathBuffer.close();
                        this.mBitmapCanvas.drawPath(this.mPathBuffer, this.mRenderPaint);
                    }
                }
            }
        }
        MPPointF.recycleInstance(centerCircleBox);
    }
    
    protected void drawHole(final Canvas canvas) {
        if (this.mChart.isDrawHoleEnabled() && this.mBitmapCanvas != null) {
            final float radius = this.mChart.getRadius();
            final float n = this.mChart.getHoleRadius() / 100.0f * radius;
            final MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
            if (Color.alpha(this.mHolePaint.getColor()) > 0) {
                this.mBitmapCanvas.drawCircle(centerCircleBox.x, centerCircleBox.y, n, this.mHolePaint);
            }
            if (Color.alpha(this.mTransparentCirclePaint.getColor()) > 0 && this.mChart.getTransparentCircleRadius() > this.mChart.getHoleRadius()) {
                final int alpha = this.mTransparentCirclePaint.getAlpha();
                final float n2 = this.mChart.getTransparentCircleRadius() / 100.0f;
                this.mTransparentCirclePaint.setAlpha((int)(alpha * this.mAnimator.getPhaseX() * this.mAnimator.getPhaseY()));
                this.mHoleCirclePath.reset();
                this.mHoleCirclePath.addCircle(centerCircleBox.x, centerCircleBox.y, radius * n2, Path$Direction.CW);
                this.mHoleCirclePath.addCircle(centerCircleBox.x, centerCircleBox.y, n, Path$Direction.CCW);
                this.mBitmapCanvas.drawPath(this.mHoleCirclePath, this.mTransparentCirclePaint);
                this.mTransparentCirclePaint.setAlpha(alpha);
            }
            MPPointF.recycleInstance(centerCircleBox);
        }
    }
    
    protected void drawRoundedSlices(final Canvas canvas) {
        if (!this.mChart.isDrawRoundedSlicesEnabled()) {
            return;
        }
        final IPieDataSet dataSet = this.mChart.getData().getDataSet();
        if (!dataSet.isVisible()) {
            return;
        }
        final float phaseX = this.mAnimator.getPhaseX();
        final float phaseY = this.mAnimator.getPhaseY();
        final MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
        final float radius = this.mChart.getRadius();
        final float n = (radius - this.mChart.getHoleRadius() * radius / 100.0f) / 2.0f;
        final float[] drawAngles = this.mChart.getDrawAngles();
        float rotationAngle = this.mChart.getRotationAngle();
        for (int i = 0; i < dataSet.getEntryCount(); ++i) {
            final float n2 = drawAngles[i];
            if (Math.abs(((IDataSet<Entry>)dataSet).getEntryForIndex(i).getY()) > Utils.FLOAT_EPSILON) {
                final double n3 = radius - n;
                final double n4 = (rotationAngle + n2) * phaseY;
                final float n5 = (float)(centerCircleBox.x + Math.cos(Math.toRadians(n4)) * n3);
                final float n6 = (float)(n3 * Math.sin(Math.toRadians(n4)) + centerCircleBox.y);
                this.mRenderPaint.setColor(dataSet.getColor(i));
                this.mBitmapCanvas.drawCircle(n5, n6, n, this.mRenderPaint);
            }
            rotationAngle += n2 * phaseX;
        }
        MPPointF.recycleInstance(centerCircleBox);
    }
    
    @Override
    public void drawValue(final Canvas canvas, final String s, final float n, final float n2, final int color) {
        this.mValuePaint.setColor(color);
        canvas.drawText(s, n, n2, this.mValuePaint);
    }
    
    @Override
    public void drawValues(final Canvas canvas) {
        final MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
        float radius = this.mChart.getRadius();
        final float rotationAngle = this.mChart.getRotationAngle();
        float[] drawAngles = this.mChart.getDrawAngles();
        float[] absoluteAngles = this.mChart.getAbsoluteAngles();
        float phaseX = this.mAnimator.getPhaseX();
        final float phaseY = this.mAnimator.getPhaseY();
        final float n = (radius - this.mChart.getHoleRadius() * radius / 100.0f) / 2.0f;
        final float n2 = this.mChart.getHoleRadius() / 100.0f;
        float n3 = radius / 10.0f * 3.6f;
        float n4 = rotationAngle;
        if (this.mChart.isDrawHoleEnabled()) {
            final float n5 = (radius - radius * n2) / 2.0f;
            n4 = rotationAngle;
            n3 = n5;
            if (!this.mChart.isDrawSlicesUnderHoleEnabled()) {
                n4 = rotationAngle;
                n3 = n5;
                if (this.mChart.isDrawRoundedSlicesEnabled()) {
                    n4 = (float)(rotationAngle + n * 360.0f / (radius * 6.283185307179586));
                    n3 = n5;
                }
            }
        }
        float n6 = radius - n3;
        final PieData pieData = this.mChart.getData();
        final List<IPieDataSet> dataSets = pieData.getDataSets();
        final float yValueSum = pieData.getYValueSum();
        final boolean drawEntryLabelsEnabled = this.mChart.isDrawEntryLabelsEnabled();
        canvas.save();
        final float convertDpToPixel = Utils.convertDpToPixel(5.0f);
        int n7 = 0;
        int i = 0;
        final float n8 = n4;
        float n9 = phaseY;
        while (i < dataSets.size()) {
            final IPieDataSet set = dataSets.get(i);
            final boolean drawValuesEnabled = set.isDrawValuesEnabled();
            float[] array2;
            float n11;
            float[] array3;
            float n12;
            float n13;
            float n14;
            if (!drawValuesEnabled && !drawEntryLabelsEnabled) {
                final float n10 = radius;
                final float[] array = absoluteAngles;
                array2 = drawAngles;
                n11 = n6;
                array3 = array;
                n12 = phaseX;
                n13 = n9;
                n14 = n10;
            }
            else {
                final PieDataSet.ValuePosition xValuePosition = set.getXValuePosition();
                final PieDataSet.ValuePosition yValuePosition = set.getYValuePosition();
                this.applyValueTextStyle(set);
                final float n15 = Utils.calcTextHeight(this.mValuePaint, "Q") + Utils.convertDpToPixel(4.0f);
                final ValueFormatter valueFormatter = set.getValueFormatter();
                final int entryCount = set.getEntryCount();
                this.mValueLinePaint.setColor(set.getValueLineColor());
                this.mValueLinePaint.setStrokeWidth(Utils.convertDpToPixel(set.getValueLineWidth()));
                final float sliceSpace = this.getSliceSpace(set);
                final MPPointF instance = MPPointF.getInstance(set.getIconsOffset());
                instance.x = Utils.convertDpToPixel(instance.x);
                instance.y = Utils.convertDpToPixel(instance.y);
                for (int j = 0; j < entryCount; ++j) {
                    final PieEntry pieEntry = set.getEntryForIndex(j);
                    float n16;
                    if (n7 == 0) {
                        n16 = 0.0f;
                    }
                    else {
                        n16 = absoluteAngles[n7 - 1] * phaseX;
                    }
                    final float n17 = n8 + (n16 + (drawAngles[n7] - sliceSpace / (n6 * 0.017453292f) / 2.0f) / 2.0f) * n9;
                    float y;
                    if (this.mChart.isUsePercentValuesEnabled()) {
                        y = pieEntry.getY() / yValueSum * 100.0f;
                    }
                    else {
                        y = pieEntry.getY();
                    }
                    final String pieLabel = valueFormatter.getPieLabel(y, pieEntry);
                    final String label = pieEntry.getLabel();
                    final double a = n17 * 0.017453292f;
                    final float n18 = (float)Math.cos(a);
                    final float n19 = (float)Math.sin(a);
                    final boolean b = drawEntryLabelsEnabled && xValuePosition == PieDataSet.ValuePosition.OUTSIDE_SLICE;
                    final boolean b2 = drawValuesEnabled && yValuePosition == PieDataSet.ValuePosition.OUTSIDE_SLICE;
                    final boolean b3 = drawEntryLabelsEnabled && xValuePosition == PieDataSet.ValuePosition.INSIDE_SLICE;
                    final boolean b4 = drawValuesEnabled && yValuePosition == PieDataSet.ValuePosition.INSIDE_SLICE;
                    if (b || b2) {
                        final float valueLinePart1Length = set.getValueLinePart1Length();
                        final float valueLinePart2Length = set.getValueLinePart2Length();
                        final float n20 = set.getValueLinePart1OffsetPercentage() / 100.0f;
                        float n22;
                        if (this.mChart.isDrawHoleEnabled()) {
                            final float n21 = radius * n2;
                            n22 = (radius - n21) * n20 + n21;
                        }
                        else {
                            n22 = radius * n20;
                        }
                        float n23;
                        if (set.isValueLineVariableLength()) {
                            n23 = valueLinePart2Length * n6 * (float)Math.abs(Math.sin(a));
                        }
                        else {
                            n23 = valueLinePart2Length * n6;
                        }
                        final float x = centerCircleBox.x;
                        final float y2 = centerCircleBox.y;
                        final float n24 = (valueLinePart1Length + 1.0f) * n6;
                        final float n25 = n24 * n18 + centerCircleBox.x;
                        final float n26 = centerCircleBox.y + n24 * n19;
                        final double n27 = n17 % 360.0;
                        float n29;
                        float n30;
                        if (n27 >= 90.0 && n27 <= 270.0) {
                            final float n28 = n25 - n23;
                            this.mValuePaint.setTextAlign(Paint$Align.RIGHT);
                            if (b) {
                                this.mEntryLabelsPaint.setTextAlign(Paint$Align.RIGHT);
                            }
                            n29 = n28;
                            n30 = n28 - convertDpToPixel;
                        }
                        else {
                            n29 = n25 + n23;
                            this.mValuePaint.setTextAlign(Paint$Align.LEFT);
                            if (b) {
                                this.mEntryLabelsPaint.setTextAlign(Paint$Align.LEFT);
                            }
                            n30 = n29 + convertDpToPixel;
                        }
                        if (set.getValueLineColor() != 1122867) {
                            if (set.isUsingSliceColorAsValueLineColor()) {
                                this.mValueLinePaint.setColor(set.getColor(j));
                            }
                            canvas.drawLine(n22 * n18 + x, n22 * n19 + y2, n25, n26, this.mValueLinePaint);
                            canvas.drawLine(n25, n26, n29, n26, this.mValueLinePaint);
                        }
                        if (b && b2) {
                            this.drawValue(canvas, pieLabel, n30, n26, set.getValueTextColor(j));
                            if (j < pieData.getEntryCount() && label != null) {
                                this.drawEntryLabel(canvas, label, n30, n26 + n15);
                            }
                        }
                        else if (b) {
                            if (j < pieData.getEntryCount() && label != null) {
                                this.drawEntryLabel(canvas, label, n30, n26 + n15 / 2.0f);
                            }
                        }
                        else if (b2) {
                            this.drawValue(canvas, pieLabel, n30, n26 + n15 / 2.0f, set.getValueTextColor(j));
                        }
                    }
                    if (b3 || b4) {
                        final float n31 = n6 * n18 + centerCircleBox.x;
                        final float n32 = n6 * n19 + centerCircleBox.y;
                        this.mValuePaint.setTextAlign(Paint$Align.CENTER);
                        if (b3 && b4) {
                            this.drawValue(canvas, pieLabel, n31, n32, set.getValueTextColor(j));
                            if (j < pieData.getEntryCount() && label != null) {
                                this.drawEntryLabel(canvas, label, n31, n32 + n15);
                            }
                        }
                        else if (b3) {
                            if (j < pieData.getEntryCount() && label != null) {
                                this.drawEntryLabel(canvas, label, n31, n32 + n15 / 2.0f);
                            }
                        }
                        else if (b4) {
                            this.drawValue(canvas, pieLabel, n31, n32 + n15 / 2.0f, set.getValueTextColor(j));
                        }
                    }
                    if (pieEntry.getIcon() != null && set.isDrawIconsEnabled()) {
                        final Drawable icon = pieEntry.getIcon();
                        Utils.drawImage(canvas, icon, (int)((n6 + instance.y) * n18 + centerCircleBox.x), (int)((n6 + instance.y) * n19 + centerCircleBox.y + instance.x), icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
                    }
                    ++n7;
                }
                final float n33 = radius;
                final float[] array4 = drawAngles;
                array3 = absoluteAngles;
                final float n34 = phaseX;
                n13 = n9;
                final float n35 = n6;
                MPPointF.recycleInstance(instance);
                n14 = n33;
                n12 = n34;
                n11 = n35;
                array2 = array4;
            }
            ++i;
            final float[] array5 = array2;
            final float n36 = n11;
            absoluteAngles = array3;
            final float n37 = n13;
            radius = n14;
            drawAngles = array5;
            phaseX = n12;
            n9 = n37;
            n6 = n36;
        }
        MPPointF.recycleInstance(centerCircleBox);
        canvas.restore();
    }
    
    public TextPaint getPaintCenterText() {
        return this.mCenterTextPaint;
    }
    
    public Paint getPaintEntryLabels() {
        return this.mEntryLabelsPaint;
    }
    
    public Paint getPaintHole() {
        return this.mHolePaint;
    }
    
    public Paint getPaintTransparentCircle() {
        return this.mTransparentCirclePaint;
    }
    
    protected float getSliceSpace(final IPieDataSet set) {
        if (!set.isAutomaticallyDisableSliceSpacingEnabled()) {
            return set.getSliceSpace();
        }
        float sliceSpace;
        if (set.getSliceSpace() / this.mViewPortHandler.getSmallestContentExtension() > set.getYMin() / this.mChart.getData().getYValueSum() * 2.0f) {
            sliceSpace = 0.0f;
        }
        else {
            sliceSpace = set.getSliceSpace();
        }
        return sliceSpace;
    }
    
    @Override
    public void initBuffers() {
    }
    
    public void releaseBitmap() {
        final Canvas mBitmapCanvas = this.mBitmapCanvas;
        if (mBitmapCanvas != null) {
            mBitmapCanvas.setBitmap((Bitmap)null);
            this.mBitmapCanvas = null;
        }
        final WeakReference<Bitmap> mDrawBitmap = this.mDrawBitmap;
        if (mDrawBitmap != null) {
            final Bitmap bitmap = mDrawBitmap.get();
            if (bitmap != null) {
                bitmap.recycle();
            }
            this.mDrawBitmap.clear();
            this.mDrawBitmap = null;
        }
    }
}

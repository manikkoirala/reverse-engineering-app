// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.renderer;

import android.graphics.Path$Direction;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.interfaces.dataprovider.ChartInterface;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.interfaces.datasets.ILineScatterCandleRadarDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import java.util.Iterator;
import android.graphics.drawable.Drawable;
import android.graphics.PathEffect;
import com.github.mikephil.charting.utils.Transformer;
import java.util.List;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;
import com.github.mikephil.charting.interfaces.dataprovider.BarLineScatterCandleBubbleDataProvider;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import android.graphics.Paint$Style;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import java.util.HashMap;
import android.graphics.Bitmap;
import java.lang.ref.WeakReference;
import android.graphics.Paint;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import android.graphics.Bitmap$Config;
import android.graphics.Canvas;
import android.graphics.Path;

public class LineChartRenderer extends LineRadarRenderer
{
    protected Path cubicFillPath;
    protected Path cubicPath;
    protected Canvas mBitmapCanvas;
    protected Bitmap$Config mBitmapConfig;
    protected LineDataProvider mChart;
    protected Paint mCirclePaintInner;
    private float[] mCirclesBuffer;
    protected WeakReference<Bitmap> mDrawBitmap;
    protected Path mGenerateFilledPathBuffer;
    private HashMap<IDataSet, DataSetImageCache> mImageCaches;
    private float[] mLineBuffer;
    
    public LineChartRenderer(final LineDataProvider mChart, final ChartAnimator chartAnimator, final ViewPortHandler viewPortHandler) {
        super(chartAnimator, viewPortHandler);
        this.mBitmapConfig = Bitmap$Config.ARGB_8888;
        this.cubicPath = new Path();
        this.cubicFillPath = new Path();
        this.mLineBuffer = new float[4];
        this.mGenerateFilledPathBuffer = new Path();
        this.mImageCaches = new HashMap<IDataSet, DataSetImageCache>();
        this.mCirclesBuffer = new float[2];
        this.mChart = mChart;
        (this.mCirclePaintInner = new Paint(1)).setStyle(Paint$Style.FILL);
        this.mCirclePaintInner.setColor(-1);
    }
    
    private void generateFilledPath(final ILineDataSet set, int i, final int n, final Path path) {
        final float fillLinePosition = set.getFillFormatter().getFillLinePosition(set, this.mChart);
        final float phaseY = this.mAnimator.getPhaseY();
        final boolean b = set.getMode() == LineDataSet.Mode.STEPPED;
        path.reset();
        Entry entryForIndex = set.getEntryForIndex(i);
        path.moveTo(entryForIndex.getX(), fillLinePosition);
        path.lineTo(entryForIndex.getX(), entryForIndex.getY() * phaseY);
        Entry entryForIndex2 = null;
        ++i;
        while (i <= n) {
            entryForIndex2 = set.getEntryForIndex(i);
            if (b) {
                path.lineTo(entryForIndex2.getX(), entryForIndex.getY() * phaseY);
            }
            path.lineTo(entryForIndex2.getX(), entryForIndex2.getY() * phaseY);
            ++i;
            entryForIndex = entryForIndex2;
        }
        if (entryForIndex2 != null) {
            path.lineTo(entryForIndex2.getX(), fillLinePosition);
        }
        path.close();
    }
    
    protected void drawCircles(final Canvas canvas) {
        this.mRenderPaint.setStyle(Paint$Style.FILL);
        final float phaseY = this.mAnimator.getPhaseY();
        final float[] mCirclesBuffer = this.mCirclesBuffer;
        mCirclesBuffer[1] = (mCirclesBuffer[0] = 0.0f);
        final List<ILineDataSet> dataSets = this.mChart.getLineData().getDataSets();
        for (int i = 0; i < dataSets.size(); ++i) {
            final ILineDataSet key = dataSets.get(i);
            if (key.isVisible() && key.isDrawCirclesEnabled()) {
                if (key.getEntryCount() != 0) {
                    this.mCirclePaintInner.setColor(key.getCircleHoleColor());
                    final Transformer transformer = this.mChart.getTransformer(key.getAxisDependency());
                    this.mXBounds.set(this.mChart, key);
                    final float circleRadius = key.getCircleRadius();
                    final float circleHoleRadius = key.getCircleHoleRadius();
                    final boolean b = key.isDrawCircleHoleEnabled() && circleHoleRadius < circleRadius && circleHoleRadius > 0.0f;
                    final boolean b2 = b && key.getCircleHoleColor() == 1122867;
                    DataSetImageCache value;
                    if (this.mImageCaches.containsKey(key)) {
                        value = this.mImageCaches.get(key);
                    }
                    else {
                        value = new DataSetImageCache();
                        this.mImageCaches.put(key, value);
                    }
                    if (value.init(key)) {
                        value.fill(key, b, b2);
                    }
                    for (int range = this.mXBounds.range, min = this.mXBounds.min, j = this.mXBounds.min; j <= range + min; ++j) {
                        final Entry entryForIndex = key.getEntryForIndex(j);
                        if (entryForIndex == null) {
                            break;
                        }
                        this.mCirclesBuffer[0] = entryForIndex.getX();
                        this.mCirclesBuffer[1] = entryForIndex.getY() * phaseY;
                        transformer.pointValuesToPixel(this.mCirclesBuffer);
                        if (!this.mViewPortHandler.isInBoundsRight(this.mCirclesBuffer[0])) {
                            break;
                        }
                        if (this.mViewPortHandler.isInBoundsLeft(this.mCirclesBuffer[0])) {
                            if (this.mViewPortHandler.isInBoundsY(this.mCirclesBuffer[1])) {
                                final Bitmap bitmap = value.getBitmap(j);
                                if (bitmap != null) {
                                    final float[] mCirclesBuffer2 = this.mCirclesBuffer;
                                    canvas.drawBitmap(bitmap, mCirclesBuffer2[0] - circleRadius, mCirclesBuffer2[1] - circleRadius, (Paint)null);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    protected void drawCubicBezier(final ILineDataSet set) {
        final float phaseY = this.mAnimator.getPhaseY();
        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
        this.mXBounds.set(this.mChart, set);
        final float cubicIntensity = set.getCubicIntensity();
        this.cubicPath.reset();
        if (this.mXBounds.range >= 1) {
            final int n = this.mXBounds.min + 1;
            final int min = this.mXBounds.min;
            final int range = this.mXBounds.range;
            Entry entryForIndex = set.getEntryForIndex(Math.max(n - 2, 0));
            Entry entry = set.getEntryForIndex(Math.max(n - 1, 0));
            int n2 = -1;
            if (entry == null) {
                return;
            }
            this.cubicPath.moveTo(entry.getX(), entry.getY() * phaseY);
            int i = this.mXBounds.min + 1;
            Entry entry2 = entry;
            while (i <= this.mXBounds.range + this.mXBounds.min) {
                if (n2 != i) {
                    entry = set.getEntryForIndex(i);
                }
                final int n3 = i + 1;
                if (n3 < set.getEntryCount()) {
                    i = n3;
                }
                final Entry entryForIndex2 = set.getEntryForIndex(i);
                this.cubicPath.cubicTo(entry2.getX() + (entry.getX() - entryForIndex.getX()) * cubicIntensity, (entry2.getY() + (entry.getY() - entryForIndex.getY()) * cubicIntensity) * phaseY, entry.getX() - (entryForIndex2.getX() - entry2.getX()) * cubicIntensity, (entry.getY() - (entryForIndex2.getY() - entry2.getY()) * cubicIntensity) * phaseY, entry.getX(), entry.getY() * phaseY);
                entryForIndex = entry2;
                entry2 = entry;
                entry = entryForIndex2;
                n2 = i;
                i = n3;
            }
        }
        if (set.isDrawFilledEnabled()) {
            this.cubicFillPath.reset();
            this.cubicFillPath.addPath(this.cubicPath);
            this.drawCubicFill(this.mBitmapCanvas, set, this.cubicFillPath, transformer, this.mXBounds);
        }
        this.mRenderPaint.setColor(set.getColor());
        this.mRenderPaint.setStyle(Paint$Style.STROKE);
        transformer.pathValueToPixel(this.cubicPath);
        this.mBitmapCanvas.drawPath(this.cubicPath, this.mRenderPaint);
        this.mRenderPaint.setPathEffect((PathEffect)null);
    }
    
    protected void drawCubicFill(final Canvas canvas, final ILineDataSet set, final Path path, final Transformer transformer, final XBounds xBounds) {
        final float fillLinePosition = set.getFillFormatter().getFillLinePosition(set, this.mChart);
        path.lineTo(set.getEntryForIndex(xBounds.min + xBounds.range).getX(), fillLinePosition);
        path.lineTo(set.getEntryForIndex(xBounds.min).getX(), fillLinePosition);
        path.close();
        transformer.pathValueToPixel(path);
        final Drawable fillDrawable = set.getFillDrawable();
        if (fillDrawable != null) {
            this.drawFilledPath(canvas, path, fillDrawable);
        }
        else {
            this.drawFilledPath(canvas, path, set.getFillColor(), set.getFillAlpha());
        }
    }
    
    @Override
    public void drawData(final Canvas canvas) {
        final int n = (int)this.mViewPortHandler.getChartWidth();
        final int n2 = (int)this.mViewPortHandler.getChartHeight();
        final WeakReference<Bitmap> mDrawBitmap = this.mDrawBitmap;
        Bitmap bitmap;
        if (mDrawBitmap == null) {
            bitmap = null;
        }
        else {
            bitmap = mDrawBitmap.get();
        }
        Bitmap bitmap2 = null;
        Label_0117: {
            if (bitmap != null && bitmap.getWidth() == n) {
                bitmap2 = bitmap;
                if (bitmap.getHeight() == n2) {
                    break Label_0117;
                }
            }
            if (n <= 0 || n2 <= 0) {
                return;
            }
            bitmap2 = Bitmap.createBitmap(n, n2, this.mBitmapConfig);
            this.mDrawBitmap = new WeakReference<Bitmap>(bitmap2);
            this.mBitmapCanvas = new Canvas(bitmap2);
        }
        bitmap2.eraseColor(0);
        for (final ILineDataSet set : this.mChart.getLineData().getDataSets()) {
            if (set.isVisible()) {
                this.drawDataSet(canvas, set);
            }
        }
        canvas.drawBitmap(bitmap2, 0.0f, 0.0f, this.mRenderPaint);
    }
    
    protected void drawDataSet(final Canvas canvas, final ILineDataSet set) {
        if (set.getEntryCount() < 1) {
            return;
        }
        this.mRenderPaint.setStrokeWidth(set.getLineWidth());
        this.mRenderPaint.setPathEffect((PathEffect)set.getDashPathEffect());
        final int n = LineChartRenderer$1.$SwitchMap$com$github$mikephil$charting$data$LineDataSet$Mode[set.getMode().ordinal()];
        if (n != 3) {
            if (n != 4) {
                this.drawLinear(canvas, set);
            }
            else {
                this.drawHorizontalBezier(set);
            }
        }
        else {
            this.drawCubicBezier(set);
        }
        this.mRenderPaint.setPathEffect((PathEffect)null);
    }
    
    @Override
    public void drawExtras(final Canvas canvas) {
        this.drawCircles(canvas);
    }
    
    @Override
    public void drawHighlighted(final Canvas canvas, final Highlight[] array) {
        final LineData lineData = this.mChart.getLineData();
        for (final Highlight highlight : array) {
            final ILineDataSet set = lineData.getDataSetByIndex(highlight.getDataSetIndex());
            if (set != null) {
                if (set.isHighlightEnabled()) {
                    final Entry entryForXValue = set.getEntryForXValue(highlight.getX(), highlight.getY());
                    if (this.isInBoundsX(entryForXValue, set)) {
                        final MPPointD pixelForValues = this.mChart.getTransformer(set.getAxisDependency()).getPixelForValues(entryForXValue.getX(), entryForXValue.getY() * this.mAnimator.getPhaseY());
                        highlight.setDraw((float)pixelForValues.x, (float)pixelForValues.y);
                        this.drawHighlightLines(canvas, (float)pixelForValues.x, (float)pixelForValues.y, set);
                    }
                }
            }
        }
    }
    
    protected void drawHorizontalBezier(final ILineDataSet set) {
        final float phaseY = this.mAnimator.getPhaseY();
        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
        this.mXBounds.set(this.mChart, set);
        this.cubicPath.reset();
        if (this.mXBounds.range >= 1) {
            Entry entryForIndex = set.getEntryForIndex(this.mXBounds.min);
            this.cubicPath.moveTo(entryForIndex.getX(), entryForIndex.getY() * phaseY);
            Entry entryForIndex2;
            for (int i = this.mXBounds.min + 1; i <= this.mXBounds.range + this.mXBounds.min; ++i, entryForIndex = entryForIndex2) {
                entryForIndex2 = set.getEntryForIndex(i);
                final float n = entryForIndex.getX() + (entryForIndex2.getX() - entryForIndex.getX()) / 2.0f;
                this.cubicPath.cubicTo(n, entryForIndex.getY() * phaseY, n, entryForIndex2.getY() * phaseY, entryForIndex2.getX(), entryForIndex2.getY() * phaseY);
            }
        }
        if (set.isDrawFilledEnabled()) {
            this.cubicFillPath.reset();
            this.cubicFillPath.addPath(this.cubicPath);
            this.drawCubicFill(this.mBitmapCanvas, set, this.cubicFillPath, transformer, this.mXBounds);
        }
        this.mRenderPaint.setColor(set.getColor());
        this.mRenderPaint.setStyle(Paint$Style.STROKE);
        transformer.pathValueToPixel(this.cubicPath);
        this.mBitmapCanvas.drawPath(this.cubicPath, this.mRenderPaint);
        this.mRenderPaint.setPathEffect((PathEffect)null);
    }
    
    protected void drawLinear(final Canvas canvas, final ILineDataSet set) {
        final int entryCount = set.getEntryCount();
        final boolean b = set.getMode() == LineDataSet.Mode.STEPPED;
        int b2;
        if (b) {
            b2 = 4;
        }
        else {
            b2 = 2;
        }
        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
        final float phaseY = this.mAnimator.getPhaseY();
        this.mRenderPaint.setStyle(Paint$Style.STROKE);
        Canvas mBitmapCanvas;
        if (set.isDashedLineEnabled()) {
            mBitmapCanvas = this.mBitmapCanvas;
        }
        else {
            mBitmapCanvas = canvas;
        }
        this.mXBounds.set(this.mChart, set);
        if (set.isDrawFilledEnabled() && entryCount > 0) {
            this.drawLinearFill(canvas, set, transformer, this.mXBounds);
        }
        if (set.getColors().size() > 1) {
            final int length = this.mLineBuffer.length;
            final int n = b2 * 2;
            if (length <= n) {
                this.mLineBuffer = new float[b2 * 4];
            }
            for (int i = this.mXBounds.min; i <= this.mXBounds.range + this.mXBounds.min; ++i) {
                final Entry entryForIndex = set.getEntryForIndex(i);
                if (entryForIndex != null) {
                    this.mLineBuffer[0] = entryForIndex.getX();
                    this.mLineBuffer[1] = entryForIndex.getY() * phaseY;
                    if (i < this.mXBounds.max) {
                        final Entry entryForIndex2 = set.getEntryForIndex(i + 1);
                        if (entryForIndex2 == null) {
                            break;
                        }
                        if (b) {
                            this.mLineBuffer[2] = entryForIndex2.getX();
                            final float[] mLineBuffer = this.mLineBuffer;
                            final float n2 = mLineBuffer[1];
                            mLineBuffer[3] = n2;
                            mLineBuffer[4] = mLineBuffer[2];
                            mLineBuffer[5] = n2;
                            mLineBuffer[6] = entryForIndex2.getX();
                            this.mLineBuffer[7] = entryForIndex2.getY() * phaseY;
                        }
                        else {
                            this.mLineBuffer[2] = entryForIndex2.getX();
                            this.mLineBuffer[3] = entryForIndex2.getY() * phaseY;
                        }
                    }
                    else {
                        final float[] mLineBuffer2 = this.mLineBuffer;
                        mLineBuffer2[2] = mLineBuffer2[0];
                        mLineBuffer2[3] = mLineBuffer2[1];
                    }
                    transformer.pointValuesToPixel(this.mLineBuffer);
                    if (!this.mViewPortHandler.isInBoundsRight(this.mLineBuffer[0])) {
                        break;
                    }
                    if (this.mViewPortHandler.isInBoundsLeft(this.mLineBuffer[2])) {
                        if (this.mViewPortHandler.isInBoundsTop(this.mLineBuffer[1]) || this.mViewPortHandler.isInBoundsBottom(this.mLineBuffer[3])) {
                            this.mRenderPaint.setColor(set.getColor(i));
                            mBitmapCanvas.drawLines(this.mLineBuffer, 0, n, this.mRenderPaint);
                        }
                    }
                }
            }
        }
        else {
            final int length2 = this.mLineBuffer.length;
            final int n3 = entryCount * b2;
            if (length2 < Math.max(n3, b2) * 2) {
                this.mLineBuffer = new float[Math.max(n3, b2) * 4];
            }
            if (set.getEntryForIndex(this.mXBounds.min) != null) {
                int j = this.mXBounds.min;
                int n4 = 0;
                while (j <= this.mXBounds.range + this.mXBounds.min) {
                    int n5;
                    if (j == 0) {
                        n5 = 0;
                    }
                    else {
                        n5 = j - 1;
                    }
                    final Entry entryForIndex3 = set.getEntryForIndex(n5);
                    final Entry entryForIndex4 = set.getEntryForIndex(j);
                    int n6 = n4;
                    if (entryForIndex3 != null) {
                        if (entryForIndex4 == null) {
                            n6 = n4;
                        }
                        else {
                            final float[] mLineBuffer3 = this.mLineBuffer;
                            final int n7 = n4 + 1;
                            mLineBuffer3[n4] = entryForIndex3.getX();
                            final float[] mLineBuffer4 = this.mLineBuffer;
                            final int n8 = n7 + 1;
                            mLineBuffer4[n7] = entryForIndex3.getY() * phaseY;
                            int n9 = n8;
                            if (b) {
                                final float[] mLineBuffer5 = this.mLineBuffer;
                                final int n10 = n8 + 1;
                                mLineBuffer5[n8] = entryForIndex4.getX();
                                final float[] mLineBuffer6 = this.mLineBuffer;
                                final int n11 = n10 + 1;
                                mLineBuffer6[n10] = entryForIndex3.getY() * phaseY;
                                final float[] mLineBuffer7 = this.mLineBuffer;
                                final int n12 = n11 + 1;
                                mLineBuffer7[n11] = entryForIndex4.getX();
                                final float[] mLineBuffer8 = this.mLineBuffer;
                                n9 = n12 + 1;
                                mLineBuffer8[n12] = entryForIndex3.getY() * phaseY;
                            }
                            final float[] mLineBuffer9 = this.mLineBuffer;
                            n6 = n9 + 1;
                            mLineBuffer9[n9] = entryForIndex4.getX();
                            this.mLineBuffer[n6] = entryForIndex4.getY() * phaseY;
                            ++n6;
                        }
                    }
                    ++j;
                    n4 = n6;
                }
                if (n4 > 0) {
                    transformer.pointValuesToPixel(this.mLineBuffer);
                    final int max = Math.max((this.mXBounds.range + 1) * b2, b2);
                    this.mRenderPaint.setColor(set.getColor());
                    mBitmapCanvas.drawLines(this.mLineBuffer, 0, max * 2, this.mRenderPaint);
                }
            }
        }
        this.mRenderPaint.setPathEffect((PathEffect)null);
    }
    
    protected void drawLinearFill(final Canvas canvas, final ILineDataSet set, final Transformer transformer, final XBounds xBounds) {
        final Path mGenerateFilledPathBuffer = this.mGenerateFilledPathBuffer;
        final int min = xBounds.min;
        final int n = xBounds.range + xBounds.min;
        int n2 = 0;
        int i;
        int n3;
        do {
            i = n2 * 128 + min;
            if ((n3 = i + 128) > n) {
                n3 = n;
            }
            if (i <= n3) {
                this.generateFilledPath(set, i, n3, mGenerateFilledPathBuffer);
                transformer.pathValueToPixel(mGenerateFilledPathBuffer);
                final Drawable fillDrawable = set.getFillDrawable();
                if (fillDrawable != null) {
                    this.drawFilledPath(canvas, mGenerateFilledPathBuffer, fillDrawable);
                }
                else {
                    this.drawFilledPath(canvas, mGenerateFilledPathBuffer, set.getFillColor(), set.getFillAlpha());
                }
            }
            ++n2;
        } while (i <= n3);
    }
    
    @Override
    public void drawValue(final Canvas canvas, final String s, final float n, final float n2, final int color) {
        this.mValuePaint.setColor(color);
        canvas.drawText(s, n, n2, this.mValuePaint);
    }
    
    @Override
    public void drawValues(final Canvas canvas) {
        if (this.isDrawingValuesAllowed(this.mChart)) {
            final List<ILineDataSet> dataSets = this.mChart.getLineData().getDataSets();
            for (int i = 0; i < dataSets.size(); ++i) {
                final ILineDataSet set = dataSets.get(i);
                if (this.shouldDrawValues(set)) {
                    if (set.getEntryCount() >= 1) {
                        this.applyValueTextStyle(set);
                        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
                        int n = (int)(set.getCircleRadius() * 1.75f);
                        if (!set.isDrawCirclesEnabled()) {
                            n /= 2;
                        }
                        final int n2 = n;
                        this.mXBounds.set(this.mChart, set);
                        final float[] generateTransformedValuesLine = transformer.generateTransformedValuesLine(set, this.mAnimator.getPhaseX(), this.mAnimator.getPhaseY(), this.mXBounds.min, this.mXBounds.max);
                        final ValueFormatter valueFormatter = set.getValueFormatter();
                        final MPPointF instance = MPPointF.getInstance(set.getIconsOffset());
                        instance.x = Utils.convertDpToPixel(instance.x);
                        instance.y = Utils.convertDpToPixel(instance.y);
                        for (int j = 0; j < generateTransformedValuesLine.length; j += 2) {
                            final float n3 = generateTransformedValuesLine[j];
                            final float n4 = generateTransformedValuesLine[j + 1];
                            if (!this.mViewPortHandler.isInBoundsRight(n3)) {
                                break;
                            }
                            if (this.mViewPortHandler.isInBoundsLeft(n3)) {
                                if (this.mViewPortHandler.isInBoundsY(n4)) {
                                    final int n5 = j / 2;
                                    final Entry entryForIndex = set.getEntryForIndex(this.mXBounds.min + n5);
                                    if (set.isDrawValuesEnabled()) {
                                        this.drawValue(canvas, valueFormatter.getPointLabel(entryForIndex), n3, n4 - n2, set.getValueTextColor(n5));
                                    }
                                    if (entryForIndex.getIcon() != null && set.isDrawIconsEnabled()) {
                                        final Drawable icon = entryForIndex.getIcon();
                                        Utils.drawImage(canvas, icon, (int)(n3 + instance.x), (int)(n4 + instance.y), icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
                                    }
                                }
                            }
                        }
                        MPPointF.recycleInstance(instance);
                    }
                }
            }
        }
    }
    
    public Bitmap$Config getBitmapConfig() {
        return this.mBitmapConfig;
    }
    
    @Override
    public void initBuffers() {
    }
    
    public void releaseBitmap() {
        final Canvas mBitmapCanvas = this.mBitmapCanvas;
        if (mBitmapCanvas != null) {
            mBitmapCanvas.setBitmap((Bitmap)null);
            this.mBitmapCanvas = null;
        }
        final WeakReference<Bitmap> mDrawBitmap = this.mDrawBitmap;
        if (mDrawBitmap != null) {
            final Bitmap bitmap = mDrawBitmap.get();
            if (bitmap != null) {
                bitmap.recycle();
            }
            this.mDrawBitmap.clear();
            this.mDrawBitmap = null;
        }
    }
    
    public void setBitmapConfig(final Bitmap$Config mBitmapConfig) {
        this.mBitmapConfig = mBitmapConfig;
        this.releaseBitmap();
    }
    
    private class DataSetImageCache
    {
        private Bitmap[] circleBitmaps;
        private Path mCirclePathBuffer;
        final LineChartRenderer this$0;
        
        private DataSetImageCache(final LineChartRenderer this$0) {
            this.this$0 = this$0;
            this.mCirclePathBuffer = new Path();
        }
        
        protected void fill(final ILineDataSet set, final boolean b, final boolean b2) {
            final int circleColorCount = set.getCircleColorCount();
            final float circleRadius = set.getCircleRadius();
            final float circleHoleRadius = set.getCircleHoleRadius();
            for (int i = 0; i < circleColorCount; ++i) {
                final Bitmap$Config argb_4444 = Bitmap$Config.ARGB_4444;
                final int n = (int)(circleRadius * 2.1);
                final Bitmap bitmap = Bitmap.createBitmap(n, n, argb_4444);
                final Canvas canvas = new Canvas(bitmap);
                this.circleBitmaps[i] = bitmap;
                this.this$0.mRenderPaint.setColor(set.getCircleColor(i));
                if (b2) {
                    this.mCirclePathBuffer.reset();
                    this.mCirclePathBuffer.addCircle(circleRadius, circleRadius, circleRadius, Path$Direction.CW);
                    this.mCirclePathBuffer.addCircle(circleRadius, circleRadius, circleHoleRadius, Path$Direction.CCW);
                    canvas.drawPath(this.mCirclePathBuffer, this.this$0.mRenderPaint);
                }
                else {
                    canvas.drawCircle(circleRadius, circleRadius, circleRadius, this.this$0.mRenderPaint);
                    if (b) {
                        canvas.drawCircle(circleRadius, circleRadius, circleHoleRadius, this.this$0.mCirclePaintInner);
                    }
                }
            }
        }
        
        protected Bitmap getBitmap(final int n) {
            final Bitmap[] circleBitmaps = this.circleBitmaps;
            return circleBitmaps[n % circleBitmaps.length];
        }
        
        protected boolean init(final ILineDataSet set) {
            final int circleColorCount = set.getCircleColorCount();
            final Bitmap[] circleBitmaps = this.circleBitmaps;
            boolean b = true;
            if (circleBitmaps == null) {
                this.circleBitmaps = new Bitmap[circleColorCount];
            }
            else if (circleBitmaps.length != circleColorCount) {
                this.circleBitmaps = new Bitmap[circleColorCount];
            }
            else {
                b = false;
            }
            return b;
        }
    }
}

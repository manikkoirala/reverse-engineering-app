// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.renderer;

import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.formatter.ValueFormatter;
import java.util.List;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.dataprovider.ChartInterface;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.interfaces.datasets.ILineScatterCandleRadarDataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import android.graphics.Paint;
import com.github.mikephil.charting.utils.Transformer;
import android.graphics.Paint$Style;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;
import com.github.mikephil.charting.interfaces.dataprovider.BarLineScatterCandleBubbleDataProvider;
import java.util.Iterator;
import com.github.mikephil.charting.interfaces.datasets.ICandleDataSet;
import android.graphics.Canvas;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.interfaces.dataprovider.CandleDataProvider;

public class CandleStickChartRenderer extends LineScatterCandleRadarRenderer
{
    private float[] mBodyBuffers;
    protected CandleDataProvider mChart;
    private float[] mCloseBuffers;
    private float[] mOpenBuffers;
    private float[] mRangeBuffers;
    private float[] mShadowBuffers;
    
    public CandleStickChartRenderer(final CandleDataProvider mChart, final ChartAnimator chartAnimator, final ViewPortHandler viewPortHandler) {
        super(chartAnimator, viewPortHandler);
        this.mShadowBuffers = new float[8];
        this.mBodyBuffers = new float[4];
        this.mRangeBuffers = new float[4];
        this.mOpenBuffers = new float[4];
        this.mCloseBuffers = new float[4];
        this.mChart = mChart;
    }
    
    @Override
    public void drawData(final Canvas canvas) {
        for (final ICandleDataSet set : this.mChart.getCandleData().getDataSets()) {
            if (set.isVisible()) {
                this.drawDataSet(canvas, set);
            }
        }
    }
    
    protected void drawDataSet(final Canvas canvas, final ICandleDataSet set) {
        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
        final float phaseY = this.mAnimator.getPhaseY();
        final float barSpace = set.getBarSpace();
        final boolean showCandleBar = set.getShowCandleBar();
        this.mXBounds.set(this.mChart, set);
        this.mRenderPaint.setStrokeWidth(set.getShadowWidth());
        for (int i = this.mXBounds.min; i <= this.mXBounds.range + this.mXBounds.min; ++i) {
            final CandleEntry candleEntry = set.getEntryForIndex(i);
            if (candleEntry != null) {
                final float x = candleEntry.getX();
                final float open = candleEntry.getOpen();
                final float close = candleEntry.getClose();
                final float high = candleEntry.getHigh();
                final float low = candleEntry.getLow();
                if (showCandleBar) {
                    final float[] mShadowBuffers = this.mShadowBuffers;
                    mShadowBuffers[2] = (mShadowBuffers[0] = x);
                    mShadowBuffers[6] = (mShadowBuffers[4] = x);
                    final float n = fcmpl(open, close);
                    if (n > 0) {
                        mShadowBuffers[1] = high * phaseY;
                        mShadowBuffers[3] = open * phaseY;
                        mShadowBuffers[5] = low * phaseY;
                        mShadowBuffers[7] = close * phaseY;
                    }
                    else if (open < close) {
                        mShadowBuffers[1] = high * phaseY;
                        mShadowBuffers[3] = close * phaseY;
                        mShadowBuffers[5] = low * phaseY;
                        mShadowBuffers[7] = open * phaseY;
                    }
                    else {
                        mShadowBuffers[1] = high * phaseY;
                        final float n2 = open * phaseY;
                        mShadowBuffers[3] = n2;
                        mShadowBuffers[5] = low * phaseY;
                        mShadowBuffers[7] = n2;
                    }
                    transformer.pointValuesToPixel(mShadowBuffers);
                    if (set.getShadowColorSameAsCandle()) {
                        if (n > 0) {
                            final Paint mRenderPaint = this.mRenderPaint;
                            int color;
                            if (set.getDecreasingColor() == 1122867) {
                                color = set.getColor(i);
                            }
                            else {
                                color = set.getDecreasingColor();
                            }
                            mRenderPaint.setColor(color);
                        }
                        else if (open < close) {
                            final Paint mRenderPaint2 = this.mRenderPaint;
                            int color2;
                            if (set.getIncreasingColor() == 1122867) {
                                color2 = set.getColor(i);
                            }
                            else {
                                color2 = set.getIncreasingColor();
                            }
                            mRenderPaint2.setColor(color2);
                        }
                        else {
                            final Paint mRenderPaint3 = this.mRenderPaint;
                            int color3;
                            if (set.getNeutralColor() == 1122867) {
                                color3 = set.getColor(i);
                            }
                            else {
                                color3 = set.getNeutralColor();
                            }
                            mRenderPaint3.setColor(color3);
                        }
                    }
                    else {
                        final Paint mRenderPaint4 = this.mRenderPaint;
                        int color4;
                        if (set.getShadowColor() == 1122867) {
                            color4 = set.getColor(i);
                        }
                        else {
                            color4 = set.getShadowColor();
                        }
                        mRenderPaint4.setColor(color4);
                    }
                    this.mRenderPaint.setStyle(Paint$Style.STROKE);
                    canvas.drawLines(this.mShadowBuffers, this.mRenderPaint);
                    final float[] mBodyBuffers = this.mBodyBuffers;
                    mBodyBuffers[0] = x - 0.5f + barSpace;
                    mBodyBuffers[1] = close * phaseY;
                    mBodyBuffers[2] = x + 0.5f - barSpace;
                    mBodyBuffers[3] = open * phaseY;
                    transformer.pointValuesToPixel(mBodyBuffers);
                    if (n > 0) {
                        if (set.getDecreasingColor() == 1122867) {
                            this.mRenderPaint.setColor(set.getColor(i));
                        }
                        else {
                            this.mRenderPaint.setColor(set.getDecreasingColor());
                        }
                        this.mRenderPaint.setStyle(set.getDecreasingPaintStyle());
                        final float[] mBodyBuffers2 = this.mBodyBuffers;
                        canvas.drawRect(mBodyBuffers2[0], mBodyBuffers2[3], mBodyBuffers2[2], mBodyBuffers2[1], this.mRenderPaint);
                    }
                    else if (open < close) {
                        if (set.getIncreasingColor() == 1122867) {
                            this.mRenderPaint.setColor(set.getColor(i));
                        }
                        else {
                            this.mRenderPaint.setColor(set.getIncreasingColor());
                        }
                        this.mRenderPaint.setStyle(set.getIncreasingPaintStyle());
                        final float[] mBodyBuffers3 = this.mBodyBuffers;
                        canvas.drawRect(mBodyBuffers3[0], mBodyBuffers3[1], mBodyBuffers3[2], mBodyBuffers3[3], this.mRenderPaint);
                    }
                    else {
                        if (set.getNeutralColor() == 1122867) {
                            this.mRenderPaint.setColor(set.getColor(i));
                        }
                        else {
                            this.mRenderPaint.setColor(set.getNeutralColor());
                        }
                        final float[] mBodyBuffers4 = this.mBodyBuffers;
                        canvas.drawLine(mBodyBuffers4[0], mBodyBuffers4[1], mBodyBuffers4[2], mBodyBuffers4[3], this.mRenderPaint);
                    }
                }
                else {
                    final float[] mRangeBuffers = this.mRangeBuffers;
                    mRangeBuffers[0] = x;
                    mRangeBuffers[1] = high * phaseY;
                    mRangeBuffers[2] = x;
                    mRangeBuffers[3] = low * phaseY;
                    final float[] mOpenBuffers = this.mOpenBuffers;
                    mOpenBuffers[0] = x - 0.5f + barSpace;
                    final float n3 = open * phaseY;
                    mOpenBuffers[1] = n3;
                    mOpenBuffers[2] = x;
                    mOpenBuffers[3] = n3;
                    final float[] mCloseBuffers = this.mCloseBuffers;
                    mCloseBuffers[0] = 0.5f + x - barSpace;
                    final float n4 = close * phaseY;
                    mCloseBuffers[1] = n4;
                    mCloseBuffers[2] = x;
                    mCloseBuffers[3] = n4;
                    transformer.pointValuesToPixel(mRangeBuffers);
                    transformer.pointValuesToPixel(this.mOpenBuffers);
                    transformer.pointValuesToPixel(this.mCloseBuffers);
                    int color5;
                    if (open > close) {
                        if (set.getDecreasingColor() == 1122867) {
                            color5 = set.getColor(i);
                        }
                        else {
                            color5 = set.getDecreasingColor();
                        }
                    }
                    else if (open < close) {
                        if (set.getIncreasingColor() == 1122867) {
                            color5 = set.getColor(i);
                        }
                        else {
                            color5 = set.getIncreasingColor();
                        }
                    }
                    else if (set.getNeutralColor() == 1122867) {
                        color5 = set.getColor(i);
                    }
                    else {
                        color5 = set.getNeutralColor();
                    }
                    this.mRenderPaint.setColor(color5);
                    final float[] mRangeBuffers2 = this.mRangeBuffers;
                    canvas.drawLine(mRangeBuffers2[0], mRangeBuffers2[1], mRangeBuffers2[2], mRangeBuffers2[3], this.mRenderPaint);
                    final float[] mOpenBuffers2 = this.mOpenBuffers;
                    canvas.drawLine(mOpenBuffers2[0], mOpenBuffers2[1], mOpenBuffers2[2], mOpenBuffers2[3], this.mRenderPaint);
                    final float[] mCloseBuffers2 = this.mCloseBuffers;
                    canvas.drawLine(mCloseBuffers2[0], mCloseBuffers2[1], mCloseBuffers2[2], mCloseBuffers2[3], this.mRenderPaint);
                }
            }
        }
    }
    
    @Override
    public void drawExtras(final Canvas canvas) {
    }
    
    @Override
    public void drawHighlighted(final Canvas canvas, final Highlight[] array) {
        final CandleData candleData = this.mChart.getCandleData();
        for (final Highlight highlight : array) {
            final ICandleDataSet set = candleData.getDataSetByIndex(highlight.getDataSetIndex());
            if (set != null) {
                if (set.isHighlightEnabled()) {
                    final CandleEntry candleEntry = set.getEntryForXValue(highlight.getX(), highlight.getY());
                    if (this.isInBoundsX(candleEntry, set)) {
                        final MPPointD pixelForValues = this.mChart.getTransformer(set.getAxisDependency()).getPixelForValues(candleEntry.getX(), (candleEntry.getLow() * this.mAnimator.getPhaseY() + candleEntry.getHigh() * this.mAnimator.getPhaseY()) / 2.0f);
                        highlight.setDraw((float)pixelForValues.x, (float)pixelForValues.y);
                        this.drawHighlightLines(canvas, (float)pixelForValues.x, (float)pixelForValues.y, set);
                    }
                }
            }
        }
    }
    
    @Override
    public void drawValue(final Canvas canvas, final String s, final float n, final float n2, final int color) {
        this.mValuePaint.setColor(color);
        canvas.drawText(s, n, n2, this.mValuePaint);
    }
    
    @Override
    public void drawValues(final Canvas canvas) {
        if (this.isDrawingValuesAllowed(this.mChart)) {
            final List<ICandleDataSet> dataSets = this.mChart.getCandleData().getDataSets();
            for (int i = 0; i < dataSets.size(); ++i) {
                final ICandleDataSet set = dataSets.get(i);
                if (this.shouldDrawValues(set)) {
                    if (set.getEntryCount() >= 1) {
                        this.applyValueTextStyle(set);
                        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
                        this.mXBounds.set(this.mChart, set);
                        final float[] generateTransformedValuesCandle = transformer.generateTransformedValuesCandle(set, this.mAnimator.getPhaseX(), this.mAnimator.getPhaseY(), this.mXBounds.min, this.mXBounds.max);
                        final float convertDpToPixel = Utils.convertDpToPixel(5.0f);
                        final ValueFormatter valueFormatter = set.getValueFormatter();
                        final MPPointF instance = MPPointF.getInstance(set.getIconsOffset());
                        instance.x = Utils.convertDpToPixel(instance.x);
                        instance.y = Utils.convertDpToPixel(instance.y);
                        for (int j = 0; j < generateTransformedValuesCandle.length; j += 2) {
                            final float n = generateTransformedValuesCandle[j];
                            final float n2 = generateTransformedValuesCandle[j + 1];
                            if (!this.mViewPortHandler.isInBoundsRight(n)) {
                                break;
                            }
                            if (this.mViewPortHandler.isInBoundsLeft(n)) {
                                if (this.mViewPortHandler.isInBoundsY(n2)) {
                                    final int n3 = j / 2;
                                    final CandleEntry candleEntry = set.getEntryForIndex(this.mXBounds.min + n3);
                                    if (set.isDrawValuesEnabled()) {
                                        this.drawValue(canvas, valueFormatter.getCandleLabel(candleEntry), n, n2 - convertDpToPixel, set.getValueTextColor(n3));
                                    }
                                    if (candleEntry.getIcon() != null && set.isDrawIconsEnabled()) {
                                        final Drawable icon = candleEntry.getIcon();
                                        Utils.drawImage(canvas, icon, (int)(n + instance.x), (int)(n2 + instance.y), icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
                                    }
                                }
                            }
                        }
                        MPPointF.recycleInstance(instance);
                    }
                }
            }
        }
    }
    
    @Override
    public void initBuffers() {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.renderer;

import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.formatter.ValueFormatter;
import java.util.List;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.dataprovider.ChartInterface;
import com.github.mikephil.charting.highlight.Range;
import com.github.mikephil.charting.interfaces.datasets.IBarLineScatterCandleBubbleDataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.Transformer;
import android.graphics.Shader;
import android.graphics.LinearGradient;
import android.graphics.Shader$TileMode;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint$Style;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import android.graphics.RectF;
import com.github.mikephil.charting.buffer.BarBuffer;
import android.graphics.Paint;

public class BarChartRenderer extends BarLineScatterCandleBubbleRenderer
{
    protected Paint mBarBorderPaint;
    protected BarBuffer[] mBarBuffers;
    protected RectF mBarRect;
    private RectF mBarShadowRectBuffer;
    protected BarDataProvider mChart;
    protected Paint mShadowPaint;
    
    public BarChartRenderer(final BarDataProvider mChart, final ChartAnimator chartAnimator, final ViewPortHandler viewPortHandler) {
        super(chartAnimator, viewPortHandler);
        this.mBarRect = new RectF();
        this.mBarShadowRectBuffer = new RectF();
        this.mChart = mChart;
        (this.mHighlightPaint = new Paint(1)).setStyle(Paint$Style.FILL);
        this.mHighlightPaint.setColor(Color.rgb(0, 0, 0));
        this.mHighlightPaint.setAlpha(120);
        (this.mShadowPaint = new Paint(1)).setStyle(Paint$Style.FILL);
        (this.mBarBorderPaint = new Paint(1)).setStyle(Paint$Style.STROKE);
    }
    
    @Override
    public void drawData(final Canvas canvas) {
        final BarData barData = this.mChart.getBarData();
        for (int i = 0; i < barData.getDataSetCount(); ++i) {
            final IBarDataSet set = barData.getDataSetByIndex(i);
            if (set.isVisible()) {
                this.drawDataSet(canvas, set, i);
            }
        }
    }
    
    protected void drawDataSet(final Canvas canvas, final IBarDataSet set, int dataSet) {
        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
        this.mBarBorderPaint.setColor(set.getBarBorderColor());
        this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(set.getBarBorderWidth()));
        final float barBorderWidth = set.getBarBorderWidth();
        final int n = 0;
        final int n2 = 1;
        final boolean b = barBorderWidth > 0.0f;
        final float phaseX = this.mAnimator.getPhaseX();
        final float phaseY = this.mAnimator.getPhaseY();
        if (this.mChart.isDrawBarShadowEnabled()) {
            this.mShadowPaint.setColor(set.getBarShadowColor());
            final float n3 = this.mChart.getBarData().getBarWidth() / 2.0f;
            for (int min = Math.min((int)Math.ceil(set.getEntryCount() * phaseX), set.getEntryCount()), i = 0; i < min; ++i) {
                final float x = set.getEntryForIndex(i).getX();
                this.mBarShadowRectBuffer.left = x - n3;
                this.mBarShadowRectBuffer.right = x + n3;
                transformer.rectValueToPixel(this.mBarShadowRectBuffer);
                if (this.mViewPortHandler.isInBoundsLeft(this.mBarShadowRectBuffer.right)) {
                    if (!this.mViewPortHandler.isInBoundsRight(this.mBarShadowRectBuffer.left)) {
                        break;
                    }
                    this.mBarShadowRectBuffer.top = this.mViewPortHandler.contentTop();
                    this.mBarShadowRectBuffer.bottom = this.mViewPortHandler.contentBottom();
                    canvas.drawRect(this.mBarShadowRectBuffer, this.mShadowPaint);
                }
            }
        }
        final BarBuffer barBuffer = this.mBarBuffers[dataSet];
        barBuffer.setPhases(phaseX, phaseY);
        barBuffer.setDataSet(dataSet);
        barBuffer.setInverted(this.mChart.isInverted(set.getAxisDependency()));
        barBuffer.setBarWidth(this.mChart.getBarData().getBarWidth());
        barBuffer.feed(set);
        transformer.pointValuesToPixel(barBuffer.buffer);
        if (set.getColors().size() == 1) {
            dataSet = n2;
        }
        else {
            dataSet = 0;
        }
        int j = n;
        if (dataSet != 0) {
            this.mRenderPaint.setColor(set.getColor());
            j = n;
        }
        while (j < barBuffer.size()) {
            final ViewPortHandler mViewPortHandler = this.mViewPortHandler;
            final float[] buffer = barBuffer.buffer;
            final int n4 = j + 2;
            if (mViewPortHandler.isInBoundsLeft(buffer[n4])) {
                if (!this.mViewPortHandler.isInBoundsRight(barBuffer.buffer[j])) {
                    break;
                }
                if (dataSet == 0) {
                    this.mRenderPaint.setColor(set.getColor(j / 4));
                }
                if (set.getGradientColor() != null) {
                    final GradientColor gradientColor = set.getGradientColor();
                    this.mRenderPaint.setShader((Shader)new LinearGradient(barBuffer.buffer[j], barBuffer.buffer[j + 3], barBuffer.buffer[j], barBuffer.buffer[j + 1], gradientColor.getStartColor(), gradientColor.getEndColor(), Shader$TileMode.MIRROR));
                }
                if (set.getGradientColors() != null) {
                    final Paint mRenderPaint = this.mRenderPaint;
                    final float n5 = barBuffer.buffer[j];
                    final float n6 = barBuffer.buffer[j + 3];
                    final float n7 = barBuffer.buffer[j];
                    final float n8 = barBuffer.buffer[j + 1];
                    final int n9 = j / 4;
                    mRenderPaint.setShader((Shader)new LinearGradient(n5, n6, n7, n8, set.getGradientColor(n9).getStartColor(), set.getGradientColor(n9).getEndColor(), Shader$TileMode.MIRROR));
                }
                final float n10 = barBuffer.buffer[j];
                final float[] buffer2 = barBuffer.buffer;
                final int n11 = j + 1;
                final float n12 = buffer2[n11];
                final float n13 = barBuffer.buffer[n4];
                final float[] buffer3 = barBuffer.buffer;
                final int n14 = j + 3;
                canvas.drawRect(n10, n12, n13, buffer3[n14], this.mRenderPaint);
                if (b) {
                    canvas.drawRect(barBuffer.buffer[j], barBuffer.buffer[n11], barBuffer.buffer[n4], barBuffer.buffer[n14], this.mBarBorderPaint);
                }
            }
            j += 4;
        }
    }
    
    @Override
    public void drawExtras(final Canvas canvas) {
    }
    
    @Override
    public void drawHighlighted(final Canvas canvas, final Highlight[] array) {
        final BarData barData = this.mChart.getBarData();
        for (final Highlight highlight : array) {
            final IBarDataSet set = barData.getDataSetByIndex(highlight.getDataSetIndex());
            if (set != null) {
                if (set.isHighlightEnabled()) {
                    final BarEntry barEntry = set.getEntryForXValue(highlight.getX(), highlight.getY());
                    if (this.isInBoundsX(barEntry, set)) {
                        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
                        this.mHighlightPaint.setColor(set.getHighLightColor());
                        this.mHighlightPaint.setAlpha(set.getHighLightAlpha());
                        float n;
                        float to;
                        if (highlight.getStackIndex() >= 0 && barEntry.isStacked()) {
                            if (this.mChart.isHighlightFullBarEnabled()) {
                                n = barEntry.getPositiveSum();
                                to = -barEntry.getNegativeSum();
                            }
                            else {
                                final Range range = barEntry.getRanges()[highlight.getStackIndex()];
                                n = range.from;
                                to = range.to;
                            }
                        }
                        else {
                            n = barEntry.getY();
                            to = 0.0f;
                        }
                        this.prepareBarHighlight(barEntry.getX(), n, to, barData.getBarWidth() / 2.0f, transformer);
                        this.setHighlightDrawPos(highlight, this.mBarRect);
                        canvas.drawRect(this.mBarRect, this.mHighlightPaint);
                    }
                }
            }
        }
    }
    
    @Override
    public void drawValue(final Canvas canvas, final String s, final float n, final float n2, final int color) {
        this.mValuePaint.setColor(color);
        canvas.drawText(s, n, n2, this.mValuePaint);
    }
    
    @Override
    public void drawValues(final Canvas canvas) {
        if (this.isDrawingValuesAllowed(this.mChart)) {
            List<IBarDataSet> dataSets = this.mChart.getBarData().getDataSets();
            float convertDpToPixel = Utils.convertDpToPixel(4.5f);
            int drawValueAboveBarEnabled = this.mChart.isDrawValueAboveBarEnabled() ? 1 : 0;
            float n;
            List<IBarDataSet> list;
            for (int i = 0; i < this.mChart.getBarData().getDataSetCount(); ++i, dataSets = list, convertDpToPixel = n) {
                final IBarDataSet set = dataSets.get(i);
                if (!this.shouldDrawValues(set)) {
                    n = convertDpToPixel;
                    list = dataSets;
                }
                else {
                    this.applyValueTextStyle(set);
                    final boolean inverted = this.mChart.isInverted(set.getAxisDependency());
                    final float n2 = (float)Utils.calcTextHeight(this.mValuePaint, "8");
                    float n3;
                    if (drawValueAboveBarEnabled != 0) {
                        n3 = -convertDpToPixel;
                    }
                    else {
                        n3 = n2 + convertDpToPixel;
                    }
                    float n4;
                    if (drawValueAboveBarEnabled != 0) {
                        n4 = n2 + convertDpToPixel;
                    }
                    else {
                        n4 = -convertDpToPixel;
                    }
                    float n5 = n3;
                    float n6 = n4;
                    if (inverted) {
                        n5 = -n3 - n2;
                        n6 = -n4 - n2;
                    }
                    final BarBuffer barBuffer = this.mBarBuffers[i];
                    final float phaseY = this.mAnimator.getPhaseY();
                    final ValueFormatter valueFormatter = set.getValueFormatter();
                    final MPPointF instance = MPPointF.getInstance(set.getIconsOffset());
                    instance.x = Utils.convertDpToPixel(instance.x);
                    instance.y = Utils.convertDpToPixel(instance.y);
                    MPPointF mpPointF;
                    int n13;
                    if (!set.isStacked()) {
                        int n7 = 0;
                        final ValueFormatter valueFormatter2 = valueFormatter;
                        while (n7 < barBuffer.buffer.length * this.mAnimator.getPhaseX()) {
                            final float n8 = (barBuffer.buffer[n7] + barBuffer.buffer[n7 + 2]) / 2.0f;
                            if (!this.mViewPortHandler.isInBoundsRight(n8)) {
                                break;
                            }
                            final ViewPortHandler mViewPortHandler = this.mViewPortHandler;
                            final float[] buffer = barBuffer.buffer;
                            final int n9 = n7 + 1;
                            if (mViewPortHandler.isInBoundsY(buffer[n9])) {
                                if (this.mViewPortHandler.isInBoundsLeft(n8)) {
                                    final int n10 = n7 / 4;
                                    final BarEntry barEntry = set.getEntryForIndex(n10);
                                    final float y = barEntry.getY();
                                    if (set.isDrawValuesEnabled()) {
                                        final String barLabel = valueFormatter2.getBarLabel(barEntry);
                                        final float[] buffer2 = barBuffer.buffer;
                                        float n11;
                                        if (y >= 0.0f) {
                                            n11 = buffer2[n9] + n5;
                                        }
                                        else {
                                            n11 = buffer2[n7 + 3] + n6;
                                        }
                                        this.drawValue(canvas, barLabel, n8, n11, set.getValueTextColor(n10));
                                    }
                                    if (barEntry.getIcon() != null && set.isDrawIconsEnabled()) {
                                        final Drawable icon = barEntry.getIcon();
                                        float n12;
                                        if (y >= 0.0f) {
                                            n12 = barBuffer.buffer[n9] + n5;
                                        }
                                        else {
                                            n12 = barBuffer.buffer[n7 + 3] + n6;
                                        }
                                        Utils.drawImage(canvas, icon, (int)(n8 + instance.x), (int)(n12 + instance.y), icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
                                    }
                                }
                            }
                            n7 += 4;
                        }
                        mpPointF = instance;
                        n13 = drawValueAboveBarEnabled;
                        list = dataSets;
                    }
                    else {
                        final List<IBarDataSet> list2 = dataSets;
                        final MPPointF mpPointF2 = instance;
                        final Transformer transformer = this.mChart.getTransformer(set.getAxisDependency());
                        int n14 = 0;
                        int n15 = 0;
                        final float n16 = convertDpToPixel;
                        while (true) {
                            mpPointF = mpPointF2;
                            convertDpToPixel = n16;
                            n13 = drawValueAboveBarEnabled;
                            list = list2;
                            if (n14 >= set.getEntryCount() * this.mAnimator.getPhaseX()) {
                                break;
                            }
                            final BarEntry barEntry2 = set.getEntryForIndex(n14);
                            final float[] yVals = barEntry2.getYVals();
                            final float n17 = (barBuffer.buffer[n15] + barBuffer.buffer[n15 + 2]) / 2.0f;
                            final int valueTextColor = set.getValueTextColor(n14);
                            if (yVals == null) {
                                if (!this.mViewPortHandler.isInBoundsRight(n17)) {
                                    mpPointF = mpPointF2;
                                    convertDpToPixel = n16;
                                    n13 = drawValueAboveBarEnabled;
                                    list = list2;
                                    break;
                                }
                                final ViewPortHandler mViewPortHandler2 = this.mViewPortHandler;
                                final float[] buffer3 = barBuffer.buffer;
                                final int n18 = n15 + 1;
                                if (!mViewPortHandler2.isInBoundsY(buffer3[n18]) || !this.mViewPortHandler.isInBoundsLeft(n17)) {
                                    continue;
                                }
                                if (set.isDrawValuesEnabled()) {
                                    final String barLabel2 = valueFormatter.getBarLabel(barEntry2);
                                    final float n19 = barBuffer.buffer[n18];
                                    float n20;
                                    if (barEntry2.getY() >= 0.0f) {
                                        n20 = n5;
                                    }
                                    else {
                                        n20 = n6;
                                    }
                                    this.drawValue(canvas, barLabel2, n17, n19 + n20, valueTextColor);
                                }
                                if (barEntry2.getIcon() != null && set.isDrawIconsEnabled()) {
                                    final Drawable icon2 = barEntry2.getIcon();
                                    final float n21 = barBuffer.buffer[n18];
                                    float n22;
                                    if (barEntry2.getY() >= 0.0f) {
                                        n22 = n5;
                                    }
                                    else {
                                        n22 = n6;
                                    }
                                    Utils.drawImage(canvas, icon2, (int)(mpPointF2.x + n17), (int)(n21 + n22 + mpPointF2.y), icon2.getIntrinsicWidth(), icon2.getIntrinsicHeight());
                                }
                            }
                            else {
                                final int n23 = yVals.length * 2;
                                final float[] array = new float[n23];
                                float n24 = -barEntry2.getNegativeSum();
                                int j = 0;
                                int n25 = 0;
                                float n26 = 0.0f;
                                while (j < n23) {
                                    float n27 = yVals[n25];
                                    final float n28 = fcmpl(n27, 0.0f);
                                    if (n28 != 0 || (n26 != 0.0f && n24 != 0.0f)) {
                                        if (n28 >= 0) {
                                            n26 = (n27 += n26);
                                        }
                                        else {
                                            final float n29 = n24 - n27;
                                            n27 = n24;
                                            n24 = n29;
                                        }
                                    }
                                    array[j + 1] = n27 * phaseY;
                                    j += 2;
                                    ++n25;
                                }
                                transformer.pointValuesToPixel(array);
                                for (int k = 0; k < n23; k += 2) {
                                    final float n30 = yVals[k / 2];
                                    final boolean b = (n30 == 0.0f && n24 == 0.0f && n26 > 0.0f) || n30 < 0.0f;
                                    final float n31 = array[k + 1];
                                    float n32;
                                    if (b) {
                                        n32 = n6;
                                    }
                                    else {
                                        n32 = n5;
                                    }
                                    final float n33 = n31 + n32;
                                    if (!this.mViewPortHandler.isInBoundsRight(n17)) {
                                        break;
                                    }
                                    if (this.mViewPortHandler.isInBoundsY(n33)) {
                                        if (this.mViewPortHandler.isInBoundsLeft(n17)) {
                                            if (set.isDrawValuesEnabled()) {
                                                this.drawValue(canvas, valueFormatter.getBarStackedLabel(n30, barEntry2), n17, n33, valueTextColor);
                                            }
                                            if (barEntry2.getIcon() != null && set.isDrawIconsEnabled()) {
                                                final Drawable icon3 = barEntry2.getIcon();
                                                Utils.drawImage(canvas, icon3, (int)(n17 + mpPointF2.x), (int)(n33 + mpPointF2.y), icon3.getIntrinsicWidth(), icon3.getIntrinsicHeight());
                                            }
                                        }
                                    }
                                }
                            }
                            if (yVals == null) {
                                n15 += 4;
                            }
                            else {
                                n15 += yVals.length * 4;
                            }
                            ++n14;
                        }
                    }
                    n = convertDpToPixel;
                    drawValueAboveBarEnabled = n13;
                    MPPointF.recycleInstance(mpPointF);
                }
            }
        }
    }
    
    @Override
    public void initBuffers() {
        final BarData barData = this.mChart.getBarData();
        this.mBarBuffers = new BarBuffer[barData.getDataSetCount()];
        for (int i = 0; i < this.mBarBuffers.length; ++i) {
            final IBarDataSet set = barData.getDataSetByIndex(i);
            final BarBuffer[] mBarBuffers = this.mBarBuffers;
            final int entryCount = set.getEntryCount();
            int stackSize;
            if (set.isStacked()) {
                stackSize = set.getStackSize();
            }
            else {
                stackSize = 1;
            }
            mBarBuffers[i] = new BarBuffer(entryCount * 4 * stackSize, barData.getDataSetCount(), set.isStacked());
        }
    }
    
    protected void prepareBarHighlight(final float n, final float n2, final float n3, final float n4, final Transformer transformer) {
        this.mBarRect.set(n - n4, n2, n + n4, n3);
        transformer.rectToPixelPhase(this.mBarRect, this.mAnimator.getPhaseY());
    }
    
    protected void setHighlightDrawPos(final Highlight highlight, final RectF rectF) {
        highlight.setDraw(rectF.centerX(), rectF.top);
    }
}

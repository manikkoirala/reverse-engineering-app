// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.components;

import com.github.mikephil.charting.utils.Utils;
import android.graphics.Paint;

public class YAxis extends AxisBase
{
    private AxisDependency mAxisDependency;
    private boolean mDrawBottomYLabelEntry;
    private boolean mDrawTopYLabelEntry;
    protected boolean mDrawZeroLine;
    protected boolean mInverted;
    protected float mMaxWidth;
    protected float mMinWidth;
    private YAxisLabelPosition mPosition;
    protected float mSpacePercentBottom;
    protected float mSpacePercentTop;
    private boolean mUseAutoScaleRestrictionMax;
    private boolean mUseAutoScaleRestrictionMin;
    protected int mZeroLineColor;
    protected float mZeroLineWidth;
    
    public YAxis() {
        this.mDrawBottomYLabelEntry = true;
        this.mDrawTopYLabelEntry = true;
        this.mInverted = false;
        this.mDrawZeroLine = false;
        this.mUseAutoScaleRestrictionMin = false;
        this.mUseAutoScaleRestrictionMax = false;
        this.mZeroLineColor = -7829368;
        this.mZeroLineWidth = 1.0f;
        this.mSpacePercentTop = 10.0f;
        this.mSpacePercentBottom = 10.0f;
        this.mPosition = YAxisLabelPosition.OUTSIDE_CHART;
        this.mMinWidth = 0.0f;
        this.mMaxWidth = Float.POSITIVE_INFINITY;
        this.mAxisDependency = AxisDependency.LEFT;
        this.mYOffset = 0.0f;
    }
    
    public YAxis(final AxisDependency mAxisDependency) {
        this.mDrawBottomYLabelEntry = true;
        this.mDrawTopYLabelEntry = true;
        this.mInverted = false;
        this.mDrawZeroLine = false;
        this.mUseAutoScaleRestrictionMin = false;
        this.mUseAutoScaleRestrictionMax = false;
        this.mZeroLineColor = -7829368;
        this.mZeroLineWidth = 1.0f;
        this.mSpacePercentTop = 10.0f;
        this.mSpacePercentBottom = 10.0f;
        this.mPosition = YAxisLabelPosition.OUTSIDE_CHART;
        this.mMinWidth = 0.0f;
        this.mMaxWidth = Float.POSITIVE_INFINITY;
        this.mAxisDependency = mAxisDependency;
        this.mYOffset = 0.0f;
    }
    
    @Override
    public void calculate(float n, float abs) {
        float n2 = n;
        float n3 = abs;
        if (Math.abs(abs - n) == 0.0f) {
            n3 = abs + 1.0f;
            n2 = n - 1.0f;
        }
        abs = Math.abs(n3 - n2);
        if (this.mCustomAxisMin) {
            n = this.mAxisMinimum;
        }
        else {
            n = n2 - abs / 100.0f * this.getSpaceBottom();
        }
        this.mAxisMinimum = n;
        if (this.mCustomAxisMax) {
            n = this.mAxisMaximum;
        }
        else {
            n = n3 + abs / 100.0f * this.getSpaceTop();
        }
        this.mAxisMaximum = n;
        this.mAxisRange = Math.abs(this.mAxisMinimum - this.mAxisMaximum);
    }
    
    public AxisDependency getAxisDependency() {
        return this.mAxisDependency;
    }
    
    public YAxisLabelPosition getLabelPosition() {
        return this.mPosition;
    }
    
    public float getMaxWidth() {
        return this.mMaxWidth;
    }
    
    public float getMinWidth() {
        return this.mMinWidth;
    }
    
    public float getRequiredHeightSpace(final Paint paint) {
        paint.setTextSize(this.mTextSize);
        return Utils.calcTextHeight(paint, this.getLongestLabel()) + this.getYOffset() * 2.0f;
    }
    
    public float getRequiredWidthSpace(final Paint paint) {
        paint.setTextSize(this.mTextSize);
        final float a = Utils.calcTextWidth(paint, this.getLongestLabel()) + this.getXOffset() * 2.0f;
        final float minWidth = this.getMinWidth();
        final float maxWidth = this.getMaxWidth();
        float convertDpToPixel = minWidth;
        if (minWidth > 0.0f) {
            convertDpToPixel = Utils.convertDpToPixel(minWidth);
        }
        float convertDpToPixel2 = maxWidth;
        if (maxWidth > 0.0f) {
            convertDpToPixel2 = maxWidth;
            if (maxWidth != Float.POSITIVE_INFINITY) {
                convertDpToPixel2 = Utils.convertDpToPixel(maxWidth);
            }
        }
        if (convertDpToPixel2 <= 0.0) {
            convertDpToPixel2 = a;
        }
        return Math.max(convertDpToPixel, Math.min(a, convertDpToPixel2));
    }
    
    public float getSpaceBottom() {
        return this.mSpacePercentBottom;
    }
    
    public float getSpaceTop() {
        return this.mSpacePercentTop;
    }
    
    public int getZeroLineColor() {
        return this.mZeroLineColor;
    }
    
    public float getZeroLineWidth() {
        return this.mZeroLineWidth;
    }
    
    public boolean isDrawBottomYLabelEntryEnabled() {
        return this.mDrawBottomYLabelEntry;
    }
    
    public boolean isDrawTopYLabelEntryEnabled() {
        return this.mDrawTopYLabelEntry;
    }
    
    public boolean isDrawZeroLineEnabled() {
        return this.mDrawZeroLine;
    }
    
    public boolean isInverted() {
        return this.mInverted;
    }
    
    @Deprecated
    public boolean isUseAutoScaleMaxRestriction() {
        return this.mUseAutoScaleRestrictionMax;
    }
    
    @Deprecated
    public boolean isUseAutoScaleMinRestriction() {
        return this.mUseAutoScaleRestrictionMin;
    }
    
    public boolean needsOffset() {
        return this.isEnabled() && this.isDrawLabelsEnabled() && this.getLabelPosition() == YAxisLabelPosition.OUTSIDE_CHART;
    }
    
    public void setDrawTopYLabelEntry(final boolean mDrawTopYLabelEntry) {
        this.mDrawTopYLabelEntry = mDrawTopYLabelEntry;
    }
    
    public void setDrawZeroLine(final boolean mDrawZeroLine) {
        this.mDrawZeroLine = mDrawZeroLine;
    }
    
    public void setInverted(final boolean mInverted) {
        this.mInverted = mInverted;
    }
    
    public void setMaxWidth(final float mMaxWidth) {
        this.mMaxWidth = mMaxWidth;
    }
    
    public void setMinWidth(final float mMinWidth) {
        this.mMinWidth = mMinWidth;
    }
    
    public void setPosition(final YAxisLabelPosition mPosition) {
        this.mPosition = mPosition;
    }
    
    public void setSpaceBottom(final float mSpacePercentBottom) {
        this.mSpacePercentBottom = mSpacePercentBottom;
    }
    
    public void setSpaceTop(final float mSpacePercentTop) {
        this.mSpacePercentTop = mSpacePercentTop;
    }
    
    @Deprecated
    public void setStartAtZero(final boolean b) {
        if (b) {
            this.setAxisMinimum(0.0f);
        }
        else {
            this.resetAxisMinimum();
        }
    }
    
    @Deprecated
    public void setUseAutoScaleMaxRestriction(final boolean mUseAutoScaleRestrictionMax) {
        this.mUseAutoScaleRestrictionMax = mUseAutoScaleRestrictionMax;
    }
    
    @Deprecated
    public void setUseAutoScaleMinRestriction(final boolean mUseAutoScaleRestrictionMin) {
        this.mUseAutoScaleRestrictionMin = mUseAutoScaleRestrictionMin;
    }
    
    public void setZeroLineColor(final int mZeroLineColor) {
        this.mZeroLineColor = mZeroLineColor;
    }
    
    public void setZeroLineWidth(final float n) {
        this.mZeroLineWidth = Utils.convertDpToPixel(n);
    }
    
    public enum AxisDependency
    {
        private static final AxisDependency[] $VALUES;
        
        LEFT, 
        RIGHT;
    }
    
    public enum YAxisLabelPosition
    {
        private static final YAxisLabelPosition[] $VALUES;
        
        INSIDE_CHART, 
        OUTSIDE_CHART;
    }
}

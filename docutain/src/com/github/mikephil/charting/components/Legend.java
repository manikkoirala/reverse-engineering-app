// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.components;

import com.github.mikephil.charting.utils.ViewPortHandler;
import android.graphics.Paint;
import com.github.mikephil.charting.utils.Utils;
import java.util.ArrayList;
import android.graphics.DashPathEffect;
import com.github.mikephil.charting.utils.FSize;
import java.util.List;

public class Legend extends ComponentBase
{
    private List<Boolean> mCalculatedLabelBreakPoints;
    private List<FSize> mCalculatedLabelSizes;
    private List<FSize> mCalculatedLineSizes;
    private LegendDirection mDirection;
    private boolean mDrawInside;
    private LegendEntry[] mEntries;
    private LegendEntry[] mExtraEntries;
    private DashPathEffect mFormLineDashEffect;
    private float mFormLineWidth;
    private float mFormSize;
    private float mFormToTextSpace;
    private LegendHorizontalAlignment mHorizontalAlignment;
    private boolean mIsLegendCustom;
    private float mMaxSizePercent;
    public float mNeededHeight;
    public float mNeededWidth;
    private LegendOrientation mOrientation;
    private LegendForm mShape;
    private float mStackSpace;
    public float mTextHeightMax;
    public float mTextWidthMax;
    private LegendVerticalAlignment mVerticalAlignment;
    private boolean mWordWrapEnabled;
    private float mXEntrySpace;
    private float mYEntrySpace;
    
    public Legend() {
        this.mEntries = new LegendEntry[0];
        this.mIsLegendCustom = false;
        this.mHorizontalAlignment = LegendHorizontalAlignment.LEFT;
        this.mVerticalAlignment = LegendVerticalAlignment.BOTTOM;
        this.mOrientation = LegendOrientation.HORIZONTAL;
        this.mDrawInside = false;
        this.mDirection = LegendDirection.LEFT_TO_RIGHT;
        this.mShape = LegendForm.SQUARE;
        this.mFormSize = 8.0f;
        this.mFormLineWidth = 3.0f;
        this.mFormLineDashEffect = null;
        this.mXEntrySpace = 6.0f;
        this.mYEntrySpace = 0.0f;
        this.mFormToTextSpace = 5.0f;
        this.mStackSpace = 3.0f;
        this.mMaxSizePercent = 0.95f;
        this.mNeededWidth = 0.0f;
        this.mNeededHeight = 0.0f;
        this.mTextHeightMax = 0.0f;
        this.mTextWidthMax = 0.0f;
        this.mWordWrapEnabled = false;
        this.mCalculatedLabelSizes = new ArrayList<FSize>(16);
        this.mCalculatedLabelBreakPoints = new ArrayList<Boolean>(16);
        this.mCalculatedLineSizes = new ArrayList<FSize>(16);
        this.mTextSize = Utils.convertDpToPixel(10.0f);
        this.mXOffset = Utils.convertDpToPixel(5.0f);
        this.mYOffset = Utils.convertDpToPixel(3.0f);
    }
    
    public Legend(final LegendEntry[] mEntries) {
        this();
        if (mEntries != null) {
            this.mEntries = mEntries;
            return;
        }
        throw new IllegalArgumentException("entries array is NULL");
    }
    
    public void calculateDimensions(final Paint paint, final ViewPortHandler viewPortHandler) {
        final float convertDpToPixel = Utils.convertDpToPixel(this.mFormSize);
        final float convertDpToPixel2 = Utils.convertDpToPixel(this.mStackSpace);
        final float convertDpToPixel3 = Utils.convertDpToPixel(this.mFormToTextSpace);
        final float convertDpToPixel4 = Utils.convertDpToPixel(this.mXEntrySpace);
        final float convertDpToPixel5 = Utils.convertDpToPixel(this.mYEntrySpace);
        final boolean mWordWrapEnabled = this.mWordWrapEnabled;
        final LegendEntry[] mEntries = this.mEntries;
        final int length = mEntries.length;
        this.mTextWidthMax = this.getMaximumEntryWidth(paint);
        this.mTextHeightMax = this.getMaximumEntryHeight(paint);
        final int n = Legend$1.$SwitchMap$com$github$mikephil$charting$components$Legend$LegendOrientation[this.mOrientation.ordinal()];
        if (n != 1) {
            if (n == 2) {
                final float lineHeight = Utils.getLineHeight(paint);
                final float lineSpacing = Utils.getLineSpacing(paint);
                final float contentWidth = viewPortHandler.contentWidth();
                final float mMaxSizePercent = this.mMaxSizePercent;
                this.mCalculatedLabelBreakPoints.clear();
                this.mCalculatedLabelSizes.clear();
                this.mCalculatedLineSizes.clear();
                int i = 0;
                int n2 = -1;
                float n3 = 0.0f;
                float n4 = 0.0f;
                float b = 0.0f;
                final LegendEntry[] array = mEntries;
                final float n5 = convertDpToPixel2;
                final float n6 = convertDpToPixel;
                while (i < length) {
                    final LegendEntry legendEntry = array[i];
                    final boolean b2 = legendEntry.form != LegendForm.NONE;
                    float convertDpToPixel6;
                    if (Float.isNaN(legendEntry.formSize)) {
                        convertDpToPixel6 = n6;
                    }
                    else {
                        convertDpToPixel6 = Utils.convertDpToPixel(legendEntry.formSize);
                    }
                    final String label = legendEntry.label;
                    this.mCalculatedLabelBreakPoints.add(false);
                    float n7;
                    if (n2 == -1) {
                        n7 = 0.0f;
                    }
                    else {
                        n7 = n4 + n5;
                    }
                    float n9;
                    int n10;
                    if (label != null) {
                        this.mCalculatedLabelSizes.add(Utils.calcTextSize(paint, label));
                        float n8;
                        if (b2) {
                            n8 = convertDpToPixel3 + convertDpToPixel6;
                        }
                        else {
                            n8 = 0.0f;
                        }
                        n9 = n7 + n8 + this.mCalculatedLabelSizes.get(i).width;
                        n10 = n2;
                    }
                    else {
                        this.mCalculatedLabelSizes.add(FSize.getInstance(0.0f, 0.0f));
                        if (!b2) {
                            convertDpToPixel6 = 0.0f;
                        }
                        final float n11 = n9 = n7 + convertDpToPixel6;
                        n10 = n2;
                        if (n2 == -1) {
                            n10 = i;
                            n9 = n11;
                        }
                    }
                    float a = 0.0f;
                    float n12 = 0.0f;
                    Label_0597: {
                        if (label == null) {
                            a = n3;
                            n12 = b;
                            if (i != length - 1) {
                                break Label_0597;
                            }
                        }
                        final float n13 = fcmpl(b, 0.0f);
                        float n14;
                        if (n13 == 0) {
                            n14 = 0.0f;
                        }
                        else {
                            n14 = convertDpToPixel4;
                        }
                        float b3;
                        if (mWordWrapEnabled && n13 != 0 && contentWidth * mMaxSizePercent - b < n14 + n9) {
                            this.mCalculatedLineSizes.add(FSize.getInstance(b, lineHeight));
                            a = Math.max(n3, b);
                            final List<Boolean> mCalculatedLabelBreakPoints = this.mCalculatedLabelBreakPoints;
                            int n15;
                            if (n10 > -1) {
                                n15 = n10;
                            }
                            else {
                                n15 = i;
                            }
                            mCalculatedLabelBreakPoints.set(n15, true);
                            b3 = n9;
                        }
                        else {
                            a = n3;
                            b3 = b + (n14 + n9);
                        }
                        if (i == length - 1) {
                            this.mCalculatedLineSizes.add(FSize.getInstance(b3, lineHeight));
                            a = Math.max(a, b3);
                        }
                        n12 = b3;
                    }
                    if (label != null) {
                        n10 = -1;
                    }
                    ++i;
                    n2 = n10;
                    n3 = a;
                    n4 = n9;
                    b = n12;
                }
                this.mNeededWidth = n3;
                final float n16 = (float)this.mCalculatedLineSizes.size();
                int n17;
                if (this.mCalculatedLineSizes.size() == 0) {
                    n17 = 0;
                }
                else {
                    n17 = this.mCalculatedLineSizes.size() - 1;
                }
                this.mNeededHeight = lineHeight * n16 + (lineSpacing + convertDpToPixel5) * n17;
            }
        }
        else {
            final float lineHeight2 = Utils.getLineHeight(paint);
            float mNeededHeight = 0.0f;
            float b4 = 0.0f;
            int j = 0;
            float mNeededWidth = 0.0f;
            int n18 = 0;
            while (j < length) {
                final LegendEntry legendEntry2 = mEntries[j];
                final boolean b5 = legendEntry2.form != LegendForm.NONE;
                float convertDpToPixel7;
                if (Float.isNaN(legendEntry2.formSize)) {
                    convertDpToPixel7 = convertDpToPixel;
                }
                else {
                    convertDpToPixel7 = Utils.convertDpToPixel(legendEntry2.formSize);
                }
                final String label2 = legendEntry2.label;
                if (n18 == 0) {
                    b4 = 0.0f;
                }
                float b6 = b4;
                if (b5) {
                    float n19 = b4;
                    if (n18 != 0) {
                        n19 = b4 + convertDpToPixel2;
                    }
                    b6 = n19 + convertDpToPixel7;
                }
                if (label2 != null) {
                    float n21 = 0.0f;
                    Label_0881: {
                        float n20;
                        if (b5 && n18 == 0) {
                            n20 = b6 + convertDpToPixel3;
                        }
                        else {
                            n20 = b6;
                            if (n18 != 0) {
                                mNeededWidth = Math.max(mNeededWidth, b6);
                                mNeededHeight += lineHeight2 + convertDpToPixel5;
                                n21 = 0.0f;
                                n18 = 0;
                                break Label_0881;
                            }
                        }
                        n21 = n20;
                    }
                    final float n22 = (float)Utils.calcTextWidth(paint, label2);
                    float n23 = mNeededHeight;
                    if (j < length - 1) {
                        n23 = mNeededHeight + (lineHeight2 + convertDpToPixel5);
                    }
                    final float n24 = n21 + n22;
                    mNeededHeight = n23;
                    b4 = n24;
                }
                else {
                    final float n25 = b4 = b6 + convertDpToPixel7;
                    if (j < length - 1) {
                        b4 = n25 + convertDpToPixel2;
                    }
                    n18 = 1;
                }
                mNeededWidth = Math.max(mNeededWidth, b4);
                ++j;
            }
            this.mNeededWidth = mNeededWidth;
            this.mNeededHeight = mNeededHeight;
        }
        this.mNeededHeight += this.mYOffset;
        this.mNeededWidth += this.mXOffset;
    }
    
    public List<Boolean> getCalculatedLabelBreakPoints() {
        return this.mCalculatedLabelBreakPoints;
    }
    
    public List<FSize> getCalculatedLabelSizes() {
        return this.mCalculatedLabelSizes;
    }
    
    public List<FSize> getCalculatedLineSizes() {
        return this.mCalculatedLineSizes;
    }
    
    public LegendDirection getDirection() {
        return this.mDirection;
    }
    
    public LegendEntry[] getEntries() {
        return this.mEntries;
    }
    
    public LegendEntry[] getExtraEntries() {
        return this.mExtraEntries;
    }
    
    public LegendForm getForm() {
        return this.mShape;
    }
    
    public DashPathEffect getFormLineDashEffect() {
        return this.mFormLineDashEffect;
    }
    
    public float getFormLineWidth() {
        return this.mFormLineWidth;
    }
    
    public float getFormSize() {
        return this.mFormSize;
    }
    
    public float getFormToTextSpace() {
        return this.mFormToTextSpace;
    }
    
    public LegendHorizontalAlignment getHorizontalAlignment() {
        return this.mHorizontalAlignment;
    }
    
    public float getMaxSizePercent() {
        return this.mMaxSizePercent;
    }
    
    public float getMaximumEntryHeight(final Paint paint) {
        final LegendEntry[] mEntries = this.mEntries;
        final int length = mEntries.length;
        float n = 0.0f;
        float n2;
        for (int i = 0; i < length; ++i, n = n2) {
            final String label = mEntries[i].label;
            if (label == null) {
                n2 = n;
            }
            else {
                final float n3 = (float)Utils.calcTextHeight(paint, label);
                n2 = n;
                if (n3 > n) {
                    n2 = n3;
                }
            }
        }
        return n;
    }
    
    public float getMaximumEntryWidth(final Paint paint) {
        final float convertDpToPixel = Utils.convertDpToPixel(this.mFormToTextSpace);
        final LegendEntry[] mEntries = this.mEntries;
        final int length = mEntries.length;
        float n = 0.0f;
        float n2 = 0.0f;
        float n4;
        float n5;
        for (int i = 0; i < length; ++i, n = n5, n2 = n4) {
            final LegendEntry legendEntry = mEntries[i];
            float n3;
            if (Float.isNaN(legendEntry.formSize)) {
                n3 = this.mFormSize;
            }
            else {
                n3 = legendEntry.formSize;
            }
            final float convertDpToPixel2 = Utils.convertDpToPixel(n3);
            n4 = n2;
            if (convertDpToPixel2 > n2) {
                n4 = convertDpToPixel2;
            }
            final String label = legendEntry.label;
            if (label == null) {
                n5 = n;
            }
            else {
                final float n6 = (float)Utils.calcTextWidth(paint, label);
                n5 = n;
                if (n6 > n) {
                    n5 = n6;
                }
            }
        }
        return n + n2 + convertDpToPixel;
    }
    
    public LegendOrientation getOrientation() {
        return this.mOrientation;
    }
    
    public float getStackSpace() {
        return this.mStackSpace;
    }
    
    public LegendVerticalAlignment getVerticalAlignment() {
        return this.mVerticalAlignment;
    }
    
    public float getXEntrySpace() {
        return this.mXEntrySpace;
    }
    
    public float getYEntrySpace() {
        return this.mYEntrySpace;
    }
    
    public boolean isDrawInsideEnabled() {
        return this.mDrawInside;
    }
    
    public boolean isLegendCustom() {
        return this.mIsLegendCustom;
    }
    
    public boolean isWordWrapEnabled() {
        return this.mWordWrapEnabled;
    }
    
    public void resetCustom() {
        this.mIsLegendCustom = false;
    }
    
    public void setCustom(final List<LegendEntry> list) {
        this.mEntries = list.toArray(new LegendEntry[list.size()]);
        this.mIsLegendCustom = true;
    }
    
    public void setCustom(final LegendEntry[] mEntries) {
        this.mEntries = mEntries;
        this.mIsLegendCustom = true;
    }
    
    public void setDirection(final LegendDirection mDirection) {
        this.mDirection = mDirection;
    }
    
    public void setDrawInside(final boolean mDrawInside) {
        this.mDrawInside = mDrawInside;
    }
    
    public void setEntries(final List<LegendEntry> list) {
        this.mEntries = list.toArray(new LegendEntry[list.size()]);
    }
    
    public void setExtra(final List<LegendEntry> list) {
        this.mExtraEntries = list.toArray(new LegendEntry[list.size()]);
    }
    
    public void setExtra(final int[] array, final String[] array2) {
        final ArrayList list = new ArrayList();
        for (int i = 0; i < Math.min(array.length, array2.length); ++i) {
            final LegendEntry legendEntry = new LegendEntry();
            legendEntry.formColor = array[i];
            legendEntry.label = array2[i];
            if (legendEntry.formColor != 1122868 && legendEntry.formColor != 0) {
                if (legendEntry.formColor == 1122867) {
                    legendEntry.form = LegendForm.EMPTY;
                }
            }
            else {
                legendEntry.form = LegendForm.NONE;
            }
            list.add(legendEntry);
        }
        this.mExtraEntries = (LegendEntry[])list.toArray(new LegendEntry[list.size()]);
    }
    
    public void setExtra(final LegendEntry[] array) {
        LegendEntry[] mExtraEntries = array;
        if (array == null) {
            mExtraEntries = new LegendEntry[0];
        }
        this.mExtraEntries = mExtraEntries;
    }
    
    public void setForm(final LegendForm mShape) {
        this.mShape = mShape;
    }
    
    public void setFormLineDashEffect(final DashPathEffect mFormLineDashEffect) {
        this.mFormLineDashEffect = mFormLineDashEffect;
    }
    
    public void setFormLineWidth(final float mFormLineWidth) {
        this.mFormLineWidth = mFormLineWidth;
    }
    
    public void setFormSize(final float mFormSize) {
        this.mFormSize = mFormSize;
    }
    
    public void setFormToTextSpace(final float mFormToTextSpace) {
        this.mFormToTextSpace = mFormToTextSpace;
    }
    
    public void setHorizontalAlignment(final LegendHorizontalAlignment mHorizontalAlignment) {
        this.mHorizontalAlignment = mHorizontalAlignment;
    }
    
    public void setMaxSizePercent(final float mMaxSizePercent) {
        this.mMaxSizePercent = mMaxSizePercent;
    }
    
    public void setOrientation(final LegendOrientation mOrientation) {
        this.mOrientation = mOrientation;
    }
    
    public void setStackSpace(final float mStackSpace) {
        this.mStackSpace = mStackSpace;
    }
    
    public void setVerticalAlignment(final LegendVerticalAlignment mVerticalAlignment) {
        this.mVerticalAlignment = mVerticalAlignment;
    }
    
    public void setWordWrapEnabled(final boolean mWordWrapEnabled) {
        this.mWordWrapEnabled = mWordWrapEnabled;
    }
    
    public void setXEntrySpace(final float mxEntrySpace) {
        this.mXEntrySpace = mxEntrySpace;
    }
    
    public void setYEntrySpace(final float myEntrySpace) {
        this.mYEntrySpace = myEntrySpace;
    }
    
    public enum LegendDirection
    {
        private static final LegendDirection[] $VALUES;
        
        LEFT_TO_RIGHT, 
        RIGHT_TO_LEFT;
    }
    
    public enum LegendForm
    {
        private static final LegendForm[] $VALUES;
        
        CIRCLE, 
        DEFAULT, 
        EMPTY, 
        LINE, 
        NONE, 
        SQUARE;
    }
    
    public enum LegendHorizontalAlignment
    {
        private static final LegendHorizontalAlignment[] $VALUES;
        
        CENTER, 
        LEFT, 
        RIGHT;
    }
    
    public enum LegendOrientation
    {
        private static final LegendOrientation[] $VALUES;
        
        HORIZONTAL, 
        VERTICAL;
    }
    
    public enum LegendVerticalAlignment
    {
        private static final LegendVerticalAlignment[] $VALUES;
        
        BOTTOM, 
        CENTER, 
        TOP;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.interfaces.dataprovider;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.data.ChartData;
import android.graphics.RectF;
import com.github.mikephil.charting.utils.MPPointF;

public interface ChartInterface
{
    MPPointF getCenterOfView();
    
    MPPointF getCenterOffsets();
    
    RectF getContentRect();
    
    ChartData getData();
    
    ValueFormatter getDefaultValueFormatter();
    
    int getHeight();
    
    float getMaxHighlightDistance();
    
    int getMaxVisibleCount();
    
    int getWidth();
    
    float getXChartMax();
    
    float getXChartMin();
    
    float getXRange();
    
    float getYChartMax();
    
    float getYChartMin();
}

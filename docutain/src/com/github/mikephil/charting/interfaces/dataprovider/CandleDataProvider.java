// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.interfaces.dataprovider;

import com.github.mikephil.charting.data.CandleData;

public interface CandleDataProvider extends BarLineScatterCandleBubbleDataProvider
{
    CandleData getCandleData();
}

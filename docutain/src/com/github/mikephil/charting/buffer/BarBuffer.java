// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.buffer;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

public class BarBuffer extends AbstractBuffer<IBarDataSet>
{
    protected float mBarWidth;
    protected boolean mContainsStacks;
    protected int mDataSetCount;
    protected int mDataSetIndex;
    protected boolean mInverted;
    
    public BarBuffer(final int n, final int mDataSetCount, final boolean mContainsStacks) {
        super(n);
        this.mDataSetIndex = 0;
        this.mInverted = false;
        this.mBarWidth = 1.0f;
        this.mDataSetCount = mDataSetCount;
        this.mContainsStacks = mContainsStacks;
    }
    
    protected void addBar(final float n, final float n2, final float n3, final float n4) {
        this.buffer[this.index++] = n;
        this.buffer[this.index++] = n2;
        this.buffer[this.index++] = n3;
        this.buffer[this.index++] = n4;
    }
    
    @Override
    public void feed(final IBarDataSet set) {
        final float n = (float)set.getEntryCount();
        final float phaseX = this.phaseX;
        final float n2 = this.mBarWidth / 2.0f;
        for (int n3 = 0; n3 < n * phaseX; ++n3) {
            final BarEntry barEntry = set.getEntryForIndex(n3);
            if (barEntry != null) {
                final float x = barEntry.getX();
                float y = barEntry.getY();
                final float[] yVals = barEntry.getYVals();
                if (this.mContainsStacks && yVals != null) {
                    float n4 = -barEntry.getNegativeSum();
                    int i = 0;
                    float n5 = 0.0f;
                    while (i < yVals.length) {
                        final float n6 = yVals[i];
                        final float n7 = fcmpl(n6, 0.0f);
                        float n8;
                        float n9;
                        float n10;
                        if (n7 == 0 && (n5 == 0.0f || n4 == 0.0f)) {
                            n8 = n6;
                            n9 = n4;
                            n4 = n8;
                            n10 = n5;
                        }
                        else if (n7 >= 0) {
                            n8 = n6 + n5;
                            n9 = n4;
                            n4 = n5;
                            n10 = n8;
                        }
                        else {
                            n8 = Math.abs(n6) + n4;
                            n9 = Math.abs(n6) + n4;
                            n10 = n5;
                        }
                        float n11;
                        if (this.mInverted) {
                            if (n4 >= n8) {
                                n11 = n4;
                            }
                            else {
                                n11 = n8;
                            }
                            if (n4 > n8) {
                                n4 = n8;
                            }
                        }
                        else {
                            float n12;
                            if (n4 >= n8) {
                                n12 = n4;
                            }
                            else {
                                n12 = n8;
                            }
                            if (n4 > n8) {
                                n4 = n8;
                            }
                            final float n13 = n12;
                            n11 = n4;
                            n4 = n13;
                        }
                        this.addBar(x - n2, n4 * this.phaseY, x + n2, n11 * this.phaseY);
                        ++i;
                        n4 = n9;
                        n5 = n10;
                    }
                }
                else {
                    float n14;
                    if (this.mInverted) {
                        if (y >= 0.0f) {
                            n14 = y;
                        }
                        else {
                            n14 = 0.0f;
                        }
                        if (y > 0.0f) {
                            y = 0.0f;
                        }
                    }
                    else {
                        float n15;
                        if (y >= 0.0f) {
                            n15 = y;
                        }
                        else {
                            n15 = 0.0f;
                        }
                        if (y > 0.0f) {
                            y = 0.0f;
                        }
                        final float n16 = n15;
                        n14 = y;
                        y = n16;
                    }
                    if (y > 0.0f) {
                        y *= this.phaseY;
                    }
                    else {
                        n14 *= this.phaseY;
                    }
                    this.addBar(x - n2, y, x + n2, n14);
                }
            }
        }
        this.reset();
    }
    
    public void setBarWidth(final float mBarWidth) {
        this.mBarWidth = mBarWidth;
    }
    
    public void setDataSet(final int mDataSetIndex) {
        this.mDataSetIndex = mDataSetIndex;
    }
    
    public void setInverted(final boolean mInverted) {
        this.mInverted = mInverted;
    }
}

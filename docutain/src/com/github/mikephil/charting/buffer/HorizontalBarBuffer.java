// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.buffer;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

public class HorizontalBarBuffer extends BarBuffer
{
    public HorizontalBarBuffer(final int n, final int n2, final boolean b) {
        super(n, n2, b);
    }
    
    @Override
    public void feed(final IBarDataSet set) {
        final float n = (float)set.getEntryCount();
        final float phaseX = this.phaseX;
        final float n2 = this.mBarWidth / 2.0f;
        for (int n3 = 0; n3 < n * phaseX; ++n3) {
            final BarEntry barEntry = set.getEntryForIndex(n3);
            if (barEntry != null) {
                final float x = barEntry.getX();
                float y = barEntry.getY();
                final float[] yVals = barEntry.getYVals();
                if (this.mContainsStacks && yVals != null) {
                    float n4 = -barEntry.getNegativeSum();
                    int i = 0;
                    float n5 = 0.0f;
                    while (i < yVals.length) {
                        final float n6 = yVals[i];
                        float n7;
                        float n8;
                        float n9;
                        if (n6 >= 0.0f) {
                            n7 = n6 + n5;
                            n8 = n4;
                            n4 = n5;
                            n9 = n7;
                        }
                        else {
                            n7 = Math.abs(n6) + n4;
                            n8 = Math.abs(n6) + n4;
                            n9 = n5;
                        }
                        float n10;
                        if (this.mInverted) {
                            if (n4 >= n7) {
                                n10 = n4;
                            }
                            else {
                                n10 = n7;
                            }
                            if (n4 > n7) {
                                n4 = n7;
                            }
                        }
                        else {
                            float n11;
                            if (n4 >= n7) {
                                n11 = n4;
                            }
                            else {
                                n11 = n7;
                            }
                            if (n4 > n7) {
                                n4 = n7;
                            }
                            final float n12 = n4;
                            n4 = n11;
                            n10 = n12;
                        }
                        this.addBar(n10 * this.phaseY, x + n2, n4 * this.phaseY, x - n2);
                        ++i;
                        n4 = n8;
                        n5 = n9;
                    }
                }
                else {
                    float n13;
                    if (this.mInverted) {
                        if (y >= 0.0f) {
                            n13 = y;
                        }
                        else {
                            n13 = 0.0f;
                        }
                        if (y > 0.0f) {
                            y = 0.0f;
                        }
                    }
                    else {
                        float n14;
                        if (y >= 0.0f) {
                            n14 = y;
                        }
                        else {
                            n14 = 0.0f;
                        }
                        if (y > 0.0f) {
                            y = 0.0f;
                        }
                        final float n15 = n14;
                        n13 = y;
                        y = n15;
                    }
                    if (y > 0.0f) {
                        y *= this.phaseY;
                    }
                    else {
                        n13 *= this.phaseY;
                    }
                    this.addBar(n13, x + n2, y, x - n2);
                }
            }
        }
        this.reset();
    }
}

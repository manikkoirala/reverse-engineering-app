// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.animation;

import android.animation.TimeInterpolator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator$AnimatorUpdateListener;

public class ChartAnimator
{
    private ValueAnimator$AnimatorUpdateListener mListener;
    protected float mPhaseX;
    protected float mPhaseY;
    
    public ChartAnimator() {
        this.mPhaseY = 1.0f;
        this.mPhaseX = 1.0f;
    }
    
    public ChartAnimator(final ValueAnimator$AnimatorUpdateListener mListener) {
        this.mPhaseY = 1.0f;
        this.mPhaseX = 1.0f;
        this.mListener = mListener;
    }
    
    private ObjectAnimator xAnimator(final int n, final Easing.EasingFunction interpolator) {
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat((Object)this, "phaseX", new float[] { 0.0f, 1.0f });
        ofFloat.setInterpolator((TimeInterpolator)interpolator);
        ofFloat.setDuration((long)n);
        return ofFloat;
    }
    
    private ObjectAnimator yAnimator(final int n, final Easing.EasingFunction interpolator) {
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat((Object)this, "phaseY", new float[] { 0.0f, 1.0f });
        ofFloat.setInterpolator((TimeInterpolator)interpolator);
        ofFloat.setDuration((long)n);
        return ofFloat;
    }
    
    public void animateX(final int n) {
        this.animateX(n, Easing.Linear);
    }
    
    public void animateX(final int n, final Easing.EasingFunction easingFunction) {
        final ObjectAnimator xAnimator = this.xAnimator(n, easingFunction);
        xAnimator.addUpdateListener(this.mListener);
        xAnimator.start();
    }
    
    public void animateXY(final int n, final int n2) {
        this.animateXY(n, n2, Easing.Linear, Easing.Linear);
    }
    
    public void animateXY(final int n, final int n2, final Easing.EasingFunction easingFunction) {
        final ObjectAnimator xAnimator = this.xAnimator(n, easingFunction);
        final ObjectAnimator yAnimator = this.yAnimator(n2, easingFunction);
        if (n > n2) {
            xAnimator.addUpdateListener(this.mListener);
        }
        else {
            yAnimator.addUpdateListener(this.mListener);
        }
        xAnimator.start();
        yAnimator.start();
    }
    
    public void animateXY(final int n, final int n2, final Easing.EasingFunction easingFunction, final Easing.EasingFunction easingFunction2) {
        final ObjectAnimator xAnimator = this.xAnimator(n, easingFunction);
        final ObjectAnimator yAnimator = this.yAnimator(n2, easingFunction2);
        if (n > n2) {
            xAnimator.addUpdateListener(this.mListener);
        }
        else {
            yAnimator.addUpdateListener(this.mListener);
        }
        xAnimator.start();
        yAnimator.start();
    }
    
    public void animateY(final int n) {
        this.animateY(n, Easing.Linear);
    }
    
    public void animateY(final int n, final Easing.EasingFunction easingFunction) {
        final ObjectAnimator yAnimator = this.yAnimator(n, easingFunction);
        yAnimator.addUpdateListener(this.mListener);
        yAnimator.start();
    }
    
    public float getPhaseX() {
        return this.mPhaseX;
    }
    
    public float getPhaseY() {
        return this.mPhaseY;
    }
    
    public void setPhaseX(final float n) {
        float mPhaseX;
        if (n > 1.0f) {
            mPhaseX = 1.0f;
        }
        else {
            mPhaseX = n;
            if (n < 0.0f) {
                mPhaseX = 0.0f;
            }
        }
        this.mPhaseX = mPhaseX;
    }
    
    public void setPhaseY(final float n) {
        float mPhaseY;
        if (n > 1.0f) {
            mPhaseY = 1.0f;
        }
        else {
            mPhaseY = n;
            if (n < 0.0f) {
                mPhaseY = 0.0f;
            }
        }
        this.mPhaseY = mPhaseY;
    }
}

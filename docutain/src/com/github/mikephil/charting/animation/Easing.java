// 
// Decompiled by Procyon v0.6.0
// 

package com.github.mikephil.charting.animation;

import android.animation.TimeInterpolator;

public class Easing
{
    private static final float DOUBLE_PI = 6.2831855f;
    public static final EasingFunction EaseInBack;
    public static final EasingFunction EaseInBounce;
    public static final EasingFunction EaseInCirc;
    public static final EasingFunction EaseInCubic;
    public static final EasingFunction EaseInElastic;
    public static final EasingFunction EaseInExpo;
    public static final EasingFunction EaseInOutBack;
    public static final EasingFunction EaseInOutBounce;
    public static final EasingFunction EaseInOutCirc;
    public static final EasingFunction EaseInOutCubic;
    public static final EasingFunction EaseInOutElastic;
    public static final EasingFunction EaseInOutExpo;
    public static final EasingFunction EaseInOutQuad;
    public static final EasingFunction EaseInOutQuart;
    public static final EasingFunction EaseInOutSine;
    public static final EasingFunction EaseInQuad;
    public static final EasingFunction EaseInQuart;
    public static final EasingFunction EaseInSine;
    public static final EasingFunction EaseOutBack;
    public static final EasingFunction EaseOutBounce;
    public static final EasingFunction EaseOutCirc;
    public static final EasingFunction EaseOutCubic;
    public static final EasingFunction EaseOutElastic;
    public static final EasingFunction EaseOutExpo;
    public static final EasingFunction EaseOutQuad;
    public static final EasingFunction EaseOutQuart;
    public static final EasingFunction EaseOutSine;
    public static final EasingFunction Linear;
    
    static {
        Linear = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return n;
            }
        };
        EaseInQuad = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return n * n;
            }
        };
        EaseOutQuad = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return -n * (n - 2.0f);
            }
        };
        EaseInOutQuad = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                n *= 2.0f;
                if (n < 1.0f) {
                    return 0.5f * n * n;
                }
                --n;
                return (n * (n - 2.0f) - 1.0f) * -0.5f;
            }
        };
        EaseInCubic = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return (float)Math.pow(n, 3.0);
            }
        };
        EaseOutCubic = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return (float)Math.pow(n - 1.0f, 3.0) + 1.0f;
            }
        };
        EaseInOutCubic = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                n *= 2.0f;
                if (n < 1.0f) {
                    n = (float)Math.pow(n, 3.0);
                }
                else {
                    n = (float)Math.pow(n - 2.0f, 3.0) + 2.0f;
                }
                return n * 0.5f;
            }
        };
        EaseInQuart = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return (float)Math.pow(n, 4.0);
            }
        };
        EaseOutQuart = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return -((float)Math.pow(n - 1.0f, 4.0) - 1.0f);
            }
        };
        EaseInOutQuart = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                n *= 2.0f;
                if (n < 1.0f) {
                    return (float)Math.pow(n, 4.0) * 0.5f;
                }
                return ((float)Math.pow(n - 2.0f, 4.0) - 2.0f) * -0.5f;
            }
        };
        EaseInSine = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return -(float)Math.cos(n * 1.5707963267948966) + 1.0f;
            }
        };
        EaseOutSine = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return (float)Math.sin(n * 1.5707963267948966);
            }
        };
        EaseInOutSine = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return ((float)Math.cos(n * 3.141592653589793) - 1.0f) * -0.5f;
            }
        };
        EaseInExpo = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                final float n2 = 0.0f;
                if (n == 0.0f) {
                    n = n2;
                }
                else {
                    n = (float)Math.pow(2.0, (n - 1.0f) * 10.0f);
                }
                return n;
            }
        };
        EaseOutExpo = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                final float n2 = 1.0f;
                if (n == 1.0f) {
                    n = n2;
                }
                else {
                    n = -(float)Math.pow(2.0, (n + 1.0f) * -10.0f);
                }
                return n;
            }
        };
        EaseInOutExpo = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                if (n == 0.0f) {
                    return 0.0f;
                }
                if (n == 1.0f) {
                    return 1.0f;
                }
                n *= 2.0f;
                if (n < 1.0f) {
                    n = (float)Math.pow(2.0, (n - 1.0f) * 10.0f);
                }
                else {
                    n = -(float)Math.pow(2.0, (n - 1.0f) * -10.0f) + 2.0f;
                }
                return n * 0.5f;
            }
        };
        EaseInCirc = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return -((float)Math.sqrt(1.0f - n * n) - 1.0f);
            }
        };
        EaseOutCirc = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                --n;
                return (float)Math.sqrt(1.0f - n * n);
            }
        };
        EaseInOutCirc = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                n *= 2.0f;
                if (n < 1.0f) {
                    return ((float)Math.sqrt(1.0f - n * n) - 1.0f) * -0.5f;
                }
                n -= 2.0f;
                return ((float)Math.sqrt(1.0f - n * n) + 1.0f) * 0.5f;
            }
        };
        EaseInElastic = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                if (n == 0.0f) {
                    return 0.0f;
                }
                if (n == 1.0f) {
                    return 1.0f;
                }
                final float n2 = (float)Math.asin(1.0);
                --n;
                return -((float)Math.pow(2.0, 10.0f * n) * (float)Math.sin((n - 0.047746483f * n2) * 6.2831855f / 0.3f));
            }
        };
        EaseOutElastic = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                if (n == 0.0f) {
                    return 0.0f;
                }
                if (n == 1.0f) {
                    return 1.0f;
                }
                return (float)Math.pow(2.0, -10.0f * n) * (float)Math.sin((n - 0.047746483f * (float)Math.asin(1.0)) * 6.2831855f / 0.3f) + 1.0f;
            }
        };
        EaseInOutElastic = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                if (n == 0.0f) {
                    return 0.0f;
                }
                final float n2 = n * 2.0f;
                if (n2 == 2.0f) {
                    return 1.0f;
                }
                n = (float)Math.asin(1.0) * 0.07161972f;
                if (n2 < 1.0f) {
                    final float n3 = n2 - 1.0f;
                    return (float)Math.pow(2.0, 10.0f * n3) * (float)Math.sin((n3 * 1.0f - n) * 6.2831855f * 2.2222223f) * -0.5f;
                }
                final float n4 = n2 - 1.0f;
                return (float)Math.pow(2.0, -10.0f * n4) * 0.5f * (float)Math.sin((n4 * 1.0f - n) * 6.2831855f * 2.2222223f) + 1.0f;
            }
        };
        EaseInBack = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return n * n * (n * 2.70158f - 1.70158f);
            }
        };
        EaseOutBack = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                --n;
                return n * n * (n * 2.70158f + 1.70158f) + 1.0f;
            }
        };
        EaseInOutBack = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                n *= 2.0f;
                if (n < 1.0f) {
                    return n * n * (3.5949094f * n - 2.5949094f) * 0.5f;
                }
                n -= 2.0f;
                return (n * n * (3.5949094f * n + 2.5949094f) + 2.0f) * 0.5f;
            }
        };
        EaseInBounce = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                return 1.0f - Easing.EaseOutBounce.getInterpolation(1.0f - n);
            }
        };
        EaseOutBounce = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(float n) {
                if (n < 0.36363637f) {
                    return 7.5625f * n * n;
                }
                if (n < 0.72727275f) {
                    n -= 0.54545456f;
                    return 7.5625f * n * n + 0.75f;
                }
                if (n < 0.90909094f) {
                    n -= 0.8181818f;
                    return 7.5625f * n * n + 0.9375f;
                }
                n -= 0.95454544f;
                return 7.5625f * n * n + 0.984375f;
            }
        };
        EaseInOutBounce = (EasingFunction)new EasingFunction() {
            @Override
            public float getInterpolation(final float n) {
                if (n < 0.5f) {
                    return Easing.EaseInBounce.getInterpolation(n * 2.0f) * 0.5f;
                }
                return Easing.EaseOutBounce.getInterpolation(n * 2.0f - 1.0f) * 0.5f + 0.5f;
            }
        };
    }
    
    public interface EasingFunction extends TimeInterpolator
    {
        float getInterpolation(final float p0);
    }
}

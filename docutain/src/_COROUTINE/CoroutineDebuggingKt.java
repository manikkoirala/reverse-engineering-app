// 
// Decompiled by Procyon v0.6.0
// 

package _COROUTINE;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0002\u001a\u0014\u0010\u0004\u001a\u00020\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0001H\u0002\"\u0014\u0010\u0000\u001a\u00020\u0001X\u0080D¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0003¨\u0006\b" }, d2 = { "ARTIFICIAL_FRAME_PACKAGE_NAME", "", "getARTIFICIAL_FRAME_PACKAGE_NAME", "()Ljava/lang/String;", "artificialFrame", "Ljava/lang/StackTraceElement;", "", "name", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class CoroutineDebuggingKt
{
    private static final String ARTIFICIAL_FRAME_PACKAGE_NAME = "_COROUTINE";
    
    private static final StackTraceElement artificialFrame(final Throwable t, final String str) {
        final StackTraceElement stackTraceElement = t.getStackTrace()[0];
        final StringBuilder sb = new StringBuilder();
        sb.append(CoroutineDebuggingKt.ARTIFICIAL_FRAME_PACKAGE_NAME);
        sb.append('.');
        sb.append(str);
        return new StackTraceElement(sb.toString(), "_", stackTraceElement.getFileName(), stackTraceElement.getLineNumber());
    }
    
    public static final String getARTIFICIAL_FRAME_PACKAGE_NAME() {
        return CoroutineDebuggingKt.ARTIFICIAL_FRAME_PACKAGE_NAME;
    }
}

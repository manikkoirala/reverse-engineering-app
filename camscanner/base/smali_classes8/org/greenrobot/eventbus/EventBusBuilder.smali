.class public Lorg/greenrobot/eventbus/EventBusBuilder;
.super Ljava/lang/Object;
.source "EventBusBuilder.java"


# static fields
.field private static final OO0o〇〇:Ljava/util/concurrent/ExecutorService;


# instance fields
.field O8:Z

.field OO0o〇〇〇〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/greenrobot/eventbus/meta/SubscriberInfoIndex;",
            ">;"
        }
    .end annotation
.end field

.field Oo08:Z

.field oO80:Z

.field o〇0:Z

.field 〇080:Z

.field 〇80〇808〇O:Ljava/util/concurrent/ExecutorService;

.field 〇8o8o〇:Lorg/greenrobot/eventbus/Logger;

.field 〇O8o08O:Lorg/greenrobot/eventbus/MainThreadSupport;

.field 〇o00〇〇Oo:Z

.field 〇o〇:Z

.field 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sput-object v0, Lorg/greenrobot/eventbus/EventBusBuilder;->OO0o〇〇:Ljava/util/concurrent/ExecutorService;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->〇080:Z

    .line 6
    .line 7
    iput-boolean v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->〇o00〇〇Oo:Z

    .line 8
    .line 9
    iput-boolean v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->〇o〇:Z

    .line 10
    .line 11
    iput-boolean v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->O8:Z

    .line 12
    .line 13
    iput-boolean v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->o〇0:Z

    .line 14
    .line 15
    sget-object v0, Lorg/greenrobot/eventbus/EventBusBuilder;->OO0o〇〇:Ljava/util/concurrent/ExecutorService;

    .line 16
    .line 17
    iput-object v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->〇80〇808〇O:Ljava/util/concurrent/ExecutorService;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method O8()Lorg/greenrobot/eventbus/MainThreadSupport;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->〇O8o08O:Lorg/greenrobot/eventbus/MainThreadSupport;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-static {}, Lorg/greenrobot/eventbus/android/AndroidComponents;->〇080()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-static {}, Lorg/greenrobot/eventbus/android/AndroidComponents;->〇o00〇〇Oo()Lorg/greenrobot/eventbus/android/AndroidComponents;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v0, v0, Lorg/greenrobot/eventbus/android/AndroidComponents;->〇o00〇〇Oo:Lorg/greenrobot/eventbus/MainThreadSupport;

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_1
    const/4 v0, 0x0

    .line 20
    return-object v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public Oo08()Lorg/greenrobot/eventbus/EventBus;
    .locals 3

    .line 1
    const-class v0, Lorg/greenrobot/eventbus/EventBus;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lorg/greenrobot/eventbus/EventBus;->〇0〇O0088o:Lorg/greenrobot/eventbus/EventBus;

    .line 5
    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lorg/greenrobot/eventbus/EventBusBuilder;->〇o00〇〇Oo()Lorg/greenrobot/eventbus/EventBus;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    sput-object v1, Lorg/greenrobot/eventbus/EventBus;->〇0〇O0088o:Lorg/greenrobot/eventbus/EventBus;

    .line 13
    .line 14
    sget-object v1, Lorg/greenrobot/eventbus/EventBus;->〇0〇O0088o:Lorg/greenrobot/eventbus/EventBus;

    .line 15
    .line 16
    monitor-exit v0

    .line 17
    return-object v1

    .line 18
    :cond_0
    new-instance v1, Lorg/greenrobot/eventbus/EventBusException;

    .line 19
    .line 20
    const-string v2, "Default instance already exists. It may be only set once before it\'s used the first time to ensure consistent behavior."

    .line 21
    .line 22
    invoke-direct {v1, v2}, Lorg/greenrobot/eventbus/EventBusException;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    throw v1

    .line 26
    :catchall_0
    move-exception v1

    .line 27
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    throw v1
    .line 29
    .line 30
.end method

.method public 〇080(Lorg/greenrobot/eventbus/meta/SubscriberInfoIndex;)Lorg/greenrobot/eventbus/EventBusBuilder;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->OO0o〇〇〇〇0:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->OO0o〇〇〇〇0:Ljava/util/List;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->OO0o〇〇〇〇0:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    return-object p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇o00〇〇Oo()Lorg/greenrobot/eventbus/EventBus;
    .locals 1

    .line 1
    new-instance v0, Lorg/greenrobot/eventbus/EventBus;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lorg/greenrobot/eventbus/EventBus;-><init>(Lorg/greenrobot/eventbus/EventBusBuilder;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method 〇o〇()Lorg/greenrobot/eventbus/Logger;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/greenrobot/eventbus/EventBusBuilder;->〇8o8o〇:Lorg/greenrobot/eventbus/Logger;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-static {}, Lorg/greenrobot/eventbus/Logger$Default;->〇080()Lorg/greenrobot/eventbus/Logger;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

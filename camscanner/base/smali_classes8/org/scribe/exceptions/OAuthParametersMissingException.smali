.class public Lorg/scribe/exceptions/OAuthParametersMissingException;
.super Lorg/scribe/exceptions/OAuthException;
.source "OAuthParametersMissingException.java"


# static fields
.field private static final MSG:Ljava/lang/String; = "Could not find oauth parameters in request: %s. OAuth parameters must be specified with the addOAuthParameter() method"

.field private static final serialVersionUID:J = 0x18389511fdda10dfL


# direct methods
.method public constructor <init>(Lorg/scribe/model/OAuthRequest;)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    aput-object p1, v0, v1

    .line 6
    .line 7
    const-string p1, "Could not find oauth parameters in request: %s. OAuth parameters must be specified with the addOAuthParameter() method"

    .line 8
    .line 9
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-direct {p0, p1}, Lorg/scribe/exceptions/OAuthException;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

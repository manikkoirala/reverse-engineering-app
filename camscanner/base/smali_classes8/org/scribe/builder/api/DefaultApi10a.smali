.class public abstract Lorg/scribe/builder/api/DefaultApi10a;
.super Ljava/lang/Object;
.source "DefaultApi10a.java"

# interfaces
.implements Lorg/scribe/builder/api/Api;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public createService(Lorg/scribe/model/OAuthConfig;)Lorg/scribe/oauth/OAuthService;
    .locals 1

    .line 1
    new-instance v0, Lorg/scribe/oauth/OAuth10aServiceImpl;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lorg/scribe/oauth/OAuth10aServiceImpl;-><init>(Lorg/scribe/builder/api/DefaultApi10a;Lorg/scribe/model/OAuthConfig;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public abstract getAccessTokenEndpoint()Ljava/lang/String;
.end method

.method public getAccessTokenExtractor()Lorg/scribe/extractors/AccessTokenExtractor;
    .locals 1

    .line 1
    new-instance v0, Lorg/scribe/extractors/TokenExtractorImpl;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/scribe/extractors/TokenExtractorImpl;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getAccessTokenVerb()Lorg/scribe/model/Verb;
    .locals 1

    .line 1
    sget-object v0, Lorg/scribe/model/Verb;->POST:Lorg/scribe/model/Verb;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getAuthorizationUrl(Lorg/scribe/model/Token;)Ljava/lang/String;
.end method

.method public getBaseStringExtractor()Lorg/scribe/extractors/BaseStringExtractor;
    .locals 1

    .line 1
    new-instance v0, Lorg/scribe/extractors/BaseStringExtractorImpl;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/scribe/extractors/BaseStringExtractorImpl;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeaderExtractor()Lorg/scribe/extractors/HeaderExtractor;
    .locals 1

    .line 1
    new-instance v0, Lorg/scribe/extractors/HeaderExtractorImpl;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/scribe/extractors/HeaderExtractorImpl;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getRequestTokenEndpoint()Ljava/lang/String;
.end method

.method public getRequestTokenExtractor()Lorg/scribe/extractors/RequestTokenExtractor;
    .locals 1

    .line 1
    new-instance v0, Lorg/scribe/extractors/TokenExtractorImpl;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/scribe/extractors/TokenExtractorImpl;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRequestTokenVerb()Lorg/scribe/model/Verb;
    .locals 1

    .line 1
    sget-object v0, Lorg/scribe/model/Verb;->POST:Lorg/scribe/model/Verb;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSignatureService()Lorg/scribe/services/SignatureService;
    .locals 1

    .line 1
    new-instance v0, Lorg/scribe/services/HMACSha1SignatureService;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/scribe/services/HMACSha1SignatureService;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTimestampService()Lorg/scribe/services/TimestampService;
    .locals 1

    .line 1
    new-instance v0, Lorg/scribe/services/TimestampServiceImpl;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/scribe/services/TimestampServiceImpl;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

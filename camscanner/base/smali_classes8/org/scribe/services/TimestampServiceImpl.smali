.class public Lorg/scribe/services/TimestampServiceImpl;
.super Ljava/lang/Object;
.source "TimestampServiceImpl.java"

# interfaces
.implements Lorg/scribe/services/TimestampService;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/scribe/services/TimestampServiceImpl$Timer;
    }
.end annotation


# instance fields
.field private timer:Lorg/scribe/services/TimestampServiceImpl$Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lorg/scribe/services/TimestampServiceImpl$Timer;

    .line 5
    .line 6
    invoke-direct {v0}, Lorg/scribe/services/TimestampServiceImpl$Timer;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lorg/scribe/services/TimestampServiceImpl;->timer:Lorg/scribe/services/TimestampServiceImpl$Timer;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getTs()Ljava/lang/Long;
    .locals 4

    .line 1
    iget-object v0, p0, Lorg/scribe/services/TimestampServiceImpl;->timer:Lorg/scribe/services/TimestampServiceImpl$Timer;

    .line 2
    .line 3
    invoke-virtual {v0}, Lorg/scribe/services/TimestampServiceImpl$Timer;->getMilis()Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    const-wide/16 v2, 0x3e8

    .line 12
    .line 13
    div-long/2addr v0, v2

    .line 14
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getNonce()Ljava/lang/String;
    .locals 4

    .line 1
    invoke-direct {p0}, Lorg/scribe/services/TimestampServiceImpl;->getTs()Ljava/lang/Long;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    iget-object v2, p0, Lorg/scribe/services/TimestampServiceImpl;->timer:Lorg/scribe/services/TimestampServiceImpl$Timer;

    .line 10
    .line 11
    invoke-virtual {v2}, Lorg/scribe/services/TimestampServiceImpl$Timer;->getRandomInteger()Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    int-to-long v2, v2

    .line 20
    add-long/2addr v0, v2

    .line 21
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTimestampInSeconds()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/scribe/services/TimestampServiceImpl;->getTs()Ljava/lang/Long;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method setTimer(Lorg/scribe/services/TimestampServiceImpl$Timer;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lorg/scribe/services/TimestampServiceImpl;->timer:Lorg/scribe/services/TimestampServiceImpl$Timer;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

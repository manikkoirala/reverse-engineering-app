.class public abstract Lorg/scribe/services/Base64Encoder;
.super Ljava/lang/Object;
.source "Base64Encoder.java"


# static fields
.field private static instance:Lorg/scribe/services/Base64Encoder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static createEncoderInstance()Lorg/scribe/services/Base64Encoder;
    .locals 1

    .line 1
    invoke-static {}, Lorg/scribe/services/CommonsEncoder;->isPresent()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lorg/scribe/services/CommonsEncoder;

    .line 8
    .line 9
    invoke-direct {v0}, Lorg/scribe/services/CommonsEncoder;-><init>()V

    .line 10
    .line 11
    .line 12
    return-object v0

    .line 13
    :cond_0
    new-instance v0, Lorg/scribe/services/DatatypeConverterEncoder;

    .line 14
    .line 15
    invoke-direct {v0}, Lorg/scribe/services/DatatypeConverterEncoder;-><init>()V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static declared-synchronized getInstance()Lorg/scribe/services/Base64Encoder;
    .locals 2

    .line 1
    const-class v0, Lorg/scribe/services/Base64Encoder;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lorg/scribe/services/Base64Encoder;->instance:Lorg/scribe/services/Base64Encoder;

    .line 5
    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lorg/scribe/services/Base64Encoder;->createEncoderInstance()Lorg/scribe/services/Base64Encoder;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    sput-object v1, Lorg/scribe/services/Base64Encoder;->instance:Lorg/scribe/services/Base64Encoder;

    .line 13
    .line 14
    :cond_0
    sget-object v1, Lorg/scribe/services/Base64Encoder;->instance:Lorg/scribe/services/Base64Encoder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    .line 16
    monitor-exit v0

    .line 17
    return-object v1

    .line 18
    :catchall_0
    move-exception v1

    .line 19
    monitor-exit v0

    .line 20
    throw v1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static type()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lorg/scribe/services/Base64Encoder;->getInstance()Lorg/scribe/services/Base64Encoder;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lorg/scribe/services/Base64Encoder;->getType()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public abstract encode([B)Ljava/lang/String;
.end method

.method public abstract getType()Ljava/lang/String;
.end method

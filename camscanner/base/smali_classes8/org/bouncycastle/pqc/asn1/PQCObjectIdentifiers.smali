.class public interface abstract Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;
.super Ljava/lang/Object;


# static fields
.field public static final O8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OO0o〇〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OO0o〇〇〇〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final Oo08:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final Oooo8o0〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final oO80:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇080:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇80〇808〇O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇8o8o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O8o08O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇o00〇〇Oo:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇808〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇888:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇8O0〇8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 2
    .line 3
    const-string v1, "1.3.6.1.4.1.8301.3.1.3.5.3.2"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇080:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 9
    .line 10
    const-string v1, "1"

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    sput-object v2, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇o00〇〇Oo:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 17
    .line 18
    const-string v2, "2"

    .line 19
    .line 20
    invoke-virtual {v0, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    sput-object v3, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 25
    .line 26
    const-string v3, "3"

    .line 27
    .line 28
    invoke-virtual {v0, v3}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 29
    .line 30
    .line 31
    move-result-object v4

    .line 32
    sput-object v4, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->O8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 33
    .line 34
    const-string v4, "4"

    .line 35
    .line 36
    invoke-virtual {v0, v4}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 37
    .line 38
    .line 39
    move-result-object v5

    .line 40
    sput-object v5, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->Oo08:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 41
    .line 42
    const-string v5, "5"

    .line 43
    .line 44
    invoke-virtual {v0, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->o〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 49
    .line 50
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 51
    .line 52
    const-string v6, "1.3.6.1.4.1.8301.3.1.3.3"

    .line 53
    .line 54
    invoke-direct {v0, v6}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇〇888:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    sput-object v1, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->oO80:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 64
    .line 65
    invoke-virtual {v0, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    sput-object v1, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇80〇808〇O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 70
    .line 71
    invoke-virtual {v0, v3}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    sput-object v1, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->OO0o〇〇〇〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 76
    .line 77
    invoke-virtual {v0, v4}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    sput-object v1, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇8o8o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 82
    .line 83
    invoke-virtual {v0, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇O8o08O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 88
    .line 89
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 90
    .line 91
    const-string v1, "1.3.6.1.4.1.8301.3.1.3.4.1"

    .line 92
    .line 93
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->OO0o〇〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 97
    .line 98
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 99
    .line 100
    const-string v1, "1.3.6.1.4.1.8301.3.1.3.4.2"

    .line 101
    .line 102
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->Oooo8o0〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 106
    .line 107
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 108
    .line 109
    const-string v1, "1.3.6.1.4.1.8301.3.1.3.4.2.1"

    .line 110
    .line 111
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇〇808〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 115
    .line 116
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 117
    .line 118
    const-string v1, "1.3.6.1.4.1.8301.3.1.3.4.2.2"

    .line 119
    .line 120
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇O〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 124
    .line 125
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 126
    .line 127
    const-string v1, "1.3.6.1.4.1.8301.3.1.3.4.2.3"

    .line 128
    .line 129
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇O00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 133
    .line 134
    sget-object v0, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 135
    .line 136
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇〇8O0〇8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 137
    .line 138
    sget-object v0, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 139
    .line 140
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 141
    .line 142
    sget-object v0, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 143
    .line 144
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 145
    .line 146
    sget-object v0, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 147
    .line 148
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 149
    .line 150
    sget-object v0, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇oo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 151
    .line 152
    sput-object v0, Lorg/bouncycastle/pqc/asn1/PQCObjectIdentifiers;->〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 153
    .line 154
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

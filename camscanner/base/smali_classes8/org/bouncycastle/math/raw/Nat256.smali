.class public abstract Lorg/bouncycastle/math/raw/Nat256;
.super Ljava/lang/Object;


# direct methods
.method public static O8([I[I)I
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p0, v0

    .line 3
    .line 4
    int-to-long v1, v1

    .line 5
    const-wide v3, 0xffffffffL

    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    and-long/2addr v1, v3

    .line 11
    aget v5, p1, v0

    .line 12
    .line 13
    int-to-long v5, v5

    .line 14
    and-long/2addr v5, v3

    .line 15
    add-long/2addr v1, v5

    .line 16
    const-wide/16 v5, 0x0

    .line 17
    .line 18
    add-long/2addr v1, v5

    .line 19
    long-to-int v5, v1

    .line 20
    aput v5, p1, v0

    .line 21
    .line 22
    const/16 v0, 0x20

    .line 23
    .line 24
    ushr-long/2addr v1, v0

    .line 25
    const/4 v5, 0x1

    .line 26
    aget v6, p0, v5

    .line 27
    .line 28
    int-to-long v6, v6

    .line 29
    and-long/2addr v6, v3

    .line 30
    aget v8, p1, v5

    .line 31
    .line 32
    int-to-long v8, v8

    .line 33
    and-long/2addr v8, v3

    .line 34
    add-long/2addr v6, v8

    .line 35
    add-long/2addr v1, v6

    .line 36
    long-to-int v6, v1

    .line 37
    aput v6, p1, v5

    .line 38
    .line 39
    ushr-long/2addr v1, v0

    .line 40
    const/4 v5, 0x2

    .line 41
    aget v6, p0, v5

    .line 42
    .line 43
    int-to-long v6, v6

    .line 44
    and-long/2addr v6, v3

    .line 45
    aget v8, p1, v5

    .line 46
    .line 47
    int-to-long v8, v8

    .line 48
    and-long/2addr v8, v3

    .line 49
    add-long/2addr v6, v8

    .line 50
    add-long/2addr v1, v6

    .line 51
    long-to-int v6, v1

    .line 52
    aput v6, p1, v5

    .line 53
    .line 54
    ushr-long/2addr v1, v0

    .line 55
    const/4 v5, 0x3

    .line 56
    aget v6, p0, v5

    .line 57
    .line 58
    int-to-long v6, v6

    .line 59
    and-long/2addr v6, v3

    .line 60
    aget v8, p1, v5

    .line 61
    .line 62
    int-to-long v8, v8

    .line 63
    and-long/2addr v8, v3

    .line 64
    add-long/2addr v6, v8

    .line 65
    add-long/2addr v1, v6

    .line 66
    long-to-int v6, v1

    .line 67
    aput v6, p1, v5

    .line 68
    .line 69
    ushr-long/2addr v1, v0

    .line 70
    const/4 v5, 0x4

    .line 71
    aget v6, p0, v5

    .line 72
    .line 73
    int-to-long v6, v6

    .line 74
    and-long/2addr v6, v3

    .line 75
    aget v8, p1, v5

    .line 76
    .line 77
    int-to-long v8, v8

    .line 78
    and-long/2addr v8, v3

    .line 79
    add-long/2addr v6, v8

    .line 80
    add-long/2addr v1, v6

    .line 81
    long-to-int v6, v1

    .line 82
    aput v6, p1, v5

    .line 83
    .line 84
    ushr-long/2addr v1, v0

    .line 85
    const/4 v5, 0x5

    .line 86
    aget v6, p0, v5

    .line 87
    .line 88
    int-to-long v6, v6

    .line 89
    and-long/2addr v6, v3

    .line 90
    aget v8, p1, v5

    .line 91
    .line 92
    int-to-long v8, v8

    .line 93
    and-long/2addr v8, v3

    .line 94
    add-long/2addr v6, v8

    .line 95
    add-long/2addr v1, v6

    .line 96
    long-to-int v6, v1

    .line 97
    aput v6, p1, v5

    .line 98
    .line 99
    ushr-long/2addr v1, v0

    .line 100
    const/4 v5, 0x6

    .line 101
    aget v6, p0, v5

    .line 102
    .line 103
    int-to-long v6, v6

    .line 104
    and-long/2addr v6, v3

    .line 105
    aget v8, p1, v5

    .line 106
    .line 107
    int-to-long v8, v8

    .line 108
    and-long/2addr v8, v3

    .line 109
    add-long/2addr v6, v8

    .line 110
    add-long/2addr v1, v6

    .line 111
    long-to-int v6, v1

    .line 112
    aput v6, p1, v5

    .line 113
    .line 114
    ushr-long/2addr v1, v0

    .line 115
    const/4 v5, 0x7

    .line 116
    aget p0, p0, v5

    .line 117
    .line 118
    int-to-long v6, p0

    .line 119
    and-long/2addr v6, v3

    .line 120
    aget p0, p1, v5

    .line 121
    .line 122
    int-to-long v8, p0

    .line 123
    and-long/2addr v3, v8

    .line 124
    add-long/2addr v6, v3

    .line 125
    add-long/2addr v1, v6

    .line 126
    long-to-int p0, v1

    .line 127
    aput p0, p1, v5

    .line 128
    .line 129
    ushr-long p0, v1, v0

    .line 130
    .line 131
    long-to-int p1, p0

    .line 132
    return p1
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public static O8ooOoo〇(I[I[I)I
    .locals 11

    .line 1
    int-to-long v0, p0

    .line 2
    const-wide v2, 0xffffffffL

    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    and-long/2addr v0, v2

    .line 8
    const/4 p0, 0x0

    .line 9
    aget v4, p2, p0

    .line 10
    .line 11
    int-to-long v4, v4

    .line 12
    and-long/2addr v4, v2

    .line 13
    mul-long v4, v4, v0

    .line 14
    .line 15
    aget v6, p1, p0

    .line 16
    .line 17
    int-to-long v6, v6

    .line 18
    and-long/2addr v6, v2

    .line 19
    add-long/2addr v4, v6

    .line 20
    const-wide/16 v6, 0x0

    .line 21
    .line 22
    add-long/2addr v4, v6

    .line 23
    long-to-int v6, v4

    .line 24
    aput v6, p2, p0

    .line 25
    .line 26
    const/16 p0, 0x20

    .line 27
    .line 28
    ushr-long/2addr v4, p0

    .line 29
    const/4 v6, 0x1

    .line 30
    aget v7, p2, v6

    .line 31
    .line 32
    int-to-long v7, v7

    .line 33
    and-long/2addr v7, v2

    .line 34
    mul-long v7, v7, v0

    .line 35
    .line 36
    aget v9, p1, v6

    .line 37
    .line 38
    int-to-long v9, v9

    .line 39
    and-long/2addr v9, v2

    .line 40
    add-long/2addr v7, v9

    .line 41
    add-long/2addr v4, v7

    .line 42
    long-to-int v7, v4

    .line 43
    aput v7, p2, v6

    .line 44
    .line 45
    ushr-long/2addr v4, p0

    .line 46
    const/4 v6, 0x2

    .line 47
    aget v7, p2, v6

    .line 48
    .line 49
    int-to-long v7, v7

    .line 50
    and-long/2addr v7, v2

    .line 51
    mul-long v7, v7, v0

    .line 52
    .line 53
    aget v9, p1, v6

    .line 54
    .line 55
    int-to-long v9, v9

    .line 56
    and-long/2addr v9, v2

    .line 57
    add-long/2addr v7, v9

    .line 58
    add-long/2addr v4, v7

    .line 59
    long-to-int v7, v4

    .line 60
    aput v7, p2, v6

    .line 61
    .line 62
    ushr-long/2addr v4, p0

    .line 63
    const/4 v6, 0x3

    .line 64
    aget v7, p2, v6

    .line 65
    .line 66
    int-to-long v7, v7

    .line 67
    and-long/2addr v7, v2

    .line 68
    mul-long v7, v7, v0

    .line 69
    .line 70
    aget v9, p1, v6

    .line 71
    .line 72
    int-to-long v9, v9

    .line 73
    and-long/2addr v9, v2

    .line 74
    add-long/2addr v7, v9

    .line 75
    add-long/2addr v4, v7

    .line 76
    long-to-int v7, v4

    .line 77
    aput v7, p2, v6

    .line 78
    .line 79
    ushr-long/2addr v4, p0

    .line 80
    const/4 v6, 0x4

    .line 81
    aget v7, p2, v6

    .line 82
    .line 83
    int-to-long v7, v7

    .line 84
    and-long/2addr v7, v2

    .line 85
    mul-long v7, v7, v0

    .line 86
    .line 87
    aget v9, p1, v6

    .line 88
    .line 89
    int-to-long v9, v9

    .line 90
    and-long/2addr v9, v2

    .line 91
    add-long/2addr v7, v9

    .line 92
    add-long/2addr v4, v7

    .line 93
    long-to-int v7, v4

    .line 94
    aput v7, p2, v6

    .line 95
    .line 96
    ushr-long/2addr v4, p0

    .line 97
    const/4 v6, 0x5

    .line 98
    aget v7, p2, v6

    .line 99
    .line 100
    int-to-long v7, v7

    .line 101
    and-long/2addr v7, v2

    .line 102
    mul-long v7, v7, v0

    .line 103
    .line 104
    aget v9, p1, v6

    .line 105
    .line 106
    int-to-long v9, v9

    .line 107
    and-long/2addr v9, v2

    .line 108
    add-long/2addr v7, v9

    .line 109
    add-long/2addr v4, v7

    .line 110
    long-to-int v7, v4

    .line 111
    aput v7, p2, v6

    .line 112
    .line 113
    ushr-long/2addr v4, p0

    .line 114
    const/4 v6, 0x6

    .line 115
    aget v7, p2, v6

    .line 116
    .line 117
    int-to-long v7, v7

    .line 118
    and-long/2addr v7, v2

    .line 119
    mul-long v7, v7, v0

    .line 120
    .line 121
    aget v9, p1, v6

    .line 122
    .line 123
    int-to-long v9, v9

    .line 124
    and-long/2addr v9, v2

    .line 125
    add-long/2addr v7, v9

    .line 126
    add-long/2addr v4, v7

    .line 127
    long-to-int v7, v4

    .line 128
    aput v7, p2, v6

    .line 129
    .line 130
    ushr-long/2addr v4, p0

    .line 131
    const/4 v6, 0x7

    .line 132
    aget v7, p2, v6

    .line 133
    .line 134
    int-to-long v7, v7

    .line 135
    and-long/2addr v7, v2

    .line 136
    mul-long v0, v0, v7

    .line 137
    .line 138
    aget p1, p1, v6

    .line 139
    .line 140
    int-to-long v7, p1

    .line 141
    and-long/2addr v2, v7

    .line 142
    add-long/2addr v0, v2

    .line 143
    add-long/2addr v4, v0

    .line 144
    long-to-int p1, v4

    .line 145
    aput p1, p2, v6

    .line 146
    .line 147
    ushr-long p0, v4, p0

    .line 148
    .line 149
    long-to-int p1, p0

    .line 150
    return p1
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static O8〇o([I)Ljava/math/BigInteger;
    .locals 4

    .line 1
    const/16 v0, 0x20

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    const/16 v2, 0x8

    .line 7
    .line 8
    if-ge v1, v2, :cond_1

    .line 9
    .line 10
    aget v2, p0, v1

    .line 11
    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    rsub-int/lit8 v3, v1, 0x7

    .line 15
    .line 16
    shl-int/lit8 v3, v3, 0x2

    .line 17
    .line 18
    invoke-static {v2, v0, v3}, Lorg/bouncycastle/util/Pack;->〇o〇(I[BI)V

    .line 19
    .line 20
    .line 21
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    new-instance p0, Ljava/math/BigInteger;

    .line 25
    .line 26
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0, v1, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 28
    .line 29
    .line 30
    return-object p0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static OO0o〇〇(Ljava/math/BigInteger;)[I
    .locals 4

    .line 1
    invoke-virtual {p0}, Ljava/math/BigInteger;->signum()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ltz v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p0}, Ljava/math/BigInteger;->bitLength()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/16 v1, 0x100

    .line 12
    .line 13
    if-gt v0, v1, :cond_1

    .line 14
    .line 15
    invoke-static {}, Lorg/bouncycastle/math/raw/Nat256;->o〇0()[I

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x0

    .line 20
    :goto_0
    invoke-virtual {p0}, Ljava/math/BigInteger;->signum()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    add-int/lit8 v2, v1, 0x1

    .line 27
    .line 28
    invoke-virtual {p0}, Ljava/math/BigInteger;->intValue()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    aput v3, v0, v1

    .line 33
    .line 34
    const/16 v1, 0x20

    .line 35
    .line 36
    invoke-virtual {p0, v1}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    move v1, v2

    .line 41
    goto :goto_0

    .line 42
    :cond_0
    return-object v0

    .line 43
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 44
    .line 45
    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    .line 46
    .line 47
    .line 48
    throw p0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static OO0o〇〇〇〇0([II[II[II)Z
    .locals 7

    .line 1
    invoke-static {p0, p1, p2, p3}, Lorg/bouncycastle/math/raw/Nat256;->〇O〇([II[II)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static/range {p0 .. p5}, Lorg/bouncycastle/math/raw/Nat256;->o〇〇0〇([II[II[II)I

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v1, p2

    .line 12
    move v2, p3

    .line 13
    move-object v3, p0

    .line 14
    move v4, p1

    .line 15
    move-object v5, p4

    .line 16
    move v6, p5

    .line 17
    invoke-static/range {v1 .. v6}, Lorg/bouncycastle/math/raw/Nat256;->o〇〇0〇([II[II[II)I

    .line 18
    .line 19
    .line 20
    :goto_0
    return v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method public static OOO〇O0([I[I[I)I
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p0, v0

    .line 3
    .line 4
    int-to-long v1, v1

    .line 5
    const-wide v3, 0xffffffffL

    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    and-long/2addr v1, v3

    .line 11
    aget v5, p1, v0

    .line 12
    .line 13
    int-to-long v5, v5

    .line 14
    and-long/2addr v5, v3

    .line 15
    sub-long/2addr v1, v5

    .line 16
    const-wide/16 v5, 0x0

    .line 17
    .line 18
    add-long/2addr v1, v5

    .line 19
    long-to-int v5, v1

    .line 20
    aput v5, p2, v0

    .line 21
    .line 22
    const/16 v0, 0x20

    .line 23
    .line 24
    shr-long/2addr v1, v0

    .line 25
    const/4 v5, 0x1

    .line 26
    aget v6, p0, v5

    .line 27
    .line 28
    int-to-long v6, v6

    .line 29
    and-long/2addr v6, v3

    .line 30
    aget v8, p1, v5

    .line 31
    .line 32
    int-to-long v8, v8

    .line 33
    and-long/2addr v8, v3

    .line 34
    sub-long/2addr v6, v8

    .line 35
    add-long/2addr v1, v6

    .line 36
    long-to-int v6, v1

    .line 37
    aput v6, p2, v5

    .line 38
    .line 39
    shr-long/2addr v1, v0

    .line 40
    const/4 v5, 0x2

    .line 41
    aget v6, p0, v5

    .line 42
    .line 43
    int-to-long v6, v6

    .line 44
    and-long/2addr v6, v3

    .line 45
    aget v8, p1, v5

    .line 46
    .line 47
    int-to-long v8, v8

    .line 48
    and-long/2addr v8, v3

    .line 49
    sub-long/2addr v6, v8

    .line 50
    add-long/2addr v1, v6

    .line 51
    long-to-int v6, v1

    .line 52
    aput v6, p2, v5

    .line 53
    .line 54
    shr-long/2addr v1, v0

    .line 55
    const/4 v5, 0x3

    .line 56
    aget v6, p0, v5

    .line 57
    .line 58
    int-to-long v6, v6

    .line 59
    and-long/2addr v6, v3

    .line 60
    aget v8, p1, v5

    .line 61
    .line 62
    int-to-long v8, v8

    .line 63
    and-long/2addr v8, v3

    .line 64
    sub-long/2addr v6, v8

    .line 65
    add-long/2addr v1, v6

    .line 66
    long-to-int v6, v1

    .line 67
    aput v6, p2, v5

    .line 68
    .line 69
    shr-long/2addr v1, v0

    .line 70
    const/4 v5, 0x4

    .line 71
    aget v6, p0, v5

    .line 72
    .line 73
    int-to-long v6, v6

    .line 74
    and-long/2addr v6, v3

    .line 75
    aget v8, p1, v5

    .line 76
    .line 77
    int-to-long v8, v8

    .line 78
    and-long/2addr v8, v3

    .line 79
    sub-long/2addr v6, v8

    .line 80
    add-long/2addr v1, v6

    .line 81
    long-to-int v6, v1

    .line 82
    aput v6, p2, v5

    .line 83
    .line 84
    shr-long/2addr v1, v0

    .line 85
    const/4 v5, 0x5

    .line 86
    aget v6, p0, v5

    .line 87
    .line 88
    int-to-long v6, v6

    .line 89
    and-long/2addr v6, v3

    .line 90
    aget v8, p1, v5

    .line 91
    .line 92
    int-to-long v8, v8

    .line 93
    and-long/2addr v8, v3

    .line 94
    sub-long/2addr v6, v8

    .line 95
    add-long/2addr v1, v6

    .line 96
    long-to-int v6, v1

    .line 97
    aput v6, p2, v5

    .line 98
    .line 99
    shr-long/2addr v1, v0

    .line 100
    const/4 v5, 0x6

    .line 101
    aget v6, p0, v5

    .line 102
    .line 103
    int-to-long v6, v6

    .line 104
    and-long/2addr v6, v3

    .line 105
    aget v8, p1, v5

    .line 106
    .line 107
    int-to-long v8, v8

    .line 108
    and-long/2addr v8, v3

    .line 109
    sub-long/2addr v6, v8

    .line 110
    add-long/2addr v1, v6

    .line 111
    long-to-int v6, v1

    .line 112
    aput v6, p2, v5

    .line 113
    .line 114
    shr-long/2addr v1, v0

    .line 115
    const/4 v5, 0x7

    .line 116
    aget p0, p0, v5

    .line 117
    .line 118
    int-to-long v6, p0

    .line 119
    and-long/2addr v6, v3

    .line 120
    aget p0, p1, v5

    .line 121
    .line 122
    int-to-long p0, p0

    .line 123
    and-long/2addr p0, v3

    .line 124
    sub-long/2addr v6, p0

    .line 125
    add-long/2addr v1, v6

    .line 126
    long-to-int p0, v1

    .line 127
    aput p0, p2, v5

    .line 128
    .line 129
    shr-long p0, v1, v0

    .line 130
    .line 131
    long-to-int p1, p0

    .line 132
    return p1
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static Oo08([II[II)I
    .locals 11

    .line 1
    add-int/lit8 v0, p1, 0x0

    .line 2
    .line 3
    aget v1, p0, v0

    .line 4
    .line 5
    int-to-long v1, v1

    .line 6
    const-wide v3, 0xffffffffL

    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    and-long/2addr v1, v3

    .line 12
    add-int/lit8 v5, p3, 0x0

    .line 13
    .line 14
    aget v6, p2, v5

    .line 15
    .line 16
    int-to-long v6, v6

    .line 17
    and-long/2addr v6, v3

    .line 18
    add-long/2addr v1, v6

    .line 19
    const-wide/16 v6, 0x0

    .line 20
    .line 21
    add-long/2addr v1, v6

    .line 22
    long-to-int v6, v1

    .line 23
    aput v6, p0, v0

    .line 24
    .line 25
    aput v6, p2, v5

    .line 26
    .line 27
    const/16 v0, 0x20

    .line 28
    .line 29
    ushr-long/2addr v1, v0

    .line 30
    add-int/lit8 v5, p1, 0x1

    .line 31
    .line 32
    aget v6, p0, v5

    .line 33
    .line 34
    int-to-long v6, v6

    .line 35
    and-long/2addr v6, v3

    .line 36
    add-int/lit8 v8, p3, 0x1

    .line 37
    .line 38
    aget v9, p2, v8

    .line 39
    .line 40
    int-to-long v9, v9

    .line 41
    and-long/2addr v9, v3

    .line 42
    add-long/2addr v6, v9

    .line 43
    add-long/2addr v1, v6

    .line 44
    long-to-int v6, v1

    .line 45
    aput v6, p0, v5

    .line 46
    .line 47
    aput v6, p2, v8

    .line 48
    .line 49
    ushr-long/2addr v1, v0

    .line 50
    add-int/lit8 v5, p1, 0x2

    .line 51
    .line 52
    aget v6, p0, v5

    .line 53
    .line 54
    int-to-long v6, v6

    .line 55
    and-long/2addr v6, v3

    .line 56
    add-int/lit8 v8, p3, 0x2

    .line 57
    .line 58
    aget v9, p2, v8

    .line 59
    .line 60
    int-to-long v9, v9

    .line 61
    and-long/2addr v9, v3

    .line 62
    add-long/2addr v6, v9

    .line 63
    add-long/2addr v1, v6

    .line 64
    long-to-int v6, v1

    .line 65
    aput v6, p0, v5

    .line 66
    .line 67
    aput v6, p2, v8

    .line 68
    .line 69
    ushr-long/2addr v1, v0

    .line 70
    add-int/lit8 v5, p1, 0x3

    .line 71
    .line 72
    aget v6, p0, v5

    .line 73
    .line 74
    int-to-long v6, v6

    .line 75
    and-long/2addr v6, v3

    .line 76
    add-int/lit8 v8, p3, 0x3

    .line 77
    .line 78
    aget v9, p2, v8

    .line 79
    .line 80
    int-to-long v9, v9

    .line 81
    and-long/2addr v9, v3

    .line 82
    add-long/2addr v6, v9

    .line 83
    add-long/2addr v1, v6

    .line 84
    long-to-int v6, v1

    .line 85
    aput v6, p0, v5

    .line 86
    .line 87
    aput v6, p2, v8

    .line 88
    .line 89
    ushr-long/2addr v1, v0

    .line 90
    add-int/lit8 v5, p1, 0x4

    .line 91
    .line 92
    aget v6, p0, v5

    .line 93
    .line 94
    int-to-long v6, v6

    .line 95
    and-long/2addr v6, v3

    .line 96
    add-int/lit8 v8, p3, 0x4

    .line 97
    .line 98
    aget v9, p2, v8

    .line 99
    .line 100
    int-to-long v9, v9

    .line 101
    and-long/2addr v9, v3

    .line 102
    add-long/2addr v6, v9

    .line 103
    add-long/2addr v1, v6

    .line 104
    long-to-int v6, v1

    .line 105
    aput v6, p0, v5

    .line 106
    .line 107
    aput v6, p2, v8

    .line 108
    .line 109
    ushr-long/2addr v1, v0

    .line 110
    add-int/lit8 v5, p1, 0x5

    .line 111
    .line 112
    aget v6, p0, v5

    .line 113
    .line 114
    int-to-long v6, v6

    .line 115
    and-long/2addr v6, v3

    .line 116
    add-int/lit8 v8, p3, 0x5

    .line 117
    .line 118
    aget v9, p2, v8

    .line 119
    .line 120
    int-to-long v9, v9

    .line 121
    and-long/2addr v9, v3

    .line 122
    add-long/2addr v6, v9

    .line 123
    add-long/2addr v1, v6

    .line 124
    long-to-int v6, v1

    .line 125
    aput v6, p0, v5

    .line 126
    .line 127
    aput v6, p2, v8

    .line 128
    .line 129
    ushr-long/2addr v1, v0

    .line 130
    add-int/lit8 v5, p1, 0x6

    .line 131
    .line 132
    aget v6, p0, v5

    .line 133
    .line 134
    int-to-long v6, v6

    .line 135
    and-long/2addr v6, v3

    .line 136
    add-int/lit8 v8, p3, 0x6

    .line 137
    .line 138
    aget v9, p2, v8

    .line 139
    .line 140
    int-to-long v9, v9

    .line 141
    and-long/2addr v9, v3

    .line 142
    add-long/2addr v6, v9

    .line 143
    add-long/2addr v1, v6

    .line 144
    long-to-int v6, v1

    .line 145
    aput v6, p0, v5

    .line 146
    .line 147
    aput v6, p2, v8

    .line 148
    .line 149
    ushr-long/2addr v1, v0

    .line 150
    add-int/lit8 p1, p1, 0x7

    .line 151
    .line 152
    aget v5, p0, p1

    .line 153
    .line 154
    int-to-long v5, v5

    .line 155
    and-long/2addr v5, v3

    .line 156
    add-int/lit8 p3, p3, 0x7

    .line 157
    .line 158
    aget v7, p2, p3

    .line 159
    .line 160
    int-to-long v7, v7

    .line 161
    and-long/2addr v3, v7

    .line 162
    add-long/2addr v5, v3

    .line 163
    add-long/2addr v1, v5

    .line 164
    long-to-int v3, v1

    .line 165
    aput v3, p0, p1

    .line 166
    .line 167
    aput v3, p2, p3

    .line 168
    .line 169
    ushr-long p0, v1, v0

    .line 170
    .line 171
    long-to-int p1, p0

    .line 172
    return p1
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static OoO8([I)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    const/16 v2, 0x8

    .line 4
    .line 5
    if-ge v1, v2, :cond_1

    .line 6
    .line 7
    aget v2, p0, v1

    .line 8
    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    return v0

    .line 12
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    const/4 p0, 0x1

    .line 16
    return p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static Oooo8o0〇(Ljava/math/BigInteger;)[J
    .locals 5

    .line 1
    invoke-virtual {p0}, Ljava/math/BigInteger;->signum()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ltz v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p0}, Ljava/math/BigInteger;->bitLength()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/16 v1, 0x100

    .line 12
    .line 13
    if-gt v0, v1, :cond_1

    .line 14
    .line 15
    invoke-static {}, Lorg/bouncycastle/math/raw/Nat256;->〇〇888()[J

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/4 v1, 0x0

    .line 20
    :goto_0
    invoke-virtual {p0}, Ljava/math/BigInteger;->signum()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    add-int/lit8 v2, v1, 0x1

    .line 27
    .line 28
    invoke-virtual {p0}, Ljava/math/BigInteger;->longValue()J

    .line 29
    .line 30
    .line 31
    move-result-wide v3

    .line 32
    aput-wide v3, v0, v1

    .line 33
    .line 34
    const/16 v1, 0x40

    .line 35
    .line 36
    invoke-virtual {p0, v1}, Ljava/math/BigInteger;->shiftRight(I)Ljava/math/BigInteger;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    move v1, v2

    .line 41
    goto :goto_0

    .line 42
    :cond_0
    return-object v0

    .line 43
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 44
    .line 45
    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    .line 46
    .line 47
    .line 48
    throw p0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static O〇8O8〇008([I[I[I)I
    .locals 34

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p1, v0

    .line 3
    .line 4
    int-to-long v1, v1

    .line 5
    const-wide v3, 0xffffffffL

    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    and-long/2addr v1, v3

    .line 11
    const/4 v5, 0x1

    .line 12
    aget v5, p1, v5

    .line 13
    .line 14
    int-to-long v5, v5

    .line 15
    and-long/2addr v5, v3

    .line 16
    const/4 v7, 0x2

    .line 17
    aget v7, p1, v7

    .line 18
    .line 19
    int-to-long v7, v7

    .line 20
    and-long/2addr v7, v3

    .line 21
    const/4 v9, 0x3

    .line 22
    aget v9, p1, v9

    .line 23
    .line 24
    int-to-long v9, v9

    .line 25
    and-long/2addr v9, v3

    .line 26
    const/4 v11, 0x4

    .line 27
    aget v11, p1, v11

    .line 28
    .line 29
    int-to-long v11, v11

    .line 30
    and-long/2addr v11, v3

    .line 31
    const/4 v13, 0x5

    .line 32
    aget v13, p1, v13

    .line 33
    .line 34
    int-to-long v13, v13

    .line 35
    and-long/2addr v13, v3

    .line 36
    const/4 v15, 0x6

    .line 37
    aget v15, p1, v15

    .line 38
    .line 39
    move-wide/from16 v17, v1

    .line 40
    .line 41
    int-to-long v0, v15

    .line 42
    and-long/2addr v0, v3

    .line 43
    const/4 v2, 0x7

    .line 44
    aget v2, p1, v2

    .line 45
    .line 46
    move-wide/from16 v19, v0

    .line 47
    .line 48
    int-to-long v0, v2

    .line 49
    and-long/2addr v0, v3

    .line 50
    const-wide/16 v21, 0x0

    .line 51
    .line 52
    move-wide/from16 v23, v21

    .line 53
    .line 54
    const/4 v2, 0x0

    .line 55
    :goto_0
    const/16 v15, 0x8

    .line 56
    .line 57
    if-ge v2, v15, :cond_0

    .line 58
    .line 59
    aget v15, p0, v2

    .line 60
    .line 61
    move-wide/from16 v25, v0

    .line 62
    .line 63
    int-to-long v0, v15

    .line 64
    and-long/2addr v0, v3

    .line 65
    mul-long v15, v0, v17

    .line 66
    .line 67
    add-int/lit8 v27, v2, 0x0

    .line 68
    .line 69
    move-wide/from16 v28, v13

    .line 70
    .line 71
    aget v13, p2, v27

    .line 72
    .line 73
    int-to-long v13, v13

    .line 74
    and-long/2addr v13, v3

    .line 75
    add-long/2addr v15, v13

    .line 76
    add-long v13, v15, v21

    .line 77
    .line 78
    long-to-int v15, v13

    .line 79
    aput v15, p2, v27

    .line 80
    .line 81
    const/16 v15, 0x20

    .line 82
    .line 83
    ushr-long/2addr v13, v15

    .line 84
    mul-long v30, v0, v5

    .line 85
    .line 86
    add-int/lit8 v16, v2, 0x1

    .line 87
    .line 88
    aget v15, p2, v16

    .line 89
    .line 90
    move-wide/from16 v32, v5

    .line 91
    .line 92
    int-to-long v5, v15

    .line 93
    and-long/2addr v5, v3

    .line 94
    add-long v30, v30, v5

    .line 95
    .line 96
    add-long v13, v13, v30

    .line 97
    .line 98
    long-to-int v5, v13

    .line 99
    aput v5, p2, v16

    .line 100
    .line 101
    const/16 v5, 0x20

    .line 102
    .line 103
    ushr-long/2addr v13, v5

    .line 104
    mul-long v30, v0, v7

    .line 105
    .line 106
    add-int/lit8 v6, v2, 0x2

    .line 107
    .line 108
    aget v15, p2, v6

    .line 109
    .line 110
    move/from16 v27, v6

    .line 111
    .line 112
    int-to-long v5, v15

    .line 113
    and-long/2addr v5, v3

    .line 114
    add-long v30, v30, v5

    .line 115
    .line 116
    add-long v13, v13, v30

    .line 117
    .line 118
    long-to-int v5, v13

    .line 119
    aput v5, p2, v27

    .line 120
    .line 121
    const/16 v5, 0x20

    .line 122
    .line 123
    ushr-long/2addr v13, v5

    .line 124
    mul-long v30, v0, v9

    .line 125
    .line 126
    add-int/lit8 v6, v2, 0x3

    .line 127
    .line 128
    aget v15, p2, v6

    .line 129
    .line 130
    move/from16 v27, v6

    .line 131
    .line 132
    int-to-long v5, v15

    .line 133
    and-long/2addr v5, v3

    .line 134
    add-long v30, v30, v5

    .line 135
    .line 136
    add-long v13, v13, v30

    .line 137
    .line 138
    long-to-int v5, v13

    .line 139
    aput v5, p2, v27

    .line 140
    .line 141
    const/16 v5, 0x20

    .line 142
    .line 143
    ushr-long/2addr v13, v5

    .line 144
    mul-long v30, v0, v11

    .line 145
    .line 146
    add-int/lit8 v6, v2, 0x4

    .line 147
    .line 148
    aget v15, p2, v6

    .line 149
    .line 150
    move/from16 v27, v6

    .line 151
    .line 152
    int-to-long v5, v15

    .line 153
    and-long/2addr v5, v3

    .line 154
    add-long v30, v30, v5

    .line 155
    .line 156
    add-long v13, v13, v30

    .line 157
    .line 158
    long-to-int v5, v13

    .line 159
    aput v5, p2, v27

    .line 160
    .line 161
    const/16 v5, 0x20

    .line 162
    .line 163
    ushr-long/2addr v13, v5

    .line 164
    mul-long v30, v0, v28

    .line 165
    .line 166
    add-int/lit8 v6, v2, 0x5

    .line 167
    .line 168
    aget v15, p2, v6

    .line 169
    .line 170
    move/from16 v27, v6

    .line 171
    .line 172
    int-to-long v5, v15

    .line 173
    and-long/2addr v5, v3

    .line 174
    add-long v30, v30, v5

    .line 175
    .line 176
    add-long v13, v13, v30

    .line 177
    .line 178
    long-to-int v5, v13

    .line 179
    aput v5, p2, v27

    .line 180
    .line 181
    const/16 v5, 0x20

    .line 182
    .line 183
    ushr-long/2addr v13, v5

    .line 184
    mul-long v30, v0, v19

    .line 185
    .line 186
    add-int/lit8 v6, v2, 0x6

    .line 187
    .line 188
    aget v15, p2, v6

    .line 189
    .line 190
    move/from16 v27, v6

    .line 191
    .line 192
    int-to-long v5, v15

    .line 193
    and-long/2addr v5, v3

    .line 194
    add-long v30, v30, v5

    .line 195
    .line 196
    add-long v13, v13, v30

    .line 197
    .line 198
    long-to-int v5, v13

    .line 199
    aput v5, p2, v27

    .line 200
    .line 201
    const/16 v5, 0x20

    .line 202
    .line 203
    ushr-long/2addr v13, v5

    .line 204
    mul-long v0, v0, v25

    .line 205
    .line 206
    add-int/lit8 v6, v2, 0x7

    .line 207
    .line 208
    aget v15, p2, v6

    .line 209
    .line 210
    move/from16 v27, v6

    .line 211
    .line 212
    int-to-long v5, v15

    .line 213
    and-long/2addr v5, v3

    .line 214
    add-long/2addr v0, v5

    .line 215
    add-long/2addr v13, v0

    .line 216
    long-to-int v0, v13

    .line 217
    aput v0, p2, v27

    .line 218
    .line 219
    const/16 v0, 0x20

    .line 220
    .line 221
    ushr-long v5, v13, v0

    .line 222
    .line 223
    add-int/lit8 v2, v2, 0x8

    .line 224
    .line 225
    aget v1, p2, v2

    .line 226
    .line 227
    int-to-long v13, v1

    .line 228
    and-long/2addr v13, v3

    .line 229
    move-wide/from16 v3, v23

    .line 230
    .line 231
    add-long v23, v3, v13

    .line 232
    .line 233
    add-long v5, v5, v23

    .line 234
    .line 235
    long-to-int v1, v5

    .line 236
    aput v1, p2, v2

    .line 237
    .line 238
    ushr-long v23, v5, v0

    .line 239
    .line 240
    move/from16 v2, v16

    .line 241
    .line 242
    move-wide/from16 v0, v25

    .line 243
    .line 244
    move-wide/from16 v13, v28

    .line 245
    .line 246
    move-wide/from16 v5, v32

    .line 247
    .line 248
    const-wide v3, 0xffffffffL

    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    goto/16 :goto_0

    .line 254
    .line 255
    :cond_0
    move-wide/from16 v3, v23

    .line 256
    .line 257
    long-to-int v0, v3

    .line 258
    return v0
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method public static o800o8O([J)Z
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    const/4 v2, 0x4

    .line 4
    if-ge v1, v2, :cond_1

    .line 5
    .line 6
    aget-wide v2, p0, v1

    .line 7
    .line 8
    const-wide/16 v4, 0x0

    .line 9
    .line 10
    cmp-long v6, v2, v4

    .line 11
    .line 12
    if-eqz v6, :cond_0

    .line 13
    .line 14
    return v0

    .line 15
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/4 p0, 0x1

    .line 19
    return p0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static oO80()[I
    .locals 1

    .line 1
    const/16 v0, 0x10

    .line 2
    .line 3
    new-array v0, v0, [I

    .line 4
    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static oo88o8O([I[I[I)V
    .locals 35

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p1, v0

    .line 3
    .line 4
    int-to-long v1, v1

    .line 5
    const-wide v3, 0xffffffffL

    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    and-long/2addr v1, v3

    .line 11
    const/4 v5, 0x1

    .line 12
    aget v6, p1, v5

    .line 13
    .line 14
    int-to-long v6, v6

    .line 15
    and-long/2addr v6, v3

    .line 16
    const/4 v8, 0x2

    .line 17
    aget v9, p1, v8

    .line 18
    .line 19
    int-to-long v9, v9

    .line 20
    and-long/2addr v9, v3

    .line 21
    const/4 v11, 0x3

    .line 22
    aget v12, p1, v11

    .line 23
    .line 24
    int-to-long v12, v12

    .line 25
    and-long/2addr v12, v3

    .line 26
    const/4 v14, 0x4

    .line 27
    aget v15, p1, v14

    .line 28
    .line 29
    int-to-long v14, v15

    .line 30
    and-long/2addr v14, v3

    .line 31
    const/16 v17, 0x5

    .line 32
    .line 33
    aget v11, p1, v17

    .line 34
    .line 35
    move-wide/from16 v20, v9

    .line 36
    .line 37
    int-to-long v8, v11

    .line 38
    and-long/2addr v8, v3

    .line 39
    const/4 v10, 0x6

    .line 40
    aget v11, p1, v10

    .line 41
    .line 42
    int-to-long v10, v11

    .line 43
    and-long/2addr v10, v3

    .line 44
    const/16 v23, 0x7

    .line 45
    .line 46
    aget v5, p1, v23

    .line 47
    .line 48
    move-wide/from16 v25, v10

    .line 49
    .line 50
    int-to-long v10, v5

    .line 51
    and-long/2addr v10, v3

    .line 52
    aget v5, p0, v0

    .line 53
    .line 54
    move-wide/from16 v27, v10

    .line 55
    .line 56
    int-to-long v10, v5

    .line 57
    and-long/2addr v10, v3

    .line 58
    mul-long v29, v10, v1

    .line 59
    .line 60
    const-wide/16 v31, 0x0

    .line 61
    .line 62
    add-long v3, v29, v31

    .line 63
    .line 64
    long-to-int v5, v3

    .line 65
    aput v5, p2, v0

    .line 66
    .line 67
    const/16 v0, 0x20

    .line 68
    .line 69
    ushr-long/2addr v3, v0

    .line 70
    mul-long v29, v10, v6

    .line 71
    .line 72
    add-long v3, v3, v29

    .line 73
    .line 74
    long-to-int v5, v3

    .line 75
    const/16 v24, 0x1

    .line 76
    .line 77
    aput v5, p2, v24

    .line 78
    .line 79
    ushr-long/2addr v3, v0

    .line 80
    mul-long v29, v10, v20

    .line 81
    .line 82
    add-long v3, v3, v29

    .line 83
    .line 84
    long-to-int v5, v3

    .line 85
    const/16 v19, 0x2

    .line 86
    .line 87
    aput v5, p2, v19

    .line 88
    .line 89
    ushr-long/2addr v3, v0

    .line 90
    mul-long v29, v10, v12

    .line 91
    .line 92
    add-long v3, v3, v29

    .line 93
    .line 94
    long-to-int v5, v3

    .line 95
    const/16 v18, 0x3

    .line 96
    .line 97
    aput v5, p2, v18

    .line 98
    .line 99
    ushr-long/2addr v3, v0

    .line 100
    mul-long v18, v10, v14

    .line 101
    .line 102
    add-long v3, v3, v18

    .line 103
    .line 104
    long-to-int v5, v3

    .line 105
    const/16 v16, 0x4

    .line 106
    .line 107
    aput v5, p2, v16

    .line 108
    .line 109
    ushr-long/2addr v3, v0

    .line 110
    mul-long v18, v10, v8

    .line 111
    .line 112
    add-long v3, v3, v18

    .line 113
    .line 114
    long-to-int v5, v3

    .line 115
    aput v5, p2, v17

    .line 116
    .line 117
    ushr-long/2addr v3, v0

    .line 118
    mul-long v16, v10, v25

    .line 119
    .line 120
    add-long v3, v3, v16

    .line 121
    .line 122
    long-to-int v5, v3

    .line 123
    const/16 v16, 0x6

    .line 124
    .line 125
    aput v5, p2, v16

    .line 126
    .line 127
    ushr-long/2addr v3, v0

    .line 128
    mul-long v10, v10, v27

    .line 129
    .line 130
    add-long/2addr v3, v10

    .line 131
    long-to-int v5, v3

    .line 132
    aput v5, p2, v23

    .line 133
    .line 134
    ushr-long/2addr v3, v0

    .line 135
    long-to-int v4, v3

    .line 136
    const/16 v3, 0x8

    .line 137
    .line 138
    aput v4, p2, v3

    .line 139
    .line 140
    const/4 v5, 0x1

    .line 141
    :goto_0
    if-ge v5, v3, :cond_0

    .line 142
    .line 143
    aget v4, p0, v5

    .line 144
    .line 145
    int-to-long v10, v4

    .line 146
    const-wide v16, 0xffffffffL

    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    and-long v10, v10, v16

    .line 152
    .line 153
    mul-long v18, v10, v1

    .line 154
    .line 155
    add-int/lit8 v4, v5, 0x0

    .line 156
    .line 157
    aget v3, p2, v4

    .line 158
    .line 159
    move-wide/from16 v22, v1

    .line 160
    .line 161
    int-to-long v0, v3

    .line 162
    and-long v0, v0, v16

    .line 163
    .line 164
    add-long v18, v18, v0

    .line 165
    .line 166
    add-long v0, v18, v31

    .line 167
    .line 168
    long-to-int v3, v0

    .line 169
    aput v3, p2, v4

    .line 170
    .line 171
    const/16 v2, 0x20

    .line 172
    .line 173
    ushr-long/2addr v0, v2

    .line 174
    mul-long v3, v10, v6

    .line 175
    .line 176
    add-int/lit8 v18, v5, 0x1

    .line 177
    .line 178
    aget v2, p2, v18

    .line 179
    .line 180
    move-wide/from16 v29, v6

    .line 181
    .line 182
    int-to-long v6, v2

    .line 183
    and-long v6, v6, v16

    .line 184
    .line 185
    add-long/2addr v3, v6

    .line 186
    add-long/2addr v0, v3

    .line 187
    long-to-int v2, v0

    .line 188
    aput v2, p2, v18

    .line 189
    .line 190
    const/16 v2, 0x20

    .line 191
    .line 192
    ushr-long/2addr v0, v2

    .line 193
    mul-long v3, v10, v20

    .line 194
    .line 195
    add-int/lit8 v6, v5, 0x2

    .line 196
    .line 197
    aget v7, p2, v6

    .line 198
    .line 199
    move-wide/from16 v33, v8

    .line 200
    .line 201
    int-to-long v7, v7

    .line 202
    and-long v7, v7, v16

    .line 203
    .line 204
    add-long/2addr v3, v7

    .line 205
    add-long/2addr v0, v3

    .line 206
    long-to-int v3, v0

    .line 207
    aput v3, p2, v6

    .line 208
    .line 209
    ushr-long/2addr v0, v2

    .line 210
    mul-long v3, v10, v12

    .line 211
    .line 212
    add-int/lit8 v6, v5, 0x3

    .line 213
    .line 214
    aget v7, p2, v6

    .line 215
    .line 216
    int-to-long v7, v7

    .line 217
    and-long v7, v7, v16

    .line 218
    .line 219
    add-long/2addr v3, v7

    .line 220
    add-long/2addr v0, v3

    .line 221
    long-to-int v3, v0

    .line 222
    aput v3, p2, v6

    .line 223
    .line 224
    ushr-long/2addr v0, v2

    .line 225
    mul-long v3, v10, v14

    .line 226
    .line 227
    add-int/lit8 v6, v5, 0x4

    .line 228
    .line 229
    aget v7, p2, v6

    .line 230
    .line 231
    int-to-long v7, v7

    .line 232
    and-long v7, v7, v16

    .line 233
    .line 234
    add-long/2addr v3, v7

    .line 235
    add-long/2addr v0, v3

    .line 236
    long-to-int v3, v0

    .line 237
    aput v3, p2, v6

    .line 238
    .line 239
    ushr-long/2addr v0, v2

    .line 240
    mul-long v8, v10, v33

    .line 241
    .line 242
    add-int/lit8 v3, v5, 0x5

    .line 243
    .line 244
    aget v4, p2, v3

    .line 245
    .line 246
    int-to-long v6, v4

    .line 247
    and-long v6, v6, v16

    .line 248
    .line 249
    add-long/2addr v8, v6

    .line 250
    add-long/2addr v0, v8

    .line 251
    long-to-int v4, v0

    .line 252
    aput v4, p2, v3

    .line 253
    .line 254
    ushr-long/2addr v0, v2

    .line 255
    mul-long v3, v10, v25

    .line 256
    .line 257
    add-int/lit8 v6, v5, 0x6

    .line 258
    .line 259
    aget v7, p2, v6

    .line 260
    .line 261
    int-to-long v7, v7

    .line 262
    and-long v7, v7, v16

    .line 263
    .line 264
    add-long/2addr v3, v7

    .line 265
    add-long/2addr v0, v3

    .line 266
    long-to-int v3, v0

    .line 267
    aput v3, p2, v6

    .line 268
    .line 269
    ushr-long/2addr v0, v2

    .line 270
    mul-long v10, v10, v27

    .line 271
    .line 272
    add-int/lit8 v3, v5, 0x7

    .line 273
    .line 274
    aget v4, p2, v3

    .line 275
    .line 276
    int-to-long v6, v4

    .line 277
    and-long v6, v6, v16

    .line 278
    .line 279
    add-long/2addr v10, v6

    .line 280
    add-long/2addr v0, v10

    .line 281
    long-to-int v4, v0

    .line 282
    aput v4, p2, v3

    .line 283
    .line 284
    ushr-long/2addr v0, v2

    .line 285
    add-int/lit8 v5, v5, 0x8

    .line 286
    .line 287
    long-to-int v1, v0

    .line 288
    aput v1, p2, v5

    .line 289
    .line 290
    move/from16 v5, v18

    .line 291
    .line 292
    move-wide/from16 v1, v22

    .line 293
    .line 294
    move-wide/from16 v6, v29

    .line 295
    .line 296
    move-wide/from16 v8, v33

    .line 297
    .line 298
    const/16 v0, 0x20

    .line 299
    .line 300
    const/16 v3, 0x8

    .line 301
    .line 302
    goto/16 :goto_0

    .line 303
    .line 304
    :cond_0
    return-void
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method public static oo〇([I[I)I
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p1, v0

    .line 3
    .line 4
    int-to-long v1, v1

    .line 5
    const-wide v3, 0xffffffffL

    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    and-long/2addr v1, v3

    .line 11
    aget v5, p0, v0

    .line 12
    .line 13
    int-to-long v5, v5

    .line 14
    and-long/2addr v5, v3

    .line 15
    sub-long/2addr v1, v5

    .line 16
    const-wide/16 v5, 0x0

    .line 17
    .line 18
    add-long/2addr v1, v5

    .line 19
    long-to-int v5, v1

    .line 20
    aput v5, p1, v0

    .line 21
    .line 22
    const/16 v0, 0x20

    .line 23
    .line 24
    shr-long/2addr v1, v0

    .line 25
    const/4 v5, 0x1

    .line 26
    aget v6, p1, v5

    .line 27
    .line 28
    int-to-long v6, v6

    .line 29
    and-long/2addr v6, v3

    .line 30
    aget v8, p0, v5

    .line 31
    .line 32
    int-to-long v8, v8

    .line 33
    and-long/2addr v8, v3

    .line 34
    sub-long/2addr v6, v8

    .line 35
    add-long/2addr v1, v6

    .line 36
    long-to-int v6, v1

    .line 37
    aput v6, p1, v5

    .line 38
    .line 39
    shr-long/2addr v1, v0

    .line 40
    const/4 v5, 0x2

    .line 41
    aget v6, p1, v5

    .line 42
    .line 43
    int-to-long v6, v6

    .line 44
    and-long/2addr v6, v3

    .line 45
    aget v8, p0, v5

    .line 46
    .line 47
    int-to-long v8, v8

    .line 48
    and-long/2addr v8, v3

    .line 49
    sub-long/2addr v6, v8

    .line 50
    add-long/2addr v1, v6

    .line 51
    long-to-int v6, v1

    .line 52
    aput v6, p1, v5

    .line 53
    .line 54
    shr-long/2addr v1, v0

    .line 55
    const/4 v5, 0x3

    .line 56
    aget v6, p1, v5

    .line 57
    .line 58
    int-to-long v6, v6

    .line 59
    and-long/2addr v6, v3

    .line 60
    aget v8, p0, v5

    .line 61
    .line 62
    int-to-long v8, v8

    .line 63
    and-long/2addr v8, v3

    .line 64
    sub-long/2addr v6, v8

    .line 65
    add-long/2addr v1, v6

    .line 66
    long-to-int v6, v1

    .line 67
    aput v6, p1, v5

    .line 68
    .line 69
    shr-long/2addr v1, v0

    .line 70
    const/4 v5, 0x4

    .line 71
    aget v6, p1, v5

    .line 72
    .line 73
    int-to-long v6, v6

    .line 74
    and-long/2addr v6, v3

    .line 75
    aget v8, p0, v5

    .line 76
    .line 77
    int-to-long v8, v8

    .line 78
    and-long/2addr v8, v3

    .line 79
    sub-long/2addr v6, v8

    .line 80
    add-long/2addr v1, v6

    .line 81
    long-to-int v6, v1

    .line 82
    aput v6, p1, v5

    .line 83
    .line 84
    shr-long/2addr v1, v0

    .line 85
    const/4 v5, 0x5

    .line 86
    aget v6, p1, v5

    .line 87
    .line 88
    int-to-long v6, v6

    .line 89
    and-long/2addr v6, v3

    .line 90
    aget v8, p0, v5

    .line 91
    .line 92
    int-to-long v8, v8

    .line 93
    and-long/2addr v8, v3

    .line 94
    sub-long/2addr v6, v8

    .line 95
    add-long/2addr v1, v6

    .line 96
    long-to-int v6, v1

    .line 97
    aput v6, p1, v5

    .line 98
    .line 99
    shr-long/2addr v1, v0

    .line 100
    const/4 v5, 0x6

    .line 101
    aget v6, p1, v5

    .line 102
    .line 103
    int-to-long v6, v6

    .line 104
    and-long/2addr v6, v3

    .line 105
    aget v8, p0, v5

    .line 106
    .line 107
    int-to-long v8, v8

    .line 108
    and-long/2addr v8, v3

    .line 109
    sub-long/2addr v6, v8

    .line 110
    add-long/2addr v1, v6

    .line 111
    long-to-int v6, v1

    .line 112
    aput v6, p1, v5

    .line 113
    .line 114
    shr-long/2addr v1, v0

    .line 115
    const/4 v5, 0x7

    .line 116
    aget v6, p1, v5

    .line 117
    .line 118
    int-to-long v6, v6

    .line 119
    and-long/2addr v6, v3

    .line 120
    aget p0, p0, v5

    .line 121
    .line 122
    int-to-long v8, p0

    .line 123
    and-long/2addr v3, v8

    .line 124
    sub-long/2addr v6, v3

    .line 125
    add-long/2addr v1, v6

    .line 126
    long-to-int p0, v1

    .line 127
    aput p0, p1, v5

    .line 128
    .line 129
    shr-long p0, v1, v0

    .line 130
    .line 131
    long-to-int p1, p0

    .line 132
    return p1
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public static o〇0()[I
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [I

    .line 4
    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static o〇O8〇〇o(IJ[II)I
    .locals 15

    .line 1
    move-object/from16 v0, p3

    .line 2
    .line 3
    move v1, p0

    .line 4
    move/from16 v2, p4

    .line 5
    .line 6
    int-to-long v3, v1

    .line 7
    const-wide v5, 0xffffffffL

    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    and-long/2addr v3, v5

    .line 13
    and-long v7, p1, v5

    .line 14
    .line 15
    mul-long v9, v3, v7

    .line 16
    .line 17
    add-int/lit8 v1, v2, 0x0

    .line 18
    .line 19
    aget v11, v0, v1

    .line 20
    .line 21
    int-to-long v11, v11

    .line 22
    and-long/2addr v11, v5

    .line 23
    add-long/2addr v9, v11

    .line 24
    const-wide/16 v11, 0x0

    .line 25
    .line 26
    add-long/2addr v9, v11

    .line 27
    long-to-int v13, v9

    .line 28
    aput v13, v0, v1

    .line 29
    .line 30
    const/16 v1, 0x20

    .line 31
    .line 32
    ushr-long/2addr v9, v1

    .line 33
    ushr-long v13, p1, v1

    .line 34
    .line 35
    mul-long v3, v3, v13

    .line 36
    .line 37
    add-long/2addr v3, v7

    .line 38
    add-int/lit8 v7, v2, 0x1

    .line 39
    .line 40
    aget v8, v0, v7

    .line 41
    .line 42
    int-to-long v11, v8

    .line 43
    and-long/2addr v11, v5

    .line 44
    add-long/2addr v3, v11

    .line 45
    add-long/2addr v9, v3

    .line 46
    long-to-int v3, v9

    .line 47
    aput v3, v0, v7

    .line 48
    .line 49
    ushr-long v3, v9, v1

    .line 50
    .line 51
    add-int/lit8 v7, v2, 0x2

    .line 52
    .line 53
    aget v8, v0, v7

    .line 54
    .line 55
    int-to-long v8, v8

    .line 56
    and-long/2addr v8, v5

    .line 57
    add-long/2addr v13, v8

    .line 58
    add-long/2addr v3, v13

    .line 59
    long-to-int v8, v3

    .line 60
    aput v8, v0, v7

    .line 61
    .line 62
    ushr-long/2addr v3, v1

    .line 63
    add-int/lit8 v7, v2, 0x3

    .line 64
    .line 65
    aget v8, v0, v7

    .line 66
    .line 67
    int-to-long v8, v8

    .line 68
    and-long/2addr v5, v8

    .line 69
    add-long/2addr v3, v5

    .line 70
    long-to-int v5, v3

    .line 71
    aput v5, v0, v7

    .line 72
    .line 73
    ushr-long/2addr v3, v1

    .line 74
    const-wide/16 v5, 0x0

    .line 75
    .line 76
    cmp-long v1, v3, v5

    .line 77
    .line 78
    if-nez v1, :cond_0

    .line 79
    .line 80
    const/4 v0, 0x0

    .line 81
    goto :goto_0

    .line 82
    :cond_0
    const/16 v1, 0x8

    .line 83
    .line 84
    const/4 v3, 0x4

    .line 85
    invoke-static {v1, v0, v2, v3}, Lorg/bouncycastle/math/raw/Nat;->OoO8(I[III)I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    :goto_0
    return v0
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static o〇〇0〇([II[II[II)I
    .locals 9

    .line 1
    add-int/lit8 v0, p1, 0x0

    .line 2
    .line 3
    aget v0, p0, v0

    .line 4
    .line 5
    int-to-long v0, v0

    .line 6
    const-wide v2, 0xffffffffL

    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    and-long/2addr v0, v2

    .line 12
    add-int/lit8 v4, p3, 0x0

    .line 13
    .line 14
    aget v4, p2, v4

    .line 15
    .line 16
    int-to-long v4, v4

    .line 17
    and-long/2addr v4, v2

    .line 18
    sub-long/2addr v0, v4

    .line 19
    const-wide/16 v4, 0x0

    .line 20
    .line 21
    add-long/2addr v0, v4

    .line 22
    add-int/lit8 v4, p5, 0x0

    .line 23
    .line 24
    long-to-int v5, v0

    .line 25
    aput v5, p4, v4

    .line 26
    .line 27
    const/16 v4, 0x20

    .line 28
    .line 29
    shr-long/2addr v0, v4

    .line 30
    add-int/lit8 v5, p1, 0x1

    .line 31
    .line 32
    aget v5, p0, v5

    .line 33
    .line 34
    int-to-long v5, v5

    .line 35
    and-long/2addr v5, v2

    .line 36
    add-int/lit8 v7, p3, 0x1

    .line 37
    .line 38
    aget v7, p2, v7

    .line 39
    .line 40
    int-to-long v7, v7

    .line 41
    and-long/2addr v7, v2

    .line 42
    sub-long/2addr v5, v7

    .line 43
    add-long/2addr v0, v5

    .line 44
    add-int/lit8 v5, p5, 0x1

    .line 45
    .line 46
    long-to-int v6, v0

    .line 47
    aput v6, p4, v5

    .line 48
    .line 49
    shr-long/2addr v0, v4

    .line 50
    add-int/lit8 v5, p1, 0x2

    .line 51
    .line 52
    aget v5, p0, v5

    .line 53
    .line 54
    int-to-long v5, v5

    .line 55
    and-long/2addr v5, v2

    .line 56
    add-int/lit8 v7, p3, 0x2

    .line 57
    .line 58
    aget v7, p2, v7

    .line 59
    .line 60
    int-to-long v7, v7

    .line 61
    and-long/2addr v7, v2

    .line 62
    sub-long/2addr v5, v7

    .line 63
    add-long/2addr v0, v5

    .line 64
    add-int/lit8 v5, p5, 0x2

    .line 65
    .line 66
    long-to-int v6, v0

    .line 67
    aput v6, p4, v5

    .line 68
    .line 69
    shr-long/2addr v0, v4

    .line 70
    add-int/lit8 v5, p1, 0x3

    .line 71
    .line 72
    aget v5, p0, v5

    .line 73
    .line 74
    int-to-long v5, v5

    .line 75
    and-long/2addr v5, v2

    .line 76
    add-int/lit8 v7, p3, 0x3

    .line 77
    .line 78
    aget v7, p2, v7

    .line 79
    .line 80
    int-to-long v7, v7

    .line 81
    and-long/2addr v7, v2

    .line 82
    sub-long/2addr v5, v7

    .line 83
    add-long/2addr v0, v5

    .line 84
    add-int/lit8 v5, p5, 0x3

    .line 85
    .line 86
    long-to-int v6, v0

    .line 87
    aput v6, p4, v5

    .line 88
    .line 89
    shr-long/2addr v0, v4

    .line 90
    add-int/lit8 v5, p1, 0x4

    .line 91
    .line 92
    aget v5, p0, v5

    .line 93
    .line 94
    int-to-long v5, v5

    .line 95
    and-long/2addr v5, v2

    .line 96
    add-int/lit8 v7, p3, 0x4

    .line 97
    .line 98
    aget v7, p2, v7

    .line 99
    .line 100
    int-to-long v7, v7

    .line 101
    and-long/2addr v7, v2

    .line 102
    sub-long/2addr v5, v7

    .line 103
    add-long/2addr v0, v5

    .line 104
    add-int/lit8 v5, p5, 0x4

    .line 105
    .line 106
    long-to-int v6, v0

    .line 107
    aput v6, p4, v5

    .line 108
    .line 109
    shr-long/2addr v0, v4

    .line 110
    add-int/lit8 v5, p1, 0x5

    .line 111
    .line 112
    aget v5, p0, v5

    .line 113
    .line 114
    int-to-long v5, v5

    .line 115
    and-long/2addr v5, v2

    .line 116
    add-int/lit8 v7, p3, 0x5

    .line 117
    .line 118
    aget v7, p2, v7

    .line 119
    .line 120
    int-to-long v7, v7

    .line 121
    and-long/2addr v7, v2

    .line 122
    sub-long/2addr v5, v7

    .line 123
    add-long/2addr v0, v5

    .line 124
    add-int/lit8 v5, p5, 0x5

    .line 125
    .line 126
    long-to-int v6, v0

    .line 127
    aput v6, p4, v5

    .line 128
    .line 129
    shr-long/2addr v0, v4

    .line 130
    add-int/lit8 v5, p1, 0x6

    .line 131
    .line 132
    aget v5, p0, v5

    .line 133
    .line 134
    int-to-long v5, v5

    .line 135
    and-long/2addr v5, v2

    .line 136
    add-int/lit8 v7, p3, 0x6

    .line 137
    .line 138
    aget v7, p2, v7

    .line 139
    .line 140
    int-to-long v7, v7

    .line 141
    and-long/2addr v7, v2

    .line 142
    sub-long/2addr v5, v7

    .line 143
    add-long/2addr v0, v5

    .line 144
    add-int/lit8 v5, p5, 0x6

    .line 145
    .line 146
    long-to-int v6, v0

    .line 147
    aput v6, p4, v5

    .line 148
    .line 149
    shr-long/2addr v0, v4

    .line 150
    add-int/lit8 p1, p1, 0x7

    .line 151
    .line 152
    aget p0, p0, p1

    .line 153
    .line 154
    int-to-long p0, p0

    .line 155
    and-long/2addr p0, v2

    .line 156
    add-int/lit8 p3, p3, 0x7

    .line 157
    .line 158
    aget p2, p2, p3

    .line 159
    .line 160
    int-to-long p2, p2

    .line 161
    and-long/2addr p2, v2

    .line 162
    sub-long/2addr p0, p2

    .line 163
    add-long/2addr v0, p0

    .line 164
    add-int/lit8 p5, p5, 0x7

    .line 165
    .line 166
    long-to-int p0, v0

    .line 167
    aput p0, p4, p5

    .line 168
    .line 169
    shr-long p0, v0, v4

    .line 170
    .line 171
    long-to-int p1, p0

    .line 172
    return p1
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method public static 〇00(II[II)I
    .locals 10

    .line 1
    int-to-long v0, p0

    .line 2
    const-wide v2, 0xffffffffL

    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    and-long/2addr v0, v2

    .line 8
    int-to-long p0, p1

    .line 9
    and-long/2addr p0, v2

    .line 10
    mul-long v0, v0, p0

    .line 11
    .line 12
    add-int/lit8 v4, p3, 0x0

    .line 13
    .line 14
    aget v5, p2, v4

    .line 15
    .line 16
    int-to-long v5, v5

    .line 17
    and-long/2addr v5, v2

    .line 18
    add-long/2addr v0, v5

    .line 19
    const-wide/16 v5, 0x0

    .line 20
    .line 21
    add-long/2addr v0, v5

    .line 22
    long-to-int v7, v0

    .line 23
    aput v7, p2, v4

    .line 24
    .line 25
    const/16 v4, 0x20

    .line 26
    .line 27
    ushr-long/2addr v0, v4

    .line 28
    add-int/lit8 v7, p3, 0x1

    .line 29
    .line 30
    aget v8, p2, v7

    .line 31
    .line 32
    int-to-long v8, v8

    .line 33
    and-long/2addr v8, v2

    .line 34
    add-long/2addr p0, v8

    .line 35
    add-long/2addr v0, p0

    .line 36
    long-to-int p0, v0

    .line 37
    aput p0, p2, v7

    .line 38
    .line 39
    ushr-long p0, v0, v4

    .line 40
    .line 41
    add-int/lit8 v0, p3, 0x2

    .line 42
    .line 43
    aget v1, p2, v0

    .line 44
    .line 45
    int-to-long v7, v1

    .line 46
    and-long v1, v7, v2

    .line 47
    .line 48
    add-long/2addr p0, v1

    .line 49
    long-to-int v1, p0

    .line 50
    aput v1, p2, v0

    .line 51
    .line 52
    ushr-long/2addr p0, v4

    .line 53
    cmp-long v0, p0, v5

    .line 54
    .line 55
    if-nez v0, :cond_0

    .line 56
    .line 57
    const/4 p0, 0x0

    .line 58
    goto :goto_0

    .line 59
    :cond_0
    const/16 p0, 0x8

    .line 60
    .line 61
    const/4 p1, 0x3

    .line 62
    invoke-static {p0, p2, p3, p1}, Lorg/bouncycastle/math/raw/Nat;->OoO8(I[III)I

    .line 63
    .line 64
    .line 65
    move-result p0

    .line 66
    :goto_0
    return p0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static 〇0000OOO([I[I)V
    .locals 46

    .line 1
    const/4 v0, 0x0

    aget v1, p0, v0

    int-to-long v1, v1

    const-wide v3, 0xffffffffL

    and-long/2addr v1, v3

    const/16 v6, 0x10

    const/4 v7, 0x7

    const/4 v8, 0x0

    :goto_0
    add-int/lit8 v9, v7, -0x1

    aget v7, p0, v7

    int-to-long v10, v7

    and-long/2addr v10, v3

    mul-long v10, v10, v10

    add-int/lit8 v6, v6, -0x1

    shl-int/lit8 v7, v8, 0x1f

    const/16 v8, 0x21

    ushr-long v12, v10, v8

    long-to-int v13, v12

    or-int/2addr v7, v13

    aput v7, p1, v6

    add-int/lit8 v6, v6, -0x1

    const/4 v7, 0x1

    ushr-long v12, v10, v7

    long-to-int v13, v12

    aput v13, p1, v6

    long-to-int v11, v10

    if-gtz v9, :cond_0

    mul-long v9, v1, v1

    shl-int/lit8 v6, v11, 0x1f

    int-to-long v11, v6

    and-long/2addr v11, v3

    ushr-long v13, v9, v8

    or-long/2addr v11, v13

    long-to-int v6, v9

    aput v6, p1, v0

    const/16 v0, 0x20

    ushr-long v8, v9, v0

    long-to-int v6, v8

    and-int/2addr v6, v7

    aget v8, p0, v7

    int-to-long v8, v8

    and-long/2addr v8, v3

    const/4 v10, 0x2

    aget v13, p1, v10

    int-to-long v13, v13

    and-long/2addr v13, v3

    mul-long v15, v8, v1

    add-long/2addr v11, v15

    long-to-int v15, v11

    shl-int/lit8 v16, v15, 0x1

    or-int v6, v16, v6

    aput v6, p1, v7

    ushr-int/lit8 v6, v15, 0x1f

    ushr-long/2addr v11, v0

    add-long/2addr v13, v11

    aget v11, p0, v10

    int-to-long v11, v11

    and-long/2addr v11, v3

    const/4 v15, 0x3

    aget v7, p1, v15

    move/from16 v17, v6

    int-to-long v5, v7

    and-long/2addr v5, v3

    const/4 v7, 0x4

    aget v15, p1, v7

    move-wide/from16 v19, v8

    int-to-long v7, v15

    and-long/2addr v7, v3

    mul-long v21, v11, v1

    add-long v13, v13, v21

    long-to-int v15, v13

    shl-int/lit8 v21, v15, 0x1

    or-int v17, v21, v17

    aput v17, p1, v10

    ushr-int/lit8 v10, v15, 0x1f

    ushr-long/2addr v13, v0

    mul-long v21, v11, v19

    add-long v13, v13, v21

    add-long/2addr v5, v13

    ushr-long v13, v5, v0

    add-long/2addr v7, v13

    and-long/2addr v5, v3

    const/4 v13, 0x3

    aget v14, p0, v13

    int-to-long v13, v14

    and-long/2addr v13, v3

    const/4 v15, 0x5

    aget v9, p1, v15

    move-wide/from16 v21, v11

    int-to-long v11, v9

    and-long/2addr v11, v3

    ushr-long v23, v7, v0

    add-long v11, v11, v23

    and-long/2addr v7, v3

    const/16 v23, 0x6

    aget v9, p1, v23

    move-wide/from16 v24, v7

    int-to-long v7, v9

    and-long/2addr v7, v3

    ushr-long v26, v11, v0

    add-long v7, v7, v26

    and-long/2addr v11, v3

    mul-long v26, v13, v1

    add-long v5, v5, v26

    long-to-int v9, v5

    shl-int/lit8 v26, v9, 0x1

    or-int v10, v26, v10

    const/16 v18, 0x3

    aput v10, p1, v18

    ushr-int/lit8 v10, v9, 0x1f

    ushr-long/2addr v5, v0

    mul-long v26, v13, v19

    add-long v5, v5, v26

    add-long v5, v24, v5

    ushr-long v24, v5, v0

    mul-long v26, v13, v21

    add-long v24, v24, v26

    add-long v11, v11, v24

    and-long/2addr v5, v3

    ushr-long v24, v11, v0

    add-long v7, v7, v24

    and-long/2addr v11, v3

    const/4 v9, 0x4

    aget v15, p0, v9

    move/from16 v18, v10

    int-to-long v9, v15

    and-long/2addr v9, v3

    const/4 v15, 0x7

    aget v0, p1, v15

    move-wide/from16 v26, v13

    int-to-long v13, v0

    and-long/2addr v13, v3

    const/16 v0, 0x20

    ushr-long v28, v7, v0

    add-long v13, v13, v28

    and-long/2addr v7, v3

    const/16 v15, 0x8

    aget v0, p1, v15

    move-wide/from16 v28, v7

    int-to-long v7, v0

    and-long/2addr v7, v3

    const/16 v0, 0x20

    ushr-long v30, v13, v0

    add-long v7, v7, v30

    and-long/2addr v13, v3

    mul-long v30, v9, v1

    add-long v5, v5, v30

    long-to-int v15, v5

    shl-int/lit8 v25, v15, 0x1

    or-int v18, v25, v18

    const/16 v24, 0x4

    aput v18, p1, v24

    ushr-int/lit8 v15, v15, 0x1f

    ushr-long/2addr v5, v0

    mul-long v24, v9, v19

    add-long v5, v5, v24

    add-long/2addr v11, v5

    ushr-long v5, v11, v0

    mul-long v24, v9, v21

    add-long v5, v5, v24

    add-long v5, v28, v5

    and-long/2addr v11, v3

    ushr-long v24, v5, v0

    mul-long v28, v9, v26

    add-long v24, v24, v28

    add-long v13, v13, v24

    and-long/2addr v5, v3

    ushr-long v24, v13, v0

    add-long v7, v7, v24

    and-long/2addr v13, v3

    const/16 v17, 0x5

    aget v0, p0, v17

    move-wide/from16 v28, v9

    int-to-long v9, v0

    and-long/2addr v9, v3

    const/16 v0, 0x9

    move-wide/from16 v31, v13

    aget v13, p1, v0

    int-to-long v13, v13

    and-long/2addr v13, v3

    const/16 v18, 0x20

    ushr-long v24, v7, v18

    add-long v13, v13, v24

    and-long/2addr v7, v3

    const/16 v24, 0xa

    aget v0, p1, v24

    move-wide/from16 v33, v7

    int-to-long v7, v0

    and-long/2addr v7, v3

    ushr-long v35, v13, v18

    add-long v7, v7, v35

    and-long/2addr v13, v3

    mul-long v35, v9, v1

    add-long v11, v11, v35

    long-to-int v0, v11

    shl-int/lit8 v25, v0, 0x1

    or-int v15, v25, v15

    const/16 v17, 0x5

    aput v15, p1, v17

    ushr-int/lit8 v0, v0, 0x1f

    ushr-long v11, v11, v18

    mul-long v35, v9, v19

    add-long v11, v11, v35

    add-long/2addr v5, v11

    ushr-long v11, v5, v18

    mul-long v35, v9, v21

    add-long v11, v11, v35

    add-long v11, v31, v11

    and-long/2addr v5, v3

    ushr-long v31, v11, v18

    mul-long v35, v9, v26

    add-long v31, v31, v35

    add-long v31, v33, v31

    and-long/2addr v11, v3

    ushr-long v33, v31, v18

    mul-long v35, v9, v28

    add-long v33, v33, v35

    add-long v13, v13, v33

    and-long v31, v31, v3

    ushr-long v33, v13, v18

    add-long v7, v7, v33

    and-long/2addr v13, v3

    aget v15, p0, v23

    move-wide/from16 v33, v9

    int-to-long v9, v15

    and-long/2addr v9, v3

    const/16 v15, 0xb

    move-wide/from16 v35, v13

    aget v13, p1, v15

    int-to-long v13, v13

    and-long/2addr v13, v3

    ushr-long v37, v7, v18

    add-long v13, v13, v37

    and-long/2addr v7, v3

    const/16 v17, 0xc

    aget v15, p1, v17

    move-wide/from16 v38, v7

    int-to-long v7, v15

    and-long/2addr v7, v3

    ushr-long v40, v13, v18

    add-long v7, v7, v40

    and-long/2addr v13, v3

    mul-long v40, v9, v1

    add-long v5, v5, v40

    long-to-int v15, v5

    shl-int/lit8 v25, v15, 0x1

    or-int v0, v25, v0

    aput v0, p1, v23

    ushr-int/lit8 v0, v15, 0x1f

    ushr-long v5, v5, v18

    mul-long v40, v9, v19

    add-long v5, v5, v40

    add-long/2addr v11, v5

    ushr-long v5, v11, v18

    mul-long v40, v9, v21

    add-long v5, v5, v40

    add-long v31, v31, v5

    and-long v5, v11, v3

    ushr-long v11, v31, v18

    mul-long v40, v9, v26

    add-long v11, v11, v40

    add-long v11, v35, v11

    and-long v31, v31, v3

    ushr-long v35, v11, v18

    mul-long v40, v9, v28

    add-long v35, v35, v40

    add-long v35, v38, v35

    and-long/2addr v11, v3

    ushr-long v38, v35, v18

    mul-long v40, v9, v33

    add-long v38, v38, v40

    add-long v13, v13, v38

    and-long v35, v35, v3

    ushr-long v38, v13, v18

    add-long v7, v7, v38

    and-long/2addr v13, v3

    move-wide/from16 v38, v9

    const/4 v15, 0x7

    aget v9, p0, v15

    int-to-long v9, v9

    and-long/2addr v9, v3

    const/16 v15, 0xd

    move-wide/from16 v40, v13

    aget v13, p1, v15

    int-to-long v13, v13

    and-long/2addr v13, v3

    ushr-long v42, v7, v18

    add-long v13, v13, v42

    and-long/2addr v7, v3

    const/16 v23, 0xe

    aget v15, p1, v23

    move-wide/from16 v42, v7

    int-to-long v7, v15

    and-long/2addr v7, v3

    ushr-long v44, v13, v18

    add-long v7, v7, v44

    and-long/2addr v3, v13

    mul-long v1, v1, v9

    add-long/2addr v5, v1

    long-to-int v1, v5

    shl-int/lit8 v2, v1, 0x1

    or-int/2addr v0, v2

    const/4 v13, 0x7

    aput v0, p1, v13

    ushr-int/lit8 v0, v1, 0x1f

    ushr-long v1, v5, v18

    mul-long v5, v9, v19

    add-long/2addr v1, v5

    add-long v1, v31, v1

    ushr-long v5, v1, v18

    mul-long v13, v9, v21

    add-long/2addr v5, v13

    add-long/2addr v11, v5

    ushr-long v5, v11, v18

    mul-long v13, v9, v26

    add-long/2addr v5, v13

    add-long v5, v35, v5

    ushr-long v13, v5, v18

    mul-long v19, v9, v28

    add-long v13, v13, v19

    add-long v13, v40, v13

    ushr-long v19, v13, v18

    mul-long v21, v9, v33

    add-long v19, v19, v21

    move-wide/from16 v21, v13

    add-long v13, v42, v19

    ushr-long v19, v13, v18

    mul-long v9, v9, v38

    add-long v19, v19, v9

    add-long v3, v3, v19

    ushr-long v9, v3, v18

    add-long/2addr v7, v9

    long-to-int v2, v1

    shl-int/lit8 v1, v2, 0x1

    or-int/2addr v0, v1

    const/16 v1, 0x8

    aput v0, p1, v1

    ushr-int/lit8 v0, v2, 0x1f

    long-to-int v1, v11

    shl-int/lit8 v2, v1, 0x1

    or-int/2addr v0, v2

    const/16 v2, 0x9

    aput v0, p1, v2

    ushr-int/lit8 v0, v1, 0x1f

    long-to-int v1, v5

    shl-int/lit8 v2, v1, 0x1

    or-int/2addr v0, v2

    aput v0, p1, v24

    ushr-int/lit8 v0, v1, 0x1f

    move-wide/from16 v1, v21

    long-to-int v2, v1

    shl-int/lit8 v1, v2, 0x1

    or-int/2addr v0, v1

    const/16 v1, 0xb

    aput v0, p1, v1

    ushr-int/lit8 v0, v2, 0x1f

    long-to-int v1, v13

    shl-int/lit8 v2, v1, 0x1

    or-int/2addr v0, v2

    aput v0, p1, v17

    ushr-int/lit8 v0, v1, 0x1f

    long-to-int v1, v3

    shl-int/lit8 v2, v1, 0x1

    or-int/2addr v0, v2

    const/16 v2, 0xd

    aput v0, p1, v2

    ushr-int/lit8 v0, v1, 0x1f

    long-to-int v1, v7

    shl-int/lit8 v2, v1, 0x1

    or-int/2addr v0, v2

    aput v0, p1, v23

    ushr-int/lit8 v0, v1, 0x1f

    const/16 v1, 0xf

    aget v2, p1, v1

    const/16 v3, 0x20

    ushr-long v3, v7, v3

    long-to-int v4, v3

    add-int/2addr v2, v4

    const/4 v3, 0x1

    shl-int/2addr v2, v3

    or-int/2addr v0, v2

    aput v0, p1, v1

    return-void

    :cond_0
    move v7, v9

    move v8, v11

    goto/16 :goto_0
.end method

.method public static 〇00〇8([J)Ljava/math/BigInteger;
    .locals 7

    .line 1
    const/16 v0, 0x20

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    const/4 v2, 0x4

    .line 7
    if-ge v1, v2, :cond_1

    .line 8
    .line 9
    aget-wide v2, p0, v1

    .line 10
    .line 11
    const-wide/16 v4, 0x0

    .line 12
    .line 13
    cmp-long v6, v2, v4

    .line 14
    .line 15
    if-eqz v6, :cond_0

    .line 16
    .line 17
    rsub-int/lit8 v4, v1, 0x3

    .line 18
    .line 19
    shl-int/lit8 v4, v4, 0x3

    .line 20
    .line 21
    invoke-static {v2, v3, v0, v4}, Lorg/bouncycastle/util/Pack;->Oo08(J[BI)V

    .line 22
    .line 23
    .line 24
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    new-instance p0, Ljava/math/BigInteger;

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-direct {p0, v1, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 31
    .line 32
    .line 33
    return-object p0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static 〇080([I[I[I)I
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p0, v0

    .line 3
    .line 4
    int-to-long v1, v1

    .line 5
    const-wide v3, 0xffffffffL

    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    and-long/2addr v1, v3

    .line 11
    aget v5, p1, v0

    .line 12
    .line 13
    int-to-long v5, v5

    .line 14
    and-long/2addr v5, v3

    .line 15
    add-long/2addr v1, v5

    .line 16
    const-wide/16 v5, 0x0

    .line 17
    .line 18
    add-long/2addr v1, v5

    .line 19
    long-to-int v5, v1

    .line 20
    aput v5, p2, v0

    .line 21
    .line 22
    const/16 v0, 0x20

    .line 23
    .line 24
    ushr-long/2addr v1, v0

    .line 25
    const/4 v5, 0x1

    .line 26
    aget v6, p0, v5

    .line 27
    .line 28
    int-to-long v6, v6

    .line 29
    and-long/2addr v6, v3

    .line 30
    aget v8, p1, v5

    .line 31
    .line 32
    int-to-long v8, v8

    .line 33
    and-long/2addr v8, v3

    .line 34
    add-long/2addr v6, v8

    .line 35
    add-long/2addr v1, v6

    .line 36
    long-to-int v6, v1

    .line 37
    aput v6, p2, v5

    .line 38
    .line 39
    ushr-long/2addr v1, v0

    .line 40
    const/4 v5, 0x2

    .line 41
    aget v6, p0, v5

    .line 42
    .line 43
    int-to-long v6, v6

    .line 44
    and-long/2addr v6, v3

    .line 45
    aget v8, p1, v5

    .line 46
    .line 47
    int-to-long v8, v8

    .line 48
    and-long/2addr v8, v3

    .line 49
    add-long/2addr v6, v8

    .line 50
    add-long/2addr v1, v6

    .line 51
    long-to-int v6, v1

    .line 52
    aput v6, p2, v5

    .line 53
    .line 54
    ushr-long/2addr v1, v0

    .line 55
    const/4 v5, 0x3

    .line 56
    aget v6, p0, v5

    .line 57
    .line 58
    int-to-long v6, v6

    .line 59
    and-long/2addr v6, v3

    .line 60
    aget v8, p1, v5

    .line 61
    .line 62
    int-to-long v8, v8

    .line 63
    and-long/2addr v8, v3

    .line 64
    add-long/2addr v6, v8

    .line 65
    add-long/2addr v1, v6

    .line 66
    long-to-int v6, v1

    .line 67
    aput v6, p2, v5

    .line 68
    .line 69
    ushr-long/2addr v1, v0

    .line 70
    const/4 v5, 0x4

    .line 71
    aget v6, p0, v5

    .line 72
    .line 73
    int-to-long v6, v6

    .line 74
    and-long/2addr v6, v3

    .line 75
    aget v8, p1, v5

    .line 76
    .line 77
    int-to-long v8, v8

    .line 78
    and-long/2addr v8, v3

    .line 79
    add-long/2addr v6, v8

    .line 80
    add-long/2addr v1, v6

    .line 81
    long-to-int v6, v1

    .line 82
    aput v6, p2, v5

    .line 83
    .line 84
    ushr-long/2addr v1, v0

    .line 85
    const/4 v5, 0x5

    .line 86
    aget v6, p0, v5

    .line 87
    .line 88
    int-to-long v6, v6

    .line 89
    and-long/2addr v6, v3

    .line 90
    aget v8, p1, v5

    .line 91
    .line 92
    int-to-long v8, v8

    .line 93
    and-long/2addr v8, v3

    .line 94
    add-long/2addr v6, v8

    .line 95
    add-long/2addr v1, v6

    .line 96
    long-to-int v6, v1

    .line 97
    aput v6, p2, v5

    .line 98
    .line 99
    ushr-long/2addr v1, v0

    .line 100
    const/4 v5, 0x6

    .line 101
    aget v6, p0, v5

    .line 102
    .line 103
    int-to-long v6, v6

    .line 104
    and-long/2addr v6, v3

    .line 105
    aget v8, p1, v5

    .line 106
    .line 107
    int-to-long v8, v8

    .line 108
    and-long/2addr v8, v3

    .line 109
    add-long/2addr v6, v8

    .line 110
    add-long/2addr v1, v6

    .line 111
    long-to-int v6, v1

    .line 112
    aput v6, p2, v5

    .line 113
    .line 114
    ushr-long/2addr v1, v0

    .line 115
    const/4 v5, 0x7

    .line 116
    aget p0, p0, v5

    .line 117
    .line 118
    int-to-long v6, p0

    .line 119
    and-long/2addr v6, v3

    .line 120
    aget p0, p1, v5

    .line 121
    .line 122
    int-to-long p0, p0

    .line 123
    and-long/2addr p0, v3

    .line 124
    add-long/2addr v6, p0

    .line 125
    add-long/2addr v1, v6

    .line 126
    long-to-int p0, v1

    .line 127
    aput p0, p2, v5

    .line 128
    .line 129
    ushr-long p0, v1, v0

    .line 130
    .line 131
    long-to-int p1, p0

    .line 132
    return p1
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static 〇0〇O0088o([J)Z
    .locals 8

    .line 1
    const/4 v0, 0x0

    .line 2
    aget-wide v1, p0, v0

    .line 3
    .line 4
    const-wide/16 v3, 0x1

    .line 5
    .line 6
    cmp-long v5, v1, v3

    .line 7
    .line 8
    if-eqz v5, :cond_0

    .line 9
    .line 10
    return v0

    .line 11
    :cond_0
    const/4 v1, 0x1

    .line 12
    const/4 v2, 0x1

    .line 13
    :goto_0
    const/4 v3, 0x4

    .line 14
    if-ge v2, v3, :cond_2

    .line 15
    .line 16
    aget-wide v3, p0, v2

    .line 17
    .line 18
    const-wide/16 v5, 0x0

    .line 19
    .line 20
    cmp-long v7, v3, v5

    .line 21
    .line 22
    if-eqz v7, :cond_1

    .line 23
    .line 24
    return v0

    .line 25
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_2
    return v1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static 〇80〇808〇O()[J
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [J

    .line 4
    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static 〇8o8o〇([I[I)Z
    .locals 3

    .line 1
    const/4 v0, 0x7

    .line 2
    :goto_0
    if-ltz v0, :cond_1

    .line 3
    .line 4
    aget v1, p0, v0

    .line 5
    .line 6
    aget v2, p1, v0

    .line 7
    .line 8
    if-eq v1, v2, :cond_0

    .line 9
    .line 10
    const/4 p0, 0x0

    .line 11
    return p0

    .line 12
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    const/4 p0, 0x1

    .line 16
    return p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇O00([I[I)Z
    .locals 5

    .line 1
    const/4 v0, 0x7

    .line 2
    :goto_0
    const/4 v1, 0x1

    .line 3
    if-ltz v0, :cond_2

    .line 4
    .line 5
    aget v2, p0, v0

    .line 6
    .line 7
    const/high16 v3, -0x80000000

    .line 8
    .line 9
    xor-int/2addr v2, v3

    .line 10
    aget v4, p1, v0

    .line 11
    .line 12
    xor-int/2addr v3, v4

    .line 13
    if-ge v2, v3, :cond_0

    .line 14
    .line 15
    const/4 p0, 0x0

    .line 16
    return p0

    .line 17
    :cond_0
    if-le v2, v3, :cond_1

    .line 18
    .line 19
    return v1

    .line 20
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_2
    return v1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇O888o0o([II[II[II)V
    .locals 34

    .line 1
    add-int/lit8 v0, p3, 0x0

    .line 2
    .line 3
    aget v0, p2, v0

    .line 4
    .line 5
    int-to-long v0, v0

    .line 6
    const-wide v2, 0xffffffffL

    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    and-long/2addr v0, v2

    .line 12
    add-int/lit8 v4, p3, 0x1

    .line 13
    .line 14
    aget v4, p2, v4

    .line 15
    .line 16
    int-to-long v4, v4

    .line 17
    and-long/2addr v4, v2

    .line 18
    add-int/lit8 v6, p3, 0x2

    .line 19
    .line 20
    aget v6, p2, v6

    .line 21
    .line 22
    int-to-long v6, v6

    .line 23
    and-long/2addr v6, v2

    .line 24
    add-int/lit8 v8, p3, 0x3

    .line 25
    .line 26
    aget v8, p2, v8

    .line 27
    .line 28
    int-to-long v8, v8

    .line 29
    and-long/2addr v8, v2

    .line 30
    add-int/lit8 v10, p3, 0x4

    .line 31
    .line 32
    aget v10, p2, v10

    .line 33
    .line 34
    int-to-long v10, v10

    .line 35
    and-long/2addr v10, v2

    .line 36
    add-int/lit8 v12, p3, 0x5

    .line 37
    .line 38
    aget v12, p2, v12

    .line 39
    .line 40
    int-to-long v12, v12

    .line 41
    and-long/2addr v12, v2

    .line 42
    add-int/lit8 v14, p3, 0x6

    .line 43
    .line 44
    aget v14, p2, v14

    .line 45
    .line 46
    int-to-long v14, v14

    .line 47
    and-long/2addr v14, v2

    .line 48
    add-int/lit8 v16, p3, 0x7

    .line 49
    .line 50
    move-wide/from16 v17, v14

    .line 51
    .line 52
    aget v14, p2, v16

    .line 53
    .line 54
    int-to-long v14, v14

    .line 55
    and-long/2addr v14, v2

    .line 56
    add-int/lit8 v16, p1, 0x0

    .line 57
    .line 58
    move-wide/from16 p2, v14

    .line 59
    .line 60
    aget v14, p0, v16

    .line 61
    .line 62
    int-to-long v14, v14

    .line 63
    and-long/2addr v14, v2

    .line 64
    mul-long v19, v14, v0

    .line 65
    .line 66
    const-wide/16 v21, 0x0

    .line 67
    .line 68
    add-long v2, v19, v21

    .line 69
    .line 70
    add-int/lit8 v16, p5, 0x0

    .line 71
    .line 72
    move-wide/from16 v19, v0

    .line 73
    .line 74
    long-to-int v0, v2

    .line 75
    aput v0, p4, v16

    .line 76
    .line 77
    const/16 v0, 0x20

    .line 78
    .line 79
    ushr-long v1, v2, v0

    .line 80
    .line 81
    mul-long v25, v14, v4

    .line 82
    .line 83
    add-long v1, v1, v25

    .line 84
    .line 85
    add-int/lit8 v3, p5, 0x1

    .line 86
    .line 87
    move-wide/from16 v25, v4

    .line 88
    .line 89
    long-to-int v4, v1

    .line 90
    aput v4, p4, v3

    .line 91
    .line 92
    ushr-long/2addr v1, v0

    .line 93
    mul-long v3, v14, v6

    .line 94
    .line 95
    add-long/2addr v1, v3

    .line 96
    add-int/lit8 v3, p5, 0x2

    .line 97
    .line 98
    long-to-int v4, v1

    .line 99
    aput v4, p4, v3

    .line 100
    .line 101
    ushr-long/2addr v1, v0

    .line 102
    mul-long v3, v14, v8

    .line 103
    .line 104
    add-long/2addr v1, v3

    .line 105
    add-int/lit8 v3, p5, 0x3

    .line 106
    .line 107
    long-to-int v4, v1

    .line 108
    aput v4, p4, v3

    .line 109
    .line 110
    ushr-long/2addr v1, v0

    .line 111
    mul-long v3, v14, v10

    .line 112
    .line 113
    add-long/2addr v1, v3

    .line 114
    add-int/lit8 v3, p5, 0x4

    .line 115
    .line 116
    long-to-int v4, v1

    .line 117
    aput v4, p4, v3

    .line 118
    .line 119
    ushr-long/2addr v1, v0

    .line 120
    mul-long v3, v14, v12

    .line 121
    .line 122
    add-long/2addr v1, v3

    .line 123
    add-int/lit8 v3, p5, 0x5

    .line 124
    .line 125
    long-to-int v4, v1

    .line 126
    aput v4, p4, v3

    .line 127
    .line 128
    ushr-long/2addr v1, v0

    .line 129
    mul-long v3, v14, v17

    .line 130
    .line 131
    add-long/2addr v1, v3

    .line 132
    add-int/lit8 v3, p5, 0x6

    .line 133
    .line 134
    long-to-int v4, v1

    .line 135
    aput v4, p4, v3

    .line 136
    .line 137
    ushr-long/2addr v1, v0

    .line 138
    move-wide/from16 v3, p2

    .line 139
    .line 140
    mul-long v14, v14, v3

    .line 141
    .line 142
    add-long/2addr v1, v14

    .line 143
    add-int/lit8 v5, p5, 0x7

    .line 144
    .line 145
    long-to-int v14, v1

    .line 146
    aput v14, p4, v5

    .line 147
    .line 148
    ushr-long/2addr v1, v0

    .line 149
    add-int/lit8 v5, p5, 0x8

    .line 150
    .line 151
    long-to-int v2, v1

    .line 152
    aput v2, p4, v5

    .line 153
    .line 154
    const/4 v1, 0x1

    .line 155
    move/from16 v2, p5

    .line 156
    .line 157
    const/4 v5, 0x1

    .line 158
    :goto_0
    const/16 v14, 0x8

    .line 159
    .line 160
    if-ge v5, v14, :cond_0

    .line 161
    .line 162
    add-int/2addr v2, v1

    .line 163
    add-int v14, p1, v5

    .line 164
    .line 165
    aget v14, p0, v14

    .line 166
    .line 167
    int-to-long v14, v14

    .line 168
    const-wide v23, 0xffffffffL

    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    and-long v14, v14, v23

    .line 174
    .line 175
    mul-long v27, v14, v19

    .line 176
    .line 177
    add-int/lit8 v16, v2, 0x0

    .line 178
    .line 179
    aget v1, p4, v16

    .line 180
    .line 181
    int-to-long v0, v1

    .line 182
    and-long v0, v0, v23

    .line 183
    .line 184
    add-long v27, v27, v0

    .line 185
    .line 186
    add-long v0, v27, v21

    .line 187
    .line 188
    move/from16 v27, v5

    .line 189
    .line 190
    long-to-int v5, v0

    .line 191
    aput v5, p4, v16

    .line 192
    .line 193
    const/16 v5, 0x20

    .line 194
    .line 195
    ushr-long/2addr v0, v5

    .line 196
    mul-long v28, v14, v25

    .line 197
    .line 198
    add-int/lit8 v16, v2, 0x1

    .line 199
    .line 200
    aget v5, p4, v16

    .line 201
    .line 202
    move-wide/from16 v30, v3

    .line 203
    .line 204
    int-to-long v3, v5

    .line 205
    and-long v3, v3, v23

    .line 206
    .line 207
    add-long v28, v28, v3

    .line 208
    .line 209
    add-long v0, v0, v28

    .line 210
    .line 211
    long-to-int v3, v0

    .line 212
    aput v3, p4, v16

    .line 213
    .line 214
    const/16 v3, 0x20

    .line 215
    .line 216
    ushr-long/2addr v0, v3

    .line 217
    mul-long v4, v14, v6

    .line 218
    .line 219
    add-int/lit8 v16, v2, 0x2

    .line 220
    .line 221
    aget v3, p4, v16

    .line 222
    .line 223
    move-wide/from16 v28, v6

    .line 224
    .line 225
    int-to-long v6, v3

    .line 226
    and-long v6, v6, v23

    .line 227
    .line 228
    add-long/2addr v4, v6

    .line 229
    add-long/2addr v0, v4

    .line 230
    long-to-int v3, v0

    .line 231
    aput v3, p4, v16

    .line 232
    .line 233
    const/16 v3, 0x20

    .line 234
    .line 235
    ushr-long/2addr v0, v3

    .line 236
    mul-long v4, v14, v8

    .line 237
    .line 238
    add-int/lit8 v6, v2, 0x3

    .line 239
    .line 240
    aget v7, p4, v6

    .line 241
    .line 242
    move-wide/from16 v32, v8

    .line 243
    .line 244
    int-to-long v7, v7

    .line 245
    and-long v7, v7, v23

    .line 246
    .line 247
    add-long/2addr v4, v7

    .line 248
    add-long/2addr v0, v4

    .line 249
    long-to-int v4, v0

    .line 250
    aput v4, p4, v6

    .line 251
    .line 252
    ushr-long/2addr v0, v3

    .line 253
    mul-long v4, v14, v10

    .line 254
    .line 255
    add-int/lit8 v6, v2, 0x4

    .line 256
    .line 257
    aget v7, p4, v6

    .line 258
    .line 259
    int-to-long v7, v7

    .line 260
    and-long v7, v7, v23

    .line 261
    .line 262
    add-long/2addr v4, v7

    .line 263
    add-long/2addr v0, v4

    .line 264
    long-to-int v4, v0

    .line 265
    aput v4, p4, v6

    .line 266
    .line 267
    ushr-long/2addr v0, v3

    .line 268
    mul-long v4, v14, v12

    .line 269
    .line 270
    add-int/lit8 v6, v2, 0x5

    .line 271
    .line 272
    aget v7, p4, v6

    .line 273
    .line 274
    int-to-long v7, v7

    .line 275
    and-long v7, v7, v23

    .line 276
    .line 277
    add-long/2addr v4, v7

    .line 278
    add-long/2addr v0, v4

    .line 279
    long-to-int v4, v0

    .line 280
    aput v4, p4, v6

    .line 281
    .line 282
    ushr-long/2addr v0, v3

    .line 283
    mul-long v4, v14, v17

    .line 284
    .line 285
    add-int/lit8 v6, v2, 0x6

    .line 286
    .line 287
    aget v7, p4, v6

    .line 288
    .line 289
    int-to-long v7, v7

    .line 290
    and-long v7, v7, v23

    .line 291
    .line 292
    add-long/2addr v4, v7

    .line 293
    add-long/2addr v0, v4

    .line 294
    long-to-int v4, v0

    .line 295
    aput v4, p4, v6

    .line 296
    .line 297
    ushr-long/2addr v0, v3

    .line 298
    mul-long v14, v14, v30

    .line 299
    .line 300
    add-int/lit8 v4, v2, 0x7

    .line 301
    .line 302
    aget v5, p4, v4

    .line 303
    .line 304
    int-to-long v5, v5

    .line 305
    and-long v5, v5, v23

    .line 306
    .line 307
    add-long/2addr v14, v5

    .line 308
    add-long/2addr v0, v14

    .line 309
    long-to-int v5, v0

    .line 310
    aput v5, p4, v4

    .line 311
    .line 312
    ushr-long/2addr v0, v3

    .line 313
    add-int/lit8 v4, v2, 0x8

    .line 314
    .line 315
    long-to-int v1, v0

    .line 316
    aput v1, p4, v4

    .line 317
    .line 318
    add-int/lit8 v5, v27, 0x1

    .line 319
    .line 320
    move-wide/from16 v6, v28

    .line 321
    .line 322
    move-wide/from16 v3, v30

    .line 323
    .line 324
    move-wide/from16 v8, v32

    .line 325
    .line 326
    const/16 v0, 0x20

    .line 327
    .line 328
    const/4 v1, 0x1

    .line 329
    goto/16 :goto_0

    .line 330
    .line 331
    :cond_0
    return-void
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method public static 〇O8o08O([J[J)Z
    .locals 6

    .line 1
    const/4 v0, 0x3

    .line 2
    :goto_0
    if-ltz v0, :cond_1

    .line 3
    .line 4
    aget-wide v1, p0, v0

    .line 5
    .line 6
    aget-wide v3, p1, v0

    .line 7
    .line 8
    cmp-long v5, v1, v3

    .line 9
    .line 10
    if-eqz v5, :cond_0

    .line 11
    .line 12
    const/4 p0, 0x0

    .line 13
    return p0

    .line 14
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    const/4 p0, 0x1

    .line 18
    return p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇O〇([II[II)Z
    .locals 5

    .line 1
    const/4 v0, 0x7

    .line 2
    :goto_0
    const/4 v1, 0x1

    .line 3
    if-ltz v0, :cond_2

    .line 4
    .line 5
    add-int v2, p1, v0

    .line 6
    .line 7
    aget v2, p0, v2

    .line 8
    .line 9
    const/high16 v3, -0x80000000

    .line 10
    .line 11
    xor-int/2addr v2, v3

    .line 12
    add-int v4, p3, v0

    .line 13
    .line 14
    aget v4, p2, v4

    .line 15
    .line 16
    xor-int/2addr v3, v4

    .line 17
    if-ge v2, v3, :cond_0

    .line 18
    .line 19
    const/4 p0, 0x0

    .line 20
    return p0

    .line 21
    :cond_0
    if-le v2, v3, :cond_1

    .line 22
    .line 23
    return v1

    .line 24
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_2
    return v1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static 〇o([I)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    aput v0, p0, v0

    .line 3
    .line 4
    const/4 v1, 0x1

    .line 5
    aput v0, p0, v1

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    aput v0, p0, v1

    .line 9
    .line 10
    const/4 v1, 0x3

    .line 11
    aput v0, p0, v1

    .line 12
    .line 13
    const/4 v1, 0x4

    .line 14
    aput v0, p0, v1

    .line 15
    .line 16
    const/4 v1, 0x5

    .line 17
    aput v0, p0, v1

    .line 18
    .line 19
    const/4 v1, 0x6

    .line 20
    aput v0, p0, v1

    .line 21
    .line 22
    const/4 v1, 0x7

    .line 23
    aput v0, p0, v1

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static 〇o00〇〇Oo([I[I[I)I
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p0, v0

    .line 3
    .line 4
    int-to-long v1, v1

    .line 5
    const-wide v3, 0xffffffffL

    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    and-long/2addr v1, v3

    .line 11
    aget v5, p1, v0

    .line 12
    .line 13
    int-to-long v5, v5

    .line 14
    and-long/2addr v5, v3

    .line 15
    add-long/2addr v1, v5

    .line 16
    aget v5, p2, v0

    .line 17
    .line 18
    int-to-long v5, v5

    .line 19
    and-long/2addr v5, v3

    .line 20
    add-long/2addr v1, v5

    .line 21
    const-wide/16 v5, 0x0

    .line 22
    .line 23
    add-long/2addr v1, v5

    .line 24
    long-to-int v5, v1

    .line 25
    aput v5, p2, v0

    .line 26
    .line 27
    const/16 v0, 0x20

    .line 28
    .line 29
    ushr-long/2addr v1, v0

    .line 30
    const/4 v5, 0x1

    .line 31
    aget v6, p0, v5

    .line 32
    .line 33
    int-to-long v6, v6

    .line 34
    and-long/2addr v6, v3

    .line 35
    aget v8, p1, v5

    .line 36
    .line 37
    int-to-long v8, v8

    .line 38
    and-long/2addr v8, v3

    .line 39
    add-long/2addr v6, v8

    .line 40
    aget v8, p2, v5

    .line 41
    .line 42
    int-to-long v8, v8

    .line 43
    and-long/2addr v8, v3

    .line 44
    add-long/2addr v6, v8

    .line 45
    add-long/2addr v1, v6

    .line 46
    long-to-int v6, v1

    .line 47
    aput v6, p2, v5

    .line 48
    .line 49
    ushr-long/2addr v1, v0

    .line 50
    const/4 v5, 0x2

    .line 51
    aget v6, p0, v5

    .line 52
    .line 53
    int-to-long v6, v6

    .line 54
    and-long/2addr v6, v3

    .line 55
    aget v8, p1, v5

    .line 56
    .line 57
    int-to-long v8, v8

    .line 58
    and-long/2addr v8, v3

    .line 59
    add-long/2addr v6, v8

    .line 60
    aget v8, p2, v5

    .line 61
    .line 62
    int-to-long v8, v8

    .line 63
    and-long/2addr v8, v3

    .line 64
    add-long/2addr v6, v8

    .line 65
    add-long/2addr v1, v6

    .line 66
    long-to-int v6, v1

    .line 67
    aput v6, p2, v5

    .line 68
    .line 69
    ushr-long/2addr v1, v0

    .line 70
    const/4 v5, 0x3

    .line 71
    aget v6, p0, v5

    .line 72
    .line 73
    int-to-long v6, v6

    .line 74
    and-long/2addr v6, v3

    .line 75
    aget v8, p1, v5

    .line 76
    .line 77
    int-to-long v8, v8

    .line 78
    and-long/2addr v8, v3

    .line 79
    add-long/2addr v6, v8

    .line 80
    aget v8, p2, v5

    .line 81
    .line 82
    int-to-long v8, v8

    .line 83
    and-long/2addr v8, v3

    .line 84
    add-long/2addr v6, v8

    .line 85
    add-long/2addr v1, v6

    .line 86
    long-to-int v6, v1

    .line 87
    aput v6, p2, v5

    .line 88
    .line 89
    ushr-long/2addr v1, v0

    .line 90
    const/4 v5, 0x4

    .line 91
    aget v6, p0, v5

    .line 92
    .line 93
    int-to-long v6, v6

    .line 94
    and-long/2addr v6, v3

    .line 95
    aget v8, p1, v5

    .line 96
    .line 97
    int-to-long v8, v8

    .line 98
    and-long/2addr v8, v3

    .line 99
    add-long/2addr v6, v8

    .line 100
    aget v8, p2, v5

    .line 101
    .line 102
    int-to-long v8, v8

    .line 103
    and-long/2addr v8, v3

    .line 104
    add-long/2addr v6, v8

    .line 105
    add-long/2addr v1, v6

    .line 106
    long-to-int v6, v1

    .line 107
    aput v6, p2, v5

    .line 108
    .line 109
    ushr-long/2addr v1, v0

    .line 110
    const/4 v5, 0x5

    .line 111
    aget v6, p0, v5

    .line 112
    .line 113
    int-to-long v6, v6

    .line 114
    and-long/2addr v6, v3

    .line 115
    aget v8, p1, v5

    .line 116
    .line 117
    int-to-long v8, v8

    .line 118
    and-long/2addr v8, v3

    .line 119
    add-long/2addr v6, v8

    .line 120
    aget v8, p2, v5

    .line 121
    .line 122
    int-to-long v8, v8

    .line 123
    and-long/2addr v8, v3

    .line 124
    add-long/2addr v6, v8

    .line 125
    add-long/2addr v1, v6

    .line 126
    long-to-int v6, v1

    .line 127
    aput v6, p2, v5

    .line 128
    .line 129
    ushr-long/2addr v1, v0

    .line 130
    const/4 v5, 0x6

    .line 131
    aget v6, p0, v5

    .line 132
    .line 133
    int-to-long v6, v6

    .line 134
    and-long/2addr v6, v3

    .line 135
    aget v8, p1, v5

    .line 136
    .line 137
    int-to-long v8, v8

    .line 138
    and-long/2addr v8, v3

    .line 139
    add-long/2addr v6, v8

    .line 140
    aget v8, p2, v5

    .line 141
    .line 142
    int-to-long v8, v8

    .line 143
    and-long/2addr v8, v3

    .line 144
    add-long/2addr v6, v8

    .line 145
    add-long/2addr v1, v6

    .line 146
    long-to-int v6, v1

    .line 147
    aput v6, p2, v5

    .line 148
    .line 149
    ushr-long/2addr v1, v0

    .line 150
    const/4 v5, 0x7

    .line 151
    aget p0, p0, v5

    .line 152
    .line 153
    int-to-long v6, p0

    .line 154
    and-long/2addr v6, v3

    .line 155
    aget p0, p1, v5

    .line 156
    .line 157
    int-to-long p0, p0

    .line 158
    and-long/2addr p0, v3

    .line 159
    add-long/2addr v6, p0

    .line 160
    aget p0, p2, v5

    .line 161
    .line 162
    int-to-long p0, p0

    .line 163
    and-long/2addr p0, v3

    .line 164
    add-long/2addr v6, p0

    .line 165
    add-long/2addr v1, v6

    .line 166
    long-to-int p0, v1

    .line 167
    aput p0, p2, v5

    .line 168
    .line 169
    ushr-long p0, v1, v0

    .line 170
    .line 171
    long-to-int p1, p0

    .line 172
    return p1
.end method

.method public static 〇oOO8O8([II[II)V
    .locals 48

    .line 1
    add-int/lit8 v0, p1, 0x0

    .line 2
    .line 3
    aget v0, p0, v0

    .line 4
    .line 5
    int-to-long v0, v0

    .line 6
    const-wide v2, 0xffffffffL

    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    and-long/2addr v0, v2

    .line 12
    const/4 v4, 0x0

    .line 13
    const/16 v6, 0x10

    .line 14
    .line 15
    const/4 v7, 0x7

    .line 16
    :goto_0
    add-int/lit8 v8, v7, -0x1

    .line 17
    .line 18
    add-int v7, p1, v7

    .line 19
    .line 20
    aget v7, p0, v7

    .line 21
    .line 22
    int-to-long v9, v7

    .line 23
    and-long/2addr v9, v2

    .line 24
    mul-long v9, v9, v9

    .line 25
    .line 26
    add-int/lit8 v6, v6, -0x1

    .line 27
    .line 28
    add-int v7, p3, v6

    .line 29
    .line 30
    shl-int/lit8 v4, v4, 0x1f

    .line 31
    .line 32
    const/16 v11, 0x21

    .line 33
    .line 34
    ushr-long v12, v9, v11

    .line 35
    .line 36
    long-to-int v13, v12

    .line 37
    or-int/2addr v4, v13

    .line 38
    aput v4, p2, v7

    .line 39
    .line 40
    add-int/lit8 v6, v6, -0x1

    .line 41
    .line 42
    add-int v4, p3, v6

    .line 43
    .line 44
    const/4 v7, 0x1

    .line 45
    ushr-long v12, v9, v7

    .line 46
    .line 47
    long-to-int v13, v12

    .line 48
    aput v13, p2, v4

    .line 49
    .line 50
    long-to-int v4, v9

    .line 51
    if-gtz v8, :cond_0

    .line 52
    .line 53
    mul-long v8, v0, v0

    .line 54
    .line 55
    shl-int/lit8 v4, v4, 0x1f

    .line 56
    .line 57
    int-to-long v12, v4

    .line 58
    and-long/2addr v12, v2

    .line 59
    ushr-long v10, v8, v11

    .line 60
    .line 61
    or-long/2addr v10, v12

    .line 62
    add-int/lit8 v4, p3, 0x0

    .line 63
    .line 64
    long-to-int v6, v8

    .line 65
    aput v6, p2, v4

    .line 66
    .line 67
    const/16 v4, 0x20

    .line 68
    .line 69
    ushr-long/2addr v8, v4

    .line 70
    long-to-int v6, v8

    .line 71
    and-int/2addr v6, v7

    .line 72
    add-int/lit8 v8, p1, 0x1

    .line 73
    .line 74
    aget v8, p0, v8

    .line 75
    .line 76
    int-to-long v8, v8

    .line 77
    and-long/2addr v8, v2

    .line 78
    add-int/lit8 v12, p3, 0x2

    .line 79
    .line 80
    aget v13, p2, v12

    .line 81
    .line 82
    int-to-long v13, v13

    .line 83
    and-long/2addr v13, v2

    .line 84
    mul-long v15, v8, v0

    .line 85
    .line 86
    add-long/2addr v10, v15

    .line 87
    long-to-int v15, v10

    .line 88
    add-int/lit8 v16, p3, 0x1

    .line 89
    .line 90
    shl-int/lit8 v17, v15, 0x1

    .line 91
    .line 92
    or-int v6, v17, v6

    .line 93
    .line 94
    aput v6, p2, v16

    .line 95
    .line 96
    ushr-int/lit8 v6, v15, 0x1f

    .line 97
    .line 98
    ushr-long/2addr v10, v4

    .line 99
    add-long/2addr v13, v10

    .line 100
    add-int/lit8 v10, p1, 0x2

    .line 101
    .line 102
    aget v10, p0, v10

    .line 103
    .line 104
    int-to-long v10, v10

    .line 105
    and-long/2addr v10, v2

    .line 106
    add-int/lit8 v15, p3, 0x3

    .line 107
    .line 108
    aget v7, p2, v15

    .line 109
    .line 110
    int-to-long v4, v7

    .line 111
    and-long/2addr v4, v2

    .line 112
    add-int/lit8 v7, p3, 0x4

    .line 113
    .line 114
    move/from16 v19, v15

    .line 115
    .line 116
    aget v15, p2, v7

    .line 117
    .line 118
    move-wide/from16 v20, v4

    .line 119
    .line 120
    int-to-long v4, v15

    .line 121
    and-long/2addr v4, v2

    .line 122
    mul-long v22, v10, v0

    .line 123
    .line 124
    add-long v13, v13, v22

    .line 125
    .line 126
    long-to-int v15, v13

    .line 127
    shl-int/lit8 v22, v15, 0x1

    .line 128
    .line 129
    or-int v6, v22, v6

    .line 130
    .line 131
    aput v6, p2, v12

    .line 132
    .line 133
    ushr-int/lit8 v6, v15, 0x1f

    .line 134
    .line 135
    const/16 v12, 0x20

    .line 136
    .line 137
    ushr-long/2addr v13, v12

    .line 138
    mul-long v22, v10, v8

    .line 139
    .line 140
    add-long v13, v13, v22

    .line 141
    .line 142
    add-long v13, v20, v13

    .line 143
    .line 144
    ushr-long v20, v13, v12

    .line 145
    .line 146
    add-long v4, v4, v20

    .line 147
    .line 148
    and-long v12, v13, v2

    .line 149
    .line 150
    add-int/lit8 v14, p1, 0x3

    .line 151
    .line 152
    aget v14, p0, v14

    .line 153
    .line 154
    int-to-long v14, v14

    .line 155
    and-long/2addr v14, v2

    .line 156
    add-int/lit8 v20, p3, 0x5

    .line 157
    .line 158
    move/from16 v21, v7

    .line 159
    .line 160
    aget v7, p2, v20

    .line 161
    .line 162
    move-wide/from16 v22, v10

    .line 163
    .line 164
    int-to-long v10, v7

    .line 165
    and-long/2addr v10, v2

    .line 166
    const/16 v7, 0x20

    .line 167
    .line 168
    ushr-long v24, v4, v7

    .line 169
    .line 170
    add-long v10, v10, v24

    .line 171
    .line 172
    and-long/2addr v4, v2

    .line 173
    add-int/lit8 v24, p3, 0x6

    .line 174
    .line 175
    aget v7, p2, v24

    .line 176
    .line 177
    move-wide/from16 v25, v4

    .line 178
    .line 179
    int-to-long v4, v7

    .line 180
    and-long/2addr v4, v2

    .line 181
    const/16 v7, 0x20

    .line 182
    .line 183
    ushr-long v27, v10, v7

    .line 184
    .line 185
    add-long v4, v4, v27

    .line 186
    .line 187
    and-long/2addr v10, v2

    .line 188
    mul-long v27, v14, v0

    .line 189
    .line 190
    add-long v12, v12, v27

    .line 191
    .line 192
    long-to-int v2, v12

    .line 193
    shl-int/lit8 v3, v2, 0x1

    .line 194
    .line 195
    or-int/2addr v3, v6

    .line 196
    aput v3, p2, v19

    .line 197
    .line 198
    ushr-int/lit8 v2, v2, 0x1f

    .line 199
    .line 200
    ushr-long/2addr v12, v7

    .line 201
    mul-long v18, v14, v8

    .line 202
    .line 203
    add-long v12, v12, v18

    .line 204
    .line 205
    add-long v12, v25, v12

    .line 206
    .line 207
    ushr-long v18, v12, v7

    .line 208
    .line 209
    mul-long v25, v14, v22

    .line 210
    .line 211
    add-long v18, v18, v25

    .line 212
    .line 213
    add-long v10, v10, v18

    .line 214
    .line 215
    const-wide v25, 0xffffffffL

    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    and-long v12, v12, v25

    .line 221
    .line 222
    ushr-long v27, v10, v7

    .line 223
    .line 224
    add-long v4, v4, v27

    .line 225
    .line 226
    and-long v6, v10, v25

    .line 227
    .line 228
    add-int/lit8 v3, p1, 0x4

    .line 229
    .line 230
    aget v3, p0, v3

    .line 231
    .line 232
    int-to-long v10, v3

    .line 233
    and-long v10, v10, v25

    .line 234
    .line 235
    add-int/lit8 v3, p3, 0x7

    .line 236
    .line 237
    move-wide/from16 v29, v14

    .line 238
    .line 239
    aget v14, p2, v3

    .line 240
    .line 241
    int-to-long v14, v14

    .line 242
    and-long v14, v14, v25

    .line 243
    .line 244
    const/16 v18, 0x20

    .line 245
    .line 246
    ushr-long v27, v4, v18

    .line 247
    .line 248
    add-long v14, v14, v27

    .line 249
    .line 250
    and-long v4, v4, v25

    .line 251
    .line 252
    add-int/lit8 v19, p3, 0x8

    .line 253
    .line 254
    move/from16 v31, v3

    .line 255
    .line 256
    aget v3, p2, v19

    .line 257
    .line 258
    move-wide/from16 v32, v4

    .line 259
    .line 260
    int-to-long v3, v3

    .line 261
    and-long v3, v3, v25

    .line 262
    .line 263
    ushr-long v27, v14, v18

    .line 264
    .line 265
    add-long v3, v3, v27

    .line 266
    .line 267
    and-long v14, v14, v25

    .line 268
    .line 269
    mul-long v25, v10, v0

    .line 270
    .line 271
    add-long v12, v12, v25

    .line 272
    .line 273
    long-to-int v5, v12

    .line 274
    shl-int/lit8 v25, v5, 0x1

    .line 275
    .line 276
    or-int v2, v25, v2

    .line 277
    .line 278
    aput v2, p2, v21

    .line 279
    .line 280
    ushr-int/lit8 v2, v5, 0x1f

    .line 281
    .line 282
    ushr-long v12, v12, v18

    .line 283
    .line 284
    mul-long v25, v10, v8

    .line 285
    .line 286
    add-long v12, v12, v25

    .line 287
    .line 288
    add-long/2addr v6, v12

    .line 289
    ushr-long v12, v6, v18

    .line 290
    .line 291
    mul-long v25, v10, v22

    .line 292
    .line 293
    add-long v12, v12, v25

    .line 294
    .line 295
    add-long v12, v32, v12

    .line 296
    .line 297
    const-wide v25, 0xffffffffL

    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    and-long v5, v6, v25

    .line 303
    .line 304
    ushr-long v27, v12, v18

    .line 305
    .line 306
    mul-long v32, v10, v29

    .line 307
    .line 308
    add-long v27, v27, v32

    .line 309
    .line 310
    add-long v14, v14, v27

    .line 311
    .line 312
    and-long v12, v12, v25

    .line 313
    .line 314
    ushr-long v27, v14, v18

    .line 315
    .line 316
    add-long v3, v3, v27

    .line 317
    .line 318
    and-long v14, v14, v25

    .line 319
    .line 320
    add-int/lit8 v7, p1, 0x5

    .line 321
    .line 322
    aget v7, p0, v7

    .line 323
    .line 324
    move-wide/from16 v32, v10

    .line 325
    .line 326
    int-to-long v10, v7

    .line 327
    and-long v10, v10, v25

    .line 328
    .line 329
    add-int/lit8 v7, p3, 0x9

    .line 330
    .line 331
    move-wide/from16 v34, v14

    .line 332
    .line 333
    aget v14, p2, v7

    .line 334
    .line 335
    int-to-long v14, v14

    .line 336
    and-long v14, v14, v25

    .line 337
    .line 338
    const/16 v18, 0x20

    .line 339
    .line 340
    ushr-long v27, v3, v18

    .line 341
    .line 342
    add-long v14, v14, v27

    .line 343
    .line 344
    and-long v3, v3, v25

    .line 345
    .line 346
    add-int/lit8 v21, p3, 0xa

    .line 347
    .line 348
    move/from16 v36, v7

    .line 349
    .line 350
    aget v7, p2, v21

    .line 351
    .line 352
    move-wide/from16 v37, v3

    .line 353
    .line 354
    int-to-long v3, v7

    .line 355
    and-long v3, v3, v25

    .line 356
    .line 357
    ushr-long v27, v14, v18

    .line 358
    .line 359
    add-long v3, v3, v27

    .line 360
    .line 361
    and-long v14, v14, v25

    .line 362
    .line 363
    mul-long v25, v10, v0

    .line 364
    .line 365
    add-long v5, v5, v25

    .line 366
    .line 367
    long-to-int v7, v5

    .line 368
    shl-int/lit8 v25, v7, 0x1

    .line 369
    .line 370
    or-int v2, v25, v2

    .line 371
    .line 372
    aput v2, p2, v20

    .line 373
    .line 374
    ushr-int/lit8 v2, v7, 0x1f

    .line 375
    .line 376
    ushr-long v5, v5, v18

    .line 377
    .line 378
    mul-long v25, v10, v8

    .line 379
    .line 380
    add-long v5, v5, v25

    .line 381
    .line 382
    add-long/2addr v12, v5

    .line 383
    ushr-long v5, v12, v18

    .line 384
    .line 385
    mul-long v25, v10, v22

    .line 386
    .line 387
    add-long v5, v5, v25

    .line 388
    .line 389
    add-long v5, v34, v5

    .line 390
    .line 391
    const-wide v25, 0xffffffffL

    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    and-long v12, v12, v25

    .line 397
    .line 398
    ushr-long v27, v5, v18

    .line 399
    .line 400
    mul-long v34, v10, v29

    .line 401
    .line 402
    add-long v27, v27, v34

    .line 403
    .line 404
    add-long v27, v37, v27

    .line 405
    .line 406
    and-long v5, v5, v25

    .line 407
    .line 408
    ushr-long v34, v27, v18

    .line 409
    .line 410
    mul-long v37, v10, v32

    .line 411
    .line 412
    add-long v34, v34, v37

    .line 413
    .line 414
    add-long v14, v14, v34

    .line 415
    .line 416
    and-long v34, v27, v25

    .line 417
    .line 418
    ushr-long v27, v14, v18

    .line 419
    .line 420
    add-long v3, v3, v27

    .line 421
    .line 422
    and-long v14, v14, v25

    .line 423
    .line 424
    add-int/lit8 v7, p1, 0x6

    .line 425
    .line 426
    aget v7, p0, v7

    .line 427
    .line 428
    move-wide/from16 v37, v10

    .line 429
    .line 430
    int-to-long v10, v7

    .line 431
    and-long v10, v10, v25

    .line 432
    .line 433
    add-int/lit8 v7, p3, 0xb

    .line 434
    .line 435
    move-wide/from16 v39, v14

    .line 436
    .line 437
    aget v14, p2, v7

    .line 438
    .line 439
    int-to-long v14, v14

    .line 440
    and-long v14, v14, v25

    .line 441
    .line 442
    const/16 v18, 0x20

    .line 443
    .line 444
    ushr-long v27, v3, v18

    .line 445
    .line 446
    add-long v14, v14, v27

    .line 447
    .line 448
    and-long v3, v3, v25

    .line 449
    .line 450
    add-int/lit8 v20, p3, 0xc

    .line 451
    .line 452
    move/from16 v41, v7

    .line 453
    .line 454
    aget v7, p2, v20

    .line 455
    .line 456
    move-wide/from16 v42, v3

    .line 457
    .line 458
    int-to-long v3, v7

    .line 459
    and-long v3, v3, v25

    .line 460
    .line 461
    ushr-long v27, v14, v18

    .line 462
    .line 463
    add-long v3, v3, v27

    .line 464
    .line 465
    and-long v14, v14, v25

    .line 466
    .line 467
    mul-long v25, v10, v0

    .line 468
    .line 469
    add-long v12, v12, v25

    .line 470
    .line 471
    long-to-int v7, v12

    .line 472
    shl-int/lit8 v25, v7, 0x1

    .line 473
    .line 474
    or-int v2, v25, v2

    .line 475
    .line 476
    aput v2, p2, v24

    .line 477
    .line 478
    ushr-int/lit8 v2, v7, 0x1f

    .line 479
    .line 480
    ushr-long v12, v12, v18

    .line 481
    .line 482
    mul-long v24, v10, v8

    .line 483
    .line 484
    add-long v12, v12, v24

    .line 485
    .line 486
    add-long/2addr v5, v12

    .line 487
    ushr-long v12, v5, v18

    .line 488
    .line 489
    mul-long v24, v10, v22

    .line 490
    .line 491
    add-long v12, v12, v24

    .line 492
    .line 493
    add-long v34, v34, v12

    .line 494
    .line 495
    const-wide v12, 0xffffffffL

    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    and-long/2addr v5, v12

    .line 501
    ushr-long v24, v34, v18

    .line 502
    .line 503
    mul-long v26, v10, v29

    .line 504
    .line 505
    add-long v24, v24, v26

    .line 506
    .line 507
    add-long v24, v39, v24

    .line 508
    .line 509
    and-long v26, v34, v12

    .line 510
    .line 511
    ushr-long v34, v24, v18

    .line 512
    .line 513
    mul-long v39, v10, v32

    .line 514
    .line 515
    add-long v34, v34, v39

    .line 516
    .line 517
    add-long v34, v42, v34

    .line 518
    .line 519
    and-long v24, v24, v12

    .line 520
    .line 521
    ushr-long v39, v34, v18

    .line 522
    .line 523
    mul-long v42, v10, v37

    .line 524
    .line 525
    add-long v39, v39, v42

    .line 526
    .line 527
    add-long v14, v14, v39

    .line 528
    .line 529
    and-long v34, v34, v12

    .line 530
    .line 531
    ushr-long v39, v14, v18

    .line 532
    .line 533
    add-long v3, v3, v39

    .line 534
    .line 535
    and-long/2addr v14, v12

    .line 536
    const/4 v7, 0x7

    .line 537
    add-int/lit8 v7, p1, 0x7

    .line 538
    .line 539
    aget v7, p0, v7

    .line 540
    .line 541
    move-wide/from16 v39, v10

    .line 542
    .line 543
    int-to-long v10, v7

    .line 544
    and-long/2addr v10, v12

    .line 545
    add-int/lit8 v7, p3, 0xd

    .line 546
    .line 547
    move-wide/from16 v42, v14

    .line 548
    .line 549
    aget v14, p2, v7

    .line 550
    .line 551
    int-to-long v14, v14

    .line 552
    and-long/2addr v14, v12

    .line 553
    const/16 v17, 0x20

    .line 554
    .line 555
    ushr-long v44, v3, v17

    .line 556
    .line 557
    add-long v14, v14, v44

    .line 558
    .line 559
    and-long/2addr v3, v12

    .line 560
    add-int/lit8 v28, p3, 0xe

    .line 561
    .line 562
    move/from16 p0, v7

    .line 563
    .line 564
    aget v7, p2, v28

    .line 565
    .line 566
    move-wide/from16 v44, v3

    .line 567
    .line 568
    int-to-long v3, v7

    .line 569
    and-long/2addr v3, v12

    .line 570
    ushr-long v46, v14, v17

    .line 571
    .line 572
    add-long v3, v3, v46

    .line 573
    .line 574
    and-long/2addr v12, v14

    .line 575
    mul-long v0, v0, v10

    .line 576
    .line 577
    add-long/2addr v5, v0

    .line 578
    long-to-int v0, v5

    .line 579
    shl-int/lit8 v1, v0, 0x1

    .line 580
    .line 581
    or-int/2addr v1, v2

    .line 582
    aput v1, p2, v31

    .line 583
    .line 584
    ushr-int/lit8 v0, v0, 0x1f

    .line 585
    .line 586
    ushr-long v1, v5, v17

    .line 587
    .line 588
    mul-long v8, v8, v10

    .line 589
    .line 590
    add-long/2addr v1, v8

    .line 591
    add-long v1, v26, v1

    .line 592
    .line 593
    ushr-long v5, v1, v17

    .line 594
    .line 595
    mul-long v7, v10, v22

    .line 596
    .line 597
    add-long/2addr v5, v7

    .line 598
    add-long v5, v24, v5

    .line 599
    .line 600
    ushr-long v7, v5, v17

    .line 601
    .line 602
    mul-long v14, v10, v29

    .line 603
    .line 604
    add-long/2addr v7, v14

    .line 605
    add-long v7, v34, v7

    .line 606
    .line 607
    ushr-long v14, v7, v17

    .line 608
    .line 609
    mul-long v22, v10, v32

    .line 610
    .line 611
    add-long v14, v14, v22

    .line 612
    .line 613
    add-long v14, v42, v14

    .line 614
    .line 615
    ushr-long v22, v14, v17

    .line 616
    .line 617
    mul-long v24, v10, v37

    .line 618
    .line 619
    add-long v22, v22, v24

    .line 620
    .line 621
    move-wide/from16 v24, v14

    .line 622
    .line 623
    add-long v14, v44, v22

    .line 624
    .line 625
    ushr-long v22, v14, v17

    .line 626
    .line 627
    mul-long v10, v10, v39

    .line 628
    .line 629
    add-long v22, v22, v10

    .line 630
    .line 631
    add-long v12, v12, v22

    .line 632
    .line 633
    ushr-long v9, v12, v17

    .line 634
    .line 635
    add-long/2addr v3, v9

    .line 636
    long-to-int v2, v1

    .line 637
    shl-int/lit8 v1, v2, 0x1

    .line 638
    .line 639
    or-int/2addr v0, v1

    .line 640
    aput v0, p2, v19

    .line 641
    .line 642
    ushr-int/lit8 v0, v2, 0x1f

    .line 643
    .line 644
    long-to-int v1, v5

    .line 645
    shl-int/lit8 v2, v1, 0x1

    .line 646
    .line 647
    or-int/2addr v0, v2

    .line 648
    aput v0, p2, v36

    .line 649
    .line 650
    ushr-int/lit8 v0, v1, 0x1f

    .line 651
    .line 652
    long-to-int v1, v7

    .line 653
    shl-int/lit8 v2, v1, 0x1

    .line 654
    .line 655
    or-int/2addr v0, v2

    .line 656
    aput v0, p2, v21

    .line 657
    .line 658
    ushr-int/lit8 v0, v1, 0x1f

    .line 659
    .line 660
    move-wide/from16 v1, v24

    .line 661
    .line 662
    long-to-int v2, v1

    .line 663
    shl-int/lit8 v1, v2, 0x1

    .line 664
    .line 665
    or-int/2addr v0, v1

    .line 666
    aput v0, p2, v41

    .line 667
    .line 668
    ushr-int/lit8 v0, v2, 0x1f

    .line 669
    .line 670
    long-to-int v1, v14

    .line 671
    shl-int/lit8 v2, v1, 0x1

    .line 672
    .line 673
    or-int/2addr v0, v2

    .line 674
    aput v0, p2, v20

    .line 675
    .line 676
    ushr-int/lit8 v0, v1, 0x1f

    .line 677
    .line 678
    long-to-int v1, v12

    .line 679
    shl-int/lit8 v2, v1, 0x1

    .line 680
    .line 681
    or-int/2addr v0, v2

    .line 682
    aput v0, p2, p0

    .line 683
    .line 684
    ushr-int/lit8 v0, v1, 0x1f

    .line 685
    .line 686
    long-to-int v1, v3

    .line 687
    shl-int/lit8 v2, v1, 0x1

    .line 688
    .line 689
    or-int/2addr v0, v2

    .line 690
    aput v0, p2, v28

    .line 691
    .line 692
    ushr-int/lit8 v0, v1, 0x1f

    .line 693
    .line 694
    add-int/lit8 v1, p3, 0xf

    .line 695
    .line 696
    aget v2, p2, v1

    .line 697
    .line 698
    const/16 v5, 0x20

    .line 699
    .line 700
    ushr-long/2addr v3, v5

    .line 701
    long-to-int v4, v3

    .line 702
    add-int/2addr v2, v4

    .line 703
    const/4 v3, 0x1

    .line 704
    shl-int/2addr v2, v3

    .line 705
    or-int/2addr v0, v2

    .line 706
    aput v0, p2, v1

    .line 707
    .line 708
    return-void

    .line 709
    :cond_0
    move v7, v8

    .line 710
    goto/16 :goto_0
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
.end method

.method public static 〇oo〇(I[II[II[II)J
    .locals 13

    .line 1
    move v0, p0

    .line 2
    int-to-long v0, v0

    .line 3
    const-wide v2, 0xffffffffL

    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    and-long/2addr v0, v2

    .line 9
    add-int/lit8 v4, p2, 0x0

    .line 10
    .line 11
    aget v4, p1, v4

    .line 12
    .line 13
    int-to-long v4, v4

    .line 14
    and-long/2addr v4, v2

    .line 15
    mul-long v6, v0, v4

    .line 16
    .line 17
    add-int/lit8 v8, p4, 0x0

    .line 18
    .line 19
    aget v8, p3, v8

    .line 20
    .line 21
    int-to-long v8, v8

    .line 22
    and-long/2addr v8, v2

    .line 23
    add-long/2addr v6, v8

    .line 24
    const-wide/16 v8, 0x0

    .line 25
    .line 26
    add-long/2addr v6, v8

    .line 27
    add-int/lit8 v8, p6, 0x0

    .line 28
    .line 29
    long-to-int v9, v6

    .line 30
    aput v9, p5, v8

    .line 31
    .line 32
    const/16 v8, 0x20

    .line 33
    .line 34
    ushr-long/2addr v6, v8

    .line 35
    add-int/lit8 v9, p2, 0x1

    .line 36
    .line 37
    aget v9, p1, v9

    .line 38
    .line 39
    int-to-long v9, v9

    .line 40
    and-long/2addr v9, v2

    .line 41
    mul-long v11, v0, v9

    .line 42
    .line 43
    add-long/2addr v11, v4

    .line 44
    add-int/lit8 v4, p4, 0x1

    .line 45
    .line 46
    aget v4, p3, v4

    .line 47
    .line 48
    int-to-long v4, v4

    .line 49
    and-long/2addr v4, v2

    .line 50
    add-long/2addr v11, v4

    .line 51
    add-long/2addr v6, v11

    .line 52
    add-int/lit8 v4, p6, 0x1

    .line 53
    .line 54
    long-to-int v5, v6

    .line 55
    aput v5, p5, v4

    .line 56
    .line 57
    ushr-long v4, v6, v8

    .line 58
    .line 59
    add-int/lit8 v6, p2, 0x2

    .line 60
    .line 61
    aget v6, p1, v6

    .line 62
    .line 63
    int-to-long v6, v6

    .line 64
    and-long/2addr v6, v2

    .line 65
    mul-long v11, v0, v6

    .line 66
    .line 67
    add-long/2addr v11, v9

    .line 68
    add-int/lit8 v9, p4, 0x2

    .line 69
    .line 70
    aget v9, p3, v9

    .line 71
    .line 72
    int-to-long v9, v9

    .line 73
    and-long/2addr v9, v2

    .line 74
    add-long/2addr v11, v9

    .line 75
    add-long/2addr v4, v11

    .line 76
    add-int/lit8 v9, p6, 0x2

    .line 77
    .line 78
    long-to-int v10, v4

    .line 79
    aput v10, p5, v9

    .line 80
    .line 81
    ushr-long/2addr v4, v8

    .line 82
    add-int/lit8 v9, p2, 0x3

    .line 83
    .line 84
    aget v9, p1, v9

    .line 85
    .line 86
    int-to-long v9, v9

    .line 87
    and-long/2addr v9, v2

    .line 88
    mul-long v11, v0, v9

    .line 89
    .line 90
    add-long/2addr v11, v6

    .line 91
    add-int/lit8 v6, p4, 0x3

    .line 92
    .line 93
    aget v6, p3, v6

    .line 94
    .line 95
    int-to-long v6, v6

    .line 96
    and-long/2addr v6, v2

    .line 97
    add-long/2addr v11, v6

    .line 98
    add-long/2addr v4, v11

    .line 99
    add-int/lit8 v6, p6, 0x3

    .line 100
    .line 101
    long-to-int v7, v4

    .line 102
    aput v7, p5, v6

    .line 103
    .line 104
    ushr-long/2addr v4, v8

    .line 105
    add-int/lit8 v6, p2, 0x4

    .line 106
    .line 107
    aget v6, p1, v6

    .line 108
    .line 109
    int-to-long v6, v6

    .line 110
    and-long/2addr v6, v2

    .line 111
    mul-long v11, v0, v6

    .line 112
    .line 113
    add-long/2addr v11, v9

    .line 114
    add-int/lit8 v9, p4, 0x4

    .line 115
    .line 116
    aget v9, p3, v9

    .line 117
    .line 118
    int-to-long v9, v9

    .line 119
    and-long/2addr v9, v2

    .line 120
    add-long/2addr v11, v9

    .line 121
    add-long/2addr v4, v11

    .line 122
    add-int/lit8 v9, p6, 0x4

    .line 123
    .line 124
    long-to-int v10, v4

    .line 125
    aput v10, p5, v9

    .line 126
    .line 127
    ushr-long/2addr v4, v8

    .line 128
    add-int/lit8 v9, p2, 0x5

    .line 129
    .line 130
    aget v9, p1, v9

    .line 131
    .line 132
    int-to-long v9, v9

    .line 133
    and-long/2addr v9, v2

    .line 134
    mul-long v11, v0, v9

    .line 135
    .line 136
    add-long/2addr v11, v6

    .line 137
    add-int/lit8 v6, p4, 0x5

    .line 138
    .line 139
    aget v6, p3, v6

    .line 140
    .line 141
    int-to-long v6, v6

    .line 142
    and-long/2addr v6, v2

    .line 143
    add-long/2addr v11, v6

    .line 144
    add-long/2addr v4, v11

    .line 145
    add-int/lit8 v6, p6, 0x5

    .line 146
    .line 147
    long-to-int v7, v4

    .line 148
    aput v7, p5, v6

    .line 149
    .line 150
    ushr-long/2addr v4, v8

    .line 151
    add-int/lit8 v6, p2, 0x6

    .line 152
    .line 153
    aget v6, p1, v6

    .line 154
    .line 155
    int-to-long v6, v6

    .line 156
    and-long/2addr v6, v2

    .line 157
    mul-long v11, v0, v6

    .line 158
    .line 159
    add-long/2addr v11, v9

    .line 160
    add-int/lit8 v9, p4, 0x6

    .line 161
    .line 162
    aget v9, p3, v9

    .line 163
    .line 164
    int-to-long v9, v9

    .line 165
    and-long/2addr v9, v2

    .line 166
    add-long/2addr v11, v9

    .line 167
    add-long/2addr v4, v11

    .line 168
    add-int/lit8 v9, p6, 0x6

    .line 169
    .line 170
    long-to-int v10, v4

    .line 171
    aput v10, p5, v9

    .line 172
    .line 173
    ushr-long/2addr v4, v8

    .line 174
    add-int/lit8 v9, p2, 0x7

    .line 175
    .line 176
    aget v9, p1, v9

    .line 177
    .line 178
    int-to-long v9, v9

    .line 179
    and-long/2addr v9, v2

    .line 180
    mul-long v0, v0, v9

    .line 181
    .line 182
    add-long/2addr v0, v6

    .line 183
    add-int/lit8 v6, p4, 0x7

    .line 184
    .line 185
    aget v6, p3, v6

    .line 186
    .line 187
    int-to-long v6, v6

    .line 188
    and-long/2addr v2, v6

    .line 189
    add-long/2addr v0, v2

    .line 190
    add-long/2addr v4, v0

    .line 191
    add-int/lit8 v0, p6, 0x7

    .line 192
    .line 193
    long-to-int v1, v4

    .line 194
    aput v1, p5, v0

    .line 195
    .line 196
    ushr-long v0, v4, v8

    .line 197
    .line 198
    add-long/2addr v0, v9

    .line 199
    return-wide v0
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
.end method

.method public static 〇o〇([II[III)I
    .locals 9

    .line 1
    int-to-long v0, p4

    .line 2
    const-wide v2, 0xffffffffL

    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    and-long/2addr v0, v2

    .line 8
    add-int/lit8 p4, p1, 0x0

    .line 9
    .line 10
    aget p4, p0, p4

    .line 11
    .line 12
    int-to-long v4, p4

    .line 13
    and-long/2addr v4, v2

    .line 14
    add-int/lit8 p4, p3, 0x0

    .line 15
    .line 16
    aget v6, p2, p4

    .line 17
    .line 18
    int-to-long v6, v6

    .line 19
    and-long/2addr v6, v2

    .line 20
    add-long/2addr v4, v6

    .line 21
    add-long/2addr v0, v4

    .line 22
    long-to-int v4, v0

    .line 23
    aput v4, p2, p4

    .line 24
    .line 25
    const/16 p4, 0x20

    .line 26
    .line 27
    ushr-long/2addr v0, p4

    .line 28
    add-int/lit8 v4, p1, 0x1

    .line 29
    .line 30
    aget v4, p0, v4

    .line 31
    .line 32
    int-to-long v4, v4

    .line 33
    and-long/2addr v4, v2

    .line 34
    add-int/lit8 v6, p3, 0x1

    .line 35
    .line 36
    aget v7, p2, v6

    .line 37
    .line 38
    int-to-long v7, v7

    .line 39
    and-long/2addr v7, v2

    .line 40
    add-long/2addr v4, v7

    .line 41
    add-long/2addr v0, v4

    .line 42
    long-to-int v4, v0

    .line 43
    aput v4, p2, v6

    .line 44
    .line 45
    ushr-long/2addr v0, p4

    .line 46
    add-int/lit8 v4, p1, 0x2

    .line 47
    .line 48
    aget v4, p0, v4

    .line 49
    .line 50
    int-to-long v4, v4

    .line 51
    and-long/2addr v4, v2

    .line 52
    add-int/lit8 v6, p3, 0x2

    .line 53
    .line 54
    aget v7, p2, v6

    .line 55
    .line 56
    int-to-long v7, v7

    .line 57
    and-long/2addr v7, v2

    .line 58
    add-long/2addr v4, v7

    .line 59
    add-long/2addr v0, v4

    .line 60
    long-to-int v4, v0

    .line 61
    aput v4, p2, v6

    .line 62
    .line 63
    ushr-long/2addr v0, p4

    .line 64
    add-int/lit8 v4, p1, 0x3

    .line 65
    .line 66
    aget v4, p0, v4

    .line 67
    .line 68
    int-to-long v4, v4

    .line 69
    and-long/2addr v4, v2

    .line 70
    add-int/lit8 v6, p3, 0x3

    .line 71
    .line 72
    aget v7, p2, v6

    .line 73
    .line 74
    int-to-long v7, v7

    .line 75
    and-long/2addr v7, v2

    .line 76
    add-long/2addr v4, v7

    .line 77
    add-long/2addr v0, v4

    .line 78
    long-to-int v4, v0

    .line 79
    aput v4, p2, v6

    .line 80
    .line 81
    ushr-long/2addr v0, p4

    .line 82
    add-int/lit8 v4, p1, 0x4

    .line 83
    .line 84
    aget v4, p0, v4

    .line 85
    .line 86
    int-to-long v4, v4

    .line 87
    and-long/2addr v4, v2

    .line 88
    add-int/lit8 v6, p3, 0x4

    .line 89
    .line 90
    aget v7, p2, v6

    .line 91
    .line 92
    int-to-long v7, v7

    .line 93
    and-long/2addr v7, v2

    .line 94
    add-long/2addr v4, v7

    .line 95
    add-long/2addr v0, v4

    .line 96
    long-to-int v4, v0

    .line 97
    aput v4, p2, v6

    .line 98
    .line 99
    ushr-long/2addr v0, p4

    .line 100
    add-int/lit8 v4, p1, 0x5

    .line 101
    .line 102
    aget v4, p0, v4

    .line 103
    .line 104
    int-to-long v4, v4

    .line 105
    and-long/2addr v4, v2

    .line 106
    add-int/lit8 v6, p3, 0x5

    .line 107
    .line 108
    aget v7, p2, v6

    .line 109
    .line 110
    int-to-long v7, v7

    .line 111
    and-long/2addr v7, v2

    .line 112
    add-long/2addr v4, v7

    .line 113
    add-long/2addr v0, v4

    .line 114
    long-to-int v4, v0

    .line 115
    aput v4, p2, v6

    .line 116
    .line 117
    ushr-long/2addr v0, p4

    .line 118
    add-int/lit8 v4, p1, 0x6

    .line 119
    .line 120
    aget v4, p0, v4

    .line 121
    .line 122
    int-to-long v4, v4

    .line 123
    and-long/2addr v4, v2

    .line 124
    add-int/lit8 v6, p3, 0x6

    .line 125
    .line 126
    aget v7, p2, v6

    .line 127
    .line 128
    int-to-long v7, v7

    .line 129
    and-long/2addr v7, v2

    .line 130
    add-long/2addr v4, v7

    .line 131
    add-long/2addr v0, v4

    .line 132
    long-to-int v4, v0

    .line 133
    aput v4, p2, v6

    .line 134
    .line 135
    ushr-long/2addr v0, p4

    .line 136
    add-int/lit8 p1, p1, 0x7

    .line 137
    .line 138
    aget p0, p0, p1

    .line 139
    .line 140
    int-to-long p0, p0

    .line 141
    and-long/2addr p0, v2

    .line 142
    add-int/lit8 p3, p3, 0x7

    .line 143
    .line 144
    aget v4, p2, p3

    .line 145
    .line 146
    int-to-long v4, v4

    .line 147
    and-long/2addr v2, v4

    .line 148
    add-long/2addr p0, v2

    .line 149
    add-long/2addr v0, p0

    .line 150
    long-to-int p0, v0

    .line 151
    aput p0, p2, p3

    .line 152
    .line 153
    ushr-long p0, v0, p4

    .line 154
    .line 155
    long-to-int p1, p0

    .line 156
    return p1
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method public static 〇〇808〇([II)I
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    aget p0, p0, v0

    .line 5
    .line 6
    :goto_0
    and-int/lit8 p0, p0, 0x1

    .line 7
    .line 8
    return p0

    .line 9
    :cond_0
    and-int/lit16 v1, p1, 0xff

    .line 10
    .line 11
    if-eq v1, p1, :cond_1

    .line 12
    .line 13
    return v0

    .line 14
    :cond_1
    ushr-int/lit8 v0, p1, 0x5

    .line 15
    .line 16
    and-int/lit8 p1, p1, 0x1f

    .line 17
    .line 18
    aget p0, p0, v0

    .line 19
    .line 20
    ushr-int/2addr p0, p1

    .line 21
    goto :goto_0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇〇888()[J
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v0, v0, [J

    .line 3
    .line 4
    return-object v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static 〇〇8O0〇8([I)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p0, v0

    .line 3
    .line 4
    const/4 v2, 0x1

    .line 5
    if-eq v1, v2, :cond_0

    .line 6
    .line 7
    return v0

    .line 8
    :cond_0
    const/4 v1, 0x1

    .line 9
    :goto_0
    const/16 v3, 0x8

    .line 10
    .line 11
    if-ge v1, v3, :cond_2

    .line 12
    .line 13
    aget v3, p0, v1

    .line 14
    .line 15
    if-eqz v3, :cond_1

    .line 16
    .line 17
    return v0

    .line 18
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_2
    return v2
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

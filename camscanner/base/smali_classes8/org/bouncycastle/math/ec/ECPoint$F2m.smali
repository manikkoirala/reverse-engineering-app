.class public Lorg/bouncycastle/math/ec/ECPoint$F2m;
.super Lorg/bouncycastle/math/ec/ECPoint$AbstractF2m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/bouncycastle/math/ec/ECPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "F2m"
.end annotation


# direct methods
.method public constructor <init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    return-void
.end method

.method public constructor <init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V
    .locals 3

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lorg/bouncycastle/math/ec/ECPoint$AbstractF2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-ne v2, v0, :cond_3

    if-eqz p2, :cond_2

    iget-object p2, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o00〇〇Oo:Lorg/bouncycastle/math/ec/ECFieldElement;

    iget-object p3, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    invoke-static {p2, p3}, Lorg/bouncycastle/math/ec/ECFieldElement$F2m;->OoO8(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)V

    if-eqz p1, :cond_2

    iget-object p1, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o00〇〇Oo:Lorg/bouncycastle/math/ec/ECFieldElement;

    iget-object p2, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇080:Lorg/bouncycastle/math/ec/ECCurve;

    invoke-virtual {p2}, Lorg/bouncycastle/math/ec/ECCurve;->〇8o8o〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    move-result-object p2

    invoke-static {p1, p2}, Lorg/bouncycastle/math/ec/ECFieldElement$F2m;->OoO8(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)V

    :cond_2
    iput-boolean p4, p0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Exactly one of the field elements is null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method constructor <init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;[Lorg/bouncycastle/math/ec/ECFieldElement;Z)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/bouncycastle/math/ec/ECPoint$AbstractF2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;[Lorg/bouncycastle/math/ec/ECFieldElement;)V

    iput-boolean p5, p0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    return-void
.end method


# virtual methods
.method protected O8()Lorg/bouncycastle/math/ec/ECPoint;
    .locals 4

    .line 1
    new-instance v0, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 2
    .line 3
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->o〇0()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->〇〇888()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    const/4 v3, 0x0

    .line 12
    invoke-direct {v0, v3, v1, v2}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public O〇8O8〇008()Lorg/bouncycastle/math/ec/ECPoint;
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lorg/bouncycastle/math/ec/ECPoint;->〇0〇O0088o()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    return-object v0

    .line 10
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/bouncycastle/math/ec/ECPoint;->〇80〇808〇O()Lorg/bouncycastle/math/ec/ECCurve;

    .line 11
    .line 12
    .line 13
    move-result-object v3

    .line 14
    iget-object v1, v0, Lorg/bouncycastle/math/ec/ECPoint;->〇o00〇〇Oo:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 15
    .line 16
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-eqz v2, :cond_1

    .line 21
    .line 22
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECCurve;->〇O00()Lorg/bouncycastle/math/ec/ECPoint;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    return-object v1

    .line 27
    :cond_1
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECCurve;->Oooo8o0〇()I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_10

    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    const/4 v5, 0x1

    .line 35
    if-eq v2, v5, :cond_d

    .line 36
    .line 37
    const/4 v6, 0x6

    .line 38
    if-ne v2, v6, :cond_c

    .line 39
    .line 40
    iget-object v2, v0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 41
    .line 42
    iget-object v6, v0, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 43
    .line 44
    aget-object v6, v6, v4

    .line 45
    .line 46
    invoke-virtual {v6}, Lorg/bouncycastle/math/ec/ECFieldElement;->oO80()Z

    .line 47
    .line 48
    .line 49
    move-result v7

    .line 50
    if-eqz v7, :cond_2

    .line 51
    .line 52
    move-object v8, v2

    .line 53
    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {v2, v6}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 55
    .line 56
    .line 57
    move-result-object v8

    .line 58
    :goto_0
    if-eqz v7, :cond_3

    .line 59
    .line 60
    move-object v9, v6

    .line 61
    goto :goto_1

    .line 62
    :cond_3
    invoke-virtual {v6}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 63
    .line 64
    .line 65
    move-result-object v9

    .line 66
    :goto_1
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECCurve;->〇8o8o〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 67
    .line 68
    .line 69
    move-result-object v10

    .line 70
    if-eqz v7, :cond_4

    .line 71
    .line 72
    move-object v11, v10

    .line 73
    goto :goto_2

    .line 74
    :cond_4
    invoke-virtual {v10, v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 75
    .line 76
    .line 77
    move-result-object v11

    .line 78
    :goto_2
    invoke-virtual {v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 79
    .line 80
    .line 81
    move-result-object v12

    .line 82
    invoke-virtual {v12, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 83
    .line 84
    .line 85
    move-result-object v12

    .line 86
    invoke-virtual {v12, v11}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 87
    .line 88
    .line 89
    move-result-object v12

    .line 90
    invoke-virtual {v12}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 91
    .line 92
    .line 93
    move-result v13

    .line 94
    if-eqz v13, :cond_5

    .line 95
    .line 96
    new-instance v1, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 97
    .line 98
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECCurve;->〇O8o08O()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    invoke-virtual {v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->Oooo8o0〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    iget-boolean v4, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 107
    .line 108
    invoke-direct {v1, v3, v12, v2, v4}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 109
    .line 110
    .line 111
    return-object v1

    .line 112
    :cond_5
    invoke-virtual {v12}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 113
    .line 114
    .line 115
    move-result-object v13

    .line 116
    if-eqz v7, :cond_6

    .line 117
    .line 118
    move-object v14, v12

    .line 119
    goto :goto_3

    .line 120
    :cond_6
    invoke-virtual {v12, v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 121
    .line 122
    .line 123
    move-result-object v14

    .line 124
    :goto_3
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECCurve;->〇O8o08O()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 125
    .line 126
    .line 127
    move-result-object v15

    .line 128
    invoke-virtual {v15}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇o〇()I

    .line 129
    .line 130
    .line 131
    move-result v4

    .line 132
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECCurve;->〇O〇()I

    .line 133
    .line 134
    .line 135
    move-result v16

    .line 136
    move-object/from16 v17, v3

    .line 137
    .line 138
    shr-int/lit8 v3, v16, 0x1

    .line 139
    .line 140
    if-ge v4, v3, :cond_9

    .line 141
    .line 142
    invoke-virtual {v2, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    invoke-virtual {v15}, Lorg/bouncycastle/math/ec/ECFieldElement;->oO80()Z

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    if-eqz v2, :cond_7

    .line 155
    .line 156
    invoke-virtual {v11, v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 157
    .line 158
    .line 159
    move-result-object v2

    .line 160
    invoke-virtual {v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 161
    .line 162
    .line 163
    move-result-object v2

    .line 164
    goto :goto_4

    .line 165
    :cond_7
    invoke-virtual {v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 166
    .line 167
    .line 168
    move-result-object v2

    .line 169
    invoke-virtual {v11, v15, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O〇(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 170
    .line 171
    .line 172
    move-result-object v2

    .line 173
    :goto_4
    invoke-virtual {v1, v12}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 174
    .line 175
    .line 176
    move-result-object v3

    .line 177
    invoke-virtual {v3, v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 178
    .line 179
    .line 180
    move-result-object v3

    .line 181
    invoke-virtual {v3, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 182
    .line 183
    .line 184
    move-result-object v1

    .line 185
    invoke-virtual {v1, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 186
    .line 187
    .line 188
    move-result-object v1

    .line 189
    invoke-virtual {v1, v13}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 190
    .line 191
    .line 192
    move-result-object v1

    .line 193
    invoke-virtual {v10}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 194
    .line 195
    .line 196
    move-result v2

    .line 197
    if-eqz v2, :cond_8

    .line 198
    .line 199
    goto :goto_6

    .line 200
    :cond_8
    invoke-virtual {v10}, Lorg/bouncycastle/math/ec/ECFieldElement;->oO80()Z

    .line 201
    .line 202
    .line 203
    move-result v2

    .line 204
    if-nez v2, :cond_b

    .line 205
    .line 206
    invoke-virtual {v10}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇o00〇〇Oo()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 207
    .line 208
    .line 209
    move-result-object v2

    .line 210
    invoke-virtual {v2, v14}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 211
    .line 212
    .line 213
    move-result-object v2

    .line 214
    invoke-virtual {v1, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 215
    .line 216
    .line 217
    move-result-object v1

    .line 218
    goto :goto_7

    .line 219
    :cond_9
    if-eqz v7, :cond_a

    .line 220
    .line 221
    goto :goto_5

    .line 222
    :cond_a
    invoke-virtual {v1, v6}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 223
    .line 224
    .line 225
    move-result-object v1

    .line 226
    :goto_5
    invoke-virtual {v1, v12, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O〇(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 227
    .line 228
    .line 229
    move-result-object v1

    .line 230
    invoke-virtual {v1, v13}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 231
    .line 232
    .line 233
    move-result-object v1

    .line 234
    :goto_6
    invoke-virtual {v1, v14}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 235
    .line 236
    .line 237
    move-result-object v1

    .line 238
    :cond_b
    :goto_7
    new-instance v8, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 239
    .line 240
    new-array v6, v5, [Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 241
    .line 242
    const/4 v2, 0x0

    .line 243
    aput-object v14, v6, v2

    .line 244
    .line 245
    iget-boolean v7, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 246
    .line 247
    move-object v2, v8

    .line 248
    move-object/from16 v3, v17

    .line 249
    .line 250
    move-object v4, v13

    .line 251
    move-object v5, v1

    .line 252
    invoke-direct/range {v2 .. v7}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;[Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 253
    .line 254
    .line 255
    return-object v8

    .line 256
    :cond_c
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 257
    .line 258
    const-string v2, "unsupported coordinate system"

    .line 259
    .line 260
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 261
    .line 262
    .line 263
    throw v1

    .line 264
    :cond_d
    move-object/from16 v17, v3

    .line 265
    .line 266
    iget-object v2, v0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 267
    .line 268
    iget-object v3, v0, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 269
    .line 270
    const/4 v4, 0x0

    .line 271
    aget-object v3, v3, v4

    .line 272
    .line 273
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->oO80()Z

    .line 274
    .line 275
    .line 276
    move-result v4

    .line 277
    if-eqz v4, :cond_e

    .line 278
    .line 279
    move-object v6, v1

    .line 280
    goto :goto_8

    .line 281
    :cond_e
    invoke-virtual {v1, v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 282
    .line 283
    .line 284
    move-result-object v6

    .line 285
    :goto_8
    if-eqz v4, :cond_f

    .line 286
    .line 287
    goto :goto_9

    .line 288
    :cond_f
    invoke-virtual {v2, v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 289
    .line 290
    .line 291
    move-result-object v2

    .line 292
    :goto_9
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 293
    .line 294
    .line 295
    move-result-object v1

    .line 296
    invoke-virtual {v1, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 297
    .line 298
    .line 299
    move-result-object v2

    .line 300
    invoke-virtual {v6}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 301
    .line 302
    .line 303
    move-result-object v3

    .line 304
    invoke-virtual {v2, v6}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 305
    .line 306
    .line 307
    move-result-object v4

    .line 308
    invoke-virtual/range {v17 .. v17}, Lorg/bouncycastle/math/ec/ECCurve;->〇8o8o〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 309
    .line 310
    .line 311
    move-result-object v7

    .line 312
    invoke-virtual {v4, v2, v3, v7}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O8o08O(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 313
    .line 314
    .line 315
    move-result-object v2

    .line 316
    invoke-virtual {v6, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 317
    .line 318
    .line 319
    move-result-object v7

    .line 320
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 321
    .line 322
    .line 323
    move-result-object v1

    .line 324
    invoke-virtual {v1, v6, v2, v4}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O8o08O(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 325
    .line 326
    .line 327
    move-result-object v1

    .line 328
    invoke-virtual {v6, v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 329
    .line 330
    .line 331
    move-result-object v2

    .line 332
    new-instance v8, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 333
    .line 334
    new-array v6, v5, [Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 335
    .line 336
    const/4 v3, 0x0

    .line 337
    aput-object v2, v6, v3

    .line 338
    .line 339
    iget-boolean v9, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 340
    .line 341
    move-object v2, v8

    .line 342
    move-object/from16 v3, v17

    .line 343
    .line 344
    move-object v4, v7

    .line 345
    move-object v5, v1

    .line 346
    move v7, v9

    .line 347
    invoke-direct/range {v2 .. v7}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;[Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 348
    .line 349
    .line 350
    return-object v8

    .line 351
    :cond_10
    move-object/from16 v17, v3

    .line 352
    .line 353
    iget-object v2, v0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 354
    .line 355
    invoke-virtual {v2, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->O8(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 356
    .line 357
    .line 358
    move-result-object v2

    .line 359
    invoke-virtual {v2, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 360
    .line 361
    .line 362
    move-result-object v2

    .line 363
    invoke-virtual {v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 364
    .line 365
    .line 366
    move-result-object v3

    .line 367
    invoke-virtual {v3, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 368
    .line 369
    .line 370
    move-result-object v3

    .line 371
    invoke-virtual/range {v17 .. v17}, Lorg/bouncycastle/math/ec/ECCurve;->〇8o8o〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 372
    .line 373
    .line 374
    move-result-object v4

    .line 375
    invoke-virtual {v3, v4}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 376
    .line 377
    .line 378
    move-result-object v3

    .line 379
    invoke-virtual {v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇o00〇〇Oo()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 380
    .line 381
    .line 382
    move-result-object v2

    .line 383
    invoke-virtual {v1, v3, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O〇(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 384
    .line 385
    .line 386
    move-result-object v1

    .line 387
    new-instance v2, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 388
    .line 389
    iget-boolean v4, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 390
    .line 391
    move-object/from16 v5, v17

    .line 392
    .line 393
    invoke-direct {v2, v5, v3, v1, v4}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 394
    .line 395
    .line 396
    return-object v2
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method protected oO80()Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->Oooo8o0〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    return v2

    .line 13
    :cond_0
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->OO0o〇〇〇〇0()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    const/4 v4, 0x5

    .line 22
    if-eq v3, v4, :cond_1

    .line 23
    .line 24
    const/4 v4, 0x6

    .line 25
    if-eq v3, v4, :cond_1

    .line 26
    .line 27
    invoke-virtual {v1, v0}, Lorg/bouncycastle/math/ec/ECFieldElement;->O8(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇8O0〇8()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    return v0

    .line 36
    :cond_1
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇8O0〇8()Z

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    invoke-virtual {v0}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇8O0〇8()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-eq v1, v0, :cond_2

    .line 45
    .line 46
    const/4 v2, 0x1

    .line 47
    :cond_2
    return v2
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public 〇080(Lorg/bouncycastle/math/ec/ECPoint;)Lorg/bouncycastle/math/ec/ECPoint;
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    invoke-virtual/range {p0 .. p0}, Lorg/bouncycastle/math/ec/ECPoint;->〇0〇O0088o()Z

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    return-object v1

    .line 12
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/bouncycastle/math/ec/ECPoint;->〇0〇O0088o()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_1

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/bouncycastle/math/ec/ECPoint;->〇80〇808〇O()Lorg/bouncycastle/math/ec/ECCurve;

    .line 20
    .line 21
    .line 22
    move-result-object v4

    .line 23
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->Oooo8o0〇()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    iget-object v3, v0, Lorg/bouncycastle/math/ec/ECPoint;->〇o00〇〇Oo:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 28
    .line 29
    iget-object v5, v1, Lorg/bouncycastle/math/ec/ECPoint;->〇o00〇〇Oo:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 30
    .line 31
    if-eqz v2, :cond_15

    .line 32
    .line 33
    const/4 v6, 0x1

    .line 34
    const/4 v7, 0x0

    .line 35
    if-eq v2, v6, :cond_e

    .line 36
    .line 37
    const/4 v8, 0x6

    .line 38
    if-ne v2, v8, :cond_d

    .line 39
    .line 40
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    if-eqz v2, :cond_3

    .line 45
    .line 46
    invoke-virtual {v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    if-eqz v2, :cond_2

    .line 51
    .line 52
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇O00()Lorg/bouncycastle/math/ec/ECPoint;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    return-object v1

    .line 57
    :cond_2
    invoke-virtual {v1, v0}, Lorg/bouncycastle/math/ec/ECPoint;->〇080(Lorg/bouncycastle/math/ec/ECPoint;)Lorg/bouncycastle/math/ec/ECPoint;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    return-object v1

    .line 62
    :cond_3
    iget-object v2, v0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 63
    .line 64
    iget-object v8, v0, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 65
    .line 66
    aget-object v8, v8, v7

    .line 67
    .line 68
    iget-object v9, v1, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 69
    .line 70
    iget-object v1, v1, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 71
    .line 72
    aget-object v1, v1, v7

    .line 73
    .line 74
    invoke-virtual {v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->oO80()Z

    .line 75
    .line 76
    .line 77
    move-result v10

    .line 78
    if-nez v10, :cond_4

    .line 79
    .line 80
    invoke-virtual {v5, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 81
    .line 82
    .line 83
    move-result-object v11

    .line 84
    invoke-virtual {v9, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 85
    .line 86
    .line 87
    move-result-object v12

    .line 88
    goto :goto_0

    .line 89
    :cond_4
    move-object v11, v5

    .line 90
    move-object v12, v9

    .line 91
    :goto_0
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->oO80()Z

    .line 92
    .line 93
    .line 94
    move-result v13

    .line 95
    if-nez v13, :cond_5

    .line 96
    .line 97
    invoke-virtual {v3, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 98
    .line 99
    .line 100
    move-result-object v3

    .line 101
    invoke-virtual {v2, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 102
    .line 103
    .line 104
    move-result-object v14

    .line 105
    goto :goto_1

    .line 106
    :cond_5
    move-object v14, v2

    .line 107
    :goto_1
    invoke-virtual {v14, v12}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 108
    .line 109
    .line 110
    move-result-object v12

    .line 111
    invoke-virtual {v3, v11}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 112
    .line 113
    .line 114
    move-result-object v14

    .line 115
    invoke-virtual {v14}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 116
    .line 117
    .line 118
    move-result v15

    .line 119
    if-eqz v15, :cond_7

    .line 120
    .line 121
    invoke-virtual {v12}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 122
    .line 123
    .line 124
    move-result v1

    .line 125
    if-eqz v1, :cond_6

    .line 126
    .line 127
    invoke-virtual/range {p0 .. p0}, Lorg/bouncycastle/math/ec/ECPoint$F2m;->O〇8O8〇008()Lorg/bouncycastle/math/ec/ECPoint;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    return-object v1

    .line 132
    :cond_6
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇O00()Lorg/bouncycastle/math/ec/ECPoint;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    return-object v1

    .line 137
    :cond_7
    invoke-virtual {v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 138
    .line 139
    .line 140
    move-result v5

    .line 141
    if-eqz v5, :cond_9

    .line 142
    .line 143
    invoke-virtual/range {p0 .. p0}, Lorg/bouncycastle/math/ec/ECPoint;->oo88o8O()Lorg/bouncycastle/math/ec/ECPoint;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECPoint;->〇O〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 148
    .line 149
    .line 150
    move-result-object v2

    .line 151
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECPoint;->〇O00()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 152
    .line 153
    .line 154
    move-result-object v1

    .line 155
    invoke-virtual {v1, v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 156
    .line 157
    .line 158
    move-result-object v3

    .line 159
    invoke-virtual {v3, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->O8(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 160
    .line 161
    .line 162
    move-result-object v3

    .line 163
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 164
    .line 165
    .line 166
    move-result-object v5

    .line 167
    invoke-virtual {v5, v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 168
    .line 169
    .line 170
    move-result-object v5

    .line 171
    invoke-virtual {v5, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 172
    .line 173
    .line 174
    move-result-object v5

    .line 175
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇8o8o〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 176
    .line 177
    .line 178
    move-result-object v8

    .line 179
    invoke-virtual {v5, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 180
    .line 181
    .line 182
    move-result-object v5

    .line 183
    invoke-virtual {v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 184
    .line 185
    .line 186
    move-result v8

    .line 187
    if-eqz v8, :cond_8

    .line 188
    .line 189
    new-instance v1, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 190
    .line 191
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇O8o08O()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 192
    .line 193
    .line 194
    move-result-object v2

    .line 195
    invoke-virtual {v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->Oooo8o0〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 196
    .line 197
    .line 198
    move-result-object v2

    .line 199
    iget-boolean v3, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 200
    .line 201
    invoke-direct {v1, v4, v5, v2, v3}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 202
    .line 203
    .line 204
    return-object v1

    .line 205
    :cond_8
    invoke-virtual {v2, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 206
    .line 207
    .line 208
    move-result-object v2

    .line 209
    invoke-virtual {v3, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 210
    .line 211
    .line 212
    move-result-object v2

    .line 213
    invoke-virtual {v2, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 214
    .line 215
    .line 216
    move-result-object v2

    .line 217
    invoke-virtual {v2, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 218
    .line 219
    .line 220
    move-result-object v1

    .line 221
    invoke-virtual {v1, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->O8(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 222
    .line 223
    .line 224
    move-result-object v1

    .line 225
    invoke-virtual {v1, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 226
    .line 227
    .line 228
    move-result-object v1

    .line 229
    sget-object v2, Lorg/bouncycastle/math/ec/ECConstants;->〇o00〇〇Oo:Ljava/math/BigInteger;

    .line 230
    .line 231
    invoke-virtual {v4, v2}, Lorg/bouncycastle/math/ec/ECCurve;->OO0o〇〇〇〇0(Ljava/math/BigInteger;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 232
    .line 233
    .line 234
    move-result-object v2

    .line 235
    goto :goto_3

    .line 236
    :cond_9
    invoke-virtual {v14}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 237
    .line 238
    .line 239
    move-result-object v5

    .line 240
    invoke-virtual {v12, v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 241
    .line 242
    .line 243
    move-result-object v3

    .line 244
    invoke-virtual {v12, v11}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 245
    .line 246
    .line 247
    move-result-object v9

    .line 248
    invoke-virtual {v3, v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 249
    .line 250
    .line 251
    move-result-object v3

    .line 252
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 253
    .line 254
    .line 255
    move-result v11

    .line 256
    if-eqz v11, :cond_a

    .line 257
    .line 258
    new-instance v1, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 259
    .line 260
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇O8o08O()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 261
    .line 262
    .line 263
    move-result-object v2

    .line 264
    invoke-virtual {v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->Oooo8o0〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 265
    .line 266
    .line 267
    move-result-object v2

    .line 268
    iget-boolean v5, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 269
    .line 270
    invoke-direct {v1, v4, v3, v2, v5}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 271
    .line 272
    .line 273
    return-object v1

    .line 274
    :cond_a
    invoke-virtual {v12, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 275
    .line 276
    .line 277
    move-result-object v11

    .line 278
    if-nez v13, :cond_b

    .line 279
    .line 280
    invoke-virtual {v11, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 281
    .line 282
    .line 283
    move-result-object v1

    .line 284
    goto :goto_2

    .line 285
    :cond_b
    move-object v1, v11

    .line 286
    :goto_2
    invoke-virtual {v9, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 287
    .line 288
    .line 289
    move-result-object v5

    .line 290
    invoke-virtual {v2, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 291
    .line 292
    .line 293
    move-result-object v2

    .line 294
    invoke-virtual {v5, v1, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O〇(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 295
    .line 296
    .line 297
    move-result-object v2

    .line 298
    if-nez v10, :cond_c

    .line 299
    .line 300
    invoke-virtual {v1, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 301
    .line 302
    .line 303
    move-result-object v1

    .line 304
    :cond_c
    move-object v5, v3

    .line 305
    move-object/from16 v16, v2

    .line 306
    .line 307
    move-object v2, v1

    .line 308
    move-object/from16 v1, v16

    .line 309
    .line 310
    :goto_3
    new-instance v9, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 311
    .line 312
    new-array v8, v6, [Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 313
    .line 314
    aput-object v2, v8, v7

    .line 315
    .line 316
    iget-boolean v2, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 317
    .line 318
    move-object v3, v9

    .line 319
    move-object v6, v1

    .line 320
    move-object v7, v8

    .line 321
    move v8, v2

    .line 322
    invoke-direct/range {v3 .. v8}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;[Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 323
    .line 324
    .line 325
    return-object v9

    .line 326
    :cond_d
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 327
    .line 328
    const-string v2, "unsupported coordinate system"

    .line 329
    .line 330
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 331
    .line 332
    .line 333
    throw v1

    .line 334
    :cond_e
    iget-object v2, v0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 335
    .line 336
    iget-object v8, v0, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 337
    .line 338
    aget-object v8, v8, v7

    .line 339
    .line 340
    iget-object v9, v1, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 341
    .line 342
    iget-object v1, v1, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 343
    .line 344
    aget-object v1, v1, v7

    .line 345
    .line 346
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->oO80()Z

    .line 347
    .line 348
    .line 349
    move-result v10

    .line 350
    invoke-virtual {v8, v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 351
    .line 352
    .line 353
    move-result-object v9

    .line 354
    if-eqz v10, :cond_f

    .line 355
    .line 356
    move-object v11, v2

    .line 357
    goto :goto_4

    .line 358
    :cond_f
    invoke-virtual {v2, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 359
    .line 360
    .line 361
    move-result-object v11

    .line 362
    :goto_4
    invoke-virtual {v9, v11}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 363
    .line 364
    .line 365
    move-result-object v9

    .line 366
    invoke-virtual {v8, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 367
    .line 368
    .line 369
    move-result-object v5

    .line 370
    if-eqz v10, :cond_10

    .line 371
    .line 372
    move-object v11, v3

    .line 373
    goto :goto_5

    .line 374
    :cond_10
    invoke-virtual {v3, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 375
    .line 376
    .line 377
    move-result-object v11

    .line 378
    :goto_5
    invoke-virtual {v5, v11}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 379
    .line 380
    .line 381
    move-result-object v5

    .line 382
    invoke-virtual {v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 383
    .line 384
    .line 385
    move-result v11

    .line 386
    if-eqz v11, :cond_12

    .line 387
    .line 388
    invoke-virtual {v9}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 389
    .line 390
    .line 391
    move-result v1

    .line 392
    if-eqz v1, :cond_11

    .line 393
    .line 394
    invoke-virtual/range {p0 .. p0}, Lorg/bouncycastle/math/ec/ECPoint$F2m;->O〇8O8〇008()Lorg/bouncycastle/math/ec/ECPoint;

    .line 395
    .line 396
    .line 397
    move-result-object v1

    .line 398
    return-object v1

    .line 399
    :cond_11
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇O00()Lorg/bouncycastle/math/ec/ECPoint;

    .line 400
    .line 401
    .line 402
    move-result-object v1

    .line 403
    return-object v1

    .line 404
    :cond_12
    invoke-virtual {v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 405
    .line 406
    .line 407
    move-result-object v11

    .line 408
    invoke-virtual {v11, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 409
    .line 410
    .line 411
    move-result-object v12

    .line 412
    if-eqz v10, :cond_13

    .line 413
    .line 414
    goto :goto_6

    .line 415
    :cond_13
    invoke-virtual {v8, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 416
    .line 417
    .line 418
    move-result-object v8

    .line 419
    :goto_6
    invoke-virtual {v9, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 420
    .line 421
    .line 422
    move-result-object v13

    .line 423
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇8o8o〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 424
    .line 425
    .line 426
    move-result-object v14

    .line 427
    invoke-virtual {v13, v9, v11, v14}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O8o08O(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 428
    .line 429
    .line 430
    move-result-object v14

    .line 431
    invoke-virtual {v14, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 432
    .line 433
    .line 434
    move-result-object v14

    .line 435
    invoke-virtual {v14, v12}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 436
    .line 437
    .line 438
    move-result-object v14

    .line 439
    invoke-virtual {v5, v14}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 440
    .line 441
    .line 442
    move-result-object v15

    .line 443
    if-eqz v10, :cond_14

    .line 444
    .line 445
    goto :goto_7

    .line 446
    :cond_14
    invoke-virtual {v11, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 447
    .line 448
    .line 449
    move-result-object v11

    .line 450
    :goto_7
    invoke-virtual {v9, v3, v5, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O8o08O(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 451
    .line 452
    .line 453
    move-result-object v1

    .line 454
    invoke-virtual {v1, v11, v13, v14}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇O8o08O(Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 455
    .line 456
    .line 457
    move-result-object v1

    .line 458
    invoke-virtual {v12, v8}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 459
    .line 460
    .line 461
    move-result-object v2

    .line 462
    new-instance v9, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 463
    .line 464
    new-array v8, v6, [Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 465
    .line 466
    aput-object v2, v8, v7

    .line 467
    .line 468
    iget-boolean v2, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 469
    .line 470
    move-object v3, v9

    .line 471
    move-object v5, v15

    .line 472
    move-object v6, v1

    .line 473
    move-object v7, v8

    .line 474
    move v8, v2

    .line 475
    invoke-direct/range {v3 .. v8}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;[Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 476
    .line 477
    .line 478
    return-object v9

    .line 479
    :cond_15
    iget-object v2, v0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 480
    .line 481
    iget-object v1, v1, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 482
    .line 483
    invoke-virtual {v3, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 484
    .line 485
    .line 486
    move-result-object v5

    .line 487
    invoke-virtual {v2, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 488
    .line 489
    .line 490
    move-result-object v1

    .line 491
    invoke-virtual {v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 492
    .line 493
    .line 494
    move-result v6

    .line 495
    if-eqz v6, :cond_17

    .line 496
    .line 497
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 498
    .line 499
    .line 500
    move-result v1

    .line 501
    if-eqz v1, :cond_16

    .line 502
    .line 503
    invoke-virtual/range {p0 .. p0}, Lorg/bouncycastle/math/ec/ECPoint$F2m;->O〇8O8〇008()Lorg/bouncycastle/math/ec/ECPoint;

    .line 504
    .line 505
    .line 506
    move-result-object v1

    .line 507
    return-object v1

    .line 508
    :cond_16
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇O00()Lorg/bouncycastle/math/ec/ECPoint;

    .line 509
    .line 510
    .line 511
    move-result-object v1

    .line 512
    return-object v1

    .line 513
    :cond_17
    invoke-virtual {v1, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->O8(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 514
    .line 515
    .line 516
    move-result-object v1

    .line 517
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇〇808〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 518
    .line 519
    .line 520
    move-result-object v6

    .line 521
    invoke-virtual {v6, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 522
    .line 523
    .line 524
    move-result-object v6

    .line 525
    invoke-virtual {v6, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 526
    .line 527
    .line 528
    move-result-object v5

    .line 529
    invoke-virtual {v4}, Lorg/bouncycastle/math/ec/ECCurve;->〇8o8o〇()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 530
    .line 531
    .line 532
    move-result-object v6

    .line 533
    invoke-virtual {v5, v6}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 534
    .line 535
    .line 536
    move-result-object v5

    .line 537
    invoke-virtual {v3, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 538
    .line 539
    .line 540
    move-result-object v3

    .line 541
    invoke-virtual {v1, v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 542
    .line 543
    .line 544
    move-result-object v1

    .line 545
    invoke-virtual {v1, v5}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 546
    .line 547
    .line 548
    move-result-object v1

    .line 549
    invoke-virtual {v1, v2}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 550
    .line 551
    .line 552
    move-result-object v1

    .line 553
    new-instance v2, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 554
    .line 555
    iget-boolean v3, v0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 556
    .line 557
    invoke-direct {v2, v4, v5, v1, v3}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 558
    .line 559
    .line 560
    return-object v2
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public 〇O00()Lorg/bouncycastle/math/ec/ECFieldElement;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->OO0o〇〇〇〇0()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x5

    .line 6
    const/4 v2, 0x6

    .line 7
    if-eq v0, v1, :cond_0

    .line 8
    .line 9
    if-eq v0, v2, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 12
    .line 13
    return-object v0

    .line 14
    :cond_0
    iget-object v1, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o00〇〇Oo:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 15
    .line 16
    iget-object v3, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 17
    .line 18
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->〇0〇O0088o()Z

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    if-nez v4, :cond_3

    .line 23
    .line 24
    invoke-virtual {v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    if-eqz v4, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    invoke-virtual {v3, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-virtual {v3, v1}, Lorg/bouncycastle/math/ec/ECFieldElement;->OO0o〇〇〇〇0(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    if-ne v2, v0, :cond_2

    .line 40
    .line 41
    iget-object v0, p0, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 42
    .line 43
    const/4 v2, 0x0

    .line 44
    aget-object v0, v0, v2

    .line 45
    .line 46
    invoke-virtual {v0}, Lorg/bouncycastle/math/ec/ECFieldElement;->oO80()Z

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    if-nez v2, :cond_2

    .line 51
    .line 52
    invoke-virtual {v1, v0}, Lorg/bouncycastle/math/ec/ECFieldElement;->O8(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    :cond_2
    return-object v1

    .line 57
    :cond_3
    :goto_0
    return-object v3
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public 〇O888o0o()Lorg/bouncycastle/math/ec/ECPoint;
    .locals 9

    .line 1
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->〇0〇O0088o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-object p0

    .line 8
    :cond_0
    iget-object v3, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o00〇〇Oo:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 9
    .line 10
    invoke-virtual {v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇80〇808〇O()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    return-object p0

    .line 17
    :cond_1
    invoke-virtual {p0}, Lorg/bouncycastle/math/ec/ECPoint;->OO0o〇〇〇〇0()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_5

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    const/4 v2, 0x0

    .line 25
    if-eq v0, v1, :cond_4

    .line 26
    .line 27
    const/4 v4, 0x5

    .line 28
    if-eq v0, v4, :cond_3

    .line 29
    .line 30
    const/4 v4, 0x6

    .line 31
    if-ne v0, v4, :cond_2

    .line 32
    .line 33
    iget-object v0, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 34
    .line 35
    iget-object v4, p0, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 36
    .line 37
    aget-object v4, v4, v2

    .line 38
    .line 39
    new-instance v7, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 40
    .line 41
    iget-object v5, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇080:Lorg/bouncycastle/math/ec/ECCurve;

    .line 42
    .line 43
    invoke-virtual {v0, v4}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    new-array v6, v1, [Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 48
    .line 49
    aput-object v4, v6, v2

    .line 50
    .line 51
    iget-boolean v8, p0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 52
    .line 53
    move-object v1, v7

    .line 54
    move-object v2, v5

    .line 55
    move-object v4, v0

    .line 56
    move-object v5, v6

    .line 57
    move v6, v8

    .line 58
    invoke-direct/range {v1 .. v6}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;[Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 59
    .line 60
    .line 61
    return-object v7

    .line 62
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 63
    .line 64
    const-string v1, "unsupported coordinate system"

    .line 65
    .line 66
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    throw v0

    .line 70
    :cond_3
    iget-object v0, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 71
    .line 72
    new-instance v1, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 73
    .line 74
    iget-object v2, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇080:Lorg/bouncycastle/math/ec/ECCurve;

    .line 75
    .line 76
    invoke-virtual {v0}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇o00〇〇Oo()Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    iget-boolean v4, p0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 81
    .line 82
    invoke-direct {v1, v2, v3, v0, v4}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 83
    .line 84
    .line 85
    return-object v1

    .line 86
    :cond_4
    iget-object v0, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 87
    .line 88
    iget-object v4, p0, Lorg/bouncycastle/math/ec/ECPoint;->O8:[Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 89
    .line 90
    aget-object v4, v4, v2

    .line 91
    .line 92
    new-instance v7, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 93
    .line 94
    iget-object v5, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇080:Lorg/bouncycastle/math/ec/ECCurve;

    .line 95
    .line 96
    invoke-virtual {v0, v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    new-array v6, v1, [Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 101
    .line 102
    aput-object v4, v6, v2

    .line 103
    .line 104
    iget-boolean v8, p0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 105
    .line 106
    move-object v1, v7

    .line 107
    move-object v2, v5

    .line 108
    move-object v4, v0

    .line 109
    move-object v5, v6

    .line 110
    move v6, v8

    .line 111
    invoke-direct/range {v1 .. v6}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;[Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 112
    .line 113
    .line 114
    return-object v7

    .line 115
    :cond_5
    iget-object v0, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇o〇:Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 116
    .line 117
    new-instance v1, Lorg/bouncycastle/math/ec/ECPoint$F2m;

    .line 118
    .line 119
    iget-object v2, p0, Lorg/bouncycastle/math/ec/ECPoint;->〇080:Lorg/bouncycastle/math/ec/ECCurve;

    .line 120
    .line 121
    invoke-virtual {v0, v3}, Lorg/bouncycastle/math/ec/ECFieldElement;->〇080(Lorg/bouncycastle/math/ec/ECFieldElement;)Lorg/bouncycastle/math/ec/ECFieldElement;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    iget-boolean v4, p0, Lorg/bouncycastle/math/ec/ECPoint;->Oo08:Z

    .line 126
    .line 127
    invoke-direct {v1, v2, v3, v0, v4}, Lorg/bouncycastle/math/ec/ECPoint$F2m;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECFieldElement;Lorg/bouncycastle/math/ec/ECFieldElement;Z)V

    .line 128
    .line 129
    .line 130
    return-object v1
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.class public interface abstract Lorg/bouncycastle/jce/interfaces/ElGamalPublicKey;
.super Ljava/lang/Object;

# interfaces
.implements Ljavax/crypto/interfaces/DHKey;
.implements Ljavax/crypto/interfaces/DHPublicKey;


# virtual methods
.method public abstract synthetic getParameters()Lorg/bouncycastle/jce/spec/ElGamalParameterSpec;
.end method

.method public abstract getY()Ljava/math/BigInteger;
.end method

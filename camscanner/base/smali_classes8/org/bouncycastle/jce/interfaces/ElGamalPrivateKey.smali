.class public interface abstract Lorg/bouncycastle/jce/interfaces/ElGamalPrivateKey;
.super Ljava/lang/Object;

# interfaces
.implements Ljavax/crypto/interfaces/DHKey;
.implements Ljavax/crypto/interfaces/DHPrivateKey;


# virtual methods
.method public abstract synthetic getParameters()Lorg/bouncycastle/jce/spec/ElGamalParameterSpec;
.end method

.method public abstract getX()Ljava/math/BigInteger;
.end method

.class public Lorg/jacoco/core/analysis/NodeComparator;
.super Ljava/lang/Object;
.source "NodeComparator.java"

# interfaces
.implements Ljava/util/Comparator;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lorg/jacoco/core/analysis/ICoverageNode;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x76a98e21db97e297L


# instance fields
.field private final counterComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lorg/jacoco/core/analysis/ICounter;",
            ">;"
        }
    .end annotation
.end field

.field private final entity:Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;


# direct methods
.method constructor <init>(Ljava/util/Comparator;Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator<",
            "Lorg/jacoco/core/analysis/ICounter;",
            ">;",
            "Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lorg/jacoco/core/analysis/NodeComparator;->counterComparator:Ljava/util/Comparator;

    .line 5
    .line 6
    iput-object p2, p0, Lorg/jacoco/core/analysis/NodeComparator;->entity:Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lorg/jacoco/core/analysis/ICoverageNode;

    check-cast p2, Lorg/jacoco/core/analysis/ICoverageNode;

    invoke-virtual {p0, p1, p2}, Lorg/jacoco/core/analysis/NodeComparator;->compare(Lorg/jacoco/core/analysis/ICoverageNode;Lorg/jacoco/core/analysis/ICoverageNode;)I

    move-result p1

    return p1
.end method

.method public compare(Lorg/jacoco/core/analysis/ICoverageNode;Lorg/jacoco/core/analysis/ICoverageNode;)I
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/jacoco/core/analysis/NodeComparator;->entity:Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;

    invoke-interface {p1, v0}, Lorg/jacoco/core/analysis/ICoverageNode;->〇080(Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;)Lorg/jacoco/core/analysis/ICounter;

    move-result-object p1

    .line 3
    iget-object v0, p0, Lorg/jacoco/core/analysis/NodeComparator;->entity:Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;

    invoke-interface {p2, v0}, Lorg/jacoco/core/analysis/ICoverageNode;->〇080(Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;)Lorg/jacoco/core/analysis/ICounter;

    move-result-object p2

    .line 4
    iget-object v0, p0, Lorg/jacoco/core/analysis/NodeComparator;->counterComparator:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public second(Ljava/util/Comparator;)Lorg/jacoco/core/analysis/NodeComparator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator<",
            "Lorg/jacoco/core/analysis/ICoverageNode;",
            ">;)",
            "Lorg/jacoco/core/analysis/NodeComparator;"
        }
    .end annotation

    .line 1
    new-instance v6, Lorg/jacoco/core/analysis/NodeComparator$1;

    .line 2
    .line 3
    const/4 v2, 0x0

    .line 4
    const/4 v3, 0x0

    .line 5
    move-object v0, v6

    .line 6
    move-object v1, p0

    .line 7
    move-object v4, p0

    .line 8
    move-object v5, p1

    .line 9
    invoke-direct/range {v0 .. v5}, Lorg/jacoco/core/analysis/NodeComparator$1;-><init>(Lorg/jacoco/core/analysis/NodeComparator;Ljava/util/Comparator;Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;Ljava/util/Comparator;Ljava/util/Comparator;)V

    .line 10
    .line 11
    .line 12
    return-object v6
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public sort(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lorg/jacoco/core/analysis/ICoverageNode;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

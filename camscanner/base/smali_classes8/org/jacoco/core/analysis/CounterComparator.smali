.class public Lorg/jacoco/core/analysis/CounterComparator;
.super Ljava/lang/Object;
.source "CounterComparator.java"

# interfaces
.implements Ljava/util/Comparator;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lorg/jacoco/core/analysis/ICounter;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final COVEREDITEMS:Lorg/jacoco/core/analysis/CounterComparator;

.field public static final COVEREDRATIO:Lorg/jacoco/core/analysis/CounterComparator;

.field public static final MISSEDITEMS:Lorg/jacoco/core/analysis/CounterComparator;

.field public static final MISSEDRATIO:Lorg/jacoco/core/analysis/CounterComparator;

.field public static final TOTALITEMS:Lorg/jacoco/core/analysis/CounterComparator;

.field private static final serialVersionUID:J = -0x346c3ea518271ffcL


# instance fields
.field private final reverse:Z

.field private final value:Lorg/jacoco/core/analysis/ICounter$CounterValue;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lorg/jacoco/core/analysis/CounterComparator;

    .line 2
    .line 3
    sget-object v1, Lorg/jacoco/core/analysis/ICounter$CounterValue;->TOTALCOUNT:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lorg/jacoco/core/analysis/CounterComparator;-><init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lorg/jacoco/core/analysis/CounterComparator;->TOTALITEMS:Lorg/jacoco/core/analysis/CounterComparator;

    .line 9
    .line 10
    new-instance v0, Lorg/jacoco/core/analysis/CounterComparator;

    .line 11
    .line 12
    sget-object v1, Lorg/jacoco/core/analysis/ICounter$CounterValue;->COVEREDCOUNT:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    .line 13
    .line 14
    invoke-direct {v0, v1}, Lorg/jacoco/core/analysis/CounterComparator;-><init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;)V

    .line 15
    .line 16
    .line 17
    sput-object v0, Lorg/jacoco/core/analysis/CounterComparator;->COVEREDITEMS:Lorg/jacoco/core/analysis/CounterComparator;

    .line 18
    .line 19
    new-instance v0, Lorg/jacoco/core/analysis/CounterComparator;

    .line 20
    .line 21
    sget-object v1, Lorg/jacoco/core/analysis/ICounter$CounterValue;->MISSEDCOUNT:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    .line 22
    .line 23
    invoke-direct {v0, v1}, Lorg/jacoco/core/analysis/CounterComparator;-><init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;)V

    .line 24
    .line 25
    .line 26
    sput-object v0, Lorg/jacoco/core/analysis/CounterComparator;->MISSEDITEMS:Lorg/jacoco/core/analysis/CounterComparator;

    .line 27
    .line 28
    new-instance v0, Lorg/jacoco/core/analysis/CounterComparator;

    .line 29
    .line 30
    sget-object v1, Lorg/jacoco/core/analysis/ICounter$CounterValue;->COVEREDRATIO:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    .line 31
    .line 32
    invoke-direct {v0, v1}, Lorg/jacoco/core/analysis/CounterComparator;-><init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;)V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lorg/jacoco/core/analysis/CounterComparator;->COVEREDRATIO:Lorg/jacoco/core/analysis/CounterComparator;

    .line 36
    .line 37
    new-instance v0, Lorg/jacoco/core/analysis/CounterComparator;

    .line 38
    .line 39
    sget-object v1, Lorg/jacoco/core/analysis/ICounter$CounterValue;->MISSEDRATIO:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    .line 40
    .line 41
    invoke-direct {v0, v1}, Lorg/jacoco/core/analysis/CounterComparator;-><init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;)V

    .line 42
    .line 43
    .line 44
    sput-object v0, Lorg/jacoco/core/analysis/CounterComparator;->MISSEDRATIO:Lorg/jacoco/core/analysis/CounterComparator;

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private constructor <init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lorg/jacoco/core/analysis/CounterComparator;-><init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;Z)V

    return-void
.end method

.method private constructor <init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;Z)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lorg/jacoco/core/analysis/CounterComparator;->value:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    .line 4
    iput-boolean p2, p0, Lorg/jacoco/core/analysis/CounterComparator;->reverse:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lorg/jacoco/core/analysis/ICounter;

    check-cast p2, Lorg/jacoco/core/analysis/ICounter;

    invoke-virtual {p0, p1, p2}, Lorg/jacoco/core/analysis/CounterComparator;->compare(Lorg/jacoco/core/analysis/ICounter;Lorg/jacoco/core/analysis/ICounter;)I

    move-result p1

    return p1
.end method

.method public compare(Lorg/jacoco/core/analysis/ICounter;Lorg/jacoco/core/analysis/ICounter;)I
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/jacoco/core/analysis/CounterComparator;->value:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    invoke-interface {p1, v0}, Lorg/jacoco/core/analysis/ICounter;->〇080(Lorg/jacoco/core/analysis/ICounter$CounterValue;)D

    move-result-wide v0

    iget-object p1, p0, Lorg/jacoco/core/analysis/CounterComparator;->value:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    invoke-interface {p2, p1}, Lorg/jacoco/core/analysis/ICounter;->〇080(Lorg/jacoco/core/analysis/ICounter$CounterValue;)D

    move-result-wide p1

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Double;->compare(DD)I

    move-result p1

    .line 3
    iget-boolean p2, p0, Lorg/jacoco/core/analysis/CounterComparator;->reverse:Z

    if-eqz p2, :cond_0

    neg-int p1, p1

    :cond_0
    return p1
.end method

.method public on(Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;)Lorg/jacoco/core/analysis/NodeComparator;
    .locals 1

    .line 1
    new-instance v0, Lorg/jacoco/core/analysis/NodeComparator;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lorg/jacoco/core/analysis/NodeComparator;-><init>(Ljava/util/Comparator;Lorg/jacoco/core/analysis/ICoverageNode$CounterEntity;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public reverse()Lorg/jacoco/core/analysis/CounterComparator;
    .locals 3

    .line 1
    new-instance v0, Lorg/jacoco/core/analysis/CounterComparator;

    .line 2
    .line 3
    iget-object v1, p0, Lorg/jacoco/core/analysis/CounterComparator;->value:Lorg/jacoco/core/analysis/ICounter$CounterValue;

    .line 4
    .line 5
    iget-boolean v2, p0, Lorg/jacoco/core/analysis/CounterComparator;->reverse:Z

    .line 6
    .line 7
    xor-int/lit8 v2, v2, 0x1

    .line 8
    .line 9
    invoke-direct {v0, v1, v2}, Lorg/jacoco/core/analysis/CounterComparator;-><init>(Lorg/jacoco/core/analysis/ICounter$CounterValue;Z)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

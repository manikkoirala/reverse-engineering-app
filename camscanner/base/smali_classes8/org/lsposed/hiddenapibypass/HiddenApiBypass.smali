.class public final Lorg/lsposed/hiddenapibypass/HiddenApiBypass;
.super Ljava/lang/Object;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x1c
.end annotation


# static fields
.field private static final O8:J

.field private static final Oo08:J

.field private static final oO80:Ljava/util/Set;

.field private static final o〇0:J

.field private static final 〇080:Lsun/misc/Unsafe;

.field private static final 〇o00〇〇Oo:J

.field private static final 〇o〇:J

.field private static final 〇〇888:J


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 1
    const-class v0, Lorg/lsposed/hiddenapibypass/Helper$NeverCall;

    .line 2
    .line 3
    new-instance v1, Ljava/util/HashSet;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 6
    .line 7
    .line 8
    sput-object v1, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->oO80:Ljava/util/Set;

    .line 9
    .line 10
    :try_start_0
    const-class v1, Lsun/misc/Unsafe;

    .line 11
    .line 12
    const-string v2, "getUnsafe"

    .line 13
    .line 14
    const/4 v3, 0x0

    .line 15
    new-array v4, v3, [Ljava/lang/Class;

    .line 16
    .line 17
    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    new-array v2, v3, [Ljava/lang/Object;

    .line 22
    .line 23
    const/4 v4, 0x0

    .line 24
    invoke-virtual {v1, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    check-cast v1, Lsun/misc/Unsafe;

    .line 29
    .line 30
    sput-object v1, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇080:Lsun/misc/Unsafe;

    .line 31
    .line 32
    const-class v2, Lorg/lsposed/hiddenapibypass/Helper$MethodHandle;

    .line 33
    .line 34
    const-string v4, "artFieldOrMethod"

    .line 35
    .line 36
    invoke-virtual {v2, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-virtual {v1, v2}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    .line 41
    .line 42
    .line 43
    move-result-wide v4

    .line 44
    sput-wide v4, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇o00〇〇Oo:J

    .line 45
    .line 46
    const-class v2, Lorg/lsposed/hiddenapibypass/Helper$MethodHandleImpl;

    .line 47
    .line 48
    const-string v6, "info"

    .line 49
    .line 50
    invoke-virtual {v2, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    invoke-virtual {v1, v2}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    .line 55
    .line 56
    .line 57
    move-result-wide v6

    .line 58
    sput-wide v6, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇o〇:J

    .line 59
    .line 60
    const-class v2, Lorg/lsposed/hiddenapibypass/Helper$Class;

    .line 61
    .line 62
    const-string v6, "methods"

    .line 63
    .line 64
    invoke-virtual {v2, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    invoke-virtual {v1, v2}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    .line 69
    .line 70
    .line 71
    move-result-wide v6

    .line 72
    sput-wide v6, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->O8:J

    .line 73
    .line 74
    const-class v2, Lorg/lsposed/hiddenapibypass/Helper$HandleInfo;

    .line 75
    .line 76
    const-string v8, "member"

    .line 77
    .line 78
    invoke-virtual {v2, v8}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    invoke-virtual {v1, v2}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    .line 83
    .line 84
    .line 85
    move-result-wide v8

    .line 86
    sput-wide v8, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->Oo08:J

    .line 87
    .line 88
    invoke-static {}, Lorg/lsposed/hiddenapibypass/〇080;->〇080()Ljava/lang/invoke/MethodHandles$Lookup;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    const-string v8, "a"

    .line 93
    .line 94
    new-array v9, v3, [Ljava/lang/Class;

    .line 95
    .line 96
    invoke-virtual {v0, v8, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 97
    .line 98
    .line 99
    move-result-object v8

    .line 100
    invoke-static {v2, v8}, Lorg/lsposed/hiddenapibypass/〇o00〇〇Oo;->〇080(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/reflect/Method;)Ljava/lang/invoke/MethodHandle;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    invoke-static {}, Lorg/lsposed/hiddenapibypass/〇080;->〇080()Ljava/lang/invoke/MethodHandles$Lookup;

    .line 105
    .line 106
    .line 107
    move-result-object v8

    .line 108
    const-string v9, "b"

    .line 109
    .line 110
    new-array v3, v3, [Ljava/lang/Class;

    .line 111
    .line 112
    invoke-virtual {v0, v9, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 113
    .line 114
    .line 115
    move-result-object v3

    .line 116
    invoke-static {v8, v3}, Lorg/lsposed/hiddenapibypass/〇o00〇〇Oo;->〇080(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/reflect/Method;)Ljava/lang/invoke/MethodHandle;

    .line 117
    .line 118
    .line 119
    move-result-object v3

    .line 120
    invoke-virtual {v1, v2, v4, v5}, Lsun/misc/Unsafe;->getLong(Ljava/lang/Object;J)J

    .line 121
    .line 122
    .line 123
    move-result-wide v8

    .line 124
    invoke-virtual {v1, v3, v4, v5}, Lsun/misc/Unsafe;->getLong(Ljava/lang/Object;J)J

    .line 125
    .line 126
    .line 127
    move-result-wide v2

    .line 128
    invoke-virtual {v1, v0, v6, v7}, Lsun/misc/Unsafe;->getLong(Ljava/lang/Object;J)J

    .line 129
    .line 130
    .line 131
    move-result-wide v0

    .line 132
    sub-long/2addr v2, v8

    .line 133
    sput-wide v2, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->o〇0:J

    .line 134
    .line 135
    sub-long/2addr v8, v0

    .line 136
    sub-long/2addr v8, v2

    .line 137
    sput-wide v8, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇〇888:J
    :try_end_0
    .catch Ljava/lang/ReflectiveOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    .line 139
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 141
    new-instance v1, Ljava/lang/ExceptionInInitializerError;

    .line 142
    .line 143
    invoke-direct {v1, v0}, Ljava/lang/ExceptionInInitializerError;-><init>(Ljava/lang/Throwable;)V

    .line 144
    .line 145
    .line 146
    throw v1
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static varargs O8([Ljava/lang/String;)Z
    .locals 1

    .line 1
    sget-object v0, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->oO80:Ljava/util/Set;

    .line 2
    .line 3
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-interface {v0, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 8
    .line 9
    .line 10
    move-object p0, v0

    .line 11
    check-cast p0, Ljava/util/HashSet;

    .line 12
    .line 13
    invoke-virtual {p0}, Ljava/util/HashSet;->size()I

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    new-array p0, p0, [Ljava/lang/String;

    .line 18
    .line 19
    invoke-interface {v0, p0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    invoke-static {p0}, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇〇888([Ljava/lang/String;)Z

    .line 23
    .line 24
    .line 25
    move-result p0

    .line 26
    return p0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static synthetic Oo08(Ljava/lang/reflect/Executable;)Z
    .locals 1

    .line 1
    invoke-static {p0}, Lorg/lsposed/hiddenapibypass/oO80;->〇080(Ljava/lang/reflect/Executable;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const-string v0, "setHiddenApiExemptions"

    .line 6
    .line 7
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result p0

    .line 11
    return p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static o〇0(Ljava/lang/Class;)Ljava/util/List;
    .locals 13

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-nez v1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_0
    :try_start_0
    invoke-static {}, Lorg/lsposed/hiddenapibypass/〇080;->〇080()Ljava/lang/invoke/MethodHandles$Lookup;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const-class v2, Lorg/lsposed/hiddenapibypass/Helper$NeverCall;

    .line 24
    .line 25
    const-string v3, "a"

    .line 26
    .line 27
    const/4 v4, 0x0

    .line 28
    new-array v5, v4, [Ljava/lang/Class;

    .line 29
    .line 30
    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-static {v1, v2}, Lorg/lsposed/hiddenapibypass/〇o00〇〇Oo;->〇080(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/reflect/Method;)Ljava/lang/invoke/MethodHandle;

    .line 35
    .line 36
    .line 37
    move-result-object v1
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    sget-object v2, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇080:Lsun/misc/Unsafe;

    .line 39
    .line 40
    sget-wide v5, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->O8:J

    .line 41
    .line 42
    invoke-virtual {v2, p0, v5, v6}, Lsun/misc/Unsafe;->getLong(Ljava/lang/Object;J)J

    .line 43
    .line 44
    .line 45
    move-result-wide v11

    .line 46
    invoke-virtual {v2, v11, v12}, Lsun/misc/Unsafe;->getInt(J)I

    .line 47
    .line 48
    .line 49
    move-result p0

    .line 50
    :goto_0
    if-ge v4, p0, :cond_1

    .line 51
    .line 52
    int-to-long v2, v4

    .line 53
    sget-wide v5, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->o〇0:J

    .line 54
    .line 55
    mul-long v2, v2, v5

    .line 56
    .line 57
    add-long/2addr v2, v11

    .line 58
    sget-wide v5, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇〇888:J

    .line 59
    .line 60
    add-long v9, v2, v5

    .line 61
    .line 62
    sget-object v2, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇080:Lsun/misc/Unsafe;

    .line 63
    .line 64
    sget-wide v7, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇o00〇〇Oo:J

    .line 65
    .line 66
    move-object v5, v2

    .line 67
    move-object v6, v1

    .line 68
    invoke-virtual/range {v5 .. v10}, Lsun/misc/Unsafe;->putLong(Ljava/lang/Object;JJ)V

    .line 69
    .line 70
    .line 71
    sget-wide v5, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇o〇:J

    .line 72
    .line 73
    const/4 v3, 0x0

    .line 74
    invoke-virtual {v2, v1, v5, v6, v3}, Lsun/misc/Unsafe;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    .line 75
    .line 76
    .line 77
    :try_start_1
    invoke-static {}, Lorg/lsposed/hiddenapibypass/〇080;->〇080()Ljava/lang/invoke/MethodHandles$Lookup;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-static {v2, v1}, Lorg/lsposed/hiddenapibypass/〇80〇808〇O;->〇080(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/invoke/MethodHandle;)Ljava/lang/invoke/MethodHandleInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    .line 83
    .line 84
    :catchall_0
    sget-object v2, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇080:Lsun/misc/Unsafe;

    .line 85
    .line 86
    sget-wide v5, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇o〇:J

    .line 87
    .line 88
    invoke-virtual {v2, v1, v5, v6}, Lsun/misc/Unsafe;->getObject(Ljava/lang/Object;J)Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    move-result-object v3

    .line 92
    check-cast v3, Ljava/lang/invoke/MethodHandleInfo;

    .line 93
    .line 94
    sget-wide v5, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->Oo08:J

    .line 95
    .line 96
    invoke-virtual {v2, v3, v5, v6}, Lsun/misc/Unsafe;->getObject(Ljava/lang/Object;J)Ljava/lang/Object;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    check-cast v2, Ljava/lang/reflect/Executable;

    .line 101
    .line 102
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    add-int/lit8 v4, v4, 0x1

    .line 106
    .line 107
    goto :goto_0

    .line 108
    :catch_0
    :cond_1
    :goto_1
    return-object v0
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static synthetic 〇080(Ljava/lang/reflect/Executable;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->Oo08(Ljava/lang/reflect/Executable;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic 〇o00〇〇Oo(Ljava/lang/reflect/Executable;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->〇o〇(Ljava/lang/reflect/Executable;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static synthetic 〇o〇(Ljava/lang/reflect/Executable;)Z
    .locals 1

    .line 1
    invoke-static {p0}, Lorg/lsposed/hiddenapibypass/oO80;->〇080(Ljava/lang/reflect/Executable;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    const-string v0, "getRuntime"

    .line 6
    .line 7
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result p0

    .line 11
    return p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static varargs 〇〇888([Ljava/lang/String;)Z
    .locals 6

    .line 1
    const-class v0, Ldalvik/system/VMRuntime;

    .line 2
    .line 3
    invoke-static {v0}, Lorg/lsposed/hiddenapibypass/HiddenApiBypass;->o〇0(Ljava/lang/Class;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lorg/lsposed/hiddenapibypass/〇o〇;->〇080(Ljava/util/List;)Ljava/util/stream/Stream;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    new-instance v2, Lorg/lsposed/hiddenapibypass/〇8o8o〇;

    .line 12
    .line 13
    invoke-direct {v2}, Lorg/lsposed/hiddenapibypass/〇8o8o〇;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-static {v1, v2}, Lio/opentelemetry/context/〇0〇O0088o;->〇080(Ljava/util/stream/Stream;Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v1}, Lorg/lsposed/hiddenapibypass/o〇0;->〇080(Ljava/util/stream/Stream;)Ljava/util/Optional;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0}, Lorg/lsposed/hiddenapibypass/〇o〇;->〇080(Ljava/util/List;)Ljava/util/stream/Stream;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    new-instance v2, Lorg/lsposed/hiddenapibypass/OO0o〇〇〇〇0;

    .line 29
    .line 30
    invoke-direct {v2}, Lorg/lsposed/hiddenapibypass/OO0o〇〇〇〇0;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-static {v0, v2}, Lio/opentelemetry/context/〇0〇O0088o;->〇080(Ljava/util/stream/Stream;Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-static {v0}, Lorg/lsposed/hiddenapibypass/o〇0;->〇080(Ljava/util/stream/Stream;)Ljava/util/Optional;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-static {v1}, Lorg/lsposed/hiddenapibypass/〇〇888;->〇080(Ljava/util/Optional;)Z

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    const/4 v3, 0x0

    .line 46
    if-eqz v2, :cond_0

    .line 47
    .line 48
    invoke-static {v0}, Lorg/lsposed/hiddenapibypass/〇〇888;->〇080(Ljava/util/Optional;)Z

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    if-eqz v2, :cond_0

    .line 53
    .line 54
    invoke-static {v1}, Lorg/lsposed/hiddenapibypass/O8;->〇080(Ljava/util/Optional;)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    check-cast v2, Ljava/lang/reflect/Executable;

    .line 59
    .line 60
    const/4 v4, 0x1

    .line 61
    invoke-static {v2, v4}, Lorg/lsposed/hiddenapibypass/Oo08;->〇080(Ljava/lang/reflect/Executable;Z)V

    .line 62
    .line 63
    .line 64
    :try_start_0
    invoke-static {v1}, Lorg/lsposed/hiddenapibypass/O8;->〇080(Ljava/util/Optional;)Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    check-cast v1, Ljava/lang/reflect/Method;

    .line 69
    .line 70
    new-array v2, v3, [Ljava/lang/Object;

    .line 71
    .line 72
    const/4 v5, 0x0

    .line 73
    invoke-virtual {v1, v5, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-static {v0}, Lorg/lsposed/hiddenapibypass/O8;->〇080(Ljava/util/Optional;)Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    check-cast v2, Ljava/lang/reflect/Executable;

    .line 82
    .line 83
    invoke-static {v2, v4}, Lorg/lsposed/hiddenapibypass/Oo08;->〇080(Ljava/lang/reflect/Executable;Z)V

    .line 84
    .line 85
    .line 86
    invoke-static {v0}, Lorg/lsposed/hiddenapibypass/O8;->〇080(Ljava/util/Optional;)Ljava/lang/Object;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    check-cast v0, Ljava/lang/reflect/Method;

    .line 91
    .line 92
    new-array v2, v4, [Ljava/lang/Object;

    .line 93
    .line 94
    aput-object p0, v2, v3

    .line 95
    .line 96
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .line 98
    .line 99
    return v4

    .line 100
    :catch_0
    :cond_0
    return v3
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

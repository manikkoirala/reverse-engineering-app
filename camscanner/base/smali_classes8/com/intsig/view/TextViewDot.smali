.class public Lcom/intsig/view/TextViewDot;
.super Landroidx/appcompat/widget/AppCompatTextView;
.source "TextViewDot.java"


# instance fields
.field private O8o08O8O:I

.field private OO:F

.field private o0:Lcom/intsig/view/ViewDotStrategy;

.field private o〇00O:Landroid/graphics/Paint;

.field private 〇080OO8〇0:Landroid/graphics/Rect;

.field private 〇08O〇00〇o:Z

.field private 〇0O:Ljava/lang/String;

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p2, Lcom/intsig/view/ViewDotStrategy;

    invoke-direct {p2}, Lcom/intsig/view/ViewDotStrategy;-><init>()V

    iput-object p2, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    const/4 p2, 0x2

    .line 3
    iput p2, p0, Lcom/intsig/view/TextViewDot;->〇OOo8〇0:I

    .line 4
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/intsig/view/TextViewDot;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 5
    invoke-direct {p0, p1}, Lcom/intsig/view/TextViewDot;->〇080(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 6
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 7
    new-instance p2, Lcom/intsig/view/ViewDotStrategy;

    invoke-direct {p2}, Lcom/intsig/view/ViewDotStrategy;-><init>()V

    iput-object p2, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    const/4 p2, 0x2

    .line 8
    iput p2, p0, Lcom/intsig/view/TextViewDot;->〇OOo8〇0:I

    .line 9
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/intsig/view/TextViewDot;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/view/TextViewDot;->〇080(Landroid/content/Context;)V

    return-void
.end method

.method private getRedDotX()F
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/view/ViewDotStrategy;->〇o〇()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-virtual {p0}, Landroid/widget/TextView;->getGravity()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    iget v2, p0, Lcom/intsig/view/TextViewDot;->OO:F

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    cmpl-float v2, v2, v3

    .line 15
    .line 16
    if-lez v2, :cond_3

    .line 17
    .line 18
    and-int/lit8 v2, v1, 0x1

    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    const/high16 v4, 0x40000000    # 2.0f

    .line 22
    .line 23
    if-ne v2, v3, :cond_0

    .line 24
    .line 25
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {p0}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    iget v1, p0, Lcom/intsig/view/TextViewDot;->OO:F

    .line 42
    .line 43
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    int-to-float v2, v2

    .line 48
    div-float/2addr v2, v4

    .line 49
    add-float/2addr v1, v2

    .line 50
    div-float/2addr v0, v4

    .line 51
    goto :goto_1

    .line 52
    :cond_0
    const v2, 0x800003

    .line 53
    .line 54
    .line 55
    and-int v3, v1, v2

    .line 56
    .line 57
    if-eq v3, v2, :cond_2

    .line 58
    .line 59
    const/4 v2, 0x3

    .line 60
    and-int/2addr v1, v2

    .line 61
    if-ne v1, v2, :cond_1

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    int-to-float v1, v1

    .line 69
    mul-float v0, v0, v4

    .line 70
    .line 71
    sub-float/2addr v1, v0

    .line 72
    goto :goto_2

    .line 73
    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {p0}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    iget v1, p0, Lcom/intsig/view/TextViewDot;->OO:F

    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_3
    const/4 v1, 0x0

    .line 93
    iget-object v2, p0, Lcom/intsig/view/TextViewDot;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 94
    .line 95
    invoke-virtual {p0, v1, v2}, Landroid/widget/TextView;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 96
    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/view/TextViewDot;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 99
    .line 100
    iget v1, v1, Landroid/graphics/Rect;->right:I

    .line 101
    .line 102
    int-to-float v1, v1

    .line 103
    :goto_1
    add-float/2addr v1, v0

    .line 104
    :goto_2
    return v1
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private getRedDotY()F
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/widget/TextView;->getGravity()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/view/ViewDotStrategy;->〇o〇()F

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual {p0}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    iput-object v2, p0, Lcom/intsig/view/TextViewDot;->〇0O:Ljava/lang/String;

    .line 20
    .line 21
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_0

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/view/TextViewDot;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 28
    .line 29
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 30
    .line 31
    :goto_0
    int-to-float v0, v0

    .line 32
    :goto_1
    sub-float/2addr v0, v1

    .line 33
    goto :goto_2

    .line 34
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    iget-object v3, p0, Lcom/intsig/view/TextViewDot;->〇0O:Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    iget-object v5, p0, Lcom/intsig/view/TextViewDot;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 45
    .line 46
    const/4 v6, 0x0

    .line 47
    invoke-virtual {v2, v3, v6, v4, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 48
    .line 49
    .line 50
    and-int/lit8 v2, v0, 0x30

    .line 51
    .line 52
    const/16 v3, 0x30

    .line 53
    .line 54
    if-ne v2, v3, :cond_1

    .line 55
    .line 56
    const/4 v0, 0x0

    .line 57
    goto :goto_2

    .line 58
    :cond_1
    const/16 v2, 0x10

    .line 59
    .line 60
    and-int/2addr v0, v2

    .line 61
    if-ne v0, v2, :cond_2

    .line 62
    .line 63
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    iget-object v2, p0, Lcom/intsig/view/TextViewDot;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 68
    .line 69
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 70
    .line 71
    .line 72
    move-result v2

    .line 73
    sub-int/2addr v0, v2

    .line 74
    int-to-float v0, v0

    .line 75
    const/high16 v2, 0x40000000    # 2.0f

    .line 76
    .line 77
    div-float/2addr v0, v2

    .line 78
    goto :goto_1

    .line 79
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    iget-object v2, p0, Lcom/intsig/view/TextViewDot;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 84
    .line 85
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 86
    .line 87
    .line 88
    move-result v2

    .line 89
    sub-int/2addr v0, v2

    .line 90
    goto :goto_0

    .line 91
    :goto_2
    return v0
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private 〇080(Landroid/content/Context;)V
    .locals 2

    .line 1
    const/4 v0, 0x3

    .line 2
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 3
    .line 4
    .line 5
    move-result v1

    .line 6
    iput v1, p0, Lcom/intsig/view/TextViewDot;->〇OOo8〇0:I

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 9
    .line 10
    invoke-virtual {v1, p1}, Lcom/intsig/view/ViewDotStrategy;->O8(Landroid/content/Context;)V

    .line 11
    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 14
    .line 15
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    int-to-float p1, p1

    .line 20
    invoke-virtual {v1, p1}, Lcom/intsig/view/ViewDotStrategy;->o〇0(F)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/view/ViewDotStrategy;->〇o〇()F

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    const/high16 v0, 0x40000000    # 2.0f

    .line 30
    .line 31
    mul-float p1, p1, v0

    .line 32
    .line 33
    iput p1, p0, Lcom/intsig/view/TextViewDot;->OO:F

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
.end method

.method private 〇o00〇〇Oo()V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 5
    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/view/TextViewDot;->o〇00O:Landroid/graphics/Paint;

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    sget v2, Lcom/intsig/comm/R$color;->cs_white_FFFFFF:I

    .line 14
    .line 15
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const/16 v1, 0xc

    .line 27
    .line 28
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    iput v0, p0, Lcom/intsig/view/TextViewDot;->O8o08O8O:I

    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method


# virtual methods
.method public O8(ZI)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/view/ViewDotStrategy;->〇〇888(Z)V

    .line 4
    .line 5
    .line 6
    int-to-float p1, p2

    .line 7
    iput p1, p0, Lcom/intsig/view/TextViewDot;->OO:F

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .line 1
    iget-boolean v0, p0, Lcom/intsig/view/TextViewDot;->〇08O〇00〇o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/view/TextViewDot;->o〇00O:Landroid/graphics/Paint;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/view/TextViewDot;->〇o00〇〇Oo()V

    .line 10
    .line 11
    .line 12
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getPaddingStart()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    int-to-float v0, v0

    .line 23
    const/high16 v1, 0x3f000000    # 0.5f

    .line 24
    .line 25
    mul-float v3, v0, v1

    .line 26
    .line 27
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    int-to-float v0, v0

    .line 32
    mul-float v4, v0, v1

    .line 33
    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    int-to-float v0, v0

    .line 39
    invoke-virtual {p0}, Landroid/view/View;->getPaddingEnd()I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    int-to-float v2, v2

    .line 44
    mul-float v2, v2, v1

    .line 45
    .line 46
    sub-float v5, v0, v2

    .line 47
    .line 48
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    int-to-float v0, v0

    .line 53
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    int-to-float v2, v2

    .line 58
    mul-float v2, v2, v1

    .line 59
    .line 60
    sub-float v6, v0, v2

    .line 61
    .line 62
    iget v0, p0, Lcom/intsig/view/TextViewDot;->O8o08O8O:I

    .line 63
    .line 64
    int-to-float v7, v0

    .line 65
    int-to-float v8, v0

    .line 66
    iget-object v9, p0, Lcom/intsig/view/TextViewDot;->o〇00O:Landroid/graphics/Paint;

    .line 67
    .line 68
    move-object v2, p1

    .line 69
    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    .line 70
    .line 71
    .line 72
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/view/ViewDotStrategy;->〇o〇()F

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    invoke-direct {p0}, Lcom/intsig/view/TextViewDot;->getRedDotX()F

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 86
    .line 87
    .line 88
    move-result v2

    .line 89
    int-to-float v2, v2

    .line 90
    sub-float/2addr v2, v0

    .line 91
    cmpl-float v2, v1, v2

    .line 92
    .line 93
    if-lez v2, :cond_2

    .line 94
    .line 95
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 96
    .line 97
    .line 98
    move-result v1

    .line 99
    int-to-float v1, v1

    .line 100
    sub-float/2addr v1, v0

    .line 101
    :cond_2
    invoke-direct {p0}, Lcom/intsig/view/TextViewDot;->getRedDotY()F

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    iget v3, p0, Lcom/intsig/view/TextViewDot;->〇OOo8〇0:I

    .line 106
    .line 107
    int-to-float v4, v3

    .line 108
    add-float/2addr v4, v0

    .line 109
    cmpg-float v4, v2, v4

    .line 110
    .line 111
    if-gez v4, :cond_3

    .line 112
    .line 113
    int-to-float v2, v3

    .line 114
    add-float/2addr v2, v0

    .line 115
    :cond_3
    iget-object v0, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 116
    .line 117
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/view/ViewDotStrategy;->〇080(Landroid/graphics/Canvas;FF)V

    .line 118
    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setUseSceneStyle(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/view/TextViewDot;->〇08O〇00〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇o〇(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/TextViewDot;->o0:Lcom/intsig/view/ViewDotStrategy;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/view/ViewDotStrategy;->〇〇888(Z)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

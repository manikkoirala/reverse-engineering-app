.class Lcom/intsig/view/InkSettingLayout$ColorItemViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "InkSettingLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/view/InkSettingLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ColorItemViewHolder"
.end annotation


# instance fields
.field o0:Lcom/intsig/view/color/ColorItemView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    sget v0, Lcom/intsig/innote/R$id;->color_item:I

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/view/color/ColorItemView;

    .line 11
    .line 12
    iput-object p1, p0, Lcom/intsig/view/InkSettingLayout$ColorItemViewHolder;->o0:Lcom/intsig/view/color/ColorItemView;

    .line 13
    .line 14
    const/high16 v0, 0x41b00000    # 22.0f

    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/view/color/ColorItemView;->setDrawableSize(I)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout$ColorItemViewHolder;->o0:Lcom/intsig/view/color/ColorItemView;

    .line 24
    .line 25
    const v0, 0x3f970a3d    # 1.18f

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v0}, Lcom/intsig/view/color/ColorItemView;->setSelectScale(F)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout$ColorItemViewHolder;->o0:Lcom/intsig/view/color/ColorItemView;

    .line 32
    .line 33
    const/4 v0, 0x1

    .line 34
    invoke-virtual {p1, v0}, Lcom/intsig/view/color/ColorItemView;->setInnerBlackRing(Z)V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout$ColorItemViewHolder;->o0:Lcom/intsig/view/color/ColorItemView;

    .line 38
    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    .line 40
    .line 41
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    invoke-virtual {p1, v0}, Lcom/intsig/view/color/ColorItemView;->setSelectBorderWidth(I)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout$ColorItemViewHolder;->o0:Lcom/intsig/view/color/ColorItemView;

    .line 49
    .line 50
    const/high16 v0, 0x3f000000    # 0.5f

    .line 51
    .line 52
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    invoke-virtual {p1, v0}, Lcom/intsig/view/color/ColorItemView;->setDefaultBorderWidth(I)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

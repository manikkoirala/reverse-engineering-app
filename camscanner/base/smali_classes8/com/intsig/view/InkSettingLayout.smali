.class public Lcom/intsig/view/InkSettingLayout;
.super Landroid/widget/FrameLayout;
.source "InkSettingLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;,
        Lcom/intsig/view/InkSettingLayout$ColorChangeListener;,
        Lcom/intsig/view/InkSettingLayout$ColorItemViewHolder;,
        Lcom/intsig/view/InkSettingLayout$ColorItemData;,
        Lcom/intsig/view/InkSettingLayout$ColorListAdapter;
    }
.end annotation


# instance fields
.field private O8o08O8O:Landroid/widget/ImageView;

.field private OO:I

.field private OO〇00〇8oO:I

.field private o0:Landroidx/recyclerview/widget/RecyclerView;

.field private o8〇OO0〇0o:Landroid/view/View;

.field private oOo0:I

.field private oOo〇8o008:Landroid/view/View;

.field private ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

.field private o〇00O:Landroid/widget/ImageView;

.field private 〇080OO8〇0:Landroid/widget/TextView;

.field private 〇08O〇00〇o:Landroid/widget/SeekBar;

.field private final 〇0O:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/intsig/notes/pen/PenState;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇8〇oO〇〇8o:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private 〇OOo8〇0:Lcom/intsig/view/InkSettingLayout$ColorListAdapter;

.field private 〇〇08O:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p2, Landroid/util/SparseArray;

    invoke-direct {p2}, Landroid/util/SparseArray;-><init>()V

    iput-object p2, p0, Lcom/intsig/view/InkSettingLayout;->〇0O:Landroid/util/SparseArray;

    .line 3
    new-instance p2, Lcom/intsig/view/InkSettingLayout$1;

    invoke-direct {p2, p0}, Lcom/intsig/view/InkSettingLayout$1;-><init>(Lcom/intsig/view/InkSettingLayout;)V

    iput-object p2, p0, Lcom/intsig/view/InkSettingLayout;->〇8〇oO〇〇8o:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/view/InkSettingLayout;->〇8o8o〇(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 5
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    new-instance p2, Landroid/util/SparseArray;

    invoke-direct {p2}, Landroid/util/SparseArray;-><init>()V

    iput-object p2, p0, Lcom/intsig/view/InkSettingLayout;->〇0O:Landroid/util/SparseArray;

    .line 7
    new-instance p2, Lcom/intsig/view/InkSettingLayout$1;

    invoke-direct {p2, p0}, Lcom/intsig/view/InkSettingLayout$1;-><init>(Lcom/intsig/view/InkSettingLayout;)V

    iput-object p2, p0, Lcom/intsig/view/InkSettingLayout;->〇8〇oO〇〇8o:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/view/InkSettingLayout;->〇8o8o〇(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/view/InkSettingLayout;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/view/InkSettingLayout;->OO〇00〇8oO:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private OO0o〇〇〇〇0(Landroid/view/View;Z)V
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    const/4 p2, 0x1

    .line 4
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 5
    .line 6
    .line 7
    const/high16 p2, 0x3f800000    # 1.0f

    .line 8
    .line 9
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 p2, 0x0

    .line 14
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 15
    .line 16
    .line 17
    const p2, 0x3e6b851f    # 0.23f

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 21
    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic Oo08(Lcom/intsig/view/InkSettingLayout;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/view/InkSettingLayout;->oOo0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getCurrentPageId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "CSAnnotation"

    .line 10
    .line 11
    return-object v0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->〇〇08O:Ljava/lang/String;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static bridge synthetic oO80(Lcom/intsig/view/InkSettingLayout;Landroid/view/View;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/InkSettingLayout;->〇O00(Landroid/view/View;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method static bridge synthetic o〇0(Lcom/intsig/view/InkSettingLayout;)Landroid/util/SparseArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/view/InkSettingLayout;->〇0O:Landroid/util/SparseArray;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private setUpColorRecyclerView(Landroid/content/Context;)V
    .locals 2

    .line 1
    sget v0, Lcom/intsig/innote/R$id;->color_list:I

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/view/InkSettingLayout;->o0:Landroidx/recyclerview/widget/RecyclerView;

    .line 10
    .line 11
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-direct {v0, p1, v1, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o0:Landroidx/recyclerview/widget/RecyclerView;

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o0:Landroidx/recyclerview/widget/RecyclerView;

    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic 〇080(Lcom/intsig/view/InkSettingLayout;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/InkSettingLayout;->〇O8o08O(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private 〇8o8o〇(Landroid/content/Context;)V
    .locals 3

    .line 1
    const-string v0, "layout_inflater"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/view/LayoutInflater;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    sget v1, Lcom/intsig/innote/R$layout;->ink_settting_pnl:I

    .line 12
    .line 13
    const/4 v2, 0x1

    .line 14
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    sget v0, Lcom/intsig/innote/R$id;->seekbar_adjust_size:I

    .line 18
    .line 19
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/SeekBar;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/view/InkSettingLayout;->〇08O〇00〇o:Landroid/widget/SeekBar;

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/view/InkSettingLayout;->〇8〇oO〇〇8o:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 30
    .line 31
    .line 32
    sget v0, Lcom/intsig/innote/R$id;->iv_undo:I

    .line 33
    .line 34
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    check-cast v0, Landroid/widget/ImageView;

    .line 39
    .line 40
    iput-object v0, p0, Lcom/intsig/view/InkSettingLayout;->o〇00O:Landroid/widget/ImageView;

    .line 41
    .line 42
    sget v0, Lcom/intsig/innote/R$id;->iv_redo:I

    .line 43
    .line 44
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    check-cast v0, Landroid/widget/ImageView;

    .line 49
    .line 50
    iput-object v0, p0, Lcom/intsig/view/InkSettingLayout;->O8o08O8O:Landroid/widget/ImageView;

    .line 51
    .line 52
    sget v0, Lcom/intsig/innote/R$id;->tv_title:I

    .line 53
    .line 54
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    check-cast v0, Landroid/widget/TextView;

    .line 59
    .line 60
    iput-object v0, p0, Lcom/intsig/view/InkSettingLayout;->〇080OO8〇0:Landroid/widget/TextView;

    .line 61
    .line 62
    sget v0, Lcom/intsig/innote/R$id;->view_color_mask:I

    .line 63
    .line 64
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    iput-object v0, p0, Lcom/intsig/view/InkSettingLayout;->o8〇OO0〇0o:Landroid/view/View;

    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->o〇00O:Landroid/widget/ImageView;

    .line 71
    .line 72
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->O8o08O8O:Landroid/widget/ImageView;

    .line 76
    .line 77
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    .line 79
    .line 80
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 81
    .line 82
    const/16 v1, 0x16

    .line 83
    .line 84
    if-le v0, v1, :cond_0

    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->〇08O〇00〇o:Landroid/widget/SeekBar;

    .line 87
    .line 88
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    const/4 v1, -0x1

    .line 93
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 94
    .line 95
    iget-object v1, p0, Lcom/intsig/view/InkSettingLayout;->〇08O〇00〇o:Landroid/widget/SeekBar;

    .line 96
    .line 97
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    .line 99
    .line 100
    :cond_0
    sget v0, Lcom/intsig/innote/R$id;->iv_close:I

    .line 101
    .line 102
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    .line 108
    .line 109
    sget v0, Lcom/intsig/innote/R$id;->iv_save:I

    .line 110
    .line 111
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    .line 117
    .line 118
    sget v0, Lcom/intsig/innote/R$id;->iv_mark_pen:I

    .line 119
    .line 120
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    .line 126
    .line 127
    sget v0, Lcom/intsig/innote/R$id;->iv_hight_light_pen:I

    .line 128
    .line 129
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    .line 135
    .line 136
    sget v0, Lcom/intsig/innote/R$id;->iv_eraser:I

    .line 137
    .line 138
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    .line 144
    .line 145
    invoke-direct {p0, p1}, Lcom/intsig/view/InkSettingLayout;->setUpColorRecyclerView(Landroid/content/Context;)V

    .line 146
    .line 147
    .line 148
    :cond_1
    const/high16 p1, 0x41200000    # 10.0f

    .line 149
    .line 150
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 151
    .line 152
    .line 153
    move-result p1

    .line 154
    iput p1, p0, Lcom/intsig/view/InkSettingLayout;->oOo0:I

    .line 155
    .line 156
    const/high16 p1, 0x42700000    # 60.0f

    .line 157
    .line 158
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 159
    .line 160
    .line 161
    move-result p1

    .line 162
    iput p1, p0, Lcom/intsig/view/InkSettingLayout;->OO〇00〇8oO:I

    .line 163
    .line 164
    return-void
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private 〇O00(Landroid/view/View;I)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-ne v0, p2, :cond_1

    .line 9
    .line 10
    return-void

    .line 11
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇O8o08O(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;->Oo08(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o0:Landroidx/recyclerview/widget/RecyclerView;

    .line 9
    .line 10
    if-eqz p1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 13
    .line 14
    .line 15
    :cond_1
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/view/InkSettingLayout;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/view/InkSettingLayout;->OO:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/view/InkSettingLayout;)Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/view/InkSettingLayout;->ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/view/InkSettingLayout;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/view/InkSettingLayout;->oOo〇8o008:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public OO0o〇〇(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/view/InkSettingLayout$ColorItemData;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/view/InkSettingLayout$ColorListAdapter;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/view/InkSettingLayout$ColorListAdapter;-><init>(Ljava/util/List;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/view/InkSettingLayout;->〇OOo8〇0:Lcom/intsig/view/InkSettingLayout$ColorListAdapter;

    .line 7
    .line 8
    new-instance v1, Lcom/intsig/view/O8;

    .line 9
    .line 10
    invoke-direct {v1, p0}, Lcom/intsig/view/O8;-><init>(Lcom/intsig/view/InkSettingLayout;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/view/InkSettingLayout$ColorListAdapter;->OoO8(Lcom/intsig/view/InkSettingLayout$ColorChangeListener;)V

    .line 14
    .line 15
    .line 16
    const/high16 v0, 0x40e00000    # 7.0f

    .line 17
    .line 18
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    const/high16 v4, 0x42b80000    # 92.0f

    .line 39
    .line 40
    invoke-static {v4}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    sub-int/2addr v3, v4

    .line 45
    if-lez v3, :cond_1

    .line 46
    .line 47
    const/high16 v0, 0x41d00000    # 26.0f

    .line 48
    .line 49
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    const/high16 v1, 0x42200000    # 40.0f

    .line 54
    .line 55
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    div-int v1, v3, v1

    .line 60
    .line 61
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    const/high16 v4, 0x40000000    # 2.0f

    .line 66
    .line 67
    if-ge v1, v2, :cond_0

    .line 68
    .line 69
    int-to-float p1, v3

    .line 70
    int-to-float v1, v1

    .line 71
    const/high16 v2, 0x3f000000    # 0.5f

    .line 72
    .line 73
    add-float/2addr v1, v2

    .line 74
    div-float/2addr p1, v1

    .line 75
    int-to-float v0, v0

    .line 76
    sub-float/2addr p1, v0

    .line 77
    div-float/2addr p1, v4

    .line 78
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    move v0, v1

    .line 83
    move v2, v0

    .line 84
    goto :goto_0

    .line 85
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 86
    .line 87
    .line 88
    move-result p1

    .line 89
    const/high16 v1, 0x41900000    # 18.0f

    .line 90
    .line 91
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    sub-int/2addr v3, v1

    .line 96
    int-to-float v2, v3

    .line 97
    const/high16 v5, 0x3f800000    # 1.0f

    .line 98
    .line 99
    mul-float v2, v2, v5

    .line 100
    .line 101
    int-to-float v5, p1

    .line 102
    div-float/2addr v2, v5

    .line 103
    int-to-float v5, v0

    .line 104
    sub-float/2addr v2, v5

    .line 105
    div-float/2addr v2, v4

    .line 106
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    add-int/lit8 v4, p1, -0x1

    .line 111
    .line 112
    mul-int/lit8 v4, v4, 0x2

    .line 113
    .line 114
    mul-int v4, v4, v2

    .line 115
    .line 116
    sub-int/2addr v3, v4

    .line 117
    mul-int p1, p1, v0

    .line 118
    .line 119
    sub-int p1, v3, p1

    .line 120
    .line 121
    move v0, v2

    .line 122
    move v2, p1

    .line 123
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 124
    if-gez v1, :cond_2

    .line 125
    .line 126
    const/4 v1, 0x0

    .line 127
    :cond_2
    if-gez v2, :cond_3

    .line 128
    .line 129
    const/4 v2, 0x0

    .line 130
    :cond_3
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o0:Landroidx/recyclerview/widget/RecyclerView;

    .line 131
    .line 132
    new-instance v3, Lcom/intsig/view/InkSettingLayout$2;

    .line 133
    .line 134
    invoke-direct {v3, p0, v0, v2, v1}, Lcom/intsig/view/InkSettingLayout$2;-><init>(Lcom/intsig/view/InkSettingLayout;III)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {p1, v3}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 138
    .line 139
    .line 140
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o0:Landroidx/recyclerview/widget/RecyclerView;

    .line 141
    .line 142
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->〇OOo8〇0:Lcom/intsig/view/InkSettingLayout$ColorListAdapter;

    .line 143
    .line 144
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 145
    .line 146
    .line 147
    return-void
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public Oooo8o0〇(IZ)V
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/view/InkSettingLayout;->OO:I

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    if-eqz p2, :cond_1

    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/view/InkSettingLayout;->ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

    .line 9
    .line 10
    if-eqz p2, :cond_1

    .line 11
    .line 12
    invoke-interface {p2, p1}, Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;->〇o00〇〇Oo(I)V

    .line 13
    .line 14
    .line 15
    :cond_1
    iput p1, p0, Lcom/intsig/view/InkSettingLayout;->OO:I

    .line 16
    .line 17
    const/4 p2, 0x3

    .line 18
    new-array v0, p2, [I

    .line 19
    .line 20
    fill-array-data v0, :array_0

    .line 21
    .line 22
    .line 23
    new-array v1, p2, [Landroid/view/View;

    .line 24
    .line 25
    sget v2, Lcom/intsig/innote/R$id;->iv_mark_pen:I

    .line 26
    .line 27
    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    const/4 v3, 0x0

    .line 32
    aput-object v2, v1, v3

    .line 33
    .line 34
    sget v2, Lcom/intsig/innote/R$id;->iv_hight_light_pen:I

    .line 35
    .line 36
    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    const/4 v4, 0x1

    .line 41
    aput-object v2, v1, v4

    .line 42
    .line 43
    sget v2, Lcom/intsig/innote/R$id;->iv_eraser:I

    .line 44
    .line 45
    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    const/4 v5, 0x2

    .line 50
    aput-object v2, v1, v5

    .line 51
    .line 52
    const/4 v2, 0x0

    .line 53
    :goto_0
    if-ge v2, p2, :cond_3

    .line 54
    .line 55
    aget-object v6, v1, v2

    .line 56
    .line 57
    aget v7, v0, v2

    .line 58
    .line 59
    if-ne p1, v7, :cond_2

    .line 60
    .line 61
    const/4 v7, 0x1

    .line 62
    goto :goto_1

    .line 63
    :cond_2
    const/4 v7, 0x0

    .line 64
    :goto_1
    invoke-virtual {v6, v7}, Landroid/view/View;->setSelected(Z)V

    .line 65
    .line 66
    .line 67
    add-int/lit8 v2, v2, 0x1

    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_3
    iget-object p2, p0, Lcom/intsig/view/InkSettingLayout;->〇0O:Landroid/util/SparseArray;

    .line 71
    .line 72
    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    check-cast p2, Lcom/intsig/notes/pen/PenState;

    .line 77
    .line 78
    const-string v0, "InkSettingLayout"

    .line 79
    .line 80
    if-nez p2, :cond_4

    .line 81
    .line 82
    const-string p1, "penState == null"

    .line 83
    .line 84
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    return-void

    .line 88
    :cond_4
    iget-object v1, p0, Lcom/intsig/view/InkSettingLayout;->〇08O〇00〇o:Landroid/widget/SeekBar;

    .line 89
    .line 90
    iget v2, p2, Lcom/intsig/notes/pen/PenState;->〇080:I

    .line 91
    .line 92
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 93
    .line 94
    .line 95
    if-ne p1, v5, :cond_6

    .line 96
    .line 97
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->〇OOo8〇0:Lcom/intsig/view/InkSettingLayout$ColorListAdapter;

    .line 98
    .line 99
    if-eqz p1, :cond_5

    .line 100
    .line 101
    invoke-virtual {p1, v3}, Lcom/intsig/view/InkSettingLayout$ColorListAdapter;->o800o8O(Z)V

    .line 102
    .line 103
    .line 104
    :cond_5
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o0:Landroidx/recyclerview/widget/RecyclerView;

    .line 105
    .line 106
    const p2, 0x3e99999a    # 0.3f

    .line 107
    .line 108
    .line 109
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 110
    .line 111
    .line 112
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o8〇OO0〇0o:Landroid/view/View;

    .line 113
    .line 114
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 115
    .line 116
    .line 117
    return-void

    .line 118
    :cond_6
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->〇OOo8〇0:Lcom/intsig/view/InkSettingLayout$ColorListAdapter;

    .line 119
    .line 120
    if-eqz p1, :cond_7

    .line 121
    .line 122
    invoke-virtual {p1, v4}, Lcom/intsig/view/InkSettingLayout$ColorListAdapter;->o800o8O(Z)V

    .line 123
    .line 124
    .line 125
    :cond_7
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o8〇OO0〇0o:Landroid/view/View;

    .line 126
    .line 127
    const/16 v1, 0x8

    .line 128
    .line 129
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    .line 131
    .line 132
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o0:Landroidx/recyclerview/widget/RecyclerView;

    .line 133
    .line 134
    const/high16 v1, 0x3f800000    # 1.0f

    .line 135
    .line 136
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 137
    .line 138
    .line 139
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->〇OOo8〇0:Lcom/intsig/view/InkSettingLayout$ColorListAdapter;

    .line 140
    .line 141
    if-nez p1, :cond_8

    .line 142
    .line 143
    const-string p1, "colorListAdapter == null"

    .line 144
    .line 145
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    goto :goto_2

    .line 149
    :cond_8
    iget p2, p2, Lcom/intsig/notes/pen/PenState;->〇o00〇〇Oo:I

    .line 150
    .line 151
    invoke-virtual {p1, p2}, Lcom/intsig/view/InkSettingLayout$ColorListAdapter;->〇O888o0o(I)V

    .line 152
    .line 153
    .line 154
    :goto_2
    return-void

    .line 155
    :array_0
    .array-data 4
        0x1
        0x3
        0x2
    .end array-data
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    sget v0, Lcom/intsig/innote/R$id;->iv_close:I

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    const-string v2, "InkSettingLayout"

    .line 9
    .line 10
    if-ne p1, v0, :cond_1

    .line 11
    .line 12
    const-string p1, "close"

    .line 13
    .line 14
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

    .line 18
    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    invoke-interface {p1}, Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;->onClose()V

    .line 22
    .line 23
    .line 24
    :cond_0
    invoke-direct {p0}, Lcom/intsig/view/InkSettingLayout;->getCurrentPageId()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v0, "exit"

    .line 29
    .line 30
    invoke-static {p1, v0, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 31
    .line 32
    .line 33
    goto/16 :goto_0

    .line 34
    .line 35
    :cond_1
    sget v0, Lcom/intsig/innote/R$id;->iv_save:I

    .line 36
    .line 37
    if-ne p1, v0, :cond_3

    .line 38
    .line 39
    const-string p1, "save"

    .line 40
    .line 41
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

    .line 45
    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    invoke-interface {v0}, Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;->〇080()V

    .line 49
    .line 50
    .line 51
    :cond_2
    invoke-direct {p0}, Lcom/intsig/view/InkSettingLayout;->getCurrentPageId()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-static {v0, p1, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_0

    .line 59
    .line 60
    :cond_3
    sget v0, Lcom/intsig/innote/R$id;->iv_eraser:I

    .line 61
    .line 62
    const/4 v3, 0x1

    .line 63
    if-ne p1, v0, :cond_4

    .line 64
    .line 65
    const-string p1, "eraser"

    .line 66
    .line 67
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    const/4 v0, 0x2

    .line 71
    invoke-virtual {p0, v0, v3}, Lcom/intsig/view/InkSettingLayout;->Oooo8o0〇(IZ)V

    .line 72
    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/view/InkSettingLayout;->getCurrentPageId()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    invoke-static {v0, p1, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 79
    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_4
    sget v0, Lcom/intsig/innote/R$id;->iv_mark_pen:I

    .line 83
    .line 84
    if-ne p1, v0, :cond_5

    .line 85
    .line 86
    const-string p1, "mark_pen"

    .line 87
    .line 88
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p0, v3, v3}, Lcom/intsig/view/InkSettingLayout;->Oooo8o0〇(IZ)V

    .line 92
    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/view/InkSettingLayout;->getCurrentPageId()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    const-string v0, "watercolor_pen"

    .line 99
    .line 100
    invoke-static {p1, v0, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 101
    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_5
    sget v0, Lcom/intsig/innote/R$id;->iv_hight_light_pen:I

    .line 105
    .line 106
    if-ne p1, v0, :cond_6

    .line 107
    .line 108
    const-string p1, "hight_light_pen"

    .line 109
    .line 110
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    const/4 p1, 0x3

    .line 114
    invoke-virtual {p0, p1, v3}, Lcom/intsig/view/InkSettingLayout;->Oooo8o0〇(IZ)V

    .line 115
    .line 116
    .line 117
    invoke-direct {p0}, Lcom/intsig/view/InkSettingLayout;->getCurrentPageId()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    const-string v0, "highlighter_pen"

    .line 122
    .line 123
    invoke-static {p1, v0, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_6
    sget v0, Lcom/intsig/innote/R$id;->iv_undo:I

    .line 128
    .line 129
    if-ne p1, v0, :cond_8

    .line 130
    .line 131
    const-string p1, "onUndo"

    .line 132
    .line 133
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

    .line 137
    .line 138
    if-eqz p1, :cond_7

    .line 139
    .line 140
    invoke-interface {p1}, Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;->〇o〇()V

    .line 141
    .line 142
    .line 143
    :cond_7
    invoke-direct {p0}, Lcom/intsig/view/InkSettingLayout;->getCurrentPageId()Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object p1

    .line 147
    const-string v0, "previous_step"

    .line 148
    .line 149
    invoke-static {p1, v0, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 150
    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_8
    sget v0, Lcom/intsig/innote/R$id;->iv_redo:I

    .line 154
    .line 155
    if-ne p1, v0, :cond_a

    .line 156
    .line 157
    const-string p1, "onRedo"

    .line 158
    .line 159
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

    .line 163
    .line 164
    if-eqz p1, :cond_9

    .line 165
    .line 166
    invoke-interface {p1}, Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;->o〇0()V

    .line 167
    .line 168
    .line 169
    :cond_9
    invoke-direct {p0}, Lcom/intsig/view/InkSettingLayout;->getCurrentPageId()Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object p1

    .line 173
    const-string v0, "next_step"

    .line 174
    .line 175
    invoke-static {p1, v0, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 176
    .line 177
    .line 178
    :cond_a
    :goto_0
    return-void
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setCurrentPageId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/view/InkSettingLayout;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setInkSettingLayoutListener(Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/view/InkSettingLayout;->ooo0〇〇O:Lcom/intsig/view/InkSettingLayout$InkSettingLayoutListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setViewSizeIndicator(Landroid/view/View;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/view/InkSettingLayout;->oOo〇8o008:Landroid/view/View;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇80〇808〇O(ILcom/intsig/notes/pen/PenState;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->〇0O:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public 〇O〇(ZZ)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/16 v1, 0x8

    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    if-nez p2, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->〇080OO8〇0:Landroid/widget/TextView;

    .line 9
    .line 10
    invoke-direct {p0, p1, v0}, Lcom/intsig/view/InkSettingLayout;->〇O00(Landroid/view/View;I)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->O8o08O8O:Landroid/widget/ImageView;

    .line 14
    .line 15
    invoke-direct {p0, p1, v1}, Lcom/intsig/view/InkSettingLayout;->〇O00(Landroid/view/View;I)V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/view/InkSettingLayout;->o〇00O:Landroid/widget/ImageView;

    .line 19
    .line 20
    invoke-direct {p0, p1, v1}, Lcom/intsig/view/InkSettingLayout;->〇O00(Landroid/view/View;I)V

    .line 21
    .line 22
    .line 23
    return-void

    .line 24
    :cond_0
    iget-object v2, p0, Lcom/intsig/view/InkSettingLayout;->〇080OO8〇0:Landroid/widget/TextView;

    .line 25
    .line 26
    invoke-direct {p0, v2, v1}, Lcom/intsig/view/InkSettingLayout;->〇O00(Landroid/view/View;I)V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/view/InkSettingLayout;->O8o08O8O:Landroid/widget/ImageView;

    .line 30
    .line 31
    invoke-direct {p0, v1, v0}, Lcom/intsig/view/InkSettingLayout;->〇O00(Landroid/view/View;I)V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/view/InkSettingLayout;->o〇00O:Landroid/widget/ImageView;

    .line 35
    .line 36
    invoke-direct {p0, v1, v0}, Lcom/intsig/view/InkSettingLayout;->〇O00(Landroid/view/View;I)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/view/InkSettingLayout;->O8o08O8O:Landroid/widget/ImageView;

    .line 40
    .line 41
    invoke-direct {p0, v0, p2}, Lcom/intsig/view/InkSettingLayout;->OO0o〇〇〇〇0(Landroid/view/View;Z)V

    .line 42
    .line 43
    .line 44
    iget-object p2, p0, Lcom/intsig/view/InkSettingLayout;->o〇00O:Landroid/widget/ImageView;

    .line 45
    .line 46
    invoke-direct {p0, p2, p1}, Lcom/intsig/view/InkSettingLayout;->OO0o〇〇〇〇0(Landroid/view/View;Z)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public 〇〇808〇()V
    .locals 4

    .line 1
    sget v0, Lcom/intsig/innote/R$id;->tv_size_des:I

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/widget/TextView;

    .line 8
    .line 9
    sget v1, Lcom/intsig/innote/R$id;->tv_title:I

    .line 10
    .line 11
    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Landroid/widget/TextView;

    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    sget v3, Lcom/intsig/res/InkSettingResIds;->〇080:I

    .line 22
    .line 23
    invoke-static {v2, v3}, Lcom/intsig/res/InkSettingResIds;->〇080(Landroid/content/Context;I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    sget v2, Lcom/intsig/res/InkSettingResIds;->〇o00〇〇Oo:I

    .line 35
    .line 36
    invoke-static {v0, v2}, Lcom/intsig/res/InkSettingResIds;->〇080(Landroid/content/Context;I)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

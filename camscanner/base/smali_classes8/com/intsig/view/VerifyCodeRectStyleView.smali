.class public final Lcom/intsig/view/VerifyCodeRectStyleView;
.super Landroid/widget/RelativeLayout;
.source "VerifyCodeRectStyleView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/view/VerifyCodeRectStyleView$VerifyCodeInputCompleteListener;,
        Lcom/intsig/view/VerifyCodeRectStyleView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8o08O8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final o〇00O:Lcom/intsig/view/VerifyCodeRectStyleView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇080OO8〇0:I


# instance fields
.field private OO:Lcom/intsig/view/VerifyCodeRectStyleView$VerifyCodeInputCompleteListener;

.field private o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/view/VerifyCodeRectStyleView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/view/VerifyCodeRectStyleView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/view/VerifyCodeRectStyleView;->o〇00O:Lcom/intsig/view/VerifyCodeRectStyleView$Companion;

    .line 8
    .line 9
    const-string v0, "VerifyCodeRectStyleView"

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/view/VerifyCodeRectStyleView;->O8o08O8O:Ljava/lang/String;

    .line 12
    .line 13
    const/4 v0, 0x6

    .line 14
    sput v0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇080OO8〇0:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/view/VerifyCodeRectStyleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    const-string p2, ""

    .line 5
    iput-object p2, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇08O〇00〇o:Ljava/lang/String;

    .line 6
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget p2, Lcom/intsig/comm/R$layout;->view_verify_code_rect_style:I

    const/4 p3, 0x1

    invoke-virtual {p1, p2, p0, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 7
    invoke-static {p0}, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->bind(Landroid/view/View;)Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    move-result-object p1

    const-string p2, "bind(this)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 8
    invoke-direct {p0}, Lcom/intsig/view/VerifyCodeRectStyleView;->Oo08()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/view/VerifyCodeRectStyleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final Oo08()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->〇OOo8〇0:Landroid/widget/EditText;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 12
    .line 13
    iget-object v1, v1, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->OO:Landroid/widget/TextView;

    .line 14
    .line 15
    const-string v2, "mBinding.tvVerifyCode1"

    .line 16
    .line 17
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 26
    .line 27
    iget-object v1, v1, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 28
    .line 29
    const-string v2, "mBinding.tvVerifyCode2"

    .line 30
    .line 31
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 40
    .line 41
    iget-object v1, v1, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->o〇00O:Landroid/widget/TextView;

    .line 42
    .line 43
    const-string v2, "mBinding.tvVerifyCode3"

    .line 44
    .line 45
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    .line 52
    .line 53
    iget-object v1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 54
    .line 55
    iget-object v1, v1, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 56
    .line 57
    const-string v2, "mBinding.tvVerifyCode4"

    .line 58
    .line 59
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 68
    .line 69
    iget-object v1, v1, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 70
    .line 71
    const-string v2, "mBinding.tvVerifyCode5"

    .line 72
    .line 73
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    .line 80
    .line 81
    iget-object v1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 82
    .line 83
    iget-object v1, v1, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->〇0O:Landroid/widget/TextView;

    .line 84
    .line 85
    const-string v2, "mBinding.tvVerifyCode6"

    .line 86
    .line 87
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 94
    .line 95
    iget-object v0, v0, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->〇OOo8〇0:Landroid/widget/EditText;

    .line 96
    .line 97
    new-instance v1, Lcom/intsig/view/VerifyCodeRectStyleView$initViews$1;

    .line 98
    .line 99
    invoke-direct {v1, p0}, Lcom/intsig/view/VerifyCodeRectStyleView$initViews$1;-><init>(Lcom/intsig/view/VerifyCodeRectStyleView;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 103
    .line 104
    .line 105
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static final synthetic 〇080()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇080OO8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/view/VerifyCodeRectStyleView;)Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/view/VerifyCodeRectStyleView;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public final O8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->〇OOo8〇0:Landroid/widget/EditText;

    .line 4
    .line 5
    const-string v1, ""

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇OOo8〇0:Ljava/util/List;

    .line 11
    .line 12
    check-cast v0, Ljava/lang/Iterable;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Landroid/widget/TextView;

    .line 29
    .line 30
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public final getEtVerify()Landroid/widget/EditText;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->o0:Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/comm/databinding/ViewVerifyCodeRectStyleBinding;->〇OOo8〇0:Landroid/widget/EditText;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getMCompleteListener()Lcom/intsig/view/VerifyCodeRectStyleView$VerifyCodeInputCompleteListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->OO:Lcom/intsig/view/VerifyCodeRectStyleView$VerifyCodeInputCompleteListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getMInputContent()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇08O〇00〇o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final setMCompleteListener(Lcom/intsig/view/VerifyCodeRectStyleView$VerifyCodeInputCompleteListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->OO:Lcom/intsig/view/VerifyCodeRectStyleView$VerifyCodeInputCompleteListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final setMInputContent(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/view/VerifyCodeRectStyleView;->〇08O〇00〇o:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

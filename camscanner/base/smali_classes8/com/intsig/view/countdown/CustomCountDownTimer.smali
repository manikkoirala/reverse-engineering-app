.class public abstract Lcom/intsig/view/countdown/CustomCountDownTimer;
.super Ljava/lang/Object;
.source "CustomCountDownTimer.java"


# instance fields
.field private O8:J

.field private Oo08:Z

.field private o〇0:Z

.field private final 〇080:J

.field private final 〇o00〇〇Oo:J

.field private 〇o〇:J

.field private 〇〇888:Landroid/os/Handler;


# direct methods
.method public constructor <init>(JJ)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->Oo08:Z

    .line 6
    .line 7
    iput-boolean v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->o〇0:Z

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/view/countdown/CustomCountDownTimer$1;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/intsig/view/countdown/CustomCountDownTimer$1;-><init>(Lcom/intsig/view/countdown/CustomCountDownTimer;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇〇888:Landroid/os/Handler;

    .line 15
    .line 16
    const-wide/16 v0, 0x3e8

    .line 17
    .line 18
    cmp-long v2, p3, v0

    .line 19
    .line 20
    if-lez v2, :cond_0

    .line 21
    .line 22
    const-wide/16 v0, 0xf

    .line 23
    .line 24
    add-long/2addr p1, v0

    .line 25
    :cond_0
    iput-wide p1, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇080:J

    .line 26
    .line 27
    iput-wide p3, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇o00〇〇Oo:J

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic O8(Lcom/intsig/view/countdown/CustomCountDownTimer;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇o〇:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private declared-synchronized oO80(J)Lcom/intsig/view/countdown/CustomCountDownTimer;
    .locals 3

    .line 1
    monitor-enter p0

    .line 2
    const/4 v0, 0x0

    .line 3
    :try_start_0
    iput-boolean v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->Oo08:Z

    .line 4
    .line 5
    const-wide/16 v0, 0x0

    .line 6
    .line 7
    cmp-long v2, p1, v0

    .line 8
    .line 9
    if-gtz v2, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/view/countdown/CustomCountDownTimer;->Oo08()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    .line 13
    .line 14
    monitor-exit p0

    .line 15
    return-object p0

    .line 16
    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    add-long/2addr v0, p1

    .line 21
    iput-wide v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇o〇:J

    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇〇888:Landroid/os/Handler;

    .line 24
    .line 25
    const/4 p2, 0x1

    .line 26
    invoke-virtual {p1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 31
    .line 32
    .line 33
    monitor-exit p0

    .line 34
    return-object p0

    .line 35
    :catchall_0
    move-exception p1

    .line 36
    monitor-exit p0

    .line 37
    throw p1
    .line 38
.end method

.method static bridge synthetic 〇080(Lcom/intsig/view/countdown/CustomCountDownTimer;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->o〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/view/countdown/CustomCountDownTimer;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->Oo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/view/countdown/CustomCountDownTimer;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇o00〇〇Oo:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public final declared-synchronized OO0o〇〇〇〇0()V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    const/4 v0, 0x1

    .line 3
    :try_start_0
    iput-boolean v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->Oo08:Z

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇〇888:Landroid/os/Handler;

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    .line 9
    .line 10
    monitor-exit p0

    .line 11
    return-void

    .line 12
    :catchall_0
    move-exception v0

    .line 13
    monitor-exit p0

    .line 14
    throw v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract Oo08()V
.end method

.method public abstract o〇0(J)V
.end method

.method public final declared-synchronized 〇80〇808〇O()V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-wide v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇080:J

    .line 3
    .line 4
    invoke-direct {p0, v0, v1}, Lcom/intsig/view/countdown/CustomCountDownTimer;->oO80(J)Lcom/intsig/view/countdown/CustomCountDownTimer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5
    .line 6
    .line 7
    monitor-exit p0

    .line 8
    return-void

    .line 9
    :catchall_0
    move-exception v0

    .line 10
    monitor-exit p0

    .line 11
    throw v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final declared-synchronized 〇〇888()V
    .locals 5

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->Oo08:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    monitor-exit p0

    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x1

    .line 9
    :try_start_1
    iput-boolean v0, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->o〇0:Z

    .line 10
    .line 11
    iget-wide v1, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇o〇:J

    .line 12
    .line 13
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 14
    .line 15
    .line 16
    move-result-wide v3

    .line 17
    sub-long/2addr v1, v3

    .line 18
    iput-wide v1, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->O8:J

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇〇888:Landroid/os/Handler;

    .line 21
    .line 22
    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23
    .line 24
    .line 25
    monitor-exit p0

    .line 26
    return-void

    .line 27
    :catchall_0
    move-exception v0

    .line 28
    monitor-exit p0

    .line 29
    throw v0
    .line 30
.end method

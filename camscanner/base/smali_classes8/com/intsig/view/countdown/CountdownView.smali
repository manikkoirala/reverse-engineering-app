.class public Lcom/intsig/view/countdown/CountdownView;
.super Landroid/view/View;
.source "CountdownView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/view/countdown/CountdownView$OnCountdownIntervalListener;,
        Lcom/intsig/view/countdown/CountdownView$OnCountdownEndListener;
    }
.end annotation


# instance fields
.field private O8o08O8O:J

.field private OO:Lcom/intsig/view/countdown/CountdownView$OnCountdownEndListener;

.field private o0:Lcom/intsig/view/countdown/BaseCountdown;

.field private o〇00O:Z

.field private 〇080OO8〇0:J

.field private 〇08O〇00〇o:Lcom/intsig/view/countdown/CountdownView$OnCountdownIntervalListener;

.field private 〇0O:J

.field private 〇OOo8〇0:Lcom/intsig/view/countdown/CustomCountDownTimer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/view/countdown/CountdownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 3
    sget-object p3, Lcom/intsig/comm/R$styleable;->CountdownView:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 4
    sget p3, Lcom/intsig/comm/R$styleable;->CountdownView_isHideTimeBackground:I

    const/4 v0, 0x1

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/intsig/view/countdown/CountdownView;->o〇00O:Z

    if-eqz p3, :cond_0

    .line 5
    new-instance p3, Lcom/intsig/view/countdown/BaseCountdown;

    invoke-direct {p3}, Lcom/intsig/view/countdown/BaseCountdown;-><init>()V

    goto :goto_0

    :cond_0
    new-instance p3, Lcom/intsig/view/countdown/BackgroundCountdown;

    invoke-direct {p3}, Lcom/intsig/view/countdown/BackgroundCountdown;-><init>()V

    :goto_0
    iput-object p3, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 6
    invoke-virtual {p3, p1, p2}, Lcom/intsig/view/countdown/BaseCountdown;->〇80〇808〇O(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 7
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 8
    iget-object p1, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    invoke-virtual {p1}, Lcom/intsig/view/countdown/BaseCountdown;->〇O〇()V

    return-void
.end method

.method private Oo08()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/view/countdown/BaseCountdown;->〇0〇O0088o()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private o〇0(J)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 2
    .line 3
    iget-boolean v1, v0, Lcom/intsig/view/countdown/BaseCountdown;->〇8o8o〇:Z

    .line 4
    .line 5
    const-wide/32 v2, 0x36ee80

    .line 6
    .line 7
    .line 8
    if-nez v1, :cond_0

    .line 9
    .line 10
    const-wide/32 v4, 0x5265c00

    .line 11
    .line 12
    .line 13
    div-long v6, p1, v4

    .line 14
    .line 15
    long-to-int v1, v6

    .line 16
    rem-long v4, p1, v4

    .line 17
    .line 18
    div-long/2addr v4, v2

    .line 19
    long-to-int v5, v4

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    div-long v4, p1, v2

    .line 22
    .line 23
    long-to-int v1, v4

    .line 24
    const/4 v4, 0x0

    .line 25
    move v5, v1

    .line 26
    const/4 v1, 0x0

    .line 27
    :goto_0
    rem-long v2, p1, v2

    .line 28
    .line 29
    const-wide/32 v6, 0xea60

    .line 30
    .line 31
    .line 32
    div-long/2addr v2, v6

    .line 33
    long-to-int v3, v2

    .line 34
    rem-long v6, p1, v6

    .line 35
    .line 36
    const-wide/16 v8, 0x3e8

    .line 37
    .line 38
    div-long/2addr v6, v8

    .line 39
    long-to-int v4, v6

    .line 40
    rem-long/2addr p1, v8

    .line 41
    long-to-int p2, p1

    .line 42
    move v2, v5

    .line 43
    move v5, p2

    .line 44
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/view/countdown/BaseCountdown;->o800o8O(IIIII)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method static bridge synthetic 〇080(Lcom/intsig/view/countdown/CountdownView;)Lcom/intsig/view/countdown/CountdownView$OnCountdownEndListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/view/countdown/CountdownView;->OO:Lcom/intsig/view/countdown/CountdownView$OnCountdownEndListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private 〇o〇(III)I
    .locals 2

    .line 1
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 6
    .line 7
    .line 8
    move-result p3

    .line 9
    const/high16 v1, 0x40000000    # 2.0f

    .line 10
    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    goto :goto_1

    .line 18
    :cond_0
    const/4 p3, 0x1

    .line 19
    if-ne p1, p3, :cond_1

    .line 20
    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    .line 26
    .line 27
    .line 28
    move-result p3

    .line 29
    goto :goto_0

    .line 30
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 35
    .line 36
    .line 37
    move-result p3

    .line 38
    :goto_0
    add-int/2addr p1, p3

    .line 39
    add-int/2addr p1, p2

    .line 40
    :goto_1
    return p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public O8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->〇OOo8〇0:Lcom/intsig/view/countdown/CustomCountDownTimer;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇〇888()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public OO0o〇〇〇〇0(J)V
    .locals 8

    .line 1
    iput-wide p1, p0, Lcom/intsig/view/countdown/CountdownView;->〇0O:J

    .line 2
    .line 3
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/countdown/CountdownView;->o〇0(J)V

    .line 4
    .line 5
    .line 6
    iget-wide v0, p0, Lcom/intsig/view/countdown/CountdownView;->〇080OO8〇0:J

    .line 7
    .line 8
    const-wide/16 v2, 0x0

    .line 9
    .line 10
    cmp-long v4, v0, v2

    .line 11
    .line 12
    if-lez v4, :cond_1

    .line 13
    .line 14
    iget-object v4, p0, Lcom/intsig/view/countdown/CountdownView;->〇08O〇00〇o:Lcom/intsig/view/countdown/CountdownView$OnCountdownIntervalListener;

    .line 15
    .line 16
    if-eqz v4, :cond_1

    .line 17
    .line 18
    iget-wide v5, p0, Lcom/intsig/view/countdown/CountdownView;->O8o08O8O:J

    .line 19
    .line 20
    cmp-long v7, v5, v2

    .line 21
    .line 22
    if-nez v7, :cond_0

    .line 23
    .line 24
    iput-wide p1, p0, Lcom/intsig/view/countdown/CountdownView;->O8o08O8O:J

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    add-long/2addr v0, p1

    .line 28
    cmp-long v2, v0, v5

    .line 29
    .line 30
    if-gtz v2, :cond_1

    .line 31
    .line 32
    iput-wide p1, p0, Lcom/intsig/view/countdown/CountdownView;->O8o08O8O:J

    .line 33
    .line 34
    iget-wide p1, p0, Lcom/intsig/view/countdown/CountdownView;->〇0O:J

    .line 35
    .line 36
    invoke-interface {v4, p0, p1, p2}, Lcom/intsig/view/countdown/CountdownView$OnCountdownIntervalListener;->〇080(Lcom/intsig/view/countdown/CountdownView;J)V

    .line 37
    .line 38
    .line 39
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/view/countdown/BaseCountdown;->o〇0()Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-nez p1, :cond_3

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/view/countdown/BaseCountdown;->〇〇888()Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-eqz p1, :cond_2

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 57
    .line 58
    .line 59
    goto :goto_2

    .line 60
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/intsig/view/countdown/CountdownView;->Oo08()V

    .line 61
    .line 62
    .line 63
    :goto_2
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getDay()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/view/countdown/BaseCountdown;->〇080:I

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHour()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/view/countdown/BaseCountdown;->〇o00〇〇Oo:I

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMinute()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/view/countdown/BaseCountdown;->〇o〇:I

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRemainTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/view/countdown/CountdownView;->〇0O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSecond()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/view/countdown/BaseCountdown;->O8:I

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public oO80(J)V
    .locals 8

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    cmp-long v2, p1, v0

    .line 4
    .line 5
    if-gtz v2, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iput-wide v0, p0, Lcom/intsig/view/countdown/CountdownView;->O8o08O8O:J

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->〇OOo8〇0:Lcom/intsig/view/countdown/CustomCountDownTimer;

    .line 11
    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/view/countdown/CustomCountDownTimer;->OO0o〇〇〇〇0()V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->〇OOo8〇0:Lcom/intsig/view/countdown/CustomCountDownTimer;

    .line 19
    .line 20
    :cond_1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 21
    .line 22
    iget-boolean v0, v0, Lcom/intsig/view/countdown/BaseCountdown;->OO0o〇〇〇〇0:Z

    .line 23
    .line 24
    if-eqz v0, :cond_2

    .line 25
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/intsig/view/countdown/CountdownView;->OO0o〇〇〇〇0(J)V

    .line 27
    .line 28
    .line 29
    const-wide/16 v0, 0xa

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_2
    const-wide/16 v0, 0x3e8

    .line 33
    .line 34
    :goto_0
    move-wide v6, v0

    .line 35
    new-instance v0, Lcom/intsig/view/countdown/CountdownView$1;

    .line 36
    .line 37
    move-object v2, v0

    .line 38
    move-object v3, p0

    .line 39
    move-wide v4, p1

    .line 40
    invoke-direct/range {v2 .. v7}, Lcom/intsig/view/countdown/CountdownView$1;-><init>(Lcom/intsig/view/countdown/CountdownView;JJ)V

    .line 41
    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->〇OOo8〇0:Lcom/intsig/view/countdown/CustomCountDownTimer;

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇80〇808〇O()V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/view/countdown/CountdownView;->〇80〇808〇O()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/view/countdown/BaseCountdown;->〇O00(Landroid/graphics/Canvas;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected onMeasure(II)V
    .locals 7

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/view/countdown/BaseCountdown;->〇o00〇〇Oo()I

    .line 7
    .line 8
    .line 9
    move-result v5

    .line 10
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/view/countdown/BaseCountdown;->〇080()I

    .line 13
    .line 14
    .line 15
    move-result v6

    .line 16
    const/4 v0, 0x1

    .line 17
    invoke-direct {p0, v0, v5, p1}, Lcom/intsig/view/countdown/CountdownView;->〇o〇(III)I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    const/4 p1, 0x2

    .line 22
    invoke-direct {p0, p1, v6, p2}, Lcom/intsig/view/countdown/CountdownView;->〇o〇(III)I

    .line 23
    .line 24
    .line 25
    move-result v4

    .line 26
    invoke-virtual {p0, v3, v4}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 30
    .line 31
    move-object v2, p0

    .line 32
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/view/countdown/BaseCountdown;->〇〇8O0〇8(Landroid/view/View;IIII)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setOnCountdownEndListener(Lcom/intsig/view/countdown/CountdownView$OnCountdownEndListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/view/countdown/CountdownView;->OO:Lcom/intsig/view/countdown/CountdownView$OnCountdownEndListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRemainTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/view/countdown/CountdownView;->〇0O:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇80〇808〇O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->〇OOo8〇0:Lcom/intsig/view/countdown/CustomCountDownTimer;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/view/countdown/CustomCountDownTimer;->OO0o〇〇〇〇0()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇o00〇〇Oo()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/view/countdown/CountdownView;->o0:Lcom/intsig/view/countdown/BaseCountdown;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x0

    .line 5
    const/4 v3, 0x0

    .line 6
    const/4 v4, 0x0

    .line 7
    const/4 v5, 0x0

    .line 8
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/view/countdown/BaseCountdown;->o800o8O(IIIII)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇〇888(JLcom/intsig/view/countdown/CountdownView$OnCountdownIntervalListener;)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/view/countdown/CountdownView;->〇080OO8〇0:J

    .line 2
    .line 3
    iput-object p3, p0, Lcom/intsig/view/countdown/CountdownView;->〇08O〇00〇o:Lcom/intsig/view/countdown/CountdownView$OnCountdownIntervalListener;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

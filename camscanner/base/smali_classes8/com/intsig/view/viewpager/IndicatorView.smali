.class public final Lcom/intsig/view/viewpager/IndicatorView;
.super Landroid/view/View;
.source "IndicatorView.kt"

# interfaces
.implements Lcom/intsig/view/viewpager/Indicator;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O0O:F

.field private O8o08O8O:I

.field private OO:Landroid/graphics/Path;

.field private OO〇00〇8oO:Landroid/widget/RelativeLayout$LayoutParams;

.field private final o0:Landroid/view/animation/Interpolator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8oOOo:F

.field private o8〇OO0〇0o:Z

.field private final oOo0:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooo0〇〇O:F

.field private o〇00O:I

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:F

.field private 〇0O:I

.field private 〇8〇oO〇〇8o:I

.field private 〇OOo8〇0:Landroid/view/animation/Interpolator;

.field private 〇O〇〇O8:F

.field private 〇〇08O:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o0:Landroid/view/animation/Interpolator;

    const p1, -0x777778

    .line 3
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇080OO8〇0:I

    const/4 p1, -0x1

    .line 4
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇0O:I

    .line 5
    new-instance p1, Landroid/graphics/Paint;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 6
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    const/high16 p1, 0x40600000    # 3.5f

    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    move-result p2

    int-to-float p2, p2

    iput p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    const/high16 p2, 0x3f800000    # 1.0f

    .line 8
    iput p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇〇08O:F

    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 10
    iput p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8oOOo:F

    const/high16 p1, 0x40800000    # 4.0f

    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇O〇〇O8:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 13
    new-instance p1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o0:Landroid/view/animation/Interpolator;

    const p1, -0x777778

    .line 14
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇080OO8〇0:I

    const/4 p1, -0x1

    .line 15
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇0O:I

    .line 16
    new-instance p1, Landroid/graphics/Paint;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 17
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    const/high16 p1, 0x40600000    # 3.5f

    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    move-result p2

    int-to-float p2, p2

    iput p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    const/high16 p2, 0x3f800000    # 1.0f

    .line 19
    iput p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇〇08O:F

    .line 20
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 21
    iput p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8oOOo:F

    const/high16 p1, 0x40800000    # 4.0f

    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇O〇〇O8:F

    return-void
.end method

.method private final O8(Landroid/graphics/Canvas;F)V
    .locals 10

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/viewpager/IndicatorView;->oO80(Landroid/graphics/Canvas;F)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->OO0o〇〇〇〇0()F

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 9
    .line 10
    invoke-direct {p0, v1}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    iget v2, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 15
    .line 16
    add-int/lit8 v2, v2, 0x1

    .line 17
    .line 18
    iget v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 19
    .line 20
    rem-int/2addr v2, v3

    .line 21
    invoke-direct {p0, v2}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    iget v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 30
    .line 31
    iget v5, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8oOOo:F

    .line 32
    .line 33
    mul-float v5, v5, v4

    .line 34
    .line 35
    sub-float v6, v5, v3

    .line 36
    .line 37
    mul-float v6, v6, v0

    .line 38
    .line 39
    sub-float/2addr v5, v6

    .line 40
    add-float/2addr v3, v6

    .line 41
    iget v6, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 42
    .line 43
    sub-float v6, v4, v6

    .line 44
    .line 45
    mul-float v6, v6, v0

    .line 46
    .line 47
    iget-object v7, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 48
    .line 49
    iget v8, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇0O:I

    .line 50
    .line 51
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    .line 53
    .line 54
    const v7, 0x3f7d70a4    # 0.99f

    .line 55
    .line 56
    .line 57
    cmpg-float v7, v0, v7

    .line 58
    .line 59
    if-gez v7, :cond_0

    .line 60
    .line 61
    sub-float v7, p2, v4

    .line 62
    .line 63
    add-float/2addr v7, v6

    .line 64
    sub-float v8, v1, v5

    .line 65
    .line 66
    add-float/2addr v1, v5

    .line 67
    add-float/2addr v4, p2

    .line 68
    sub-float/2addr v4, v6

    .line 69
    iget-object v9, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 70
    .line 71
    invoke-virtual {v9, v8, v7, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 72
    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 75
    .line 76
    iget-object v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 77
    .line 78
    invoke-virtual {p1, v1, v5, v5, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 79
    .line 80
    .line 81
    :cond_0
    const v1, 0x3dcccccd    # 0.1f

    .line 82
    .line 83
    .line 84
    cmpl-float v0, v0, v1

    .line 85
    .line 86
    if-lez v0, :cond_1

    .line 87
    .line 88
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 89
    .line 90
    sub-float v1, p2, v0

    .line 91
    .line 92
    sub-float/2addr v1, v6

    .line 93
    sub-float v4, v2, v3

    .line 94
    .line 95
    add-float/2addr v2, v3

    .line 96
    add-float/2addr p2, v0

    .line 97
    add-float/2addr p2, v6

    .line 98
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 99
    .line 100
    invoke-virtual {v0, v4, v1, v2, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 101
    .line 102
    .line 103
    iget-object p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 106
    .line 107
    invoke-virtual {p1, p2, v3, v3, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 108
    .line 109
    .line 110
    :cond_1
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private final OO0o〇〇〇〇0()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->o0:Landroid/view/animation/Interpolator;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇08O〇00〇o:F

    .line 4
    .line 5
    invoke-interface {v0, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private final Oo08(Landroid/graphics/Canvas;F)V
    .locals 5

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/viewpager/IndicatorView;->oO80(Landroid/graphics/Canvas;F)V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 11
    .line 12
    add-int/lit8 v1, v1, 0x1

    .line 13
    .line 14
    iget v2, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 15
    .line 16
    rem-int/2addr v1, v2

    .line 17
    invoke-direct {p0, v1}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioSelectedRadius()F

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    sub-float v3, v0, v2

    .line 26
    .line 27
    add-float/2addr v0, v2

    .line 28
    sub-float v4, v1, v2

    .line 29
    .line 30
    add-float/2addr v1, v2

    .line 31
    sub-float/2addr v4, v3

    .line 32
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->OO0o〇〇〇〇0()F

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    mul-float v4, v4, v2

    .line 37
    .line 38
    add-float/2addr v3, v4

    .line 39
    sub-float/2addr v1, v0

    .line 40
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->OO0o〇〇〇〇0()F

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    mul-float v1, v1, v2

    .line 45
    .line 46
    add-float/2addr v0, v1

    .line 47
    iget-object v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 48
    .line 49
    iget v2, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 50
    .line 51
    sub-float v4, p2, v2

    .line 52
    .line 53
    add-float/2addr p2, v2

    .line 54
    invoke-virtual {v1, v3, v4, v0, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 55
    .line 56
    .line 57
    iget-boolean p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8〇OO0〇0o:Z

    .line 58
    .line 59
    if-nez p2, :cond_1

    .line 60
    .line 61
    const/4 p2, 0x0

    .line 62
    invoke-direct {p0, p2}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 63
    .line 64
    .line 65
    move-result p2

    .line 66
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 67
    .line 68
    add-int/lit8 v1, v1, -0x1

    .line 69
    .line 70
    invoke-direct {p0, v1}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 75
    .line 76
    .line 77
    move-result v2

    .line 78
    sub-float v2, p2, v2

    .line 79
    .line 80
    const/4 v4, 0x0

    .line 81
    cmpg-float v2, v3, v2

    .line 82
    .line 83
    if-gez v2, :cond_0

    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 86
    .line 87
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    sub-float/2addr p2, v1

    .line 92
    sub-float/2addr p2, v3

    .line 93
    invoke-virtual {v0, p2, v4}, Landroid/graphics/RectF;->offset(FF)V

    .line 94
    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_0
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 98
    .line 99
    .line 100
    move-result p2

    .line 101
    add-float/2addr p2, v1

    .line 102
    cmpl-float p2, v0, p2

    .line 103
    .line 104
    if-lez p2, :cond_1

    .line 105
    .line 106
    iget-object p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 109
    .line 110
    .line 111
    move-result v2

    .line 112
    add-float/2addr v1, v2

    .line 113
    sub-float/2addr v1, v0

    .line 114
    invoke-virtual {p2, v1, v4}, Landroid/graphics/RectF;->offset(FF)V

    .line 115
    .line 116
    .line 117
    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 118
    .line 119
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇0O:I

    .line 120
    .line 121
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 122
    .line 123
    .line 124
    iget-object p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 125
    .line 126
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 127
    .line 128
    iget-object v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 129
    .line 130
    invoke-virtual {p1, p2, v0, v0, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 131
    .line 132
    .line 133
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private final getRatioRadius()F
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇〇08O:F

    .line 4
    .line 5
    mul-float v0, v0, v1

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private final getRatioSelectedRadius()F
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8oOOo:F

    .line 4
    .line 5
    mul-float v0, v0, v1

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private final oO80(Landroid/graphics/Canvas;F)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇080OO8〇0:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    :goto_0
    if-ge v1, v0, :cond_0

    .line 12
    .line 13
    invoke-direct {p0, v1}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    sub-float v4, v2, v3

    .line 22
    .line 23
    iget v5, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 24
    .line 25
    sub-float v6, p2, v5

    .line 26
    .line 27
    add-float/2addr v2, v3

    .line 28
    add-float/2addr v5, p2

    .line 29
    iget-object v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 30
    .line 31
    invoke-virtual {v3, v4, v6, v2, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 32
    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 35
    .line 36
    iget v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 37
    .line 38
    iget-object v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 39
    .line 40
    invoke-virtual {p1, v2, v3, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 41
    .line 42
    .line 43
    add-int/lit8 v1, v1, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇0(Landroid/graphics/Canvas;F)V
    .locals 10

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/viewpager/IndicatorView;->oO80(Landroid/graphics/Canvas;F)V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioSelectedRadius()F

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    sub-float v2, v0, v1

    .line 15
    .line 16
    add-float/2addr v0, v1

    .line 17
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->OO0o〇〇〇〇0()F

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    iget v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇O〇〇O8:F

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 24
    .line 25
    .line 26
    move-result v5

    .line 27
    invoke-static {v5, v1}, Ljava/lang/Math;->max(FF)F

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    const/4 v5, 0x2

    .line 32
    int-to-float v5, v5

    .line 33
    mul-float v1, v1, v5

    .line 34
    .line 35
    add-float/2addr v4, v1

    .line 36
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 37
    .line 38
    add-int/lit8 v6, v1, 0x1

    .line 39
    .line 40
    iget v7, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 41
    .line 42
    rem-int/2addr v6, v7

    .line 43
    const/4 v7, 0x0

    .line 44
    const/high16 v8, 0x40000000    # 2.0f

    .line 45
    .line 46
    const/high16 v9, 0x3f000000    # 0.5f

    .line 47
    .line 48
    if-nez v6, :cond_0

    .line 49
    .line 50
    int-to-float v1, v1

    .line 51
    neg-float v1, v1

    .line 52
    mul-float v4, v4, v1

    .line 53
    .line 54
    mul-float v1, v4, v3

    .line 55
    .line 56
    mul-float v1, v1, v5

    .line 57
    .line 58
    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    add-float/2addr v2, v1

    .line 63
    sub-float/2addr v3, v9

    .line 64
    mul-float v4, v4, v3

    .line 65
    .line 66
    mul-float v4, v4, v8

    .line 67
    .line 68
    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    goto :goto_0

    .line 73
    :cond_0
    sub-float v1, v3, v9

    .line 74
    .line 75
    mul-float v1, v1, v4

    .line 76
    .line 77
    mul-float v1, v1, v8

    .line 78
    .line 79
    invoke-static {v1, v7}, Ljava/lang/Math;->max(FF)F

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    add-float/2addr v2, v1

    .line 84
    mul-float v3, v3, v4

    .line 85
    .line 86
    mul-float v3, v3, v5

    .line 87
    .line 88
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    :goto_0
    add-float/2addr v0, v1

    .line 93
    iget-object v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 94
    .line 95
    iget v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 96
    .line 97
    sub-float v4, p2, v3

    .line 98
    .line 99
    add-float/2addr p2, v3

    .line 100
    invoke-virtual {v1, v2, v4, v0, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 101
    .line 102
    .line 103
    iget-object p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 104
    .line 105
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇0O:I

    .line 106
    .line 107
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 108
    .line 109
    .line 110
    iget-object p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 111
    .line 112
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 113
    .line 114
    iget-object v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 115
    .line 116
    invoke-virtual {p1, p2, v0, v0, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private final 〇80〇808〇O(I)F
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioSelectedRadius()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/high16 v2, 0x40000000    # 2.0f

    .line 14
    .line 15
    mul-float v2, v2, v1

    .line 16
    .line 17
    iget v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇O〇〇O8:F

    .line 18
    .line 19
    add-float/2addr v2, v3

    .line 20
    iget-boolean v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8〇OO0〇0o:Z

    .line 21
    .line 22
    if-eqz v3, :cond_0

    .line 23
    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    int-to-float v3, v3

    .line 29
    add-float/2addr v3, v1

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    int-to-float v3, v3

    .line 36
    :goto_0
    int-to-float p1, p1

    .line 37
    mul-float v2, v2, p1

    .line 38
    .line 39
    add-float/2addr v3, v2

    .line 40
    iget p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇8〇oO〇〇8o:I

    .line 41
    .line 42
    const/4 v2, 0x3

    .line 43
    if-ne p1, v2, :cond_1

    .line 44
    .line 45
    const/4 p1, 0x0

    .line 46
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    goto :goto_1

    .line 51
    :cond_1
    sub-float/2addr v1, v0

    .line 52
    const/4 p1, 0x2

    .line 53
    int-to-float p1, p1

    .line 54
    div-float/2addr v1, p1

    .line 55
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    add-float/2addr v3, p1

    .line 64
    return v3
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private final 〇8o8o〇(I)I
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/high16 v1, -0x80000000

    .line 10
    .line 11
    if-eq v0, v1, :cond_0

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/high16 v1, 0x40000000    # 2.0f

    .line 16
    .line 17
    if-eq v0, v1, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioSelectedRadius()F

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    const/4 v0, 0x2

    .line 34
    int-to-float v0, v0

    .line 35
    mul-float p1, p1, v0

    .line 36
    .line 37
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    int-to-float v0, v0

    .line 42
    add-float/2addr p1, v0

    .line 43
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    int-to-float v0, v0

    .line 48
    add-float/2addr p1, v0

    .line 49
    float-to-int p1, p1

    .line 50
    :cond_1
    :goto_0
    return p1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private final 〇O8o08O(I)I
    .locals 4

    .line 1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/high16 v1, -0x80000000

    .line 10
    .line 11
    if-eq v0, v1, :cond_0

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/high16 v1, 0x40000000    # 2.0f

    .line 16
    .line 17
    if-eq v0, v1, :cond_1

    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioSelectedRadius()F

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    const/4 v2, 0x2

    .line 34
    int-to-float v2, v2

    .line 35
    mul-float v1, v1, v2

    .line 36
    .line 37
    iget v2, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 38
    .line 39
    int-to-float v3, v2

    .line 40
    mul-float v1, v1, v3

    .line 41
    .line 42
    add-int/lit8 v2, v2, -0x1

    .line 43
    .line 44
    int-to-float v2, v2

    .line 45
    iget v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇O〇〇O8:F

    .line 46
    .line 47
    mul-float v2, v2, v3

    .line 48
    .line 49
    sub-float/2addr p1, v0

    .line 50
    add-float/2addr v1, v2

    .line 51
    add-float/2addr v1, p1

    .line 52
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    int-to-float p1, p1

    .line 57
    add-float/2addr v1, p1

    .line 58
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    int-to-float p1, p1

    .line 63
    add-float/2addr v1, p1

    .line 64
    float-to-int p1, v1

    .line 65
    :cond_1
    :goto_0
    return p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private final 〇o00〇〇Oo(F)I
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0, p1}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    float-to-int p1, p1

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private final 〇o〇(Landroid/graphics/Canvas;F)V
    .locals 12

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/viewpager/IndicatorView;->oO80(Landroid/graphics/Canvas;F)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇OOo8〇0:Landroid/view/animation/Interpolator;

    .line 16
    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    .line 20
    .line 21
    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇OOo8〇0:Landroid/view/animation/Interpolator;

    .line 25
    .line 26
    :cond_1
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 27
    .line 28
    invoke-direct {p0, v0}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 33
    .line 34
    add-int/lit8 v1, v1, 0x1

    .line 35
    .line 36
    iget v2, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 37
    .line 38
    rem-int/2addr v1, v2

    .line 39
    invoke-direct {p0, v1}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    sub-float/2addr v1, v0

    .line 44
    iget-object v2, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇OOo8〇0:Landroid/view/animation/Interpolator;

    .line 45
    .line 46
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    iget v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇08O〇00〇o:F

    .line 50
    .line 51
    invoke-interface {v2, v3}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    mul-float v2, v2, v1

    .line 56
    .line 57
    add-float/2addr v2, v0

    .line 58
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->OO0o〇〇〇〇0()F

    .line 59
    .line 60
    .line 61
    move-result v3

    .line 62
    mul-float v1, v1, v3

    .line 63
    .line 64
    add-float/2addr v0, v1

    .line 65
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioSelectedRadius()F

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    iget v3, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 70
    .line 71
    const v4, 0x3f11eb85    # 0.57f

    .line 72
    .line 73
    .line 74
    mul-float v3, v3, v4

    .line 75
    .line 76
    iget v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8oOOo:F

    .line 77
    .line 78
    mul-float v4, v4, v3

    .line 79
    .line 80
    sub-float v5, v4, v1

    .line 81
    .line 82
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->OO0o〇〇〇〇0()F

    .line 83
    .line 84
    .line 85
    move-result v6

    .line 86
    mul-float v5, v5, v6

    .line 87
    .line 88
    add-float/2addr v5, v1

    .line 89
    sub-float/2addr v1, v4

    .line 90
    iget-object v6, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇OOo8〇0:Landroid/view/animation/Interpolator;

    .line 91
    .line 92
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 93
    .line 94
    .line 95
    iget v7, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇08O〇00〇o:F

    .line 96
    .line 97
    invoke-interface {v6, v7}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    .line 98
    .line 99
    .line 100
    move-result v6

    .line 101
    mul-float v1, v1, v6

    .line 102
    .line 103
    add-float/2addr v4, v1

    .line 104
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 105
    .line 106
    sub-float/2addr v1, v3

    .line 107
    invoke-direct {p0}, Lcom/intsig/view/viewpager/IndicatorView;->OO0o〇〇〇〇0()F

    .line 108
    .line 109
    .line 110
    move-result v6

    .line 111
    mul-float v1, v1, v6

    .line 112
    .line 113
    iget v6, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 114
    .line 115
    sub-float/2addr v6, v3

    .line 116
    iget-object v7, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇OOo8〇0:Landroid/view/animation/Interpolator;

    .line 117
    .line 118
    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 119
    .line 120
    .line 121
    iget v8, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇08O〇00〇o:F

    .line 122
    .line 123
    invoke-interface {v7, v8}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    .line 124
    .line 125
    .line 126
    move-result v7

    .line 127
    mul-float v6, v6, v7

    .line 128
    .line 129
    iget-object v7, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 130
    .line 131
    iget v8, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇0O:I

    .line 132
    .line 133
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    .line 135
    .line 136
    iget-object v7, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 137
    .line 138
    sub-float v8, v2, v5

    .line 139
    .line 140
    iget v9, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 141
    .line 142
    sub-float v10, p2, v9

    .line 143
    .line 144
    add-float/2addr v10, v1

    .line 145
    add-float v11, v2, v5

    .line 146
    .line 147
    add-float/2addr v9, p2

    .line 148
    sub-float/2addr v9, v1

    .line 149
    invoke-virtual {v7, v8, v10, v11, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 150
    .line 151
    .line 152
    iget-object v7, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 153
    .line 154
    iget-object v8, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 155
    .line 156
    invoke-virtual {p1, v7, v5, v5, v8}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 157
    .line 158
    .line 159
    iget-object v5, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 160
    .line 161
    sub-float v7, v0, v4

    .line 162
    .line 163
    sub-float v8, p2, v3

    .line 164
    .line 165
    sub-float/2addr v8, v6

    .line 166
    add-float v9, v0, v4

    .line 167
    .line 168
    add-float/2addr v3, p2

    .line 169
    add-float/2addr v3, v6

    .line 170
    invoke-virtual {v5, v7, v8, v9, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 171
    .line 172
    .line 173
    iget-object v5, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 174
    .line 175
    iget-object v6, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 176
    .line 177
    invoke-virtual {p1, v5, v4, v4, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 178
    .line 179
    .line 180
    iget-object v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 181
    .line 182
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 186
    .line 187
    .line 188
    iget-object v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 189
    .line 190
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 191
    .line 192
    .line 193
    invoke-virtual {v4, v0, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 194
    .line 195
    .line 196
    iget-object v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 197
    .line 198
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 199
    .line 200
    .line 201
    invoke-virtual {v4, v0, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    .line 203
    .line 204
    iget-object v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 205
    .line 206
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 207
    .line 208
    .line 209
    sub-float v5, v2, v0

    .line 210
    .line 211
    const/high16 v6, 0x40000000    # 2.0f

    .line 212
    .line 213
    div-float/2addr v5, v6

    .line 214
    add-float/2addr v5, v0

    .line 215
    iget v6, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 216
    .line 217
    sub-float v6, p2, v6

    .line 218
    .line 219
    add-float/2addr v6, v1

    .line 220
    invoke-virtual {v4, v5, p2, v2, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 221
    .line 222
    .line 223
    iget-object v4, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 224
    .line 225
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 226
    .line 227
    .line 228
    iget v6, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 229
    .line 230
    add-float/2addr v6, p2

    .line 231
    sub-float/2addr v6, v1

    .line 232
    invoke-virtual {v4, v2, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 233
    .line 234
    .line 235
    iget-object v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 236
    .line 237
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 238
    .line 239
    .line 240
    invoke-virtual {v1, v5, p2, v0, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 241
    .line 242
    .line 243
    iget-object p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 244
    .line 245
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 246
    .line 247
    .line 248
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 249
    .line 250
    .line 251
    iget-object p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO:Landroid/graphics/Path;

    .line 252
    .line 253
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 254
    .line 255
    .line 256
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 257
    .line 258
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 259
    .line 260
    .line 261
    return-void
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private final 〇〇888(Landroid/graphics/Canvas;F)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    invoke-direct/range {p0 .. p0}, Lcom/intsig/view/viewpager/IndicatorView;->OO0o〇〇〇〇0()F

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    invoke-direct/range {p0 .. p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioSelectedRadius()F

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    invoke-direct/range {p0 .. p0}, Lcom/intsig/view/viewpager/IndicatorView;->getRatioRadius()F

    .line 14
    .line 15
    .line 16
    move-result v4

    .line 17
    sub-float v5, v3, v4

    .line 18
    .line 19
    mul-float v6, v5, v2

    .line 20
    .line 21
    iget v7, v0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 22
    .line 23
    const/4 v8, 0x1

    .line 24
    add-int/2addr v7, v8

    .line 25
    iget v9, v0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 26
    .line 27
    rem-int/2addr v7, v9

    .line 28
    const/4 v9, 0x0

    .line 29
    if-nez v7, :cond_0

    .line 30
    .line 31
    const/4 v10, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v10, 0x0

    .line 34
    :goto_0
    iget-object v11, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 35
    .line 36
    iget v12, v0, Lcom/intsig/view/viewpager/IndicatorView;->〇080OO8〇0:I

    .line 37
    .line 38
    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 39
    .line 40
    .line 41
    iget v11, v0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 42
    .line 43
    :goto_1
    if-ge v9, v11, :cond_3

    .line 44
    .line 45
    invoke-direct {v0, v9}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 46
    .line 47
    .line 48
    move-result v12

    .line 49
    if-eqz v10, :cond_1

    .line 50
    .line 51
    add-float/2addr v12, v6

    .line 52
    :cond_1
    sub-float v13, v12, v4

    .line 53
    .line 54
    iget v14, v0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 55
    .line 56
    sub-float v15, p2, v14

    .line 57
    .line 58
    add-float/2addr v12, v4

    .line 59
    add-float v14, p2, v14

    .line 60
    .line 61
    move/from16 v16, v4

    .line 62
    .line 63
    iget v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 64
    .line 65
    add-int/2addr v4, v8

    .line 66
    if-gt v4, v9, :cond_2

    .line 67
    .line 68
    iget-object v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 69
    .line 70
    add-float/2addr v13, v5

    .line 71
    add-float/2addr v12, v5

    .line 72
    invoke-virtual {v4, v13, v15, v12, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 73
    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_2
    iget-object v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 77
    .line 78
    invoke-virtual {v4, v13, v15, v12, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 79
    .line 80
    .line 81
    :goto_2
    iget-object v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 82
    .line 83
    iget v12, v0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 84
    .line 85
    iget-object v13, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 86
    .line 87
    invoke-virtual {v1, v4, v12, v12, v13}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 88
    .line 89
    .line 90
    add-int/lit8 v9, v9, 0x1

    .line 91
    .line 92
    move/from16 v4, v16

    .line 93
    .line 94
    goto :goto_1

    .line 95
    :cond_3
    iget-object v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 96
    .line 97
    iget v8, v0, Lcom/intsig/view/viewpager/IndicatorView;->〇0O:I

    .line 98
    .line 99
    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 100
    .line 101
    .line 102
    const v4, 0x3f7d70a4    # 0.99f

    .line 103
    .line 104
    .line 105
    const/4 v8, 0x2

    .line 106
    cmpg-float v4, v2, v4

    .line 107
    .line 108
    if-gez v4, :cond_5

    .line 109
    .line 110
    iget v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 111
    .line 112
    invoke-direct {v0, v4}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    sub-float/2addr v4, v3

    .line 117
    if-eqz v10, :cond_4

    .line 118
    .line 119
    add-float/2addr v4, v6

    .line 120
    :cond_4
    int-to-float v9, v8

    .line 121
    mul-float v9, v9, v3

    .line 122
    .line 123
    add-float/2addr v9, v4

    .line 124
    add-float/2addr v9, v5

    .line 125
    sub-float/2addr v9, v6

    .line 126
    iget-object v11, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 127
    .line 128
    iget v12, v0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 129
    .line 130
    sub-float v13, p2, v12

    .line 131
    .line 132
    add-float v12, p2, v12

    .line 133
    .line 134
    invoke-virtual {v11, v4, v13, v9, v12}, Landroid/graphics/RectF;->set(FFFF)V

    .line 135
    .line 136
    .line 137
    iget-object v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 138
    .line 139
    iget v9, v0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 140
    .line 141
    iget-object v11, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 142
    .line 143
    invoke-virtual {v1, v4, v9, v9, v11}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 144
    .line 145
    .line 146
    :cond_5
    const v4, 0x3dcccccd    # 0.1f

    .line 147
    .line 148
    .line 149
    cmpl-float v2, v2, v4

    .line 150
    .line 151
    if-lez v2, :cond_7

    .line 152
    .line 153
    invoke-direct {v0, v7}, Lcom/intsig/view/viewpager/IndicatorView;->〇80〇808〇O(I)F

    .line 154
    .line 155
    .line 156
    move-result v2

    .line 157
    add-float/2addr v2, v3

    .line 158
    if-eqz v10, :cond_6

    .line 159
    .line 160
    move v5, v6

    .line 161
    :cond_6
    add-float/2addr v2, v5

    .line 162
    int-to-float v4, v8

    .line 163
    mul-float v3, v3, v4

    .line 164
    .line 165
    sub-float v3, v2, v3

    .line 166
    .line 167
    sub-float/2addr v3, v6

    .line 168
    iget-object v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 169
    .line 170
    iget v5, v0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 171
    .line 172
    sub-float v6, p2, v5

    .line 173
    .line 174
    add-float v5, p2, v5

    .line 175
    .line 176
    invoke-virtual {v4, v3, v6, v2, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 177
    .line 178
    .line 179
    iget-object v2, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo0:Landroid/graphics/RectF;

    .line 180
    .line 181
    iget v3, v0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 182
    .line 183
    iget-object v4, v0, Lcom/intsig/view/viewpager/IndicatorView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 184
    .line 185
    invoke-virtual {v1, v2, v3, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 186
    .line 187
    .line 188
    :cond_7
    return-void
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method


# virtual methods
.method public final OO0o〇〇(I)Lcom/intsig/view/viewpager/IndicatorView;
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇080OO8〇0:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final OoO8(I)Lcom/intsig/view/viewpager/IndicatorView;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final Oooo8o0〇(F)Lcom/intsig/view/viewpager/IndicatorView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 8
    .line 9
    cmpg-float v0, v0, v1

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    if-eqz v0, :cond_1

    .line 17
    .line 18
    int-to-float v0, p1

    .line 19
    iput v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 20
    .line 21
    :cond_1
    int-to-float p1, p1

    .line 22
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->ooo0〇〇O:F

    .line 23
    .line 24
    return-object p0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParams()Landroid/widget/RelativeLayout$LayoutParams;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO〇00〇8oO:Landroid/widget/RelativeLayout$LayoutParams;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 6
    .line 7
    const/4 v1, -0x2

    .line 8
    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO〇00〇8oO:Landroid/widget/RelativeLayout$LayoutParams;

    .line 12
    .line 13
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    const/16 v1, 0xc

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO〇00〇8oO:Landroid/widget/RelativeLayout$LayoutParams;

    .line 22
    .line 23
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    const/16 v1, 0xe

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO〇00〇8oO:Landroid/widget/RelativeLayout$LayoutParams;

    .line 32
    .line 33
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    const/high16 v1, 0x41200000    # 10.0f

    .line 37
    .line 38
    invoke-direct {p0, v1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 43
    .line 44
    :cond_0
    iget-object v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->OO〇00〇8oO:Landroid/widget/RelativeLayout$LayoutParams;

    .line 45
    .line 46
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    return-object v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getView()Landroid/view/View;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 7
    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    int-to-float v0, v0

    .line 19
    const/high16 v1, 0x40000000    # 2.0f

    .line 20
    .line 21
    div-float/2addr v0, v1

    .line 22
    const/high16 v1, 0x3f000000    # 0.5f

    .line 23
    .line 24
    add-float/2addr v0, v1

    .line 25
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇8〇oO〇〇8o:I

    .line 26
    .line 27
    if-nez v1, :cond_1

    .line 28
    .line 29
    invoke-direct {p0, p1, v0}, Lcom/intsig/view/viewpager/IndicatorView;->Oo08(Landroid/graphics/Canvas;F)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const/4 v2, 0x1

    .line 34
    if-ne v1, v2, :cond_2

    .line 35
    .line 36
    invoke-direct {p0, p1, v0}, Lcom/intsig/view/viewpager/IndicatorView;->o〇0(Landroid/graphics/Canvas;F)V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_2
    const/4 v2, 0x2

    .line 41
    if-ne v1, v2, :cond_3

    .line 42
    .line 43
    invoke-direct {p0, p1, v0}, Lcom/intsig/view/viewpager/IndicatorView;->〇o〇(Landroid/graphics/Canvas;F)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_3
    const/4 v2, 0x3

    .line 48
    if-ne v1, v2, :cond_4

    .line 49
    .line 50
    invoke-direct {p0, p1, v0}, Lcom/intsig/view/viewpager/IndicatorView;->〇〇888(Landroid/graphics/Canvas;F)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_4
    const/4 v2, 0x4

    .line 55
    if-ne v1, v2, :cond_5

    .line 56
    .line 57
    invoke-direct {p0, p1, v0}, Lcom/intsig/view/viewpager/IndicatorView;->O8(Landroid/graphics/Canvas;F)V

    .line 58
    .line 59
    .line 60
    :cond_5
    :goto_0
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇O8o08O(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    invoke-direct {p0, p2}, Lcom/intsig/view/viewpager/IndicatorView;->〇8o8o〇(I)I

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o〇00O:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇08O〇00〇o:F

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public onPageSelected(I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇080(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->O8o08O8O:I

    .line 2
    .line 3
    const/4 p2, 0x1

    .line 4
    if-le p1, p2, :cond_0

    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/16 p1, 0x8

    .line 9
    .line 10
    :goto_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final 〇0〇O0088o(F)Lcom/intsig/view/viewpager/IndicatorView;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    int-to-float p1, p1

    .line 6
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇O〇〇O8:F

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final 〇O00(F)Lcom/intsig/view/viewpager/IndicatorView;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8oOOo:F

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final 〇O〇(F)Lcom/intsig/view/viewpager/IndicatorView;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/view/viewpager/IndicatorView;->〇o00〇〇Oo(F)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    int-to-float p1, p1

    .line 6
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->O0O:F

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final 〇〇808〇(F)Lcom/intsig/view/viewpager/IndicatorView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇〇08O:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8oOOo:F

    .line 4
    .line 5
    cmpg-float v0, v0, v1

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    if-eqz v0, :cond_1

    .line 13
    .line 14
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->o8oOOo:F

    .line 15
    .line 16
    :cond_1
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇〇08O:F

    .line 17
    .line 18
    return-object p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final 〇〇8O0〇8(I)Lcom/intsig/view/viewpager/IndicatorView;
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/view/viewpager/IndicatorView;->〇0O:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public abstract Lcom/intsig/view/viewpager/indicator/attacher/DotsIndicatorAttacher;
.super Ljava/lang/Object;
.source "DotsIndicatorAttacher.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Attachable:",
        "Ljava/lang/Object;",
        "Adapter:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public final O8(Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator;",
            "TAttachable;)V"
        }
    .end annotation

    .line 1
    const-string v0, "baseDotsIndicator"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, p2}, Lcom/intsig/view/viewpager/indicator/attacher/DotsIndicatorAttacher;->〇o00〇〇Oo(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/view/viewpager/indicator/attacher/DotsIndicatorAttacher$setup$1;

    .line 13
    .line 14
    invoke-direct {v1, p1}, Lcom/intsig/view/viewpager/indicator/attacher/DotsIndicatorAttacher$setup$1;-><init>(Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0, p2, v0, v1}, Lcom/intsig/view/viewpager/indicator/attacher/DotsIndicatorAttacher;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function0;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, p2, v0}, Lcom/intsig/view/viewpager/indicator/attacher/DotsIndicatorAttacher;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Pager;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    invoke-virtual {p1, p2}, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator;->setPager(Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Pager;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator;->〇O8o08O()V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 32
    .line 33
    const-string p2, "Please set an adapter to the view pager (1 or 2) or the recycler before initializing the dots indicator"

    .line 34
    .line 35
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    throw p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public abstract 〇080(Ljava/lang/Object;Ljava/lang/Object;)Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Pager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TAttachable;TAdapter;)",
            "Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Pager;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract 〇o00〇〇Oo(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TAttachable;)TAdapter;"
        }
    .end annotation
.end method

.method public abstract 〇o〇(Ljava/lang/Object;Ljava/lang/Object;Lkotlin/jvm/functions/Function0;)V
    .param p3    # Lkotlin/jvm/functions/Function0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TAttachable;TAdapter;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation
.end method

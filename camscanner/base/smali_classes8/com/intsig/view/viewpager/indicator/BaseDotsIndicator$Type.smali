.class public final enum Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;
.super Ljava/lang/Enum;
.source "BaseDotsIndicator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

.field public static final enum DEFAULT:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

.field public static final enum SPRING:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

.field public static final enum WORM:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;


# instance fields
.field private final defaultSize:F

.field private final defaultSpacing:F

.field private final dotsClickableId:I

.field private final dotsColorId:I

.field private final dotsCornerRadiusId:I

.field private final dotsSizeId:I

.field private final dotsSpacingId:I

.field private final styleableId:[I
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private static final synthetic $values()[Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;
    .locals 3

    .line 1
    const/4 v0, 0x3

    .line 2
    new-array v0, v0, [Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->DEFAULT:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->SPRING:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->WORM:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static constructor <clinit>()V
    .locals 24

    .line 1
    new-instance v11, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 2
    .line 3
    const-string v1, "DEFAULT"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/high16 v3, 0x41800000    # 16.0f

    .line 7
    .line 8
    const/high16 v4, 0x41000000    # 8.0f

    .line 9
    .line 10
    sget-object v5, Lcom/intsig/comm/R$styleable;->SpringDotsIndicator:[I

    .line 11
    .line 12
    const-string v0, "SpringDotsIndicator"

    .line 13
    .line 14
    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    sget v6, Lcom/intsig/comm/R$styleable;->SpringDotsIndicator_dotsColor:I

    .line 18
    .line 19
    sget v7, Lcom/intsig/comm/R$styleable;->SpringDotsIndicator_dotsSize:I

    .line 20
    .line 21
    sget v8, Lcom/intsig/comm/R$styleable;->SpringDotsIndicator_dotsSpacing:I

    .line 22
    .line 23
    sget v9, Lcom/intsig/comm/R$styleable;->SpringDotsIndicator_dotsCornerRadius:I

    .line 24
    .line 25
    sget v23, Lcom/intsig/comm/R$styleable;->SpringDotsIndicator_dotsClickable:I

    .line 26
    .line 27
    move-object v0, v11

    .line 28
    move/from16 v10, v23

    .line 29
    .line 30
    invoke-direct/range {v0 .. v10}, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;-><init>(Ljava/lang/String;IFF[IIIIII)V

    .line 31
    .line 32
    .line 33
    sput-object v11, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->DEFAULT:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 34
    .line 35
    new-instance v0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 36
    .line 37
    const-string v13, "SPRING"

    .line 38
    .line 39
    const/4 v14, 0x1

    .line 40
    const/high16 v15, 0x41800000    # 16.0f

    .line 41
    .line 42
    const/high16 v16, 0x40800000    # 4.0f

    .line 43
    .line 44
    sget-object v1, Lcom/intsig/comm/R$styleable;->DotsIndicator:[I

    .line 45
    .line 46
    const-string v2, "DotsIndicator"

    .line 47
    .line 48
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    sget v18, Lcom/intsig/comm/R$styleable;->DotsIndicator_dotsColor:I

    .line 52
    .line 53
    sget v19, Lcom/intsig/comm/R$styleable;->DotsIndicator_dotsSize:I

    .line 54
    .line 55
    sget v20, Lcom/intsig/comm/R$styleable;->DotsIndicator_dotsSpacing:I

    .line 56
    .line 57
    sget v21, Lcom/intsig/comm/R$styleable;->DotsIndicator_dotsCornerRadius:I

    .line 58
    .line 59
    move-object v12, v0

    .line 60
    move-object/from16 v17, v1

    .line 61
    .line 62
    move/from16 v22, v23

    .line 63
    .line 64
    invoke-direct/range {v12 .. v22}, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;-><init>(Ljava/lang/String;IFF[IIIIII)V

    .line 65
    .line 66
    .line 67
    sput-object v0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->SPRING:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 68
    .line 69
    new-instance v0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 70
    .line 71
    const-string v13, "WORM"

    .line 72
    .line 73
    const/4 v14, 0x2

    .line 74
    sget-object v1, Lcom/intsig/comm/R$styleable;->WormDotsIndicator:[I

    .line 75
    .line 76
    const-string v2, "WormDotsIndicator"

    .line 77
    .line 78
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    sget v18, Lcom/intsig/comm/R$styleable;->WormDotsIndicator_dotsColor:I

    .line 82
    .line 83
    sget v19, Lcom/intsig/comm/R$styleable;->WormDotsIndicator_dotsSize:I

    .line 84
    .line 85
    sget v20, Lcom/intsig/comm/R$styleable;->WormDotsIndicator_dotsSpacing:I

    .line 86
    .line 87
    sget v21, Lcom/intsig/comm/R$styleable;->WormDotsIndicator_dotsCornerRadius:I

    .line 88
    .line 89
    move-object v12, v0

    .line 90
    move-object/from16 v17, v1

    .line 91
    .line 92
    invoke-direct/range {v12 .. v22}, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;-><init>(Ljava/lang/String;IFF[IIIIII)V

    .line 93
    .line 94
    .line 95
    sput-object v0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->WORM:Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 96
    .line 97
    invoke-static {}, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->$values()[Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    sput-object v0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->$VALUES:[Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 102
    .line 103
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private constructor <init>(Ljava/lang/String;IFF[IIIIII)V
    .locals 0
    .param p3    # F
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p4    # F
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p5    # [I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Landroidx/annotation/StyleableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF[IIIIII)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->defaultSize:F

    .line 5
    .line 6
    iput p4, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->defaultSpacing:F

    .line 7
    .line 8
    iput-object p5, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->styleableId:[I

    .line 9
    .line 10
    iput p6, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsColorId:I

    .line 11
    .line 12
    iput p7, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsSizeId:I

    .line 13
    .line 14
    iput p8, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsSpacingId:I

    .line 15
    .line 16
    iput p9, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsCornerRadiusId:I

    .line 17
    .line 18
    iput p10, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsClickableId:I

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static values()[Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->$VALUES:[Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public final getDefaultSize()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->defaultSize:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getDefaultSpacing()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->defaultSpacing:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getDotsClickableId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsClickableId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getDotsColorId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsColorId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getDotsCornerRadiusId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsCornerRadiusId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getDotsSizeId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsSizeId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getDotsSpacingId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->dotsSpacingId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final getStyleableId()[I
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/view/viewpager/indicator/BaseDotsIndicator$Type;->styleableId:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

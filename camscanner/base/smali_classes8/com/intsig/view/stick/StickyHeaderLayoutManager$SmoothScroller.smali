.class Lcom/intsig/view/stick/StickyHeaderLayoutManager$SmoothScroller;
.super Landroidx/recyclerview/widget/LinearSmoothScroller;
.source "StickyHeaderLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/view/stick/StickyHeaderLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmoothScroller"
.end annotation


# instance fields
.field private final 〇080:F

.field private final 〇o00〇〇Oo:F

.field final synthetic 〇o〇:Lcom/intsig/view/stick/StickyHeaderLayoutManager;


# direct methods
.method constructor <init>(Lcom/intsig/view/stick/StickyHeaderLayoutManager;Landroid/content/Context;I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/view/stick/StickyHeaderLayoutManager$SmoothScroller;->〇o〇:Lcom/intsig/view/stick/StickyHeaderLayoutManager;

    .line 2
    .line 3
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/LinearSmoothScroller;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    int-to-float p1, p3

    .line 7
    iput p1, p0, Lcom/intsig/view/stick/StickyHeaderLayoutManager$SmoothScroller;->〇080:F

    .line 8
    .line 9
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/LinearSmoothScroller;->calculateSpeedPerPixel(Landroid/util/DisplayMetrics;)F

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    const/16 p2, 0x2710

    .line 22
    .line 23
    if-ge p3, p2, :cond_0

    .line 24
    .line 25
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    .line 26
    .line 27
    .line 28
    move-result p2

    .line 29
    int-to-float p2, p2

    .line 30
    mul-float p2, p2, p1

    .line 31
    .line 32
    float-to-int p1, p2

    .line 33
    int-to-float p1, p1

    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const/high16 p1, 0x447a0000    # 1000.0f

    .line 36
    .line 37
    :goto_0
    iput p1, p0, Lcom/intsig/view/stick/StickyHeaderLayoutManager$SmoothScroller;->〇o00〇〇Oo:F

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method protected calculateTimeForScrolling(I)I
    .locals 1

    .line 1
    int-to-float p1, p1

    .line 2
    iget v0, p0, Lcom/intsig/view/stick/StickyHeaderLayoutManager$SmoothScroller;->〇080:F

    .line 3
    .line 4
    div-float/2addr p1, v0

    .line 5
    iget v0, p0, Lcom/intsig/view/stick/StickyHeaderLayoutManager$SmoothScroller;->〇o00〇〇Oo:F

    .line 6
    .line 7
    mul-float v0, v0, p1

    .line 8
    .line 9
    float-to-int p1, v0

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public computeScrollVectorForPosition(I)Landroid/graphics/PointF;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/PointF;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/view/stick/StickyHeaderLayoutManager$SmoothScroller;->〇o〇:Lcom/intsig/view/stick/StickyHeaderLayoutManager;

    .line 4
    .line 5
    invoke-static {v1, p1}, Lcom/intsig/view/stick/StickyHeaderLayoutManager;->O8ooOoo〇(Lcom/intsig/view/stick/StickyHeaderLayoutManager;I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-float p1, p1

    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-direct {v0, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public Lcom/intsig/view/RotateTextView;
.super Landroidx/appcompat/widget/AppCompatTextView;
.source "RotateTextView.java"


# instance fields
.field private O8o08O8O:J

.field private OO:I

.field private o0:I

.field private o〇00O:Z

.field private 〇080OO8〇0:J

.field private 〇08O〇00〇o:Z

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 9
    iput p1, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 10
    iput p1, p0, Lcom/intsig/view/RotateTextView;->〇OOo8〇0:I

    .line 11
    iput p1, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 12
    iput-boolean p1, p0, Lcom/intsig/view/RotateTextView;->〇08O〇00〇o:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/intsig/view/RotateTextView;->o〇00O:Z

    const-wide/16 p1, 0x0

    .line 13
    iput-wide p1, p0, Lcom/intsig/view/RotateTextView;->O8o08O8O:J

    .line 14
    iput-wide p1, p0, Lcom/intsig/view/RotateTextView;->〇080OO8〇0:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    .line 2
    iput p1, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 3
    iput p1, p0, Lcom/intsig/view/RotateTextView;->〇OOo8〇0:I

    .line 4
    iput p1, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 5
    iput-boolean p1, p0, Lcom/intsig/view/RotateTextView;->〇08O〇00〇o:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/intsig/view/RotateTextView;->o〇00O:Z

    const-wide/16 p1, 0x0

    .line 6
    iput-wide p1, p0, Lcom/intsig/view/RotateTextView;->O8o08O8O:J

    .line 7
    iput-wide p1, p0, Lcom/intsig/view/RotateTextView;->〇080OO8〇0:J

    return-void
.end method


# virtual methods
.method protected getDegree()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    iget v2, v1, Landroid/graphics/Rect;->right:I

    .line 13
    .line 14
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 15
    .line 16
    sub-int/2addr v2, v3

    .line 17
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 18
    .line 19
    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 20
    .line 21
    sub-int/2addr v3, v1

    .line 22
    if-eqz v2, :cond_6

    .line 23
    .line 24
    if-nez v3, :cond_1

    .line 25
    .line 26
    goto/16 :goto_3

    .line 27
    .line 28
    :cond_1
    iget v1, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 29
    .line 30
    iget v4, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 31
    .line 32
    if-eq v1, v4, :cond_5

    .line 33
    .line 34
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    .line 35
    .line 36
    .line 37
    move-result-wide v4

    .line 38
    iget-wide v6, p0, Lcom/intsig/view/RotateTextView;->〇080OO8〇0:J

    .line 39
    .line 40
    cmp-long v1, v4, v6

    .line 41
    .line 42
    if-gez v1, :cond_4

    .line 43
    .line 44
    iget-wide v6, p0, Lcom/intsig/view/RotateTextView;->O8o08O8O:J

    .line 45
    .line 46
    sub-long/2addr v4, v6

    .line 47
    long-to-int v1, v4

    .line 48
    iget v4, p0, Lcom/intsig/view/RotateTextView;->〇OOo8〇0:I

    .line 49
    .line 50
    iget-boolean v5, p0, Lcom/intsig/view/RotateTextView;->〇08O〇00〇o:Z

    .line 51
    .line 52
    if-eqz v5, :cond_2

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    neg-int v1, v1

    .line 56
    :goto_0
    mul-int/lit16 v1, v1, 0x10e

    .line 57
    .line 58
    div-int/lit16 v1, v1, 0x3e8

    .line 59
    .line 60
    add-int/2addr v4, v1

    .line 61
    if-ltz v4, :cond_3

    .line 62
    .line 63
    rem-int/lit16 v4, v4, 0x168

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_3
    rem-int/lit16 v4, v4, 0x168

    .line 67
    .line 68
    add-int/lit16 v4, v4, 0x168

    .line 69
    .line 70
    :goto_1
    iput v4, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 71
    .line 72
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 73
    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_4
    iget v1, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 77
    .line 78
    iput v1, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 79
    .line 80
    :cond_5
    :goto_2
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    .line 89
    .line 90
    .line 91
    move-result v5

    .line 92
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 93
    .line 94
    .line 95
    move-result v6

    .line 96
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 97
    .line 98
    .line 99
    move-result v7

    .line 100
    sub-int/2addr v7, v1

    .line 101
    sub-int/2addr v7, v5

    .line 102
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 103
    .line 104
    .line 105
    move-result v5

    .line 106
    sub-int/2addr v5, v4

    .line 107
    sub-int/2addr v5, v6

    .line 108
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    .line 109
    .line 110
    .line 111
    move-result v6

    .line 112
    div-int/lit8 v7, v7, 0x2

    .line 113
    .line 114
    add-int/2addr v1, v7

    .line 115
    int-to-float v1, v1

    .line 116
    div-int/lit8 v5, v5, 0x2

    .line 117
    .line 118
    add-int/2addr v4, v5

    .line 119
    int-to-float v4, v4

    .line 120
    invoke-virtual {p1, v1, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 121
    .line 122
    .line 123
    iget v1, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 124
    .line 125
    neg-int v1, v1

    .line 126
    int-to-float v1, v1

    .line 127
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    .line 128
    .line 129
    .line 130
    neg-int v1, v2

    .line 131
    div-int/lit8 v1, v1, 0x2

    .line 132
    .line 133
    int-to-float v1, v1

    .line 134
    neg-int v2, v3

    .line 135
    div-int/lit8 v2, v2, 0x2

    .line 136
    .line 137
    int-to-float v2, v2

    .line 138
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 142
    .line 143
    .line 144
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 148
    .line 149
    .line 150
    :cond_6
    :goto_3
    return-void
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setDegree(I)V
    .locals 4

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    rem-int/lit16 p1, p1, 0x168

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    rem-int/lit16 p1, p1, 0x168

    .line 7
    .line 8
    add-int/lit16 p1, p1, 0x168

    .line 9
    .line 10
    :goto_0
    iget v0, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 11
    .line 12
    if-ne p1, v0, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iput p1, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 16
    .line 17
    iget-boolean v0, p0, Lcom/intsig/view/RotateTextView;->o〇00O:Z

    .line 18
    .line 19
    if-nez v0, :cond_2

    .line 20
    .line 21
    iput p1, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 22
    .line 23
    :cond_2
    iget p1, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 24
    .line 25
    iput p1, p0, Lcom/intsig/view/RotateTextView;->〇OOo8〇0:I

    .line 26
    .line 27
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    .line 28
    .line 29
    .line 30
    move-result-wide v0

    .line 31
    iput-wide v0, p0, Lcom/intsig/view/RotateTextView;->O8o08O8O:J

    .line 32
    .line 33
    iget p1, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/view/RotateTextView;->o0:I

    .line 36
    .line 37
    sub-int/2addr p1, v2

    .line 38
    if-ltz p1, :cond_3

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_3
    add-int/lit16 p1, p1, 0x168

    .line 42
    .line 43
    :goto_1
    const/16 v2, 0xb4

    .line 44
    .line 45
    if-le p1, v2, :cond_4

    .line 46
    .line 47
    add-int/lit16 p1, p1, -0x168

    .line 48
    .line 49
    :cond_4
    if-ltz p1, :cond_5

    .line 50
    .line 51
    const/4 v2, 0x1

    .line 52
    goto :goto_2

    .line 53
    :cond_5
    const/4 v2, 0x0

    .line 54
    :goto_2
    iput-boolean v2, p0, Lcom/intsig/view/RotateTextView;->〇08O〇00〇o:Z

    .line 55
    .line 56
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    mul-int/lit16 p1, p1, 0x3e8

    .line 61
    .line 62
    div-int/lit16 p1, p1, 0x10e

    .line 63
    .line 64
    int-to-long v2, p1

    .line 65
    add-long/2addr v0, v2

    .line 66
    iput-wide v0, p0, Lcom/intsig/view/RotateTextView;->〇080OO8〇0:J

    .line 67
    .line 68
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setDegree2(I)V
    .locals 1

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    rem-int/lit16 p1, p1, 0x168

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    rem-int/lit16 p1, p1, 0x168

    .line 7
    .line 8
    add-int/lit16 p1, p1, 0x168

    .line 9
    .line 10
    :goto_0
    iget v0, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 11
    .line 12
    if-ne p1, v0, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iput p1, p0, Lcom/intsig/view/RotateTextView;->OO:I

    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOrientation(I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/view/RotateTextView;->setDegree(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

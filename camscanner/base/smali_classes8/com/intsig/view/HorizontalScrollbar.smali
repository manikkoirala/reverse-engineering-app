.class public final Lcom/intsig/view/HorizontalScrollbar;
.super Landroid/view/View;
.source "HorizontalScrollbar.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/view/HorizontalScrollbar$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇0O:Lcom/intsig/view/HorizontalScrollbar$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:F

.field private OO:I

.field private o0:I

.field private o〇00O:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/view/HorizontalScrollbar$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/view/HorizontalScrollbar$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/view/HorizontalScrollbar;->〇0O:Lcom/intsig/view/HorizontalScrollbar$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x17

    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/intsig/view/HorizontalScrollbar;->o0:I

    const v0, -0xe0e0f

    .line 3
    iput v0, p0, Lcom/intsig/view/HorizontalScrollbar;->〇OOo8〇0:I

    const/high16 v0, -0x1000000

    .line 4
    iput v0, p0, Lcom/intsig/view/HorizontalScrollbar;->OO:I

    .line 5
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intsig/view/HorizontalScrollbar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 6
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intsig/view/HorizontalScrollbar;->o〇00O:Landroid/graphics/Paint;

    const v0, 0x3dcccccd    # 0.1f

    .line 7
    iput v0, p0, Lcom/intsig/view/HorizontalScrollbar;->〇080OO8〇0:F

    .line 8
    iget-object v0, p0, Lcom/intsig/view/HorizontalScrollbar;->〇08O〇00〇o:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 9
    iget-object v0, p0, Lcom/intsig/view/HorizontalScrollbar;->〇08O〇00〇o:Landroid/graphics/Paint;

    iget v1, p0, Lcom/intsig/view/HorizontalScrollbar;->〇OOo8〇0:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 10
    iget-object v0, p0, Lcom/intsig/view/HorizontalScrollbar;->o〇00O:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 11
    iget-object v0, p0, Lcom/intsig/view/HorizontalScrollbar;->o〇00O:Landroid/graphics/Paint;

    iget v1, p0, Lcom/intsig/view/HorizontalScrollbar;->OO:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/HorizontalScrollbar;->〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 13
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    sget-object p3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    invoke-virtual {p3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object p3

    const/16 v0, 0x17

    invoke-static {p3, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    move-result p3

    iput p3, p0, Lcom/intsig/view/HorizontalScrollbar;->o0:I

    const p3, -0xe0e0f

    .line 15
    iput p3, p0, Lcom/intsig/view/HorizontalScrollbar;->〇OOo8〇0:I

    const/high16 p3, -0x1000000

    .line 16
    iput p3, p0, Lcom/intsig/view/HorizontalScrollbar;->OO:I

    .line 17
    new-instance p3, Landroid/graphics/Paint;

    invoke-direct {p3}, Landroid/graphics/Paint;-><init>()V

    iput-object p3, p0, Lcom/intsig/view/HorizontalScrollbar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 18
    new-instance p3, Landroid/graphics/Paint;

    invoke-direct {p3}, Landroid/graphics/Paint;-><init>()V

    iput-object p3, p0, Lcom/intsig/view/HorizontalScrollbar;->o〇00O:Landroid/graphics/Paint;

    const p3, 0x3dcccccd    # 0.1f

    .line 19
    iput p3, p0, Lcom/intsig/view/HorizontalScrollbar;->〇080OO8〇0:F

    .line 20
    iget-object p3, p0, Lcom/intsig/view/HorizontalScrollbar;->〇08O〇00〇o:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 21
    iget-object p3, p0, Lcom/intsig/view/HorizontalScrollbar;->〇08O〇00〇o:Landroid/graphics/Paint;

    iget v0, p0, Lcom/intsig/view/HorizontalScrollbar;->〇OOo8〇0:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 22
    iget-object p3, p0, Lcom/intsig/view/HorizontalScrollbar;->o〇00O:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 23
    iget-object p3, p0, Lcom/intsig/view/HorizontalScrollbar;->o〇00O:Landroid/graphics/Paint;

    iget v0, p0, Lcom/intsig/view/HorizontalScrollbar;->OO:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/HorizontalScrollbar;->〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final 〇080(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/16 v1, 0x17

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    sget-object v1, Lcom/intsig/comm/R$styleable;->HorizontalScrollbar:[I

    .line 19
    .line 20
    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const-string p2, "context.obtainStyledAttr\u2026able.HorizontalScrollbar)"

    .line 25
    .line 26
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    sget p2, Lcom/intsig/comm/R$styleable;->HorizontalScrollbar_barBgColor:I

    .line 30
    .line 31
    const v1, -0xe0e0f

    .line 32
    .line 33
    .line 34
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 35
    .line 36
    .line 37
    move-result p2

    .line 38
    iput p2, p0, Lcom/intsig/view/HorizontalScrollbar;->〇OOo8〇0:I

    .line 39
    .line 40
    sget p2, Lcom/intsig/comm/R$styleable;->HorizontalScrollbar_barThumbColor:I

    .line 41
    .line 42
    const/high16 v1, -0x1000000

    .line 43
    .line 44
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 45
    .line 46
    .line 47
    move-result p2

    .line 48
    iput p2, p0, Lcom/intsig/view/HorizontalScrollbar;->OO:I

    .line 49
    .line 50
    sget p2, Lcom/intsig/comm/R$styleable;->HorizontalScrollbar_barRadius:I

    .line 51
    .line 52
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 53
    .line 54
    .line 55
    move-result p2

    .line 56
    iput p2, p0, Lcom/intsig/view/HorizontalScrollbar;->o0:I

    .line 57
    .line 58
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    .line 60
    .line 61
    iget-object p1, p0, Lcom/intsig/view/HorizontalScrollbar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 62
    .line 63
    iget p2, p0, Lcom/intsig/view/HorizontalScrollbar;->〇OOo8〇0:I

    .line 64
    .line 65
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    .line 67
    .line 68
    iget-object p1, p0, Lcom/intsig/view/HorizontalScrollbar;->o〇00O:Landroid/graphics/Paint;

    .line 69
    .line 70
    iget p2, p0, Lcom/intsig/view/HorizontalScrollbar;->OO:I

    .line 71
    .line 72
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 73
    .line 74
    .line 75
    :cond_1
    :goto_0
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/view/HorizontalScrollbar;->〇080OO8〇0:F

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    int-to-float v1, v1

    .line 13
    mul-float v0, v0, v1

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    int-to-float v1, v1

    .line 20
    sub-float/2addr v1, v0

    .line 21
    iget v2, p0, Lcom/intsig/view/HorizontalScrollbar;->O8o08O8O:F

    .line 22
    .line 23
    mul-float v4, v1, v2

    .line 24
    .line 25
    const/4 v6, 0x0

    .line 26
    const/4 v7, 0x0

    .line 27
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    int-to-float v8, v1

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    int-to-float v9, v1

    .line 37
    iget v1, p0, Lcom/intsig/view/HorizontalScrollbar;->o0:I

    .line 38
    .line 39
    int-to-float v10, v1

    .line 40
    int-to-float v11, v1

    .line 41
    iget-object v12, p0, Lcom/intsig/view/HorizontalScrollbar;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 42
    .line 43
    move-object v5, p1

    .line 44
    invoke-virtual/range {v5 .. v12}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    .line 45
    .line 46
    .line 47
    const/4 v5, 0x0

    .line 48
    add-float v6, v4, v0

    .line 49
    .line 50
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    int-to-float v7, v0

    .line 55
    iget v0, p0, Lcom/intsig/view/HorizontalScrollbar;->o0:I

    .line 56
    .line 57
    int-to-float v8, v0

    .line 58
    int-to-float v9, v0

    .line 59
    iget-object v10, p0, Lcom/intsig/view/HorizontalScrollbar;->o〇00O:Landroid/graphics/Paint;

    .line 60
    .line 61
    move-object v3, p1

    .line 62
    invoke-virtual/range {v3 .. v10}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    .line 63
    .line 64
    .line 65
    :cond_0
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public final 〇o00〇〇Oo(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/view/HorizontalScrollbar;->O8o08O8O:F

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final 〇o〇(F)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "updateThumbPercentage, thumbPercentage:"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "HorizontalScrollbar"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iput p1, p0, Lcom/intsig/view/HorizontalScrollbar;->〇080OO8〇0:F

    .line 24
    .line 25
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public final Lcom/intsig/office_preview/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office_preview/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BOTTOM_END:I = 0x7f0a0002

.field public static final BOTTOM_START:I = 0x7f0a0003

.field public static final NO_DEBUG:I = 0x7f0a000e

.field public static final SHOW_ALL:I = 0x7f0a0012

.field public static final SHOW_PATH:I = 0x7f0a0013

.field public static final SHOW_PROGRESS:I = 0x7f0a0014

.field public static final TOP_END:I = 0x7f0a0017

.field public static final TOP_START:I = 0x7f0a0018

.field public static final accelerate:I = 0x7f0a001c

.field public static final accessibility_action_clickable_span:I = 0x7f0a001d

.field public static final accessibility_custom_action_0:I = 0x7f0a001e

.field public static final accessibility_custom_action_1:I = 0x7f0a001f

.field public static final accessibility_custom_action_10:I = 0x7f0a0020

.field public static final accessibility_custom_action_11:I = 0x7f0a0021

.field public static final accessibility_custom_action_12:I = 0x7f0a0022

.field public static final accessibility_custom_action_13:I = 0x7f0a0023

.field public static final accessibility_custom_action_14:I = 0x7f0a0024

.field public static final accessibility_custom_action_15:I = 0x7f0a0025

.field public static final accessibility_custom_action_16:I = 0x7f0a0026

.field public static final accessibility_custom_action_17:I = 0x7f0a0027

.field public static final accessibility_custom_action_18:I = 0x7f0a0028

.field public static final accessibility_custom_action_19:I = 0x7f0a0029

.field public static final accessibility_custom_action_2:I = 0x7f0a002a

.field public static final accessibility_custom_action_20:I = 0x7f0a002b

.field public static final accessibility_custom_action_21:I = 0x7f0a002c

.field public static final accessibility_custom_action_22:I = 0x7f0a002d

.field public static final accessibility_custom_action_23:I = 0x7f0a002e

.field public static final accessibility_custom_action_24:I = 0x7f0a002f

.field public static final accessibility_custom_action_25:I = 0x7f0a0030

.field public static final accessibility_custom_action_26:I = 0x7f0a0031

.field public static final accessibility_custom_action_27:I = 0x7f0a0032

.field public static final accessibility_custom_action_28:I = 0x7f0a0033

.field public static final accessibility_custom_action_29:I = 0x7f0a0034

.field public static final accessibility_custom_action_3:I = 0x7f0a0035

.field public static final accessibility_custom_action_30:I = 0x7f0a0036

.field public static final accessibility_custom_action_31:I = 0x7f0a0037

.field public static final accessibility_custom_action_4:I = 0x7f0a0038

.field public static final accessibility_custom_action_5:I = 0x7f0a0039

.field public static final accessibility_custom_action_6:I = 0x7f0a003a

.field public static final accessibility_custom_action_7:I = 0x7f0a003b

.field public static final accessibility_custom_action_8:I = 0x7f0a003c

.field public static final accessibility_custom_action_9:I = 0x7f0a003d

.field public static final action0:I = 0x7f0a0051

.field public static final actionDown:I = 0x7f0a0053

.field public static final actionDownUp:I = 0x7f0a0054

.field public static final actionUp:I = 0x7f0a0059

.field public static final action_bar:I = 0x7f0a005a

.field public static final action_bar_activity_content:I = 0x7f0a005b

.field public static final action_bar_container:I = 0x7f0a005c

.field public static final action_bar_root:I = 0x7f0a0060

.field public static final action_bar_spinner:I = 0x7f0a0061

.field public static final action_bar_subtitle:I = 0x7f0a0062

.field public static final action_bar_title:I = 0x7f0a0063

.field public static final action_btn:I = 0x7f0a0064

.field public static final action_container:I = 0x7f0a0066

.field public static final action_context_bar:I = 0x7f0a0067

.field public static final action_divider:I = 0x7f0a0068

.field public static final action_image:I = 0x7f0a0069

.field public static final action_menu_divider:I = 0x7f0a006b

.field public static final action_menu_presenter:I = 0x7f0a006c

.field public static final action_mode_bar:I = 0x7f0a006d

.field public static final action_mode_bar_stub:I = 0x7f0a006e

.field public static final action_mode_close_button:I = 0x7f0a006f

.field public static final action_text:I = 0x7f0a0072

.field public static final actions:I = 0x7f0a0073

.field public static final activity_chooser_view_content:I = 0x7f0a0075

.field public static final add:I = 0x7f0a008e

.field public static final adjust_height:I = 0x7f0a0097

.field public static final adjust_width:I = 0x7f0a009f

.field public static final alertTitle:I = 0x7f0a0133

.field public static final aligned:I = 0x7f0a0134

.field public static final allStates:I = 0x7f0a0136

.field public static final animateToEnd:I = 0x7f0a013c

.field public static final animateToStart:I = 0x7f0a013d

.field public static final antiClockwise:I = 0x7f0a013e

.field public static final anticipate:I = 0x7f0a013f

.field public static final app_bar_layout:I = 0x7f0a0143

.field public static final arc:I = 0x7f0a015d

.field public static final arrow:I = 0x7f0a015e

.field public static final asConfigured:I = 0x7f0a0161

.field public static final async:I = 0x7f0a0163

.field public static final auto:I = 0x7f0a01b2

.field public static final autoComplete:I = 0x7f0a01b3

.field public static final autoCompleteToEnd:I = 0x7f0a01b4

.field public static final autoCompleteToStart:I = 0x7f0a01b5

.field public static final automatic:I = 0x7f0a01b8

.field public static final barrier:I = 0x7f0a01c7

.field public static final baseline:I = 0x7f0a01cf

.field public static final bestChoice:I = 0x7f0a01d2

.field public static final blocking:I = 0x7f0a01db

.field public static final bottom:I = 0x7f0a01e2

.field public static final bottom_panel:I = 0x7f0a01ed

.field public static final bounce:I = 0x7f0a01f9

.field public static final btnSheet:I = 0x7f0a020d

.field public static final btn_clear:I = 0x7f0a0236

.field public static final btn_common:I = 0x7f0a0237

.field public static final btn_drag:I = 0x7f0a024e

.field public static final btn_filter:I = 0x7f0a025a

.field public static final btn_hide:I = 0x7f0a0266

.field public static final btn_reopen:I = 0x7f0a029d

.field public static final btn_scale:I = 0x7f0a02a2

.field public static final btn_shortcut:I = 0x7f0a02ac

.field public static final btn_simulator:I = 0x7f0a02af

.field public static final buttonPanel:I = 0x7f0a02e2

.field public static final cancel_action:I = 0x7f0a02ef

.field public static final cancel_button:I = 0x7f0a02f0

.field public static final card_view:I = 0x7f0a02ff

.field public static final carryVelocity:I = 0x7f0a0303

.field public static final center:I = 0x7f0a0346

.field public static final centerCrop:I = 0x7f0a0347

.field public static final centerInside:I = 0x7f0a0348

.field public static final chain:I = 0x7f0a035b

.field public static final checkbox:I = 0x7f0a035f

.field public static final checked:I = 0x7f0a0363

.field public static final chronometer:I = 0x7f0a0365

.field public static final circle_center:I = 0x7f0a0366

.field public static final clear_text:I = 0x7f0a0464

.field public static final clockwise:I = 0x7f0a0469

.field public static final closest:I = 0x7f0a046c

.field public static final column:I = 0x7f0a0486

.field public static final column_reverse:I = 0x7f0a0487

.field public static final compress:I = 0x7f0a04a2

.field public static final confirm_button:I = 0x7f0a04a3

.field public static final constraint:I = 0x7f0a04a5

.field public static final container:I = 0x7f0a04ac

.field public static final content:I = 0x7f0a04ad

.field public static final contentPanel:I = 0x7f0a04ae

.field public static final content_panel:I = 0x7f0a04af

.field public static final contiguous:I = 0x7f0a04b1

.field public static final continuousVelocity:I = 0x7f0a04b3

.field public static final coordinator:I = 0x7f0a04b5

.field public static final cos:I = 0x7f0a04b7

.field public static final counterclockwise:I = 0x7f0a04be

.field public static final cover:I = 0x7f0a04c0

.field public static final cradle:I = 0x7f0a04c2

.field public static final currentState:I = 0x7f0a04ec

.field public static final custom:I = 0x7f0a04ed

.field public static final customPanel:I = 0x7f0a04ee

.field public static final custom_panel:I = 0x7f0a04ef

.field public static final cut:I = 0x7f0a04f1

.field public static final dark:I = 0x7f0a0500

.field public static final date_picker_actions:I = 0x7f0a0504

.field public static final decelerate:I = 0x7f0a0507

.field public static final decelerateAndComplete:I = 0x7f0a0508

.field public static final decor_content_parent:I = 0x7f0a0509

.field public static final default_activity_button:I = 0x7f0a050a

.field public static final deltaRelative:I = 0x7f0a050b

.field public static final design_bottom_sheet:I = 0x7f0a0512

.field public static final design_menu_item_action_area:I = 0x7f0a0513

.field public static final design_menu_item_action_area_stub:I = 0x7f0a0514

.field public static final design_menu_item_text:I = 0x7f0a0515

.field public static final design_navigation_view:I = 0x7f0a0516

.field public static final dialog_button:I = 0x7f0a0518

.field public static final disjoint:I = 0x7f0a0530

.field public static final dot:I = 0x7f0a054c

.field public static final dragAnticlockwise:I = 0x7f0a0554

.field public static final dragClockwise:I = 0x7f0a0555

.field public static final dragDown:I = 0x7f0a0556

.field public static final dragEnd:I = 0x7f0a0557

.field public static final dragLeft:I = 0x7f0a0558

.field public static final dragRight:I = 0x7f0a0559

.field public static final dragStart:I = 0x7f0a055a

.field public static final dragUp:I = 0x7f0a055b

.field public static final dropdown_menu:I = 0x7f0a0561

.field public static final easeIn:I = 0x7f0a0567

.field public static final easeInOut:I = 0x7f0a0568

.field public static final easeOut:I = 0x7f0a0569

.field public static final east:I = 0x7f0a056a

.field public static final edge:I = 0x7f0a056e

.field public static final edit_query:I = 0x7f0a0573

.field public static final elastic:I = 0x7f0a057a

.field public static final embed:I = 0x7f0a057e

.field public static final end:I = 0x7f0a0581

.field public static final endToStart:I = 0x7f0a0582

.field public static final end_padder:I = 0x7f0a0584

.field public static final entrance_collage:I = 0x7f0a0589

.field public static final et_filter:I = 0x7f0a059a

.field public static final et_verify_code:I = 0x7f0a05b8

.field public static final expand_activities_button:I = 0x7f0a05d1

.field public static final expanded_menu:I = 0x7f0a05d2

.field public static final fade:I = 0x7f0a05df

.field public static final fill:I = 0x7f0a05e8

.field public static final filled:I = 0x7f0a05ee

.field public static final fitCenter:I = 0x7f0a05f0

.field public static final fitEnd:I = 0x7f0a05f1

.field public static final fitStart:I = 0x7f0a05f2

.field public static final fitXY:I = 0x7f0a05f4

.field public static final fixed:I = 0x7f0a05f5

.field public static final flex_end:I = 0x7f0a065e

.field public static final flex_start:I = 0x7f0a065f

.field public static final flip:I = 0x7f0a0661

.field public static final floating:I = 0x7f0a0664

.field public static final forever:I = 0x7f0a066b

.field public static final fragment_container:I = 0x7f0a0671

.field public static final fragment_container_id:I = 0x7f0a0672

.field public static final fragment_container_view_tag:I = 0x7f0a0673

.field public static final frame_parent:I = 0x7f0a0679

.field public static final frost:I = 0x7f0a067b

.field public static final fullscreen_header:I = 0x7f0a067c

.field public static final ghost_view:I = 0x7f0a0692

.field public static final ghost_view_holder:I = 0x7f0a0693

.field public static final glide_custom_view_target_tag:I = 0x7f0a06b0

.field public static final gone:I = 0x7f0a06b3

.field public static final grid_dlgshares_other:I = 0x7f0a06ba

.field public static final grid_dlgshares_recent:I = 0x7f0a06bb

.field public static final group_divider:I = 0x7f0a06ca

.field public static final guideline:I = 0x7f0a06fc

.field public static final hardware:I = 0x7f0a0701

.field public static final header_title:I = 0x7f0a0703

.field public static final home:I = 0x7f0a0707

.field public static final honorRequest:I = 0x7f0a070a

.field public static final horizontal_only:I = 0x7f0a070f

.field public static final hsvButtons:I = 0x7f0a0712

.field public static final icon:I = 0x7f0a071f

.field public static final icon_group:I = 0x7f0a0721

.field public static final icon_only:I = 0x7f0a0722

.field public static final icon_view:I = 0x7f0a0725

.field public static final ignore:I = 0x7f0a0727

.field public static final ignoreRequest:I = 0x7f0a0728

.field public static final image:I = 0x7f0a0730

.field public static final immediateStop:I = 0x7f0a0763

.field public static final included:I = 0x7f0a0787

.field public static final indeterminate:I = 0x7f0a0788

.field public static final indicator_container:I = 0x7f0a078b

.field public static final info:I = 0x7f0a078d

.field public static final invisible:I = 0x7f0a079b

.field public static final inward:I = 0x7f0a079c

.field public static final italic:I = 0x7f0a07ad

.field public static final item_gp_question_image:I = 0x7f0a081d

.field public static final item_gp_question_image_desc:I = 0x7f0a081e

.field public static final item_gp_question_image_desc2:I = 0x7f0a081f

.field public static final item_gp_question_image_top_dec:I = 0x7f0a0820

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a0826

.field public static final iv:I = 0x7f0a082e

.field public static final iv_bg:I = 0x7f0a0853

.field public static final iv_flag:I = 0x7f0a08ef

.field public static final iv_icon:I = 0x7f0a0931

.field public static final iv_img:I = 0x7f0a093c

.field public static final iv_introduce:I = 0x7f0a0945

.field public static final iv_mask:I = 0x7f0a0981

.field public static final iv_new_flag:I = 0x7f0a09a3

.field public static final iv_premium_icon:I = 0x7f0a09ea

.field public static final iv_show_thumb:I = 0x7f0a0a3c

.field public static final iv_squared_icon:I = 0x7f0a0a41

.field public static final iv_vip:I = 0x7f0a0aab

.field public static final jumpToEnd:I = 0x7f0a0ad6

.field public static final jumpToStart:I = 0x7f0a0ad7

.field public static final label_title:I = 0x7f0a0af9

.field public static final labeled:I = 0x7f0a0afa

.field public static final lav_loading:I = 0x7f0a0b00

.field public static final layout:I = 0x7f0a0b03

.field public static final left:I = 0x7f0a0b3f

.field public static final leftToRight:I = 0x7f0a0b40

.field public static final left_bottom:I = 0x7f0a0b41

.field public static final legacy:I = 0x7f0a0b44

.field public static final light:I = 0x7f0a0b45

.field public static final line1:I = 0x7f0a0b47

.field public static final line3:I = 0x7f0a0b4a

.field public static final linear:I = 0x7f0a0b54

.field public static final listMode:I = 0x7f0a0b59

.field public static final list_item:I = 0x7f0a0b5c

.field public static final list_panel:I = 0x7f0a0b5e

.field public static final llButtons:I = 0x7f0a0b63

.field public static final ll_btn:I = 0x7f0a0b8e

.field public static final ll_content:I = 0x7f0a0bbb

.field public static final ll_root:I = 0x7f0a0cc0

.field public static final lottie_layer_name:I = 0x7f0a0d56

.field public static final m3_side_sheet:I = 0x7f0a0d64

.field public static final main_fragment_id:I = 0x7f0a0d72

.field public static final marquee:I = 0x7f0a0d80

.field public static final masked:I = 0x7f0a0d89

.field public static final massage_blank:I = 0x7f0a0d8a

.field public static final match_constraint:I = 0x7f0a0d8b

.field public static final match_parent:I = 0x7f0a0d8d

.field public static final material_clock_display:I = 0x7f0a0d8e

.field public static final material_clock_display_and_toggle:I = 0x7f0a0d8f

.field public static final material_clock_face:I = 0x7f0a0d90

.field public static final material_clock_hand:I = 0x7f0a0d91

.field public static final material_clock_level:I = 0x7f0a0d92

.field public static final material_clock_period_am_button:I = 0x7f0a0d93

.field public static final material_clock_period_pm_button:I = 0x7f0a0d94

.field public static final material_clock_period_toggle:I = 0x7f0a0d95

.field public static final material_hour_text_input:I = 0x7f0a0d96

.field public static final material_hour_tv:I = 0x7f0a0d97

.field public static final material_label:I = 0x7f0a0d98

.field public static final material_minute_text_input:I = 0x7f0a0d99

.field public static final material_minute_tv:I = 0x7f0a0d9a

.field public static final material_textinput_timepicker:I = 0x7f0a0d9b

.field public static final material_timepicker_cancel_button:I = 0x7f0a0d9c

.field public static final material_timepicker_container:I = 0x7f0a0d9d

.field public static final material_timepicker_mode_button:I = 0x7f0a0d9e

.field public static final material_timepicker_ok_button:I = 0x7f0a0d9f

.field public static final material_timepicker_view:I = 0x7f0a0da0

.field public static final material_value_index:I = 0x7f0a0da1

.field public static final matrix:I = 0x7f0a0da2

.field public static final media_actions:I = 0x7f0a0da4

.field public static final menu_list:I = 0x7f0a0da8

.field public static final menu_title:I = 0x7f0a0dad

.field public static final message:I = 0x7f0a0daf

.field public static final message_close:I = 0x7f0a0db0

.field public static final message_close_test_time:I = 0x7f0a0db1

.field public static final message_content:I = 0x7f0a0db2

.field public static final message_icon:I = 0x7f0a0db3

.field public static final message_panel:I = 0x7f0a0db4

.field public static final message_root:I = 0x7f0a0db5

.field public static final middle:I = 0x7f0a0db8

.field public static final mini:I = 0x7f0a0dbd

.field public static final month_grid:I = 0x7f0a0dc3

.field public static final month_navigation_bar:I = 0x7f0a0dc4

.field public static final month_navigation_fragment_toggle:I = 0x7f0a0dc5

.field public static final month_navigation_next:I = 0x7f0a0dc6

.field public static final month_navigation_previous:I = 0x7f0a0dc7

.field public static final month_title:I = 0x7f0a0dc8

.field public static final motion_base:I = 0x7f0a0dc9

.field public static final mtrl_anchor_parent:I = 0x7f0a0dd1

.field public static final mtrl_calendar_day_selector_frame:I = 0x7f0a0dd2

.field public static final mtrl_calendar_days_of_week:I = 0x7f0a0dd3

.field public static final mtrl_calendar_frame:I = 0x7f0a0dd4

.field public static final mtrl_calendar_main_pane:I = 0x7f0a0dd5

.field public static final mtrl_calendar_months:I = 0x7f0a0dd6

.field public static final mtrl_calendar_selection_frame:I = 0x7f0a0dd7

.field public static final mtrl_calendar_text_input_frame:I = 0x7f0a0dd8

.field public static final mtrl_calendar_year_selector_frame:I = 0x7f0a0dd9

.field public static final mtrl_card_checked_layer_id:I = 0x7f0a0dda

.field public static final mtrl_child_content_container:I = 0x7f0a0ddb

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a0ddc

.field public static final mtrl_motion_snapshot_view:I = 0x7f0a0ddd

.field public static final mtrl_picker_fullscreen:I = 0x7f0a0dde

.field public static final mtrl_picker_header:I = 0x7f0a0ddf

.field public static final mtrl_picker_header_selection_text:I = 0x7f0a0de0

.field public static final mtrl_picker_header_title_and_selection:I = 0x7f0a0de1

.field public static final mtrl_picker_header_toggle:I = 0x7f0a0de2

.field public static final mtrl_picker_text_input_date:I = 0x7f0a0de3

.field public static final mtrl_picker_text_input_range_end:I = 0x7f0a0de4

.field public static final mtrl_picker_text_input_range_start:I = 0x7f0a0de5

.field public static final mtrl_picker_title_text:I = 0x7f0a0de6

.field public static final mtrl_view_tag_bottom_padding:I = 0x7f0a0de7

.field public static final multiply:I = 0x7f0a0dea

.field public static final navigation_bar_item_active_indicator_view:I = 0x7f0a0df2

.field public static final navigation_bar_item_icon_container:I = 0x7f0a0df3

.field public static final navigation_bar_item_icon_view:I = 0x7f0a0df4

.field public static final navigation_bar_item_labels_group:I = 0x7f0a0df5

.field public static final navigation_bar_item_large_label_view:I = 0x7f0a0df6

.field public static final navigation_bar_item_small_label_view:I = 0x7f0a0df7

.field public static final navigation_header_container:I = 0x7f0a0df8

.field public static final neverCompleteToEnd:I = 0x7f0a0dfd

.field public static final neverCompleteToStart:I = 0x7f0a0dfe

.field public static final noState:I = 0x7f0a0e05

.field public static final none:I = 0x7f0a0e06

.field public static final normal:I = 0x7f0a0e07

.field public static final north:I = 0x7f0a0e0e

.field public static final notification_background:I = 0x7f0a0e10

.field public static final notification_main_column:I = 0x7f0a0e11

.field public static final notification_main_column_container:I = 0x7f0a0e12

.field public static final nowrap:I = 0x7f0a0e15

.field public static final off:I = 0x7f0a0e29

.field public static final on:I = 0x7f0a0e2e

.field public static final outline:I = 0x7f0a0e3e

.field public static final outward:I = 0x7f0a0e3f

.field public static final overshoot:I = 0x7f0a0e40

.field public static final packed:I = 0x7f0a0e41

.field public static final parallax:I = 0x7f0a0e4f

.field public static final parent:I = 0x7f0a0e50

.field public static final parentPanel:I = 0x7f0a0e51

.field public static final parentRelative:I = 0x7f0a0e52

.field public static final parent_matrix:I = 0x7f0a0e53

.field public static final password_toggle:I = 0x7f0a0e57

.field public static final path:I = 0x7f0a0e58

.field public static final pathRelative:I = 0x7f0a0e59

.field public static final pb_loading:I = 0x7f0a0e5f

.field public static final percent:I = 0x7f0a0e78

.field public static final pin:I = 0x7f0a0e81

.field public static final popup_menu_listview:I = 0x7f0a0e88

.field public static final position:I = 0x7f0a0e89

.field public static final postLayout:I = 0x7f0a0e8a

.field public static final pressed:I = 0x7f0a0e8f

.field public static final progress:I = 0x7f0a0e96

.field public static final progress_circular:I = 0x7f0a0e9a

.field public static final progress_horizontal:I = 0x7f0a0e9b

.field public static final progress_number:I = 0x7f0a0e9c

.field public static final progress_percent:I = 0x7f0a0e9d

.field public static final purchase_viewpager:I = 0x7f0a0ebe

.field public static final purchase_viewpager_dots:I = 0x7f0a0ebf

.field public static final radio:I = 0x7f0a0ece

.field public static final rectangles:I = 0x7f0a0f0f

.field public static final restart:I = 0x7f0a0f30

.field public static final reverse:I = 0x7f0a0f34

.field public static final reverseSawtooth:I = 0x7f0a0f35

.field public static final right:I = 0x7f0a0f4f

.field public static final rightToLeft:I = 0x7f0a0f50

.field public static final right_icon:I = 0x7f0a0f51

.field public static final right_side:I = 0x7f0a0f53

.field public static final rl_cs_loading_root:I = 0x7f0a0f82

.field public static final root_rotate:I = 0x7f0a1007

.field public static final rounded:I = 0x7f0a1012

.field public static final row:I = 0x7f0a1013

.field public static final row_index_key:I = 0x7f0a1014

.field public static final row_reverse:I = 0x7f0a1015

.field public static final save_non_transition_alpha:I = 0x7f0a107c

.field public static final save_overlay_view:I = 0x7f0a107d

.field public static final sawtooth:I = 0x7f0a107e

.field public static final scale:I = 0x7f0a1093

.field public static final screen:I = 0x7f0a1096

.field public static final scrollIndicatorDown:I = 0x7f0a1099

.field public static final scrollIndicatorUp:I = 0x7f0a109a

.field public static final scrollView:I = 0x7f0a109b

.field public static final scroll_view:I = 0x7f0a10a1

.field public static final scrollable:I = 0x7f0a10a3

.field public static final search_badge:I = 0x7f0a10a7

.field public static final search_bar:I = 0x7f0a10a8

.field public static final search_bar_text_view:I = 0x7f0a10a9

.field public static final search_button:I = 0x7f0a10aa

.field public static final search_close_btn:I = 0x7f0a10ab

.field public static final search_edit_frame:I = 0x7f0a10ac

.field public static final search_go_btn:I = 0x7f0a10ad

.field public static final search_mag_icon:I = 0x7f0a10af

.field public static final search_plate:I = 0x7f0a10b0

.field public static final search_src_text:I = 0x7f0a10b1

.field public static final search_view_background:I = 0x7f0a10b2

.field public static final search_view_clear_button:I = 0x7f0a10b3

.field public static final search_view_content_container:I = 0x7f0a10b4

.field public static final search_view_divider:I = 0x7f0a10b5

.field public static final search_view_dummy_toolbar:I = 0x7f0a10b6

.field public static final search_view_edit_text:I = 0x7f0a10b7

.field public static final search_view_header_container:I = 0x7f0a10b8

.field public static final search_view_root:I = 0x7f0a10b9

.field public static final search_view_scrim:I = 0x7f0a10ba

.field public static final search_view_search_prefix:I = 0x7f0a10bb

.field public static final search_view_status_bar_spacer:I = 0x7f0a10bc

.field public static final search_view_toolbar:I = 0x7f0a10bd

.field public static final search_view_toolbar_container:I = 0x7f0a10be

.field public static final search_voice_btn:I = 0x7f0a10bf

.field public static final sectioning_adapter_tag_key_view_viewholder:I = 0x7f0a10c1

.field public static final select_dialog_listview:I = 0x7f0a10c3

.field public static final selected:I = 0x7f0a10c6

.field public static final selection_type:I = 0x7f0a10c7

.field public static final sep_recent_other:I = 0x7f0a10cd

.field public static final sharedValueSet:I = 0x7f0a10d2

.field public static final sharedValueUnset:I = 0x7f0a10d3

.field public static final shortcut:I = 0x7f0a10d4

.field public static final sin:I = 0x7f0a10e4

.field public static final skipped:I = 0x7f0a10e8

.field public static final slide:I = 0x7f0a10ea

.field public static final snackbar_action:I = 0x7f0a10ee

.field public static final snackbar_text:I = 0x7f0a10ef

.field public static final software:I = 0x7f0a10f2

.field public static final south:I = 0x7f0a10f3

.field public static final space_around:I = 0x7f0a10f8

.field public static final space_between:I = 0x7f0a10fb

.field public static final space_evenly:I = 0x7f0a1105

.field public static final spacer:I = 0x7f0a1111

.field public static final special_effects_controller_view_tag:I = 0x7f0a1112

.field public static final spline:I = 0x7f0a1115

.field public static final split_action_bar:I = 0x7f0a1116

.field public static final spread:I = 0x7f0a1117

.field public static final spread_inside:I = 0x7f0a1118

.field public static final spring:I = 0x7f0a1119

.field public static final square:I = 0x7f0a111a

.field public static final src_atop:I = 0x7f0a111b

.field public static final src_in:I = 0x7f0a111c

.field public static final src_over:I = 0x7f0a111d

.field public static final standard:I = 0x7f0a111e

.field public static final start:I = 0x7f0a111f

.field public static final startHorizontal:I = 0x7f0a1120

.field public static final startToEnd:I = 0x7f0a1121

.field public static final startVertical:I = 0x7f0a1122

.field public static final staticLayout:I = 0x7f0a1125

.field public static final staticPostLayout:I = 0x7f0a1126

.field public static final status_bar_latest_event_content:I = 0x7f0a112a

.field public static final stop:I = 0x7f0a1135

.field public static final stretch:I = 0x7f0a1136

.field public static final submenuarrow:I = 0x7f0a1154

.field public static final submit_area:I = 0x7f0a1155

.field public static final tabMode:I = 0x7f0a1174

.field public static final tag_accessibility_actions:I = 0x7f0a1180

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a1181

.field public static final tag_accessibility_heading:I = 0x7f0a1182

.field public static final tag_accessibility_pane_title:I = 0x7f0a1183

.field public static final tag_ad_id:I = 0x7f0a1184

.field public static final tag_doc_tencent_id:I = 0x7f0a1187

.field public static final tag_feed_back_id:I = 0x7f0a1188

.field public static final tag_on_apply_window_listener:I = 0x7f0a118c

.field public static final tag_on_receive_content_listener:I = 0x7f0a118d

.field public static final tag_on_receive_content_mime_types:I = 0x7f0a118e

.field public static final tag_screen_reader_focusable:I = 0x7f0a118f

.field public static final tag_state_description:I = 0x7f0a1191

.field public static final tag_transition_group:I = 0x7f0a1193

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a1194

.field public static final tag_unhandled_key_listeners:I = 0x7f0a1195

.field public static final tag_window_insets_animation_callback:I = 0x7f0a1197

.field public static final text:I = 0x7f0a119d

.field public static final text2:I = 0x7f0a119f

.field public static final textSpacerNoButtons:I = 0x7f0a11a3

.field public static final textSpacerNoTitle:I = 0x7f0a11a4

.field public static final text_input_end_icon:I = 0x7f0a11b6

.field public static final text_input_error_icon:I = 0x7f0a11b7

.field public static final text_input_start_icon:I = 0x7f0a11b8

.field public static final textinput_counter:I = 0x7f0a11bd

.field public static final textinput_error:I = 0x7f0a11be

.field public static final textinput_helper_text:I = 0x7f0a11bf

.field public static final textinput_placeholder:I = 0x7f0a11c0

.field public static final textinput_prefix_text:I = 0x7f0a11c1

.field public static final textinput_suffix_text:I = 0x7f0a11c2

.field public static final time:I = 0x7f0a11c8

.field public static final title:I = 0x7f0a11d1

.field public static final titleDividerNoCustom:I = 0x7f0a11d2

.field public static final title_container:I = 0x7f0a11d3

.field public static final title_panel:I = 0x7f0a11d4

.field public static final title_template:I = 0x7f0a11d6

.field public static final toolbar:I = 0x7f0a11df

.field public static final toolbar_menu_container:I = 0x7f0a11e5

.field public static final toolbar_title:I = 0x7f0a11eb

.field public static final toolbar_title_container_layout:I = 0x7f0a11ec

.field public static final toolbar_title_layout:I = 0x7f0a11ed

.field public static final top:I = 0x7f0a11ee

.field public static final topPanel:I = 0x7f0a11ef

.field public static final touch_outside:I = 0x7f0a11fc

.field public static final transition_current_scene:I = 0x7f0a1200

.field public static final transition_layout_save:I = 0x7f0a1202

.field public static final transition_position:I = 0x7f0a1205

.field public static final transition_scene_layoutid_cache:I = 0x7f0a1206

.field public static final transition_transform:I = 0x7f0a1207

.field public static final triangle:I = 0x7f0a1211

.field public static final tv:I = 0x7f0a1221

.field public static final tv_base:I = 0x7f0a1290

.field public static final tv_cover_num:I = 0x7f0a1346

.field public static final tv_des:I = 0x7f0a136d

.field public static final tv_dialog_enable:I = 0x7f0a1381

.field public static final tv_dialog_title:I = 0x7f0a1388

.field public static final tv_dialog_unable:I = 0x7f0a1398

.field public static final tv_flag:I = 0x7f0a1448

.field public static final tv_info:I = 0x7f0a14fc

.field public static final tv_message:I = 0x7f0a15be

.field public static final tv_show_thumb:I = 0x7f0a17cf

.field public static final tv_squared_label:I = 0x7f0a17f1

.field public static final tv_title:I = 0x7f0a187b

.field public static final tv_title_tips:I = 0x7f0a1891

.field public static final tv_verify_code_1:I = 0x7f0a18fb

.field public static final tv_verify_code_2:I = 0x7f0a18fc

.field public static final tv_verify_code_3:I = 0x7f0a18fd

.field public static final tv_verify_code_4:I = 0x7f0a18fe

.field public static final tv_verify_code_5:I = 0x7f0a18ff

.field public static final tv_verify_code_6:I = 0x7f0a1900

.field public static final unchecked:I = 0x7f0a196b

.field public static final uniform:I = 0x7f0a196d

.field public static final unlabeled:I = 0x7f0a196f

.field public static final up:I = 0x7f0a1971

.field public static final vertical_only:I = 0x7f0a19f6

.field public static final view_flipper:I = 0x7f0a1a1c

.field public static final view_offset_helper:I = 0x7f0a1a3b

.field public static final view_transition:I = 0x7f0a1a71

.field public static final view_tree_lifecycle_owner:I = 0x7f0a1a72

.field public static final view_tree_on_back_pressed_dispatcher_owner:I = 0x7f0a1a73

.field public static final view_tree_saved_state_registry_owner:I = 0x7f0a1a74

.field public static final view_tree_view_model_store_owner:I = 0x7f0a1a75

.field public static final visible:I = 0x7f0a1a84

.field public static final visible_removing_fragment_view_tag:I = 0x7f0a1a85

.field public static final west:I = 0x7f0a1ab8

.field public static final wide:I = 0x7f0a1abf

.field public static final widget_Rotate:I = 0x7f0a1ac0

.field public static final widget_imageview:I = 0x7f0a1ac1

.field public static final widget_textview:I = 0x7f0a1ac3

.field public static final with_icon:I = 0x7f0a1ac5

.field public static final withinBounds:I = 0x7f0a1ac6

.field public static final wrap:I = 0x7f0a1acc

.field public static final wrap_content:I = 0x7f0a1acd

.field public static final wrap_content_constrained:I = 0x7f0a1ace

.field public static final wrap_reverse:I = 0x7f0a1acf

.field public static final x_left:I = 0x7f0a1ad0

.field public static final x_right:I = 0x7f0a1ad1

.field public static final xbutton:I = 0x7f0a1ad3

.field public static final xedit:I = 0x7f0a1ad4

.field public static final xedit_layout:I = 0x7f0a1ad5


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

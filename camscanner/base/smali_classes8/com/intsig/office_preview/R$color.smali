.class public final Lcom/intsig/office_preview/R$color;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office_preview/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f060000

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f060001

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f060002

.field public static final abc_btn_colored_text_material:I = 0x7f060003

.field public static final abc_color_highlight_material:I = 0x7f060004

.field public static final abc_decor_view_status_guard:I = 0x7f060005

.field public static final abc_decor_view_status_guard_light:I = 0x7f060006

.field public static final abc_hint_foreground_material_dark:I = 0x7f060007

.field public static final abc_hint_foreground_material_light:I = 0x7f060008

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f060009

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f06000a

.field public static final abc_primary_text_material_dark:I = 0x7f06000b

.field public static final abc_primary_text_material_light:I = 0x7f06000c

.field public static final abc_search_url_text:I = 0x7f06000d

.field public static final abc_search_url_text_normal:I = 0x7f06000e

.field public static final abc_search_url_text_pressed:I = 0x7f06000f

.field public static final abc_search_url_text_selected:I = 0x7f060010

.field public static final abc_secondary_text_material_dark:I = 0x7f060011

.field public static final abc_secondary_text_material_light:I = 0x7f060012

.field public static final abc_tint_btn_checkable:I = 0x7f060013

.field public static final abc_tint_default:I = 0x7f060014

.field public static final abc_tint_edittext:I = 0x7f060015

.field public static final abc_tint_seek_thumb:I = 0x7f060016

.field public static final abc_tint_spinner:I = 0x7f060017

.field public static final abc_tint_switch_track:I = 0x7f060018

.field public static final accent_material_dark:I = 0x7f060019

.field public static final accent_material_light:I = 0x7f06001a

.field public static final action_bar_backgroud_color:I = 0x7f06001b

.field public static final action_bar_backgroud_color_alpha:I = 0x7f06001c

.field public static final action_bar_backgroud_color_only_read:I = 0x7f06001d

.field public static final actionbar_button_pressed:I = 0x7f06001e

.field public static final activity_bg_color:I = 0x7f06001f

.field public static final ad_background_color:I = 0x7f060020

.field public static final ad_holo_light_pressed:I = 0x7f060021

.field public static final add_tag_fab_color:I = 0x7f060022

.field public static final androidx_core_ripple_material_light:I = 0x7f06002b

.field public static final androidx_core_secondary_text_default_material_light:I = 0x7f06002c

.field public static final appwidget_text:I = 0x7f060047

.field public static final background:I = 0x7f060048

.field public static final background_floating_material_dark:I = 0x7f060049

.field public static final background_floating_material_light:I = 0x7f06004a

.field public static final background_material_dark:I = 0x7f06004b

.field public static final background_material_light:I = 0x7f06004c

.field public static final bg_addcolla_list_selected:I = 0x7f06004d

.field public static final bg_btn_green_disabled:I = 0x7f06004e

.field public static final bg_btn_green_pressed:I = 0x7f06004f

.field public static final bg_circle_progress_bar:I = 0x7f060050

.field public static final bg_composite_image:I = 0x7f060051

.field public static final bg_doc_popmenu:I = 0x7f060052

.field public static final bg_edt_doc_title_focused:I = 0x7f060053

.field public static final bg_imagepage_bar:I = 0x7f060054

.field public static final bg_line_color:I = 0x7f060055

.field public static final bg_main_menu_hint:I = 0x7f060056

.field public static final bg_menu_black_style:I = 0x7f060057

.field public static final bg_menu_item_normal:I = 0x7f060058

.field public static final bg_menu_item_selected:I = 0x7f060059

.field public static final bg_pack_alpha_touch_dismiss:I = 0x7f06005a

.field public static final bg_send_print_fax:I = 0x7f06005b

.field public static final bg_tag_list_item_clicked:I = 0x7f06005c

.field public static final bg_upgrade_describtion:I = 0x7f06005d

.field public static final bg_white:I = 0x7f06005e

.field public static final black:I = 0x7f06005f

.field public static final black_98:I = 0x7f060060

.field public static final border_color:I = 0x7f060061

.field public static final bright_foreground_disabled_material_dark:I = 0x7f060062

.field public static final bright_foreground_disabled_material_light:I = 0x7f060063

.field public static final bright_foreground_inverse_material_dark:I = 0x7f060064

.field public static final bright_foreground_inverse_material_light:I = 0x7f060065

.field public static final bright_foreground_material_dark:I = 0x7f060066

.field public static final bright_foreground_material_light:I = 0x7f060067

.field public static final btn_background_color:I = 0x7f06006c

.field public static final btn_bg_red_color:I = 0x7f06006d

.field public static final btn_disable_inside:I = 0x7f06006e

.field public static final btn_disable_stroke:I = 0x7f06006f

.field public static final btn_holo_dark_disable:I = 0x7f060070

.field public static final btn_holo_dark_normal:I = 0x7f060071

.field public static final btn_holo_dark_pressed:I = 0x7f060072

.field public static final btn_holo_light_disable:I = 0x7f060073

.field public static final btn_holo_light_normal:I = 0x7f060074

.field public static final btn_holo_light_pressed:I = 0x7f060075

.field public static final btn_press_red_color:I = 0x7f060076

.field public static final btn_stroke_color:I = 0x7f060077

.field public static final btn_text_holo:I = 0x7f060078

.field public static final btn_text_holo_light:I = 0x7f060079

.field public static final btn_unable:I = 0x7f06007a

.field public static final button_enable:I = 0x7f06007b

.field public static final button_material_dark:I = 0x7f06007c

.field public static final button_material_light:I = 0x7f06007d

.field public static final button_unable:I = 0x7f06007e

.field public static final cad_black:I = 0x7f06007f

.field public static final cad_gray:I = 0x7f060081

.field public static final cad_light:I = 0x7f060082

.field public static final cad_transparent:I = 0x7f060083

.field public static final cad_white:I = 0x7f060084

.field public static final calendar_background:I = 0x7f060085

.field public static final cardview_dark_background:I = 0x7f060086

.field public static final cardview_light_background:I = 0x7f060087

.field public static final cardview_shadow_end_color:I = 0x7f060088

.field public static final cardview_shadow_start_color:I = 0x7f060089

.field public static final check_box_material:I = 0x7f06008b

.field public static final check_box_other_share_doc_list:I = 0x7f06008c

.field public static final choose_country_edittext_color:I = 0x7f06008d

.field public static final colorAccent:I = 0x7f06008e

.field public static final colorAccentAlpha:I = 0x7f06008f

.field public static final colorAccentAlphaLight:I = 0x7f060090

.field public static final colorBackgroundCacheHint:I = 0x7f060091

.field public static final colorPrimary:I = 0x7f060092

.field public static final colorPrimaryDark:I = 0x7f060093

.field public static final color_09403A:I = 0x7f060094

.field public static final color_19BCAA:I = 0x7f060095

.field public static final color_1B2D2F:I = 0x7f060096

.field public static final color_20ba9d:I = 0x7f060097

.field public static final color_617D9D:I = 0x7f060098

.field public static final color_7DB4B3:I = 0x7f060099

.field public static final color_7f7f7f:I = 0x7f06009a

.field public static final color_94000000:I = 0x7f06009b

.field public static final color_99101500:I = 0x7f06009c

.field public static final color_FF101500:I = 0x7f06009d

.field public static final color_FF181818:I = 0x7f06009e

.field public static final color_FF19BC9C:I = 0x7f06009f

.field public static final color_FF1C1C1E:I = 0x7f0600a0

.field public static final color_FF30BC9F:I = 0x7f0600a1

.field public static final color_FF455A6C:I = 0x7f0600a2

.field public static final color_FF455A6D:I = 0x7f0600a3

.field public static final color_FF4B5A65:I = 0x7f0600a4

.field public static final color_FF69737C:I = 0x7f0600a5

.field public static final color_FF7A8590:I = 0x7f0600a6

.field public static final color_FF7E7E7E:I = 0x7f0600a7

.field public static final color_FF848484:I = 0x7f0600a8

.field public static final color_FF9C9C9C:I = 0x7f0600a9

.field public static final color_FFE6E6E6:I = 0x7f0600aa

.field public static final color_FFF7F7F7:I = 0x7f0600ab

.field public static final color_FFFBFBFB:I = 0x7f0600ac

.field public static final color_black_03:I = 0x7f0600ad

.field public static final color_blue_267DBA:I = 0x7f0600ae

.field public static final color_blue_79EEEB:I = 0x7f0600af

.field public static final color_blue_C8E4FF:I = 0x7f0600b0

.field public static final color_brown_512F0A:I = 0x7f0600b1

.field public static final color_email_hight_light:I = 0x7f0600b2

.field public static final color_green_043A3F:I = 0x7f0600b4

.field public static final color_green_095156:I = 0x7f0600b5

.field public static final color_green_7C0B4C51:I = 0x7f0600b6

.field public static final color_green_EAFCF8:I = 0x7f0600b7

.field public static final color_green_FFEBCC:I = 0x7f0600b8

.field public static final color_image_break:I = 0x7f0600b9

.field public static final color_main_color:I = 0x7f0600ba

.field public static final color_pink_FF7255:I = 0x7f0600bb

.field public static final color_reward_blue:I = 0x7f0600bc

.field public static final color_reward_deep_green:I = 0x7f0600bd

.field public static final color_reward_green:I = 0x7f0600be

.field public static final color_reward_red:I = 0x7f0600bf

.field public static final color_reward_yellow:I = 0x7f0600c0

.field public static final color_search_hight_light_bg:I = 0x7f0600c1

.field public static final color_word_normal:I = 0x7f0600c2

.field public static final common_google_signin_btn_text_dark:I = 0x7f0600d4

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f0600d5

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f0600d6

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0600d7

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f0600d8

.field public static final common_google_signin_btn_text_light:I = 0x7f0600d9

.field public static final common_google_signin_btn_text_light_default:I = 0x7f0600da

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f0600db

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0600dc

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f0600dd

.field public static final common_google_signin_btn_tint:I = 0x7f0600de

.field public static final compposite_line_color:I = 0x7f0600df

.field public static final cs_17bd9e:I = 0x7f0600e0

.field public static final cs_1fe1cc:I = 0x7f0600e1

.field public static final cs_CCFFFFFF:I = 0x7f0600e2

.field public static final cs_base_000000:I = 0x7f0600e3

.field public static final cs_base_1C1C1E:I = 0x7f0600e4

.field public static final cs_base_1e2939:I = 0x7f0600e5

.field public static final cs_base_212121:I = 0x7f0600e6

.field public static final cs_base_34485E:I = 0x7f0600e7

.field public static final cs_base_FFFFFF:I = 0x7f0600e8

.field public static final cs_base_actionbar_button_pressed:I = 0x7f0600e9

.field public static final cs_base_cad_gray:I = 0x7f0600ea

.field public static final cs_bg_EFEFEF:I = 0x7f0600eb

.field public static final cs_black:I = 0x7f0600ec

.field public static final cs_black_0D7DFC:I = 0x7f0600ed

.field public static final cs_black_141414:I = 0x7f0600ee

.field public static final cs_black_212121:I = 0x7f0600ef

.field public static final cs_black_223346:I = 0x7f0600f0

.field public static final cs_black_333333:I = 0x7f0600f1

.field public static final cs_black_33495E:I = 0x7f0600f2

.field public static final cs_black_34485E:I = 0x7f0600f3

.field public static final cs_black_435B74:I = 0x7f0600f4

.field public static final cs_black_4B4B4B:I = 0x7f0600f5

.field public static final cs_black_4B4B4B_60:I = 0x7f0600f6

.field public static final cs_black_536F8B:I = 0x7f0600f7

.field public static final cs_black_617D9D:I = 0x7f0600f8

.field public static final cs_black_7890AE:I = 0x7f0600f9

.field public static final cs_black_805A5A5A:I = 0x7f0600fa

.field public static final cs_black_8EA5BF:I = 0x7f0600fb

.field public static final cs_black_95:I = 0x7f0600fc

.field public static final cs_black_99000000:I = 0x7f0600fd

.field public static final cs_black_99223346:I = 0x7f0600fe

.field public static final cs_black_995A5A5A:I = 0x7f0600ff

.field public static final cs_black_9C223346:I = 0x7f060100

.field public static final cs_black_ACBFD6:I = 0x7f060101

.field public static final cs_black_AE223346:I = 0x7f060102

.field public static final cs_black_B3000000:I = 0x7f060103

.field public static final cs_black_BD212121:I = 0x7f060104

.field public static final cs_black_C8D9EC:I = 0x7f060105

.field public static final cs_black_D9000000:I = 0x7f060106

.field public static final cs_black_E6EFFF:I = 0x7f060107

.field public static final cs_black_FF1D2A3A:I = 0x7f060108

.field public static final cs_black_FF223346:I = 0x7f060109

.field public static final cs_black_FF353535:I = 0x7f06010a

.field public static final cs_black_FF5A5A5A:I = 0x7f06010b

.field public static final cs_blackgold_4CDFB981:I = 0x7f06010c

.field public static final cs_blackgold_FFDFB981:I = 0x7f06010d

.field public static final cs_blue_0077FF:I = 0x7f06010e

.field public static final cs_blue_008FED:I = 0x7f06010f

.field public static final cs_blue_1DA9FF:I = 0x7f060110

.field public static final cs_blue_48AFE6:I = 0x7f060111

.field public static final cs_blue_4BAFFD:I = 0x7f060112

.field public static final cs_blue_50D5F4:I = 0x7f060113

.field public static final cs_bronze_4CFEC1AB:I = 0x7f060114

.field public static final cs_bronze_FFFEC1AB:I = 0x7f060115

.field public static final cs_card_photo_tips:I = 0x7f060116

.field public static final cs_color_000000:I = 0x7f060117

.field public static final cs_color_007AFF:I = 0x7f060118

.field public static final cs_color_00ad79:I = 0x7f060119

.field public static final cs_color_020203:I = 0x7f06011a

.field public static final cs_color_095156:I = 0x7f06011b

.field public static final cs_color_09A94B:I = 0x7f06011c

.field public static final cs_color_0D0D0D:I = 0x7f06011d

.field public static final cs_color_0D1D3A:I = 0x7f06011e

.field public static final cs_color_0DACD5:I = 0x7f06011f

.field public static final cs_color_0a1b37:I = 0x7f060120

.field public static final cs_color_19BC51:I = 0x7f060121

.field public static final cs_color_19BC51_10_percdent:I = 0x7f060122

.field public static final cs_color_19BCAA:I = 0x7f060123

.field public static final cs_color_19BCAA_10_percent:I = 0x7f060124

.field public static final cs_color_1A19BCAA:I = 0x7f060125

.field public static final cs_color_1C1C1E:I = 0x7f060126

.field public static final cs_color_1C2A42:I = 0x7f060127

.field public static final cs_color_1D1D1D:I = 0x7f060128

.field public static final cs_color_1EFF6748:I = 0x7f060129

.field public static final cs_color_223345:I = 0x7f06012a

.field public static final cs_color_242424:I = 0x7f06012b

.field public static final cs_color_242424_0:I = 0x7f06012c

.field public static final cs_color_242424_80:I = 0x7f06012d

.field public static final cs_color_283750:I = 0x7f06012e

.field public static final cs_color_283752:I = 0x7f06012f

.field public static final cs_color_2A2620:I = 0x7f060130

.field public static final cs_color_2C2C2D:I = 0x7f060131

.field public static final cs_color_2C2C2E:I = 0x7f060132

.field public static final cs_color_383648:I = 0x7f060133

.field public static final cs_color_3DADE9:I = 0x7f060134

.field public static final cs_color_413F52:I = 0x7f060135

.field public static final cs_color_413F52_0:I = 0x7f060136

.field public static final cs_color_413F52_80:I = 0x7f060137

.field public static final cs_color_4580E4:I = 0x7f060138

.field public static final cs_color_4580F2:I = 0x7f060139

.field public static final cs_color_4580F2_10_percdent:I = 0x7f06013a

.field public static final cs_color_49475B:I = 0x7f06013b

.field public static final cs_color_494949:I = 0x7f06013c

.field public static final cs_color_49494A:I = 0x7f06013d

.field public static final cs_color_515DDF:I = 0x7f06013e

.field public static final cs_color_535353:I = 0x7f06013f

.field public static final cs_color_53A93F:I = 0x7f060140

.field public static final cs_color_546470:I = 0x7f060141

.field public static final cs_color_581B00:I = 0x7f060142

.field public static final cs_color_5890FB:I = 0x7f060143

.field public static final cs_color_5ADFD4:I = 0x7f060144

.field public static final cs_color_5B77C7:I = 0x7f060145

.field public static final cs_color_5E3519:I = 0x7f060146

.field public static final cs_color_60534C:I = 0x7f060147

.field public static final cs_color_626074:I = 0x7f060148

.field public static final cs_color_747474:I = 0x7f060149

.field public static final cs_color_788389:I = 0x7f06014a

.field public static final cs_color_7F7F85:I = 0x7f06014b

.field public static final cs_color_805537:I = 0x7f06014c

.field public static final cs_color_809C9C9C:I = 0x7f06014d

.field public static final cs_color_80FFFFFF:I = 0x7f06014e

.field public static final cs_color_8150F8:I = 0x7f06014f

.field public static final cs_color_838383:I = 0x7f060150

.field public static final cs_color_878787:I = 0x7f060151

.field public static final cs_color_8B8A8F:I = 0x7f060152

.field public static final cs_color_979797:I = 0x7f060153

.field public static final cs_color_9DAAB3:I = 0x7f060154

.field public static final cs_color_A0DCD9:I = 0x7f060155

.field public static final cs_color_A34A0F:I = 0x7f060156

.field public static final cs_color_A6D2E8:I = 0x7f060157

.field public static final cs_color_AA8367:I = 0x7f060158

.field public static final cs_color_ABC6F0:I = 0x7f060159

.field public static final cs_color_ACDDDB:I = 0x7f06015a

.field public static final cs_color_ACDDDB_0:I = 0x7f06015b

.field public static final cs_color_ACDDDB_80:I = 0x7f06015c

.field public static final cs_color_AD9F7D:I = 0x7f06015d

.field public static final cs_color_AED8EE:I = 0x7f06015e

.field public static final cs_color_AED8EE_0:I = 0x7f06015f

.field public static final cs_color_AED8EE_80:I = 0x7f060160

.field public static final cs_color_B2C4F3:I = 0x7f060161

.field public static final cs_color_B2CEF3_0:I = 0x7f060162

.field public static final cs_color_B2CEF3_80:I = 0x7f060163

.field public static final cs_color_B5B5B5:I = 0x7f060164

.field public static final cs_color_B7FFFFFF:I = 0x7f060165

.field public static final cs_color_BCEAE5:I = 0x7f060166

.field public static final cs_color_BDBDBD:I = 0x7f060167

.field public static final cs_color_C3E3F4:I = 0x7f060168

.field public static final cs_color_C3EDE5:I = 0x7f060169

.field public static final cs_color_CC19BCAA:I = 0x7f06016a

.field public static final cs_color_CEEDEC:I = 0x7f06016b

.field public static final cs_color_D3480D:I = 0x7f06016c

.field public static final cs_color_D59B45:I = 0x7f06016d

.field public static final cs_color_D5DEFC:I = 0x7f06016e

.field public static final cs_color_D8F0EF:I = 0x7f06016f

.field public static final cs_color_D9D9D9:I = 0x7f060170

.field public static final cs_color_DB5658:I = 0x7f060171

.field public static final cs_color_DDA760:I = 0x7f060172

.field public static final cs_color_DFE5FC:I = 0x7f060173

.field public static final cs_color_E2F4FE:I = 0x7f060174

.field public static final cs_color_E3FFE1AA:I = 0x7f060175

.field public static final cs_color_E3FFE4B0:I = 0x7f060176

.field public static final cs_color_E5BA79:I = 0x7f060177

.field public static final cs_color_E98457:I = 0x7f060178

.field public static final cs_color_ECECEC:I = 0x7f060179

.field public static final cs_color_EDCC8B:I = 0x7f06017a

.field public static final cs_color_EECB94:I = 0x7f06017b

.field public static final cs_color_EFAC8E:I = 0x7f06017c

.field public static final cs_color_EFC788_10:I = 0x7f06017d

.field public static final cs_color_EFC788_40:I = 0x7f06017e

.field public static final cs_color_F1B54C:I = 0x7f06017f

.field public static final cs_color_F1F1F1:I = 0x7f060180

.field public static final cs_color_F2C384:I = 0x7f060181

.field public static final cs_color_F2D5A8:I = 0x7f060182

.field public static final cs_color_F3E0BA:I = 0x7f060183

.field public static final cs_color_F4B193:I = 0x7f060184

.field public static final cs_color_F4B193_0:I = 0x7f060185

.field public static final cs_color_F4B193_80:I = 0x7f060186

.field public static final cs_color_F4C36C:I = 0x7f060187

.field public static final cs_color_F4D9AF:I = 0x7f060188

.field public static final cs_color_F4F4F4:I = 0x7f060189

.field public static final cs_color_F5C776:I = 0x7f06018a

.field public static final cs_color_F5C776_0:I = 0x7f06018b

.field public static final cs_color_F5C776_80:I = 0x7f06018c

.field public static final cs_color_F5D08E:I = 0x7f06018d

.field public static final cs_color_F5D29C:I = 0x7f06018e

.field public static final cs_color_F6C1A9:I = 0x7f06018f

.field public static final cs_color_F6C87F:I = 0x7f060190

.field public static final cs_color_F6D597:I = 0x7f060191

.field public static final cs_color_F6DEB9:I = 0x7f060192

.field public static final cs_color_F6F6F8:I = 0x7f060193

.field public static final cs_color_F7DDC1:I = 0x7f060194

.field public static final cs_color_F7FAF9:I = 0x7f060195

.field public static final cs_color_F8E6C9:I = 0x7f060196

.field public static final cs_color_F9D8A5:I = 0x7f060197

.field public static final cs_color_FA7125:I = 0x7f060198

.field public static final cs_color_FAD1BF:I = 0x7f060199

.field public static final cs_color_FB5D29:I = 0x7f06019a

.field public static final cs_color_FBC793:I = 0x7f06019b

.field public static final cs_color_FBDAA0:I = 0x7f06019c

.field public static final cs_color_FBE2BC:I = 0x7f06019d

.field public static final cs_color_FCEDDD:I = 0x7f06019e

.field public static final cs_color_FCF8F0_20:I = 0x7f06019f

.field public static final cs_color_FD6261:I = 0x7f0601a0

.field public static final cs_color_FE9143:I = 0x7f0601a1

.field public static final cs_color_FEF1DA:I = 0x7f0601a2

.field public static final cs_color_FF111111:I = 0x7f0601a3

.field public static final cs_color_FF122C49:I = 0x7f0601a4

.field public static final cs_color_FF149688:I = 0x7f0601a5

.field public static final cs_color_FF19BCAA:I = 0x7f0601a6

.field public static final cs_color_FF238AFF:I = 0x7f0601a7

.field public static final cs_color_FF282421:I = 0x7f0601a8

.field public static final cs_color_FF492F0C:I = 0x7f0601a9

.field public static final cs_color_FF6A48:I = 0x7f0601aa

.field public static final cs_color_FF6D3B05:I = 0x7f0601ab

.field public static final cs_color_FF7255_10_percdent:I = 0x7f0601ac

.field public static final cs_color_FF7B5A:I = 0x7f0601ad

.field public static final cs_color_FF835C:I = 0x7f0601ae

.field public static final cs_color_FF893F:I = 0x7f0601af

.field public static final cs_color_FF9321_10_percdent:I = 0x7f0601b0

.field public static final cs_color_FF965B10:I = 0x7f0601b1

.field public static final cs_color_FFA0A0A0:I = 0x7f0601b2

.field public static final cs_color_FFA722:I = 0x7f0601b3

.field public static final cs_color_FFCE83:I = 0x7f0601b4

.field public static final cs_color_FFD022:I = 0x7f0601b5

.field public static final cs_color_FFD023:I = 0x7f0601b6

.field public static final cs_color_FFD59B45:I = 0x7f0601b7

.field public static final cs_color_FFDFDFDF:I = 0x7f0601b8

.field public static final cs_color_FFE0BD:I = 0x7f0601b9

.field public static final cs_color_FFE1DA:I = 0x7f0601ba

.field public static final cs_color_FFE4E6E9:I = 0x7f0601bb

.field public static final cs_color_FFE6E8EB:I = 0x7f0601bc

.field public static final cs_color_FFE7AD:I = 0x7f0601bd

.field public static final cs_color_FFE7AD_50:I = 0x7f0601be

.field public static final cs_color_FFE8AF62:I = 0x7f0601bf

.field public static final cs_color_FFE8BA7C:I = 0x7f0601c0

.field public static final cs_color_FFE8EAEC:I = 0x7f0601c1

.field public static final cs_color_FFEAECF3:I = 0x7f0601c2

.field public static final cs_color_FFECDD:I = 0x7f0601c3

.field public static final cs_color_FFECEEF0:I = 0x7f0601c4

.field public static final cs_color_FFF0DD:I = 0x7f0601c5

.field public static final cs_color_FFF1DC:I = 0x7f0601c6

.field public static final cs_color_FFF2D1:I = 0x7f0601c7

.field public static final cs_color_FFF2D9:I = 0x7f0601c8

.field public static final cs_color_FFF3E9:I = 0x7f0601c9

.field public static final cs_color_FFF4F5F6:I = 0x7f0601ca

.field public static final cs_color_FFF7E9:I = 0x7f0601cb

.field public static final cs_color_FFF7F7F9:I = 0x7f0601cc

.field public static final cs_color_FFF9EC:I = 0x7f0601cd

.field public static final cs_color_FFF9ED:I = 0x7f0601ce

.field public static final cs_color_FFFAF0_60:I = 0x7f0601cf

.field public static final cs_color_FFFAFAFC:I = 0x7f0601d0

.field public static final cs_color_FFFBE3B8:I = 0x7f0601d1

.field public static final cs_color_FFFBF3:I = 0x7f0601d2

.field public static final cs_color_FFFF6748:I = 0x7f0601d3

.field public static final cs_color_FFFF6949:I = 0x7f0601d4

.field public static final cs_color_FFFF8C3E:I = 0x7f0601d5

.field public static final cs_color_FFFF9079:I = 0x7f0601d6

.field public static final cs_color_FFFF9312:I = 0x7f0601d7

.field public static final cs_color_FFFFC8A6:I = 0x7f0601d8

.field public static final cs_color_FFFFE4B1:I = 0x7f0601d9

.field public static final cs_color_FFFFEFD1:I = 0x7f0601da

.field public static final cs_color_auxiliary_1:I = 0x7f0601db

.field public static final cs_color_auxiliary_2:I = 0x7f0601dc

.field public static final cs_color_auxiliary_3:I = 0x7f0601dd

.field public static final cs_color_auxiliary_4:I = 0x7f0601de

.field public static final cs_color_bcbcbc:I = 0x7f0601df

.field public static final cs_color_bg_0:I = 0x7f0601e0

.field public static final cs_color_bg_1:I = 0x7f0601e1

.field public static final cs_color_bg_1_1:I = 0x7f0601e2

.field public static final cs_color_bg_2:I = 0x7f0601e3

.field public static final cs_color_bg_3:I = 0x7f0601e4

.field public static final cs_color_bg_4:I = 0x7f0601e5

.field public static final cs_color_bg_4_1:I = 0x7f0601e6

.field public static final cs_color_bg_splash:I = 0x7f0601e7

.field public static final cs_color_bg_trans:I = 0x7f0601e8

.field public static final cs_color_black_10:I = 0x7f0601e9

.field public static final cs_color_border_0:I = 0x7f0601ea

.field public static final cs_color_border_1:I = 0x7f0601eb

.field public static final cs_color_border_2:I = 0x7f0601ec

.field public static final cs_color_border_3:I = 0x7f0601ed

.field public static final cs_color_brand:I = 0x7f0601ee

.field public static final cs_color_brand_active:I = 0x7f0601ef

.field public static final cs_color_brand_disabled:I = 0x7f0601f0

.field public static final cs_color_cbcbcb:I = 0x7f0601f1

.field public static final cs_color_danger:I = 0x7f0601f2

.field public static final cs_color_danger_active:I = 0x7f0601f3

.field public static final cs_color_disabled_bg:I = 0x7f0601f4

.field public static final cs_color_disabled_text:I = 0x7f0601f5

.field public static final cs_color_f2f2f2:I = 0x7f0601f6

.field public static final cs_color_ff6a2a:I = 0x7f0601f7

.field public static final cs_color_fff8ca:I = 0x7f0601f8

.field public static final cs_color_fill_0:I = 0x7f0601f9

.field public static final cs_color_fill_1:I = 0x7f0601fa

.field public static final cs_color_gray_bg:I = 0x7f0601fb

.field public static final cs_color_king_kong_cover:I = 0x7f0601fc

.field public static final cs_color_secondary:I = 0x7f0601fd

.field public static final cs_color_secondary_active:I = 0x7f0601fe

.field public static final cs_color_secondary_disabled:I = 0x7f0601ff

.field public static final cs_color_success:I = 0x7f060200

.field public static final cs_color_success_active:I = 0x7f060201

.field public static final cs_color_tertiary:I = 0x7f060202

.field public static final cs_color_tertiary_active:I = 0x7f060203

.field public static final cs_color_text_0:I = 0x7f060204

.field public static final cs_color_text_1:I = 0x7f060205

.field public static final cs_color_text_2:I = 0x7f060206

.field public static final cs_color_text_3:I = 0x7f060207

.field public static final cs_color_text_4:I = 0x7f060208

.field public static final cs_color_text_5:I = 0x7f060209

.field public static final cs_color_time_splite:I = 0x7f06020a

.field public static final cs_color_warning:I = 0x7f06020b

.field public static final cs_color_warning_active:I = 0x7f06020c

.field public static final cs_color_white_40:I = 0x7f06020d

.field public static final cs_color_white_70:I = 0x7f06020e

.field public static final cs_dark_F7F7F7:I = 0x7f06020f

.field public static final cs_divider_E0E0E0:I = 0x7f060210

.field public static final cs_gold_4CFFD173:I = 0x7f060211

.field public static final cs_gold_E8BC72:I = 0x7f060212

.field public static final cs_gold_F9F4D6:I = 0x7f060213

.field public static final cs_gold_FFFFD173:I = 0x7f060214

.field public static final cs_green_005230:I = 0x7f060215

.field public static final cs_green_006F4B:I = 0x7f060216

.field public static final cs_green_007F59:I = 0x7f060217

.field public static final cs_green_008F68:I = 0x7f060218

.field public static final cs_green_009C75:I = 0x7f060219

.field public static final cs_green_00AD87:I = 0x7f06021a

.field public static final cs_green_00B15F:I = 0x7f06021b

.field public static final cs_green_07494F:I = 0x7f06021c

.field public static final cs_green_19BC9C:I = 0x7f06021d

.field public static final cs_green_38F4CF:I = 0x7f06021e

.field public static final cs_green_3ECBB7:I = 0x7f06021f

.field public static final cs_green_6FD0B8:I = 0x7f060220

.field public static final cs_green_AAE2D4:I = 0x7f060221

.field public static final cs_green_DCF3EE:I = 0x7f060222

.field public static final cs_grey_00DCDCDC:I = 0x7f060223

.field public static final cs_grey_212121:I = 0x7f060224

.field public static final cs_grey_2C2C2C:I = 0x7f060225

.field public static final cs_grey_5A5A5A:I = 0x7f060226

.field public static final cs_grey_5F5F5F:I = 0x7f060227

.field public static final cs_grey_606060:I = 0x7f060228

.field public static final cs_grey_666666:I = 0x7f060229

.field public static final cs_grey_69737B:I = 0x7f06022a

.field public static final cs_grey_7b7b7b:I = 0x7f06022b

.field public static final cs_grey_80485769:I = 0x7f06022c

.field public static final cs_grey_80999999:I = 0x7f06022d

.field public static final cs_grey_92A1B0:I = 0x7f06022e

.field public static final cs_grey_999999:I = 0x7f06022f

.field public static final cs_grey_9C9C9C:I = 0x7f060230

.field public static final cs_grey_A4A4A4:I = 0x7f060231

.field public static final cs_grey_CCFFFFFF:I = 0x7f060232

.field public static final cs_grey_D8D8D8:I = 0x7f060233

.field public static final cs_grey_DCDCDC:I = 0x7f060234

.field public static final cs_grey_E5E5E5:I = 0x7f060235

.field public static final cs_grey_ECF0F1:I = 0x7f060236

.field public static final cs_grey_EDF3F3:I = 0x7f060237

.field public static final cs_grey_F1F1F1:I = 0x7f060238

.field public static final cs_grey_FAFAFA:I = 0x7f060239

.field public static final cs_grey_FF6A6A6A:I = 0x7f06023a

.field public static final cs_grey_FFE4E4E4:I = 0x7f06023b

.field public static final cs_grey_cccccc:I = 0x7f06023c

.field public static final cs_ope_F6C87F:I = 0x7f06023d

.field public static final cs_ope_FFBC4F:I = 0x7f06023e

.field public static final cs_ope_FFDBA1:I = 0x7f06023f

.field public static final cs_ope_black_95:I = 0x7f060240

.field public static final cs_ope_color_000000:I = 0x7f060241

.field public static final cs_ope_color_000000_30:I = 0x7f060242

.field public static final cs_ope_color_000000_40:I = 0x7f060243

.field public static final cs_ope_color_000000_60:I = 0x7f060244

.field public static final cs_ope_color_000000_cc:I = 0x7f060245

.field public static final cs_ope_color_012D4D:I = 0x7f060246

.field public static final cs_ope_color_07494F:I = 0x7f060247

.field public static final cs_ope_color_096EEE:I = 0x7f060248

.field public static final cs_ope_color_0A6EEE:I = 0x7f060249

.field public static final cs_ope_color_15130F:I = 0x7f06024a

.field public static final cs_ope_color_16223D:I = 0x7f06024b

.field public static final cs_ope_color_191001:I = 0x7f06024c

.field public static final cs_ope_color_1919BCAA:I = 0x7f06024d

.field public static final cs_ope_color_19BCAA:I = 0x7f06024e

.field public static final cs_ope_color_19BCAA_50:I = 0x7f06024f

.field public static final cs_ope_color_202020:I = 0x7f060250

.field public static final cs_ope_color_212121:I = 0x7f060251

.field public static final cs_ope_color_2E2E2E:I = 0x7f060252

.field public static final cs_ope_color_34121A:I = 0x7f060253

.field public static final cs_ope_color_3A3A3C:I = 0x7f060254

.field public static final cs_ope_color_414141:I = 0x7f060255

.field public static final cs_ope_color_473815:I = 0x7f060256

.field public static final cs_ope_color_48484A:I = 0x7f060257

.field public static final cs_ope_color_4D2C05:I = 0x7f060258

.field public static final cs_ope_color_4a3006:I = 0x7f060259

.field public static final cs_ope_color_582500:I = 0x7f06025a

.field public static final cs_ope_color_5A5A5A:I = 0x7f06025b

.field public static final cs_ope_color_5C3C06:I = 0x7f06025c

.field public static final cs_ope_color_5E1900:I = 0x7f06025d

.field public static final cs_ope_color_5E6A72:I = 0x7f06025e

.field public static final cs_ope_color_643000:I = 0x7f06025f

.field public static final cs_ope_color_6616223D:I = 0x7f060260

.field public static final cs_ope_color_685029:I = 0x7f060261

.field public static final cs_ope_color_763B01:I = 0x7f060262

.field public static final cs_ope_color_7B7B7B:I = 0x7f060263

.field public static final cs_ope_color_7E3F00:I = 0x7f060264

.field public static final cs_ope_color_815315:I = 0x7f060265

.field public static final cs_ope_color_842612:I = 0x7f060266

.field public static final cs_ope_color_86320B:I = 0x7f060267

.field public static final cs_ope_color_9C9C9C:I = 0x7f060268

.field public static final cs_ope_color_A1640F:I = 0x7f060269

.field public static final cs_ope_color_A34A0F:I = 0x7f06026a

.field public static final cs_ope_color_AF6363:I = 0x7f06026b

.field public static final cs_ope_color_B4431A:I = 0x7f06026c

.field public static final cs_ope_color_B4431A_60:I = 0x7f06026d

.field public static final cs_ope_color_B76927:I = 0x7f06026e

.field public static final cs_ope_color_BDF1F8:I = 0x7f06026f

.field public static final cs_ope_color_C14520:I = 0x7f060270

.field public static final cs_ope_color_C2DAFF:I = 0x7f060271

.field public static final cs_ope_color_C6690D:I = 0x7f060272

.field public static final cs_ope_color_C68A1B:I = 0x7f060273

.field public static final cs_ope_color_C7FFFC_00:I = 0x7f060274

.field public static final cs_ope_color_CCCCCC:I = 0x7f060275

.field public static final cs_ope_color_D27812:I = 0x7f060276

.field public static final cs_ope_color_D5700D:I = 0x7f060277

.field public static final cs_ope_color_D79F4B:I = 0x7f060278

.field public static final cs_ope_color_D8D8D8:I = 0x7f060279

.field public static final cs_ope_color_DA9C3D:I = 0x7f06027a

.field public static final cs_ope_color_DCA042:I = 0x7f06027b

.field public static final cs_ope_color_DCDCDC:I = 0x7f06027c

.field public static final cs_ope_color_DCFFFF:I = 0x7f06027d

.field public static final cs_ope_color_DDAA5A:I = 0x7f06027e

.field public static final cs_ope_color_DE51EC:I = 0x7f06027f

.field public static final cs_ope_color_E1A954:I = 0x7f060280

.field public static final cs_ope_color_E5AA56:I = 0x7f060281

.field public static final cs_ope_color_E5EFFF:I = 0x7f060282

.field public static final cs_ope_color_E98715:I = 0x7f060283

.field public static final cs_ope_color_E9FBFB:I = 0x7f060284

.field public static final cs_ope_color_EBD0AD:I = 0x7f060285

.field public static final cs_ope_color_ECCB8C:I = 0x7f060286

.field public static final cs_ope_color_EF3C33:I = 0x7f060287

.field public static final cs_ope_color_EFC788:I = 0x7f060288

.field public static final cs_ope_color_F0DAB7:I = 0x7f060289

.field public static final cs_ope_color_F1F1F1:I = 0x7f06028a

.field public static final cs_ope_color_F34136:I = 0x7f06028b

.field public static final cs_ope_color_F4463A:I = 0x7f06028c

.field public static final cs_ope_color_F4F4F4:I = 0x7f06028d

.field public static final cs_ope_color_F55247:I = 0x7f06028e

.field public static final cs_ope_color_F6982E:I = 0x7f06028f

.field public static final cs_ope_color_F76B39:I = 0x7f060290

.field public static final cs_ope_color_F86833:I = 0x7f060291

.field public static final cs_ope_color_F8ECD9:I = 0x7f060292

.field public static final cs_ope_color_F9FFFD:I = 0x7f060293

.field public static final cs_ope_color_F9FFFF:I = 0x7f060294

.field public static final cs_ope_color_FADACE:I = 0x7f060295

.field public static final cs_ope_color_FAE9CC:I = 0x7f060296

.field public static final cs_ope_color_FB9F38:I = 0x7f060297

.field public static final cs_ope_color_FBA749:I = 0x7f060298

.field public static final cs_ope_color_FBE2CF:I = 0x7f060299

.field public static final cs_ope_color_FBE3C0:I = 0x7f06029a

.field public static final cs_ope_color_FC631B:I = 0x7f06029b

.field public static final cs_ope_color_FCE7CC:I = 0x7f06029c

.field public static final cs_ope_color_FCEDD8:I = 0x7f06029d

.field public static final cs_ope_color_FD6828:I = 0x7f06029e

.field public static final cs_ope_color_FDE7CB:I = 0x7f06029f

.field public static final cs_ope_color_FE6E04:I = 0x7f0602a0

.field public static final cs_ope_color_FEA459:I = 0x7f0602a1

.field public static final cs_ope_color_FEE8CD:I = 0x7f0602a2

.field public static final cs_ope_color_FEF7EC:I = 0x7f0602a3

.field public static final cs_ope_color_FEF8EF:I = 0x7f0602a4

.field public static final cs_ope_color_FF4337:I = 0x7f0602a5

.field public static final cs_ope_color_FF4534:I = 0x7f0602a6

.field public static final cs_ope_color_FF4C00:I = 0x7f0602a7

.field public static final cs_ope_color_FF5122:I = 0x7f0602a8

.field public static final cs_ope_color_FF5223:I = 0x7f0602a9

.field public static final cs_ope_color_FF5F05:I = 0x7f0602aa

.field public static final cs_ope_color_FF6549:I = 0x7f0602ab

.field public static final cs_ope_color_FF655B:I = 0x7f0602ac

.field public static final cs_ope_color_FF6748:I = 0x7f0602ad

.field public static final cs_ope_color_FF7043:I = 0x7f0602ae

.field public static final cs_ope_color_FF7557:I = 0x7f0602af

.field public static final cs_ope_color_FF814A:I = 0x7f0602b0

.field public static final cs_ope_color_FF8227:I = 0x7f0602b1

.field public static final cs_ope_color_FF8515:I = 0x7f0602b2

.field public static final cs_ope_color_FF8761:I = 0x7f0602b3

.field public static final cs_ope_color_FF8934:I = 0x7f0602b4

.field public static final cs_ope_color_FF8C23:I = 0x7f0602b5

.field public static final cs_ope_color_FF8C3E:I = 0x7f0602b6

.field public static final cs_ope_color_FF912C:I = 0x7f0602b7

.field public static final cs_ope_color_FF9B3E:I = 0x7f0602b8

.field public static final cs_ope_color_FFA857:I = 0x7f0602b9

.field public static final cs_ope_color_FFB039:I = 0x7f0602ba

.field public static final cs_ope_color_FFB18F:I = 0x7f0602bb

.field public static final cs_ope_color_FFB546:I = 0x7f0602bc

.field public static final cs_ope_color_FFB800:I = 0x7f0602bd

.field public static final cs_ope_color_FFBA6D:I = 0x7f0602be

.field public static final cs_ope_color_FFBE38:I = 0x7f0602bf

.field public static final cs_ope_color_FFBE5B:I = 0x7f0602c0

.field public static final cs_ope_color_FFC07A:I = 0x7f0602c1

.field public static final cs_ope_color_FFC094:I = 0x7f0602c2

.field public static final cs_ope_color_FFC8B1:I = 0x7f0602c3

.field public static final cs_ope_color_FFD19A:I = 0x7f0602c4

.field public static final cs_ope_color_FFD1A0:I = 0x7f0602c5

.field public static final cs_ope_color_FFDCB1:I = 0x7f0602c6

.field public static final cs_ope_color_FFDEA1:I = 0x7f0602c7

.field public static final cs_ope_color_FFE5A4:I = 0x7f0602c8

.field public static final cs_ope_color_FFE5BC:I = 0x7f0602c9

.field public static final cs_ope_color_FFE6A7:I = 0x7f0602ca

.field public static final cs_ope_color_FFE8AC:I = 0x7f0602cb

.field public static final cs_ope_color_FFE9E4_90:I = 0x7f0602cc

.field public static final cs_ope_color_FFECBA:I = 0x7f0602cd

.field public static final cs_ope_color_FFECD6:I = 0x7f0602ce

.field public static final cs_ope_color_FFEECC:I = 0x7f0602cf

.field public static final cs_ope_color_FFEEDB:I = 0x7f0602d0

.field public static final cs_ope_color_FFEFC2:I = 0x7f0602d1

.field public static final cs_ope_color_FFF1BF:I = 0x7f0602d2

.field public static final cs_ope_color_FFF1E0:I = 0x7f0602d3

.field public static final cs_ope_color_FFF494:I = 0x7f0602d4

.field public static final cs_ope_color_FFF4DE:I = 0x7f0602d5

.field public static final cs_ope_color_FFF4E8:I = 0x7f0602d6

.field public static final cs_ope_color_FFF4F2:I = 0x7f0602d7

.field public static final cs_ope_color_FFF5E3:I = 0x7f0602d8

.field public static final cs_ope_color_FFF5F1:I = 0x7f0602d9

.field public static final cs_ope_color_FFF7EB:I = 0x7f0602da

.field public static final cs_ope_color_FFFAF1:I = 0x7f0602db

.field public static final cs_ope_color_FFFAF4:I = 0x7f0602dc

.field public static final cs_ope_color_FFFCEF:I = 0x7f0602dd

.field public static final cs_ope_color_FFFDF7:I = 0x7f0602de

.field public static final cs_ope_color_FFFDF8:I = 0x7f0602df

.field public static final cs_ope_color_FFFFFF:I = 0x7f0602e0

.field public static final cs_ope_color_FFFFFF_10:I = 0x7f0602e1

.field public static final cs_ope_color_FFFFFF_30:I = 0x7f0602e2

.field public static final cs_ope_color_FFFFFF_60:I = 0x7f0602e3

.field public static final cs_ope_color_brand_alpha_6:I = 0x7f0602e4

.field public static final cs_ope_color_danger_40:I = 0x7f0602e5

.field public static final cs_ope_color_transparent:I = 0x7f0602e6

.field public static final cs_orange_EC6C57:I = 0x7f0602e7

.field public static final cs_orange_FF6634:I = 0x7f0602e8

.field public static final cs_orange_FF6A48:I = 0x7f0602e9

.field public static final cs_orange_FF7046:I = 0x7f0602ea

.field public static final cs_orange_FF9312:I = 0x7f0602eb

.field public static final cs_orange_FFF6F1:I = 0x7f0602ec

.field public static final cs_orange_FFFA8902:I = 0x7f0602ed

.field public static final cs_red_E53804:I = 0x7f0602ee

.field public static final cs_red_F38080:I = 0x7f0602ef

.field public static final cs_red_FF1518:I = 0x7f0602f0

.field public static final cs_red_FF3D30:I = 0x7f0602f1

.field public static final cs_red_FF6161:I = 0x7f0602f2

.field public static final cs_silver_4CDFE4E8:I = 0x7f0602f3

.field public static final cs_silver_FFDFE4E8:I = 0x7f0602f4

.field public static final cs_transparent:I = 0x7f0602f5

.field public static final cs_white_80FFFFFF:I = 0x7f0602f6

.field public static final cs_white_F2FFFFFF:I = 0x7f0602f7

.field public static final cs_white_F5F5F5:I = 0x7f0602f8

.field public static final cs_white_F8F8F8:I = 0x7f0602f9

.field public static final cs_white_FFFFFF:I = 0x7f0602fa

.field public static final cs_yellow_FFE6C5:I = 0x7f0602fb

.field public static final cs_yellow_FFF8CA:I = 0x7f0602fc

.field public static final deep_clean_line_color:I = 0x7f0602fd

.field public static final default_shadow_color:I = 0x7f060300

.field public static final default_splash_color:I = 0x7f060301

.field public static final default_text_color:I = 0x7f060302

.field public static final default_text_color_dark:I = 0x7f060303

.field public static final default_text_color_hint:I = 0x7f060304

.field public static final default_text_color_tip:I = 0x7f060305

.field public static final default_text_color_warning:I = 0x7f060306

.field public static final default_textview_color:I = 0x7f060307

.field public static final default_textview_hint_color:I = 0x7f060308

.field public static final design_bottom_navigation_shadow_color:I = 0x7f060309

.field public static final design_box_stroke_color:I = 0x7f06030a

.field public static final design_dark_default_color_background:I = 0x7f06030b

.field public static final design_dark_default_color_error:I = 0x7f06030c

.field public static final design_dark_default_color_on_background:I = 0x7f06030d

.field public static final design_dark_default_color_on_error:I = 0x7f06030e

.field public static final design_dark_default_color_on_primary:I = 0x7f06030f

.field public static final design_dark_default_color_on_secondary:I = 0x7f060310

.field public static final design_dark_default_color_on_surface:I = 0x7f060311

.field public static final design_dark_default_color_primary:I = 0x7f060312

.field public static final design_dark_default_color_primary_dark:I = 0x7f060313

.field public static final design_dark_default_color_primary_variant:I = 0x7f060314

.field public static final design_dark_default_color_secondary:I = 0x7f060315

.field public static final design_dark_default_color_secondary_variant:I = 0x7f060316

.field public static final design_dark_default_color_surface:I = 0x7f060317

.field public static final design_default_color_background:I = 0x7f060318

.field public static final design_default_color_error:I = 0x7f060319

.field public static final design_default_color_on_background:I = 0x7f06031a

.field public static final design_default_color_on_error:I = 0x7f06031b

.field public static final design_default_color_on_primary:I = 0x7f06031c

.field public static final design_default_color_on_secondary:I = 0x7f06031d

.field public static final design_default_color_on_surface:I = 0x7f06031e

.field public static final design_default_color_primary:I = 0x7f06031f

.field public static final design_default_color_primary_dark:I = 0x7f060320

.field public static final design_default_color_primary_variant:I = 0x7f060321

.field public static final design_default_color_secondary:I = 0x7f060322

.field public static final design_default_color_secondary_variant:I = 0x7f060323

.field public static final design_default_color_surface:I = 0x7f060324

.field public static final design_error:I = 0x7f060325

.field public static final design_fab_shadow_end_color:I = 0x7f060326

.field public static final design_fab_shadow_mid_color:I = 0x7f060327

.field public static final design_fab_shadow_start_color:I = 0x7f060328

.field public static final design_fab_stroke_end_inner_color:I = 0x7f060329

.field public static final design_fab_stroke_end_outer_color:I = 0x7f06032a

.field public static final design_fab_stroke_top_inner_color:I = 0x7f06032b

.field public static final design_fab_stroke_top_outer_color:I = 0x7f06032c

.field public static final design_icon_tint:I = 0x7f06032d

.field public static final design_snackbar_background_color:I = 0x7f06032e

.field public static final dim_foreground_disabled_material_dark:I = 0x7f060338

.field public static final dim_foreground_disabled_material_light:I = 0x7f060339

.field public static final dim_foreground_material_dark:I = 0x7f06033a

.field public static final dim_foreground_material_light:I = 0x7f06033b

.field public static final disable_text_color:I = 0x7f06033c

.field public static final divider_menu_blak_style:I = 0x7f06033d

.field public static final dlg_button_textcolor:I = 0x7f06033e

.field public static final dlg_title_textcolor:I = 0x7f06033f

.field public static final doc_list_item_time:I = 0x7f060340

.field public static final doc_list_item_title:I = 0x7f060341

.field public static final doc_page_item_detail_bg:I = 0x7f060342

.field public static final doc_page_item_note_bg:I = 0x7f060343

.field public static final dragndrop_background:I = 0x7f060344

.field public static final drawer_end_grid_background:I = 0x7f060345

.field public static final drawer_menu_item_normal:I = 0x7f060346

.field public static final drawer_menu_item_pressed:I = 0x7f060347

.field public static final drawer_menu_item_selected:I = 0x7f060348

.field public static final drawer_menu_list_diver:I = 0x7f060349

.field public static final empty_doc_color:I = 0x7f06034a

.field public static final error_color_material_dark:I = 0x7f06034b

.field public static final error_color_material_light:I = 0x7f06034c

.field public static final fax_default_text_color:I = 0x7f06034d

.field public static final fax_default_text_color_hint:I = 0x7f06034e

.field public static final fax_label_text_color:I = 0x7f06034f

.field public static final foreground_material_dark:I = 0x7f060350

.field public static final foreground_material_light:I = 0x7f060351

.field public static final gray_464646:I = 0x7f060352

.field public static final gray_a0a0a0:I = 0x7f060353

.field public static final gray_background:I = 0x7f060354

.field public static final gray_e7e7e7:I = 0x7f060355

.field public static final gray_e8e8e8:I = 0x7f060356

.field public static final green_ok:I = 0x7f060357

.field public static final guid_btn_use_direct_press_color:I = 0x7f060358

.field public static final guide_activity_status_bar:I = 0x7f060359

.field public static final guide_btn_text_color:I = 0x7f06035a

.field public static final highlighted_text_material_dark:I = 0x7f06035b

.field public static final highlighted_text_material_light:I = 0x7f06035c

.field public static final hint_tag_item_selected:I = 0x7f06035d

.field public static final hinttextcolor:I = 0x7f06035e

.field public static final holo_btn_disable_text_color:I = 0x7f06035f

.field public static final holo_btn_enable_text_color:I = 0x7f060360

.field public static final holo_btn_text_color:I = 0x7f060361

.field public static final holo_common_long_pressed:I = 0x7f060362

.field public static final holo_common_normal:I = 0x7f060363

.field public static final holo_common_pressed:I = 0x7f060364

.field public static final image_scan_adjust_panel_bg_color:I = 0x7f060366

.field public static final image_view_dot_color:I = 0x7f060367

.field public static final img_text_compare_selected:I = 0x7f060368

.field public static final layout_background_color:I = 0x7f060375

.field public static final light_gray:I = 0x7f060376

.field public static final list_menu_item_pressed:I = 0x7f060377

.field public static final list_preference_selector_normal:I = 0x7f060378

.field public static final list_selector_normal:I = 0x7f060379

.field public static final list_selector_pressed:I = 0x7f06037a

.field public static final logagent_bg_white:I = 0x7f06037b

.field public static final logagent_btn_bg_red_color:I = 0x7f06037c

.field public static final logagent_cad_black:I = 0x7f06037d

.field public static final logagent_color_reward_yellow:I = 0x7f06037e

.field public static final logagent_cs_blue_008FED:I = 0x7f06037f

.field public static final logagent_green_ok:I = 0x7f060380

.field public static final low_storage_advise_textcolor:I = 0x7f060381

.field public static final m3_appbar_overlay_color:I = 0x7f060382

.field public static final m3_assist_chip_icon_tint_color:I = 0x7f060383

.field public static final m3_assist_chip_stroke_color:I = 0x7f060384

.field public static final m3_button_background_color_selector:I = 0x7f060385

.field public static final m3_button_foreground_color_selector:I = 0x7f060386

.field public static final m3_button_outline_color_selector:I = 0x7f060387

.field public static final m3_button_ripple_color:I = 0x7f060388

.field public static final m3_button_ripple_color_selector:I = 0x7f060389

.field public static final m3_calendar_item_disabled_text:I = 0x7f06038a

.field public static final m3_calendar_item_stroke_color:I = 0x7f06038b

.field public static final m3_card_foreground_color:I = 0x7f06038c

.field public static final m3_card_ripple_color:I = 0x7f06038d

.field public static final m3_card_stroke_color:I = 0x7f06038e

.field public static final m3_checkbox_button_icon_tint:I = 0x7f06038f

.field public static final m3_checkbox_button_tint:I = 0x7f060390

.field public static final m3_chip_assist_text_color:I = 0x7f060391

.field public static final m3_chip_background_color:I = 0x7f060392

.field public static final m3_chip_ripple_color:I = 0x7f060393

.field public static final m3_chip_stroke_color:I = 0x7f060394

.field public static final m3_chip_text_color:I = 0x7f060395

.field public static final m3_dark_default_color_primary_text:I = 0x7f060396

.field public static final m3_dark_default_color_secondary_text:I = 0x7f060397

.field public static final m3_dark_highlighted_text:I = 0x7f060398

.field public static final m3_dark_hint_foreground:I = 0x7f060399

.field public static final m3_dark_primary_text_disable_only:I = 0x7f06039a

.field public static final m3_default_color_primary_text:I = 0x7f06039b

.field public static final m3_default_color_secondary_text:I = 0x7f06039c

.field public static final m3_dynamic_dark_default_color_primary_text:I = 0x7f06039d

.field public static final m3_dynamic_dark_default_color_secondary_text:I = 0x7f06039e

.field public static final m3_dynamic_dark_highlighted_text:I = 0x7f06039f

.field public static final m3_dynamic_dark_hint_foreground:I = 0x7f0603a0

.field public static final m3_dynamic_dark_primary_text_disable_only:I = 0x7f0603a1

.field public static final m3_dynamic_default_color_primary_text:I = 0x7f0603a2

.field public static final m3_dynamic_default_color_secondary_text:I = 0x7f0603a3

.field public static final m3_dynamic_highlighted_text:I = 0x7f0603a4

.field public static final m3_dynamic_hint_foreground:I = 0x7f0603a5

.field public static final m3_dynamic_primary_text_disable_only:I = 0x7f0603a6

.field public static final m3_efab_ripple_color_selector:I = 0x7f0603a7

.field public static final m3_elevated_chip_background_color:I = 0x7f0603a8

.field public static final m3_fab_efab_background_color_selector:I = 0x7f0603a9

.field public static final m3_fab_efab_foreground_color_selector:I = 0x7f0603aa

.field public static final m3_fab_ripple_color_selector:I = 0x7f0603ab

.field public static final m3_filled_icon_button_container_color_selector:I = 0x7f0603ac

.field public static final m3_highlighted_text:I = 0x7f0603ad

.field public static final m3_hint_foreground:I = 0x7f0603ae

.field public static final m3_icon_button_icon_color_selector:I = 0x7f0603af

.field public static final m3_navigation_bar_item_with_indicator_icon_tint:I = 0x7f0603b0

.field public static final m3_navigation_bar_item_with_indicator_label_tint:I = 0x7f0603b1

.field public static final m3_navigation_bar_ripple_color_selector:I = 0x7f0603b2

.field public static final m3_navigation_item_background_color:I = 0x7f0603b3

.field public static final m3_navigation_item_icon_tint:I = 0x7f0603b4

.field public static final m3_navigation_item_ripple_color:I = 0x7f0603b5

.field public static final m3_navigation_item_text_color:I = 0x7f0603b6

.field public static final m3_navigation_rail_item_with_indicator_icon_tint:I = 0x7f0603b7

.field public static final m3_navigation_rail_item_with_indicator_label_tint:I = 0x7f0603b8

.field public static final m3_navigation_rail_ripple_color_selector:I = 0x7f0603b9

.field public static final m3_popupmenu_overlay_color:I = 0x7f0603ba

.field public static final m3_primary_text_disable_only:I = 0x7f0603bb

.field public static final m3_radiobutton_button_tint:I = 0x7f0603bc

.field public static final m3_radiobutton_ripple_tint:I = 0x7f0603bd

.field public static final m3_ref_palette_black:I = 0x7f0603be

.field public static final m3_ref_palette_dynamic_neutral0:I = 0x7f0603bf

.field public static final m3_ref_palette_dynamic_neutral10:I = 0x7f0603c0

.field public static final m3_ref_palette_dynamic_neutral100:I = 0x7f0603c1

.field public static final m3_ref_palette_dynamic_neutral12:I = 0x7f0603c2

.field public static final m3_ref_palette_dynamic_neutral17:I = 0x7f0603c3

.field public static final m3_ref_palette_dynamic_neutral20:I = 0x7f0603c4

.field public static final m3_ref_palette_dynamic_neutral22:I = 0x7f0603c5

.field public static final m3_ref_palette_dynamic_neutral24:I = 0x7f0603c6

.field public static final m3_ref_palette_dynamic_neutral30:I = 0x7f0603c7

.field public static final m3_ref_palette_dynamic_neutral4:I = 0x7f0603c8

.field public static final m3_ref_palette_dynamic_neutral40:I = 0x7f0603c9

.field public static final m3_ref_palette_dynamic_neutral50:I = 0x7f0603ca

.field public static final m3_ref_palette_dynamic_neutral6:I = 0x7f0603cb

.field public static final m3_ref_palette_dynamic_neutral60:I = 0x7f0603cc

.field public static final m3_ref_palette_dynamic_neutral70:I = 0x7f0603cd

.field public static final m3_ref_palette_dynamic_neutral80:I = 0x7f0603ce

.field public static final m3_ref_palette_dynamic_neutral87:I = 0x7f0603cf

.field public static final m3_ref_palette_dynamic_neutral90:I = 0x7f0603d0

.field public static final m3_ref_palette_dynamic_neutral92:I = 0x7f0603d1

.field public static final m3_ref_palette_dynamic_neutral94:I = 0x7f0603d2

.field public static final m3_ref_palette_dynamic_neutral95:I = 0x7f0603d3

.field public static final m3_ref_palette_dynamic_neutral96:I = 0x7f0603d4

.field public static final m3_ref_palette_dynamic_neutral98:I = 0x7f0603d5

.field public static final m3_ref_palette_dynamic_neutral99:I = 0x7f0603d6

.field public static final m3_ref_palette_dynamic_neutral_variant0:I = 0x7f0603d7

.field public static final m3_ref_palette_dynamic_neutral_variant10:I = 0x7f0603d8

.field public static final m3_ref_palette_dynamic_neutral_variant100:I = 0x7f0603d9

.field public static final m3_ref_palette_dynamic_neutral_variant20:I = 0x7f0603da

.field public static final m3_ref_palette_dynamic_neutral_variant30:I = 0x7f0603db

.field public static final m3_ref_palette_dynamic_neutral_variant40:I = 0x7f0603dc

.field public static final m3_ref_palette_dynamic_neutral_variant50:I = 0x7f0603dd

.field public static final m3_ref_palette_dynamic_neutral_variant60:I = 0x7f0603de

.field public static final m3_ref_palette_dynamic_neutral_variant70:I = 0x7f0603df

.field public static final m3_ref_palette_dynamic_neutral_variant80:I = 0x7f0603e0

.field public static final m3_ref_palette_dynamic_neutral_variant90:I = 0x7f0603e1

.field public static final m3_ref_palette_dynamic_neutral_variant95:I = 0x7f0603e2

.field public static final m3_ref_palette_dynamic_neutral_variant99:I = 0x7f0603e3

.field public static final m3_ref_palette_dynamic_primary0:I = 0x7f0603e4

.field public static final m3_ref_palette_dynamic_primary10:I = 0x7f0603e5

.field public static final m3_ref_palette_dynamic_primary100:I = 0x7f0603e6

.field public static final m3_ref_palette_dynamic_primary20:I = 0x7f0603e7

.field public static final m3_ref_palette_dynamic_primary30:I = 0x7f0603e8

.field public static final m3_ref_palette_dynamic_primary40:I = 0x7f0603e9

.field public static final m3_ref_palette_dynamic_primary50:I = 0x7f0603ea

.field public static final m3_ref_palette_dynamic_primary60:I = 0x7f0603eb

.field public static final m3_ref_palette_dynamic_primary70:I = 0x7f0603ec

.field public static final m3_ref_palette_dynamic_primary80:I = 0x7f0603ed

.field public static final m3_ref_palette_dynamic_primary90:I = 0x7f0603ee

.field public static final m3_ref_palette_dynamic_primary95:I = 0x7f0603ef

.field public static final m3_ref_palette_dynamic_primary99:I = 0x7f0603f0

.field public static final m3_ref_palette_dynamic_secondary0:I = 0x7f0603f1

.field public static final m3_ref_palette_dynamic_secondary10:I = 0x7f0603f2

.field public static final m3_ref_palette_dynamic_secondary100:I = 0x7f0603f3

.field public static final m3_ref_palette_dynamic_secondary20:I = 0x7f0603f4

.field public static final m3_ref_palette_dynamic_secondary30:I = 0x7f0603f5

.field public static final m3_ref_palette_dynamic_secondary40:I = 0x7f0603f6

.field public static final m3_ref_palette_dynamic_secondary50:I = 0x7f0603f7

.field public static final m3_ref_palette_dynamic_secondary60:I = 0x7f0603f8

.field public static final m3_ref_palette_dynamic_secondary70:I = 0x7f0603f9

.field public static final m3_ref_palette_dynamic_secondary80:I = 0x7f0603fa

.field public static final m3_ref_palette_dynamic_secondary90:I = 0x7f0603fb

.field public static final m3_ref_palette_dynamic_secondary95:I = 0x7f0603fc

.field public static final m3_ref_palette_dynamic_secondary99:I = 0x7f0603fd

.field public static final m3_ref_palette_dynamic_tertiary0:I = 0x7f0603fe

.field public static final m3_ref_palette_dynamic_tertiary10:I = 0x7f0603ff

.field public static final m3_ref_palette_dynamic_tertiary100:I = 0x7f060400

.field public static final m3_ref_palette_dynamic_tertiary20:I = 0x7f060401

.field public static final m3_ref_palette_dynamic_tertiary30:I = 0x7f060402

.field public static final m3_ref_palette_dynamic_tertiary40:I = 0x7f060403

.field public static final m3_ref_palette_dynamic_tertiary50:I = 0x7f060404

.field public static final m3_ref_palette_dynamic_tertiary60:I = 0x7f060405

.field public static final m3_ref_palette_dynamic_tertiary70:I = 0x7f060406

.field public static final m3_ref_palette_dynamic_tertiary80:I = 0x7f060407

.field public static final m3_ref_palette_dynamic_tertiary90:I = 0x7f060408

.field public static final m3_ref_palette_dynamic_tertiary95:I = 0x7f060409

.field public static final m3_ref_palette_dynamic_tertiary99:I = 0x7f06040a

.field public static final m3_ref_palette_error0:I = 0x7f06040b

.field public static final m3_ref_palette_error10:I = 0x7f06040c

.field public static final m3_ref_palette_error100:I = 0x7f06040d

.field public static final m3_ref_palette_error20:I = 0x7f06040e

.field public static final m3_ref_palette_error30:I = 0x7f06040f

.field public static final m3_ref_palette_error40:I = 0x7f060410

.field public static final m3_ref_palette_error50:I = 0x7f060411

.field public static final m3_ref_palette_error60:I = 0x7f060412

.field public static final m3_ref_palette_error70:I = 0x7f060413

.field public static final m3_ref_palette_error80:I = 0x7f060414

.field public static final m3_ref_palette_error90:I = 0x7f060415

.field public static final m3_ref_palette_error95:I = 0x7f060416

.field public static final m3_ref_palette_error99:I = 0x7f060417

.field public static final m3_ref_palette_neutral0:I = 0x7f060418

.field public static final m3_ref_palette_neutral10:I = 0x7f060419

.field public static final m3_ref_palette_neutral100:I = 0x7f06041a

.field public static final m3_ref_palette_neutral12:I = 0x7f06041b

.field public static final m3_ref_palette_neutral17:I = 0x7f06041c

.field public static final m3_ref_palette_neutral20:I = 0x7f06041d

.field public static final m3_ref_palette_neutral22:I = 0x7f06041e

.field public static final m3_ref_palette_neutral24:I = 0x7f06041f

.field public static final m3_ref_palette_neutral30:I = 0x7f060420

.field public static final m3_ref_palette_neutral4:I = 0x7f060421

.field public static final m3_ref_palette_neutral40:I = 0x7f060422

.field public static final m3_ref_palette_neutral50:I = 0x7f060423

.field public static final m3_ref_palette_neutral6:I = 0x7f060424

.field public static final m3_ref_palette_neutral60:I = 0x7f060425

.field public static final m3_ref_palette_neutral70:I = 0x7f060426

.field public static final m3_ref_palette_neutral80:I = 0x7f060427

.field public static final m3_ref_palette_neutral87:I = 0x7f060428

.field public static final m3_ref_palette_neutral90:I = 0x7f060429

.field public static final m3_ref_palette_neutral92:I = 0x7f06042a

.field public static final m3_ref_palette_neutral94:I = 0x7f06042b

.field public static final m3_ref_palette_neutral95:I = 0x7f06042c

.field public static final m3_ref_palette_neutral96:I = 0x7f06042d

.field public static final m3_ref_palette_neutral98:I = 0x7f06042e

.field public static final m3_ref_palette_neutral99:I = 0x7f06042f

.field public static final m3_ref_palette_neutral_variant0:I = 0x7f060430

.field public static final m3_ref_palette_neutral_variant10:I = 0x7f060431

.field public static final m3_ref_palette_neutral_variant100:I = 0x7f060432

.field public static final m3_ref_palette_neutral_variant20:I = 0x7f060433

.field public static final m3_ref_palette_neutral_variant30:I = 0x7f060434

.field public static final m3_ref_palette_neutral_variant40:I = 0x7f060435

.field public static final m3_ref_palette_neutral_variant50:I = 0x7f060436

.field public static final m3_ref_palette_neutral_variant60:I = 0x7f060437

.field public static final m3_ref_palette_neutral_variant70:I = 0x7f060438

.field public static final m3_ref_palette_neutral_variant80:I = 0x7f060439

.field public static final m3_ref_palette_neutral_variant90:I = 0x7f06043a

.field public static final m3_ref_palette_neutral_variant95:I = 0x7f06043b

.field public static final m3_ref_palette_neutral_variant99:I = 0x7f06043c

.field public static final m3_ref_palette_primary0:I = 0x7f06043d

.field public static final m3_ref_palette_primary10:I = 0x7f06043e

.field public static final m3_ref_palette_primary100:I = 0x7f06043f

.field public static final m3_ref_palette_primary20:I = 0x7f060440

.field public static final m3_ref_palette_primary30:I = 0x7f060441

.field public static final m3_ref_palette_primary40:I = 0x7f060442

.field public static final m3_ref_palette_primary50:I = 0x7f060443

.field public static final m3_ref_palette_primary60:I = 0x7f060444

.field public static final m3_ref_palette_primary70:I = 0x7f060445

.field public static final m3_ref_palette_primary80:I = 0x7f060446

.field public static final m3_ref_palette_primary90:I = 0x7f060447

.field public static final m3_ref_palette_primary95:I = 0x7f060448

.field public static final m3_ref_palette_primary99:I = 0x7f060449

.field public static final m3_ref_palette_secondary0:I = 0x7f06044a

.field public static final m3_ref_palette_secondary10:I = 0x7f06044b

.field public static final m3_ref_palette_secondary100:I = 0x7f06044c

.field public static final m3_ref_palette_secondary20:I = 0x7f06044d

.field public static final m3_ref_palette_secondary30:I = 0x7f06044e

.field public static final m3_ref_palette_secondary40:I = 0x7f06044f

.field public static final m3_ref_palette_secondary50:I = 0x7f060450

.field public static final m3_ref_palette_secondary60:I = 0x7f060451

.field public static final m3_ref_palette_secondary70:I = 0x7f060452

.field public static final m3_ref_palette_secondary80:I = 0x7f060453

.field public static final m3_ref_palette_secondary90:I = 0x7f060454

.field public static final m3_ref_palette_secondary95:I = 0x7f060455

.field public static final m3_ref_palette_secondary99:I = 0x7f060456

.field public static final m3_ref_palette_tertiary0:I = 0x7f060457

.field public static final m3_ref_palette_tertiary10:I = 0x7f060458

.field public static final m3_ref_palette_tertiary100:I = 0x7f060459

.field public static final m3_ref_palette_tertiary20:I = 0x7f06045a

.field public static final m3_ref_palette_tertiary30:I = 0x7f06045b

.field public static final m3_ref_palette_tertiary40:I = 0x7f06045c

.field public static final m3_ref_palette_tertiary50:I = 0x7f06045d

.field public static final m3_ref_palette_tertiary60:I = 0x7f06045e

.field public static final m3_ref_palette_tertiary70:I = 0x7f06045f

.field public static final m3_ref_palette_tertiary80:I = 0x7f060460

.field public static final m3_ref_palette_tertiary90:I = 0x7f060461

.field public static final m3_ref_palette_tertiary95:I = 0x7f060462

.field public static final m3_ref_palette_tertiary99:I = 0x7f060463

.field public static final m3_ref_palette_white:I = 0x7f060464

.field public static final m3_selection_control_ripple_color_selector:I = 0x7f060465

.field public static final m3_simple_item_ripple_color:I = 0x7f060466

.field public static final m3_slider_active_track_color:I = 0x7f060467

.field public static final m3_slider_halo_color:I = 0x7f060468

.field public static final m3_slider_inactive_track_color:I = 0x7f060469

.field public static final m3_slider_thumb_color:I = 0x7f06046a

.field public static final m3_switch_thumb_tint:I = 0x7f06046b

.field public static final m3_switch_track_tint:I = 0x7f06046c

.field public static final m3_sys_color_dark_background:I = 0x7f06046d

.field public static final m3_sys_color_dark_error:I = 0x7f06046e

.field public static final m3_sys_color_dark_error_container:I = 0x7f06046f

.field public static final m3_sys_color_dark_inverse_on_surface:I = 0x7f060470

.field public static final m3_sys_color_dark_inverse_primary:I = 0x7f060471

.field public static final m3_sys_color_dark_inverse_surface:I = 0x7f060472

.field public static final m3_sys_color_dark_on_background:I = 0x7f060473

.field public static final m3_sys_color_dark_on_error:I = 0x7f060474

.field public static final m3_sys_color_dark_on_error_container:I = 0x7f060475

.field public static final m3_sys_color_dark_on_primary:I = 0x7f060476

.field public static final m3_sys_color_dark_on_primary_container:I = 0x7f060477

.field public static final m3_sys_color_dark_on_secondary:I = 0x7f060478

.field public static final m3_sys_color_dark_on_secondary_container:I = 0x7f060479

.field public static final m3_sys_color_dark_on_surface:I = 0x7f06047a

.field public static final m3_sys_color_dark_on_surface_variant:I = 0x7f06047b

.field public static final m3_sys_color_dark_on_tertiary:I = 0x7f06047c

.field public static final m3_sys_color_dark_on_tertiary_container:I = 0x7f06047d

.field public static final m3_sys_color_dark_outline:I = 0x7f06047e

.field public static final m3_sys_color_dark_outline_variant:I = 0x7f06047f

.field public static final m3_sys_color_dark_primary:I = 0x7f060480

.field public static final m3_sys_color_dark_primary_container:I = 0x7f060481

.field public static final m3_sys_color_dark_secondary:I = 0x7f060482

.field public static final m3_sys_color_dark_secondary_container:I = 0x7f060483

.field public static final m3_sys_color_dark_surface:I = 0x7f060484

.field public static final m3_sys_color_dark_surface_bright:I = 0x7f060485

.field public static final m3_sys_color_dark_surface_container:I = 0x7f060486

.field public static final m3_sys_color_dark_surface_container_high:I = 0x7f060487

.field public static final m3_sys_color_dark_surface_container_highest:I = 0x7f060488

.field public static final m3_sys_color_dark_surface_container_low:I = 0x7f060489

.field public static final m3_sys_color_dark_surface_container_lowest:I = 0x7f06048a

.field public static final m3_sys_color_dark_surface_dim:I = 0x7f06048b

.field public static final m3_sys_color_dark_surface_variant:I = 0x7f06048c

.field public static final m3_sys_color_dark_tertiary:I = 0x7f06048d

.field public static final m3_sys_color_dark_tertiary_container:I = 0x7f06048e

.field public static final m3_sys_color_dynamic_dark_background:I = 0x7f06048f

.field public static final m3_sys_color_dynamic_dark_inverse_on_surface:I = 0x7f060490

.field public static final m3_sys_color_dynamic_dark_inverse_primary:I = 0x7f060491

.field public static final m3_sys_color_dynamic_dark_inverse_surface:I = 0x7f060492

.field public static final m3_sys_color_dynamic_dark_on_background:I = 0x7f060493

.field public static final m3_sys_color_dynamic_dark_on_primary:I = 0x7f060494

.field public static final m3_sys_color_dynamic_dark_on_primary_container:I = 0x7f060495

.field public static final m3_sys_color_dynamic_dark_on_secondary:I = 0x7f060496

.field public static final m3_sys_color_dynamic_dark_on_secondary_container:I = 0x7f060497

.field public static final m3_sys_color_dynamic_dark_on_surface:I = 0x7f060498

.field public static final m3_sys_color_dynamic_dark_on_surface_variant:I = 0x7f060499

.field public static final m3_sys_color_dynamic_dark_on_tertiary:I = 0x7f06049a

.field public static final m3_sys_color_dynamic_dark_on_tertiary_container:I = 0x7f06049b

.field public static final m3_sys_color_dynamic_dark_outline:I = 0x7f06049c

.field public static final m3_sys_color_dynamic_dark_outline_variant:I = 0x7f06049d

.field public static final m3_sys_color_dynamic_dark_primary:I = 0x7f06049e

.field public static final m3_sys_color_dynamic_dark_primary_container:I = 0x7f06049f

.field public static final m3_sys_color_dynamic_dark_secondary:I = 0x7f0604a0

.field public static final m3_sys_color_dynamic_dark_secondary_container:I = 0x7f0604a1

.field public static final m3_sys_color_dynamic_dark_surface:I = 0x7f0604a2

.field public static final m3_sys_color_dynamic_dark_surface_bright:I = 0x7f0604a3

.field public static final m3_sys_color_dynamic_dark_surface_container:I = 0x7f0604a4

.field public static final m3_sys_color_dynamic_dark_surface_container_high:I = 0x7f0604a5

.field public static final m3_sys_color_dynamic_dark_surface_container_highest:I = 0x7f0604a6

.field public static final m3_sys_color_dynamic_dark_surface_container_low:I = 0x7f0604a7

.field public static final m3_sys_color_dynamic_dark_surface_container_lowest:I = 0x7f0604a8

.field public static final m3_sys_color_dynamic_dark_surface_dim:I = 0x7f0604a9

.field public static final m3_sys_color_dynamic_dark_surface_variant:I = 0x7f0604aa

.field public static final m3_sys_color_dynamic_dark_tertiary:I = 0x7f0604ab

.field public static final m3_sys_color_dynamic_dark_tertiary_container:I = 0x7f0604ac

.field public static final m3_sys_color_dynamic_light_background:I = 0x7f0604ad

.field public static final m3_sys_color_dynamic_light_inverse_on_surface:I = 0x7f0604ae

.field public static final m3_sys_color_dynamic_light_inverse_primary:I = 0x7f0604af

.field public static final m3_sys_color_dynamic_light_inverse_surface:I = 0x7f0604b0

.field public static final m3_sys_color_dynamic_light_on_background:I = 0x7f0604b1

.field public static final m3_sys_color_dynamic_light_on_primary:I = 0x7f0604b2

.field public static final m3_sys_color_dynamic_light_on_primary_container:I = 0x7f0604b3

.field public static final m3_sys_color_dynamic_light_on_secondary:I = 0x7f0604b4

.field public static final m3_sys_color_dynamic_light_on_secondary_container:I = 0x7f0604b5

.field public static final m3_sys_color_dynamic_light_on_surface:I = 0x7f0604b6

.field public static final m3_sys_color_dynamic_light_on_surface_variant:I = 0x7f0604b7

.field public static final m3_sys_color_dynamic_light_on_tertiary:I = 0x7f0604b8

.field public static final m3_sys_color_dynamic_light_on_tertiary_container:I = 0x7f0604b9

.field public static final m3_sys_color_dynamic_light_outline:I = 0x7f0604ba

.field public static final m3_sys_color_dynamic_light_outline_variant:I = 0x7f0604bb

.field public static final m3_sys_color_dynamic_light_primary:I = 0x7f0604bc

.field public static final m3_sys_color_dynamic_light_primary_container:I = 0x7f0604bd

.field public static final m3_sys_color_dynamic_light_secondary:I = 0x7f0604be

.field public static final m3_sys_color_dynamic_light_secondary_container:I = 0x7f0604bf

.field public static final m3_sys_color_dynamic_light_surface:I = 0x7f0604c0

.field public static final m3_sys_color_dynamic_light_surface_bright:I = 0x7f0604c1

.field public static final m3_sys_color_dynamic_light_surface_container:I = 0x7f0604c2

.field public static final m3_sys_color_dynamic_light_surface_container_high:I = 0x7f0604c3

.field public static final m3_sys_color_dynamic_light_surface_container_highest:I = 0x7f0604c4

.field public static final m3_sys_color_dynamic_light_surface_container_low:I = 0x7f0604c5

.field public static final m3_sys_color_dynamic_light_surface_container_lowest:I = 0x7f0604c6

.field public static final m3_sys_color_dynamic_light_surface_dim:I = 0x7f0604c7

.field public static final m3_sys_color_dynamic_light_surface_variant:I = 0x7f0604c8

.field public static final m3_sys_color_dynamic_light_tertiary:I = 0x7f0604c9

.field public static final m3_sys_color_dynamic_light_tertiary_container:I = 0x7f0604ca

.field public static final m3_sys_color_dynamic_on_primary_fixed:I = 0x7f0604cb

.field public static final m3_sys_color_dynamic_on_primary_fixed_variant:I = 0x7f0604cc

.field public static final m3_sys_color_dynamic_on_secondary_fixed:I = 0x7f0604cd

.field public static final m3_sys_color_dynamic_on_secondary_fixed_variant:I = 0x7f0604ce

.field public static final m3_sys_color_dynamic_on_tertiary_fixed:I = 0x7f0604cf

.field public static final m3_sys_color_dynamic_on_tertiary_fixed_variant:I = 0x7f0604d0

.field public static final m3_sys_color_dynamic_primary_fixed:I = 0x7f0604d1

.field public static final m3_sys_color_dynamic_primary_fixed_dim:I = 0x7f0604d2

.field public static final m3_sys_color_dynamic_secondary_fixed:I = 0x7f0604d3

.field public static final m3_sys_color_dynamic_secondary_fixed_dim:I = 0x7f0604d4

.field public static final m3_sys_color_dynamic_tertiary_fixed:I = 0x7f0604d5

.field public static final m3_sys_color_dynamic_tertiary_fixed_dim:I = 0x7f0604d6

.field public static final m3_sys_color_light_background:I = 0x7f0604d7

.field public static final m3_sys_color_light_error:I = 0x7f0604d8

.field public static final m3_sys_color_light_error_container:I = 0x7f0604d9

.field public static final m3_sys_color_light_inverse_on_surface:I = 0x7f0604da

.field public static final m3_sys_color_light_inverse_primary:I = 0x7f0604db

.field public static final m3_sys_color_light_inverse_surface:I = 0x7f0604dc

.field public static final m3_sys_color_light_on_background:I = 0x7f0604dd

.field public static final m3_sys_color_light_on_error:I = 0x7f0604de

.field public static final m3_sys_color_light_on_error_container:I = 0x7f0604df

.field public static final m3_sys_color_light_on_primary:I = 0x7f0604e0

.field public static final m3_sys_color_light_on_primary_container:I = 0x7f0604e1

.field public static final m3_sys_color_light_on_secondary:I = 0x7f0604e2

.field public static final m3_sys_color_light_on_secondary_container:I = 0x7f0604e3

.field public static final m3_sys_color_light_on_surface:I = 0x7f0604e4

.field public static final m3_sys_color_light_on_surface_variant:I = 0x7f0604e5

.field public static final m3_sys_color_light_on_tertiary:I = 0x7f0604e6

.field public static final m3_sys_color_light_on_tertiary_container:I = 0x7f0604e7

.field public static final m3_sys_color_light_outline:I = 0x7f0604e8

.field public static final m3_sys_color_light_outline_variant:I = 0x7f0604e9

.field public static final m3_sys_color_light_primary:I = 0x7f0604ea

.field public static final m3_sys_color_light_primary_container:I = 0x7f0604eb

.field public static final m3_sys_color_light_secondary:I = 0x7f0604ec

.field public static final m3_sys_color_light_secondary_container:I = 0x7f0604ed

.field public static final m3_sys_color_light_surface:I = 0x7f0604ee

.field public static final m3_sys_color_light_surface_bright:I = 0x7f0604ef

.field public static final m3_sys_color_light_surface_container:I = 0x7f0604f0

.field public static final m3_sys_color_light_surface_container_high:I = 0x7f0604f1

.field public static final m3_sys_color_light_surface_container_highest:I = 0x7f0604f2

.field public static final m3_sys_color_light_surface_container_low:I = 0x7f0604f3

.field public static final m3_sys_color_light_surface_container_lowest:I = 0x7f0604f4

.field public static final m3_sys_color_light_surface_dim:I = 0x7f0604f5

.field public static final m3_sys_color_light_surface_variant:I = 0x7f0604f6

.field public static final m3_sys_color_light_tertiary:I = 0x7f0604f7

.field public static final m3_sys_color_light_tertiary_container:I = 0x7f0604f8

.field public static final m3_sys_color_on_primary_fixed:I = 0x7f0604f9

.field public static final m3_sys_color_on_primary_fixed_variant:I = 0x7f0604fa

.field public static final m3_sys_color_on_secondary_fixed:I = 0x7f0604fb

.field public static final m3_sys_color_on_secondary_fixed_variant:I = 0x7f0604fc

.field public static final m3_sys_color_on_tertiary_fixed:I = 0x7f0604fd

.field public static final m3_sys_color_on_tertiary_fixed_variant:I = 0x7f0604fe

.field public static final m3_sys_color_primary_fixed:I = 0x7f0604ff

.field public static final m3_sys_color_primary_fixed_dim:I = 0x7f060500

.field public static final m3_sys_color_secondary_fixed:I = 0x7f060501

.field public static final m3_sys_color_secondary_fixed_dim:I = 0x7f060502

.field public static final m3_sys_color_tertiary_fixed:I = 0x7f060503

.field public static final m3_sys_color_tertiary_fixed_dim:I = 0x7f060504

.field public static final m3_tabs_icon_color:I = 0x7f060505

.field public static final m3_tabs_icon_color_secondary:I = 0x7f060506

.field public static final m3_tabs_ripple_color:I = 0x7f060507

.field public static final m3_tabs_ripple_color_secondary:I = 0x7f060508

.field public static final m3_tabs_text_color:I = 0x7f060509

.field public static final m3_tabs_text_color_secondary:I = 0x7f06050a

.field public static final m3_text_button_background_color_selector:I = 0x7f06050b

.field public static final m3_text_button_foreground_color_selector:I = 0x7f06050c

.field public static final m3_text_button_ripple_color_selector:I = 0x7f06050d

.field public static final m3_textfield_filled_background_color:I = 0x7f06050e

.field public static final m3_textfield_indicator_text_color:I = 0x7f06050f

.field public static final m3_textfield_input_text_color:I = 0x7f060510

.field public static final m3_textfield_label_color:I = 0x7f060511

.field public static final m3_textfield_stroke_color:I = 0x7f060512

.field public static final m3_timepicker_button_background_color:I = 0x7f060513

.field public static final m3_timepicker_button_ripple_color:I = 0x7f060514

.field public static final m3_timepicker_button_text_color:I = 0x7f060515

.field public static final m3_timepicker_clock_text_color:I = 0x7f060516

.field public static final m3_timepicker_display_background_color:I = 0x7f060517

.field public static final m3_timepicker_display_ripple_color:I = 0x7f060518

.field public static final m3_timepicker_display_text_color:I = 0x7f060519

.field public static final m3_timepicker_secondary_text_button_ripple_color:I = 0x7f06051a

.field public static final m3_timepicker_secondary_text_button_text_color:I = 0x7f06051b

.field public static final m3_timepicker_time_input_stroke_color:I = 0x7f06051c

.field public static final m3_tonal_button_ripple_color_selector:I = 0x7f06051d

.field public static final main_colorprimarydark:I = 0x7f06051e

.field public static final main_doc_list_lock_bg:I = 0x7f06051f

.field public static final main_drawer_menu_bg_color:I = 0x7f060520

.field public static final main_list_item_tag_color:I = 0x7f060521

.field public static final main_list_item_time_color:I = 0x7f060522

.field public static final main_list_item_title_color:I = 0x7f060523

.field public static final main_menu_background_color:I = 0x7f060524

.field public static final main_pull_action_bg_color:I = 0x7f060525

.field public static final main_selected_tag_text_color:I = 0x7f060526

.field public static final main_title_color:I = 0x7f060527

.field public static final mainmenu_colorprimarydark:I = 0x7f060528

.field public static final material_blue_grey_800:I = 0x7f060529

.field public static final material_blue_grey_900:I = 0x7f06052a

.field public static final material_blue_grey_950:I = 0x7f06052b

.field public static final material_cursor_color:I = 0x7f06052c

.field public static final material_deep_teal_200:I = 0x7f06052d

.field public static final material_deep_teal_500:I = 0x7f06052e

.field public static final material_divider_color:I = 0x7f06052f

.field public static final material_dynamic_neutral0:I = 0x7f060530

.field public static final material_dynamic_neutral10:I = 0x7f060531

.field public static final material_dynamic_neutral100:I = 0x7f060532

.field public static final material_dynamic_neutral20:I = 0x7f060533

.field public static final material_dynamic_neutral30:I = 0x7f060534

.field public static final material_dynamic_neutral40:I = 0x7f060535

.field public static final material_dynamic_neutral50:I = 0x7f060536

.field public static final material_dynamic_neutral60:I = 0x7f060537

.field public static final material_dynamic_neutral70:I = 0x7f060538

.field public static final material_dynamic_neutral80:I = 0x7f060539

.field public static final material_dynamic_neutral90:I = 0x7f06053a

.field public static final material_dynamic_neutral95:I = 0x7f06053b

.field public static final material_dynamic_neutral99:I = 0x7f06053c

.field public static final material_dynamic_neutral_variant0:I = 0x7f06053d

.field public static final material_dynamic_neutral_variant10:I = 0x7f06053e

.field public static final material_dynamic_neutral_variant100:I = 0x7f06053f

.field public static final material_dynamic_neutral_variant20:I = 0x7f060540

.field public static final material_dynamic_neutral_variant30:I = 0x7f060541

.field public static final material_dynamic_neutral_variant40:I = 0x7f060542

.field public static final material_dynamic_neutral_variant50:I = 0x7f060543

.field public static final material_dynamic_neutral_variant60:I = 0x7f060544

.field public static final material_dynamic_neutral_variant70:I = 0x7f060545

.field public static final material_dynamic_neutral_variant80:I = 0x7f060546

.field public static final material_dynamic_neutral_variant90:I = 0x7f060547

.field public static final material_dynamic_neutral_variant95:I = 0x7f060548

.field public static final material_dynamic_neutral_variant99:I = 0x7f060549

.field public static final material_dynamic_primary0:I = 0x7f06054a

.field public static final material_dynamic_primary10:I = 0x7f06054b

.field public static final material_dynamic_primary100:I = 0x7f06054c

.field public static final material_dynamic_primary20:I = 0x7f06054d

.field public static final material_dynamic_primary30:I = 0x7f06054e

.field public static final material_dynamic_primary40:I = 0x7f06054f

.field public static final material_dynamic_primary50:I = 0x7f060550

.field public static final material_dynamic_primary60:I = 0x7f060551

.field public static final material_dynamic_primary70:I = 0x7f060552

.field public static final material_dynamic_primary80:I = 0x7f060553

.field public static final material_dynamic_primary90:I = 0x7f060554

.field public static final material_dynamic_primary95:I = 0x7f060555

.field public static final material_dynamic_primary99:I = 0x7f060556

.field public static final material_dynamic_secondary0:I = 0x7f060557

.field public static final material_dynamic_secondary10:I = 0x7f060558

.field public static final material_dynamic_secondary100:I = 0x7f060559

.field public static final material_dynamic_secondary20:I = 0x7f06055a

.field public static final material_dynamic_secondary30:I = 0x7f06055b

.field public static final material_dynamic_secondary40:I = 0x7f06055c

.field public static final material_dynamic_secondary50:I = 0x7f06055d

.field public static final material_dynamic_secondary60:I = 0x7f06055e

.field public static final material_dynamic_secondary70:I = 0x7f06055f

.field public static final material_dynamic_secondary80:I = 0x7f060560

.field public static final material_dynamic_secondary90:I = 0x7f060561

.field public static final material_dynamic_secondary95:I = 0x7f060562

.field public static final material_dynamic_secondary99:I = 0x7f060563

.field public static final material_dynamic_tertiary0:I = 0x7f060564

.field public static final material_dynamic_tertiary10:I = 0x7f060565

.field public static final material_dynamic_tertiary100:I = 0x7f060566

.field public static final material_dynamic_tertiary20:I = 0x7f060567

.field public static final material_dynamic_tertiary30:I = 0x7f060568

.field public static final material_dynamic_tertiary40:I = 0x7f060569

.field public static final material_dynamic_tertiary50:I = 0x7f06056a

.field public static final material_dynamic_tertiary60:I = 0x7f06056b

.field public static final material_dynamic_tertiary70:I = 0x7f06056c

.field public static final material_dynamic_tertiary80:I = 0x7f06056d

.field public static final material_dynamic_tertiary90:I = 0x7f06056e

.field public static final material_dynamic_tertiary95:I = 0x7f06056f

.field public static final material_dynamic_tertiary99:I = 0x7f060570

.field public static final material_grey_100:I = 0x7f060571

.field public static final material_grey_300:I = 0x7f060572

.field public static final material_grey_50:I = 0x7f060573

.field public static final material_grey_600:I = 0x7f060574

.field public static final material_grey_800:I = 0x7f060575

.field public static final material_grey_850:I = 0x7f060576

.field public static final material_grey_900:I = 0x7f060577

.field public static final material_harmonized_color_error:I = 0x7f060578

.field public static final material_harmonized_color_error_container:I = 0x7f060579

.field public static final material_harmonized_color_on_error:I = 0x7f06057a

.field public static final material_harmonized_color_on_error_container:I = 0x7f06057b

.field public static final material_on_background_disabled:I = 0x7f06057c

.field public static final material_on_background_emphasis_high_type:I = 0x7f06057d

.field public static final material_on_background_emphasis_medium:I = 0x7f06057e

.field public static final material_on_primary_disabled:I = 0x7f06057f

.field public static final material_on_primary_emphasis_high_type:I = 0x7f060580

.field public static final material_on_primary_emphasis_medium:I = 0x7f060581

.field public static final material_on_surface_disabled:I = 0x7f060582

.field public static final material_on_surface_emphasis_high_type:I = 0x7f060583

.field public static final material_on_surface_emphasis_medium:I = 0x7f060584

.field public static final material_on_surface_stroke:I = 0x7f060585

.field public static final material_personalized__highlighted_text:I = 0x7f060586

.field public static final material_personalized__highlighted_text_inverse:I = 0x7f060587

.field public static final material_personalized_color_background:I = 0x7f060588

.field public static final material_personalized_color_control_activated:I = 0x7f060589

.field public static final material_personalized_color_control_highlight:I = 0x7f06058a

.field public static final material_personalized_color_control_normal:I = 0x7f06058b

.field public static final material_personalized_color_error:I = 0x7f06058c

.field public static final material_personalized_color_error_container:I = 0x7f06058d

.field public static final material_personalized_color_on_background:I = 0x7f06058e

.field public static final material_personalized_color_on_error:I = 0x7f06058f

.field public static final material_personalized_color_on_error_container:I = 0x7f060590

.field public static final material_personalized_color_on_primary:I = 0x7f060591

.field public static final material_personalized_color_on_primary_container:I = 0x7f060592

.field public static final material_personalized_color_on_secondary:I = 0x7f060593

.field public static final material_personalized_color_on_secondary_container:I = 0x7f060594

.field public static final material_personalized_color_on_surface:I = 0x7f060595

.field public static final material_personalized_color_on_surface_inverse:I = 0x7f060596

.field public static final material_personalized_color_on_surface_variant:I = 0x7f060597

.field public static final material_personalized_color_on_tertiary:I = 0x7f060598

.field public static final material_personalized_color_on_tertiary_container:I = 0x7f060599

.field public static final material_personalized_color_outline:I = 0x7f06059a

.field public static final material_personalized_color_outline_variant:I = 0x7f06059b

.field public static final material_personalized_color_primary:I = 0x7f06059c

.field public static final material_personalized_color_primary_container:I = 0x7f06059d

.field public static final material_personalized_color_primary_inverse:I = 0x7f06059e

.field public static final material_personalized_color_primary_text:I = 0x7f06059f

.field public static final material_personalized_color_primary_text_inverse:I = 0x7f0605a0

.field public static final material_personalized_color_secondary:I = 0x7f0605a1

.field public static final material_personalized_color_secondary_container:I = 0x7f0605a2

.field public static final material_personalized_color_secondary_text:I = 0x7f0605a3

.field public static final material_personalized_color_secondary_text_inverse:I = 0x7f0605a4

.field public static final material_personalized_color_surface:I = 0x7f0605a5

.field public static final material_personalized_color_surface_bright:I = 0x7f0605a6

.field public static final material_personalized_color_surface_container:I = 0x7f0605a7

.field public static final material_personalized_color_surface_container_high:I = 0x7f0605a8

.field public static final material_personalized_color_surface_container_highest:I = 0x7f0605a9

.field public static final material_personalized_color_surface_container_low:I = 0x7f0605aa

.field public static final material_personalized_color_surface_container_lowest:I = 0x7f0605ab

.field public static final material_personalized_color_surface_dim:I = 0x7f0605ac

.field public static final material_personalized_color_surface_inverse:I = 0x7f0605ad

.field public static final material_personalized_color_surface_variant:I = 0x7f0605ae

.field public static final material_personalized_color_tertiary:I = 0x7f0605af

.field public static final material_personalized_color_tertiary_container:I = 0x7f0605b0

.field public static final material_personalized_color_text_hint_foreground_inverse:I = 0x7f0605b1

.field public static final material_personalized_color_text_primary_inverse:I = 0x7f0605b2

.field public static final material_personalized_color_text_primary_inverse_disable_only:I = 0x7f0605b3

.field public static final material_personalized_color_text_secondary_and_tertiary_inverse:I = 0x7f0605b4

.field public static final material_personalized_color_text_secondary_and_tertiary_inverse_disabled:I = 0x7f0605b5

.field public static final material_personalized_hint_foreground:I = 0x7f0605b6

.field public static final material_personalized_hint_foreground_inverse:I = 0x7f0605b7

.field public static final material_personalized_primary_inverse_text_disable_only:I = 0x7f0605b8

.field public static final material_personalized_primary_text_disable_only:I = 0x7f0605b9

.field public static final material_slider_active_tick_marks_color:I = 0x7f0605ba

.field public static final material_slider_active_track_color:I = 0x7f0605bb

.field public static final material_slider_halo_color:I = 0x7f0605bc

.field public static final material_slider_inactive_tick_marks_color:I = 0x7f0605bd

.field public static final material_slider_inactive_track_color:I = 0x7f0605be

.field public static final material_slider_thumb_color:I = 0x7f0605bf

.field public static final material_timepicker_button_background:I = 0x7f0605c0

.field public static final material_timepicker_button_stroke:I = 0x7f0605c1

.field public static final material_timepicker_clock_text_color:I = 0x7f0605c2

.field public static final material_timepicker_clockface:I = 0x7f0605c3

.field public static final material_timepicker_modebutton_tint:I = 0x7f0605c4

.field public static final menu_more_textColor:I = 0x7f0605c5

.field public static final message_view_collage_preview:I = 0x7f0605c6

.field public static final mtrl_btn_bg_color_selector:I = 0x7f0605c8

.field public static final mtrl_btn_ripple_color:I = 0x7f0605c9

.field public static final mtrl_btn_stroke_color_selector:I = 0x7f0605ca

.field public static final mtrl_btn_text_btn_bg_color_selector:I = 0x7f0605cb

.field public static final mtrl_btn_text_btn_ripple_color:I = 0x7f0605cc

.field public static final mtrl_btn_text_color_disabled:I = 0x7f0605cd

.field public static final mtrl_btn_text_color_selector:I = 0x7f0605ce

.field public static final mtrl_btn_transparent_bg_color:I = 0x7f0605cf

.field public static final mtrl_calendar_item_stroke_color:I = 0x7f0605d0

.field public static final mtrl_calendar_selected_range:I = 0x7f0605d1

.field public static final mtrl_card_view_foreground:I = 0x7f0605d2

.field public static final mtrl_card_view_ripple:I = 0x7f0605d3

.field public static final mtrl_chip_background_color:I = 0x7f0605d4

.field public static final mtrl_chip_close_icon_tint:I = 0x7f0605d5

.field public static final mtrl_chip_surface_color:I = 0x7f0605d6

.field public static final mtrl_chip_text_color:I = 0x7f0605d7

.field public static final mtrl_choice_chip_background_color:I = 0x7f0605d8

.field public static final mtrl_choice_chip_ripple_color:I = 0x7f0605d9

.field public static final mtrl_choice_chip_text_color:I = 0x7f0605da

.field public static final mtrl_error:I = 0x7f0605db

.field public static final mtrl_fab_bg_color_selector:I = 0x7f0605dc

.field public static final mtrl_fab_icon_text_color_selector:I = 0x7f0605dd

.field public static final mtrl_fab_ripple_color:I = 0x7f0605de

.field public static final mtrl_filled_background_color:I = 0x7f0605df

.field public static final mtrl_filled_icon_tint:I = 0x7f0605e0

.field public static final mtrl_filled_stroke_color:I = 0x7f0605e1

.field public static final mtrl_indicator_text_color:I = 0x7f0605e2

.field public static final mtrl_navigation_bar_colored_item_tint:I = 0x7f0605e3

.field public static final mtrl_navigation_bar_colored_ripple_color:I = 0x7f0605e4

.field public static final mtrl_navigation_bar_item_tint:I = 0x7f0605e5

.field public static final mtrl_navigation_bar_ripple_color:I = 0x7f0605e6

.field public static final mtrl_navigation_item_background_color:I = 0x7f0605e7

.field public static final mtrl_navigation_item_icon_tint:I = 0x7f0605e8

.field public static final mtrl_navigation_item_text_color:I = 0x7f0605e9

.field public static final mtrl_on_primary_text_btn_text_color_selector:I = 0x7f0605ea

.field public static final mtrl_on_surface_ripple_color:I = 0x7f0605eb

.field public static final mtrl_outlined_icon_tint:I = 0x7f0605ec

.field public static final mtrl_outlined_stroke_color:I = 0x7f0605ed

.field public static final mtrl_popupmenu_overlay_color:I = 0x7f0605ee

.field public static final mtrl_scrim_color:I = 0x7f0605ef

.field public static final mtrl_switch_thumb_icon_tint:I = 0x7f0605f0

.field public static final mtrl_switch_thumb_tint:I = 0x7f0605f1

.field public static final mtrl_switch_track_decoration_tint:I = 0x7f0605f2

.field public static final mtrl_switch_track_tint:I = 0x7f0605f3

.field public static final mtrl_tabs_colored_ripple_color:I = 0x7f0605f4

.field public static final mtrl_tabs_icon_color_selector:I = 0x7f0605f5

.field public static final mtrl_tabs_icon_color_selector_colored:I = 0x7f0605f6

.field public static final mtrl_tabs_legacy_text_color_selector:I = 0x7f0605f7

.field public static final mtrl_tabs_ripple_color:I = 0x7f0605f8

.field public static final mtrl_text_btn_text_color_selector:I = 0x7f0605f9

.field public static final mtrl_textinput_default_box_stroke_color:I = 0x7f0605fa

.field public static final mtrl_textinput_disabled_color:I = 0x7f0605fb

.field public static final mtrl_textinput_filled_box_default_background_color:I = 0x7f0605fc

.field public static final mtrl_textinput_focused_box_stroke_color:I = 0x7f0605fd

.field public static final mtrl_textinput_hovered_box_stroke_color:I = 0x7f0605fe

.field public static final nav_start_primary_color:I = 0x7f0605ff

.field public static final notification_action_color_filter:I = 0x7f060600

.field public static final notification_icon_bg_color:I = 0x7f060601

.field public static final notification_material_background_media_default_color:I = 0x7f060602

.field public static final ocr_result_title_bar:I = 0x7f060603

.field public static final pad_default_edittext_color:I = 0x7f060604

.field public static final pad_gray_color:I = 0x7f060605

.field public static final pad_image_scan_default_trim_line_color:I = 0x7f060606

.field public static final pad_image_scan_error_trim_line_color:I = 0x7f060607

.field public static final page_list_divider:I = 0x7f060608

.field public static final page_list_item_index_color:I = 0x7f060609

.field public static final page_list_item_note_background:I = 0x7f06060a

.field public static final pd_bg_center:I = 0x7f06060b

.field public static final pd_bg_end:I = 0x7f06060c

.field public static final pd_bg_start:I = 0x7f06060d

.field public static final pd_progress_center:I = 0x7f06060e

.field public static final pd_progress_end:I = 0x7f06060f

.field public static final pd_progress_start:I = 0x7f060610

.field public static final pd_sec_progress_center:I = 0x7f060611

.field public static final pd_sec_progress_end:I = 0x7f060612

.field public static final pd_sec_progress_start:I = 0x7f060613

.field public static final pdf_add_size_hint_color:I = 0x7f060614

.field public static final pdf_add_size_name_color:I = 0x7f060615

.field public static final pdf_watermark_real_size_bg:I = 0x7f060616

.field public static final point_expire_color:I = 0x7f060620

.field public static final pop_tags_item_no_selected:I = 0x7f060621

.field public static final popup_menu_item_text_color:I = 0x7f060622

.field public static final premium_start_time:I = 0x7f060623

.field public static final primary_dark_material_dark:I = 0x7f060624

.field public static final primary_dark_material_light:I = 0x7f060625

.field public static final primary_material_dark:I = 0x7f060626

.field public static final primary_material_light:I = 0x7f060627

.field public static final primary_text_default_material_dark:I = 0x7f060628

.field public static final primary_text_default_material_light:I = 0x7f060629

.field public static final primary_text_disabled_material_dark:I = 0x7f06062a

.field public static final primary_text_disabled_material_light:I = 0x7f06062b

.field public static final pull_to_refresh_text_color:I = 0x7f06062c

.field public static final pur_gray:I = 0x7f06062d

.field public static final purple_200:I = 0x7f06062e

.field public static final purple_500:I = 0x7f06062f

.field public static final purple_700:I = 0x7f060630

.field public static final radiogroup_background:I = 0x7f060631

.field public static final red_color_warning:I = 0x7f060632

.field public static final red_da4646:I = 0x7f060633

.field public static final red_dot:I = 0x7f060634

.field public static final red_error:I = 0x7f060635

.field public static final reward_default_text_color:I = 0x7f060636

.field public static final reward_disable_text_color:I = 0x7f060637

.field public static final ripple_material_dark:I = 0x7f060638

.field public static final ripple_material_light:I = 0x7f060639

.field public static final scanner_back_pressed:I = 0x7f06063a

.field public static final secondary_text_default_material_dark:I = 0x7f06063b

.field public static final secondary_text_default_material_light:I = 0x7f06063c

.field public static final secondary_text_disabled_material_dark:I = 0x7f06063d

.field public static final secondary_text_disabled_material_light:I = 0x7f06063e

.field public static final sep_btn_dark_blue:I = 0x7f060642

.field public static final sep_tab_radio_btn_green:I = 0x7f060643

.field public static final separate_line_color:I = 0x7f060644

.field public static final spinner_item_no_selected:I = 0x7f060645

.field public static final stand_red:I = 0x7f060646

.field public static final start_drawer_menu_text_color:I = 0x7f060647

.field public static final start_drawer_pressed_background:I = 0x7f060648

.field public static final storage_color_blue:I = 0x7f060649

.field public static final storage_color_orange:I = 0x7f06064a

.field public static final storage_color_red:I = 0x7f06064b

.field public static final switch_colorstate_list:I = 0x7f06064c

.field public static final switch_thumb_disabled_material_dark:I = 0x7f06064d

.field public static final switch_thumb_disabled_material_light:I = 0x7f06064e

.field public static final switch_thumb_material_dark:I = 0x7f06064f

.field public static final switch_thumb_material_light:I = 0x7f060650

.field public static final switch_thumb_normal_material_dark:I = 0x7f060651

.field public static final switch_thumb_normal_material_light:I = 0x7f060652

.field public static final sync_normal_page_bg:I = 0x7f060653

.field public static final tag_manager_text_color:I = 0x7f060654

.field public static final tag_set_unselect_text_color:I = 0x7f060655

.field public static final tags_item_no_selected:I = 0x7f060656

.field public static final teal_200:I = 0x7f060657

.field public static final teal_700:I = 0x7f060658

.field public static final text_color_white:I = 0x7f06065b

.field public static final textview_unselector_color:I = 0x7f06065c

.field public static final thumb_switch_colorstate_list:I = 0x7f06065d

.field public static final tip_textview_textcolor:I = 0x7f06065e

.field public static final tips_background:I = 0x7f06065f

.field public static final tips_drag_green:I = 0x7f060660

.field public static final title_disable_color:I = 0x7f060661

.field public static final title_tab_selected_text_color:I = 0x7f060662

.field public static final title_tab_unselected_text_color:I = 0x7f060663

.field public static final toolbar_colorprimary:I = 0x7f060664

.field public static final toolbar_colorprimarydark:I = 0x7f060665

.field public static final toolbar_title_color:I = 0x7f060666

.field public static final tooltip_background_dark:I = 0x7f060667

.field public static final tooltip_background_light:I = 0x7f060668

.field public static final transparent:I = 0x7f060669

.field public static final tv_text_color_selector:I = 0x7f0606a0

.field public static final txt_collaborate_comment_content:I = 0x7f0606a1

.field public static final txt_collaborate_empty_intro:I = 0x7f0606a2

.field public static final txt_collaborators_unaccept:I = 0x7f0606a3

.field public static final txt_enhance_mode_hl:I = 0x7f0606a4

.field public static final txt_enhance_mode_normal:I = 0x7f0606a5

.field public static final txt_main_taglist_item:I = 0x7f0606a7

.field public static final txt_main_taglist_unselect:I = 0x7f0606a8

.field public static final txt_message_item_titlecolor_read:I = 0x7f0606a9

.field public static final txt_message_item_titlecolor_unread:I = 0x7f0606aa

.field public static final txt_message_itme_timecolor:I = 0x7f0606ab

.field public static final txt_register_black:I = 0x7f0606ac

.field public static final txt_white_selected_black:I = 0x7f0606ad

.field public static final txt_white_selected_green:I = 0x7f0606ae

.field public static final upgrade_premium:I = 0x7f0606af

.field public static final uploading_item_text_color:I = 0x7f0606b0

.field public static final util_share_dlg_divider:I = 0x7f0606b1

.field public static final util_share_dlg_selector_long_pressed:I = 0x7f0606b2

.field public static final util_share_dlg_selector_normal:I = 0x7f0606b3

.field public static final util_share_dlg_selector_pressed:I = 0x7f0606b4

.field public static final util_share_dlg_txt_color:I = 0x7f0606b5

.field public static final verify_code_text_color:I = 0x7f0606b6

.field public static final viewfinder_laser:I = 0x7f0606b7

.field public static final viewfinder_mask:I = 0x7f0606b8

.field public static final water_drawable_highlight_down:I = 0x7f0606b9

.field public static final water_drawable_highlight_focus:I = 0x7f0606ba

.field public static final water_mark_actionbar_background:I = 0x7f0606bb

.field public static final web_progress_bar_color:I = 0x7f0606be

.field public static final white:I = 0x7f0606bf


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/intsig/tianshu/verify/LoginCResult;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "LoginCResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tianshu/verify/LoginCResult$EduAuth;,
        Lcom/intsig/tianshu/verify/LoginCResult$SettingInfo;,
        Lcom/intsig/tianshu/verify/LoginCResult$ClientInfo;,
        Lcom/intsig/tianshu/verify/LoginCResult$ProfileInfo;,
        Lcom/intsig/tianshu/verify/LoginCResult$LicenseDetail;,
        Lcom/intsig/tianshu/verify/LoginCResult$ApiInfo;,
        Lcom/intsig/tianshu/verify/LoginCResult$TokenInfo;,
        Lcom/intsig/tianshu/verify/LoginCResult$ConsumerInfo;
    }
.end annotation


# instance fields
.field public api:Lcom/intsig/tianshu/verify/LoginCResult$ApiInfo;

.field public apis:Lcom/intsig/tianshu/ApiCenterInfo;

.field public client_list:[Lcom/intsig/tianshu/verify/LoginCResult$ClientInfo;

.field public licenses:Ljava/lang/String;

.field public licenses_detail:Lcom/intsig/tianshu/verify/LoginCResult$LicenseDetail;

.field public profile:Lcom/intsig/tianshu/verify/LoginCResult$ProfileInfo;

.field public reg_time:J

.field public settings:Lcom/intsig/tianshu/verify/LoginCResult$SettingInfo;

.field public token:Lcom/intsig/tianshu/verify/LoginCResult$TokenInfo;

.field public user:Lcom/intsig/tianshu/verify/LoginCResult$ConsumerInfo;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

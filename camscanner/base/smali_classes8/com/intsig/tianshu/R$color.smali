.class public final Lcom/intsig/tianshu/R$color;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tianshu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f060000

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f060001

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f060002

.field public static final abc_btn_colored_text_material:I = 0x7f060003

.field public static final abc_color_highlight_material:I = 0x7f060004

.field public static final abc_decor_view_status_guard:I = 0x7f060005

.field public static final abc_decor_view_status_guard_light:I = 0x7f060006

.field public static final abc_hint_foreground_material_dark:I = 0x7f060007

.field public static final abc_hint_foreground_material_light:I = 0x7f060008

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f060009

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f06000a

.field public static final abc_primary_text_material_dark:I = 0x7f06000b

.field public static final abc_primary_text_material_light:I = 0x7f06000c

.field public static final abc_search_url_text:I = 0x7f06000d

.field public static final abc_search_url_text_normal:I = 0x7f06000e

.field public static final abc_search_url_text_pressed:I = 0x7f06000f

.field public static final abc_search_url_text_selected:I = 0x7f060010

.field public static final abc_secondary_text_material_dark:I = 0x7f060011

.field public static final abc_secondary_text_material_light:I = 0x7f060012

.field public static final abc_tint_btn_checkable:I = 0x7f060013

.field public static final abc_tint_default:I = 0x7f060014

.field public static final abc_tint_edittext:I = 0x7f060015

.field public static final abc_tint_seek_thumb:I = 0x7f060016

.field public static final abc_tint_spinner:I = 0x7f060017

.field public static final abc_tint_switch_track:I = 0x7f060018

.field public static final accent_material_dark:I = 0x7f060019

.field public static final accent_material_light:I = 0x7f06001a

.field public static final androidx_core_ripple_material_light:I = 0x7f06002b

.field public static final androidx_core_secondary_text_default_material_light:I = 0x7f06002c

.field public static final background_floating_material_dark:I = 0x7f060049

.field public static final background_floating_material_light:I = 0x7f06004a

.field public static final background_material_dark:I = 0x7f06004b

.field public static final background_material_light:I = 0x7f06004c

.field public static final bright_foreground_disabled_material_dark:I = 0x7f060062

.field public static final bright_foreground_disabled_material_light:I = 0x7f060063

.field public static final bright_foreground_inverse_material_dark:I = 0x7f060064

.field public static final bright_foreground_inverse_material_light:I = 0x7f060065

.field public static final bright_foreground_material_dark:I = 0x7f060066

.field public static final bright_foreground_material_light:I = 0x7f060067

.field public static final button_material_dark:I = 0x7f06007c

.field public static final button_material_light:I = 0x7f06007d

.field public static final common_google_signin_btn_text_dark:I = 0x7f0600d4

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f0600d5

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f0600d6

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0600d7

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f0600d8

.field public static final common_google_signin_btn_text_light:I = 0x7f0600d9

.field public static final common_google_signin_btn_text_light_default:I = 0x7f0600da

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f0600db

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0600dc

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f0600dd

.field public static final common_google_signin_btn_tint:I = 0x7f0600de

.field public static final dim_foreground_disabled_material_dark:I = 0x7f060338

.field public static final dim_foreground_disabled_material_light:I = 0x7f060339

.field public static final dim_foreground_material_dark:I = 0x7f06033a

.field public static final dim_foreground_material_light:I = 0x7f06033b

.field public static final error_color_material_dark:I = 0x7f06034b

.field public static final error_color_material_light:I = 0x7f06034c

.field public static final foreground_material_dark:I = 0x7f060350

.field public static final foreground_material_light:I = 0x7f060351

.field public static final highlighted_text_material_dark:I = 0x7f06035b

.field public static final highlighted_text_material_light:I = 0x7f06035c

.field public static final logagent_bg_white:I = 0x7f06037b

.field public static final logagent_btn_bg_red_color:I = 0x7f06037c

.field public static final logagent_cad_black:I = 0x7f06037d

.field public static final logagent_color_reward_yellow:I = 0x7f06037e

.field public static final logagent_cs_blue_008FED:I = 0x7f06037f

.field public static final logagent_green_ok:I = 0x7f060380

.field public static final material_blue_grey_800:I = 0x7f060529

.field public static final material_blue_grey_900:I = 0x7f06052a

.field public static final material_blue_grey_950:I = 0x7f06052b

.field public static final material_deep_teal_200:I = 0x7f06052d

.field public static final material_deep_teal_500:I = 0x7f06052e

.field public static final material_grey_100:I = 0x7f060571

.field public static final material_grey_300:I = 0x7f060572

.field public static final material_grey_50:I = 0x7f060573

.field public static final material_grey_600:I = 0x7f060574

.field public static final material_grey_800:I = 0x7f060575

.field public static final material_grey_850:I = 0x7f060576

.field public static final material_grey_900:I = 0x7f060577

.field public static final notification_action_color_filter:I = 0x7f060600

.field public static final notification_icon_bg_color:I = 0x7f060601

.field public static final notification_material_background_media_default_color:I = 0x7f060602

.field public static final primary_dark_material_dark:I = 0x7f060624

.field public static final primary_dark_material_light:I = 0x7f060625

.field public static final primary_material_dark:I = 0x7f060626

.field public static final primary_material_light:I = 0x7f060627

.field public static final primary_text_default_material_dark:I = 0x7f060628

.field public static final primary_text_default_material_light:I = 0x7f060629

.field public static final primary_text_disabled_material_dark:I = 0x7f06062a

.field public static final primary_text_disabled_material_light:I = 0x7f06062b

.field public static final ripple_material_dark:I = 0x7f060638

.field public static final ripple_material_light:I = 0x7f060639

.field public static final secondary_text_default_material_dark:I = 0x7f06063b

.field public static final secondary_text_default_material_light:I = 0x7f06063c

.field public static final secondary_text_disabled_material_dark:I = 0x7f06063d

.field public static final secondary_text_disabled_material_light:I = 0x7f06063e

.field public static final switch_thumb_disabled_material_dark:I = 0x7f06064d

.field public static final switch_thumb_disabled_material_light:I = 0x7f06064e

.field public static final switch_thumb_material_dark:I = 0x7f06064f

.field public static final switch_thumb_material_light:I = 0x7f060650

.field public static final switch_thumb_normal_material_dark:I = 0x7f060651

.field public static final switch_thumb_normal_material_light:I = 0x7f060652

.field public static final tooltip_background_dark:I = 0x7f060667

.field public static final tooltip_background_light:I = 0x7f060668


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

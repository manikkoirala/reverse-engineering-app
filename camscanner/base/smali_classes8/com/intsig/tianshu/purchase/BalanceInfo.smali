.class public Lcom/intsig/tianshu/purchase/BalanceInfo;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "BalanceInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tianshu/purchase/BalanceInfo$CropVipProperty;,
        Lcom/intsig/tianshu/purchase/BalanceInfo$UserDirInfo;,
        Lcom/intsig/tianshu/purchase/BalanceInfo$Invite;,
        Lcom/intsig/tianshu/purchase/BalanceInfo$PointsExchangeCfgrs;,
        Lcom/intsig/tianshu/purchase/BalanceInfo$PayGreetCardList;,
        Lcom/intsig/tianshu/purchase/BalanceInfo$TeamVipProperty;,
        Lcom/intsig/tianshu/purchase/BalanceInfo$VipLevelInfo;,
        Lcom/intsig/tianshu/purchase/BalanceInfo$LevelInfo;,
        Lcom/intsig/tianshu/purchase/BalanceInfo$PsnlVipProperty;
    }
.end annotation


# instance fields
.field public CamScanner_Counter:I

.field public CamScanner_Counter_nonvip_total:I

.field public CamScanner_Counter_vip_total:I

.field public CamScanner_DocEdit:I

.field public CamScanner_Erase:Ljava/lang/String;

.field public CamScanner_ExportOfficeExcel:I

.field public CamScanner_ExportOfficePpt:I

.field public CamScanner_ExportOfficeWord:I

.field public CamScanner_Intellect_Erase:I

.field public CamScanner_Intellect_Erase_Safe:I

.field public CamScanner_RoadMap:Ljava/lang/String;

.field public CamScanner_RoadMap_Excel:Ljava/lang/String;

.field public add_watermarks_balance:I

.field public balance_demoire:Ljava/lang/String;

.field public balance_recolor:Ljava/lang/String;

.field public capacity:Ljava/lang/String;

.field public cardocr_balance:Ljava/lang/String;

.field public cert_mode_balance:Ljava/lang/String;

.field public corp_vip_property:Ljava/lang/String;

.field public cs_license:I

.field public dir:Lcom/intsig/tianshu/purchase/BalanceInfo$UserDirInfo;

.field public excel_balance:I

.field public fax_balance:I

.field public greetcard_list:Lcom/intsig/tianshu/purchase/BalanceInfo$PayGreetCardList;

.field public imagerestore_balance:Ljava/lang/String;

.field public immt_expy_points:Ljava/lang/String;

.field public login_ocr_balance:Ljava/lang/String;

.field public lottery_chance:Ljava/lang/String;

.field public no_login_ocr_balance:Ljava/lang/String;

.field public ocr_balance:Ljava/lang/String;

.field public patting_balance:I

.field public pdf2excel_balance:I

.field public pdf2ppt_balance:I

.field public pdfword_balance:I

.field public points:Ljava/lang/String;

.field public points_exchange_cfgrs:Lcom/intsig/tianshu/purchase/BalanceInfo$PointsExchangeCfgrs;

.field public points_expiry:Ljava/lang/String;

.field public profile_card_balance:I

.field public psnl_vip_property:Lcom/intsig/tianshu/purchase/BalanceInfo$PsnlVipProperty;

.field public refresh_type_recolor:Ljava/lang/String;

.field public removead:I

.field public server_time:J

.field public team_vip_property:Lcom/intsig/tianshu/purchase/BalanceInfo$TeamVipProperty;

.field public trans_balance:Ljava/lang/String;

.field public upload_pdf_balance:I

.field public used_points:Ljava/lang/String;

.field public vip_balance_recolor:Ljava/lang/String;

.field public vip_imagerestore_balance:Ljava/lang/String;

.field public watchad_lottery_chance:Ljava/lang/String;

.field public watermarks_balance:I


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static getBalanceInfo(Ljava/lang/String;)Lcom/intsig/tianshu/purchase/BalanceInfo;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1
    const-string v0, "balance_info_list.json"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/tianshu/GreetCardUtil;->〇080(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    if-nez v0, :cond_2

    .line 13
    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    .line 20
    .line 21
    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    new-instance p0, Ljava/io/BufferedReader;

    .line 25
    .line 26
    new-instance v3, Ljava/io/InputStreamReader;

    .line 27
    .line 28
    invoke-direct {v3, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    .line 33
    .line 34
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    if-eqz v2, :cond_0

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string v3, "GreetCardUtil getGreetCardPurchaseState "

    .line 50
    .line 51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    invoke-static {v2}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    new-instance v2, Lorg/json/JSONObject;

    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    new-instance v0, Lcom/intsig/tianshu/purchase/BalanceInfo;

    .line 82
    .line 83
    invoke-direct {v0, v2}, Lcom/intsig/tianshu/purchase/BalanceInfo;-><init>(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 84
    .line 85
    .line 86
    invoke-virtual {p0}, Ljava/io/BufferedReader;->close()V

    .line 87
    .line 88
    .line 89
    move-object v1, v0

    .line 90
    goto :goto_3

    .line 91
    :catch_0
    move-exception v0

    .line 92
    goto :goto_1

    .line 93
    :catchall_0
    move-exception v0

    .line 94
    goto :goto_2

    .line 95
    :catch_1
    move-exception v0

    .line 96
    move-object p0, v1

    .line 97
    :goto_1
    :try_start_2
    const-string v2, "GreetCardUtil"

    .line 98
    .line 99
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 100
    .line 101
    .line 102
    if-eqz p0, :cond_3

    .line 103
    .line 104
    invoke-virtual {p0}, Ljava/io/BufferedReader;->close()V

    .line 105
    .line 106
    .line 107
    goto :goto_3

    .line 108
    :catchall_1
    move-exception v0

    .line 109
    move-object v1, p0

    .line 110
    :goto_2
    if-eqz v1, :cond_1

    .line 111
    .line 112
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 113
    .line 114
    .line 115
    :cond_1
    throw v0

    .line 116
    :cond_2
    const-string p0, "GreetCardUtil getPointExchangeCfgPath filename is empty"

    .line 117
    .line 118
    invoke-static {p0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    :cond_3
    :goto_3
    return-object v1
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public getPointById(Ljava/lang/String;)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tianshu/purchase/BalanceInfo;->points_exchange_cfgrs:Lcom/intsig/tianshu/purchase/BalanceInfo$PointsExchangeCfgrs;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/tianshu/base/BaseJsonObj;->toJSONObject()Lorg/json/JSONObject;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    .line 13
    .line 14
    .line 15
    move-result p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 16
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v1, "GreetCardUtilgetPointById  result "

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 34
    .line 35
    .line 36
    move v1, p1

    .line 37
    goto :goto_1

    .line 38
    :catch_0
    move-exception v0

    .line 39
    move v1, p1

    .line 40
    goto :goto_0

    .line 41
    :catch_1
    move-exception v0

    .line 42
    :goto_0
    const-string p1, "GreetCardUtil"

    .line 43
    .line 44
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 45
    .line 46
    .line 47
    :cond_0
    :goto_1
    return v1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

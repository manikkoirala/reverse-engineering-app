.class public Lcom/intsig/tianshu/purchase/BalanceInfo$PsnlVipProperty;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "BalanceInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tianshu/purchase/BalanceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PsnlVipProperty"
.end annotation


# static fields
.field public static final GP_SUBSCRIPTI_ON_ONHOLD:I = 0x1

.field public static final IS_NOT_TRIAL_GUIDE:I = 0x2

.field public static final IS_TRIAL_GUIDE:I = 0x1


# instance fields
.field public auto_renewal:Z

.field public expiry:J

.field public google_play:Ljava/lang/String;

.field public huaweipay:Ljava/lang/String;

.field public in_trial:I

.field public initial_tm:J

.field public is_trial_guide:I

.field public last_payment_method:Ljava/lang/String;

.field public level_info:Lcom/intsig/tianshu/purchase/BalanceInfo$LevelInfo;

.field public members_page:I

.field public nxt_renew_tm:J

.field public payway:I

.field public pending:I

.field public renew_method:Ljava/lang/String;

.field public renew_valid:I

.field public show_expired:I

.field public svip:I

.field public vip_level_info:Lcom/intsig/tianshu/purchase/BalanceInfo$VipLevelInfo;

.field public vip_type:Ljava/lang/String;

.field public wxpay_flag:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

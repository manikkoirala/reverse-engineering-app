.class public Lcom/intsig/tianshu/ApiCenterInfo;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "ApiCenterInfo.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# instance fields
.field public BAPI:Ljava/lang/String;

.field public BASEAPI:Ljava/lang/String;

.field public CDND_API:Ljava/lang/String;

.field public CDNSS_API:Ljava/lang/String;

.field public CDNS_API:Ljava/lang/String;

.field public DMAPI:Ljava/lang/String;

.field public EAPI:Ljava/lang/String;

.field public EDAPI:Ljava/lang/String;

.field public GID:Ljava/lang/String;

.field public IPV4_API:Ljava/lang/String;

.field public LOGAPI:Ljava/lang/String;

.field public MAPI:Ljava/lang/String;

.field public MAPI_OLD:Ljava/lang/String;

.field public MIGRATE:I

.field public OAPI:Ljava/lang/String;

.field public PAPI:Ljava/lang/String;

.field public PAPI_OLD:Ljava/lang/String;

.field public QRAPI:Ljava/lang/String;

.field public RAPI:Ljava/lang/String;

.field public SAPI:Ljava/lang/String;

.field public TAPI:Ljava/lang/String;

.field public TAPI_US:Ljava/lang/String;

.field public TMSG_API:Ljava/lang/String;

.field public TRC_API:Ljava/lang/String;

.field public UAPI:Ljava/lang/String;

.field public UAPI_CN:Ljava/lang/String;

.field public UAPI_OLD:Ljava/lang/String;

.field public UPPIC_API:Ljava/lang/String;

.field public WAPI:Ljava/lang/String;

.field public WEBAPI:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

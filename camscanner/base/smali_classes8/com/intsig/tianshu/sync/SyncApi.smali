.class public Lcom/intsig/tianshu/sync/SyncApi;
.super Ljava/lang/Object;
.source "SyncApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;
    }
.end annotation


# static fields
.field private static final 〇080:[B

.field private static final 〇o00〇〇Oo:[B

.field private static final 〇o〇:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    new-array v1, v0, [B

    .line 3
    .line 4
    sput-object v1, Lcom/intsig/tianshu/sync/SyncApi;->〇080:[B

    .line 5
    .line 6
    new-array v0, v0, [B

    .line 7
    .line 8
    sput-object v0, Lcom/intsig/tianshu/sync/SyncApi;->〇o00〇〇Oo:[B

    .line 9
    .line 10
    new-instance v0, Ljava/util/HashSet;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 13
    .line 14
    .line 15
    sput-object v0, Lcom/intsig/tianshu/sync/SyncApi;->〇o〇:Ljava/util/HashSet;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static synthetic O8([JLjava/util/List;Ljava/util/concurrent/ExecutorService;[FFLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;Lcom/intsig/tianshu/sync/SyncAdapter;Ljava/lang/String;[JLjava/lang/String;IIJLjava/lang/String;)V
    .locals 15

    move-object/from16 v9, p10

    .line 1
    :goto_0
    sget-object v0, Lcom/intsig/tianshu/sync/SyncApi;->〇o〇:Ljava/util/HashSet;

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v0, 0x64

    .line 2
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    const-string v0, "SyncApi"

    .line 3
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 5
    :cond_0
    sget-object v1, Lcom/intsig/tianshu/sync/SyncApi;->〇o00〇〇Oo:[B

    monitor-enter v1

    const/4 v2, 0x0

    .line 6
    :try_start_1
    aget-wide v3, p0, v2

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_1

    .line 7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    aput-wide v3, p0, v2

    .line 8
    :cond_1
    invoke-virtual {v0, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 9
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 10
    new-instance v0, LOO0o88/〇o00〇〇Oo;

    move-object v1, v0

    move-object/from16 v2, p3

    move/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move/from16 v6, p12

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p10

    move/from16 v10, p11

    move-wide/from16 v11, p13

    move-object/from16 v13, p15

    move-object/from16 v14, p9

    invoke-direct/range {v1 .. v14}, LOO0o88/〇o00〇〇Oo;-><init>([FFLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;ILcom/intsig/tianshu/sync/SyncAdapter;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;[J)V

    move-object/from16 v1, p2

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    move-object/from16 v1, p1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :catchall_0
    move-exception v0

    .line 11
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public static Oo08(Lcom/intsig/tianshu/sync/SyncAdapter;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;IIILjava/lang/String;I)Lcom/intsig/tianshu/sync/SyncState;
    .locals 36

    .line 1
    move-object/from16 v12, p0

    .line 2
    .line 3
    const/4 v13, 0x3

    .line 4
    new-array v14, v13, [F

    .line 5
    .line 6
    new-instance v15, Lcom/intsig/tianshu/sync/SyncState;

    .line 7
    .line 8
    invoke-interface/range {p0 .. p0}, Lcom/intsig/tianshu/sync/SyncAdapter;->O8()Lcom/intsig/tianshu/TSFolder;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/tianshu/TSFile;->〇080()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-direct {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/tianshu/sync/SyncApi$1;

    .line 22
    .line 23
    invoke-direct {v0}, Lcom/intsig/tianshu/sync/SyncApi$1;-><init>()V

    .line 24
    .line 25
    .line 26
    move-object v11, v0

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    move-object/from16 v11, p1

    .line 29
    .line 30
    :goto_0
    new-instance v10, Lcom/intsig/tianshu/sync/SyncApi$2;

    .line 31
    .line 32
    invoke-direct {v10, v14, v15, v11}, Lcom/intsig/tianshu/sync/SyncApi$2;-><init>([FLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;)V

    .line 33
    .line 34
    .line 35
    const/high16 v9, 0x40000000    # 2.0f

    .line 36
    .line 37
    invoke-virtual {v15, v9}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 38
    .line 39
    .line 40
    invoke-interface {v11, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 41
    .line 42
    .line 43
    invoke-interface/range {p0 .. p0}, Lcom/intsig/tianshu/sync/SyncAdapter;->O8()Lcom/intsig/tianshu/TSFolder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/intsig/tianshu/TSFile;->〇o00〇〇Oo()I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    invoke-virtual {v0}, Lcom/intsig/tianshu/TSFile;->〇080()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v8

    .line 55
    invoke-virtual {v15, v1}, Lcom/intsig/tianshu/sync/SyncState;->〇8o8o〇(I)V

    .line 56
    .line 57
    .line 58
    const/high16 v0, 0x40a00000    # 5.0f

    .line 59
    .line 60
    const-wide/16 v2, 0x3e8

    .line 61
    .line 62
    const-wide/16 v4, 0x64

    .line 63
    .line 64
    const/high16 v6, 0x41000000    # 8.0f

    .line 65
    .line 66
    move-object/from16 v17, v14

    .line 67
    .line 68
    const-wide/32 v13, 0x927c0

    .line 69
    .line 70
    .line 71
    move-object/from16 v18, v10

    .line 72
    .line 73
    const/4 v9, 0x1

    .line 74
    :try_start_0
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 75
    .line 76
    .line 77
    invoke-interface {v11, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 78
    .line 79
    .line 80
    if-ltz p2, :cond_1

    .line 81
    .line 82
    move/from16 v7, p2

    .line 83
    .line 84
    move-object/from16 v28, v8

    .line 85
    .line 86
    goto :goto_2

    .line 87
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 88
    .line 89
    .line 90
    move-result-wide v22
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_4

    .line 91
    :try_start_1
    invoke-static {v8}, Lcom/intsig/tianshu/TianShuAPI;->O〇〇(Ljava/lang/String;)Lcom/intsig/tianshu/TSFolder;

    .line 92
    .line 93
    .line 94
    move-result-object v0
    :try_end_1
    .catch Lcom/intsig/tianshu/exception/EurekaException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1 .. :try_end_1} :catch_4

    .line 95
    goto :goto_1

    .line 96
    :catch_0
    :try_start_2
    invoke-static {v8}, Lcom/intsig/tianshu/TianShuAPI;->O〇〇(Ljava/lang/String;)Lcom/intsig/tianshu/TSFolder;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    :goto_1
    sget-object v24, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 101
    .line 102
    invoke-virtual/range {v24 .. v24}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 103
    .line 104
    .line 105
    move-result-object v10

    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 107
    .line 108
    .line 109
    move-result-wide v26
    :try_end_2
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_2 .. :try_end_2} :catch_4

    .line 110
    move-object/from16 v28, v8

    .line 111
    .line 112
    sub-long v7, v26, v22

    .line 113
    .line 114
    :try_start_3
    invoke-virtual {v10, v7, v8}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇oo〇(J)V

    .line 115
    .line 116
    .line 117
    invoke-virtual/range {v24 .. v24}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 118
    .line 119
    .line 120
    move-result-object v7

    .line 121
    invoke-virtual {v7, v9}, Lcom/intsig/tianshu/sync/SyncTimeCount;->o〇O8〇〇o(I)V

    .line 122
    .line 123
    .line 124
    if-nez v0, :cond_2

    .line 125
    .line 126
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 127
    .line 128
    .line 129
    move-result-wide v7

    .line 130
    rem-long/2addr v7, v4
    :try_end_3
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_3 .. :try_end_3} :catch_3

    .line 131
    invoke-static {v7, v8}, Ljava/lang/Long;->signum(J)I

    .line 132
    .line 133
    .line 134
    mul-long v7, v7, v2

    .line 135
    .line 136
    add-long/2addr v7, v13

    .line 137
    :try_start_4
    invoke-virtual {v15, v7, v8}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 138
    .line 139
    .line 140
    return-object v15

    .line 141
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    .line 142
    .line 143
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .line 145
    .line 146
    const-string v8, "folder "

    .line 147
    .line 148
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0}, Lcom/intsig/tianshu/TSFile;->〇080()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v8

    .line 155
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    const-string v8, "\t"

    .line 159
    .line 160
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {v0}, Lcom/intsig/tianshu/TSFile;->〇o00〇〇Oo()I

    .line 164
    .line 165
    .line 166
    move-result v8

    .line 167
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v7

    .line 174
    invoke-static {v7}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v0}, Lcom/intsig/tianshu/TSFile;->〇o00〇〇Oo()I

    .line 178
    .line 179
    .line 180
    move-result v0
    :try_end_4
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_4 .. :try_end_4} :catch_3

    .line 181
    move v7, v0

    .line 182
    :goto_2
    :try_start_5
    invoke-virtual {v15, v6}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 183
    .line 184
    .line 185
    invoke-interface {v11, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 186
    .line 187
    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    .line 189
    .line 190
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_5
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_5 .. :try_end_5} :catch_2

    .line 191
    .line 192
    .line 193
    move-object/from16 v8, v28

    .line 194
    .line 195
    :try_start_6
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    const-string v10, ":server version\uff1a"

    .line 199
    .line 200
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    const-string v10, " <> local version:"

    .line 207
    .line 208
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .line 210
    .line 211
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object v0

    .line 218
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    if-ge v7, v1, :cond_4

    .line 222
    .line 223
    if-nez v7, :cond_3

    .line 224
    .line 225
    const/4 v0, 0x1

    .line 226
    goto :goto_3

    .line 227
    :cond_3
    invoke-interface {v12, v7}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇o〇(I)V
    :try_end_6
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_6 .. :try_end_6} :catch_1

    .line 228
    .line 229
    .line 230
    move v1, v7

    .line 231
    :cond_4
    const/4 v0, 0x0

    .line 232
    :goto_3
    move v10, v1

    .line 233
    const/4 v1, 0x0

    .line 234
    goto :goto_6

    .line 235
    :catch_1
    move-exception v0

    .line 236
    goto :goto_5

    .line 237
    :catch_2
    move-exception v0

    .line 238
    move-object/from16 v8, v28

    .line 239
    .line 240
    goto :goto_5

    .line 241
    :catch_3
    move-exception v0

    .line 242
    move-object/from16 v8, v28

    .line 243
    .line 244
    goto :goto_4

    .line 245
    :catch_4
    move-exception v0

    .line 246
    :goto_4
    const/4 v7, 0x0

    .line 247
    :goto_5
    const-string v10, "query folder:"

    .line 248
    .line 249
    invoke-static {v10, v0}, Lcom/intsig/tianshu/TianShuAPI;->〇〇00OO(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 250
    .line 251
    .line 252
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 253
    .line 254
    .line 255
    move-result v10

    .line 256
    const/16 v2, -0x69

    .line 257
    .line 258
    if-eq v10, v2, :cond_3e

    .line 259
    .line 260
    const/16 v2, -0x66

    .line 261
    .line 262
    if-ne v10, v2, :cond_5

    .line 263
    .line 264
    goto/16 :goto_32

    .line 265
    .line 266
    :cond_5
    const/16 v2, 0x12e

    .line 267
    .line 268
    if-ne v10, v2, :cond_3c

    .line 269
    .line 270
    move v10, v1

    .line 271
    const/4 v0, 0x1

    .line 272
    const/4 v1, 0x1

    .line 273
    :goto_6
    const-string v5, "TianShuAPI.batchUpload needTerminate"

    .line 274
    .line 275
    const/high16 v4, 0x42c80000    # 100.0f

    .line 276
    .line 277
    const-string v3, "CamScanner_Doc"

    .line 278
    .line 279
    const-string v2, "SyncApi"

    .line 280
    .line 281
    const/4 v6, 0x2

    .line 282
    if-eqz v0, :cond_10

    .line 283
    .line 284
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 285
    .line 286
    .line 287
    if-eqz v1, :cond_6

    .line 288
    .line 289
    const/high16 v0, 0x41100000    # 9.0f

    .line 290
    .line 291
    :try_start_7
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 292
    .line 293
    .line 294
    invoke-interface {v11, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V
    :try_end_7
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_7 .. :try_end_7} :catch_7

    .line 295
    .line 296
    .line 297
    :try_start_8
    invoke-static {v8}, Lcom/intsig/tianshu/TianShuAPI;->o0ooO(Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/intsig/tianshu/exception/EurekaException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_8 .. :try_end_8} :catch_7

    .line 298
    .line 299
    .line 300
    goto :goto_7

    .line 301
    :catch_5
    :try_start_9
    invoke-static {v8}, Lcom/intsig/tianshu/TianShuAPI;->o0ooO(Ljava/lang/String;)V

    .line 302
    .line 303
    .line 304
    :cond_6
    :goto_7
    const/high16 v0, 0x41400000    # 12.0f

    .line 305
    .line 306
    aput v0, v17, v9

    .line 307
    .line 308
    aput v4, v17, v6

    .line 309
    .line 310
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 311
    .line 312
    .line 313
    invoke-interface {v11, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 314
    .line 315
    .line 316
    add-int/2addr v7, v9

    .line 317
    const/4 v1, 0x0

    .line 318
    invoke-interface {v12, v8, v1, v7}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇o00〇〇Oo(Ljava/lang/String;II)Ljava/util/Vector;

    .line 319
    .line 320
    .line 321
    move-result-object v7

    .line 322
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    .line 323
    .line 324
    .line 325
    move-result v6

    .line 326
    if-lez v6, :cond_c

    .line 327
    .line 328
    const/high16 v0, 0x41800000    # 16.0f

    .line 329
    .line 330
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 331
    .line 332
    .line 333
    invoke-interface {v11, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 334
    .line 335
    .line 336
    const/high16 v0, 0x42a80000    # 84.0f

    .line 337
    .line 338
    int-to-float v1, v6

    .line 339
    div-float/2addr v0, v1

    .line 340
    const/4 v1, 0x0

    .line 341
    aput v0, v17, v1
    :try_end_9
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_9 .. :try_end_9} :catch_7

    .line 342
    .line 343
    const/4 v1, 0x0

    .line 344
    :goto_8
    if-ge v1, v6, :cond_c

    .line 345
    .line 346
    move-object v0, v8

    .line 347
    move/from16 v16, v1

    .line 348
    .line 349
    move v1, v10

    .line 350
    move-object v13, v2

    .line 351
    move-object v2, v7

    .line 352
    move-object v14, v3

    .line 353
    move/from16 v3, v16

    .line 354
    .line 355
    move-object/from16 v4, p5

    .line 356
    .line 357
    move-object/from16 v30, v5

    .line 358
    .line 359
    move-object/from16 v5, v18

    .line 360
    .line 361
    move v9, v6

    .line 362
    move/from16 v6, p6

    .line 363
    .line 364
    :try_start_a
    invoke-static/range {v0 .. v6}, Lcom/intsig/tianshu/TianShuAPI;->〇0〇O0088o(Ljava/lang/String;ILjava/util/Vector;ILjava/lang/String;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;I)Lcom/intsig/tianshu/UploadState;

    .line 365
    .line 366
    .line 367
    move-result-object v0
    :try_end_a
    .catch Lcom/intsig/tianshu/exception/EurekaException; {:try_start_a .. :try_end_a} :catch_6
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_a .. :try_end_a} :catch_7

    .line 368
    goto :goto_9

    .line 369
    :catch_6
    move-object v0, v8

    .line 370
    move v1, v10

    .line 371
    move-object v2, v7

    .line 372
    move/from16 v3, v16

    .line 373
    .line 374
    move-object/from16 v4, p5

    .line 375
    .line 376
    move-object/from16 v5, v18

    .line 377
    .line 378
    move/from16 v6, p6

    .line 379
    .line 380
    :try_start_b
    invoke-static/range {v0 .. v6}, Lcom/intsig/tianshu/TianShuAPI;->〇0〇O0088o(Ljava/lang/String;ILjava/util/Vector;ILjava/lang/String;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;I)Lcom/intsig/tianshu/UploadState;

    .line 381
    .line 382
    .line 383
    move-result-object v0

    .line 384
    :goto_9
    invoke-virtual {v0}, Lcom/intsig/tianshu/UploadState;->〇o00〇〇Oo()I

    .line 385
    .line 386
    .line 387
    move-result v1

    .line 388
    invoke-virtual {v0}, Lcom/intsig/tianshu/UploadState;->〇o〇()I

    .line 389
    .line 390
    .line 391
    move-result v10

    .line 392
    move/from16 v0, v16

    .line 393
    .line 394
    :goto_a
    if-ge v0, v9, :cond_9

    .line 395
    .line 396
    invoke-virtual {v7, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 397
    .line 398
    .line 399
    move-result-object v2

    .line 400
    check-cast v2, Lcom/intsig/tianshu/UploadAction;

    .line 401
    .line 402
    invoke-virtual {v2}, Lcom/intsig/tianshu/UploadAction;->oO80()I

    .line 403
    .line 404
    .line 405
    move-result v3

    .line 406
    if-gt v3, v10, :cond_8

    .line 407
    .line 408
    invoke-interface {v12, v2}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇080(Lcom/intsig/tianshu/UploadAction;)V

    .line 409
    .line 410
    .line 411
    invoke-static {v8, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 412
    .line 413
    .line 414
    move-result v2

    .line 415
    if-eqz v2, :cond_7

    .line 416
    .line 417
    sget-object v2, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 418
    .line 419
    invoke-virtual {v2}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 420
    .line 421
    .line 422
    move-result-object v2

    .line 423
    const/4 v3, 0x1

    .line 424
    invoke-virtual {v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇oOO8O8(I)V

    .line 425
    .line 426
    .line 427
    goto :goto_b

    .line 428
    :cond_7
    sget-object v2, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 429
    .line 430
    invoke-virtual {v2}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 431
    .line 432
    .line 433
    move-result-object v2

    .line 434
    const/4 v3, 0x1

    .line 435
    invoke-virtual {v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->oo〇(I)V

    .line 436
    .line 437
    .line 438
    :cond_8
    :goto_b
    iget v2, v15, Lcom/intsig/tianshu/sync/SyncState;->〇080:I

    .line 439
    .line 440
    const/4 v3, 0x1

    .line 441
    add-int/2addr v2, v3

    .line 442
    iput v2, v15, Lcom/intsig/tianshu/sync/SyncState;->〇080:I

    .line 443
    .line 444
    const/4 v2, 0x0

    .line 445
    aget v3, v17, v2

    .line 446
    .line 447
    invoke-virtual {v15, v3}, Lcom/intsig/tianshu/sync/SyncState;->〇080(F)V

    .line 448
    .line 449
    .line 450
    invoke-interface {v11, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 451
    .line 452
    .line 453
    add-int/lit8 v0, v0, 0x1

    .line 454
    .line 455
    goto :goto_a

    .line 456
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 457
    .line 458
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 459
    .line 460
    .line 461
    const-string v2, "upload all data to server :"

    .line 462
    .line 463
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 464
    .line 465
    .line 466
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 467
    .line 468
    .line 469
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 470
    .line 471
    .line 472
    move-result-object v0

    .line 473
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 474
    .line 475
    .line 476
    invoke-interface {v12, v10}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇80〇808〇O(I)V

    .line 477
    .line 478
    .line 479
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    .line 480
    .line 481
    .line 482
    move-result v0

    .line 483
    if-nez v0, :cond_b

    .line 484
    .line 485
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    .line 486
    .line 487
    .line 488
    move-result v0

    .line 489
    if-eqz v0, :cond_a

    .line 490
    .line 491
    goto :goto_c

    .line 492
    :cond_a
    move v6, v9

    .line 493
    move-object v2, v13

    .line 494
    move-object v3, v14

    .line 495
    move-object/from16 v5, v30

    .line 496
    .line 497
    const/high16 v4, 0x42c80000    # 100.0f

    .line 498
    .line 499
    const/4 v9, 0x1

    .line 500
    const-wide/32 v13, 0x927c0

    .line 501
    .line 502
    .line 503
    goto/16 :goto_8

    .line 504
    .line 505
    :cond_b
    :goto_c
    move-object/from16 v9, v30

    .line 506
    .line 507
    invoke-static {v13, v9}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_b .. :try_end_b} :catch_7

    .line 508
    .line 509
    .line 510
    :cond_c
    move-object v8, v11

    .line 511
    goto/16 :goto_31

    .line 512
    .line 513
    :catch_7
    move-exception v0

    .line 514
    const-string v1, "commit all:"

    .line 515
    .line 516
    invoke-static {v1, v0}, Lcom/intsig/tianshu/TianShuAPI;->〇〇00OO(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 517
    .line 518
    .line 519
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 520
    .line 521
    .line 522
    move-result v1

    .line 523
    const/16 v5, -0x69

    .line 524
    .line 525
    if-eq v1, v5, :cond_f

    .line 526
    .line 527
    const/16 v4, -0x66

    .line 528
    .line 529
    if-ne v1, v4, :cond_d

    .line 530
    .line 531
    goto :goto_d

    .line 532
    :cond_d
    const/16 v2, -0x130

    .line 533
    .line 534
    if-ne v1, v2, :cond_e

    .line 535
    .line 536
    const-wide/32 v2, 0x927c0

    .line 537
    .line 538
    .line 539
    invoke-virtual {v15, v2, v3}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 540
    .line 541
    .line 542
    return-object v15

    .line 543
    :cond_e
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getMessage()Ljava/lang/String;

    .line 544
    .line 545
    .line 546
    move-result-object v0

    .line 547
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->oO80(Ljava/lang/String;)V

    .line 548
    .line 549
    .line 550
    invoke-virtual {v15, v1}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 551
    .line 552
    .line 553
    return-object v15

    .line 554
    :cond_f
    :goto_d
    invoke-virtual {v15, v1}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 555
    .line 556
    .line 557
    const-wide/16 v1, 0x0

    .line 558
    .line 559
    invoke-virtual {v15, v1, v2}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 560
    .line 561
    .line 562
    return-object v15

    .line 563
    :cond_10
    move-object v13, v2

    .line 564
    move-object v14, v3

    .line 565
    move-object v9, v5

    .line 566
    const/high16 v1, 0x41000000    # 8.0f

    .line 567
    .line 568
    const/4 v2, 0x1

    .line 569
    const/16 v4, -0x66

    .line 570
    .line 571
    const/16 v5, -0x69

    .line 572
    .line 573
    aput v1, v17, v2

    .line 574
    .line 575
    if-gez p3, :cond_11

    .line 576
    .line 577
    sub-int v0, v7, v10

    .line 578
    .line 579
    goto :goto_e

    .line 580
    :cond_11
    move/from16 v0, p3

    .line 581
    .line 582
    :goto_e
    const/4 v1, 0x0

    .line 583
    invoke-interface {v12, v1}, Lcom/intsig/tianshu/sync/SyncAdapter;->OO0o〇〇(Z)I

    .line 584
    .line 585
    .line 586
    move-result v3

    .line 587
    invoke-interface {v12, v2}, Lcom/intsig/tianshu/sync/SyncAdapter;->OO0o〇〇(Z)I

    .line 588
    .line 589
    .line 590
    move-result v1

    .line 591
    add-int/2addr v3, v1

    .line 592
    add-int v1, v0, v3

    .line 593
    .line 594
    int-to-float v1, v1

    .line 595
    const/high16 v2, 0x3f800000    # 1.0f

    .line 596
    .line 597
    cmpg-float v19, v1, v2

    .line 598
    .line 599
    if-gez v19, :cond_12

    .line 600
    .line 601
    const/high16 v1, 0x3f800000    # 1.0f

    .line 602
    .line 603
    :cond_12
    int-to-float v0, v0

    .line 604
    div-float/2addr v0, v1

    .line 605
    const/16 v2, 0x5a

    .line 606
    .line 607
    int-to-float v2, v2

    .line 608
    mul-float v0, v0, v2

    .line 609
    .line 610
    const/high16 v19, 0x41000000    # 8.0f

    .line 611
    .line 612
    add-float v6, v0, v19

    .line 613
    .line 614
    int-to-float v0, v3

    .line 615
    div-float/2addr v0, v1

    .line 616
    mul-float v0, v0, v2

    .line 617
    .line 618
    add-float/2addr v0, v6

    .line 619
    const/high16 v19, 0x40000000    # 2.0f

    .line 620
    .line 621
    add-float v0, v0, v19

    .line 622
    .line 623
    const/16 v22, 0x2

    .line 624
    .line 625
    aput v0, v17, v22

    .line 626
    .line 627
    div-float/2addr v2, v1

    .line 628
    const/16 v23, 0x0

    .line 629
    .line 630
    aput v2, v17, v23

    .line 631
    .line 632
    new-instance v24, Ljava/util/ArrayList;

    .line 633
    .line 634
    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 635
    .line 636
    .line 637
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->OO0o〇〇()Ljava/util/concurrent/ExecutorService;

    .line 638
    .line 639
    .line 640
    move-result-object v0

    .line 641
    sget-object v1, Lcom/intsig/tianshu/sync/SyncApi;->〇o〇:Ljava/util/HashSet;

    .line 642
    .line 643
    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 644
    .line 645
    .line 646
    const/4 v3, 0x1

    .line 647
    new-array v2, v3, [J

    .line 648
    .line 649
    const-wide/16 v20, 0x0

    .line 650
    .line 651
    aput-wide v20, v2, v23

    .line 652
    .line 653
    new-array v1, v3, [J

    .line 654
    .line 655
    aput-wide v20, v1, v23

    .line 656
    .line 657
    new-instance v25, LOO0o88/〇080;

    .line 658
    .line 659
    move-object/from16 v28, v1

    .line 660
    .line 661
    move-object/from16 v1, v25

    .line 662
    .line 663
    move-object/from16 v29, v2

    .line 664
    .line 665
    const/16 v30, 0x1

    .line 666
    .line 667
    move-object/from16 v3, v24

    .line 668
    .line 669
    const/16 v31, -0x66

    .line 670
    .line 671
    move-object v4, v0

    .line 672
    const/16 v32, -0x69

    .line 673
    .line 674
    move-object/from16 v5, v17

    .line 675
    .line 676
    move/from16 p1, v6

    .line 677
    .line 678
    move-object/from16 v22, v13

    .line 679
    .line 680
    move v13, v7

    .line 681
    move-object v7, v15

    .line 682
    move-object/from16 p2, v8

    .line 683
    .line 684
    move-object v8, v11

    .line 685
    move-object/from16 v33, v9

    .line 686
    .line 687
    move-object/from16 v20, v14

    .line 688
    .line 689
    const/4 v14, 0x1

    .line 690
    move-object/from16 v9, p0

    .line 691
    .line 692
    move v14, v10

    .line 693
    move-object/from16 v21, v18

    .line 694
    .line 695
    const/16 v18, 0x0

    .line 696
    .line 697
    move-object/from16 v10, p2

    .line 698
    .line 699
    move-object/from16 v34, v11

    .line 700
    .line 701
    move-object/from16 v11, v28

    .line 702
    .line 703
    invoke-direct/range {v1 .. v11}, LOO0o88/〇080;-><init>([JLjava/util/List;Ljava/util/concurrent/ExecutorService;[FFLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;Lcom/intsig/tianshu/sync/SyncAdapter;Ljava/lang/String;[J)V

    .line 704
    .line 705
    .line 706
    instance-of v1, v12, Lcom/intsig/tianshu/sync/SyncStepInterface;

    .line 707
    .line 708
    if-eqz v1, :cond_13

    .line 709
    .line 710
    move-object v0, v12

    .line 711
    check-cast v0, Lcom/intsig/tianshu/sync/SyncStepInterface;

    .line 712
    .line 713
    invoke-interface {v0}, Lcom/intsig/tianshu/sync/SyncStepInterface;->〇O8o08O()V

    .line 714
    .line 715
    .line 716
    :cond_13
    invoke-interface/range {p0 .. p0}, Lcom/intsig/tianshu/sync/SyncAdapter;->Oooo8o0〇()Z

    .line 717
    .line 718
    .line 719
    move-result v0

    .line 720
    if-eqz v0, :cond_23

    .line 721
    .line 722
    :try_start_c
    new-instance v0, Ljava/lang/StringBuilder;

    .line 723
    .line 724
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 725
    .line 726
    .line 727
    const-string v2, ">>\u6279\u91cf\u66f4\u65b0 from: local: "

    .line 728
    .line 729
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 730
    .line 731
    .line 732
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 733
    .line 734
    .line 735
    const-string v2, "> server: "

    .line 736
    .line 737
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 738
    .line 739
    .line 740
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 741
    .line 742
    .line 743
    const-string v2, " begin"

    .line 744
    .line 745
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 746
    .line 747
    .line 748
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 749
    .line 750
    .line 751
    move-result-object v0

    .line 752
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 753
    .line 754
    .line 755
    const/4 v2, 0x1

    .line 756
    aget v0, v17, v2

    .line 757
    .line 758
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V
    :try_end_c
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_c .. :try_end_c} :catch_13

    .line 759
    .line 760
    .line 761
    move-object/from16 v8, v34

    .line 762
    .line 763
    :try_start_d
    invoke-interface {v8, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V
    :try_end_d
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_d .. :try_end_d} :catch_12

    .line 764
    .line 765
    .line 766
    if-ge v14, v13, :cond_14

    .line 767
    .line 768
    :try_start_e
    invoke-interface/range {p0 .. p0}, Lcom/intsig/tianshu/sync/SyncAdapter;->OO0o〇〇〇〇0()V
    :try_end_e
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_e .. :try_end_e} :catch_8

    .line 769
    .line 770
    .line 771
    goto :goto_12

    .line 772
    :catch_8
    move-exception v0

    .line 773
    move-object/from16 v7, p2

    .line 774
    .line 775
    move v10, v14

    .line 776
    :goto_f
    move-object/from16 v9, v20

    .line 777
    .line 778
    :goto_10
    move-object/from16 v11, v22

    .line 779
    .line 780
    :goto_11
    const-wide/16 v5, 0x0

    .line 781
    .line 782
    goto/16 :goto_1e

    .line 783
    .line 784
    :cond_14
    :goto_12
    move v10, v14

    .line 785
    :goto_13
    if-ge v10, v13, :cond_1c

    .line 786
    .line 787
    :try_start_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 788
    .line 789
    .line 790
    move-result-wide v31
    :try_end_f
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_f .. :try_end_f} :catch_e

    .line 791
    const/4 v5, -0x1

    .line 792
    const/4 v6, 0x0

    .line 793
    const/4 v7, 0x0

    .line 794
    move-object/from16 v2, p2

    .line 795
    .line 796
    move v3, v10

    .line 797
    move-object/from16 v4, v25

    .line 798
    .line 799
    :try_start_10
    invoke-static/range {v2 .. v7}, Lcom/intsig/tianshu/TianShuAPI;->〇O00(Ljava/lang/String;ILcom/intsig/tianshu/TianShuAPI$OnUpdateListener;IZLcom/intsig/tianshu/TianShuAPI$OnProgressListener;)I
    :try_end_10
    .catch Lcom/intsig/tianshu/exception/EurekaException; {:try_start_10 .. :try_end_10} :catch_9
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_10 .. :try_end_10} :catch_e

    .line 800
    .line 801
    .line 802
    goto :goto_14

    .line 803
    :catch_9
    const/4 v5, -0x1

    .line 804
    const/4 v6, 0x0

    .line 805
    const/4 v7, 0x0

    .line 806
    move-object/from16 v2, p2

    .line 807
    .line 808
    move v3, v10

    .line 809
    move-object/from16 v4, v25

    .line 810
    .line 811
    :try_start_11
    invoke-static/range {v2 .. v7}, Lcom/intsig/tianshu/TianShuAPI;->〇O00(Ljava/lang/String;ILcom/intsig/tianshu/TianShuAPI$OnUpdateListener;IZLcom/intsig/tianshu/TianShuAPI$OnProgressListener;)I
    :try_end_11
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_11 .. :try_end_11} :catch_e

    .line 812
    .line 813
    .line 814
    :goto_14
    move-object/from16 v7, p2

    .line 815
    .line 816
    move-object/from16 v9, v20

    .line 817
    .line 818
    :try_start_12
    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 819
    .line 820
    .line 821
    move-result v0

    .line 822
    if-eqz v0, :cond_15

    .line 823
    .line 824
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 825
    .line 826
    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 827
    .line 828
    .line 829
    move-result-object v0

    .line 830
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 831
    .line 832
    .line 833
    move-result-wide v2

    .line 834
    sub-long v2, v2, v31

    .line 835
    .line 836
    invoke-virtual {v0, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->o〇0(J)V

    .line 837
    .line 838
    .line 839
    goto :goto_15

    .line 840
    :cond_15
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 841
    .line 842
    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 843
    .line 844
    .line 845
    move-result-object v0

    .line 846
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 847
    .line 848
    .line 849
    move-result-wide v2

    .line 850
    sub-long v2, v2, v31

    .line 851
    .line 852
    invoke-virtual {v0, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇〇888(J)V

    .line 853
    .line 854
    .line 855
    :goto_15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 856
    .line 857
    .line 858
    move-result-wide v2

    .line 859
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 860
    .line 861
    .line 862
    move-result-object v4

    .line 863
    const/4 v5, 0x0

    .line 864
    :goto_16
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 865
    .line 866
    .line 867
    move-result v0

    .line 868
    if-eqz v0, :cond_17

    .line 869
    .line 870
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 871
    .line 872
    .line 873
    move-result-object v0

    .line 874
    check-cast v0, Ljava/util/concurrent/Future;
    :try_end_12
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_12 .. :try_end_12} :catch_d

    .line 875
    .line 876
    :try_start_13
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    .line 877
    .line 878
    .line 879
    move-result-object v0

    .line 880
    check-cast v0, Ljava/lang/Integer;

    .line 881
    .line 882
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 883
    .line 884
    .line 885
    move-result v0

    .line 886
    invoke-virtual {v15}, Lcom/intsig/tianshu/sync/SyncState;->〇o〇()I

    .line 887
    .line 888
    .line 889
    move-result v6

    .line 890
    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    .line 891
    .line 892
    .line 893
    move-result v0

    .line 894
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->OO0o〇〇〇〇0(I)V

    .line 895
    .line 896
    .line 897
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 898
    .line 899
    .line 900
    move-result-wide v31

    .line 901
    sub-long v31, v31, v2

    .line 902
    .line 903
    const-wide/16 v34, 0x7d0

    .line 904
    .line 905
    cmp-long v0, v31, v34

    .line 906
    .line 907
    if-lez v0, :cond_16

    .line 908
    .line 909
    invoke-virtual {v15}, Lcom/intsig/tianshu/sync/SyncState;->〇o〇()I

    .line 910
    .line 911
    .line 912
    move-result v0

    .line 913
    invoke-interface {v12, v0}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇80〇808〇O(I)V

    .line 914
    .line 915
    .line 916
    invoke-virtual {v15}, Lcom/intsig/tianshu/sync/SyncState;->〇o〇()I

    .line 917
    .line 918
    .line 919
    move-result v5

    .line 920
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 921
    .line 922
    .line 923
    move-result-wide v2
    :try_end_13
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_13 .. :try_end_13} :catch_b
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_13} :catch_a
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_13 .. :try_end_13} :catch_d

    .line 924
    :cond_16
    move-object/from16 v11, v22

    .line 925
    .line 926
    goto :goto_17

    .line 927
    :catch_a
    move-exception v0

    .line 928
    move-object/from16 v11, v22

    .line 929
    .line 930
    :try_start_14
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 931
    .line 932
    .line 933
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 934
    .line 935
    .line 936
    move-result-object v0

    .line 937
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 938
    .line 939
    .line 940
    goto :goto_17

    .line 941
    :catch_b
    move-exception v0

    .line 942
    move-object/from16 v11, v22

    .line 943
    .line 944
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 945
    .line 946
    .line 947
    :goto_17
    move-object/from16 v22, v11

    .line 948
    .line 949
    goto :goto_16

    .line 950
    :cond_17
    move-object/from16 v11, v22

    .line 951
    .line 952
    invoke-virtual {v15}, Lcom/intsig/tianshu/sync/SyncState;->〇o〇()I

    .line 953
    .line 954
    .line 955
    move-result v2
    :try_end_14
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_14 .. :try_end_14} :catch_11

    .line 956
    if-eq v5, v2, :cond_18

    .line 957
    .line 958
    :try_start_15
    invoke-interface {v12, v2}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇80〇808〇O(I)V

    .line 959
    .line 960
    .line 961
    goto :goto_18

    .line 962
    :catch_c
    move-exception v0

    .line 963
    move v10, v2

    .line 964
    goto/16 :goto_11

    .line 965
    .line 966
    :cond_18
    :goto_18
    if-ne v2, v10, :cond_19

    .line 967
    .line 968
    const/16 v0, -0x12f

    .line 969
    .line 970
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 971
    .line 972
    .line 973
    new-instance v0, Ljava/lang/StringBuilder;

    .line 974
    .line 975
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 976
    .line 977
    .line 978
    const-string v3, "sync_error folder\uff1a"

    .line 979
    .line 980
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 981
    .line 982
    .line 983
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 984
    .line 985
    .line 986
    const-string v3, "  revision == old\uff0c revision\uff1a"

    .line 987
    .line 988
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 989
    .line 990
    .line 991
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 992
    .line 993
    .line 994
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 995
    .line 996
    .line 997
    move-result-object v0

    .line 998
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 999
    .line 1000
    .line 1001
    return-object v15

    .line 1002
    :cond_19
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    .line 1003
    .line 1004
    .line 1005
    move-result v0

    .line 1006
    if-nez v0, :cond_1b

    .line 1007
    .line 1008
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    .line 1009
    .line 1010
    .line 1011
    move-result v0

    .line 1012
    if-eqz v0, :cond_1a

    .line 1013
    .line 1014
    sget-object v0, Lcom/intsig/tianshu/SyncExecuteState;->〇o00〇〇Oo:Lcom/intsig/tianshu/SyncExecuteState$Companion;

    .line 1015
    .line 1016
    invoke-virtual {v0}, Lcom/intsig/tianshu/SyncExecuteState$Companion;->〇080()Lcom/intsig/tianshu/SyncExecuteState;

    .line 1017
    .line 1018
    .line 1019
    move-result-object v0

    .line 1020
    invoke-virtual {v0}, Lcom/intsig/tianshu/SyncExecuteState;->〇080()Z

    .line 1021
    .line 1022
    .line 1023
    move-result v0

    .line 1024
    if-eqz v0, :cond_1a

    .line 1025
    .line 1026
    goto :goto_19

    .line 1027
    :cond_1a
    move v10, v2

    .line 1028
    move-object/from16 p2, v7

    .line 1029
    .line 1030
    move-object/from16 v20, v9

    .line 1031
    .line 1032
    move-object/from16 v22, v11

    .line 1033
    .line 1034
    goto/16 :goto_13

    .line 1035
    .line 1036
    :cond_1b
    :goto_19
    const-string v0, "TianShuAPI.batchUpdate needTerminate"

    .line 1037
    .line 1038
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_15 .. :try_end_15} :catch_c

    .line 1039
    .line 1040
    .line 1041
    move v10, v2

    .line 1042
    goto :goto_1a

    .line 1043
    :catch_d
    move-exception v0

    .line 1044
    goto/16 :goto_10

    .line 1045
    .line 1046
    :catch_e
    move-exception v0

    .line 1047
    move-object/from16 v7, p2

    .line 1048
    .line 1049
    goto/16 :goto_f

    .line 1050
    .line 1051
    :cond_1c
    move-object/from16 v7, p2

    .line 1052
    .line 1053
    move-object/from16 v9, v20

    .line 1054
    .line 1055
    move-object/from16 v11, v22

    .line 1056
    .line 1057
    :goto_1a
    :try_start_16
    aget-wide v2, v29, v18
    :try_end_16
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_16 .. :try_end_16} :catch_11

    .line 1058
    .line 1059
    const-wide/16 v5, 0x0

    .line 1060
    .line 1061
    cmp-long v0, v2, v5

    .line 1062
    .line 1063
    if-lez v0, :cond_1d

    .line 1064
    .line 1065
    :try_start_17
    aget-wide v22, v28, v18

    .line 1066
    .line 1067
    cmp-long v0, v22, v5

    .line 1068
    .line 1069
    if-lez v0, :cond_1d

    .line 1070
    .line 1071
    sub-long v2, v22, v2

    .line 1072
    .line 1073
    goto :goto_1b

    .line 1074
    :catch_f
    move-exception v0

    .line 1075
    goto :goto_1e

    .line 1076
    :cond_1d
    move-wide v2, v5

    .line 1077
    :goto_1b
    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 1078
    .line 1079
    .line 1080
    move-result v0

    .line 1081
    if-eqz v0, :cond_1e

    .line 1082
    .line 1083
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 1084
    .line 1085
    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 1086
    .line 1087
    .line 1088
    move-result-object v0

    .line 1089
    invoke-virtual {v0, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->o800o8O(J)V

    .line 1090
    .line 1091
    .line 1092
    goto :goto_1c

    .line 1093
    :cond_1e
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 1094
    .line 1095
    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 1096
    .line 1097
    .line 1098
    move-result-object v0

    .line 1099
    invoke-virtual {v0, v2, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇O888o0o(J)V

    .line 1100
    .line 1101
    .line 1102
    :goto_1c
    invoke-interface/range {p0 .. p0}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇〇888()V

    .line 1103
    .line 1104
    .line 1105
    new-instance v0, Ljava/lang/StringBuilder;

    .line 1106
    .line 1107
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1108
    .line 1109
    .line 1110
    const-string v2, "<<\u6279\u91cf\u66f4\u65b0 to: "

    .line 1111
    .line 1112
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1113
    .line 1114
    .line 1115
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1116
    .line 1117
    .line 1118
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1119
    .line 1120
    .line 1121
    move-result-object v0

    .line 1122
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 1123
    .line 1124
    .line 1125
    invoke-interface {v12, v10}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇80〇808〇O(I)V
    :try_end_17
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_17 .. :try_end_17} :catch_f

    .line 1126
    .line 1127
    .line 1128
    move/from16 v2, p1

    .line 1129
    .line 1130
    const/4 v3, 0x1

    .line 1131
    :try_start_18
    aput v2, v17, v3

    .line 1132
    .line 1133
    invoke-virtual {v15, v2}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 1134
    .line 1135
    .line 1136
    invoke-interface {v8, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V
    :try_end_18
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_18 .. :try_end_18} :catch_10

    .line 1137
    .line 1138
    .line 1139
    move v13, v10

    .line 1140
    const/16 v4, -0x69

    .line 1141
    .line 1142
    goto/16 :goto_1f

    .line 1143
    .line 1144
    :catch_10
    move-exception v0

    .line 1145
    move v13, v10

    .line 1146
    goto :goto_1e

    .line 1147
    :catch_11
    move-exception v0

    .line 1148
    goto/16 :goto_11

    .line 1149
    .line 1150
    :catch_12
    move-exception v0

    .line 1151
    move-object/from16 v7, p2

    .line 1152
    .line 1153
    move-object/from16 v9, v20

    .line 1154
    .line 1155
    move-object/from16 v11, v22

    .line 1156
    .line 1157
    goto :goto_1d

    .line 1158
    :catch_13
    move-exception v0

    .line 1159
    move-object/from16 v7, p2

    .line 1160
    .line 1161
    move-object/from16 v9, v20

    .line 1162
    .line 1163
    move-object/from16 v11, v22

    .line 1164
    .line 1165
    move-object/from16 v8, v34

    .line 1166
    .line 1167
    :goto_1d
    const-wide/16 v5, 0x0

    .line 1168
    .line 1169
    move v10, v14

    .line 1170
    :goto_1e
    const-string v2, "batchUpdate:"

    .line 1171
    .line 1172
    invoke-static {v2, v0}, Lcom/intsig/tianshu/TianShuAPI;->〇〇00OO(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1173
    .line 1174
    .line 1175
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 1176
    .line 1177
    .line 1178
    move-result v2

    .line 1179
    const/16 v4, -0x69

    .line 1180
    .line 1181
    if-ne v2, v4, :cond_1f

    .line 1182
    .line 1183
    invoke-virtual {v15, v2}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 1184
    .line 1185
    .line 1186
    invoke-virtual {v15, v5, v6}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1187
    .line 1188
    .line 1189
    return-object v15

    .line 1190
    :cond_1f
    const/16 v3, 0x15e

    .line 1191
    .line 1192
    if-ne v2, v3, :cond_20

    .line 1193
    .line 1194
    const-string v0, "No updates"

    .line 1195
    .line 1196
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->〇0OO8(Ljava/lang/String;)V

    .line 1197
    .line 1198
    .line 1199
    goto :goto_1f

    .line 1200
    :cond_20
    const/16 v3, -0x130

    .line 1201
    .line 1202
    if-ne v2, v3, :cond_21

    .line 1203
    .line 1204
    const-wide/32 v1, 0x927c0

    .line 1205
    .line 1206
    .line 1207
    invoke-virtual {v15, v1, v2}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1208
    .line 1209
    .line 1210
    return-object v15

    .line 1211
    :cond_21
    invoke-virtual {v15}, Lcom/intsig/tianshu/sync/SyncState;->〇o〇()I

    .line 1212
    .line 1213
    .line 1214
    move-result v1

    .line 1215
    if-le v1, v10, :cond_22

    .line 1216
    .line 1217
    invoke-interface {v12, v1}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇80〇808〇O(I)V

    .line 1218
    .line 1219
    .line 1220
    :cond_22
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1221
    .line 1222
    .line 1223
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getMessage()Ljava/lang/String;

    .line 1224
    .line 1225
    .line 1226
    move-result-object v1

    .line 1227
    invoke-virtual {v15, v1}, Lcom/intsig/tianshu/sync/SyncState;->oO80(Ljava/lang/String;)V

    .line 1228
    .line 1229
    .line 1230
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 1231
    .line 1232
    .line 1233
    move-result v0

    .line 1234
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 1235
    .line 1236
    .line 1237
    return-object v15

    .line 1238
    :cond_23
    move-object/from16 v7, p2

    .line 1239
    .line 1240
    move-object/from16 v9, v20

    .line 1241
    .line 1242
    move-object/from16 v11, v22

    .line 1243
    .line 1244
    move-object/from16 v8, v34

    .line 1245
    .line 1246
    const/16 v4, -0x69

    .line 1247
    .line 1248
    const-wide/16 v5, 0x0

    .line 1249
    .line 1250
    invoke-interface {v12, v13}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇80〇808〇O(I)V

    .line 1251
    .line 1252
    .line 1253
    move v10, v14

    .line 1254
    :goto_1f
    if-eqz v1, :cond_24

    .line 1255
    .line 1256
    move-object v0, v12

    .line 1257
    check-cast v0, Lcom/intsig/tianshu/sync/SyncStepInterface;

    .line 1258
    .line 1259
    invoke-interface {v0}, Lcom/intsig/tianshu/sync/SyncStepInterface;->o〇0()V

    .line 1260
    .line 1261
    .line 1262
    :cond_24
    const-string v0, ">>\u63d0\u4ea4\u672c\u5730\u6570\u636e"

    .line 1263
    .line 1264
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 1265
    .line 1266
    .line 1267
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 1268
    .line 1269
    .line 1270
    const/4 v1, 0x1

    .line 1271
    add-int/2addr v13, v1

    .line 1272
    move/from16 v2, p4

    .line 1273
    .line 1274
    invoke-interface {v12, v7, v2, v13}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇o00〇〇Oo(Ljava/lang/String;II)Ljava/util/Vector;

    .line 1275
    .line 1276
    .line 1277
    move-result-object v13

    .line 1278
    aget v0, v17, v1

    .line 1279
    .line 1280
    add-float v0, v0, v19

    .line 1281
    .line 1282
    aput v0, v17, v1

    .line 1283
    .line 1284
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 1285
    .line 1286
    .line 1287
    invoke-interface {v8, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 1288
    .line 1289
    .line 1290
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    .line 1291
    .line 1292
    .line 1293
    move-result v14

    .line 1294
    new-instance v0, Ljava/lang/StringBuilder;

    .line 1295
    .line 1296
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1297
    .line 1298
    .line 1299
    const-string v1, "<<listFile \u5b8c\u6bd5  $$$$$$$$$$$$$$$$$$$$$\u5373\u5c06\u63d0\u4ea4 "

    .line 1300
    .line 1301
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1302
    .line 1303
    .line 1304
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1305
    .line 1306
    .line 1307
    const-string v1, " \u4e2a\u6570\u636e"

    .line 1308
    .line 1309
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1310
    .line 1311
    .line 1312
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1313
    .line 1314
    .line 1315
    move-result-object v0

    .line 1316
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 1317
    .line 1318
    .line 1319
    if-lez v14, :cond_3b

    .line 1320
    .line 1321
    const/4 v3, 0x2

    .line 1322
    :try_start_19
    aget v0, v17, v3

    .line 1323
    .line 1324
    const/4 v1, 0x1

    .line 1325
    aget v2, v17, v1

    .line 1326
    .line 1327
    sub-float/2addr v0, v2

    .line 1328
    int-to-float v1, v14

    .line 1329
    div-float/2addr v0, v1

    .line 1330
    aput v0, v17, v18

    .line 1331
    .line 1332
    move/from16 v19, v10

    .line 1333
    .line 1334
    const/4 v10, 0x0

    .line 1335
    :goto_20
    if-ge v10, v14, :cond_38

    .line 1336
    .line 1337
    new-instance v0, Ljava/lang/StringBuilder;

    .line 1338
    .line 1339
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1340
    .line 1341
    .line 1342
    const-string v1, ">>$$$$$$$$$$$$$$$$$$$$$\u63d0\u4ea4\u6570\u636e from "

    .line 1343
    .line 1344
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1345
    .line 1346
    .line 1347
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1348
    .line 1349
    .line 1350
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1351
    .line 1352
    .line 1353
    move-result-object v0

    .line 1354
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V
    :try_end_19
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_19 .. :try_end_19} :catch_1a

    .line 1355
    .line 1356
    .line 1357
    move-object v0, v7

    .line 1358
    move/from16 v1, v19

    .line 1359
    .line 1360
    move-object v2, v13

    .line 1361
    move/from16 p1, v14

    .line 1362
    .line 1363
    const/4 v14, 0x2

    .line 1364
    move v3, v10

    .line 1365
    move-object/from16 v4, p5

    .line 1366
    .line 1367
    move-object/from16 v5, v21

    .line 1368
    .line 1369
    move/from16 v6, p6

    .line 1370
    .line 1371
    :try_start_1a
    invoke-static/range {v0 .. v6}, Lcom/intsig/tianshu/TianShuAPI;->〇0〇O0088o(Ljava/lang/String;ILjava/util/Vector;ILjava/lang/String;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;I)Lcom/intsig/tianshu/UploadState;

    .line 1372
    .line 1373
    .line 1374
    move-result-object v0
    :try_end_1a
    .catch Lcom/intsig/tianshu/exception/EurekaException; {:try_start_1a .. :try_end_1a} :catch_16
    .catch Lcom/intsig/tianshu/exception/SSLTSException; {:try_start_1a .. :try_end_1a} :catch_15
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1a .. :try_end_1a} :catch_14

    .line 1375
    goto :goto_21

    .line 1376
    :catch_14
    move-exception v0

    .line 1377
    const-wide/16 v6, 0x0

    .line 1378
    .line 1379
    goto/16 :goto_30

    .line 1380
    .line 1381
    :catch_15
    move v1, v10

    .line 1382
    goto :goto_23

    .line 1383
    :catch_16
    move-object v0, v7

    .line 1384
    move/from16 v1, v19

    .line 1385
    .line 1386
    move-object v2, v13

    .line 1387
    move v3, v10

    .line 1388
    move-object/from16 v4, p5

    .line 1389
    .line 1390
    move-object/from16 v5, v21

    .line 1391
    .line 1392
    move/from16 v6, p6

    .line 1393
    .line 1394
    :try_start_1b
    invoke-static/range {v0 .. v6}, Lcom/intsig/tianshu/TianShuAPI;->〇0〇O0088o(Ljava/lang/String;ILjava/util/Vector;ILjava/lang/String;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;I)Lcom/intsig/tianshu/UploadState;

    .line 1395
    .line 1396
    .line 1397
    move-result-object v0

    .line 1398
    :goto_21
    invoke-virtual {v0}, Lcom/intsig/tianshu/UploadState;->〇o〇()I

    .line 1399
    .line 1400
    .line 1401
    move-result v1

    .line 1402
    new-instance v2, Ljava/lang/StringBuilder;

    .line 1403
    .line 1404
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1405
    .line 1406
    .line 1407
    const-string v3, "<<\u63d0\u4ea4\u6570\u636e\u5b8c\u6bd5 error("

    .line 1408
    .line 1409
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1410
    .line 1411
    .line 1412
    invoke-virtual {v0}, Lcom/intsig/tianshu/UploadState;->〇080()I

    .line 1413
    .line 1414
    .line 1415
    move-result v3

    .line 1416
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1417
    .line 1418
    .line 1419
    const-string v3, "), uploadedReversion: "

    .line 1420
    .line 1421
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1422
    .line 1423
    .line 1424
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1425
    .line 1426
    .line 1427
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1428
    .line 1429
    .line 1430
    move-result-object v2

    .line 1431
    invoke-static {v2}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 1432
    .line 1433
    .line 1434
    if-gez v1, :cond_25

    .line 1435
    .line 1436
    goto :goto_22

    .line 1437
    :cond_25
    move/from16 v19, v1

    .line 1438
    .line 1439
    :goto_22
    invoke-virtual {v0}, Lcom/intsig/tianshu/UploadState;->〇o00〇〇Oo()I

    .line 1440
    .line 1441
    .line 1442
    move-result v1
    :try_end_1b
    .catch Lcom/intsig/tianshu/exception/SSLTSException; {:try_start_1b .. :try_end_1b} :catch_15
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1b .. :try_end_1b} :catch_14

    .line 1443
    :try_start_1c
    invoke-virtual {v0}, Lcom/intsig/tianshu/UploadState;->〇080()I

    .line 1444
    .line 1445
    .line 1446
    move-result v0
    :try_end_1c
    .catch Lcom/intsig/tianshu/exception/SSLTSException; {:try_start_1c .. :try_end_1c} :catch_17
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1c .. :try_end_1c} :catch_14

    .line 1447
    move/from16 v2, v19

    .line 1448
    .line 1449
    goto :goto_26

    .line 1450
    :catch_17
    :goto_23
    :try_start_1d
    invoke-static {v7}, Lcom/intsig/tianshu/TianShuAPI;->O〇〇(Ljava/lang/String;)Lcom/intsig/tianshu/TSFolder;

    .line 1451
    .line 1452
    .line 1453
    move-result-object v0
    :try_end_1d
    .catch Lcom/intsig/tianshu/exception/EurekaException; {:try_start_1d .. :try_end_1d} :catch_18
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1d .. :try_end_1d} :catch_14

    .line 1454
    goto :goto_24

    .line 1455
    :catch_18
    :try_start_1e
    invoke-static {v7}, Lcom/intsig/tianshu/TianShuAPI;->O〇〇(Ljava/lang/String;)Lcom/intsig/tianshu/TSFolder;

    .line 1456
    .line 1457
    .line 1458
    move-result-object v0

    .line 1459
    :goto_24
    invoke-virtual {v0}, Lcom/intsig/tianshu/TSFile;->〇o00〇〇Oo()I

    .line 1460
    .line 1461
    .line 1462
    move-result v0

    .line 1463
    move v2, v10

    .line 1464
    :goto_25
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    .line 1465
    .line 1466
    .line 1467
    move-result v3

    .line 1468
    if-ge v2, v3, :cond_27

    .line 1469
    .line 1470
    invoke-virtual {v13, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 1471
    .line 1472
    .line 1473
    move-result-object v3

    .line 1474
    check-cast v3, Lcom/intsig/tianshu/UploadAction;

    .line 1475
    .line 1476
    invoke-virtual {v3}, Lcom/intsig/tianshu/UploadAction;->oO80()I

    .line 1477
    .line 1478
    .line 1479
    move-result v3

    .line 1480
    if-ne v3, v0, :cond_26

    .line 1481
    .line 1482
    move v1, v2

    .line 1483
    :cond_26
    add-int/lit8 v2, v2, 0x1

    .line 1484
    .line 1485
    goto :goto_25

    .line 1486
    :cond_27
    move v2, v0

    .line 1487
    const/4 v0, 0x0

    .line 1488
    :goto_26
    const/4 v3, 0x0

    .line 1489
    move v4, v10

    .line 1490
    :goto_27
    if-ge v4, v1, :cond_2d

    .line 1491
    .line 1492
    aget v3, v17, v18

    .line 1493
    .line 1494
    invoke-virtual {v15, v3}, Lcom/intsig/tianshu/sync/SyncState;->〇080(F)V

    .line 1495
    .line 1496
    .line 1497
    invoke-interface {v8, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 1498
    .line 1499
    .line 1500
    invoke-virtual {v13, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 1501
    .line 1502
    .line 1503
    move-result-object v3

    .line 1504
    check-cast v3, Lcom/intsig/tianshu/UploadAction;

    .line 1505
    .line 1506
    new-instance v5, Ljava/lang/StringBuilder;

    .line 1507
    .line 1508
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1509
    .line 1510
    .line 1511
    invoke-virtual {v3}, Lcom/intsig/tianshu/UploadAction;->oO80()I

    .line 1512
    .line 1513
    .line 1514
    move-result v6

    .line 1515
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1516
    .line 1517
    .line 1518
    const-string v6, "<>"

    .line 1519
    .line 1520
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1521
    .line 1522
    .line 1523
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1524
    .line 1525
    .line 1526
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1527
    .line 1528
    .line 1529
    move-result-object v5

    .line 1530
    invoke-static {v5}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 1531
    .line 1532
    .line 1533
    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 1534
    .line 1535
    .line 1536
    move-result v5

    .line 1537
    if-eqz v5, :cond_28

    .line 1538
    .line 1539
    sget-object v5, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 1540
    .line 1541
    invoke-virtual {v5}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 1542
    .line 1543
    .line 1544
    move-result-object v5

    .line 1545
    const/4 v6, 0x1

    .line 1546
    invoke-virtual {v5, v6}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇oOO8O8(I)V

    .line 1547
    .line 1548
    .line 1549
    goto :goto_28

    .line 1550
    :cond_28
    sget-object v5, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    .line 1551
    .line 1552
    invoke-virtual {v5}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    .line 1553
    .line 1554
    .line 1555
    move-result-object v5

    .line 1556
    const/4 v6, 0x1

    .line 1557
    invoke-virtual {v5, v6}, Lcom/intsig/tianshu/sync/SyncTimeCount;->oo〇(I)V

    .line 1558
    .line 1559
    .line 1560
    :goto_28
    invoke-virtual {v3}, Lcom/intsig/tianshu/UploadAction;->oO80()I

    .line 1561
    .line 1562
    .line 1563
    move-result v5

    .line 1564
    if-le v5, v2, :cond_29

    .line 1565
    .line 1566
    goto :goto_2a

    .line 1567
    :cond_29
    invoke-interface {v12, v3}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇080(Lcom/intsig/tianshu/UploadAction;)V

    .line 1568
    .line 1569
    .line 1570
    iget v5, v3, Lcom/intsig/tianshu/UploadAction;->〇o〇:I

    .line 1571
    .line 1572
    const/4 v6, 0x1

    .line 1573
    if-eq v5, v6, :cond_2c

    .line 1574
    .line 1575
    if-eq v5, v14, :cond_2b

    .line 1576
    .line 1577
    const/4 v14, 0x3

    .line 1578
    if-eq v5, v14, :cond_2a

    .line 1579
    .line 1580
    goto :goto_29

    .line 1581
    :cond_2a
    iget v5, v15, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo:I

    .line 1582
    .line 1583
    add-int/2addr v5, v6

    .line 1584
    iput v5, v15, Lcom/intsig/tianshu/sync/SyncState;->〇o00〇〇Oo:I

    .line 1585
    .line 1586
    goto :goto_29

    .line 1587
    :cond_2b
    iget v5, v15, Lcom/intsig/tianshu/sync/SyncState;->〇o〇:I

    .line 1588
    .line 1589
    add-int/2addr v5, v6

    .line 1590
    iput v5, v15, Lcom/intsig/tianshu/sync/SyncState;->〇o〇:I

    .line 1591
    .line 1592
    goto :goto_29

    .line 1593
    :cond_2c
    iget v5, v15, Lcom/intsig/tianshu/sync/SyncState;->〇080:I

    .line 1594
    .line 1595
    add-int/2addr v5, v6

    .line 1596
    iput v5, v15, Lcom/intsig/tianshu/sync/SyncState;->〇080:I

    .line 1597
    .line 1598
    :goto_29
    add-int/lit8 v4, v4, 0x1

    .line 1599
    .line 1600
    const/4 v14, 0x2

    .line 1601
    goto :goto_27

    .line 1602
    :cond_2d
    :goto_2a
    const-string v4, ">>\u66f4\u65b0\u672c\u5730 \u7248\u672c\u53f7"

    .line 1603
    .line 1604
    invoke-static {v4}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 1605
    .line 1606
    .line 1607
    invoke-interface {v12, v2}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇80〇808〇O(I)V

    .line 1608
    .line 1609
    .line 1610
    const-string v4, "\u300a\u300a\u66f4\u65b0\u672c\u5730 \u7248\u672c\u53f7"

    .line 1611
    .line 1612
    invoke-static {v4}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 1613
    .line 1614
    .line 1615
    const/16 v4, -0x3e9

    .line 1616
    .line 1617
    if-eq v0, v4, :cond_37

    .line 1618
    .line 1619
    packed-switch v0, :pswitch_data_0

    .line 1620
    .line 1621
    .line 1622
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->〇8o()Z

    .line 1623
    .line 1624
    .line 1625
    move-result v0

    .line 1626
    if-nez v0, :cond_2f

    .line 1627
    .line 1628
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->o0O〇8o0O()Z

    .line 1629
    .line 1630
    .line 1631
    move-result v0

    .line 1632
    if-eqz v0, :cond_2e

    .line 1633
    .line 1634
    goto :goto_2b

    .line 1635
    :cond_2e
    move/from16 v14, p1

    .line 1636
    .line 1637
    move v10, v1

    .line 1638
    move/from16 v19, v2

    .line 1639
    .line 1640
    const/4 v3, 0x2

    .line 1641
    const/16 v4, -0x69

    .line 1642
    .line 1643
    const-wide/16 v5, 0x0

    .line 1644
    .line 1645
    goto/16 :goto_20

    .line 1646
    .line 1647
    :cond_2f
    :goto_2b
    move-object/from16 v1, v33

    .line 1648
    .line 1649
    invoke-static {v11, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    .line 1651
    .line 1652
    move v10, v2

    .line 1653
    goto/16 :goto_31

    .line 1654
    .line 1655
    :pswitch_0
    const-wide/16 v6, 0x0

    .line 1656
    .line 1657
    goto :goto_2f

    .line 1658
    :goto_2c
    :pswitch_1
    if-ge v10, v1, :cond_36

    .line 1659
    .line 1660
    invoke-virtual {v13, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 1661
    .line 1662
    .line 1663
    move-result-object v4

    .line 1664
    check-cast v4, Lcom/intsig/tianshu/UploadAction;

    .line 1665
    .line 1666
    invoke-virtual {v4}, Lcom/intsig/tianshu/UploadAction;->oO80()I

    .line 1667
    .line 1668
    .line 1669
    move-result v5

    .line 1670
    const/4 v6, 0x1

    .line 1671
    add-int/lit8 v9, v2, 0x1

    .line 1672
    .line 1673
    if-ne v5, v9, :cond_31

    .line 1674
    .line 1675
    invoke-virtual {v4}, Lcom/intsig/tianshu/UploadAction;->O8()I

    .line 1676
    .line 1677
    .line 1678
    move-result v5

    .line 1679
    if-eq v5, v6, :cond_30

    .line 1680
    .line 1681
    invoke-virtual {v4}, Lcom/intsig/tianshu/UploadAction;->O8()I

    .line 1682
    .line 1683
    .line 1684
    move-result v4

    .line 1685
    const/4 v5, 0x3

    .line 1686
    if-ne v4, v5, :cond_32

    .line 1687
    .line 1688
    :cond_30
    const-string v0, "upload faield:-11"

    .line 1689
    .line 1690
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->o〇8〇(Ljava/lang/String;)V
    :try_end_1e
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1e .. :try_end_1e} :catch_14

    .line 1691
    .line 1692
    .line 1693
    const-wide/16 v6, 0x0

    .line 1694
    .line 1695
    :try_start_1f
    invoke-virtual {v15, v6, v7}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1696
    .line 1697
    .line 1698
    const/4 v1, 0x1

    .line 1699
    invoke-virtual {v15, v1}, Lcom/intsig/tianshu/sync/SyncState;->OO0o〇〇(Z)V

    .line 1700
    .line 1701
    .line 1702
    return-object v15

    .line 1703
    :cond_31
    const/4 v5, 0x3

    .line 1704
    :cond_32
    const-wide/16 v6, 0x0

    .line 1705
    .line 1706
    add-int/lit8 v10, v10, 0x1

    .line 1707
    .line 1708
    goto :goto_2c

    .line 1709
    :pswitch_2
    const-wide/16 v6, 0x0

    .line 1710
    .line 1711
    :goto_2d
    if-ge v10, v1, :cond_35

    .line 1712
    .line 1713
    invoke-virtual {v13, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 1714
    .line 1715
    .line 1716
    move-result-object v4

    .line 1717
    check-cast v4, Lcom/intsig/tianshu/UploadAction;

    .line 1718
    .line 1719
    invoke-virtual {v4}, Lcom/intsig/tianshu/UploadAction;->oO80()I

    .line 1720
    .line 1721
    .line 1722
    move-result v5

    .line 1723
    const/4 v9, 0x1

    .line 1724
    add-int/lit8 v11, v2, 0x1

    .line 1725
    .line 1726
    if-ne v5, v11, :cond_34

    .line 1727
    .line 1728
    invoke-virtual {v4}, Lcom/intsig/tianshu/UploadAction;->O8()I

    .line 1729
    .line 1730
    .line 1731
    move-result v5

    .line 1732
    const/4 v9, 0x2

    .line 1733
    if-ne v5, v9, :cond_33

    .line 1734
    .line 1735
    invoke-interface {v8, v3, v0}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇o00〇〇Oo(Lcom/intsig/tianshu/UploadAction;I)V

    .line 1736
    .line 1737
    .line 1738
    invoke-interface {v12, v4}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇080(Lcom/intsig/tianshu/UploadAction;)V

    .line 1739
    .line 1740
    .line 1741
    const/4 v4, 0x1

    .line 1742
    invoke-virtual {v15, v4}, Lcom/intsig/tianshu/sync/SyncState;->OO0o〇〇(Z)V

    .line 1743
    .line 1744
    .line 1745
    goto :goto_2f

    .line 1746
    :cond_33
    const/4 v4, 0x1

    .line 1747
    goto :goto_2e

    .line 1748
    :cond_34
    const/4 v4, 0x1

    .line 1749
    const/4 v9, 0x2

    .line 1750
    :goto_2e
    add-int/lit8 v10, v10, 0x1

    .line 1751
    .line 1752
    goto :goto_2d

    .line 1753
    :cond_35
    :goto_2f
    invoke-virtual {v15, v6, v7}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1754
    .line 1755
    .line 1756
    return-object v15

    .line 1757
    :cond_36
    :pswitch_3
    const-wide/16 v6, 0x0

    .line 1758
    .line 1759
    invoke-interface {v8, v3, v0}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇o00〇〇Oo(Lcom/intsig/tianshu/UploadAction;I)V

    .line 1760
    .line 1761
    .line 1762
    invoke-virtual {v15, v6, v7}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1763
    .line 1764
    .line 1765
    return-object v15

    .line 1766
    :catch_19
    move-exception v0

    .line 1767
    goto :goto_30

    .line 1768
    :pswitch_4
    const-wide/16 v6, 0x0

    .line 1769
    .line 1770
    new-instance v1, Lcom/intsig/tianshu/exception/TianShuException;

    .line 1771
    .line 1772
    const-string v2, "batchUpload"

    .line 1773
    .line 1774
    invoke-direct {v1, v0, v2}, Lcom/intsig/tianshu/exception/TianShuException;-><init>(ILjava/lang/String;)V

    .line 1775
    .line 1776
    .line 1777
    throw v1

    .line 1778
    :pswitch_5
    const-wide/16 v6, 0x0

    .line 1779
    .line 1780
    invoke-interface {v8, v3, v0}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇o00〇〇Oo(Lcom/intsig/tianshu/UploadAction;I)V

    .line 1781
    .line 1782
    .line 1783
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 1784
    .line 1785
    .line 1786
    return-object v15

    .line 1787
    :cond_37
    const-wide/16 v6, 0x0

    .line 1788
    .line 1789
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 1790
    .line 1791
    .line 1792
    new-instance v1, Lcom/intsig/tianshu/exception/TianShuException;

    .line 1793
    .line 1794
    const-string v2, "user terminate sync."

    .line 1795
    .line 1796
    invoke-direct {v1, v0, v2}, Lcom/intsig/tianshu/exception/TianShuException;-><init>(ILjava/lang/String;)V

    .line 1797
    .line 1798
    .line 1799
    throw v1
    :try_end_1f
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1f .. :try_end_1f} :catch_19

    .line 1800
    :cond_38
    move/from16 v10, v19

    .line 1801
    .line 1802
    goto :goto_31

    .line 1803
    :catch_1a
    move-exception v0

    .line 1804
    move-wide v6, v5

    .line 1805
    :goto_30
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1806
    .line 1807
    .line 1808
    const-string v1, "batchCommit"

    .line 1809
    .line 1810
    invoke-static {v1, v0}, Lcom/intsig/tianshu/TianShuAPI;->〇〇00OO(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1811
    .line 1812
    .line 1813
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 1814
    .line 1815
    .line 1816
    move-result v1

    .line 1817
    const/16 v2, -0x130

    .line 1818
    .line 1819
    if-eq v1, v2, :cond_3a

    .line 1820
    .line 1821
    const/16 v2, -0x69

    .line 1822
    .line 1823
    if-eq v1, v2, :cond_39

    .line 1824
    .line 1825
    const/16 v2, -0x66

    .line 1826
    .line 1827
    if-eq v1, v2, :cond_39

    .line 1828
    .line 1829
    const/16 v2, 0x15f

    .line 1830
    .line 1831
    if-eq v1, v2, :cond_39

    .line 1832
    .line 1833
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getMessage()Ljava/lang/String;

    .line 1834
    .line 1835
    .line 1836
    move-result-object v0

    .line 1837
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->oO80(Ljava/lang/String;)V

    .line 1838
    .line 1839
    .line 1840
    invoke-virtual {v15, v1}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 1841
    .line 1842
    .line 1843
    return-object v15

    .line 1844
    :cond_39
    invoke-virtual {v15, v1}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 1845
    .line 1846
    .line 1847
    invoke-virtual {v15, v6, v7}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1848
    .line 1849
    .line 1850
    return-object v15

    .line 1851
    :cond_3a
    const-wide/32 v1, 0x927c0

    .line 1852
    .line 1853
    .line 1854
    invoke-virtual {v15, v1, v2}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1855
    .line 1856
    .line 1857
    return-object v15

    .line 1858
    :cond_3b
    const-string v0, "no data to commit"

    .line 1859
    .line 1860
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo0oO〇O〇O(Ljava/lang/String;)V

    .line 1861
    .line 1862
    .line 1863
    :goto_31
    invoke-virtual {v15, v10}, Lcom/intsig/tianshu/sync/SyncState;->OO0o〇〇〇〇0(I)V

    .line 1864
    .line 1865
    .line 1866
    const/high16 v1, 0x42c80000    # 100.0f

    .line 1867
    .line 1868
    invoke-virtual {v15, v1}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    .line 1869
    .line 1870
    .line 1871
    invoke-interface {v8, v15}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 1872
    .line 1873
    .line 1874
    return-object v15

    .line 1875
    :cond_3c
    const/16 v1, -0x130

    .line 1876
    .line 1877
    if-ne v10, v1, :cond_3d

    .line 1878
    .line 1879
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 1880
    .line 1881
    .line 1882
    move-result-wide v0

    .line 1883
    rem-long/2addr v0, v4

    .line 1884
    const-wide/16 v2, 0x3e8

    .line 1885
    .line 1886
    mul-long v0, v0, v2

    .line 1887
    .line 1888
    const-wide/32 v2, 0x927c0

    .line 1889
    .line 1890
    .line 1891
    add-long/2addr v0, v2

    .line 1892
    invoke-virtual {v15, v0, v1}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1893
    .line 1894
    .line 1895
    return-object v15

    .line 1896
    :cond_3d
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getMessage()Ljava/lang/String;

    .line 1897
    .line 1898
    .line 1899
    move-result-object v0

    .line 1900
    invoke-virtual {v15, v0}, Lcom/intsig/tianshu/sync/SyncState;->oO80(Ljava/lang/String;)V

    .line 1901
    .line 1902
    .line 1903
    invoke-virtual {v15, v10}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 1904
    .line 1905
    .line 1906
    return-object v15

    .line 1907
    :cond_3e
    :goto_32
    const-wide/16 v6, 0x0

    .line 1908
    .line 1909
    invoke-virtual {v15, v10}, Lcom/intsig/tianshu/sync/SyncState;->〇〇888(I)V

    .line 1910
    .line 1911
    .line 1912
    invoke-virtual {v15, v6, v7}, Lcom/intsig/tianshu/sync/SyncState;->〇80〇808〇O(J)V

    .line 1913
    .line 1914
    .line 1915
    return-object v15

    .line 1916
    nop

    :pswitch_data_0
    .packed-switch -0x10
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static update()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static synthetic 〇080([JLjava/util/List;Ljava/util/concurrent/ExecutorService;[FFLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;Lcom/intsig/tianshu/sync/SyncAdapter;Ljava/lang/String;[JLjava/lang/String;IIJLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p15}, Lcom/intsig/tianshu/sync/SyncApi;->O8([JLjava/util/List;Ljava/util/concurrent/ExecutorService;[FFLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;Lcom/intsig/tianshu/sync/SyncAdapter;Ljava/lang/String;[JLjava/lang/String;IIJLjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
.end method

.method public static synthetic 〇o00〇〇Oo([FFLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;ILcom/intsig/tianshu/sync/SyncAdapter;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;[J)Ljava/lang/Integer;
    .locals 0

    .line 1
    invoke-static/range {p0 .. p12}, Lcom/intsig/tianshu/sync/SyncApi;->〇o〇([FFLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;ILcom/intsig/tianshu/sync/SyncAdapter;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;[J)Ljava/lang/Integer;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
.end method

.method private static synthetic 〇o〇([FFLcom/intsig/tianshu/sync/SyncState;Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;ILcom/intsig/tianshu/sync/SyncAdapter;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;[J)Ljava/lang/Integer;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object v0, p2

    move v1, p4

    .line 1
    sget-object v2, Lcom/intsig/tianshu/sync/SyncApi;->〇080:[B

    monitor-enter v2

    const/4 v3, 0x1

    .line 2
    :try_start_0
    aget v4, p0, v3

    const/4 v5, 0x0

    aget v6, p0, v5

    add-float/2addr v4, v6

    aput v4, p0, v3

    cmpl-float v4, v4, p1

    if-lez v4, :cond_0

    .line 3
    aput p1, p0, v3

    .line 4
    :cond_0
    aget v4, p0, v3

    invoke-virtual {p2, v4}, Lcom/intsig/tianshu/sync/SyncState;->〇O8o08O(F)V

    move-object v4, p3

    .line 5
    invoke-interface {p3, p2}, Lcom/intsig/tianshu/sync/SyncApi$SyncProgress;->〇080(Lcom/intsig/tianshu/sync/SyncState;)V

    .line 6
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eq v1, v3, :cond_3

    const/4 v0, 0x2

    if-eq v1, v0, :cond_2

    const/4 v0, 0x3

    if-eq v1, v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    .line 7
    :cond_1
    invoke-interface/range {p5 .. p11}, Lcom/intsig/tianshu/sync/SyncAdapter;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)V

    goto :goto_0

    .line 8
    :cond_2
    invoke-interface/range {p5 .. p10}, Lcom/intsig/tianshu/sync/SyncAdapter;->Oo08(Ljava/lang/String;Ljava/lang/String;IJ)V

    goto :goto_0

    .line 9
    :cond_3
    invoke-interface/range {p5 .. p11}, Lcom/intsig/tianshu/sync/SyncAdapter;->oO80(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    const-string v0, "CamScanner_Doc"

    move-object v1, p6

    .line 10
    invoke-static {p6, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 11
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇8o8o〇(I)V

    goto :goto_2

    .line 12
    :cond_4
    sget-object v0, Lcom/intsig/tianshu/sync/SyncTimeCount;->o8O〇:Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;

    invoke-virtual {v0}, Lcom/intsig/tianshu/sync/SyncTimeCount$Companion;->〇o00〇〇Oo()Lcom/intsig/tianshu/sync/SyncTimeCount;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/intsig/tianshu/sync/SyncTimeCount;->〇〇808〇(I)V

    .line 13
    :cond_5
    :goto_2
    sget-object v1, Lcom/intsig/tianshu/sync/SyncApi;->〇o00〇〇Oo:[B

    monitor-enter v1

    .line 14
    :try_start_1
    sget-object v0, Lcom/intsig/tianshu/sync/SyncApi;->〇o〇:Ljava/util/HashSet;

    move-object v2, p7

    invoke-virtual {v0, p7}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 15
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    aput-wide v2, p12, v5

    .line 16
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    .line 18
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    .line 19
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

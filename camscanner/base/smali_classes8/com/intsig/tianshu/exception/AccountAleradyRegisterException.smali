.class public Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;
.super Lcom/intsig/tianshu/exception/TianShuException;
.source "AccountAleradyRegisterException.java"


# static fields
.field public static final STATE_EMAIL_BOUNED:I = 0x1

.field public static final STATE_EMAIL_BOUNED_REGISTERED:I = 0x3

.field public static final STATE_EMAIL_REGISTERED:I = 0x2


# instance fields
.field private email_state:I

.field private mBoundAccount:Ljava/lang/String;

.field private products:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0xca

    .line 5
    invoke-direct {p0, v0, p2}, Lcom/intsig/tianshu/exception/TianShuException;-><init>(ILjava/lang/String;)V

    const/4 p2, 0x0

    .line 6
    iput-object p2, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->mBoundAccount:Ljava/lang/String;

    .line 7
    iput-object p3, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->products:Ljava/lang/String;

    .line 8
    iput p1, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->email_state:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0xca

    .line 9
    invoke-direct {p0, v0, p2}, Lcom/intsig/tianshu/exception/TianShuException;-><init>(ILjava/lang/String;)V

    .line 10
    iput-object p3, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->products:Ljava/lang/String;

    .line 11
    iput p1, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->email_state:I

    .line 12
    iput-object p4, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->mBoundAccount:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p4}, Lcom/intsig/tianshu/exception/TianShuException;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    const/4 p1, 0x0

    .line 2
    iput p1, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->email_state:I

    const/4 p1, 0x0

    .line 3
    iput-object p1, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->mBoundAccount:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->products:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getBoundAccount()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->mBoundAccount:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEmailState()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->email_state:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getProducts()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tianshu/exception/AccountAleradyRegisterException;->products:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

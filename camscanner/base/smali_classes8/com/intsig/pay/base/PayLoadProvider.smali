.class public final Lcom/intsig/pay/base/PayLoadProvider;
.super Ljava/lang/Object;
.source "PayLoadProvider.kt"

# interfaces
.implements Landroidx/startup/Initializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroidx/startup/Initializer<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public bridge synthetic create(Landroid/content/Context;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/pay/base/PayLoadProvider;->〇080(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 5
    .line 6
    return-object p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dependencies()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "+",
            "Landroidx/startup/Initializer<",
            "*>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇80〇808〇O()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇080(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/pay/base/utils/AppContextAttach;->〇080:Lcom/intsig/pay/base/utils/AppContextAttach;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/pay/base/utils/AppContextAttach;->〇080(Landroid/content/Context;)V

    .line 9
    .line 10
    .line 11
    sget-object p1, Lcom/intsig/pay/base/core/PayPlanLoader;->〇080:Lcom/intsig/pay/base/core/PayPlanLoader;

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/pay/base/model/PayPlan;

    .line 14
    .line 15
    const/16 v1, 0xd

    .line 16
    .line 17
    const-string v2, "com.intsig.pay.hmspay.HmsPay"

    .line 18
    .line 19
    invoke-direct {v0, v1, v2}, Lcom/intsig/pay/base/model/PayPlan;-><init>(ILjava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v0}, Lcom/intsig/pay/base/core/PayPlanLoader;->〇080(Lcom/intsig/pay/base/model/PayPlan;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/intsig/pay/base/model/PayPlan;

    .line 26
    .line 27
    const/4 v1, 0x1

    .line 28
    const-string v2, "com.intsig.pay.alipay.AliPay"

    .line 29
    .line 30
    invoke-direct {v0, v1, v2}, Lcom/intsig/pay/base/model/PayPlan;-><init>(ILjava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1, v0}, Lcom/intsig/pay/base/core/PayPlanLoader;->〇080(Lcom/intsig/pay/base/model/PayPlan;)V

    .line 34
    .line 35
    .line 36
    new-instance v0, Lcom/intsig/pay/base/model/PayPlan;

    .line 37
    .line 38
    const/4 v1, 0x2

    .line 39
    const-string v2, "com.intsig.pay.wechat.WechatPay"

    .line 40
    .line 41
    invoke-direct {v0, v1, v2}, Lcom/intsig/pay/base/model/PayPlan;-><init>(ILjava/lang/String;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1, v0}, Lcom/intsig/pay/base/core/PayPlanLoader;->〇080(Lcom/intsig/pay/base/model/PayPlan;)V

    .line 45
    .line 46
    .line 47
    new-instance v0, Lcom/intsig/pay/base/model/PayPlan;

    .line 48
    .line 49
    const/4 v1, 0x4

    .line 50
    const-string v2, "com.intsig.pay.google.GooglePay"

    .line 51
    .line 52
    invoke-direct {v0, v1, v2}, Lcom/intsig/pay/base/model/PayPlan;-><init>(ILjava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1, v0}, Lcom/intsig/pay/base/core/PayPlanLoader;->〇080(Lcom/intsig/pay/base/model/PayPlan;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

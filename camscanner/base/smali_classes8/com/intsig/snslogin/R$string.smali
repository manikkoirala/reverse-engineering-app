.class public final Lcom/intsig/snslogin/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/snslogin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final a_global_msg_load_failed:I = 0x7f130089

.field public static final a_global_msg_loading:I = 0x7f13008a

.field public static final a_msg_ssl_err:I = 0x7f13034b

.field public static final abc_action_bar_home_description:I = 0x7f130493

.field public static final abc_action_bar_up_description:I = 0x7f130494

.field public static final abc_action_menu_overflow_description:I = 0x7f130495

.field public static final abc_action_mode_done:I = 0x7f130496

.field public static final abc_activity_chooser_view_see_all:I = 0x7f130497

.field public static final abc_activitychooserview_choose_application:I = 0x7f130498

.field public static final abc_capital_off:I = 0x7f130499

.field public static final abc_capital_on:I = 0x7f13049a

.field public static final abc_menu_alt_shortcut_label:I = 0x7f13049b

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f13049c

.field public static final abc_menu_delete_shortcut_label:I = 0x7f13049d

.field public static final abc_menu_enter_shortcut_label:I = 0x7f13049e

.field public static final abc_menu_function_shortcut_label:I = 0x7f13049f

.field public static final abc_menu_meta_shortcut_label:I = 0x7f1304a0

.field public static final abc_menu_shift_shortcut_label:I = 0x7f1304a1

.field public static final abc_menu_space_shortcut_label:I = 0x7f1304a2

.field public static final abc_menu_sym_shortcut_label:I = 0x7f1304a3

.field public static final abc_prepend_shortcut_label:I = 0x7f1304a4

.field public static final abc_search_hint:I = 0x7f1304a5

.field public static final abc_searchview_description_clear:I = 0x7f1304a6

.field public static final abc_searchview_description_query:I = 0x7f1304a7

.field public static final abc_searchview_description_search:I = 0x7f1304a8

.field public static final abc_searchview_description_submit:I = 0x7f1304a9

.field public static final abc_searchview_description_voice:I = 0x7f1304aa

.field public static final abc_shareactionprovider_share_with:I = 0x7f1304ab

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f1304ac

.field public static final abc_toolbar_collapse_description:I = 0x7f1304ad

.field public static final cancel:I = 0x7f13057e

.field public static final com_facebook_device_auth_instructions:I = 0x7f130590

.field public static final com_facebook_image_download_unknown_error:I = 0x7f130591

.field public static final com_facebook_internet_permission_error_message:I = 0x7f130592

.field public static final com_facebook_internet_permission_error_title:I = 0x7f130593

.field public static final com_facebook_like_button_liked:I = 0x7f130594

.field public static final com_facebook_like_button_not_liked:I = 0x7f130595

.field public static final com_facebook_loading:I = 0x7f130596

.field public static final com_facebook_loginview_cancel_action:I = 0x7f130597

.field public static final com_facebook_loginview_log_in_button:I = 0x7f130598

.field public static final com_facebook_loginview_log_in_button_continue:I = 0x7f130599

.field public static final com_facebook_loginview_log_in_button_long:I = 0x7f13059a

.field public static final com_facebook_loginview_log_out_action:I = 0x7f13059b

.field public static final com_facebook_loginview_log_out_button:I = 0x7f13059c

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f13059d

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f13059e

.field public static final com_facebook_send_button_text:I = 0x7f13059f

.field public static final com_facebook_share_button_text:I = 0x7f1305a0

.field public static final com_facebook_smart_device_instructions:I = 0x7f1305a1

.field public static final com_facebook_smart_device_instructions_or:I = 0x7f1305a2

.field public static final com_facebook_smart_login_confirmation_cancel:I = 0x7f1305a3

.field public static final com_facebook_smart_login_confirmation_continue_as:I = 0x7f1305a4

.field public static final com_facebook_smart_login_confirmation_title:I = 0x7f1305a5

.field public static final com_facebook_tooltip_default:I = 0x7f1305a6

.field public static final common_google_play_services_enable_button:I = 0x7f1305a7

.field public static final common_google_play_services_enable_text:I = 0x7f1305a8

.field public static final common_google_play_services_enable_title:I = 0x7f1305a9

.field public static final common_google_play_services_install_button:I = 0x7f1305aa

.field public static final common_google_play_services_install_text:I = 0x7f1305ab

.field public static final common_google_play_services_install_title:I = 0x7f1305ac

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1305ad

.field public static final common_google_play_services_notification_ticker:I = 0x7f1305ae

.field public static final common_google_play_services_unknown_issue:I = 0x7f1305af

.field public static final common_google_play_services_unsupported_text:I = 0x7f1305b0

.field public static final common_google_play_services_update_button:I = 0x7f1305b1

.field public static final common_google_play_services_update_text:I = 0x7f1305b2

.field public static final common_google_play_services_update_title:I = 0x7f1305b3

.field public static final common_google_play_services_updating_text:I = 0x7f1305b4

.field public static final common_google_play_services_wear_update_text:I = 0x7f1305b5

.field public static final common_open_on_phone:I = 0x7f1305b6

.field public static final common_signin_button_text:I = 0x7f1305b7

.field public static final common_signin_button_text_long:I = 0x7f1305b8

.field public static final ok:I = 0x7f131e36

.field public static final search_menu_title:I = 0x7f131e6d

.field public static final status_bar_notification_info_overflow:I = 0x7f131eca

.field public static final util_a_label_send_to_wechat_timeline:I = 0x7f131f8e

.field public static final verify_failure:I = 0x7f131f92

.field public static final vk_captcha_hint:I = 0x7f131f94

.field public static final vk_confirm:I = 0x7f131f95

.field public static final vk_retry:I = 0x7f131f96


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

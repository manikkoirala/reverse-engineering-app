.class public interface abstract Lcom/intsig/router/service/RouterLoginTaskService;
.super Ljava/lang/Object;
.source "RouterLoginTaskService.java"

# interfaces
.implements Lcom/alibaba/android/arouter/facade/template/IProvider;


# static fields
.field public static final EXTRA_ACCOUNT:Ljava/lang/String; = "extra_account"

.field public static final EXTRA_AREA_CODE:Ljava/lang/String; = "extra_area_code"

.field public static final EXTRA_INTENT_ACTION:Ljava/lang/String; = "extra_intent_action"

.field public static final EXTRA_LOGIN_TASK_LISTENER:Ljava/lang/String; = "extra_login_task_listener"

.field public static final EXTRA_PWD:Ljava/lang/String; = "extra_pwd"

.field public static final EXTRA_TAG:Ljava/lang/String; = "extra_tag"

.field public static final SPECIAL_ACCOUT_OTHER:I = 0x2


# virtual methods
.method public varargs abstract doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
.end method

.method public abstract onPostExecute(Ljava/lang/Integer;)V
.end method

.method public abstract setBasicPara(Landroid/os/Bundle;)V
.end method

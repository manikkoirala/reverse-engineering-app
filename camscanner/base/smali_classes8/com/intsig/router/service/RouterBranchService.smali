.class public interface abstract Lcom/intsig/router/service/RouterBranchService;
.super Ljava/lang/Object;
.source "RouterBranchService.java"

# interfaces
.implements Lcom/alibaba/android/arouter/facade/template/IProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/router/service/RouterBranchService$Params;
    }
.end annotation


# virtual methods
.method public abstract initSdk(Landroid/content/Context;Z)V
.end method

.method public abstract onLaunchNewIntent(Landroid/app/Activity;Landroid/content/Intent;)V
.end method

.method public abstract onLaunchStart(Landroid/app/Activity;Landroid/content/Intent;)V
.end method

.method public abstract uploadBranchId(Lcom/intsig/router/service/RouterBranchService$Params;)V
.end method

.method public abstract validate(Landroid/app/Activity;)V
.end method

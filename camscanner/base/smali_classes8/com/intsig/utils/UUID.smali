.class public final Lcom/intsig/utils/UUID;
.super Ljava/lang/Object;
.source "UUID.java"


# static fields
.field static 〇080:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/16 v0, 0x2a

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    fill-array-data v0, :array_0

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/utils/UUID;->〇080:[B

    .line 9
    .line 10
    return-void

    .line 11
    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x48t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x61t
        0x62t
        0x64t
        0x65t
        0x66t
        0x67t
        0x68t
        0x72t
        0x74t
        0x79t
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static 〇080(Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    .line 1
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    array-length v0, p0

    .line 6
    div-int/lit8 v0, v0, 0x2

    .line 7
    .line 8
    div-int/lit8 v1, v0, 0x2

    .line 9
    .line 10
    const/16 v2, 0x18

    .line 11
    .line 12
    new-array v3, v2, [B

    .line 13
    .line 14
    const/4 v4, 0x0

    .line 15
    const/4 v5, 0x0

    .line 16
    const/4 v6, 0x0

    .line 17
    :goto_0
    const/16 v7, 0x2a

    .line 18
    .line 19
    if-ge v5, v1, :cond_0

    .line 20
    .line 21
    add-int/lit8 v8, v6, 0x1

    .line 22
    .line 23
    aget-byte v6, p0, v6

    .line 24
    .line 25
    invoke-static {v6}, Lcom/intsig/utils/UUID;->〇o〇(B)B

    .line 26
    .line 27
    .line 28
    move-result v6

    .line 29
    add-int/lit8 v9, v8, 0x1

    .line 30
    .line 31
    aget-byte v8, p0, v8

    .line 32
    .line 33
    invoke-static {v8}, Lcom/intsig/utils/UUID;->〇o〇(B)B

    .line 34
    .line 35
    .line 36
    move-result v8

    .line 37
    add-int/lit8 v10, v9, 0x1

    .line 38
    .line 39
    aget-byte v9, p0, v9

    .line 40
    .line 41
    invoke-static {v9}, Lcom/intsig/utils/UUID;->〇o〇(B)B

    .line 42
    .line 43
    .line 44
    move-result v9

    .line 45
    add-int/lit8 v11, v10, 0x1

    .line 46
    .line 47
    aget-byte v10, p0, v10

    .line 48
    .line 49
    invoke-static {v10}, Lcom/intsig/utils/UUID;->〇o〇(B)B

    .line 50
    .line 51
    .line 52
    move-result v10

    .line 53
    shl-int/lit8 v6, v6, 0x4

    .line 54
    .line 55
    or-int/2addr v6, v4

    .line 56
    or-int/2addr v6, v8

    .line 57
    shl-int/lit8 v8, v9, 0xc

    .line 58
    .line 59
    or-int/2addr v6, v8

    .line 60
    shl-int/lit8 v8, v10, 0x8

    .line 61
    .line 62
    or-int/2addr v6, v8

    .line 63
    mul-int/lit8 v8, v5, 0x3

    .line 64
    .line 65
    add-int/lit8 v9, v8, 0x2

    .line 66
    .line 67
    sget-object v10, Lcom/intsig/utils/UUID;->〇080:[B

    .line 68
    .line 69
    div-int/lit16 v12, v6, 0x6e4

    .line 70
    .line 71
    aget-byte v12, v10, v12

    .line 72
    .line 73
    aput-byte v12, v3, v9

    .line 74
    .line 75
    rem-int/lit16 v6, v6, 0x6e4

    .line 76
    .line 77
    add-int/lit8 v9, v8, 0x1

    .line 78
    .line 79
    div-int/lit8 v12, v6, 0x2a

    .line 80
    .line 81
    aget-byte v12, v10, v12

    .line 82
    .line 83
    aput-byte v12, v3, v9

    .line 84
    .line 85
    add-int/2addr v8, v4

    .line 86
    rem-int/2addr v6, v7

    .line 87
    aget-byte v6, v10, v6

    .line 88
    .line 89
    aput-byte v6, v3, v8

    .line 90
    .line 91
    add-int/lit8 v5, v5, 0x1

    .line 92
    .line 93
    move v6, v11

    .line 94
    goto :goto_0

    .line 95
    :cond_0
    mul-int/lit8 v1, v1, 0x2

    .line 96
    .line 97
    if-ge v1, v0, :cond_1

    .line 98
    .line 99
    add-int/lit8 v0, v6, 0x1

    .line 100
    .line 101
    aget-byte v1, p0, v6

    .line 102
    .line 103
    invoke-static {v1}, Lcom/intsig/utils/UUID;->〇o〇(B)B

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    add-int/lit8 v6, v0, 0x1

    .line 108
    .line 109
    aget-byte v0, p0, v0

    .line 110
    .line 111
    invoke-static {v0}, Lcom/intsig/utils/UUID;->〇o〇(B)B

    .line 112
    .line 113
    .line 114
    move-result v0

    .line 115
    add-int/lit8 v8, v6, 0x1

    .line 116
    .line 117
    aget-byte v6, p0, v6

    .line 118
    .line 119
    invoke-static {v6}, Lcom/intsig/utils/UUID;->〇o〇(B)B

    .line 120
    .line 121
    .line 122
    move-result v6

    .line 123
    aget-byte p0, p0, v8

    .line 124
    .line 125
    invoke-static {p0}, Lcom/intsig/utils/UUID;->〇o〇(B)B

    .line 126
    .line 127
    .line 128
    move-result p0

    .line 129
    shl-int/lit8 v1, v1, 0xc

    .line 130
    .line 131
    or-int/2addr v1, v4

    .line 132
    shl-int/lit8 v0, v0, 0x8

    .line 133
    .line 134
    or-int/2addr v0, v1

    .line 135
    shl-int/lit8 v1, v6, 0x4

    .line 136
    .line 137
    or-int/2addr v0, v1

    .line 138
    or-int/2addr p0, v0

    .line 139
    add-int/lit8 v0, v5, 0x1

    .line 140
    .line 141
    sget-object v1, Lcom/intsig/utils/UUID;->〇080:[B

    .line 142
    .line 143
    rem-int/lit8 v6, p0, 0x2a

    .line 144
    .line 145
    aget-byte v6, v1, v6

    .line 146
    .line 147
    aput-byte v6, v3, v5

    .line 148
    .line 149
    if-lt p0, v7, :cond_1

    .line 150
    .line 151
    div-int/2addr p0, v7

    .line 152
    aget-byte p0, v1, p0

    .line 153
    .line 154
    aput-byte p0, v3, v0

    .line 155
    .line 156
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    .line 157
    .line 158
    invoke-direct {p0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 159
    .line 160
    .line 161
    :goto_1
    if-ge v4, v2, :cond_2

    .line 162
    .line 163
    aget-byte v0, v3, v4

    .line 164
    .line 165
    int-to-char v0, v0

    .line 166
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    add-int/lit8 v4, v4, 0x1

    .line 170
    .line 171
    goto :goto_1

    .line 172
    :cond_2
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object p0

    .line 176
    return-object p0
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static 〇o00〇〇Oo()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const-string v1, "-"

    .line 10
    .line 11
    const-string v2, ""

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-static {v0}, Lcom/intsig/utils/UUID;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    return-object v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static 〇o〇(B)B
    .locals 2

    .line 1
    const/16 v0, 0x41

    .line 2
    .line 3
    if-ge p0, v0, :cond_0

    .line 4
    .line 5
    add-int/lit8 p0, p0, -0x30

    .line 6
    .line 7
    :goto_0
    int-to-byte p0, p0

    .line 8
    return p0

    .line 9
    :cond_0
    const/16 v1, 0x61

    .line 10
    .line 11
    if-ge p0, v1, :cond_1

    .line 12
    .line 13
    sub-int/2addr p0, v0

    .line 14
    :goto_1
    add-int/lit8 p0, p0, 0xa

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    sub-int/2addr p0, v1

    .line 18
    goto :goto_1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

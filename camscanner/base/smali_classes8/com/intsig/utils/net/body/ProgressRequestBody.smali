.class public Lcom/intsig/utils/net/body/ProgressRequestBody;
.super Lokhttp3/RequestBody;
.source "ProgressRequestBody.java"


# instance fields
.field private final o0:Lokhttp3/RequestBody;

.field private final 〇OOo8〇0:Lcom/intsig/utils/net/listener/ProgressRequestListener;


# direct methods
.method public constructor <init>(Lokhttp3/RequestBody;Lcom/intsig/utils/net/listener/ProgressRequestListener;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lokhttp3/RequestBody;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/utils/net/body/ProgressRequestBody;->o0:Lokhttp3/RequestBody;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/utils/net/body/ProgressRequestBody;->〇OOo8〇0:Lcom/intsig/utils/net/listener/ProgressRequestListener;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public contentLength()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/utils/net/body/ProgressRequestBody;->o0:Lokhttp3/RequestBody;

    .line 2
    .line 3
    invoke-virtual {v0}, Lokhttp3/RequestBody;->contentLength()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    return-wide v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public contentType()Lokhttp3/MediaType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/utils/net/body/ProgressRequestBody;->o0:Lokhttp3/RequestBody;

    .line 2
    .line 3
    invoke-virtual {v0}, Lokhttp3/RequestBody;->contentType()Lokhttp3/MediaType;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public writeTo(Lokio/BufferedSink;)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/utils/net/body/ProgressRequestBody;->〇OOo8〇0:Lcom/intsig/utils/net/listener/ProgressRequestListener;

    .line 4
    .line 5
    if-eqz v1, :cond_4

    .line 6
    .line 7
    new-instance v1, Lokio/Buffer;

    .line 8
    .line 9
    invoke-direct {v1}, Lokio/Buffer;-><init>()V

    .line 10
    .line 11
    .line 12
    iget-object v2, v0, Lcom/intsig/utils/net/body/ProgressRequestBody;->o0:Lokhttp3/RequestBody;

    .line 13
    .line 14
    invoke-virtual {v2, v1}, Lokhttp3/RequestBody;->writeTo(Lokio/BufferedSink;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1}, Lokio/Buffer;->size()J

    .line 18
    .line 19
    .line 20
    move-result-wide v9

    .line 21
    const-wide/16 v2, -0x1

    .line 22
    .line 23
    cmp-long v4, v9, v2

    .line 24
    .line 25
    if-nez v4, :cond_0

    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    const-wide/16 v2, 0x0

    .line 29
    .line 30
    move-wide v11, v2

    .line 31
    :goto_0
    const/16 v2, 0x800

    .line 32
    .line 33
    int-to-long v6, v2

    .line 34
    add-long v13, v11, v6

    .line 35
    .line 36
    const/4 v8, 0x1

    .line 37
    const/4 v15, 0x0

    .line 38
    cmp-long v16, v13, v9

    .line 39
    .line 40
    if-gez v16, :cond_2

    .line 41
    .line 42
    invoke-interface/range {p1 .. p1}, Lokio/BufferedSink;->buffer()Lokio/Buffer;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    move-object v2, v1

    .line 47
    move-wide v4, v11

    .line 48
    invoke-virtual/range {v2 .. v7}, Lokio/Buffer;->copyTo(Lokio/Buffer;JJ)Lokio/Buffer;

    .line 49
    .line 50
    .line 51
    iget-object v3, v0, Lcom/intsig/utils/net/body/ProgressRequestBody;->〇OOo8〇0:Lcom/intsig/utils/net/listener/ProgressRequestListener;

    .line 52
    .line 53
    if-nez v16, :cond_1

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_1
    const/4 v8, 0x0

    .line 57
    :goto_1
    move-wide v4, v13

    .line 58
    move-wide v6, v9

    .line 59
    invoke-interface/range {v3 .. v8}, Lcom/intsig/utils/net/listener/ProgressRequestListener;->〇o00〇〇Oo(JJZ)V

    .line 60
    .line 61
    .line 62
    move-wide v11, v13

    .line 63
    goto :goto_0

    .line 64
    :cond_2
    invoke-interface/range {p1 .. p1}, Lokio/BufferedSink;->buffer()Lokio/Buffer;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    sub-long v6, v9, v11

    .line 69
    .line 70
    move-object v2, v1

    .line 71
    move-wide v4, v11

    .line 72
    invoke-virtual/range {v2 .. v7}, Lokio/Buffer;->copyTo(Lokio/Buffer;JJ)Lokio/Buffer;

    .line 73
    .line 74
    .line 75
    invoke-interface/range {p1 .. p1}, Lokio/BufferedSink;->flush()V

    .line 76
    .line 77
    .line 78
    iget-object v3, v0, Lcom/intsig/utils/net/body/ProgressRequestBody;->〇OOo8〇0:Lcom/intsig/utils/net/listener/ProgressRequestListener;

    .line 79
    .line 80
    cmp-long v2, v11, v9

    .line 81
    .line 82
    if-nez v2, :cond_3

    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_3
    const/4 v8, 0x0

    .line 86
    :goto_2
    move-wide v4, v11

    .line 87
    move-wide v6, v9

    .line 88
    invoke-interface/range {v3 .. v8}, Lcom/intsig/utils/net/listener/ProgressRequestListener;->〇o00〇〇Oo(JJZ)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1}, Lokio/Buffer;->clear()V

    .line 92
    .line 93
    .line 94
    goto :goto_3

    .line 95
    :cond_4
    iget-object v1, v0, Lcom/intsig/utils/net/body/ProgressRequestBody;->o0:Lokhttp3/RequestBody;

    .line 96
    .line 97
    move-object/from16 v2, p1

    .line 98
    .line 99
    invoke-virtual {v1, v2}, Lokhttp3/RequestBody;->writeTo(Lokio/BufferedSink;)V

    .line 100
    .line 101
    .line 102
    :goto_3
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

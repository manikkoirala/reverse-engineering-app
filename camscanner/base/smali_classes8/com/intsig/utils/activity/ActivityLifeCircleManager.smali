.class public Lcom/intsig/utils/activity/ActivityLifeCircleManager;
.super Ljava/lang/Object;
.source "ActivityLifeCircleManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;,
        Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;
    }
.end annotation


# static fields
.field private static O8:Ljava/lang/String; = "ActivityLifeCircleManager"


# instance fields
.field private 〇080:Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;

.field private 〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

.field private 〇o〇:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o〇(Landroid/app/Activity;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    move-object v0, p1

    .line 11
    check-cast v0, Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 18
    .line 19
    invoke-direct {p0, v0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->o〇0(Landroidx/fragment/app/FragmentManager;)Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iput-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇080:Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;

    .line 24
    .line 25
    :cond_0
    iput-object p1, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o〇:Landroid/app/Activity;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private o〇0(Landroidx/fragment/app/FragmentManager;)Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;
    .locals 2
    .param p1    # Landroidx/fragment/app/FragmentManager;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;

    .line 8
    .line 9
    if-nez p1, :cond_0

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;

    .line 12
    .line 13
    invoke-direct {p1}, Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;-><init>()V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 17
    .line 18
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    sget-object v1, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 23
    .line 24
    invoke-virtual {v0, p1, v1}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 29
    .line 30
    .line 31
    :cond_0
    return-object p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇080()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private 〇o〇(Landroid/app/Activity;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    sget-object p1, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "canMonitor false activity == null"

    .line 7
    .line 8
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return v0

    .line 12
    :cond_0
    instance-of p1, p1, Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    if-eqz p1, :cond_1

    .line 15
    .line 16
    const/4 p1, 0x1

    .line 17
    return p1

    .line 18
    :cond_1
    sget-object p1, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 19
    .line 20
    const-string v1, "canMonitor false activity  is not FragmentActivity"

    .line 21
    .line 22
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static 〇〇888(Landroid/app/Activity;)Lcom/intsig/utils/activity/ActivityLifeCircleManager;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;-><init>(Landroid/app/Activity;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public O8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇080:Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇080:Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
.end method

.method public Oo08()Landroidx/fragment/app/Fragment;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    sget-object v1, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o〇:Landroid/app/Activity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->Oo08()Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0, p1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    sget-object v0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 12
    .line 13
    const-string v1, "monitor fragment is null"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇o〇:Landroid/app/Activity;

    .line 19
    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    sget-object p1, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 23
    .line 24
    const-string p2, "monitor activity is null"

    .line 25
    .line 26
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    invoke-static {v0, p1, p2}, Lcom/intsig/utils/TransitionUtil;->〇o00〇〇Oo(Landroid/app/Activity;Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :catch_0
    move-exception p1

    .line 35
    sget-object p2, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->O8:Ljava/lang/String;

    .line 36
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v1, "startActivityForResult Exception:"

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    :goto_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;)Lcom/intsig/utils/activity/ActivityLifeCircleManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/utils/activity/ActivityLifeCircleManager;->〇080:Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/utils/activity/ActivityLifeCircleManager$MonitorFragment;->〇80O8o8O〇(Lcom/intsig/utils/activity/ActivityLifeCircleManager$LifeCircleListener;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

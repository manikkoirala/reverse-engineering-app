.class public Lcom/intsig/webstorage/WebStorageAccount;
.super Ljava/lang/Object;
.source "WebStorageAccount.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/webstorage/WebStorageAccount;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public O8o08O8O:Z

.field public OO:I

.field public o0:J

.field public o〇00O:Z

.field public 〇08O〇00〇o:Z

.field public 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/webstorage/WebStorageAccount$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/webstorage/WebStorageAccount$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/webstorage/WebStorageAccount;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/webstorage/WebStorageAccount;->o〇00O:Z

    .line 4
    iput-boolean v0, p0, Lcom/intsig/webstorage/WebStorageAccount;->O8o08O8O:Z

    .line 5
    iput p1, p0, Lcom/intsig/webstorage/WebStorageAccount;->OO:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/intsig/webstorage/WebStorageAccount;->o〇00O:Z

    .line 8
    iput-boolean v0, p0, Lcom/intsig/webstorage/WebStorageAccount;->O8o08O8O:Z

    .line 9
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/webstorage/WebStorageAccount;->〇OOo8〇0:Ljava/lang/String;

    .line 10
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/intsig/webstorage/WebStorageAccount;->OO:I

    .line 11
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/intsig/webstorage/WebStorageAccount;->o0:J

    const/4 v1, 0x3

    new-array v1, v1, [Z

    .line 12
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    aget-boolean p1, v1, v0

    .line 13
    iput-boolean p1, p0, Lcom/intsig/webstorage/WebStorageAccount;->〇08O〇00〇o:Z

    const/4 p1, 0x1

    aget-boolean p1, v1, p1

    .line 14
    iput-boolean p1, p0, Lcom/intsig/webstorage/WebStorageAccount;->o〇00O:Z

    const/4 p1, 0x2

    aget-boolean p1, v1, p1

    .line 15
    iput-boolean p1, p0, Lcom/intsig/webstorage/WebStorageAccount;->O8o08O8O:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;L〇0880O0〇/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/webstorage/WebStorageAccount;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-object p2, p0, Lcom/intsig/webstorage/WebStorageAccount;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget p2, p0, Lcom/intsig/webstorage/WebStorageAccount;->OO:I

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    .line 10
    .line 11
    iget-wide v0, p0, Lcom/intsig/webstorage/WebStorageAccount;->o0:J

    .line 12
    .line 13
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 14
    .line 15
    .line 16
    const/4 p2, 0x3

    .line 17
    new-array p2, p2, [Z

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    iget-boolean v1, p0, Lcom/intsig/webstorage/WebStorageAccount;->〇08O〇00〇o:Z

    .line 21
    .line 22
    aput-boolean v1, p2, v0

    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    iget-boolean v1, p0, Lcom/intsig/webstorage/WebStorageAccount;->o〇00O:Z

    .line 26
    .line 27
    aput-boolean v1, p2, v0

    .line 28
    .line 29
    const/4 v0, 0x2

    .line 30
    iget-boolean v1, p0, Lcom/intsig/webstorage/WebStorageAccount;->O8o08O8O:Z

    .line 31
    .line 32
    aput-boolean v1, p2, v0

    .line 33
    .line 34
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.class public Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;
.super Ljava/lang/Object;
.source "CustomOneDriveClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private OO0o〇〇〇〇0(Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 3
    .line 4
    invoke-virtual {v1}, Lcom/onedrive/sdk/core/BaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    instance-of v2, v1, Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;

    .line 9
    .line 10
    if-eqz v2, :cond_0

    .line 11
    .line 12
    check-cast v1, Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;

    .line 13
    .line 14
    invoke-virtual {v1, p1}, Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;->oO80(Landroid/app/Activity;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    .line 15
    .line 16
    .line 17
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    goto :goto_0

    .line 19
    :catch_0
    nop

    .line 20
    :cond_0
    move-object p1, v0

    .line 21
    :goto_0
    if-nez p1, :cond_2

    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/onedrive/sdk/core/BaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-interface {p1, v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    if-eqz p1, :cond_1

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_1
    new-instance p1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    .line 37
    .line 38
    const-string v0, "Unable to authenticate silently or interactively"

    .line 39
    .line 40
    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 41
    .line 42
    invoke-direct {p1, v0, v1}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 43
    .line 44
    .line 45
    throw p1

    .line 46
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 47
    .line 48
    return-object p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private oO80(Lcom/onedrive/sdk/logger/ILogger;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;->O8(Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇080(Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->OO0o〇〇〇〇0(Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public O8(Lcom/onedrive/sdk/concurrency/IExecutors;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;->〇o〇(Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;Lcom/onedrive/sdk/concurrency/IExecutors;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public Oo08(Lcom/onedrive/sdk/core/IClientConfig;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇o〇(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->O8(Lcom/onedrive/sdk/concurrency/IExecutors;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->o〇0(Lcom/onedrive/sdk/http/IHttpProvider;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-direct {v0, v1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->oO80(Lcom/onedrive/sdk/logger/ILogger;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {v0, p1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇8o8o〇(Lcom/onedrive/sdk/serializer/ISerializer;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    return-object p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public o〇0(Lcom/onedrive/sdk/http/IHttpProvider;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;->〇080(Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;Lcom/onedrive/sdk/http/IHttpProvider;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇80〇808〇O(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder$2;

    .line 8
    .line 9
    invoke-direct {v1, p0, p1, p2}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder$2;-><init>(Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)V

    .line 10
    .line 11
    .line 12
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 16
    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public 〇8o8o〇(Lcom/onedrive/sdk/serializer/ISerializer;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/onedrive/sdk/core/BaseClient;->setSerializer(Lcom/onedrive/sdk/serializer/ISerializer;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇o〇(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;->〇o00〇〇Oo(Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;Lcom/onedrive/sdk/authentication/IAuthenticator;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇〇888(Landroid/content/Context;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->validate()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    instance-of v1, v0, Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    check-cast v0, Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/onedrive/sdk/core/BaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/onedrive/sdk/core/BaseClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    iget-object v3, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 31
    .line 32
    invoke-virtual {v3}, Lcom/onedrive/sdk/core/BaseClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;->〇〇888(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/content/Context;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 37
    .line 38
    .line 39
    :cond_0
    iget-object p1, p0, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇080:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient;

    .line 40
    .line 41
    return-object p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

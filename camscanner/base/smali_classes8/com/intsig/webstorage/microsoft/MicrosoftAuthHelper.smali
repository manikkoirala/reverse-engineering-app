.class public abstract Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;
.super Ljava/lang/Object;
.source "MicrosoftAuthHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper$MicrosoftLoginListener;
    }
.end annotation


# instance fields
.field private final O8:I

.field private Oo08:J

.field private final o〇0:Ljava/lang/String;

.field protected final 〇080:Ljava/lang/String;

.field private 〇o00〇〇Oo:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

.field private 〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

.field private 〇〇888:Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const v0, 0x927c0

    .line 5
    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->O8:I

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper$1;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper$1;-><init>(Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇〇888:Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->o〇0()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iput-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 21
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v0, "_key_account_info"

    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iput-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->o〇0:Ljava/lang/String;

    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private OO0o〇〇()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇80〇808〇O()Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAccountInfo;->isExpired()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_0

    .line 29
    .line 30
    const/4 v0, 0x1

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 v0, 0x0

    .line 33
    :goto_0
    return v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method static bridge synthetic 〇080(Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇0〇O0088o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private 〇0〇O0088o()V
    .locals 6

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 6
    .line 7
    const-string v3, "saveLoginSessionInfo"

    .line 8
    .line 9
    invoke-static {v2, v3}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    :try_start_0
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇〇888:Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;->o〇0()Lcom/microsoft/services/msa/LiveConnectSession;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    iget-object v4, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->o〇0:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {v2, v4}, Lcom/microsoft/services/msa/AuthSessionUtils;->saveAuthSession(Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :catch_0
    move-exception v2

    .line 25
    iget-object v4, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 26
    .line 27
    invoke-static {v4, v3, v2}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    .line 29
    .line 30
    :goto_0
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 31
    .line 32
    new-instance v3, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v4, "saveLoginSessionInfo end spendTime: "

    .line 38
    .line 39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 43
    .line 44
    .line 45
    move-result-wide v4

    .line 46
    sub-long/2addr v4, v0

    .line 47
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-static {v2, v0}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private 〇O00()V
    .locals 6

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 6
    .line 7
    const-string v3, "readLoginSessionInfo"

    .line 8
    .line 9
    invoke-static {v2, v3}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    :try_start_0
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇〇888:Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;->o〇0()Lcom/microsoft/services/msa/LiveConnectSession;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    iget-object v4, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->o〇0:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {v2, v4}, Lcom/microsoft/services/msa/AuthSessionUtils;->readAuthSession(Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :catch_0
    move-exception v2

    .line 25
    iget-object v4, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 26
    .line 27
    invoke-static {v4, v3, v2}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    .line 29
    .line 30
    :goto_0
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 31
    .line 32
    new-instance v3, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v4, "readLoginSessionInfo end spendTime: "

    .line 38
    .line 39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 43
    .line 44
    .line 45
    move-result-wide v4

    .line 46
    sub-long/2addr v4, v0

    .line 47
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-static {v2, v0}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private 〇o〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 7
    .line 8
    const-string v1, "please ensure call init method()"

    .line 9
    .line 10
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    throw v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method O8(Landroid/app/Activity;Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper$MicrosoftLoginListener;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper$MicrosoftLoginListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o00〇〇Oo:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇8o8o〇(Landroid/content/Context;)Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o00〇〇Oo:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper$2;

    .line 15
    .line 16
    invoke-direct {v1, p0, p2}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper$2;-><init>(Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper$MicrosoftLoginListener;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p1, v1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇80〇808〇O(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public OO0o〇〇〇〇0()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected abstract Oo08()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public Oooo8o0〇()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇()V

    .line 2
    .line 3
    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "logout"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->logout()V

    .line 18
    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->o〇0:Ljava/lang/String;

    .line 25
    .line 26
    const-string v2, ""

    .line 27
    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/intsig/utils/PreferenceUtil;->oo88o8O(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :catch_0
    move-exception v0

    .line 33
    iget-object v1, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 34
    .line 35
    invoke-static {v1, v0}, Lcom/intsig/webstorage/util/CloudServiceUtils;->O8(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    .line 37
    .line 38
    :goto_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected abstract oO80()[Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method protected abstract o〇0()Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public 〇80〇808〇O()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 5
    .line 6
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAccountInfo;->getAccessToken()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-string v0, ""

    .line 22
    .line 23
    :goto_0
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇8o8o〇(Landroid/content/Context;)Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "init"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o00〇〇Oo:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    return-object p0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇〇888:Lcom/intsig/webstorage/microsoft/CustomMSAAuthenticator;

    .line 18
    .line 19
    invoke-static {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->createWithAuthenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/onedrive/sdk/core/IClientConfig;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    sget-object v2, Lcom/onedrive/sdk/logger/LoggerLevel;->Debug:Lcom/onedrive/sdk/logger/LoggerLevel;

    .line 28
    .line 29
    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->setLoggingLevel(Lcom/onedrive/sdk/logger/LoggerLevel;)V

    .line 30
    .line 31
    .line 32
    new-instance v1, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 33
    .line 34
    invoke-direct {v1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1, v0}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->Oo08(Lcom/onedrive/sdk/core/IClientConfig;)Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    iput-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o00〇〇Oo:Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Lcom/intsig/webstorage/microsoft/CustomOneDriveClient$Builder;->〇〇888(Landroid/content/Context;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    iput-object p1, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇O00()V

    .line 50
    .line 51
    .line 52
    return-object p0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public 〇O8o08O()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇80〇808〇O()Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    xor-int/lit8 v0, v0, 0x1

    .line 13
    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected 〇O〇(Lcom/onedrive/sdk/extensions/IOneDriveClient;)V
    .locals 0
    .param p1    # Lcom/onedrive/sdk/extensions/IOneDriveClient;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇o00〇〇Oo(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "login"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {p1, p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthActivity;->oooO888(Landroid/content/Context;Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;)Landroid/content/Intent;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected 〇〇808〇(Lcom/onedrive/sdk/core/ClientException;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇〇888()Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇〇8O0〇8()Z
    .locals 7

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->OO0o〇〇()Z

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    iget-wide v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->Oo08:J

    .line 12
    .line 13
    const-wide/16 v4, 0x0

    .line 14
    .line 15
    cmp-long v6, v2, v4

    .line 16
    .line 17
    if-lez v6, :cond_0

    .line 18
    .line 19
    const-wide/32 v4, 0x927c0

    .line 20
    .line 21
    .line 22
    add-long/2addr v2, v4

    .line 23
    cmp-long v4, v0, v2

    .line 24
    .line 25
    if-gez v4, :cond_0

    .line 26
    .line 27
    const/4 v0, 0x1

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇()V

    .line 30
    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 33
    .line 34
    const-string v3, "refreshToken begin"

    .line 35
    .line 36
    invoke-static {v2, v3}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    :try_start_0
    iget-object v2, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇o〇:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 40
    .line 41
    invoke-interface {v2}, Lcom/onedrive/sdk/core/IBaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    invoke-interface {v2}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-interface {v2}, Lcom/onedrive/sdk/authentication/IAccountInfo;->refresh()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->OO0o〇〇()Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-eqz v2, :cond_1

    .line 57
    .line 58
    iput-wide v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->Oo08:J

    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇0〇O0088o()V

    .line 61
    .line 62
    .line 63
    :cond_1
    iget-object v0, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 64
    .line 65
    new-instance v1, Ljava/lang/StringBuilder;

    .line 66
    .line 67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .line 69
    .line 70
    const-string v3, "refreshToken end result: "

    .line 71
    .line 72
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-static {v0, v1}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    move v0, v2

    .line 86
    :goto_0
    return v0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    iget-object v1, p0, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->〇080:Ljava/lang/String;

    .line 89
    .line 90
    const-string v2, "refreshToken error"

    .line 91
    .line 92
    invoke-static {v1, v2, v0}, Lcom/intsig/webstorage/util/CloudServiceUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/webstorage/microsoft/MicrosoftAuthHelper;->Oooo8o0〇()V

    .line 96
    .line 97
    .line 98
    const/4 v0, 0x0

    .line 99
    return v0
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

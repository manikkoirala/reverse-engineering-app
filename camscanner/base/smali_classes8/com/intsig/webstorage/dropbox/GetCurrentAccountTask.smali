.class Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;
.super Landroid/os/AsyncTask;
.source "GetCurrentAccountTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask$Callback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/dropbox/core/v2/users/FullAccount;",
        ">;"
    }
.end annotation


# instance fields
.field private final 〇080:Lcom/dropbox/core/v2/DbxClientV2;

.field private final 〇o00〇〇Oo:Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask$Callback;

.field private 〇o〇:Ljava/lang/Exception;


# direct methods
.method constructor <init>(Lcom/dropbox/core/v2/DbxClientV2;Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask$Callback;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇080:Lcom/dropbox/core/v2/DbxClientV2;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇o00〇〇Oo:Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask$Callback;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇080([Ljava/lang/Void;)Lcom/dropbox/core/v2/users/FullAccount;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/dropbox/core/v2/users/FullAccount;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇o00〇〇Oo(Lcom/dropbox/core/v2/users/FullAccount;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected varargs 〇080([Ljava/lang/Void;)Lcom/dropbox/core/v2/users/FullAccount;
    .locals 0

    .line 1
    :try_start_0
    iget-object p1, p0, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇080:Lcom/dropbox/core/v2/DbxClientV2;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/dropbox/core/v2/DbxClientV2Base;->users()Lcom/dropbox/core/v2/users/DbxUserUsersRequests;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/dropbox/core/v2/users/DbxUserUsersRequests;->getCurrentAccount()Lcom/dropbox/core/v2/users/FullAccount;

    .line 8
    .line 9
    .line 10
    move-result-object p1
    :try_end_0
    .catch Lcom/dropbox/core/DbxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    return-object p1

    .line 12
    :catch_0
    move-exception p1

    .line 13
    iput-object p1, p0, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇o〇:Ljava/lang/Exception;

    .line 14
    .line 15
    const/4 p1, 0x0

    .line 16
    return-object p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected 〇o00〇〇Oo(Lcom/dropbox/core/v2/users/FullAccount;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇o〇:Ljava/lang/Exception;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇o00〇〇Oo:Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask$Callback;

    .line 9
    .line 10
    invoke-interface {p1, v0}, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask$Callback;->onError(Ljava/lang/Exception;)V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask;->〇o00〇〇Oo:Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask$Callback;

    .line 15
    .line 16
    invoke-interface {v0, p1}, Lcom/intsig/webstorage/dropbox/GetCurrentAccountTask$Callback;->〇080(Lcom/dropbox/core/v2/users/FullAccount;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

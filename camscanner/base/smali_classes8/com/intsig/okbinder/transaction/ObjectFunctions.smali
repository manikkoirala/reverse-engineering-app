.class public final Lcom/intsig/okbinder/transaction/ObjectFunctions;
.super Ljava/lang/Object;
.source "ObjectFunctions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8:Lcom/intsig/okbinder/transaction/ObjectFunctions$equals$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇080:Lcom/intsig/okbinder/transaction/ObjectFunctions;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o00〇〇Oo:Lcom/intsig/okbinder/transaction/ObjectFunctions$toString$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o〇:Lcom/intsig/okbinder/transaction/ObjectFunctions$hashCode$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/okbinder/transaction/ObjectFunctions;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/okbinder/transaction/ObjectFunctions;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/okbinder/transaction/ObjectFunctions;->〇080:Lcom/intsig/okbinder/transaction/ObjectFunctions;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/okbinder/transaction/ObjectFunctions$toString$1;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/okbinder/transaction/ObjectFunctions$toString$1;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/okbinder/transaction/ObjectFunctions;->〇o00〇〇Oo:Lcom/intsig/okbinder/transaction/ObjectFunctions$toString$1;

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/okbinder/transaction/ObjectFunctions$hashCode$1;

    .line 16
    .line 17
    invoke-direct {v0}, Lcom/intsig/okbinder/transaction/ObjectFunctions$hashCode$1;-><init>()V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/okbinder/transaction/ObjectFunctions;->〇o〇:Lcom/intsig/okbinder/transaction/ObjectFunctions$hashCode$1;

    .line 21
    .line 22
    new-instance v0, Lcom/intsig/okbinder/transaction/ObjectFunctions$equals$1;

    .line 23
    .line 24
    invoke-direct {v0}, Lcom/intsig/okbinder/transaction/ObjectFunctions$equals$1;-><init>()V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/intsig/okbinder/transaction/ObjectFunctions;->O8:Lcom/intsig/okbinder/transaction/ObjectFunctions$equals$1;

    .line 28
    .line 29
    return-void
    .line 30
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public final 〇080(Lcom/intsig/okbinder/transaction/OkBinderFactory$BaseBinder;)V
    .locals 2
    .param p1    # Lcom/intsig/okbinder/transaction/OkBinderFactory$BaseBinder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "binder"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "toString()"

    .line 7
    .line 8
    sget-object v1, Lcom/intsig/okbinder/transaction/ObjectFunctions;->〇o00〇〇Oo:Lcom/intsig/okbinder/transaction/ObjectFunctions$toString$1;

    .line 9
    .line 10
    invoke-virtual {p1, v0, v1}, Lcom/intsig/okbinder/transaction/OkBinderFactory$BaseBinder;->〇o(Ljava/lang/String;Lcom/intsig/okbinder/transaction/OkBinderFactory$Function;)V

    .line 11
    .line 12
    .line 13
    const-string v0, "hashCode()"

    .line 14
    .line 15
    sget-object v1, Lcom/intsig/okbinder/transaction/ObjectFunctions;->〇o〇:Lcom/intsig/okbinder/transaction/ObjectFunctions$hashCode$1;

    .line 16
    .line 17
    invoke-virtual {p1, v0, v1}, Lcom/intsig/okbinder/transaction/OkBinderFactory$BaseBinder;->〇o(Ljava/lang/String;Lcom/intsig/okbinder/transaction/OkBinderFactory$Function;)V

    .line 18
    .line 19
    .line 20
    const-string v0, "equals(java.lang.Object)"

    .line 21
    .line 22
    sget-object v1, Lcom/intsig/okbinder/transaction/ObjectFunctions;->O8:Lcom/intsig/okbinder/transaction/ObjectFunctions$equals$1;

    .line 23
    .line 24
    invoke-virtual {p1, v0, v1}, Lcom/intsig/okbinder/transaction/OkBinderFactory$BaseBinder;->〇o(Ljava/lang/String;Lcom/intsig/okbinder/transaction/OkBinderFactory$Function;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

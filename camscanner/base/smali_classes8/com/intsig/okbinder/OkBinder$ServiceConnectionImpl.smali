.class public final Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;
.super Ljava/lang/Object;
.source "OkBinder.kt"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/okbinder/OkBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServiceConnectionImpl"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/content/ServiceConnection;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final OO:Lcom/intsig/okbinder/ServerInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/okbinder/ServerInfo<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/okbinder/OkBinder$SafeContinuation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/okbinder/OkBinder$SafeContinuation<",
            "Lcom/intsig/okbinder/ServerInfo<",
            "TT;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/okbinder/OkBinder$SafeContinuation;Ljava/lang/Class;Lcom/intsig/okbinder/ServerInfo;)V
    .locals 1
    .param p1    # Lcom/intsig/okbinder/OkBinder$SafeContinuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Class;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/okbinder/ServerInfo;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/okbinder/OkBinder$SafeContinuation<",
            "Lcom/intsig/okbinder/ServerInfo<",
            "TT;>;>;",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/intsig/okbinder/ServerInfo<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "continuation"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "binderClass"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "serviceInfo"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->o0:Lcom/intsig/okbinder/OkBinder$SafeContinuation;

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->〇OOo8〇0:Ljava/lang/Class;

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 24
    .line 25
    invoke-virtual {p3, p0}, Lcom/intsig/okbinder/ServerInfo;->Oo08(Landroid/content/ServiceConnection;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .line 1
    sget-object p1, Lcom/intsig/okbinder/utils/Utils;->〇080:Lcom/intsig/okbinder/utils/Utils;

    .line 2
    .line 3
    const-string v0, "onServiceConnected\uff1a"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Lcom/intsig/okbinder/utils/Utils;->O8(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object p1, Lcom/intsig/okbinder/OkBinder;->〇080:Lcom/intsig/okbinder/OkBinder;

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 11
    .line 12
    invoke-static {p1, v0}, Lcom/intsig/okbinder/OkBinder;->〇080(Lcom/intsig/okbinder/OkBinder;Lcom/intsig/okbinder/ServerInfo;)V

    .line 13
    .line 14
    .line 15
    if-nez p2, :cond_0

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 18
    .line 19
    const/4 p2, 0x0

    .line 20
    invoke-virtual {p1, p2}, Lcom/intsig/okbinder/ServerInfo;->O8(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->o0:Lcom/intsig/okbinder/OkBinder$SafeContinuation;

    .line 24
    .line 25
    iget-object p2, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 26
    .line 27
    new-instance v0, Lcom/intsig/okbinder/ConnectError;

    .line 28
    .line 29
    const-string v1, "service is null"

    .line 30
    .line 31
    invoke-direct {v0, v1}, Lcom/intsig/okbinder/ConnectError;-><init>(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p1, p2, v0}, Lcom/intsig/okbinder/OkBinder$SafeContinuation;->〇o00〇〇Oo(Ljava/lang/Object;Lcom/intsig/okbinder/ConnectError;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->〇OOo8〇0:Ljava/lang/Class;

    .line 39
    .line 40
    invoke-virtual {p1, v0, p2}, Lcom/intsig/okbinder/OkBinder;->〇〇888(Ljava/lang/Class;Landroid/os/IBinder;)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    iget-object p2, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 45
    .line 46
    invoke-virtual {p2, p1}, Lcom/intsig/okbinder/ServerInfo;->O8(Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->o0:Lcom/intsig/okbinder/OkBinder$SafeContinuation;

    .line 50
    .line 51
    iget-object p2, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 52
    .line 53
    invoke-virtual {p1, p2}, Lcom/intsig/okbinder/OkBinder$SafeContinuation;->〇080(Ljava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/okbinder/ServerInfo;->O8(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    sget-object v0, Lcom/intsig/okbinder/OkBinder;->〇080:Lcom/intsig/okbinder/OkBinder;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/okbinder/OkBinder;->〇080(Lcom/intsig/okbinder/OkBinder;Lcom/intsig/okbinder/ServerInfo;)V

    .line 12
    .line 13
    .line 14
    sget-object v0, Lcom/intsig/okbinder/utils/Utils;->〇080:Lcom/intsig/okbinder/utils/Utils;

    .line 15
    .line 16
    new-instance v1, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v2, "onServiceDisconnected name: "

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-virtual {v0, p1}, Lcom/intsig/okbinder/utils/Utils;->O8(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->o0:Lcom/intsig/okbinder/OkBinder$SafeContinuation;

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/okbinder/OkBinder$ServiceConnectionImpl;->OO:Lcom/intsig/okbinder/ServerInfo;

    .line 39
    .line 40
    new-instance v1, Lcom/intsig/okbinder/ConnectError;

    .line 41
    .line 42
    const-string v2, "onServiceDisConnect"

    .line 43
    .line 44
    invoke-direct {v1, v2}, Lcom/intsig/okbinder/ConnectError;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, v0, v1}, Lcom/intsig/okbinder/OkBinder$SafeContinuation;->〇o00〇〇Oo(Ljava/lang/Object;Lcom/intsig/okbinder/ConnectError;)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

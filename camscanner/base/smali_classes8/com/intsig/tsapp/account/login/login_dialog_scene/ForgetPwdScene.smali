.class public final Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;
.super Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;
.source "ForgetPwdScene.kt"

# interfaces
.implements Lcom/intsig/tsapp/account/iview/IForgetPwdView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final oOo〇8o008:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇0O:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lcom/intsig/tsapp/account/presenter/IForgetPwdPresenter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

.field private 〇080OO8〇0:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇0O:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion;

    .line 8
    .line 9
    const-string v0, "ForgetPwdScene"

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/tsapp/account/iview/ILoginScene;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "iLoginScene"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "params"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;)V

    .line 17
    .line 18
    .line 19
    iput-object p3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 20
    .line 21
    new-instance p1, Lcom/intsig/tsapp/account/presenter/impl/ForgetPwdPresenter;

    .line 22
    .line 23
    invoke-direct {p1, p0}, Lcom/intsig/tsapp/account/presenter/impl/ForgetPwdPresenter;-><init>(Lcom/intsig/tsapp/account/iview/IForgetPwdView;)V

    .line 24
    .line 25
    .line 26
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/presenter/IForgetPwdPresenter;

    .line 27
    .line 28
    sget-object p1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇O〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final Oooo8o0〇()V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;->O8()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const-string v2, "mBinding"

    .line 13
    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    move-object v0, v1

    .line 20
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 21
    .line 22
    new-instance v3, L〇00o80oo/〇O8o08O;

    .line 23
    .line 24
    invoke-direct {v3, p0}, L〇00o80oo/〇O8o08O;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 31
    .line 32
    if-nez v0, :cond_1

    .line 33
    .line 34
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    move-object v0, v1

    .line 38
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 39
    .line 40
    new-instance v3, L〇00o80oo/OO0o〇〇;

    .line 41
    .line 42
    invoke-direct {v3, p0}, L〇00o80oo/OO0o〇〇;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 49
    .line 50
    sget-object v3, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 51
    .line 52
    if-ne v0, v3, :cond_5

    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 55
    .line 56
    if-nez v0, :cond_2

    .line 57
    .line 58
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    move-object v0, v1

    .line 62
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->o〇00O:Landroid/widget/TextView;

    .line 63
    .line 64
    const/4 v3, 0x0

    .line 65
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 69
    .line 70
    if-nez v0, :cond_3

    .line 71
    .line 72
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    move-object v0, v1

    .line 76
    :cond_3
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->oOo〇8o008:Landroid/view/View;

    .line 77
    .line 78
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 82
    .line 83
    if-nez v0, :cond_4

    .line 84
    .line 85
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    move-object v0, v1

    .line 89
    :cond_4
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->o〇00O:Landroid/widget/TextView;

    .line 90
    .line 91
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 92
    .line 93
    invoke-virtual {v3}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;->〇o〇()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v3

    .line 97
    new-instance v4, Ljava/lang/StringBuilder;

    .line 98
    .line 99
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .line 101
    .line 102
    const-string v5, "+"

    .line 103
    .line 104
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v3

    .line 114
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    .line 116
    .line 117
    :cond_5
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 118
    .line 119
    if-nez v0, :cond_6

    .line 120
    .line 121
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_6
    move-object v1, v0

    .line 126
    :goto_0
    iget-object v0, v1, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 127
    .line 128
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 129
    .line 130
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;->〇080()Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    .line 136
    .line 137
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇〇808〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇O〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "click get verify code"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 14
    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    const-string p1, "mBinding"

    .line 18
    .line 19
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 p1, 0x0

    .line 23
    :cond_0
    iget-object p1, p1, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 24
    .line 25
    const-string v0, ""

    .line 26
    .line 27
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/presenter/IForgetPwdPresenter;

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    iget-object v2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;->〇080()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;->〇o〇()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    invoke-interface {p1, v0, v1, v2, p0}, Lcom/intsig/tsapp/account/presenter/IForgetPwdPresenter;->〇080(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇808〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    invoke-interface {p0}, Lcom/intsig/tsapp/account/iview/ILoginScene;->OoO8()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public OO0o〇〇〇〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public o8o(I)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "showErrorTips errorMsg is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    const-string v2, "mBinding"

    .line 27
    .line 28
    if-nez v0, :cond_0

    .line 29
    .line 30
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    move-object v0, v1

    .line 34
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 35
    .line 36
    const/4 v3, 0x0

    .line 37
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 41
    .line 42
    if-nez v0, :cond_1

    .line 43
    .line 44
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    move-object v1, v0

    .line 49
    :goto_0
    iget-object v0, v1, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 50
    .line 51
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public 〇080()Landroid/app/Activity;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "null cannot be cast to non-null type android.app.Activity"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    check-cast v0, Landroid/app/Activity;

    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇80〇808〇O()Landroid/view/View;
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v3, "initScene params is "

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sget v1, Lcom/intsig/camscanner/account/R$layout;->scene_forget_pwd:I

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    const-string v2, "bind(sceneView)"

    .line 41
    .line 42
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iput-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneForgetPwdBinding;

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->Oooo8o0〇()V

    .line 48
    .line 49
    .line 50
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public 〇o〇Oo0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)V
    .locals 17

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "go2VerifyCodeInputPage account is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    move-object/from16 v2, p1

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v3, ", areaCode is "

    .line 19
    .line 20
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    move-object/from16 v6, p2

    .line 24
    .line 25
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v3, ", emailPostal is "

    .line 29
    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    move-object/from16 v9, p3

    .line 34
    .line 35
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v3, ", fromWhere is "

    .line 39
    .line 40
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    move-object/from16 v11, p4

    .line 44
    .line 45
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    new-instance v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;

    .line 60
    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 62
    .line 63
    .line 64
    move-result-object v14

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 66
    .line 67
    .line 68
    move-result-object v15

    .line 69
    new-instance v13, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 70
    .line 71
    const/4 v5, 0x0

    .line 72
    const/4 v7, 0x0

    .line 73
    const/4 v8, 0x0

    .line 74
    const/4 v10, 0x0

    .line 75
    const/16 v12, 0x5a

    .line 76
    .line 77
    const/16 v16, 0x0

    .line 78
    .line 79
    move-object v3, v13

    .line 80
    move-object/from16 v4, p1

    .line 81
    .line 82
    move-object v2, v13

    .line 83
    move-object/from16 v13, v16

    .line 84
    .line 85
    invoke-direct/range {v3 .. v13}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 86
    .line 87
    .line 88
    invoke-direct {v1, v14, v15, v2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;)V

    .line 89
    .line 90
    .line 91
    const/4 v2, 0x2

    .line 92
    const/4 v3, 0x0

    .line 93
    const/4 v4, 0x0

    .line 94
    invoke-static {v0, v1, v4, v2, v3}, Lcom/intsig/tsapp/account/iview/ILoginScene$DefaultImpls;->〇080(Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;ZILjava/lang/Object;)V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/16 v0, 0x2711

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

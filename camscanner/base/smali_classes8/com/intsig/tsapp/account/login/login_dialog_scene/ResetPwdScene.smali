.class public final Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;
.super Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;
.source "ResetPwdScene.kt"

# interfaces
.implements Lcom/intsig/tsapp/account/iview/ISettingPwdView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion;,
        Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$WhenMappings;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final oOo〇8o008:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇0O:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

.field private final 〇080OO8〇0:Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇0O:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion;

    .line 8
    .line 9
    const-string v0, "ResetPwdScene"

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/tsapp/account/iview/ILoginScene;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "iLoginScene"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "params"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;)V

    .line 17
    .line 18
    .line 19
    iput-object p3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 20
    .line 21
    sget-object p1, Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;->NO_WHERE:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 24
    .line 25
    new-instance p1, Lcom/intsig/tsapp/account/presenter/impl/SettingPwdPresenter;

    .line 26
    .line 27
    invoke-direct {p1, p0}, Lcom/intsig/tsapp/account/presenter/impl/SettingPwdPresenter;-><init>(Lcom/intsig/tsapp/account/iview/ISettingPwdView;)V

    .line 28
    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇O888o0o(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final OoO8(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/view/View;)V
    .locals 10

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "click reset pwd, fromWhere is "

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 31
    .line 32
    const/4 v1, 0x0

    .line 33
    const-string v2, "mBinding"

    .line 34
    .line 35
    if-nez v0, :cond_0

    .line 36
    .line 37
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    move-object v0, v1

    .line 41
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->O8o08O8O:Landroid/widget/AutoCompleteTextView;

    .line 42
    .line 43
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v9

    .line 51
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-static {v0, v9}, Lcom/intsig/tsapp/account/util/AccountUtils;->OoO8(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const-string v3, "getCnPasswordCheckTip(context, password)"

    .line 60
    .line 61
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    const/4 v3, 0x1

    .line 69
    const/4 v4, 0x0

    .line 70
    if-lez v0, :cond_1

    .line 71
    .line 72
    const/4 v0, 0x1

    .line 73
    goto :goto_0

    .line 74
    :cond_1
    const/4 v0, 0x0

    .line 75
    :goto_0
    if-eqz v0, :cond_4

    .line 76
    .line 77
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 78
    .line 79
    if-nez p1, :cond_2

    .line 80
    .line 81
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    move-object p1, v1

    .line 85
    :cond_2
    iget-object p1, p1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 86
    .line 87
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 88
    .line 89
    .line 90
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 91
    .line 92
    if-nez p1, :cond_3

    .line 93
    .line 94
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_3
    move-object v1, p1

    .line 99
    :goto_1
    iget-object p1, v1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 100
    .line 101
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 102
    .line 103
    .line 104
    move-result-object p0

    .line 105
    invoke-static {p0, v9}, Lcom/intsig/tsapp/account/util/AccountUtils;->OoO8(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object p0

    .line 109
    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    .line 111
    .line 112
    return-void

    .line 113
    :cond_4
    invoke-direct {p0, v9}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o800o8O(Ljava/lang/String;)Z

    .line 114
    .line 115
    .line 116
    move-result v0

    .line 117
    if-nez v0, :cond_5

    .line 118
    .line 119
    return-void

    .line 120
    :cond_5
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 121
    .line 122
    if-nez v0, :cond_6

    .line 123
    .line 124
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    goto :goto_2

    .line 128
    :cond_6
    move-object v1, v0

    .line 129
    :goto_2
    iget-object v0, v1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->O8o08O8O:Landroid/widget/AutoCompleteTextView;

    .line 130
    .line 131
    invoke-static {v0}, Lcom/intsig/utils/KeyboardUtils;->〇〇888(Landroid/view/View;)V

    .line 132
    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 135
    .line 136
    sget-object v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$WhenMappings;->〇080:[I

    .line 137
    .line 138
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 139
    .line 140
    .line 141
    move-result v0

    .line 142
    aget v0, v1, v0

    .line 143
    .line 144
    if-eq v0, v3, :cond_b

    .line 145
    .line 146
    const/4 v1, 0x2

    .line 147
    if-eq v0, v1, :cond_a

    .line 148
    .line 149
    const/4 v1, 0x3

    .line 150
    if-eq v0, v1, :cond_9

    .line 151
    .line 152
    const/4 v1, 0x4

    .line 153
    if-eq v0, v1, :cond_8

    .line 154
    .line 155
    const/4 v1, 0x5

    .line 156
    if-eq v0, v1, :cond_7

    .line 157
    .line 158
    const-string p0, "UN DO"

    .line 159
    .line 160
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    goto/16 :goto_3

    .line 164
    .line 165
    :cond_7
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;

    .line 166
    .line 167
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 168
    .line 169
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v4

    .line 173
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 174
    .line 175
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇080()Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object v5

    .line 179
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 180
    .line 181
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->O8()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v7

    .line 185
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 186
    .line 187
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇〇888()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v8

    .line 191
    move-object v6, v9

    .line 192
    invoke-interface/range {v3 .. v8}, Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    goto :goto_3

    .line 196
    :cond_8
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;

    .line 197
    .line 198
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 199
    .line 200
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v4

    .line 204
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 205
    .line 206
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇080()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v5

    .line 210
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 211
    .line 212
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇〇888()Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v7

    .line 216
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 217
    .line 218
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->Oo08()Ljava/lang/String;

    .line 219
    .line 220
    .line 221
    move-result-object v8

    .line 222
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 223
    .line 224
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->o〇0()Ljava/lang/String;

    .line 225
    .line 226
    .line 227
    move-result-object p0

    .line 228
    move-object v6, v9

    .line 229
    move-object v9, p0

    .line 230
    invoke-interface/range {v3 .. v9}, Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    .line 232
    .line 233
    goto :goto_3

    .line 234
    :cond_9
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;

    .line 235
    .line 236
    iget-object v4, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 237
    .line 238
    const-string v5, "mobile"

    .line 239
    .line 240
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 241
    .line 242
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇080()Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object v6

    .line 246
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 247
    .line 248
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 249
    .line 250
    .line 251
    move-result-object v7

    .line 252
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 253
    .line 254
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->O8()Ljava/lang/String;

    .line 255
    .line 256
    .line 257
    move-result-object v8

    .line 258
    invoke-interface/range {v3 .. v9}, Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;->O8(Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    .line 260
    .line 261
    goto :goto_3

    .line 262
    :cond_a
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;

    .line 263
    .line 264
    iget-object v4, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 265
    .line 266
    const-string v5, "email"

    .line 267
    .line 268
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 269
    .line 270
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇080()Ljava/lang/String;

    .line 271
    .line 272
    .line 273
    move-result-object v6

    .line 274
    const/4 v7, 0x0

    .line 275
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 276
    .line 277
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->O8()Ljava/lang/String;

    .line 278
    .line 279
    .line 280
    move-result-object v8

    .line 281
    invoke-interface/range {v3 .. v9}, Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;->O8(Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    .line 283
    .line 284
    goto :goto_3

    .line 285
    :cond_b
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇080OO8〇0:Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;

    .line 286
    .line 287
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 288
    .line 289
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇080()Ljava/lang/String;

    .line 290
    .line 291
    .line 292
    move-result-object p0

    .line 293
    invoke-interface {p1, p0, v9}, Lcom/intsig/tsapp/account/presenter/ISettingPwdPresenter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    .line 295
    .line 296
    :goto_3
    return-void
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇0〇O0088o(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private final o800o8O(Ljava/lang/String;)Z
    .locals 5

    .line 1
    invoke-static {p1}, Lcom/intsig/comm/util/StringUtilDelegate;->〇080(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    sget v0, Lcom/intsig/camscanner/account/R$string;->a_msg_pwd_contain_blank:I

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 15
    .line 16
    .line 17
    return v1

    .line 18
    :cond_0
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->o8()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    const/4 v2, 0x1

    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 26
    .line 27
    const-string v3, "isKoreaLang"

    .line 28
    .line 29
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-static {p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->O〇O〇oO(Ljava/lang/String;)Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-nez p1, :cond_1

    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    sget v0, Lcom/intsig/camscanner/account/R$string;->cs_620_korea_14:I

    .line 43
    .line 44
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 45
    .line 46
    .line 47
    return v1

    .line 48
    :cond_1
    return v2

    .line 49
    :cond_2
    invoke-static {p1}, Lcom/intsig/comm/util/StringUtilDelegate;->Oo08(Ljava/lang/String;)Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-nez p1, :cond_3

    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    sget v3, Lcom/intsig/camscanner/account/R$string;->pwd_format_wrong:I

    .line 68
    .line 69
    new-array v2, v2, [Ljava/lang/Object;

    .line 70
    .line 71
    const/4 v4, 0x6

    .line 72
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 73
    .line 74
    .line 75
    move-result-object v4

    .line 76
    aput-object v4, v2, v1

    .line 77
    .line 78
    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->o〇0(Landroid/content/Context;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    return v1

    .line 86
    :cond_3
    return v2
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private static final oo88o8O(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "showEmailAlreadyRegisteredPrompt click sign in"

    .line 9
    .line 10
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    new-instance p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene;

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    const/4 v1, 0x0

    .line 31
    invoke-direct {p2, v0, p0, v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene$Companion$EmailPwdLoginParams;)V

    .line 32
    .line 33
    .line 34
    const/4 p0, 0x0

    .line 35
    const/4 v0, 0x2

    .line 36
    invoke-static {p1, p2, p0, v0, v1}, Lcom/intsig/tsapp/account/iview/ILoginScene$DefaultImpls;->〇080(Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;ZILjava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final 〇0〇O0088o(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    const/16 p1, 0x91

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/16 p1, 0x81

    .line 12
    .line 13
    :goto_0
    iget-object p2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    const-string v1, "mBinding"

    .line 17
    .line 18
    if-nez p2, :cond_1

    .line 19
    .line 20
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    move-object p2, v0

    .line 24
    :cond_1
    iget-object p2, p2, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->O8o08O8O:Landroid/widget/AutoCompleteTextView;

    .line 25
    .line 26
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setInputType(I)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 30
    .line 31
    if-nez p1, :cond_2

    .line 32
    .line 33
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    move-object p1, v0

    .line 37
    :cond_2
    iget-object p1, p1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->O8o08O8O:Landroid/widget/AutoCompleteTextView;

    .line 38
    .line 39
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 40
    .line 41
    if-nez p0, :cond_3

    .line 42
    .line 43
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_3
    move-object v0, p0

    .line 48
    :goto_1
    iget-object p0, v0, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->O8o08O8O:Landroid/widget/AutoCompleteTextView;

    .line 49
    .line 50
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 51
    .line 52
    .line 53
    move-result-object p0

    .line 54
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    .line 55
    .line 56
    .line 57
    move-result p0

    .line 58
    invoke-virtual {p1, p0}, Landroid/widget/EditText;->setSelection(I)V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private final 〇O00()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->〇o〇()Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;->NO_WHERE:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 10
    .line 11
    :cond_0
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    const-string v3, "mBinding"

    .line 21
    .line 22
    if-nez v1, :cond_1

    .line 23
    .line 24
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    move-object v1, v2

    .line 28
    :cond_1
    iget-object v1, v1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 29
    .line 30
    invoke-static {v0, v1}, Lcom/intsig/tsapp/account/util/AccountUtils;->o8O〇(Landroid/content/Context;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 38
    .line 39
    if-nez v1, :cond_2

    .line 40
    .line 41
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    move-object v1, v2

    .line 45
    :cond_2
    iget-object v1, v1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->O8o08O8O:Landroid/widget/AutoCompleteTextView;

    .line 46
    .line 47
    invoke-static {v0, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 51
    .line 52
    if-nez v0, :cond_3

    .line 53
    .line 54
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    move-object v0, v2

    .line 58
    :cond_3
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 59
    .line 60
    new-instance v1, L〇00o80oo/〇8〇0〇o〇O;

    .line 61
    .line 62
    invoke-direct {v1, p0}, L〇00o80oo/〇8〇0〇o〇O;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 69
    .line 70
    sget-object v1, Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;->EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 71
    .line 72
    if-eq v0, v1, :cond_5

    .line 73
    .line 74
    sget-object v1, Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;->PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 75
    .line 76
    if-eq v0, v1, :cond_5

    .line 77
    .line 78
    sget-object v1, Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;->A_KEY_LOGIN:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 79
    .line 80
    if-ne v0, v1, :cond_4

    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_4
    sget v0, Lcom/intsig/camscanner/account/R$string;->c_title_reset_password:I

    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_5
    :goto_0
    sget v0, Lcom/intsig/camscanner/account/R$string;->cs_542_renew_19:I

    .line 87
    .line 88
    :goto_1
    invoke-static {v0}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 93
    .line 94
    if-nez v1, :cond_6

    .line 95
    .line 96
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    move-object v1, v2

    .line 100
    :cond_6
    iget-object v1, v1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 101
    .line 102
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    .line 104
    .line 105
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 106
    .line 107
    if-nez v1, :cond_7

    .line 108
    .line 109
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    move-object v1, v2

    .line 113
    :cond_7
    iget-object v1, v1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->o〇00O:Landroid/widget/TextView;

    .line 114
    .line 115
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    .line 117
    .line 118
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 119
    .line 120
    if-nez v0, :cond_8

    .line 121
    .line 122
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    move-object v0, v2

    .line 126
    :cond_8
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 127
    .line 128
    new-instance v1, L〇00o80oo/O〇O〇oO;

    .line 129
    .line 130
    invoke-direct {v1, p0}, L〇00o80oo/O〇O〇oO;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 134
    .line 135
    .line 136
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 137
    .line 138
    if-nez v0, :cond_9

    .line 139
    .line 140
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    move-object v0, v2

    .line 144
    :cond_9
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->o〇00O:Landroid/widget/TextView;

    .line 145
    .line 146
    new-instance v1, L〇00o80oo/o8oO〇;

    .line 147
    .line 148
    invoke-direct {v1, p0}, L〇00o80oo/o8oO〇;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    .line 153
    .line 154
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 155
    .line 156
    if-nez v0, :cond_a

    .line 157
    .line 158
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    goto :goto_2

    .line 162
    :cond_a
    move-object v2, v0

    .line 163
    :goto_2
    iget-object v0, v2, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 164
    .line 165
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 166
    .line 167
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;->oO80()Z

    .line 168
    .line 169
    .line 170
    move-result v1

    .line 171
    if-eqz v1, :cond_b

    .line 172
    .line 173
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    const-string v2, "null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams"

    .line 178
    .line 179
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    .line 181
    .line 182
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 183
    .line 184
    const/16 v2, 0x15

    .line 185
    .line 186
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 187
    .line 188
    .line 189
    sget v1, Lcom/intsig/camscanner/account/R$drawable;->ic_common_close_24px:I

    .line 190
    .line 191
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 192
    .line 193
    .line 194
    :cond_b
    return-void
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method private static final 〇O888o0o(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "showEmailAlreadyRegisteredPrompt click change email"

    .line 9
    .line 10
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    invoke-interface {p0}, Lcom/intsig/tsapp/account/iview/ILoginScene;->OoO8()V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇〇8O0〇8(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇O〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oo88o8O(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇〇808〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->OoO8(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇8O0〇8(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    invoke-interface {p0}, Lcom/intsig/tsapp/account/iview/ILoginScene;->OoO8()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public OO0o〇〇〇〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public Ooo8〇〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public o8〇()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public oO80(ILjava/lang/String;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "showEmailAlreadyRegisteredPrompt errorMsg is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, ", email is "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p2

    .line 28
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    new-instance p2, Lcom/intsig/app/AlertDialog$Builder;

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-direct {p2, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    .line 39
    .line 40
    sget v0, Lcom/intsig/camscanner/account/R$string;->dlg_title:I

    .line 41
    .line 42
    invoke-virtual {p2, v0}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {p2, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 46
    .line 47
    .line 48
    sget p1, Lcom/intsig/camscanner/account/R$string;->a_btn_change_email:I

    .line 49
    .line 50
    new-instance v0, L〇00o80oo/o〇8oOO88;

    .line 51
    .line 52
    invoke-direct {v0, p0}, L〇00o80oo/o〇8oOO88;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p2, p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 56
    .line 57
    .line 58
    sget p1, Lcom/intsig/camscanner/account/R$string;->login_btn:I

    .line 59
    .line 60
    new-instance v0, L〇00o80oo/o〇O;

    .line 61
    .line 62
    invoke-direct {v0, p0}, L〇00o80oo/o〇O;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2, p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 66
    .line 67
    .line 68
    :try_start_0
    invoke-virtual {p2}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :catch_0
    move-exception p1

    .line 77
    sget-object p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 78
    .line 79
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    .line 81
    .line 82
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
.end method

.method public o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "go2PwdLoginPage account is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, ", areaCode is "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const/4 v0, 0x0

    .line 32
    if-eqz p2, :cond_1

    .line 33
    .line 34
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-nez v1, :cond_0

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    const/4 v1, 0x0

    .line 42
    goto :goto_1

    .line 43
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 44
    :goto_1
    if-eqz v1, :cond_2

    .line 45
    .line 46
    new-instance p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene;

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    new-instance v10, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene$Companion$EmailPwdLoginParams;

    .line 57
    .line 58
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 59
    .line 60
    const/4 v7, 0x0

    .line 61
    const/16 v8, 0x8

    .line 62
    .line 63
    const/4 v9, 0x0

    .line 64
    move-object v3, v10

    .line 65
    move-object v5, p1

    .line 66
    move-object v6, p3

    .line 67
    invoke-direct/range {v3 .. v9}, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene$Companion$EmailPwdLoginParams;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 68
    .line 69
    .line 70
    invoke-direct {p2, v1, v2, v10}, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene$Companion$EmailPwdLoginParams;)V

    .line 71
    .line 72
    .line 73
    goto :goto_2

    .line 74
    :cond_2
    new-instance v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;

    .line 75
    .line 76
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 81
    .line 82
    .line 83
    move-result-object v3

    .line 84
    new-instance v4, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 85
    .line 86
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 87
    .line 88
    invoke-direct {v4, v5, p1, p2, p3}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;)V

    .line 92
    .line 93
    .line 94
    move-object p2, v1

    .line 95
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    const/4 p3, 0x2

    .line 100
    const/4 v1, 0x0

    .line 101
    invoke-static {p1, p2, v0, p3, v1}, Lcom/intsig/tsapp/account/iview/ILoginScene$DefaultImpls;->〇080(Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;ZILjava/lang/Object;)V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public 〇080()Landroid/app/Activity;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "null cannot be cast to non-null type android.app.Activity"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    check-cast v0, Landroid/app/Activity;

    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇80〇808〇O()Landroid/view/View;
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v3, "initScene params is "

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sget v1, Lcom/intsig/camscanner/account/R$layout;->scene_reset_pwd:I

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    const-string v2, "bind(sceneView)"

    .line 41
    .line 42
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iput-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->〇O00()V

    .line 48
    .line 49
    .line 50
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "showErrorTips errorMsg is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    const-string v2, "mBinding"

    .line 27
    .line 28
    if-nez v0, :cond_0

    .line 29
    .line 30
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    move-object v0, v1

    .line 34
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 35
    .line 36
    const/4 v3, 0x0

    .line 37
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;

    .line 41
    .line 42
    if-nez v0, :cond_1

    .line 43
    .line 44
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    move-object v1, v0

    .line 49
    :goto_0
    iget-object v0, v1, Lcom/intsig/camscanner/account/databinding/SceneResetPwdBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 50
    .line 51
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public 〇oOO8O8()Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;->O8o08O8O:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/16 v0, 0x2715

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public final enum Lcom/intsig/tsapp/account/login/login_type/LoginType;
.super Ljava/lang/Enum;
.source "LoginType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/tsapp/account/login/login_type/LoginType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/tsapp/account/login/login_type/LoginType;

.field public static final enum A_KEY:Lcom/intsig/tsapp/account/login/login_type/LoginType;

.field public static final enum EMAIL:Lcom/intsig/tsapp/account/login/login_type/LoginType;

.field public static final enum GOOGLE:Lcom/intsig/tsapp/account/login/login_type/LoginType;

.field private static final LAST_LOGIN_TYPE:Ljava/lang/String; = "last_login_type"

.field public static final enum NO_RECORD:Lcom/intsig/tsapp/account/login/login_type/LoginType;

.field public static final enum PHONE:Lcom/intsig/tsapp/account/login/login_type/LoginType;

.field public static final enum VALIDATE_CODE:Lcom/intsig/tsapp/account/login/login_type/LoginType;

.field public static final enum WE_CHAT:Lcom/intsig/tsapp/account/login/login_type/LoginType;


# instance fields
.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    const-string v1, "we_chat"

    .line 4
    .line 5
    const-string v2, "WE_CHAT"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/tsapp/account/login/login_type/LoginType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sput-object v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->WE_CHAT:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 12
    .line 13
    new-instance v1, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 14
    .line 15
    const-string v2, "validate_code"

    .line 16
    .line 17
    const-string v4, "VALIDATE_CODE"

    .line 18
    .line 19
    const/4 v5, 0x1

    .line 20
    invoke-direct {v1, v4, v5, v2}, Lcom/intsig/tsapp/account/login/login_type/LoginType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sput-object v1, Lcom/intsig/tsapp/account/login/login_type/LoginType;->VALIDATE_CODE:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 26
    .line 27
    const-string v4, "email"

    .line 28
    .line 29
    const-string v6, "EMAIL"

    .line 30
    .line 31
    const/4 v7, 0x2

    .line 32
    invoke-direct {v2, v6, v7, v4}, Lcom/intsig/tsapp/account/login/login_type/LoginType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 33
    .line 34
    .line 35
    sput-object v2, Lcom/intsig/tsapp/account/login/login_type/LoginType;->EMAIL:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 36
    .line 37
    new-instance v4, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 38
    .line 39
    const-string v6, "phone"

    .line 40
    .line 41
    const-string v8, "PHONE"

    .line 42
    .line 43
    const/4 v9, 0x3

    .line 44
    invoke-direct {v4, v8, v9, v6}, Lcom/intsig/tsapp/account/login/login_type/LoginType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 45
    .line 46
    .line 47
    sput-object v4, Lcom/intsig/tsapp/account/login/login_type/LoginType;->PHONE:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 48
    .line 49
    new-instance v6, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 50
    .line 51
    const-string v8, "a_key"

    .line 52
    .line 53
    const-string v10, "A_KEY"

    .line 54
    .line 55
    const/4 v11, 0x4

    .line 56
    invoke-direct {v6, v10, v11, v8}, Lcom/intsig/tsapp/account/login/login_type/LoginType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 57
    .line 58
    .line 59
    sput-object v6, Lcom/intsig/tsapp/account/login/login_type/LoginType;->A_KEY:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 60
    .line 61
    new-instance v8, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 62
    .line 63
    const-string v10, "google"

    .line 64
    .line 65
    const-string v12, "GOOGLE"

    .line 66
    .line 67
    const/4 v13, 0x5

    .line 68
    invoke-direct {v8, v12, v13, v10}, Lcom/intsig/tsapp/account/login/login_type/LoginType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 69
    .line 70
    .line 71
    sput-object v8, Lcom/intsig/tsapp/account/login/login_type/LoginType;->GOOGLE:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 72
    .line 73
    new-instance v10, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 74
    .line 75
    const-string v12, "no_record"

    .line 76
    .line 77
    const-string v14, "NO_RECORD"

    .line 78
    .line 79
    const/4 v15, 0x6

    .line 80
    invoke-direct {v10, v14, v15, v12}, Lcom/intsig/tsapp/account/login/login_type/LoginType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 81
    .line 82
    .line 83
    sput-object v10, Lcom/intsig/tsapp/account/login/login_type/LoginType;->NO_RECORD:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 84
    .line 85
    const/4 v12, 0x7

    .line 86
    new-array v12, v12, [Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 87
    .line 88
    aput-object v0, v12, v3

    .line 89
    .line 90
    aput-object v1, v12, v5

    .line 91
    .line 92
    aput-object v2, v12, v7

    .line 93
    .line 94
    aput-object v4, v12, v9

    .line 95
    .line 96
    aput-object v6, v12, v11

    .line 97
    .line 98
    aput-object v8, v12, v13

    .line 99
    .line 100
    aput-object v10, v12, v15

    .line 101
    .line 102
    sput-object v12, Lcom/intsig/tsapp/account/login/login_type/LoginType;->$VALUES:[Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->type:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static catchLastLoginType()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/tsapp/account/login/login_type/LoginType;->NO_RECORD:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->getType()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const-string v2, "last_login_type"

    .line 12
    .line 13
    invoke-virtual {v0, v2, v1}, Lcom/intsig/utils/PreferenceUtil;->OO0o〇〇(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static hasNeverLogged()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->NO_RECORD:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->getType()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->catchLastLoginType()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static isAKeyLastLogin()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->A_KEY:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->getType()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->catchLastLoginType()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static isEmailLastLogin()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->EMAIL:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->getType()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->catchLastLoginType()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static isGoogleLastLogin()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->GOOGLE:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->getType()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->catchLastLoginType()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static isMobileLastLogin()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->PHONE:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->getType()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->catchLastLoginType()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static isWeChatLastLogin()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->WE_CHAT:Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->getType()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->catchLastLoginType()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static recordLastLoginType(Lcom/intsig/tsapp/account/login/login_type/LoginType;)V
    .locals 2
    .param p0    # Lcom/intsig/tsapp/account/login/login_type/LoginType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "last_login_type"

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->getType()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    invoke-virtual {v0, v1, p0}, Lcom/intsig/utils/PreferenceUtil;->oo88o8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/tsapp/account/login/login_type/LoginType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static values()[Lcom/intsig/tsapp/account/login/login_type/LoginType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->$VALUES:[Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/tsapp/account/login/login_type/LoginType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/tsapp/account/login/login_type/LoginType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->type:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_type/LoginType;->type:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

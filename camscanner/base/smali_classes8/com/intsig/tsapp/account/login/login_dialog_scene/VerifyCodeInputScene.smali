.class public final Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;
.super Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;
.source "VerifyCodeInputScene.kt"

# interfaces
.implements Lcom/intsig/tsapp/account/iview/IVerifyCodeView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final ooo0〇〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇8〇oO〇〇8o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/util/CountdownTimer;

.field private OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Lcom/intsig/tsapp/account/login_task/QueryUserTask;

.field private oOo0:Ljava/lang/Integer;

.field private oOo〇8o008:Ljava/lang/String;

.field private o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

.field private final 〇080OO8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Lcom/intsig/app/ProgressDialogClient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇8〇oO〇〇8o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion;

    .line 8
    .line 9
    const-string v0, "VerifyCodeInputScene"

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/tsapp/account/iview/ILoginScene;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "iLoginScene"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "params"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;)V

    .line 17
    .line 18
    .line 19
    iput-object p3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 20
    .line 21
    new-instance p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$mPresenter$2;

    .line 22
    .line 23
    invoke-direct {p1, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$mPresenter$2;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 24
    .line 25
    .line 26
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇080OO8〇0:Lkotlin/Lazy;

    .line 31
    .line 32
    const/4 p1, -0x1

    .line 33
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->oOo0:Ljava/lang/Integer;

    .line 38
    .line 39
    sget-object p1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->NO_WHERE:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 40
    .line 41
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final O8〇o(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    invoke-interface {p0}, Lcom/intsig/tsapp/account/iview/ILoginScene;->OoO8()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇00〇8(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final OOO〇O0(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;I)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    new-instance v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$initTimer$1$1;

    .line 11
    .line 12
    invoke-direct {v1, p1, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$initTimer$1$1;-><init>(ILcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/utils/ext/ContextExtKt;->〇〇888(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final Oo8Oo00oo()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "resetSendVCode"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    const-string v2, "mBinding"

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    move-object v0, v1

    .line 19
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->oOo0:Lcom/intsig/view/VerifyCodeRectStyleView;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/view/VerifyCodeRectStyleView;->O8()V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 25
    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    move-object v0, v1

    .line 32
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇0O:Landroid/widget/TextView;

    .line 33
    .line 34
    const/4 v3, 0x1

    .line 35
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 39
    .line 40
    if-nez v0, :cond_2

    .line 41
    .line 42
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    move-object v0, v1

    .line 46
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇0O:Landroid/widget/TextView;

    .line 47
    .line 48
    sget v3, Lcom/intsig/camscanner/account/R$string;->a_label_reget_verifycode:I

    .line 49
    .line 50
    invoke-static {v3}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 58
    .line 59
    if-nez v0, :cond_3

    .line 60
    .line 61
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_3
    move-object v1, v0

    .line 66
    :goto_0
    iget-object v0, v1, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇0O:Landroid/widget/TextView;

    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    sget v2, Lcom/intsig/camscanner/account/R$color;->cs_color_brand:I

    .line 73
    .line 74
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static final synthetic OoO8(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇o(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇080OO8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static final o0ooO(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V
    .locals 8

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "click tvVerifyCodeNotReceive, fromWhere "

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 31
    .line 32
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    const-string v1, "secondary_validation_login_unreceived"

    .line 37
    .line 38
    const-string v2, "args_pwd"

    .line 39
    .line 40
    const-string v3, "args_account_type"

    .line 41
    .line 42
    const-string v4, "/account/none_receive_verify_code"

    .line 43
    .line 44
    if-eqz v0, :cond_0

    .line 45
    .line 46
    const-string p1, "email"

    .line 47
    .line 48
    invoke-static {v1, p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇00O0O0(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->Oo0O0o8()V

    .line 52
    .line 53
    .line 54
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {v0, v4}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-virtual {v0, v3, p1}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    const-string v1, "args_email"

    .line 73
    .line 74
    invoke-virtual {p1, v1, v0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 79
    .line 80
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇〇888()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object p0

    .line 84
    invoke-virtual {p1, v2, p0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 85
    .line 86
    .line 87
    move-result-object p0

    .line 88
    invoke-virtual {p0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    goto/16 :goto_0

    .line 92
    .line 93
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 94
    .line 95
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8〇8〇O80(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    const-string v5, "args_phone_number"

    .line 100
    .line 101
    const-string v6, "args_area_code"

    .line 102
    .line 103
    const-string v7, "mobile"

    .line 104
    .line 105
    if-eqz v0, :cond_1

    .line 106
    .line 107
    invoke-static {v1, v7}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇00O0O0(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->Oo0O0o8()V

    .line 111
    .line 112
    .line 113
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-virtual {p1, v4}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    invoke-virtual {p1, v3, v7}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 126
    .line 127
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    invoke-virtual {p1, v6, v0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 136
    .line 137
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    invoke-virtual {p1, v5, v0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 146
    .line 147
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇〇888()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object p0

    .line 151
    invoke-virtual {p1, v2, p0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 152
    .line 153
    .line 154
    move-result-object p0

    .line 155
    invoke-virtual {p0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 156
    .line 157
    .line 158
    goto/16 :goto_0

    .line 159
    .line 160
    :cond_1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 161
    .line 162
    invoke-direct {p0, v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇8(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 163
    .line 164
    .line 165
    move-result v0

    .line 166
    if-eqz v0, :cond_2

    .line 167
    .line 168
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->Oo0O0o8()V

    .line 169
    .line 170
    .line 171
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    invoke-virtual {p1, v4}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    invoke-virtual {p1, v3, v7}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 180
    .line 181
    .line 182
    move-result-object p1

    .line 183
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 184
    .line 185
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    invoke-virtual {p1, v6, v0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 190
    .line 191
    .line 192
    move-result-object p1

    .line 193
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 194
    .line 195
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v0

    .line 199
    invoke-virtual {p1, v5, v0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 200
    .line 201
    .line 202
    move-result-object p1

    .line 203
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 204
    .line 205
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇〇888()Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object p0

    .line 209
    invoke-virtual {p1, v2, p0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 210
    .line 211
    .line 212
    move-result-object p0

    .line 213
    invoke-virtual {p0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 214
    .line 215
    .line 216
    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 218
    .line 219
    invoke-direct {p0, v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o8(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 220
    .line 221
    .line 222
    move-result v0

    .line 223
    if-eqz v0, :cond_3

    .line 224
    .line 225
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->Oo0O0o8()V

    .line 226
    .line 227
    .line 228
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 229
    .line 230
    .line 231
    move-result-object p1

    .line 232
    invoke-virtual {p1, v4}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 233
    .line 234
    .line 235
    move-result-object p1

    .line 236
    invoke-virtual {p1, v3, v7}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 237
    .line 238
    .line 239
    move-result-object p1

    .line 240
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 241
    .line 242
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object v0

    .line 246
    invoke-virtual {p1, v6, v0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 247
    .line 248
    .line 249
    move-result-object p1

    .line 250
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 251
    .line 252
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    invoke-virtual {p1, v5, v0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 257
    .line 258
    .line 259
    move-result-object p1

    .line 260
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 261
    .line 262
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇〇888()Ljava/lang/String;

    .line 263
    .line 264
    .line 265
    move-result-object p0

    .line 266
    invoke-virtual {p1, v2, p0}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 267
    .line 268
    .line 269
    move-result-object p0

    .line 270
    invoke-virtual {p0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 271
    .line 272
    .line 273
    goto :goto_0

    .line 274
    :cond_3
    const-string p0, "NOT RECEIVE>>> UN DO"

    .line 275
    .line 276
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    .line 278
    .line 279
    :goto_0
    return-void
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private final o8(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-eq p1, v0, :cond_1

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_VERIFY_CODE_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 10
    .line 11
    if-eq p1, v0, :cond_1

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_PWD_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 14
    .line 15
    if-eq p1, v0, :cond_1

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 18
    .line 19
    if-ne p1, v0, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 p1, 0x0

    .line 23
    goto :goto_1

    .line 24
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 25
    :goto_1
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static final synthetic o800o8O(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static final synthetic oo88o8O()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private final oo〇()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->o〇0()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->NO_WHERE:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 10
    .line 11
    :cond_0
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->Oo08()Ljava/lang/Integer;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->oOo0:Ljava/lang/Integer;

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    const-string v3, "mBinding"

    .line 29
    .line 30
    if-nez v1, :cond_1

    .line 31
    .line 32
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    move-object v1, v2

    .line 36
    :cond_1
    iget-object v1, v1, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->oOo0:Lcom/intsig/view/VerifyCodeRectStyleView;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/view/VerifyCodeRectStyleView;->getEtVerify()Landroid/widget/EditText;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-static {v0, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 46
    .line 47
    if-nez v0, :cond_2

    .line 48
    .line 49
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    move-object v0, v2

    .line 53
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 54
    .line 55
    new-instance v1, L〇00o80oo/〇80;

    .line 56
    .line 57
    invoke-direct {v1, p0}, L〇00o80oo/〇80;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 64
    .line 65
    if-nez v0, :cond_3

    .line 66
    .line 67
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    move-object v0, v2

    .line 71
    :cond_3
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 72
    .line 73
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    sget v4, Lcom/intsig/camscanner/account/R$string;->cs_638_phone_verifycode:I

    .line 78
    .line 79
    const/4 v5, 0x1

    .line 80
    new-array v5, v5, [Ljava/lang/Object;

    .line 81
    .line 82
    iget-object v6, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 83
    .line 84
    invoke-virtual {v6}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v6

    .line 88
    iget-object v7, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 89
    .line 90
    invoke-virtual {v7}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v7

    .line 94
    invoke-direct {p0, v6, v7}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇O8〇〇o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v6

    .line 98
    const/4 v7, 0x0

    .line 99
    aput-object v6, v5, v7

    .line 100
    .line 101
    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    .line 107
    .line 108
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 109
    .line 110
    if-nez v0, :cond_4

    .line 111
    .line 112
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    move-object v0, v2

    .line 116
    :cond_4
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->oOo0:Lcom/intsig/view/VerifyCodeRectStyleView;

    .line 117
    .line 118
    new-instance v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$initViews$2;

    .line 119
    .line 120
    invoke-direct {v1, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$initViews$2;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {v0, v1}, Lcom/intsig/view/VerifyCodeRectStyleView;->setMCompleteListener(Lcom/intsig/view/VerifyCodeRectStyleView$VerifyCodeInputCompleteListener;)V

    .line 124
    .line 125
    .line 126
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 127
    .line 128
    if-nez v0, :cond_5

    .line 129
    .line 130
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    move-object v0, v2

    .line 134
    :cond_5
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇0O:Landroid/widget/TextView;

    .line 135
    .line 136
    new-instance v1, L〇00o80oo/Ooo;

    .line 137
    .line 138
    invoke-direct {v1, p0}, L〇00o80oo/Ooo;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 145
    .line 146
    if-nez v0, :cond_6

    .line 147
    .line 148
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    move-object v0, v2

    .line 152
    :cond_6
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 153
    .line 154
    new-instance v1, L〇00o80oo/〇O〇80o08O;

    .line 155
    .line 156
    invoke-direct {v1, p0}, L〇00o80oo/〇O〇80o08O;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    .line 161
    .line 162
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 163
    .line 164
    if-nez v0, :cond_7

    .line 165
    .line 166
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    move-object v0, v2

    .line 170
    :cond_7
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 171
    .line 172
    new-instance v1, L〇00o80oo/ooo〇8oO;

    .line 173
    .line 174
    invoke-direct {v1, p0}, L〇00o80oo/ooo〇8oO;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    .line 179
    .line 180
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 181
    .line 182
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 183
    .line 184
    if-eq v0, v1, :cond_8

    .line 185
    .line 186
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 187
    .line 188
    if-eq v0, v1, :cond_8

    .line 189
    .line 190
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 191
    .line 192
    .line 193
    move-result v0

    .line 194
    if-eqz v0, :cond_b

    .line 195
    .line 196
    :cond_8
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 197
    .line 198
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->O8()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v0

    .line 202
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 203
    .line 204
    .line 205
    move-result v0

    .line 206
    if-nez v0, :cond_b

    .line 207
    .line 208
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 209
    .line 210
    if-nez v0, :cond_9

    .line 211
    .line 212
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    move-object v0, v2

    .line 216
    :cond_9
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 217
    .line 218
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 219
    .line 220
    .line 221
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 222
    .line 223
    if-nez v0, :cond_a

    .line 224
    .line 225
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    goto :goto_0

    .line 229
    :cond_a
    move-object v2, v0

    .line 230
    :goto_0
    iget-object v0, v2, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 231
    .line 232
    const/16 v1, 0x8

    .line 233
    .line 234
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 235
    .line 236
    .line 237
    :cond_b
    return-void
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method private static final o〇0OOo〇0(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "showEmailAlreadyRegisteredPrompt click sign in"

    .line 9
    .line 10
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    new-instance p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene;

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    const/4 v1, 0x0

    .line 31
    invoke-direct {p2, v0, p0, v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene$Companion$EmailPwdLoginParams;)V

    .line 32
    .line 33
    .line 34
    const/4 p0, 0x0

    .line 35
    const/4 v0, 0x2

    .line 36
    invoke-static {p1, p2, p0, v0, v1}, Lcom/intsig/tsapp/account/iview/ILoginScene$DefaultImpls;->〇080(Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;ZILjava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private final o〇8(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-eq p1, v0, :cond_1

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 10
    .line 11
    if-eq p1, v0, :cond_1

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 14
    .line 15
    if-eq p1, v0, :cond_1

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 18
    .line 19
    if-ne p1, v0, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 p1, 0x0

    .line 23
    goto :goto_1

    .line 24
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 25
    :goto_1
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private final o〇O8〇〇o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    if-eqz v0, :cond_2

    .line 14
    .line 15
    invoke-static {p1}, Lcom/intsig/comm/account_data/AccountHelper;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    goto :goto_2

    .line 20
    :cond_2
    invoke-static {p1}, Lcom/intsig/comm/account_data/AccountHelper;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v1, "+"

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string p2, " "

    .line 38
    .line 39
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    :goto_2
    return-object p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇〇0〇()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "initTimer"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/util/CountdownTimer;->o〇0()Lcom/intsig/util/CountdownTimer;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8o08O8O:Lcom/intsig/util/CountdownTimer;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const/16 v1, 0x3c

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/util/CountdownTimer;->〇80〇808〇O(I)V

    .line 19
    .line 20
    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8o08O8O:Lcom/intsig/util/CountdownTimer;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    new-instance v1, L〇00o80oo/O0o〇〇Oo;

    .line 26
    .line 27
    invoke-direct {v1, p0}, L〇00o80oo/O0o〇〇Oo;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/util/CountdownTimer;->OO0o〇〇〇〇0(Lcom/intsig/util/CountdownTimer$OnCountdownListener;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇〇0o()V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private final 〇00(I)Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0x66

    .line 2
    .line 3
    if-eq p1, v0, :cond_2

    .line 4
    .line 5
    const/16 v0, 0x6b

    .line 6
    .line 7
    if-eq p1, v0, :cond_1

    .line 8
    .line 9
    const/16 v0, 0xd3

    .line 10
    .line 11
    if-eq p1, v0, :cond_0

    .line 12
    .line 13
    packed-switch p1, :pswitch_data_0

    .line 14
    .line 15
    .line 16
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_error_validate_number:I

    .line 17
    .line 18
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    goto :goto_0

    .line 23
    :pswitch_0
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_global_toast_network_error:I

    .line 24
    .line 25
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_send_sms_error_211:I

    .line 31
    .line 32
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_error_validate_number:I

    .line 38
    .line 39
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    goto :goto_0

    .line 44
    :cond_2
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_error_phone:I

    .line 45
    .line 46
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    :goto_0
    return-object p1

    .line 51
    :pswitch_data_0
    .packed-switch -0x65
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private final 〇0000OOO()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 4
    .line 5
    if-eq v0, v1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v0, Lcom/intsig/tsapp/account/login_task/QueryUserTask;

    .line 9
    .line 10
    new-instance v3, Lcom/intsig/tsapp/account/login_task/QueryUserTask$Companion$QueryUserParams;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    new-instance v2, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$initQueryTask$1;

    .line 19
    .line 20
    invoke-direct {v2, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$initQueryTask$1;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {v3, v1, v2}, Lcom/intsig/tsapp/account/login_task/QueryUserTask$Companion$QueryUserParams;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 24
    .line 25
    .line 26
    const-wide/16 v4, 0x0

    .line 27
    .line 28
    const-wide/16 v6, 0x0

    .line 29
    .line 30
    const/4 v8, 0x6

    .line 31
    const/4 v9, 0x0

    .line 32
    move-object v2, v0

    .line 33
    invoke-direct/range {v2 .. v9}, Lcom/intsig/tsapp/account/login_task/QueryUserTask;-><init>(Lcom/intsig/tsapp/account/login_task/QueryUserTask$Companion$QueryUserParams;JJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 34
    .line 35
    .line 36
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o8〇OO0〇0o:Lcom/intsig/tsapp/account/login_task/QueryUserTask;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/view/countdown/CustomCountDownTimer;->〇80〇808〇O()V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private static final 〇00〇8(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V
    .locals 7

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o08〇〇0O()V

    .line 7
    .line 8
    .line 9
    sget-object p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 12
    .line 13
    new-instance v1, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v2, "click tvVerifyCodeResend, fromWhere "

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 34
    .line 35
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O0o〇〇o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-eqz v0, :cond_0

    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p0

    .line 57
    invoke-interface {p1, v0, p0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    goto/16 :goto_0

    .line 61
    .line 62
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 63
    .line 64
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8〇8〇O80(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    if-eqz v0, :cond_1

    .line 69
    .line 70
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 75
    .line 76
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 81
    .line 82
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object p0

    .line 86
    invoke-interface {p1, v0, p0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    goto/16 :goto_0

    .line 90
    .line 91
    :cond_1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 92
    .line 93
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-eqz v0, :cond_2

    .line 98
    .line 99
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 104
    .line 105
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 110
    .line 111
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇〇888()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object p0

    .line 115
    invoke-interface {p1, v0, p0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    goto/16 :goto_0

    .line 119
    .line 120
    :cond_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 121
    .line 122
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    const/4 v1, 0x0

    .line 127
    const/4 v2, 0x1

    .line 128
    if-eqz v0, :cond_3

    .line 129
    .line 130
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 131
    .line 132
    .line 133
    move-result-object p1

    .line 134
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 135
    .line 136
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object p0

    .line 140
    invoke-interface {p1, v2, p0, v1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->o〇0(ZLjava/lang/String;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    goto/16 :goto_0

    .line 144
    .line 145
    :cond_3
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 146
    .line 147
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 148
    .line 149
    .line 150
    move-result v0

    .line 151
    const/4 v3, 0x0

    .line 152
    if-eqz v0, :cond_4

    .line 153
    .line 154
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 155
    .line 156
    .line 157
    move-result-object p1

    .line 158
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 159
    .line 160
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object v0

    .line 164
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 165
    .line 166
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object p0

    .line 170
    invoke-interface {p1, v3, v0, p0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->o〇0(ZLjava/lang/String;Ljava/lang/String;)V

    .line 171
    .line 172
    .line 173
    goto :goto_0

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 175
    .line 176
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 177
    .line 178
    .line 179
    move-result v0

    .line 180
    if-eqz v0, :cond_5

    .line 181
    .line 182
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 183
    .line 184
    .line 185
    move-result-object p1

    .line 186
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 187
    .line 188
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 189
    .line 190
    .line 191
    move-result-object p0

    .line 192
    invoke-interface {p1, v2, p0, v1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇〇888(ZLjava/lang/String;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    goto :goto_0

    .line 196
    :cond_5
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 197
    .line 198
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8O0880(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 199
    .line 200
    .line 201
    move-result v0

    .line 202
    if-eqz v0, :cond_6

    .line 203
    .line 204
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 205
    .line 206
    .line 207
    move-result-object p1

    .line 208
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 209
    .line 210
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 211
    .line 212
    .line 213
    move-result-object v0

    .line 214
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 215
    .line 216
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object p0

    .line 220
    invoke-interface {p1, v3, v0, p0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇〇888(ZLjava/lang/String;Ljava/lang/String;)V

    .line 221
    .line 222
    .line 223
    goto :goto_0

    .line 224
    :cond_6
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 225
    .line 226
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 227
    .line 228
    .line 229
    move-result v0

    .line 230
    if-eqz v0, :cond_7

    .line 231
    .line 232
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 233
    .line 234
    .line 235
    move-result-object v1

    .line 236
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 237
    .line 238
    .line 239
    move-result-object p1

    .line 240
    const-string v0, "null cannot be cast to non-null type android.app.Activity"

    .line 241
    .line 242
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    move-object v2, p1

    .line 246
    check-cast v2, Landroid/app/Activity;

    .line 247
    .line 248
    const/4 v3, 0x0

    .line 249
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 250
    .line 251
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇080()Ljava/lang/String;

    .line 252
    .line 253
    .line 254
    move-result-object v4

    .line 255
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 256
    .line 257
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 258
    .line 259
    .line 260
    move-result-object v5

    .line 261
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 262
    .line 263
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->〇o〇()Ljava/lang/String;

    .line 264
    .line 265
    .line 266
    move-result-object v6

    .line 267
    invoke-interface/range {v1 .. v6}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->Oo08(Landroid/app/Activity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    .line 269
    .line 270
    goto :goto_0

    .line 271
    :cond_7
    const-string p0, "UN DO"

    .line 272
    .line 273
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    .line 275
    .line 276
    :goto_0
    return-void
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private final 〇08O8o〇0()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "stopCountDown"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8o08O8O:Lcom/intsig/util/CountdownTimer;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/util/CountdownTimer;->Oo08()V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8o08O8O:Lcom/intsig/util/CountdownTimer;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/util/CountdownTimer;->OO0o〇〇〇〇0(Lcom/intsig/util/CountdownTimer$OnCountdownListener;)V

    .line 21
    .line 22
    .line 23
    :cond_1
    iput-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8o08O8O:Lcom/intsig/util/CountdownTimer;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static final synthetic 〇0〇O0088o(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic 〇O00(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇〇〇0〇〇0(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final synthetic 〇O888o0o(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)Lcom/intsig/tsapp/account/login_task/QueryUserTask;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o8〇OO0〇0o:Lcom/intsig/tsapp/account/login_task/QueryUserTask;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇0OOo〇0(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇O〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8〇o(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇o(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->O8()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v2, "click tvVerifyCodeCheckEmail, emailPostal is "

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O〇8O8〇008()Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->O8()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    invoke-interface {p1, p0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->oO80(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇oo〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->Oo8Oo00oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private final 〇〇0o()V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "startCountDown"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 9
    .line 10
    const-string v1, "mBinding"

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    move-object v0, v2

    .line 19
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->oOo0:Lcom/intsig/view/VerifyCodeRectStyleView;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/view/VerifyCodeRectStyleView;->O8()V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 25
    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    move-object v0, v2

    .line 32
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇0O:Landroid/widget/TextView;

    .line 33
    .line 34
    const/4 v3, 0x0

    .line 35
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 39
    .line 40
    if-nez v0, :cond_2

    .line 41
    .line 42
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    move-object v0, v2

    .line 46
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇0O:Landroid/widget/TextView;

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    sget v5, Lcom/intsig/camscanner/account/R$color;->cs_color_text_1:I

    .line 53
    .line 54
    invoke-static {v4, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 62
    .line 63
    if-nez v0, :cond_3

    .line 64
    .line 65
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    move-object v0, v2

    .line 69
    :cond_3
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->〇0O:Landroid/widget/TextView;

    .line 70
    .line 71
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    sget v4, Lcom/intsig/camscanner/account/R$string;->cs_638_phone_verifycode_resend:I

    .line 76
    .line 77
    const/4 v5, 0x1

    .line 78
    new-array v5, v5, [Ljava/lang/Object;

    .line 79
    .line 80
    iget-object v6, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8o08O8O:Lcom/intsig/util/CountdownTimer;

    .line 81
    .line 82
    if-eqz v6, :cond_4

    .line 83
    .line 84
    invoke-virtual {v6}, Lcom/intsig/util/CountdownTimer;->〇〇888()I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    :cond_4
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    aput-object v2, v5, v3

    .line 97
    .line 98
    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8o08O8O:Lcom/intsig/util/CountdownTimer;

    .line 106
    .line 107
    if-eqz v0, :cond_5

    .line 108
    .line 109
    invoke-virtual {v0}, Lcom/intsig/util/CountdownTimer;->〇8o8o〇()V

    .line 110
    .line 111
    .line 112
    :cond_5
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static synthetic 〇〇808〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->OOO〇O0(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇8O0〇8(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o0ooO(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇〇0〇〇0(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "showEmailAlreadyRegisteredPrompt click change email"

    .line 9
    .line 10
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    invoke-interface {p0}, Lcom/intsig/tsapp/account/iview/ILoginScene;->OoO8()V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public O08000()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->oOo0:Ljava/lang/Integer;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/16 v1, 0xd9

    .line 11
    .line 12
    if-ne v0, v1, :cond_1

    .line 13
    .line 14
    const/4 v0, 0x1

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 17
    :goto_1
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public O80〇O〇080()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇0O:Lcom/intsig/app/ProgressDialogClient;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/app/ProgressDialogClient;->〇080()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public O880oOO08(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "go2AutoLoginByEmail email is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    new-instance v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene;

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    new-instance v11, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene$Companion$EmailPwdLoginParams;

    .line 38
    .line 39
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 40
    .line 41
    const/4 v8, 0x0

    .line 42
    const/16 v9, 0x8

    .line 43
    .line 44
    const/4 v10, 0x0

    .line 45
    move-object v4, v11

    .line 46
    move-object v6, p1

    .line 47
    move-object v7, p2

    .line 48
    invoke-direct/range {v4 .. v10}, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene$Companion$EmailPwdLoginParams;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {v1, v2, v3, v11}, Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/EmailPwdLoginScene$Companion$EmailPwdLoginParams;)V

    .line 52
    .line 53
    .line 54
    const/4 p1, 0x2

    .line 55
    const/4 p2, 0x0

    .line 56
    const/4 v2, 0x0

    .line 57
    invoke-static {v0, v1, v2, p1, p2}, Lcom/intsig/tsapp/account/iview/ILoginScene$DefaultImpls;->〇080(Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;ZILjava/lang/Object;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final O8ooOoo〇()Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public OO0o〇〇〇〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public OO8oO0o〇(ILjava/lang/String;)V
    .locals 3

    .line 1
    sget-object p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "sendErrorMessage errorType is "

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object p2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    const-string v1, "mBinding"

    .line 27
    .line 28
    if-nez p2, :cond_0

    .line 29
    .line 30
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    move-object p2, v0

    .line 34
    :cond_0
    iget-object p2, p2, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->o〇00O:Landroid/widget/TextView;

    .line 35
    .line 36
    const/4 v2, 0x0

    .line 37
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 38
    .line 39
    .line 40
    iget-object p2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 41
    .line 42
    if-nez p2, :cond_1

    .line 43
    .line 44
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    move-object v0, p2

    .line 49
    :goto_0
    iget-object p2, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->o〇00O:Landroid/widget/TextView;

    .line 50
    .line 51
    invoke-direct {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇00(I)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public OOO8o〇〇(Ljava/lang/String;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "saveVCodeToken"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->oOo〇8o008:Ljava/lang/String;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public Oo0O0o8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->O8o08O8O:Lcom/intsig/util/CountdownTimer;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/util/CountdownTimer;->Oo08()V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->Oo8Oo00oo()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public Ooo()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/tsapp/account/iview/ILoginScene;->O8〇o〇88()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDestroyed()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "null cannot be cast to non-null type android.app.Activity"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    check-cast v0, Landroid/app/Activity;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public o08〇〇0O()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "resetVCode"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇〇0o()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public oO80(ILjava/lang/String;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "showEmailAlreadyRegisteredPrompt errorMsg is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, ", email is "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p2

    .line 28
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    new-instance p2, Lcom/intsig/app/AlertDialog$Builder;

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-direct {p2, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    .line 39
    .line 40
    sget v0, Lcom/intsig/camscanner/account/R$string;->dlg_title:I

    .line 41
    .line 42
    invoke-virtual {p2, v0}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {p2, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 46
    .line 47
    .line 48
    sget p1, Lcom/intsig/camscanner/account/R$string;->a_btn_change_email:I

    .line 49
    .line 50
    new-instance v0, L〇00o80oo/oO00OOO;

    .line 51
    .line 52
    invoke-direct {v0, p0}, L〇00o80oo/oO00OOO;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p2, p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 56
    .line 57
    .line 58
    sget p1, Lcom/intsig/camscanner/account/R$string;->login_btn:I

    .line 59
    .line 60
    new-instance v0, L〇00o80oo/O000;

    .line 61
    .line 62
    invoke-direct {v0, p0}, L〇00o80oo/O000;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2, p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 66
    .line 67
    .line 68
    :try_start_0
    invoke-virtual {p2}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :catch_0
    move-exception p1

    .line 77
    sget-object p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 78
    .line 79
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    .line 81
    .line 82
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
.end method

.method public o〇oO(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->isDestroyed()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇0O:Lcom/intsig/app/ProgressDialogClient;

    .line 9
    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const-string v1, "null cannot be cast to non-null type android.app.Activity"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    check-cast v0, Landroid/app/Activity;

    .line 22
    .line 23
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-static {v0, p1}, Lcom/intsig/app/ProgressDialogClient;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)Lcom/intsig/app/ProgressDialogClient;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇0O:Lcom/intsig/app/ProgressDialogClient;

    .line 32
    .line 33
    :cond_1
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇0O:Lcom/intsig/app/ProgressDialogClient;

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/app/ProgressDialogClient;->O8()V

    .line 38
    .line 39
    .line 40
    :cond_2
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public 〇080()Landroid/app/Activity;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "null cannot be cast to non-null type android.app.Activity"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    check-cast v0, Landroid/app/Activity;

    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇80(Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "go2ResetPwdPage fromWhere is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    move-object/from16 v2, p1

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v3, ", account is "

    .line 19
    .line 20
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    move-object/from16 v5, p2

    .line 24
    .line 25
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v3, ", areaCode is "

    .line 29
    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    move-object/from16 v6, p3

    .line 34
    .line 35
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v3, ", phoneToken is "

    .line 39
    .line 40
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    move-object/from16 v7, p4

    .line 44
    .line 45
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    new-instance v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;

    .line 60
    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 62
    .line 63
    .line 64
    move-result-object v14

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 66
    .line 67
    .line 68
    move-result-object v15

    .line 69
    new-instance v13, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 70
    .line 71
    const/4 v8, 0x0

    .line 72
    const/4 v9, 0x0

    .line 73
    const/4 v10, 0x0

    .line 74
    const/4 v11, 0x0

    .line 75
    const/16 v12, 0xf0

    .line 76
    .line 77
    const/16 v16, 0x0

    .line 78
    .line 79
    move-object v3, v13

    .line 80
    move-object/from16 v4, p1

    .line 81
    .line 82
    move-object v2, v13

    .line 83
    move-object/from16 v13, v16

    .line 84
    .line 85
    invoke-direct/range {v3 .. v13}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;-><init>(Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 86
    .line 87
    .line 88
    invoke-direct {v1, v14, v15, v2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;)V

    .line 89
    .line 90
    .line 91
    const/4 v2, 0x2

    .line 92
    const/4 v3, 0x0

    .line 93
    const/4 v4, 0x0

    .line 94
    invoke-static {v0, v1, v4, v2, v3}, Lcom/intsig/tsapp/account/iview/ILoginScene$DefaultImpls;->〇080(Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;ZILjava/lang/Object;)V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public 〇80〇808〇O()Landroid/view/View;
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v3, "initScene params is "

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sget v1, Lcom/intsig/camscanner/account/R$layout;->scene_verify_code_input:I

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    const-string v2, "bind(sceneView)"

    .line 41
    .line 42
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iput-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇〇0〇()V

    .line 48
    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->oo〇()V

    .line 51
    .line 52
    .line 53
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇0000OOO()V

    .line 54
    .line 55
    .line 56
    return-object v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public 〇8o8o〇()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->〇8o8o〇()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 9
    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    const-string v1, "mBinding"

    .line 13
    .line 14
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    :cond_0
    iget-object v1, v1, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->oOo0:Lcom/intsig/view/VerifyCodeRectStyleView;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/view/VerifyCodeRectStyleView;->getEtVerify()Landroid/widget/EditText;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "mBinding"

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->o〇00O:Landroid/widget/TextView;

    .line 13
    .line 14
    const/4 v3, 0x0

    .line 15
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 19
    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    move-object v0, v1

    .line 26
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->o〇00O:Landroid/widget/TextView;

    .line 27
    .line 28
    if-eqz p1, :cond_2

    .line 29
    .line 30
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    if-nez v4, :cond_3

    .line 35
    .line 36
    :cond_2
    const/4 v3, 0x1

    .line 37
    :cond_3
    if-eqz v3, :cond_4

    .line 38
    .line 39
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_error_validate_number:I

    .line 40
    .line 41
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    :cond_4
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 49
    .line 50
    if-nez p1, :cond_5

    .line 51
    .line 52
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_5
    move-object v1, p1

    .line 57
    :goto_0
    iget-object p1, v1, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->oOo0:Lcom/intsig/view/VerifyCodeRectStyleView;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/view/VerifyCodeRectStyleView;->O8()V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public 〇oOO8O8()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene$Companion$VerifyCodeInputSceneParams;->o〇0()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇o〇()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->〇o〇()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "dismiss"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->〇08O8o〇0()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o8〇OO0〇0o:Lcom/intsig/tsapp/account/login_task/QueryUserTask;

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/view/countdown/CustomCountDownTimer;->OO0o〇〇〇〇0()V

    .line 19
    .line 20
    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o8〇OO0〇0o:Lcom/intsig/tsapp/account/login_task/QueryUserTask;

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    iget-object v2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;

    .line 29
    .line 30
    if-nez v2, :cond_1

    .line 31
    .line 32
    const-string v2, "mBinding"

    .line 33
    .line 34
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    move-object v0, v2

    .line 39
    :goto_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneVerifyCodeInputBinding;->oOo0:Lcom/intsig/view/VerifyCodeRectStyleView;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/view/VerifyCodeRectStyleView;->getEtVerify()Landroid/widget/EditText;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-static {v1, v0}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public 〇o〇8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/VerifyCodeInputScene;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "go2SetPwdForPhone areaCode is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    move-object/from16 v2, p1

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v3, ", account is "

    .line 19
    .line 20
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    move-object/from16 v5, p2

    .line 24
    .line 25
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v3, ", phoneToken is "

    .line 29
    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    move-object/from16 v7, p3

    .line 34
    .line 35
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v3, ", vCOde is "

    .line 39
    .line 40
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    move-object/from16 v8, p4

    .line 44
    .line 45
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    new-instance v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;

    .line 60
    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 62
    .line 63
    .line 64
    move-result-object v14

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 66
    .line 67
    .line 68
    move-result-object v15

    .line 69
    new-instance v13, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;

    .line 70
    .line 71
    sget-object v4, Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;->PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 72
    .line 73
    const/4 v9, 0x0

    .line 74
    const/4 v10, 0x0

    .line 75
    const/4 v11, 0x0

    .line 76
    const/16 v12, 0x80

    .line 77
    .line 78
    const/16 v16, 0x0

    .line 79
    .line 80
    move-object v3, v13

    .line 81
    move-object/from16 v6, p1

    .line 82
    .line 83
    move-object v2, v13

    .line 84
    move-object/from16 v13, v16

    .line 85
    .line 86
    invoke-direct/range {v3 .. v13}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;-><init>(Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 87
    .line 88
    .line 89
    invoke-direct {v1, v14, v15, v2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/ResetPwdScene$Companion$ResetPwdParams;)V

    .line 90
    .line 91
    .line 92
    const/4 v2, 0x2

    .line 93
    const/4 v3, 0x0

    .line 94
    const/4 v4, 0x0

    .line 95
    invoke-static {v0, v1, v4, v2, v3}, Lcom/intsig/tsapp/account/iview/ILoginScene$DefaultImpls;->〇080(Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;ZILjava/lang/Object;)V

    .line 96
    .line 97
    .line 98
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/16 v0, 0x2716

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

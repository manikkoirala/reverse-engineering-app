.class public final Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;
.super Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;
.source "MobilePwdLoginScene.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO〇00〇8oO:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8〇OO0〇0o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:Lorg/json/JSONObject;

.field private oOo〇8o008:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

.field private 〇080OO8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

.field private 〇0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion;

    .line 8
    .line 9
    const-string v0, "MobilePwdLoginScene"

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o8〇OO0〇0o:Ljava/lang/String;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/tsapp/account/iview/ILoginScene;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "iLoginScene"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;)V

    .line 12
    .line 13
    .line 14
    iput-object p3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 15
    .line 16
    new-instance p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$mViewModel$2;

    .line 17
    .line 18
    invoke-direct {p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$mViewModel$2;-><init>(Lcom/intsig/tsapp/account/iview/ILoginScene;)V

    .line 19
    .line 20
    .line 21
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->O8o08O8O:Lkotlin/Lazy;

    .line 26
    .line 27
    const-string p1, ""

    .line 28
    .line 29
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇080OO8〇0:Ljava/lang/String;

    .line 30
    .line 31
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 32
    .line 33
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->oOo〇8o008:Ljava/lang/String;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final synthetic O8ooOoo〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O8〇o()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 2
    .line 3
    const-string v1, "mBinding"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v2

    .line 12
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->〇O〇〇O8:Landroid/widget/TextView;

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    invoke-static {v0, v3}, Lcom/intsig/tsapp/account/util/AccountUtils;->O8ooOoo〇(Landroid/widget/TextView;Landroid/content/Context;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 22
    .line 23
    if-nez v0, :cond_1

    .line 24
    .line 25
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    move-object v0, v2

    .line 29
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 30
    .line 31
    new-instance v3, L〇00o80oo/o〇〇0〇;

    .line 32
    .line 33
    invoke-direct {v3, p0}, L〇00o80oo/o〇〇0〇;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    .line 38
    .line 39
    new-instance v0, Lorg/json/JSONObject;

    .line 40
    .line 41
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 42
    .line 43
    .line 44
    sget-object v3, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 45
    .line 46
    invoke-virtual {v3}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    const-string v5, "from_part"

    .line 51
    .line 52
    invoke-virtual {v0, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 53
    .line 54
    .line 55
    iget-object v4, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 56
    .line 57
    if-nez v4, :cond_2

    .line 58
    .line 59
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    move-object v4, v2

    .line 63
    :cond_2
    iget-object v4, v4, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->O88O:Landroid/widget/TextView;

    .line 64
    .line 65
    new-instance v5, L〇00o80oo/OOO〇O0;

    .line 66
    .line 67
    invoke-direct {v5, v0, p0}, L〇00o80oo/OOO〇O0;-><init>(Lorg/json/JSONObject;Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 74
    .line 75
    if-nez v0, :cond_3

    .line 76
    .line 77
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    move-object v0, v2

    .line 81
    :cond_3
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->oOo0:Landroid/widget/RelativeLayout;

    .line 82
    .line 83
    new-instance v4, L〇00o80oo/oo〇;

    .line 84
    .line 85
    invoke-direct {v4, p0}, L〇00o80oo/oo〇;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 92
    .line 93
    if-nez v0, :cond_4

    .line 94
    .line 95
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    move-object v0, v2

    .line 99
    :cond_4
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 100
    .line 101
    const-string v4, "mBinding.ivMobilePwdLoginClose"

    .line 102
    .line 103
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    const/4 v4, 0x5

    .line 107
    invoke-static {v0, v4, v4, v4, v4}, Lcom/intsig/utils/ext/ViewExtKt;->〇o00〇〇Oo(Landroid/view/View;IIII)V

    .line 108
    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 111
    .line 112
    if-nez v0, :cond_5

    .line 113
    .line 114
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    move-object v0, v2

    .line 118
    :cond_5
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->〇〇08O:Landroid/widget/TextView;

    .line 119
    .line 120
    new-instance v4, L〇00o80oo/O8〇o;

    .line 121
    .line 122
    invoke-direct {v4, p0}, L〇00o80oo/O8〇o;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    .line 127
    .line 128
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 129
    .line 130
    if-nez v0, :cond_6

    .line 131
    .line 132
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    move-object v0, v2

    .line 136
    :cond_6
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 137
    .line 138
    new-instance v4, L〇00o80oo/〇00〇8;

    .line 139
    .line 140
    invoke-direct {v4, p0}, L〇00o80oo/〇00〇8;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {v0, v4}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 144
    .line 145
    .line 146
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 147
    .line 148
    if-nez v0, :cond_7

    .line 149
    .line 150
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    move-object v0, v2

    .line 154
    :cond_7
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->O0O:Landroid/widget/AutoCompleteTextView;

    .line 155
    .line 156
    const-string v4, "mBinding.tvMobilePwdLoginInputNumber"

    .line 157
    .line 158
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    new-instance v4, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$$inlined$doAfterTextChanged$1;

    .line 162
    .line 163
    invoke-direct {v4, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$$inlined$doAfterTextChanged$1;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 167
    .line 168
    .line 169
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 170
    .line 171
    if-nez v0, :cond_8

    .line 172
    .line 173
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 174
    .line 175
    .line 176
    move-object v0, v2

    .line 177
    :cond_8
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 178
    .line 179
    new-instance v4, L〇00o80oo/〇o;

    .line 180
    .line 181
    invoke-direct {v4, p0}, L〇00o80oo/〇o;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 182
    .line 183
    .line 184
    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    .line 186
    .line 187
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 188
    .line 189
    if-nez v0, :cond_9

    .line 190
    .line 191
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 192
    .line 193
    .line 194
    move-object v0, v2

    .line 195
    :cond_9
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->o8oOOo:Landroid/widget/AutoCompleteTextView;

    .line 196
    .line 197
    const-string v4, "mBinding.tvMobilePwdLoginInputPwd"

    .line 198
    .line 199
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    new-instance v4, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$$inlined$doAfterTextChanged$2;

    .line 203
    .line 204
    invoke-direct {v4, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$$inlined$doAfterTextChanged$2;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 208
    .line 209
    .line 210
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 211
    .line 212
    .line 213
    move-result-object v0

    .line 214
    instance-of v4, v0, Landroidx/lifecycle/LifecycleOwner;

    .line 215
    .line 216
    if-eqz v4, :cond_a

    .line 217
    .line 218
    check-cast v0, Landroidx/lifecycle/LifecycleOwner;

    .line 219
    .line 220
    goto :goto_0

    .line 221
    :cond_a
    move-object v0, v2

    .line 222
    :goto_0
    if-eqz v0, :cond_b

    .line 223
    .line 224
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->OOO〇O0()Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;

    .line 225
    .line 226
    .line 227
    move-result-object v4

    .line 228
    invoke-virtual {v4}, Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;->O8〇o()Lcom/intsig/utils/SingleLiveEvent;

    .line 229
    .line 230
    .line 231
    move-result-object v4

    .line 232
    new-instance v5, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$9$1;

    .line 233
    .line 234
    invoke-direct {v5, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$9$1;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 235
    .line 236
    .line 237
    new-instance v6, L〇00o80oo/o0ooO;

    .line 238
    .line 239
    invoke-direct {v6, v5}, L〇00o80oo/o0ooO;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 240
    .line 241
    .line 242
    invoke-virtual {v4, v0, v6}, Lcom/intsig/utils/SingleLiveEvent;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 243
    .line 244
    .line 245
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->OOO〇O0()Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;

    .line 246
    .line 247
    .line 248
    move-result-object v4

    .line 249
    invoke-virtual {v4}, Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;->O8ooOoo〇()Landroidx/lifecycle/MutableLiveData;

    .line 250
    .line 251
    .line 252
    move-result-object v4

    .line 253
    new-instance v5, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$9$2;

    .line 254
    .line 255
    invoke-direct {v5, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$9$2;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 256
    .line 257
    .line 258
    new-instance v6, L〇00o80oo/o〇8;

    .line 259
    .line 260
    invoke-direct {v6, v5}, L〇00o80oo/o〇8;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 261
    .line 262
    .line 263
    invoke-virtual {v4, v0, v6}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 264
    .line 265
    .line 266
    :cond_b
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 267
    .line 268
    if-nez v0, :cond_c

    .line 269
    .line 270
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    move-object v0, v2

    .line 274
    :cond_c
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->OO:Landroid/widget/CheckBox;

    .line 275
    .line 276
    const-string v4, "mBinding.cbMobilePwdLoginProtocol"

    .line 277
    .line 278
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    .line 280
    .line 281
    iget-object v4, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 282
    .line 283
    if-nez v4, :cond_d

    .line 284
    .line 285
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 286
    .line 287
    .line 288
    move-object v4, v2

    .line 289
    :cond_d
    iget-object v4, v4, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->〇O〇〇O8:Landroid/widget/TextView;

    .line 290
    .line 291
    const-string v5, "mBinding.tvMobilePwdLoginProtocol"

    .line 292
    .line 293
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    .line 295
    .line 296
    iget-object v5, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 297
    .line 298
    if-nez v5, :cond_e

    .line 299
    .line 300
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 301
    .line 302
    .line 303
    move-object v5, v2

    .line 304
    :cond_e
    iget-object v5, v5, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->oOo〇8o008:Lcom/intsig/camscanner/account/databinding/ProtocolKoreaBinding;

    .line 305
    .line 306
    iget-object v5, v5, Lcom/intsig/camscanner/account/databinding/ProtocolKoreaBinding;->〇8〇oO〇〇8o:Landroid/widget/LinearLayout;

    .line 307
    .line 308
    const-string v6, "mBinding.protocolKorea.llProtocolKorea"

    .line 309
    .line 310
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    .line 312
    .line 313
    invoke-static {v0, v4, v5}, Lcom/intsig/tsapp/account/helper/LoginHelper;->OO0o〇〇(Landroid/widget/CheckBox;Landroid/widget/TextView;Landroid/widget/LinearLayout;)V

    .line 314
    .line 315
    .line 316
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 317
    .line 318
    if-nez v0, :cond_f

    .line 319
    .line 320
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 321
    .line 322
    .line 323
    move-object v0, v2

    .line 324
    :cond_f
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 325
    .line 326
    new-instance v4, L〇00o80oo/Oo8Oo00oo;

    .line 327
    .line 328
    invoke-direct {v4, p0}, L〇00o80oo/Oo8Oo00oo;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 329
    .line 330
    .line 331
    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    .line 333
    .line 334
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 335
    .line 336
    if-eqz v0, :cond_10

    .line 337
    .line 338
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;->〇080()Ljava/lang/String;

    .line 339
    .line 340
    .line 341
    move-result-object v0

    .line 342
    if-nez v0, :cond_11

    .line 343
    .line 344
    :cond_10
    invoke-virtual {v3}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->O8()Ljava/lang/String;

    .line 345
    .line 346
    .line 347
    move-result-object v0

    .line 348
    :cond_11
    iget-object v4, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 349
    .line 350
    if-eqz v4, :cond_12

    .line 351
    .line 352
    invoke-virtual {v4}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 353
    .line 354
    .line 355
    move-result-object v4

    .line 356
    if-nez v4, :cond_13

    .line 357
    .line 358
    :cond_12
    invoke-virtual {v3}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->Oo08()Ljava/lang/String;

    .line 359
    .line 360
    .line 361
    move-result-object v4

    .line 362
    :cond_13
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 363
    .line 364
    .line 365
    move-result v3

    .line 366
    if-nez v3, :cond_1a

    .line 367
    .line 368
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 369
    .line 370
    .line 371
    move-result v3

    .line 372
    if-nez v3, :cond_1a

    .line 373
    .line 374
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 375
    .line 376
    iput-object v4, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇080OO8〇0:Ljava/lang/String;

    .line 377
    .line 378
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 379
    .line 380
    if-nez v3, :cond_14

    .line 381
    .line 382
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 383
    .line 384
    .line 385
    move-object v3, v2

    .line 386
    :cond_14
    iget-object v3, v3, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 387
    .line 388
    new-instance v5, Ljava/lang/StringBuilder;

    .line 389
    .line 390
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 391
    .line 392
    .line 393
    const-string v6, "+"

    .line 394
    .line 395
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    .line 397
    .line 398
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    .line 400
    .line 401
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 402
    .line 403
    .line 404
    move-result-object v5

    .line 405
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    .line 407
    .line 408
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 409
    .line 410
    if-nez v3, :cond_15

    .line 411
    .line 412
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 413
    .line 414
    .line 415
    move-object v3, v2

    .line 416
    :cond_15
    iget-object v3, v3, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 417
    .line 418
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 419
    .line 420
    .line 421
    move-result-object v5

    .line 422
    invoke-static {v5, v4}, Lcom/intsig/comm/AreaCodeCompat;->Oo08(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 423
    .line 424
    .line 425
    move-result-object v5

    .line 426
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    .line 428
    .line 429
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 430
    .line 431
    if-nez v3, :cond_16

    .line 432
    .line 433
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 434
    .line 435
    .line 436
    move-object v3, v2

    .line 437
    :cond_16
    iget-object v3, v3, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->O0O:Landroid/widget/AutoCompleteTextView;

    .line 438
    .line 439
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    .line 441
    .line 442
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->OOO〇O0()Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;

    .line 443
    .line 444
    .line 445
    move-result-object v3

    .line 446
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 447
    .line 448
    .line 449
    move-result-object v5

    .line 450
    invoke-static {v5, v4}, Lcom/intsig/comm/AreaCodeCompat;->Oo08(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 451
    .line 452
    .line 453
    move-result-object v5

    .line 454
    invoke-virtual {v3, v4, v5}, Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;->o〇8oOO88(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 458
    .line 459
    if-nez v3, :cond_17

    .line 460
    .line 461
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 462
    .line 463
    .line 464
    move-object v3, v2

    .line 465
    :cond_17
    iget-object v3, v3, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->o8oOOo:Landroid/widget/AutoCompleteTextView;

    .line 466
    .line 467
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    .line 468
    .line 469
    .line 470
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 471
    .line 472
    if-eqz v3, :cond_18

    .line 473
    .line 474
    invoke-virtual {v3}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;->O8()Ljava/lang/Boolean;

    .line 475
    .line 476
    .line 477
    move-result-object v3

    .line 478
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 479
    .line 480
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 481
    .line 482
    .line 483
    move-result v3

    .line 484
    goto :goto_1

    .line 485
    :cond_18
    const/4 v3, 0x0

    .line 486
    :goto_1
    if-eqz v3, :cond_1a

    .line 487
    .line 488
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 489
    .line 490
    .line 491
    move-result-object v3

    .line 492
    instance-of v3, v3, Landroid/app/Activity;

    .line 493
    .line 494
    if-eqz v3, :cond_1a

    .line 495
    .line 496
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 497
    .line 498
    invoke-virtual {v3}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;->〇o〇()Ljava/lang/String;

    .line 499
    .line 500
    .line 501
    move-result-object v3

    .line 502
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 503
    .line 504
    .line 505
    move-result v3

    .line 506
    if-nez v3, :cond_1a

    .line 507
    .line 508
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 509
    .line 510
    if-nez v3, :cond_19

    .line 511
    .line 512
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 513
    .line 514
    .line 515
    goto :goto_2

    .line 516
    :cond_19
    move-object v2, v3

    .line 517
    :goto_2
    iget-object v1, v2, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->o8oOOo:Landroid/widget/AutoCompleteTextView;

    .line 518
    .line 519
    iget-object v2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 520
    .line 521
    invoke-virtual {v2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;->〇o〇()Ljava/lang/String;

    .line 522
    .line 523
    .line 524
    move-result-object v2

    .line 525
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 526
    .line 527
    .line 528
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->OOO〇O0()Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;

    .line 529
    .line 530
    .line 531
    move-result-object v1

    .line 532
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 533
    .line 534
    .line 535
    move-result-object v2

    .line 536
    check-cast v2, Landroid/app/Activity;

    .line 537
    .line 538
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 539
    .line 540
    invoke-virtual {v3}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;->〇o〇()Ljava/lang/String;

    .line 541
    .line 542
    .line 543
    move-result-object v3

    .line 544
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 545
    .line 546
    .line 547
    invoke-virtual {v1, v2, v0, v3}, Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;->oo88o8O(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    .line 549
    .line 550
    :cond_1a
    return-void
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇〇0o(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final OOO〇O0()Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->O8o08O8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static final Oo8Oo00oo(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    new-instance v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$3$1;

    .line 11
    .line 12
    invoke-direct {v0, p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$3$1;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V

    .line 13
    .line 14
    .line 15
    invoke-interface {p1, v0}, Lcom/intsig/tsapp/account/iview/ILoginScene;->〇8〇oO〇〇8o(Lkotlin/jvm/functions/Function2;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OoO8(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇0OOo〇0(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic Oooo8o0〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o0ooO(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O〇8O8〇008(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O8o〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static final o0ooO(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final o8(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 5

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o8〇OO0〇0o:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "click login"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 14
    .line 15
    const-string v0, "mBinding"

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    move-object p1, v1

    .line 24
    :cond_0
    iget-object p1, p1, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 25
    .line 26
    const-string v2, ""

    .line 27
    .line 28
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 32
    .line 33
    if-nez p1, :cond_1

    .line 34
    .line 35
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    move-object p1, v1

    .line 39
    :cond_1
    iget-object p1, p1, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->o8oOOo:Landroid/widget/AutoCompleteTextView;

    .line 40
    .line 41
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-static {p1}, Lcom/intsig/comm/util/StringUtilDelegate;->Oo08(Ljava/lang/String;)Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-nez p1, :cond_2

    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 60
    .line 61
    .line 62
    move-result-object p0

    .line 63
    sget v0, Lcom/intsig/camscanner/account/R$string;->pwd_format_wrong:I

    .line 64
    .line 65
    const/4 v1, 0x1

    .line 66
    new-array v1, v1, [Ljava/lang/Object;

    .line 67
    .line 68
    const/4 v2, 0x6

    .line 69
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    const/4 v3, 0x0

    .line 74
    aput-object v2, v1, v3

    .line 75
    .line 76
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p0

    .line 80
    invoke-static {p1, p0}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return-void

    .line 84
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    instance-of v2, p1, Landroid/app/Activity;

    .line 89
    .line 90
    if-eqz v2, :cond_3

    .line 91
    .line 92
    check-cast p1, Landroid/app/Activity;

    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_3
    move-object p1, v1

    .line 96
    :goto_0
    if-eqz p1, :cond_6

    .line 97
    .line 98
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    iget-object v3, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 103
    .line 104
    if-nez v3, :cond_4

    .line 105
    .line 106
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    move-object v3, v1

    .line 110
    :cond_4
    iget-object v3, v3, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->OO:Landroid/widget/CheckBox;

    .line 111
    .line 112
    const-string v4, "mBinding.cbMobilePwdLoginProtocol"

    .line 113
    .line 114
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    iget-object v4, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 118
    .line 119
    if-nez v4, :cond_5

    .line 120
    .line 121
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    goto :goto_1

    .line 125
    :cond_5
    move-object v1, v4

    .line 126
    :goto_1
    iget-object v0, v1, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->oOo〇8o008:Lcom/intsig/camscanner/account/databinding/ProtocolKoreaBinding;

    .line 127
    .line 128
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/ProtocolKoreaBinding;->〇8〇oO〇〇8o:Landroid/widget/LinearLayout;

    .line 129
    .line 130
    const-string v1, "mBinding.protocolKorea.llProtocolKorea"

    .line 131
    .line 132
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    new-instance v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$10$1$1;

    .line 136
    .line 137
    invoke-direct {v1, p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$initViews$10$1$1;-><init>(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/app/Activity;)V

    .line 138
    .line 139
    .line 140
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/tsapp/account/helper/LoginHelper;->〇O8o08O(Landroid/content/Context;Landroid/widget/CheckBox;Landroid/widget/LinearLayout;Lkotlin/jvm/functions/Function0;)V

    .line 141
    .line 142
    .line 143
    :cond_6
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public static final synthetic o800o8O(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static final o〇0OOo〇0(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    const/16 p1, 0x91

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/16 p1, 0x81

    .line 12
    .line 13
    :goto_0
    iget-object p2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    const-string v1, "mBinding"

    .line 17
    .line 18
    if-nez p2, :cond_1

    .line 19
    .line 20
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    move-object p2, v0

    .line 24
    :cond_1
    iget-object p2, p2, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->o8oOOo:Landroid/widget/AutoCompleteTextView;

    .line 25
    .line 26
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setInputType(I)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 30
    .line 31
    if-nez p1, :cond_2

    .line 32
    .line 33
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    move-object p1, v0

    .line 37
    :cond_2
    iget-object p1, p1, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->o8oOOo:Landroid/widget/AutoCompleteTextView;

    .line 38
    .line 39
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 40
    .line 41
    if-nez p0, :cond_3

    .line 42
    .line 43
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_3
    move-object v0, p0

    .line 48
    :goto_1
    iget-object p0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->o8oOOo:Landroid/widget/AutoCompleteTextView;

    .line 49
    .line 50
    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 51
    .line 52
    .line 53
    move-result-object p0

    .line 54
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    .line 55
    .line 56
    .line 57
    move-result p0

    .line 58
    invoke-virtual {p1, p0}, Landroid/widget/EditText;->setSelection(I)V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final o〇8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic o〇O8〇〇o(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->OOO〇O0()Lcom/intsig/tsapp/account/viewmodel/PhonePwdLoginViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static final synthetic o〇〇0〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇00()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static final synthetic 〇0000OOO(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇080OO8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇00〇8(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->oOo0:Lorg/json/JSONObject;

    .line 7
    .line 8
    const-string v0, "CSMobileLoginRegister"

    .line 9
    .line 10
    const-string v1, "close"

    .line 11
    .line 12
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogAgentHelper;->OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    invoke-interface {p0}, Lcom/intsig/tsapp/account/iview/ILoginScene;->O8〇o〇88()V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇08O8o〇0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const-string v2, "mBinding"

    .line 9
    .line 10
    if-nez v0, :cond_2

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->oOo〇8o008:Ljava/lang/String;

    .line 13
    .line 14
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_2

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 21
    .line 22
    if-nez v0, :cond_0

    .line 23
    .line 24
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    move-object v0, v1

    .line 28
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 29
    .line 30
    const/high16 v3, 0x3f800000    # 1.0f

    .line 31
    .line 32
    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 36
    .line 37
    if-nez v0, :cond_1

    .line 38
    .line 39
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    move-object v1, v0

    .line 44
    :goto_0
    iget-object v0, v1, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 45
    .line 46
    const/4 v1, 0x1

    .line 47
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 48
    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 52
    .line 53
    if-nez v0, :cond_3

    .line 54
    .line 55
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    move-object v0, v1

    .line 59
    :cond_3
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 60
    .line 61
    const/high16 v3, 0x3f000000    # 0.5f

    .line 62
    .line 63
    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 67
    .line 68
    if-nez v0, :cond_4

    .line 69
    .line 70
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_4
    move-object v1, v0

    .line 75
    :goto_1
    iget-object v0, v1, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 76
    .line 77
    const/4 v1, 0x0

    .line 78
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 79
    .line 80
    .line 81
    :goto_2
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static synthetic 〇0〇O0088o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇O00(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇〇〇0〇〇0(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇O888o0o(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇080OO8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->Oo8Oo00oo(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇O〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o8(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇o(Lorg/json/JSONObject;Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p2, "$logJson"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sget-object p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o8〇OO0〇0o:Ljava/lang/String;

    .line 12
    .line 13
    const-string v0, "click verifyCode login"

    .line 14
    .line 15
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const-string p2, "CSMobileLoginRegister"

    .line 19
    .line 20
    const-string v0, "to_verification_login"

    .line 21
    .line 22
    invoke-static {p2, v0, p0}, Lcom/intsig/log/LogAgentHelper;->OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    new-instance p2, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    const/4 v1, 0x0

    .line 40
    invoke-direct {p2, v0, p1, v1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene$Companion$MobileNumberInputParams;)V

    .line 41
    .line 42
    .line 43
    const/4 p1, 0x0

    .line 44
    invoke-interface {p0, p2, p1}, Lcom/intsig/tsapp/account/iview/ILoginScene;->〇OO8Oo0〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;Z)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final synthetic 〇oo〇(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static final 〇〇0o(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 7
    .line 8
    if-nez p0, :cond_0

    .line 9
    .line 10
    const-string p0, "mBinding"

    .line 11
    .line 12
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/4 p0, 0x0

    .line 16
    :cond_0
    iget-object p0, p0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->O0O:Landroid/widget/AutoCompleteTextView;

    .line 17
    .line 18
    const-string p1, ""

    .line 19
    .line 20
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇808〇(Lorg/json/JSONObject;Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇o(Lorg/json/JSONObject;Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇〇8O0〇8(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇00〇8(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇〇0〇〇0(Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;Landroid/view/View;)V
    .locals 7

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o8〇OO0〇0o:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "click find pwd"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-nez p1, :cond_1

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇080OO8〇0:Ljava/lang/String;

    .line 22
    .line 23
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    if-eqz p1, :cond_0

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    new-instance v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->o〇0()Lcom/intsig/tsapp/account/iview/ILoginScene;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    new-instance v3, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;

    .line 45
    .line 46
    sget-object v4, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 47
    .line 48
    iget-object v5, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇080OO8〇0:Ljava/lang/String;

    .line 49
    .line 50
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 51
    .line 52
    const-string v6, "mobile"

    .line 53
    .line 54
    invoke-direct {v3, v4, v5, p0, v6}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;-><init>(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene;-><init>(Landroid/content/Context;Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/ForgetPwdScene$Companion$ForgetPwdParams;)V

    .line 58
    .line 59
    .line 60
    const/4 p0, 0x2

    .line 61
    const/4 v1, 0x0

    .line 62
    const/4 v2, 0x0

    .line 63
    invoke-static {p1, v0, v2, p0, v1}, Lcom/intsig/tsapp/account/iview/ILoginScene$DefaultImpls;->〇080(Lcom/intsig/tsapp/account/iview/ILoginScene;Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;ZILjava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    return-void

    .line 67
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 68
    .line 69
    .line 70
    move-result-object p0

    .line 71
    sget p1, Lcom/intsig/camscanner/account/R$string;->cs_519b_find_account:I

    .line 72
    .line 73
    invoke-static {p1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    invoke-static {p0, p1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public O8()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mBinding"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public Oo08()Lkotlin/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇080OO8〇0:Ljava/lang/String;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 6
    .line 7
    new-instance v3, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v4, "getAccountInfo areaCode is "

    .line 13
    .line 14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v1, ", account is "

    .line 21
    .line 22
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    new-instance v0, Lkotlin/Pair;

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇080OO8〇0:Ljava/lang/String;

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇0O:Ljava/lang/String;

    .line 40
    .line 41
    invoke-direct {v0, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 42
    .line 43
    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public final oo〇()Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇80〇808〇O()Landroid/view/View;
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->〇08O〇00〇o:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene$Companion$MobilePwdLoginParams;

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v3, "initScene params is "

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lorg/json/JSONObject;

    .line 26
    .line 27
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->oOo0:Lorg/json/JSONObject;

    .line 31
    .line 32
    sget-object v1, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const-string v2, "from_part"

    .line 39
    .line 40
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->oOo0:Lorg/json/JSONObject;

    .line 44
    .line 45
    if-eqz v0, :cond_0

    .line 46
    .line 47
    const-string v1, "type"

    .line 48
    .line 49
    const-string v2, "password"

    .line 50
    .line 51
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 52
    .line 53
    .line 54
    :cond_0
    const-string v0, "CSMobileLoginRegister"

    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->oOo0:Lorg/json/JSONObject;

    .line 57
    .line 58
    invoke-static {v0, v1}, Lcom/intsig/log/LogAgentHelper;->O8〇o(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/AbstractLoginScene;->getContext()Landroid/content/Context;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    sget v1, Lcom/intsig/camscanner/account/R$layout;->scene_mobile_pwd_login:I

    .line 66
    .line 67
    const/4 v2, 0x0

    .line 68
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-static {v0}, Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    const-string v2, "bind(sceneView)"

    .line 77
    .line 78
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    iput-object v1, p0, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->o〇00O:Lcom/intsig/camscanner/account/databinding/SceneMobilePwdLoginBinding;

    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobilePwdLoginScene;->O8〇o()V

    .line 84
    .line 85
    .line 86
    return-object v0
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/16 v0, 0x2713

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

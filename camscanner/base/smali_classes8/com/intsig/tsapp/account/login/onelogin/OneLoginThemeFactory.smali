.class public final Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;
.super Ljava/lang/Object;
.source "OneLoginThemeFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;,
        Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;,
        Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$WhenMappings;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static OO0o〇〇:Z

.field public static final OO0o〇〇〇〇0:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static 〇8o8o〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O8o08O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:Landroid/content/Context;

.field private Oo08:Lcom/intsig/app/BaseProgressDialog;

.field private final oO80:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;

.field private final 〇80〇808〇O:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/utils/ClickLimit;

.field private final 〇〇888:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$mLoginClickListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->OO0o〇〇〇〇0:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/utils/WebUrlUtils;->〇o()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sput-object v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇8o8o〇:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/utils/WebUrlUtils;->o〇〇0〇()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sput-object v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇O8o08O:Ljava/lang/String;

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;)V
    .locals 1
    .param p2    # Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "params"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇080:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;

    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 18
    .line 19
    sget-object p1, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;->WapSrc:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o〇0:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 22
    .line 23
    new-instance p1, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$mLoginClickListener$1;

    .line 24
    .line 25
    invoke-direct {p1, p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$mLoginClickListener$1;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V

    .line 26
    .line 27
    .line 28
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇〇888:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$mLoginClickListener$1;

    .line 29
    .line 30
    new-instance p1, LO〇0888o/〇〇808〇;

    .line 31
    .line 32
    invoke-direct {p1}, LO〇0888o/〇〇808〇;-><init>()V

    .line 33
    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->oO80:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

    .line 36
    .line 37
    new-instance p1, LO〇0888o/〇O00;

    .line 38
    .line 39
    invoke-direct {p1, p0}, LO〇0888o/〇O00;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V

    .line 40
    .line 41
    .line 42
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇80〇808〇O:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇〇〇0〇〇0(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static final O8ooOoo〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->O8()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final O8〇o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 3

    .line 1
    const/4 p1, 0x2

    .line 2
    new-array p1, p1, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v0, Landroid/util/Pair;

    .line 5
    .line 6
    sget-object v1, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const-string v2, "from_part"

    .line 13
    .line 14
    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    aput-object v0, p1, v1

    .line 19
    .line 20
    new-instance v0, Landroid/util/Pair;

    .line 21
    .line 22
    const-string v1, "type"

    .line 23
    .line 24
    const-string v2, "email"

    .line 25
    .line 26
    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    aput-object v0, p1, v1

    .line 31
    .line 32
    const-string v0, "CSOneClickLogin"

    .line 33
    .line 34
    const-string v1, "more_login_method"

    .line 35
    .line 36
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 37
    .line 38
    .line 39
    if-eqz p0, :cond_0

    .line 40
    .line 41
    invoke-interface {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->〇o00〇〇Oo()V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Landroid/content/Context;Lcom/cmic/gen/sdk/view/AuthLoginCallBack;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Landroid/content/Context;Lcom/cmic/gen/sdk/view/AuthLoginCallBack;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->OOO〇O0(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final OOO〇O0(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    sget-object p0, Lcom/intsig/camscanner/dialog/loginforcompliance/LoginForComplianceDialog;->o8〇OO0〇0o:Lcom/intsig/camscanner/dialog/loginforcompliance/LoginForComplianceDialog$Companion;

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/dialog/loginforcompliance/LoginForComplianceDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    const-string p2, "CSOneClickLogin"

    .line 21
    .line 22
    const-string v0, "other_phone_login"

    .line 23
    .line 24
    const-string v1, "from"

    .line 25
    .line 26
    invoke-static {p2, v0, v1, p0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->〇o〇()V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic Oo08(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o〇〇0〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final synthetic OoO8(Z)V
    .locals 0

    .line 1
    sput-boolean p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->OO0o〇〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->O8〇o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final O〇8O8〇008(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->〇o00〇〇Oo()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final o0ooO(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    sget-object p0, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    const-string p2, "CSOneClickLogin"

    .line 21
    .line 22
    const-string v0, "close"

    .line 23
    .line 24
    const-string v1, "from_part"

    .line 25
    .line 26
    invoke-static {p2, v0, v1, p0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->Oo08()V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final synthetic o800o8O(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/app/BaseProgressDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->Oo08:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final oO(Landroid/content/Context;Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    sget p1, Lcom/intsig/camscanner/account/R$string;->one_login_agree_service:I

    .line 2
    .line 3
    invoke-static {p0, p1}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic oO80(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇〇0o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private final oo88o8O(Landroid/content/Context;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)Landroid/view/View;
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    new-instance v2, Landroid/widget/RelativeLayout;

    .line 6
    .line 7
    move-object/from16 v3, p1

    .line 8
    .line 9
    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 13
    .line 14
    const/4 v5, -0x1

    .line 15
    invoke-direct {v4, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 19
    .line 20
    .line 21
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    sget v4, Lcom/intsig/camscanner/account/R$layout;->layout_one_login_auth_custom:I

    .line 26
    .line 27
    const/4 v5, 0x0

    .line 28
    invoke-virtual {v3, v4, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    sget v3, Lcom/intsig/camscanner/account/R$id;->ll_one_login_auth_more_login_way:I

    .line 33
    .line 34
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    check-cast v3, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 39
    .line 40
    sget v4, Lcom/intsig/camscanner/account/R$id;->iv_one_login_auth_back:I

    .line 41
    .line 42
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 43
    .line 44
    .line 45
    move-result-object v4

    .line 46
    check-cast v4, Landroidx/appcompat/widget/AppCompatImageView;

    .line 47
    .line 48
    sget v6, Lcom/intsig/camscanner/account/R$id;->tv_one_login_title:I

    .line 49
    .line 50
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 51
    .line 52
    .line 53
    move-result-object v6

    .line 54
    check-cast v6, Landroid/widget/TextView;

    .line 55
    .line 56
    sget v7, Lcom/intsig/camscanner/account/R$id;->tv_one_login_other_phone_login:I

    .line 57
    .line 58
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v7

    .line 62
    check-cast v7, Landroid/widget/TextView;

    .line 63
    .line 64
    sget v8, Lcom/intsig/camscanner/account/R$id;->tv_one_login_auth_wechat:I

    .line 65
    .line 66
    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 67
    .line 68
    .line 69
    move-result-object v8

    .line 70
    check-cast v8, Landroidx/appcompat/widget/AppCompatTextView;

    .line 71
    .line 72
    sget v9, Lcom/intsig/camscanner/account/R$id;->tv_one_login_auth_mail:I

    .line 73
    .line 74
    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 75
    .line 76
    .line 77
    move-result-object v9

    .line 78
    check-cast v9, Landroidx/appcompat/widget/AppCompatTextView;

    .line 79
    .line 80
    sget v10, Lcom/intsig/camscanner/account/R$id;->tv_login_main_last_login_tips:I

    .line 81
    .line 82
    invoke-virtual {v2, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 83
    .line 84
    .line 85
    move-result-object v10

    .line 86
    check-cast v10, Lcom/intsig/comm/widget/CustomTextView;

    .line 87
    .line 88
    sget v11, Lcom/intsig/camscanner/account/R$id;->tv_one_binding_other_phone:I

    .line 89
    .line 90
    invoke-virtual {v2, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 91
    .line 92
    .line 93
    move-result-object v11

    .line 94
    check-cast v11, Landroid/widget/TextView;

    .line 95
    .line 96
    sget v12, Lcom/intsig/camscanner/account/R$id;->tv_bind_intro:I

    .line 97
    .line 98
    invoke-virtual {v2, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 99
    .line 100
    .line 101
    move-result-object v12

    .line 102
    check-cast v12, Landroid/widget/TextView;

    .line 103
    .line 104
    sget v13, Lcom/intsig/camscanner/account/R$id;->tv_one_login_skip:I

    .line 105
    .line 106
    invoke-virtual {v2, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object v13

    .line 110
    check-cast v13, Landroid/widget/TextView;

    .line 111
    .line 112
    sget v14, Lcom/intsig/camscanner/account/R$id;->tv_operator_type:I

    .line 113
    .line 114
    invoke-virtual {v2, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 115
    .line 116
    .line 117
    move-result-object v14

    .line 118
    check-cast v14, Landroidx/appcompat/widget/AppCompatTextView;

    .line 119
    .line 120
    invoke-direct/range {p0 .. p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇08O8o〇0()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v15

    .line 124
    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    .line 126
    .line 127
    iget-object v15, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 128
    .line 129
    invoke-virtual {v15}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->〇080()Z

    .line 130
    .line 131
    .line 132
    move-result v15

    .line 133
    move-object/from16 p1, v10

    .line 134
    .line 135
    const/16 v10, 0x8

    .line 136
    .line 137
    if-eqz v15, :cond_1

    .line 138
    .line 139
    invoke-virtual {v11, v5}, Landroid/view/View;->setVisibility(I)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v12, v5}, Landroid/view/View;->setVisibility(I)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 152
    .line 153
    .line 154
    invoke-static {}, Lcom/intsig/tsapp/account/exp/UserBindOptExp;->〇o00〇〇Oo()Z

    .line 155
    .line 156
    .line 157
    move-result v3

    .line 158
    if-eqz v3, :cond_0

    .line 159
    .line 160
    iget-object v3, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 161
    .line 162
    invoke-virtual {v3}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->〇o00〇〇Oo()Z

    .line 163
    .line 164
    .line 165
    move-result v3

    .line 166
    if-nez v3, :cond_0

    .line 167
    .line 168
    invoke-virtual {v13, v5}, Landroid/view/View;->setVisibility(I)V

    .line 169
    .line 170
    .line 171
    :cond_0
    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 172
    .line 173
    .line 174
    move-result-object v3

    .line 175
    const-string v6, "null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams"

    .line 176
    .line 177
    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 181
    .line 182
    sget-object v6, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 183
    .line 184
    invoke-virtual {v6}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 185
    .line 186
    .line 187
    move-result-object v6

    .line 188
    const/16 v10, 0xf0

    .line 189
    .line 190
    invoke-static {v6, v10}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 191
    .line 192
    .line 193
    move-result v6

    .line 194
    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 195
    .line 196
    goto :goto_0

    .line 197
    :cond_1
    invoke-virtual {v11, v10}, Landroid/view/View;->setVisibility(I)V

    .line 198
    .line 199
    .line 200
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 201
    .line 202
    .line 203
    :goto_0
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->o〇0OOo〇0()Z

    .line 204
    .line 205
    .line 206
    move-result v3

    .line 207
    if-eqz v3, :cond_2

    .line 208
    .line 209
    move-object/from16 v10, p1

    .line 210
    .line 211
    goto :goto_1

    .line 212
    :cond_2
    const/4 v10, 0x0

    .line 213
    :goto_1
    if-nez v10, :cond_3

    .line 214
    .line 215
    goto :goto_2

    .line 216
    :cond_3
    invoke-virtual {v10, v5}, Landroid/view/View;->setVisibility(I)V

    .line 217
    .line 218
    .line 219
    :goto_2
    new-instance v3, LO〇0888o/〇〇8O0〇8;

    .line 220
    .line 221
    invoke-direct {v3, v0, v1}, LO〇0888o/〇〇8O0〇8;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v4, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    .line 226
    .line 227
    new-instance v3, LO〇0888o/〇0〇O0088o;

    .line 228
    .line 229
    invoke-direct {v3, v0, v1}, LO〇0888o/〇0〇O0088o;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 230
    .line 231
    .line 232
    invoke-virtual {v8, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    .line 234
    .line 235
    new-instance v3, LO〇0888o/OoO8;

    .line 236
    .line 237
    invoke-direct {v3, v0, v1}, LO〇0888o/OoO8;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 238
    .line 239
    .line 240
    invoke-virtual {v7, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    .line 242
    .line 243
    new-instance v3, LO〇0888o/o800o8O;

    .line 244
    .line 245
    invoke-direct {v3, v0, v1}, LO〇0888o/o800o8O;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 246
    .line 247
    .line 248
    invoke-virtual {v9, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    .line 250
    .line 251
    new-instance v3, LO〇0888o/〇O888o0o;

    .line 252
    .line 253
    invoke-direct {v3, v0, v1}, LO〇0888o/〇O888o0o;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v11, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    .line 258
    .line 259
    new-instance v3, LO〇0888o/oo88o8O;

    .line 260
    .line 261
    invoke-direct {v3, v0, v1}, LO〇0888o/oo88o8O;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 262
    .line 263
    .line 264
    invoke-virtual {v13, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    .line 266
    .line 267
    const-string v1, "rootView"

    .line 268
    .line 269
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    .line 271
    .line 272
    return-object v2
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private final oo〇(Landroid/content/Context;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)Landroid/view/View;
    .locals 10

    .line 1
    new-instance v0, Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 7
    .line 8
    const/4 v2, -0x1

    .line 9
    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 13
    .line 14
    .line 15
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    sget v1, Lcom/intsig/camscanner/account/R$layout;->layout_one_login_auth_half_screen_for_login:I

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    sget v0, Lcom/intsig/camscanner/account/R$id;->tv_one_login_half_intro_for_login:I

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    check-cast v0, Landroid/widget/TextView;

    .line 33
    .line 34
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_one_login_half_bind_other_phone:I

    .line 35
    .line 36
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    check-cast v1, Landroid/widget/TextView;

    .line 41
    .line 42
    sget v3, Lcom/intsig/camscanner/account/R$id;->iv_one_login_half_close_for_login:I

    .line 43
    .line 44
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    check-cast v3, Landroid/widget/ImageView;

    .line 49
    .line 50
    sget v4, Lcom/intsig/camscanner/account/R$id;->iv_one_login_auth_wechat:I

    .line 51
    .line 52
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    check-cast v4, Landroid/widget/ImageView;

    .line 57
    .line 58
    sget v5, Lcom/intsig/camscanner/account/R$id;->iv_one_login_auth_mail:I

    .line 59
    .line 60
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 61
    .line 62
    .line 63
    move-result-object v5

    .line 64
    check-cast v5, Landroid/widget/ImageView;

    .line 65
    .line 66
    sget v6, Lcom/intsig/camscanner/account/R$id;->iv_one_login_auth_mobile:I

    .line 67
    .line 68
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 69
    .line 70
    .line 71
    move-result-object v6

    .line 72
    check-cast v6, Landroid/widget/ImageView;

    .line 73
    .line 74
    sget v7, Lcom/intsig/camscanner/account/R$id;->ll_one_login_auth_more_login_ways:I

    .line 75
    .line 76
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 77
    .line 78
    .line 79
    move-result-object v7

    .line 80
    check-cast v7, Landroid/widget/LinearLayout;

    .line 81
    .line 82
    sget v8, Lcom/intsig/camscanner/account/R$id;->tv_operator_type:I

    .line 83
    .line 84
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 85
    .line 86
    .line 87
    move-result-object v8

    .line 88
    check-cast v8, Landroidx/appcompat/widget/AppCompatTextView;

    .line 89
    .line 90
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇08O8o〇0()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v9

    .line 94
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    .line 96
    .line 97
    iget-object v8, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 98
    .line 99
    invoke-virtual {v8}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->〇080()Z

    .line 100
    .line 101
    .line 102
    move-result v8

    .line 103
    if-eqz v8, :cond_0

    .line 104
    .line 105
    sget v8, Lcom/intsig/camscanner/account/R$string;->cs_514_bind_phone_number:I

    .line 106
    .line 107
    invoke-static {v8}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v8

    .line 111
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 115
    .line 116
    .line 117
    const/16 v2, 0x8

    .line 118
    .line 119
    invoke-virtual {v7, v2}, Landroid/view/View;->setVisibility(I)V

    .line 120
    .line 121
    .line 122
    :cond_0
    sget-boolean v2, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->OO0o〇〇:Z

    .line 123
    .line 124
    if-eqz v2, :cond_1

    .line 125
    .line 126
    sget v2, Lcom/intsig/camscanner/account/R$string;->cs_641_log_2:I

    .line 127
    .line 128
    invoke-static {v2}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    .line 134
    .line 135
    :cond_1
    const/4 v2, 0x1

    .line 136
    const/4 v7, 0x2

    .line 137
    const/16 v8, 0xc

    .line 138
    .line 139
    const/16 v9, 0x14

    .line 140
    .line 141
    invoke-static {v0, v8, v9, v2, v7}, Landroidx/core/widget/TextViewCompat;->setAutoSizeTextTypeUniformWithConfiguration(Landroid/widget/TextView;IIII)V

    .line 142
    .line 143
    .line 144
    new-instance v0, LO〇0888o/〇〇888;

    .line 145
    .line 146
    invoke-direct {v0, p0, p2}, LO〇0888o/〇〇888;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    .line 151
    .line 152
    new-instance v0, LO〇0888o/oO80;

    .line 153
    .line 154
    invoke-direct {v0, p2}, LO〇0888o/oO80;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    .line 159
    .line 160
    new-instance v0, LO〇0888o/〇80〇808〇O;

    .line 161
    .line 162
    invoke-direct {v0, p2}, LO〇0888o/〇80〇808〇O;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    .line 167
    .line 168
    new-instance v0, LO〇0888o/OO0o〇〇〇〇0;

    .line 169
    .line 170
    invoke-direct {v0, p2}, LO〇0888o/OO0o〇〇〇〇0;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 171
    .line 172
    .line 173
    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    .line 175
    .line 176
    new-instance v0, LO〇0888o/〇8o8o〇;

    .line 177
    .line 178
    invoke-direct {v0, p2}, LO〇0888o/〇8o8o〇;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 179
    .line 180
    .line 181
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    .line 183
    .line 184
    const-string p2, "rootView"

    .line 185
    .line 186
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    .line 188
    .line 189
    return-object p1
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public static synthetic o〇0(Landroid/content/Context;Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->oO(Landroid/content/Context;Lorg/json/JSONObject;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final o〇8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 3

    .line 1
    const/4 p1, 0x2

    .line 2
    new-array p1, p1, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v0, Landroid/util/Pair;

    .line 5
    .line 6
    sget-object v1, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const-string v2, "from_part"

    .line 13
    .line 14
    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    aput-object v0, p1, v1

    .line 19
    .line 20
    new-instance v0, Landroid/util/Pair;

    .line 21
    .line 22
    const-string v1, "type"

    .line 23
    .line 24
    const-string v2, "wechat"

    .line 25
    .line 26
    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    aput-object v0, p1, v1

    .line 31
    .line 32
    const-string v0, "CSOneClickLogin"

    .line 33
    .line 34
    const-string v1, "more_login_method"

    .line 35
    .line 36
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 37
    .line 38
    .line 39
    if-eqz p0, :cond_0

    .line 40
    .line 41
    invoke-interface {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->o〇0()V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final o〇O8〇〇o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->o〇0()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final o〇〇0〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->Oo08()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final 〇00(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->〇080()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private final 〇0000OOO(Landroid/content/Context;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)Landroid/view/View;
    .locals 4

    .line 1
    new-instance v0, Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 7
    .line 8
    const/4 v2, -0x1

    .line 9
    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 13
    .line 14
    .line 15
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    sget v1, Lcom/intsig/camscanner/account/R$layout;->layout_one_login_auth_half_screen_for_compliance:I

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    sget v0, Lcom/intsig/camscanner/account/R$id;->iv_one_login_half_close:I

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    check-cast v0, Landroid/widget/ImageView;

    .line 33
    .line 34
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_one_login_half_bind_other_phone:I

    .line 35
    .line 36
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    check-cast v1, Landroid/widget/TextView;

    .line 41
    .line 42
    sget v2, Lcom/intsig/camscanner/account/R$id;->tv_operator_type:I

    .line 43
    .line 44
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    check-cast v2, Landroidx/appcompat/widget/AppCompatTextView;

    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇08O8o〇0()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    .line 56
    .line 57
    sget v2, Lcom/intsig/camscanner/account/R$id;->tv_one_login_half_intro:I

    .line 58
    .line 59
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    check-cast v2, Landroid/widget/TextView;

    .line 64
    .line 65
    sget-object v3, Lcom/intsig/camscanner/dialog/loginforcompliance/LoginForComplianceDialog;->o8〇OO0〇0o:Lcom/intsig/camscanner/dialog/loginforcompliance/LoginForComplianceDialog$Companion;

    .line 66
    .line 67
    invoke-virtual {v3}, Lcom/intsig/camscanner/dialog/loginforcompliance/LoginForComplianceDialog$Companion;->〇080()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    .line 73
    .line 74
    new-instance v2, LO〇0888o/〇O8o08O;

    .line 75
    .line 76
    invoke-direct {v2, p0, p2}, LO〇0888o/〇O8o08O;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    .line 81
    .line 82
    new-instance v0, LO〇0888o/OO0o〇〇;

    .line 83
    .line 84
    invoke-direct {v0, p0, p2}, LO〇0888o/OO0o〇〇;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    .line 89
    .line 90
    const-string p2, "rootView"

    .line 91
    .line 92
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    return-object p1
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private static final 〇00〇8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 3

    .line 1
    const/4 p1, 0x2

    .line 2
    new-array p1, p1, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v0, Landroid/util/Pair;

    .line 5
    .line 6
    sget-object v1, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const-string v2, "from_part"

    .line 13
    .line 14
    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    aput-object v0, p1, v1

    .line 19
    .line 20
    new-instance v0, Landroid/util/Pair;

    .line 21
    .line 22
    const-string v1, "type"

    .line 23
    .line 24
    const-string v2, "mobile"

    .line 25
    .line 26
    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    aput-object v0, p1, v1

    .line 31
    .line 32
    const-string v0, "CSOneClickLogin"

    .line 33
    .line 34
    const-string v1, "more_login_method"

    .line 35
    .line 36
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 37
    .line 38
    .line 39
    if-eqz p0, :cond_0

    .line 40
    .line 41
    invoke-interface {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->〇080()V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇080(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇00〇8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇08O8o〇0()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o〇0:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$WhenMappings;->〇080:[I

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    aget v0, v1, v0

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    if-eq v0, v1, :cond_2

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    if-eq v0, v1, :cond_1

    .line 16
    .line 17
    const/4 v1, 0x3

    .line 18
    if-ne v0, v1, :cond_0

    .line 19
    .line 20
    const-string v0, "\u4e2d\u56fd\u7535\u4fe1\u63d0\u4f9b\u8ba4\u8bc1\u670d\u52a1"

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 24
    .line 25
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 26
    .line 27
    .line 28
    throw v0

    .line 29
    :cond_1
    const-string v0, "\u4e2d\u56fd\u8054\u901a\u63d0\u4f9b\u8ba4\u8bc1\u670d\u52a1"

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_2
    const-string v0, "\u4e2d\u56fd\u79fb\u52a8\u63d0\u4f9b\u8ba4\u8bc1\u670d\u52a1"

    .line 33
    .line 34
    :goto_0
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static final synthetic 〇0〇O0088o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->Oo08:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static final 〇8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Landroid/content/Context;Lcom/cmic/gen/sdk/view/AuthLoginCallBack;)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->〇o〇()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->O8()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    new-instance v2, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v3, "mGenAuthLoginListener isHalfScreenForCompliance is "

    .line 24
    .line 25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v0, ", isHalfScreenForLogin is "

    .line 32
    .line 33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const-string v1, "OneLoginThemeFactory"

    .line 44
    .line 45
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->〇o〇()Z

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-eqz v0, :cond_0

    .line 55
    .line 56
    sget p0, Lcom/intsig/camscanner/account/R$string;->cs_638_login_tost:I

    .line 57
    .line 58
    invoke-static {p1, p0}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 63
    .line 64
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->O8()Z

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    const-string v1, "callback"

    .line 69
    .line 70
    const-string v2, "context"

    .line 71
    .line 72
    if-eqz v0, :cond_1

    .line 73
    .line 74
    sget-object v0, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog;->o〇00O:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$Companion;

    .line 75
    .line 76
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    new-instance v2, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$LoginProtocolParams;

    .line 80
    .line 81
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o〇0:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 82
    .line 83
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-direct {v2, p0, p2}, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$LoginProtocolParams;-><init>(Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;Lcom/cmic/gen/sdk/view/AuthLoginCallBack;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0, p1, v2}, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$Companion;->O8(Landroid/content/Context;Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$LoginProtocolParams;)V

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_1
    sget-object v0, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog;->o〇00O:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$Companion;

    .line 94
    .line 95
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    new-instance v2, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$LoginProtocolParams;

    .line 99
    .line 100
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o〇0:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 101
    .line 102
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    invoke-direct {v2, p0, p2}, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$LoginProtocolParams;-><init>(Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;Lcom/cmic/gen/sdk/view/AuthLoginCallBack;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v0, p1, v2}, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$Companion;->O8(Landroid/content/Context;Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$LoginProtocolParams;)V

    .line 109
    .line 110
    .line 111
    :goto_0
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o〇8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇O00(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇oOO8O8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->O8ooOoo〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇O〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇oo〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final 〇o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    invoke-interface {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->〇080()V

    .line 4
    .line 5
    .line 6
    :cond_0
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o0ooO(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final 〇oOO8O8(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->Oo08()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final 〇oo〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o〇:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->Oo08()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇o〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->O〇8O8〇008(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static final 〇〇0o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇080:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-interface {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->〇〇888()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic 〇〇808〇(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇00(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static synthetic 〇〇888(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o〇O8〇〇o(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static final synthetic 〇〇8O0〇8()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->OO0o〇〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static final 〇〇〇0〇〇0(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇080:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-interface {p0}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;->〇〇888()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public final O08000(Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->O8:Landroid/content/Context;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final Oo8Oo00oo(Landroid/content/Context;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;
    .locals 14
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;-><init>()V

    .line 9
    .line 10
    .line 11
    const/4 v11, 0x1

    .line 12
    invoke-virtual {v0, v11}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setFitsSystemWindows(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 13
    .line 14
    .line 15
    sget v1, Lcom/intsig/camscanner/account/R$color;->cs_color_bg_0:I

    .line 16
    .line 17
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-virtual {v0, v2, v11}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setStatusBar(IZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 22
    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇080:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;

    .line 25
    .line 26
    invoke-direct {p0, p1, v2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->oo88o8O(Landroid/content/Context;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-virtual {v0, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setAuthContentView(Landroid/view/View;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 31
    .line 32
    .line 33
    new-instance v2, LO〇0888o/o〇0;

    .line 34
    .line 35
    invoke-direct {v2, p0}, LO〇0888o/o〇0;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setGenBackPressedListener(Lcom/cmic/gen/sdk/view/GenBackPressedListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 39
    .line 40
    .line 41
    const/16 v2, 0x12

    .line 42
    .line 43
    invoke-virtual {v0, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNavTextSize(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 44
    .line 45
    .line 46
    sget v2, Lcom/intsig/camscanner/account/R$color;->cs_color_text_4:I

    .line 47
    .line 48
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    invoke-virtual {v0, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNavTextColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 53
    .line 54
    .line 55
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNavColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 60
    .line 61
    .line 62
    sget v1, Lcom/intsig/camscanner/account/R$layout;->layout_one_login_privacy_title:I

    .line 63
    .line 64
    const-string v3, "iv_one_login_return_id"

    .line 65
    .line 66
    invoke-virtual {v0, v1, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setClauseLayoutResID(ILjava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 67
    .line 68
    .line 69
    const/16 v1, 0x14

    .line 70
    .line 71
    invoke-virtual {v0, v1, v11}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNumberSize(IZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 72
    .line 73
    .line 74
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNumberColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 79
    .line 80
    .line 81
    sget v1, Lcom/intsig/camscanner/account/R$string;->one_login_one_key_login:I

    .line 82
    .line 83
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    const-string v2, "context.getString(R.stri\u2026.one_login_one_key_login)"

    .line 88
    .line 89
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    iget-object v2, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 93
    .line 94
    invoke-virtual {v2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->〇080()Z

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    if-eqz v2, :cond_0

    .line 99
    .line 100
    const/16 v1, 0xd2

    .line 101
    .line 102
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNumFieldOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 103
    .line 104
    .line 105
    const/16 v1, 0x19a

    .line 106
    .line 107
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 108
    .line 109
    .line 110
    const-string v1, "\u672c\u673a\u53f7\u7801\u4e00\u952e\u7ed1\u5b9a"

    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_0
    const/16 v2, 0xc8

    .line 114
    .line 115
    invoke-virtual {v0, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNumFieldOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 116
    .line 117
    .line 118
    const/16 v2, 0x17b

    .line 119
    .line 120
    invoke-virtual {v0, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 121
    .line 122
    .line 123
    :goto_0
    sget v2, Lcom/intsig/camscanner/account/R$color;->cs_white_FFFFFF:I

    .line 124
    .line 125
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    const/16 v3, 0x11

    .line 130
    .line 131
    invoke-virtual {v0, v1, v2, v3, v11}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnText(Ljava/lang/String;IIZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 132
    .line 133
    .line 134
    const/16 v1, 0x137

    .line 135
    .line 136
    const/16 v2, 0x30

    .line 137
    .line 138
    invoke-virtual {v0, v1, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtn(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 139
    .line 140
    .line 141
    const/16 v1, 0x10d

    .line 142
    .line 143
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 144
    .line 145
    .line 146
    const-string v1, "bg_one_login_btn"

    .line 147
    .line 148
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnImgPath(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 149
    .line 150
    .line 151
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇〇888:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$mLoginClickListener$1;

    .line 152
    .line 153
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnClickListener(Lcom/cmic/gen/sdk/view/GenLoginClickListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 154
    .line 155
    .line 156
    const/4 v12, 0x0

    .line 157
    invoke-virtual {v0, v12}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setCheckBoxLocation(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 158
    .line 159
    .line 160
    const-string v1, "one_login_checked"

    .line 161
    .line 162
    const-string v2, "one_login_unchecked"

    .line 163
    .line 164
    const/16 v3, 0xe

    .line 165
    .line 166
    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setCheckBoxImgPath(Ljava/lang/String;Ljava/lang/String;II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 167
    .line 168
    .line 169
    invoke-virtual {v0, v12}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyState(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 170
    .line 171
    .line 172
    sget v1, Lcom/intsig/camscanner/account/R$string;->one_login_agree_service:I

    .line 173
    .line 174
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object v1

    .line 178
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setCheckTipText(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 179
    .line 180
    .line 181
    iget-object v1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->oO80:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

    .line 182
    .line 183
    invoke-virtual {v0, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setGenCheckBoxListener(Lcom/cmic/gen/sdk/view/GenCheckBoxListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 184
    .line 185
    .line 186
    sget v1, Lcom/intsig/camscanner/account/R$string;->one_login_privacy_01:I

    .line 187
    .line 188
    const/4 v2, 0x3

    .line 189
    new-array v2, v2, [Ljava/lang/Object;

    .line 190
    .line 191
    const-string v3, "$$\u8fd0\u8425\u5546\u6761\u6b3e$$"

    .line 192
    .line 193
    aput-object v3, v2, v12

    .line 194
    .line 195
    sget v3, Lcom/intsig/camscanner/account/R$string;->cs_636_account_note1_2:I

    .line 196
    .line 197
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v4

    .line 201
    aput-object v4, v2, v11

    .line 202
    .line 203
    sget v4, Lcom/intsig/camscanner/account/R$string;->one_login_privacy_02:I

    .line 204
    .line 205
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v5

    .line 209
    const/4 v6, 0x2

    .line 210
    aput-object v5, v2, v6

    .line 211
    .line 212
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v2

    .line 216
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object v3

    .line 220
    sget-object v5, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇O8o08O:Ljava/lang/String;

    .line 221
    .line 222
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object v6

    .line 226
    sget-object v7, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇8o8o〇:Ljava/lang/String;

    .line 227
    .line 228
    const/4 v8, 0x0

    .line 229
    const/4 v9, 0x0

    .line 230
    const/4 v10, 0x0

    .line 231
    const/4 v13, 0x0

    .line 232
    move-object v1, v0

    .line 233
    move-object v4, v5

    .line 234
    move-object v5, v6

    .line 235
    move-object v6, v7

    .line 236
    move-object v7, v8

    .line 237
    move-object v8, v9

    .line 238
    move-object v9, v10

    .line 239
    move-object v10, v13

    .line 240
    invoke-virtual/range {v1 .. v10}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyAlignment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 241
    .line 242
    .line 243
    const/16 v2, 0xc

    .line 244
    .line 245
    sget v7, Lcom/intsig/camscanner/account/R$color;->cs_color_text_2:I

    .line 246
    .line 247
    invoke-static {p1, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 248
    .line 249
    .line 250
    move-result v3

    .line 251
    sget v8, Lcom/intsig/camscanner/account/R$color;->cs_color_brand:I

    .line 252
    .line 253
    invoke-static {p1, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 254
    .line 255
    .line 256
    move-result v4

    .line 257
    const/4 v5, 0x0

    .line 258
    const/4 v6, 0x1

    .line 259
    invoke-virtual/range {v1 .. v6}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyText(IIIZZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 260
    .line 261
    .line 262
    invoke-static {p1, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 263
    .line 264
    .line 265
    move-result v1

    .line 266
    invoke-static {p1, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 267
    .line 268
    .line 269
    move-result p1

    .line 270
    invoke-virtual {v0, v1, p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setClauseColor(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 271
    .line 272
    .line 273
    invoke-virtual {v0, v12}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setAppLanguageType(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 274
    .line 275
    .line 276
    invoke-virtual {v0, v11}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyBookSymbol(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 277
    .line 278
    .line 279
    iget-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇80〇808〇O:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

    .line 280
    .line 281
    invoke-virtual {v0, p1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setGenAuthLoginListener(Lcom/cmic/gen/sdk/view/GenAuthLoginListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 282
    .line 283
    .line 284
    invoke-virtual {v0}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->build()Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;

    .line 285
    .line 286
    .line 287
    move-result-object p1

    .line 288
    const-string v0, "Builder().apply {\n      \u2026stener)\n        }.build()"

    .line 289
    .line 290
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 291
    .line 292
    .line 293
    return-object p1
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public final O〇O〇oO(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇080:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final o8()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->O8:Landroid/content/Context;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final o〇0OOo〇0(Landroid/content/Context;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;
    .locals 17
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    const-string v2, "context"

    .line 6
    .line 7
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    invoke-static {v1, v2}, Lcom/intsig/utils/ext/ContextExtKt;->o〇0(Landroid/content/Context;I)I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    new-instance v13, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 19
    .line 20
    invoke-direct {v13}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;-><init>()V

    .line 21
    .line 22
    .line 23
    const/4 v14, 0x1

    .line 24
    invoke-virtual {v13, v14}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setFitsSystemWindows(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 25
    .line 26
    .line 27
    sget v3, Lcom/intsig/camscanner/account/R$color;->cs_color_bg_0:I

    .line 28
    .line 29
    invoke-static {v1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    invoke-virtual {v13, v4, v14}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setStatusBar(IZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 34
    .line 35
    .line 36
    iget-object v4, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 37
    .line 38
    invoke-virtual {v4}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->〇o〇()Z

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    if-eqz v4, :cond_0

    .line 43
    .line 44
    iget-object v4, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇080:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;

    .line 45
    .line 46
    invoke-direct {v0, v1, v4}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇0000OOO(Landroid/content/Context;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    invoke-virtual {v13, v4}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setAuthContentView(Landroid/view/View;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    iget-object v4, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 55
    .line 56
    invoke-virtual {v4}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->O8()Z

    .line 57
    .line 58
    .line 59
    move-result v4

    .line 60
    if-eqz v4, :cond_1

    .line 61
    .line 62
    iget-object v4, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇080:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;

    .line 63
    .line 64
    invoke-direct {v0, v1, v4}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->oo〇(Landroid/content/Context;Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$OnOneLoginInternalCallback;)Landroid/view/View;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    invoke-virtual {v13, v4}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setAuthContentView(Landroid/view/View;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 69
    .line 70
    .line 71
    :cond_1
    :goto_0
    new-instance v4, LO〇0888o/Oooo8o0〇;

    .line 72
    .line 73
    invoke-direct {v4, v0}, LO〇0888o/Oooo8o0〇;-><init>(Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v13, v4}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setGenBackPressedListener(Lcom/cmic/gen/sdk/view/GenBackPressedListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 77
    .line 78
    .line 79
    const/16 v4, 0x12

    .line 80
    .line 81
    invoke-virtual {v13, v4}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNavTextSize(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 82
    .line 83
    .line 84
    sget v4, Lcom/intsig/camscanner/account/R$color;->cs_color_text_4:I

    .line 85
    .line 86
    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 87
    .line 88
    .line 89
    move-result v5

    .line 90
    invoke-virtual {v13, v5}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNavTextColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 91
    .line 92
    .line 93
    invoke-static {v1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    invoke-virtual {v13, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNavColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 98
    .line 99
    .line 100
    sget v3, Lcom/intsig/camscanner/account/R$layout;->layout_one_login_privacy_title:I

    .line 101
    .line 102
    const-string v5, "iv_one_login_return_id"

    .line 103
    .line 104
    invoke-virtual {v13, v3, v5}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setClauseLayoutResID(ILjava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 105
    .line 106
    .line 107
    const/16 v3, 0x18

    .line 108
    .line 109
    const/4 v15, 0x0

    .line 110
    invoke-virtual {v13, v3, v15}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNumberSize(IZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 111
    .line 112
    .line 113
    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 114
    .line 115
    .line 116
    move-result v3

    .line 117
    invoke-virtual {v13, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNumberColor(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 118
    .line 119
    .line 120
    sget v3, Lcom/intsig/camscanner/account/R$string;->cs_638_login_quick_btn:I

    .line 121
    .line 122
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v3

    .line 126
    const-string v4, "context.getString(R.string.cs_638_login_quick_btn)"

    .line 127
    .line 128
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    iget-object v4, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 132
    .line 133
    invoke-virtual {v4}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->〇080()Z

    .line 134
    .line 135
    .line 136
    move-result v4

    .line 137
    if-eqz v4, :cond_2

    .line 138
    .line 139
    sget v3, Lcom/intsig/camscanner/account/R$string;->cs_638_bind_quick_btn:I

    .line 140
    .line 141
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v3

    .line 145
    const-string v4, "context.getString(R.string.cs_638_bind_quick_btn)"

    .line 146
    .line 147
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    :cond_2
    iget-object v4, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 151
    .line 152
    invoke-virtual {v4}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->O8()Z

    .line 153
    .line 154
    .line 155
    move-result v4

    .line 156
    if-eqz v4, :cond_3

    .line 157
    .line 158
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 159
    .line 160
    .line 161
    move-result v4

    .line 162
    sget-object v5, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 163
    .line 164
    invoke-virtual {v5}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 165
    .line 166
    .line 167
    move-result-object v6

    .line 168
    const/16 v7, 0xf0

    .line 169
    .line 170
    invoke-static {v6, v7}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 171
    .line 172
    .line 173
    move-result v6

    .line 174
    sub-int/2addr v4, v6

    .line 175
    invoke-static {v1, v4}, Lcom/intsig/utils/DisplayUtil;->Oooo8o0〇(Landroid/content/Context;I)I

    .line 176
    .line 177
    .line 178
    move-result v4

    .line 179
    invoke-virtual {v13, v4}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNumFieldOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 180
    .line 181
    .line 182
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 183
    .line 184
    .line 185
    move-result v4

    .line 186
    const/16 v6, 0x37

    .line 187
    .line 188
    invoke-virtual {v5}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 189
    .line 190
    .line 191
    move-result-object v5

    .line 192
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 193
    .line 194
    .line 195
    move-result v5

    .line 196
    sub-int/2addr v4, v5

    .line 197
    invoke-static {v1, v4}, Lcom/intsig/utils/DisplayUtil;->Oooo8o0〇(Landroid/content/Context;I)I

    .line 198
    .line 199
    .line 200
    move-result v4

    .line 201
    invoke-virtual {v13, v4}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 202
    .line 203
    .line 204
    goto :goto_1

    .line 205
    :cond_3
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 206
    .line 207
    .line 208
    move-result v4

    .line 209
    sget-object v5, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 210
    .line 211
    invoke-virtual {v5}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 212
    .line 213
    .line 214
    move-result-object v6

    .line 215
    const/16 v7, 0xfa

    .line 216
    .line 217
    invoke-static {v6, v7}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 218
    .line 219
    .line 220
    move-result v6

    .line 221
    sub-int/2addr v4, v6

    .line 222
    invoke-static {v1, v4}, Lcom/intsig/utils/DisplayUtil;->Oooo8o0〇(Landroid/content/Context;I)I

    .line 223
    .line 224
    .line 225
    move-result v4

    .line 226
    invoke-virtual {v13, v4}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setNumFieldOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 227
    .line 228
    .line 229
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 230
    .line 231
    .line 232
    move-result v4

    .line 233
    const/16 v6, 0x3c

    .line 234
    .line 235
    invoke-virtual {v5}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 236
    .line 237
    .line 238
    move-result-object v5

    .line 239
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 240
    .line 241
    .line 242
    move-result v5

    .line 243
    sub-int/2addr v4, v5

    .line 244
    invoke-static {v1, v4}, Lcom/intsig/utils/DisplayUtil;->Oooo8o0〇(Landroid/content/Context;I)I

    .line 245
    .line 246
    .line 247
    move-result v4

    .line 248
    invoke-virtual {v13, v4}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 249
    .line 250
    .line 251
    :goto_1
    sget v4, Lcom/intsig/camscanner/account/R$color;->cs_white_FFFFFF:I

    .line 252
    .line 253
    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 254
    .line 255
    .line 256
    move-result v4

    .line 257
    const/16 v5, 0x11

    .line 258
    .line 259
    invoke-virtual {v13, v3, v4, v5, v14}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnText(Ljava/lang/String;IIZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 260
    .line 261
    .line 262
    add-int/lit8 v2, v2, -0x40

    .line 263
    .line 264
    const/16 v3, 0x2c

    .line 265
    .line 266
    invoke-virtual {v13, v2, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtn(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 267
    .line 268
    .line 269
    iget-object v2, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇o00〇〇Oo:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;

    .line 270
    .line 271
    invoke-virtual {v2}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion$OneLoginThemeFactoryParams;->O8()Z

    .line 272
    .line 273
    .line 274
    move-result v2

    .line 275
    if-eqz v2, :cond_4

    .line 276
    .line 277
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 278
    .line 279
    .line 280
    move-result v2

    .line 281
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 282
    .line 283
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 284
    .line 285
    .line 286
    move-result-object v3

    .line 287
    const/16 v4, 0xaa

    .line 288
    .line 289
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 290
    .line 291
    .line 292
    move-result v3

    .line 293
    sub-int/2addr v2, v3

    .line 294
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->Oooo8o0〇(Landroid/content/Context;I)I

    .line 295
    .line 296
    .line 297
    move-result v2

    .line 298
    invoke-virtual {v13, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 299
    .line 300
    .line 301
    goto :goto_2

    .line 302
    :cond_4
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 303
    .line 304
    .line 305
    move-result v2

    .line 306
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 307
    .line 308
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 309
    .line 310
    .line 311
    move-result-object v3

    .line 312
    const/16 v4, 0xaf

    .line 313
    .line 314
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 315
    .line 316
    .line 317
    move-result v3

    .line 318
    sub-int/2addr v2, v3

    .line 319
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->Oooo8o0〇(Landroid/content/Context;I)I

    .line 320
    .line 321
    .line 322
    move-result v2

    .line 323
    invoke-virtual {v13, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnOffsetY(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 324
    .line 325
    .line 326
    :goto_2
    const-string v2, "bg_one_login_btn"

    .line 327
    .line 328
    invoke-virtual {v13, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnImgPath(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 329
    .line 330
    .line 331
    iget-object v2, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇〇888:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$mLoginClickListener$1;

    .line 332
    .line 333
    invoke-virtual {v13, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnClickListener(Lcom/cmic/gen/sdk/view/GenLoginClickListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 334
    .line 335
    .line 336
    const/16 v2, 0x20

    .line 337
    .line 338
    invoke-virtual {v13, v2, v2}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setLogBtnMargin(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 339
    .line 340
    .line 341
    invoke-virtual {v13, v15}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setCheckBoxLocation(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 342
    .line 343
    .line 344
    const-string v3, "ic_login_protocol_select"

    .line 345
    .line 346
    const-string v4, "ic_login_protocol_unselect"

    .line 347
    .line 348
    const/16 v5, 0xe

    .line 349
    .line 350
    invoke-virtual {v13, v3, v4, v5, v5}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setCheckBoxImgPath(Ljava/lang/String;Ljava/lang/String;II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 351
    .line 352
    .line 353
    invoke-virtual {v13, v15}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyState(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 354
    .line 355
    .line 356
    sget v3, Lcom/intsig/camscanner/account/R$string;->cs_625_privacy_agree_first:I

    .line 357
    .line 358
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 359
    .line 360
    .line 361
    move-result-object v3

    .line 362
    invoke-virtual {v13, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setCheckTipText(Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 363
    .line 364
    .line 365
    iget-object v3, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->oO80:Lcom/cmic/gen/sdk/view/GenCheckBoxListener;

    .line 366
    .line 367
    invoke-virtual {v13, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setGenCheckBoxListener(Lcom/cmic/gen/sdk/view/GenCheckBoxListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 368
    .line 369
    .line 370
    sget v3, Lcom/intsig/camscanner/account/R$string;->one_login_privacy_01:I

    .line 371
    .line 372
    const/4 v4, 0x3

    .line 373
    new-array v4, v4, [Ljava/lang/Object;

    .line 374
    .line 375
    const-string v5, "$$\u8fd0\u8425\u5546\u6761\u6b3e$$"

    .line 376
    .line 377
    aput-object v5, v4, v15

    .line 378
    .line 379
    sget v5, Lcom/intsig/camscanner/account/R$string;->cs_636_account_note1_2:I

    .line 380
    .line 381
    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 382
    .line 383
    .line 384
    move-result-object v6

    .line 385
    aput-object v6, v4, v14

    .line 386
    .line 387
    sget v6, Lcom/intsig/camscanner/account/R$string;->one_login_privacy_02:I

    .line 388
    .line 389
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 390
    .line 391
    .line 392
    move-result-object v7

    .line 393
    const/4 v8, 0x2

    .line 394
    aput-object v7, v4, v8

    .line 395
    .line 396
    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 397
    .line 398
    .line 399
    move-result-object v4

    .line 400
    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 401
    .line 402
    .line 403
    move-result-object v5

    .line 404
    sget-object v7, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇O8o08O:Ljava/lang/String;

    .line 405
    .line 406
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 407
    .line 408
    .line 409
    move-result-object v8

    .line 410
    sget-object v9, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇8o8o〇:Ljava/lang/String;

    .line 411
    .line 412
    const/4 v10, 0x0

    .line 413
    const/4 v11, 0x0

    .line 414
    const/4 v12, 0x0

    .line 415
    const/16 v16, 0x0

    .line 416
    .line 417
    move-object v3, v13

    .line 418
    move-object v6, v7

    .line 419
    move-object v7, v8

    .line 420
    move-object v8, v9

    .line 421
    move-object v9, v10

    .line 422
    move-object v10, v11

    .line 423
    move-object v11, v12

    .line 424
    move-object/from16 v12, v16

    .line 425
    .line 426
    invoke-virtual/range {v3 .. v12}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyAlignment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 427
    .line 428
    .line 429
    const/16 v4, 0xc

    .line 430
    .line 431
    sget v9, Lcom/intsig/camscanner/account/R$color;->cs_color_text_2:I

    .line 432
    .line 433
    invoke-static {v1, v9}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 434
    .line 435
    .line 436
    move-result v5

    .line 437
    sget v10, Lcom/intsig/camscanner/account/R$color;->cs_color_brand:I

    .line 438
    .line 439
    invoke-static {v1, v10}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 440
    .line 441
    .line 442
    move-result v6

    .line 443
    const/4 v7, 0x0

    .line 444
    const/4 v8, 0x1

    .line 445
    invoke-virtual/range {v3 .. v8}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyText(IIIZZ)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 446
    .line 447
    .line 448
    const/16 v3, 0x22

    .line 449
    .line 450
    invoke-virtual {v13, v2, v3}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyMargin(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 451
    .line 452
    .line 453
    invoke-static {v1, v9}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 454
    .line 455
    .line 456
    move-result v2

    .line 457
    invoke-static {v1, v10}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 458
    .line 459
    .line 460
    move-result v1

    .line 461
    invoke-virtual {v13, v2, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setClauseColor(II)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 462
    .line 463
    .line 464
    invoke-virtual {v13, v15}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setAppLanguageType(I)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 465
    .line 466
    .line 467
    invoke-virtual {v13, v14}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setPrivacyBookSymbol(Z)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 468
    .line 469
    .line 470
    iget-object v1, v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->〇80〇808〇O:Lcom/cmic/gen/sdk/view/GenAuthLoginListener;

    .line 471
    .line 472
    invoke-virtual {v13, v1}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->setGenAuthLoginListener(Lcom/cmic/gen/sdk/view/GenAuthLoginListener;)Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;

    .line 473
    .line 474
    .line 475
    invoke-virtual {v13}, Lcom/cmic/gen/sdk/view/GenAuthThemeConfig$Builder;->build()Lcom/cmic/gen/sdk/view/GenAuthThemeConfig;

    .line 476
    .line 477
    .line 478
    move-result-object v1

    .line 479
    const-string v2, "Builder().apply {\n\n     \u2026stener)\n        }.build()"

    .line 480
    .line 481
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 482
    .line 483
    .line 484
    return-object v1
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public final 〇8〇0〇o〇O(Ljava/lang/String;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    packed-switch v0, :pswitch_data_0

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :pswitch_0
    const-string v0, "3"

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-nez p1, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    sget-object p1, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;->ESrc:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 21
    .line 22
    goto :goto_1

    .line 23
    :pswitch_1
    const-string v0, "2"

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-nez p1, :cond_1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    sget-object p1, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;->OpenCloudSrc:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :pswitch_2
    const-string v0, "1"

    .line 36
    .line 37
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    if-nez p1, :cond_2

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    sget-object p1, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;->WapSrc:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_3
    :goto_0
    sget-object p1, Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;->WapSrc:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 48
    .line 49
    :goto_1
    iput-object p1, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->o〇0:Lcom/intsig/tsapp/account/dialog/LoginProtocolDialog$OperatorType;

    .line 50
    .line 51
    return-void

    .line 52
    nop

    .line 53
    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public final 〇O888o0o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->Oo08:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-ne v0, v2, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    :cond_0
    if-eqz v1, :cond_2

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->Oo08:Lcom/intsig/app/BaseProgressDialog;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 21
    .line 22
    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->Oo08:Lcom/intsig/app/BaseProgressDialog;

    .line 25
    .line 26
    :cond_2
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
.end method

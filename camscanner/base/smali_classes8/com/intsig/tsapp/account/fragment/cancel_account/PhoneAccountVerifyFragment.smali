.class public final Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "PhoneAccountVerifyFragment.kt"

# interfaces
.implements Lcom/intsig/tsapp/account/viewmodel/ChangeFragmentInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o〇00O:Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private OO:Ljava/lang/String;

.field private o0:Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;

.field private 〇08O〇00〇o:Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

.field private 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->o〇00O:Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private final o00〇88〇08()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->o0:Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_6

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->〇08O〇00〇o:Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    const-string v1, "mViewModel"

    .line 10
    .line 11
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;->O8〇o()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const/4 v3, 0x0

    .line 20
    const/4 v4, 0x1

    .line 21
    if-eqz v2, :cond_2

    .line 22
    .line 23
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-nez v2, :cond_1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    const/4 v2, 0x0

    .line 31
    goto :goto_1

    .line 32
    :cond_2
    :goto_0
    const/4 v2, 0x1

    .line 33
    :goto_1
    if-nez v2, :cond_3

    .line 34
    .line 35
    iget-object v2, v0, Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;->o〇00O:Landroid/widget/TextView;

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;->O8〇o()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    .line 43
    .line 44
    iget-object v2, v0, Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;->〇OOo8〇0:Landroid/widget/Button;

    .line 45
    .line 46
    invoke-virtual {v2, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 47
    .line 48
    .line 49
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;->o0ooO()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    if-eqz v2, :cond_4

    .line 54
    .line 55
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    if-nez v2, :cond_5

    .line 60
    .line 61
    :cond_4
    const/4 v3, 0x1

    .line 62
    :cond_5
    if-nez v3, :cond_6

    .line 63
    .line 64
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 65
    .line 66
    invoke-virtual {v1}, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;->o0ooO()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    .line 72
    .line 73
    :cond_6
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private final o880()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->o0:Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    new-array v1, v1, [Landroid/view/View;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;->〇OOo8〇0:Landroid/widget/Button;

    .line 10
    .line 11
    aput-object v0, v1, v2

    .line 12
    .line 13
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private final oO〇oo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->〇08O〇00〇o:Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mViewModel"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;->〇〇〇0〇〇0()Landroidx/lifecycle/MutableLiveData;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment$addObserver$1;

    .line 16
    .line 17
    invoke-direct {v1, p0}, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment$addObserver$1;-><init>(Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;)V

    .line 18
    .line 19
    .line 20
    new-instance v2, LOOO〇/〇o00〇〇Oo;

    .line 21
    .line 22
    invoke-direct {v2, v1}, LOOO〇/〇o00〇〇Oo;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
.end method

.method private static final oooO888(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇O8oOo0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->oooO888(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public beforeInitialize()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->beforeInitialize()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const-string v1, "area_code"

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    iput-object v1, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->OO:Ljava/lang/String;

    .line 17
    .line 18
    const-string v1, "phone_number"

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->〇OOo8〇0:Ljava/lang/String;

    .line 25
    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object p1, v0

    .line 17
    :goto_0
    sget v1, Lcom/intsig/camscanner/account/R$id;->btn_verify_phone_next:I

    .line 18
    .line 19
    if-nez p1, :cond_1

    .line 20
    .line 21
    goto :goto_2

    .line 22
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-ne p1, v1, :cond_4

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->o0:Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;

    .line 29
    .line 30
    if-eqz p1, :cond_4

    .line 31
    .line 32
    iget-object p1, p1, Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 33
    .line 34
    const-string v1, ""

    .line 35
    .line 36
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->〇08O〇00〇o:Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

    .line 40
    .line 41
    const-string v1, "mViewModel"

    .line 42
    .line 43
    if-nez p1, :cond_2

    .line 44
    .line 45
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    move-object p1, v0

    .line 49
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;->O8〇o()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    if-eqz p1, :cond_4

    .line 54
    .line 55
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->〇08O〇00〇o:Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

    .line 56
    .line 57
    if-nez v2, :cond_3

    .line 58
    .line 59
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_3
    move-object v0, v2

    .line 64
    :goto_1
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 65
    .line 66
    const-string v2, "mActivity"

    .line 67
    .line 68
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v1, p1}, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;->O8ooOoo〇(Landroid/app/Activity;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    :cond_4
    :goto_2
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iput-object p1, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->o0:Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;

    .line 8
    .line 9
    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    .line 10
    .line 11
    invoke-direct {p1, p0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 12
    .line 13
    .line 14
    const-class v0, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    check-cast p1, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

    .line 21
    .line 22
    iput-object p1, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->〇08O〇00〇o:Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    const-string v0, " "

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->o880()V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->〇08O〇00〇o:Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;

    .line 35
    .line 36
    if-nez p1, :cond_0

    .line 37
    .line 38
    const-string p1, "mViewModel"

    .line 39
    .line 40
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const/4 p1, 0x0

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 45
    .line 46
    const-string v1, "mActivity"

    .line 47
    .line 48
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->〇OOo8〇0:Ljava/lang/String;

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->OO:Ljava/lang/String;

    .line 54
    .line 55
    invoke-virtual {p1, v0, v1, v2, p0}, Lcom/intsig/tsapp/account/viewmodel/PhoneAccountVerifyViewModel;->o〇0OOo〇0(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/tsapp/account/viewmodel/ChangeFragmentInterface;)V

    .line 56
    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->oO〇oo()V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->o00〇88〇08()V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public o〇〇0〇(Lcom/intsig/mvp/fragment/BaseChangeFragment;)V
    .locals 2
    .param p1    # Lcom/intsig/mvp/fragment/BaseChangeFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "baseChangeFragment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const-string v1, "PhoneAccountVerifyFragment"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇〇0o(Landroid/app/Activity;Ljava/lang/String;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    const-string v1, "null cannot be cast to non-null type com.intsig.tsapp.account.LoginMainActivity"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 26
    .line 27
    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/account/R$layout;->fragment_phone_account_verify:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final 〇o00〇〇Oo(Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/cancel_account/PhoneAccountVerifyFragment;->o0:Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/FragmentPhoneAccountVerifyBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    new-array v1, v1, [Landroid/view/View;

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    aput-object v0, v1, v2

    .line 14
    .line 15
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->checkTargetNonNull([Landroid/view/View;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "VerifyCodeFragment.java"

# interfaces
.implements Lcom/intsig/tsapp/account/iview/IVerifyCodeView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;,
        Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$OneLineKeyBoard;,
        Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$SixBoxKeyBoard;,
        Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;,
        Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$IOnComplete;
    }
.end annotation


# instance fields
.field private O0O:Ljava/lang/String;

.field private O88O:Ljava/lang/String;

.field private O8o08O8O:Landroid/widget/TextView;

.field private OO:Landroid/widget/TextView;

.field private OO〇00〇8oO:Landroid/widget/EditText;

.field private final Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

.field private Ooo08:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

.field private final O〇08oOOO0:Lcom/intsig/util/CountdownTimer$OnCountdownListener;

.field private final O〇o88o08〇:I

.field private o0:Landroid/widget/TextView;

.field private o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

.field private o8oOOo:Ljava/lang/String;

.field private o8〇OO:Lcom/intsig/app/ProgressDialogClient;

.field private final o8〇OO0〇0o:I

.field private oOO〇〇:Ljava/lang/String;

.field private oOo0:Lcom/intsig/tsapp/account/widget/VerifyCodeView;

.field private oOo〇8o008:Landroid/widget/TextView;

.field private oo8ooo8O:Lcom/intsig/util/CountdownTimer;

.field private ooo0〇〇O:Ljava/lang/String;

.field private o〇00O:Landroid/widget/TextView;

.field private o〇oO:Z

.field private final 〇00O0:[I

.field private 〇080OO8〇0:Landroid/widget/TextView;

.field private 〇08O〇00〇o:Landroid/widget/TextView;

.field private 〇08〇o0O:Ljava/lang/String;

.field private 〇0O:Landroid/widget/TextView;

.field private 〇8〇oO〇〇8o:Ljava/lang/String;

.field private final 〇OO8ooO8〇:I

.field private 〇OOo8〇0:Landroid/widget/TextView;

.field private 〇O〇〇O8:Ljava/lang/String;

.field private 〇o0O:I

.field private 〇〇08O:Ljava/lang/String;

.field private final 〇〇o〇:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x64

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8〇OO0〇0o:I

    .line 7
    .line 8
    const/4 v0, -0x1

    .line 9
    iput v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o0O:I

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇oO:Z

    .line 13
    .line 14
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 15
    .line 16
    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 17
    .line 18
    .line 19
    iput-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇o〇:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/tsapp/account/presenter/impl/VerifyCodePresenter;

    .line 22
    .line 23
    invoke-direct {v0, p0}, Lcom/intsig/tsapp/account/presenter/impl/VerifyCodePresenter;-><init>(Lcom/intsig/tsapp/account/iview/IVerifyCodeView;)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 27
    .line 28
    const/16 v0, 0x65

    .line 29
    .line 30
    iput v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O〇o88o08〇:I

    .line 31
    .line 32
    const/4 v0, 0x3

    .line 33
    new-array v0, v0, [I

    .line 34
    .line 35
    fill-array-data v0, :array_0

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇00O0:[I

    .line 39
    .line 40
    new-instance v0, LoO8o〇o〇8/Ooo;

    .line 41
    .line 42
    invoke-direct {v0, p0}, LoO8o〇o〇8/Ooo;-><init>(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;)V

    .line 43
    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O〇08oOOO0:Lcom/intsig/util/CountdownTimer$OnCountdownListener;

    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/tsapp/sync/AppConfigJsonGet;->〇o00〇〇Oo()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->soft_keyboard_style:I

    .line 52
    .line 53
    iput v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇OO8ooO8〇:I

    .line 54
    .line 55
    return-void

    .line 56
    nop

    .line 57
    :array_0
    .array-data 4
        0x64
        0x65
        0x66
    .end array-data
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static O08〇(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;)Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;
    .locals 5
    .param p0    # Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O0o〇〇o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    move-result v0

    const-string v1, "  phoneVCODE = "

    const-string v2, "  phoneNumber = "

    const-string v3, "VerifyCodeFragment"

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇〇888()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 3
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "PHONE_REGISTER >>>   areaCode = "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇〇888()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 5
    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 6
    :cond_1
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇o〇()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->oO80()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 8
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "EMAIL_REGISTER >>>   email = "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇o〇()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " pwd = "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->oO80()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 9
    :cond_3
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8〇8〇O80(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 10
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇〇888()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 11
    :cond_4
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "PHONE_VERIFY_CODE_LOGIN >>>   areaCode = "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇〇888()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 13
    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 14
    :cond_5
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 15
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇o〇()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->Oo08()I

    move-result v0

    if-ltz v0, :cond_6

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->oO80()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_6
    return-object v4

    .line 16
    :cond_7
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 17
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->Oo08()I

    move-result v0

    if-ltz v0, :cond_8

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->oO80()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_8
    return-object v4

    .line 18
    :cond_9
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 19
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇o〇()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 20
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "EMAIL_FORGET_PWD >>> email = "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇o〇()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "  emailPostal = "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->O8()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 21
    :cond_a
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8O0880(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    move-result v0

    const-string v1, "  phoneNumber="

    if-eqz v0, :cond_c

    .line 22
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 23
    :cond_b
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "PHONE_FORGET_PWD >>> areaCode="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 24
    :cond_c
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_ACCOUNT_VERIFY:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    if-ne p0, v0, :cond_e

    .line 25
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 26
    :cond_d
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "PHONE_ACCOUNT_VERIFY >>> areaCode="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 27
    :cond_e
    new-instance v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;

    invoke-direct {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;-><init>()V

    .line 28
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "args_from_where"

    .line 29
    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 30
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇〇888()Ljava/lang/String;

    move-result-object p0

    const-string v2, "args_phone_v_code"

    invoke-virtual {v1, v2, p0}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "args_area_code"

    .line 31
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇080()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "args_phone_number"

    .line 32
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->o〇0()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "args_email"

    .line 33
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇o〇()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "args_pwd"

    .line 34
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->oO80()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "args_email_postal"

    .line 35
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->O8()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "args_error_type_for_pwd_login"

    .line 36
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->Oo08()I

    move-result v2

    invoke-virtual {v1, p0, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    const-string p0, "args_token"

    .line 37
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->OO0o〇〇〇〇0()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "args_sms_token"

    .line 38
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇80〇808〇O()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "args_domain"

    .line 39
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/model/VerifyCodeFragmentParams;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p0, p1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static O0O0〇(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O0o〇〇o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8〇8〇O80(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-nez v0, :cond_1

    .line 24
    .line 25
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_1

    .line 30
    .line 31
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->SETTING_SUPER_VERIFY_CODE:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 32
    .line 33
    if-eq p0, v0, :cond_1

    .line 34
    .line 35
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-nez v0, :cond_1

    .line 40
    .line 41
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8O0880(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-nez v0, :cond_1

    .line 46
    .line 47
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_ACCOUNT_VERIFY:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 48
    .line 49
    if-eq p0, v0, :cond_1

    .line 50
    .line 51
    invoke-static {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 52
    .line 53
    .line 54
    move-result p0

    .line 55
    if-eqz p0, :cond_0

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_0
    const/4 p0, 0x0

    .line 59
    goto :goto_1

    .line 60
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 61
    :goto_1
    return p0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private O0〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO:Landroid/widget/TextView;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 29
    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO:Landroid/widget/TextView;

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO:Landroid/widget/TextView;

    .line 38
    .line 39
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 40
    .line 41
    invoke-direct {p0, v3}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o〇88〇8(Ljava/lang/String;)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 49
    .line 50
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 51
    .line 52
    .line 53
    :goto_1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 54
    .line 55
    sget-object v3, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 56
    .line 57
    if-eq v0, v3, :cond_2

    .line 58
    .line 59
    sget-object v3, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 60
    .line 61
    if-eq v0, v3, :cond_2

    .line 62
    .line 63
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-eqz v0, :cond_3

    .line 68
    .line 69
    :cond_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8oOOo:Ljava/lang/String;

    .line 70
    .line 71
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    if-nez v0, :cond_3

    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 78
    .line 79
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 84
    .line 85
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 86
    .line 87
    .line 88
    :goto_2
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static synthetic O0〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O〇8〇008(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static O8〇8〇O80(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_VERIFY_CODE_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_VERIFY_CODE_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private OO0O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇o〇:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 14
    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, LoO8o〇o〇8/ooo〇8oO;

    .line 21
    .line 22
    invoke-direct {v1, p0}, LoO8o〇o〇8/ooo〇8oO;-><init>(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
.end method

.method private Ooo8o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_register_account:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Landroid/widget/TextView;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0:Landroid/widget/TextView;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 14
    .line 15
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_send_to:I

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Landroid/widget/TextView;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 26
    .line 27
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_check_email:I

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Landroid/widget/TextView;

    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO:Landroid/widget/TextView;

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 38
    .line 39
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_check_email_hint:I

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    check-cast v0, Landroid/widget/TextView;

    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 50
    .line 51
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_error_msg:I

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    check-cast v0, Landroid/widget/TextView;

    .line 58
    .line 59
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇00O:Landroid/widget/TextView;

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 62
    .line 63
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_check_activate_mail:I

    .line 64
    .line 65
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    check-cast v0, Landroid/widget/TextView;

    .line 70
    .line 71
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 74
    .line 75
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_resend:I

    .line 76
    .line 77
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    check-cast v0, Landroid/widget/TextView;

    .line 82
    .line 83
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 86
    .line 87
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_not_receive:I

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    check-cast v0, Landroid/widget/TextView;

    .line 94
    .line 95
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0O:Landroid/widget/TextView;

    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 98
    .line 99
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_contact_us:I

    .line 100
    .line 101
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    check-cast v0, Landroid/widget/TextView;

    .line 106
    .line 107
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 108
    .line 109
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 110
    .line 111
    sget v1, Lcom/intsig/camscanner/account/R$id;->vcv_verify_code_input:I

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    check-cast v0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;

    .line 118
    .line 119
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOo0:Lcom/intsig/tsapp/account/widget/VerifyCodeView;

    .line 120
    .line 121
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 122
    .line 123
    sget v1, Lcom/intsig/camscanner/account/R$id;->et_verify_code_input:I

    .line 124
    .line 125
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    check-cast v0, Landroid/widget/EditText;

    .line 130
    .line 131
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO〇00〇8oO:Landroid/widget/EditText;

    .line 132
    .line 133
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private O〇080〇o0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 4
    .line 5
    if-eq v0, v1, :cond_0

    .line 6
    .line 7
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_PWD_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 8
    .line 9
    if-ne v0, v1, :cond_1

    .line 10
    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O08000()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0O:Landroid/widget/TextView;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0O:Landroid/widget/TextView;

    .line 25
    .line 26
    const/16 v1, 0x8

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 29
    .line 30
    .line 31
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private O〇0O〇Oo〇o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O8〇8000(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 10
    .line 11
    invoke-direct {p0, v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0oO〇oo00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0O:Landroid/widget/TextView;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 24
    .line 25
    const/16 v1, 0x8

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 28
    .line 29
    .line 30
    :cond_1
    return-void
.end method

.method private synthetic O〇8〇008(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x64

    .line 8
    .line 9
    iput v1, v0, Landroid/os/Message;->what:I

    .line 10
    .line 11
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private O〇〇O80o8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0:Landroid/widget/TextView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 8
    .line 9
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 10
    .line 11
    if-eq v0, v1, :cond_4

    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 21
    .line 22
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0:Landroid/widget/TextView;

    .line 29
    .line 30
    sget v1, Lcom/intsig/camscanner/account/R$string;->find_pwd_title:I

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 33
    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 37
    .line 38
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_ACCOUNT_VERIFY:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 39
    .line 40
    if-eq v0, v1, :cond_3

    .line 41
    .line 42
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0:Landroid/widget/TextView;

    .line 50
    .line 51
    const/4 v1, 0x4

    .line 52
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 53
    .line 54
    .line 55
    goto :goto_2

    .line 56
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0:Landroid/widget/TextView;

    .line 57
    .line 58
    sget v1, Lcom/intsig/camscanner/account/R$string;->a_hint_input_validated_code:I

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 61
    .line 62
    .line 63
    goto :goto_2

    .line 64
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0:Landroid/widget/TextView;

    .line 65
    .line 66
    sget v1, Lcom/intsig/camscanner/account/R$string;->cs_542_renew_20:I

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 69
    .line 70
    .line 71
    :goto_2
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇Oo〇O(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private o0Oo()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    sget v1, Lcom/intsig/camscanner/account/R$string;->cs_519b_vcode_sendto:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 10
    .line 11
    invoke-static {v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-nez v1, :cond_5

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 18
    .line 19
    invoke-static {v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    goto/16 :goto_2

    .line 26
    .line 27
    :cond_0
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 28
    .line 29
    const/4 v2, 0x0

    .line 30
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 34
    .line 35
    invoke-static {v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O0o〇〇o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    const-string v2, " +"

    .line 40
    .line 41
    const-string v3, " "

    .line 42
    .line 43
    if-nez v1, :cond_4

    .line 44
    .line 45
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 46
    .line 47
    invoke-static {v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8〇8〇O80(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    if-nez v1, :cond_4

    .line 52
    .line 53
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 54
    .line 55
    invoke-static {v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    if-nez v1, :cond_4

    .line 60
    .line 61
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 62
    .line 63
    sget-object v4, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_ACCOUNT_VERIFY:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 64
    .line 65
    if-ne v1, v4, :cond_1

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    invoke-static {v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    if-eqz v1, :cond_2

    .line 73
    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 86
    .line 87
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    goto :goto_1

    .line 95
    :cond_2
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 96
    .line 97
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 98
    .line 99
    .line 100
    move-result v1

    .line 101
    if-nez v1, :cond_3

    .line 102
    .line 103
    new-instance v1, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 112
    .line 113
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    goto :goto_1

    .line 121
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 122
    .line 123
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 133
    .line 134
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 141
    .line 142
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    goto :goto_1

    .line 150
    :cond_4
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 151
    .line 152
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .line 154
    .line 155
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 162
    .line 163
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    .line 165
    .line 166
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 170
    .line 171
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    :goto_1
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 179
    .line 180
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    .line 182
    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    .line 184
    .line 185
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .line 187
    .line 188
    const-string v2, "setSendToVisible >>> sendToStr = "

    .line 189
    .line 190
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    const-string v1, "VerifyCodeFragment"

    .line 201
    .line 202
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    goto :goto_3

    .line 206
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 207
    .line 208
    const/16 v1, 0x8

    .line 209
    .line 210
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 211
    .line 212
    .line 213
    :goto_3
    return-void
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method private o0〇〇00()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇o〇()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private o808o8o08()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇o〇:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private synthetic o88()Lkotlin/Unit;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static synthetic o880(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o88()Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private oOoO8OO〇(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-static {p1, v1}, Lcom/intsig/tsapp/account/fragment/EmailLoginFragment;->〇8〇80o(Ljava/lang/String;Z)Lcom/intsig/tsapp/account/fragment/EmailLoginFragment;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic oO〇oo(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic oooO888(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇oo(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o〇08oO80o()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->SETTING_SUPER_VERIFY_CODE:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    iget-boolean v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇oO:Z

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 16
    .line 17
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 18
    .line 19
    invoke-interface {v0, v1, v2, v3}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private declared-synchronized o〇0〇o()Z
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇o〇:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 3
    .line 4
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o808o8o08()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11
    .line 12
    .line 13
    monitor-exit p0

    .line 14
    const/4 v0, 0x1

    .line 15
    return v0

    .line 16
    :cond_0
    monitor-exit p0

    .line 17
    const/4 v0, 0x0

    .line 18
    return v0

    .line 19
    :catchall_0
    move-exception v0

    .line 20
    monitor-exit p0

    .line 21
    throw v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private o〇o08〇()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    sget v3, Lcom/intsig/camscanner/account/R$color;->cs_color_text_1:I

    .line 12
    .line 13
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 21
    .line 22
    sget v2, Lcom/intsig/camscanner/account/R$string;->cs_542_renew_33:I

    .line 23
    .line 24
    const/4 v3, 0x1

    .line 25
    new-array v3, v3, [Ljava/lang/Object;

    .line 26
    .line 27
    new-instance v4, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    iget-object v5, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oo8ooo8O:Lcom/intsig/util/CountdownTimer;

    .line 33
    .line 34
    invoke-virtual {v5}, Lcom/intsig/util/CountdownTimer;->〇〇888()I

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string v5, ""

    .line 42
    .line 43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    aput-object v4, v3, v1

    .line 51
    .line 52
    invoke-virtual {p0, v2, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oo8ooo8O:Lcom/intsig/util/CountdownTimer;

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/intsig/util/CountdownTimer;->〇8o8o〇()V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private synthetic o〇oo(Ljava/lang/String;)V
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇o00〇〇Oo()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o808o8o08()V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 24
    .line 25
    invoke-interface {v0, v1, v2, p1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    goto/16 :goto_3

    .line 29
    .line 30
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 31
    .line 32
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O0o〇〇o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-nez v0, :cond_b

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 39
    .line 40
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8〇8〇O80(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-eqz v0, :cond_1

    .line 45
    .line 46
    goto/16 :goto_2

    .line 47
    .line 48
    :cond_1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 49
    .line 50
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-eqz v0, :cond_2

    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 57
    .line 58
    const/4 v2, 0x1

    .line 59
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 60
    .line 61
    const/4 v4, 0x0

    .line 62
    iget-object v5, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 63
    .line 64
    move-object v6, p1

    .line 65
    invoke-interface/range {v1 .. v6}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->O8(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    goto/16 :goto_3

    .line 69
    .line 70
    :cond_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 71
    .line 72
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-eqz v0, :cond_3

    .line 77
    .line 78
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 79
    .line 80
    const/4 v2, 0x0

    .line 81
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 82
    .line 83
    iget-object v4, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 84
    .line 85
    iget-object v5, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 86
    .line 87
    move-object v6, p1

    .line 88
    invoke-interface/range {v1 .. v6}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->O8(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    goto/16 :goto_3

    .line 92
    .line 93
    :cond_3
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 94
    .line 95
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    if-eqz v0, :cond_4

    .line 100
    .line 101
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 102
    .line 103
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 104
    .line 105
    const/4 v3, 0x1

    .line 106
    iget-object v4, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 107
    .line 108
    const/4 v5, 0x0

    .line 109
    move-object v6, p1

    .line 110
    invoke-interface/range {v1 .. v6}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇〇808〇(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    goto/16 :goto_3

    .line 114
    .line 115
    :cond_4
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 116
    .line 117
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8O0880(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 118
    .line 119
    .line 120
    move-result v0

    .line 121
    if-eqz v0, :cond_5

    .line 122
    .line 123
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 124
    .line 125
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 126
    .line 127
    const/4 v3, 0x0

    .line 128
    iget-object v4, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 129
    .line 130
    iget-object v5, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 131
    .line 132
    move-object v6, p1

    .line 133
    invoke-interface/range {v1 .. v6}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇〇808〇(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    goto :goto_3

    .line 137
    :cond_5
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 138
    .line 139
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->SETTING_SUPER_VERIFY_CODE:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 140
    .line 141
    if-ne v0, v1, :cond_6

    .line 142
    .line 143
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 144
    .line 145
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 146
    .line 147
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 148
    .line 149
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 150
    .line 151
    invoke-interface {v0, v1, v2, v3, p1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->OO0o〇〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    goto :goto_3

    .line 155
    :cond_6
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_ACCOUNT_VERIFY:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 156
    .line 157
    const/4 v2, 0x0

    .line 158
    if-ne v0, v1, :cond_7

    .line 159
    .line 160
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 161
    .line 162
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 163
    .line 164
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 165
    .line 166
    invoke-interface {v0, v2, v1, v3, p1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->Oooo8o0〇(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    goto :goto_3

    .line 170
    :cond_7
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 171
    .line 172
    .line 173
    move-result v0

    .line 174
    if-eqz v0, :cond_a

    .line 175
    .line 176
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 177
    .line 178
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 179
    .line 180
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->BIND_EMAIL:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 181
    .line 182
    if-ne v0, v1, :cond_8

    .line 183
    .line 184
    const/4 v2, 0x1

    .line 185
    const/4 v4, 0x1

    .line 186
    goto :goto_0

    .line 187
    :cond_8
    const/4 v4, 0x0

    .line 188
    :goto_0
    iget-object v5, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 189
    .line 190
    if-ne v0, v1, :cond_9

    .line 191
    .line 192
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 193
    .line 194
    goto :goto_1

    .line 195
    :cond_9
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 196
    .line 197
    :goto_1
    move-object v6, v0

    .line 198
    iget-object v8, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08〇o0O:Ljava/lang/String;

    .line 199
    .line 200
    move-object v7, p1

    .line 201
    invoke-interface/range {v3 .. v8}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇o〇(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    .line 203
    .line 204
    goto :goto_3

    .line 205
    :cond_a
    const-string p1, "VerifyCodeFragment"

    .line 206
    .line 207
    const-string v0, "afterTextChanged UN DO"

    .line 208
    .line 209
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    goto :goto_3

    .line 213
    :cond_b
    :goto_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 214
    .line 215
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 216
    .line 217
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 218
    .line 219
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 220
    .line 221
    invoke-interface {v0, v1, v2, p1, v3}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    :goto_3
    return-void
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private 〇088O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O0〇(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-boolean v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇oO:Z

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇o08〇()V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static 〇08O(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_PWD_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_PWD_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private 〇0oO〇oo00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-eq p1, v0, :cond_1

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 10
    .line 11
    if-ne p1, v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p1, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 17
    :goto_1
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private 〇0ooOOo()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, LoO8o〇o〇8/O0o〇〇Oo;

    .line 6
    .line 7
    invoke-direct {v1, p0}, LoO8o〇o〇8/O0o〇〇Oo;-><init>(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇080(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$IOnComplete;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->Oo08()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private 〇0〇0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const-string v1, "VerifyCodeFragment"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇〇0o(Landroid/app/Activity;Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    new-instance v1, LoO8o〇o〇8/〇0;

    .line 14
    .line 15
    invoke-direct {v1, p0, p1, p2}, LoO8o〇o〇8/〇0;-><init>(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇8O0880(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private 〇8〇80o(I)Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0x66

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    const/16 v0, 0xd3

    .line 6
    .line 7
    if-eq p1, v0, :cond_0

    .line 8
    .line 9
    packed-switch p1, :pswitch_data_0

    .line 10
    .line 11
    .line 12
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_error_validate_number:I

    .line 13
    .line 14
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    goto :goto_0

    .line 19
    :pswitch_0
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_global_toast_network_error:I

    .line 20
    .line 21
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    goto :goto_0

    .line 26
    :cond_0
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_send_sms_error_211:I

    .line 27
    .line 28
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    goto :goto_0

    .line 33
    :cond_1
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_error_phone:I

    .line 34
    .line 35
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    :goto_0
    return-object p1

    .line 40
    nop

    .line 41
    :pswitch_data_0
    .packed-switch -0x65
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private 〇8〇OOoooo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O0o〇〇o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {v0, v1, v2}, Lcom/intsig/tsapp/account/util/AccountUtils;->o0O0(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 20
    .line 21
    invoke-static {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    invoke-static {v0, v1, v2}, Lcom/intsig/tsapp/account/util/AccountUtils;->o0O0(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static 〇O0o〇〇o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇O〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private 〇O8〇8000(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-eq p1, v0, :cond_1

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_VERIFY_CODE_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 10
    .line 11
    if-eq p1, v0, :cond_1

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_PWD_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 14
    .line 15
    if-ne p1, v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 p1, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 21
    :goto_1
    return p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private synthetic 〇O8〇8O0oO(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOoO8OO〇(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇Oo〇O(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 1
    const-string v0, "VerifyCodeFragment"

    .line 2
    .line 3
    const-string v1, "go2AutoLoginByEmail"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o808o8o08()V

    .line 9
    .line 10
    .line 11
    const-string v0, "verification_reg_success"

    .line 12
    .line 13
    const-string v1, "email"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇00O0O0(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇o00〇〇Oo()V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 26
    .line 27
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    const/4 v2, 0x0

    .line 31
    invoke-static {p1, p2, v1, v2}, Lcom/intsig/tsapp/account/fragment/EmailLoginFragment;->o〇O8OO(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/intsig/tsapp/account/fragment/EmailLoginFragment;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->BIND_EMAIL:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->BIND_PHONE:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private 〇oO88o()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O〇〇O80o8()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0Oo()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0〇()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O〇080〇o0()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O〇0O〇Oo〇o()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private 〇oO〇08o(Ljava/lang/String;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "RestrictedApi"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇〇O80〇0o()Landroidx/appcompat/widget/AppCompatTextView;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 13
    .line 14
    .line 15
    const/16 v2, 0x12

    .line 16
    .line 17
    const/4 v3, 0x2

    .line 18
    const/16 v4, 0x10

    .line 19
    .line 20
    invoke-virtual {v1, v4, v2, v0, v3}, Landroidx/appcompat/widget/AppCompatTextView;->setAutoSizeTextTypeUniformWithConfiguration(IIII)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 28
    .line 29
    .line 30
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private 〇o〇88〇8(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    sget v1, Lcom/intsig/camscanner/account/R$string;->cs_542_renew_30:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x1

    .line 10
    new-array v1, v1, [Ljava/lang/Object;

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    aput-object p1, v1, v2

    .line 14
    .line 15
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    return-object p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private synthetic 〇〇()V
    .locals 6

    .line 1
    const-string v0, "VerifyCodeFragment"

    .line 2
    .line 3
    :goto_0
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    if-eqz v1, :cond_2

    .line 6
    .line 7
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_2

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇o〇:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 14
    .line 15
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_2

    .line 20
    .line 21
    const-wide/16 v1, 0x7d0

    .line 22
    .line 23
    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .line 25
    .line 26
    goto :goto_1

    .line 27
    :catch_0
    move-exception v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 36
    .line 37
    .line 38
    :goto_1
    :try_start_1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->O8()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    const-string v2, "email"

    .line 43
    .line 44
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 45
    .line 46
    const/4 v4, 0x0

    .line 47
    const/4 v5, 0x1

    .line 48
    invoke-static {v2, v3, v4, v1, v5}, Lcom/intsig/tianshu/TianShuAPI;->O〇oO〇oo8o(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-nez v2, :cond_0

    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇0〇o()Z

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    if-eqz v1, :cond_2

    .line 63
    .line 64
    const-string v1, "startAutoQueryInThread, autologin"

    .line 65
    .line 66
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 70
    .line 71
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 72
    .line 73
    invoke-direct {p0, v1, v2}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0〇0(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    goto :goto_2

    .line 77
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    const-string v3, "startAutoQueryInThread userId="

    .line 83
    .line 84
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1 .. :try_end_1} :catch_1

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :catch_1
    move-exception v1

    .line 99
    invoke-virtual {v1}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 100
    .line 101
    .line 102
    move-result v2

    .line 103
    const/16 v3, 0xc9

    .line 104
    .line 105
    if-ne v2, v3, :cond_1

    .line 106
    .line 107
    const-string v1, "NOT REGISTER"

    .line 108
    .line 109
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_1
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 114
    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_2
    :goto_2
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static 〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1
    .param p0    # Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O8〇8O0oO(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static 〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static 〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->EMAIL_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_FORGET_PWD:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 13
    :goto_1
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private synthetic 〇〇〇O〇(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    instance-of p2, p1, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 4
    .line 5
    if-eqz p2, :cond_0

    .line 6
    .line 7
    check-cast p1, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 8
    .line 9
    const-class p2, Lcom/intsig/tsapp/account/fragment/LoginMainFragment;

    .line 10
    .line 11
    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-virtual {p1, p2, v0}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇ooO8Ooo〇(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public O08000()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o0O:I

    .line 2
    .line 3
    const/16 v1, 0xd9

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public O80〇O〇080()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8〇OO:Lcom/intsig/app/ProgressDialogClient;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/app/ProgressDialogClient;->〇080()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public O880oOO08(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 1
    const-string v0, "VerifyCodeFragment"

    .line 2
    .line 3
    const-string v1, "go2AutoLoginByEmail"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o808o8o08()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 12
    .line 13
    sget-object v1, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    if-ne v0, v1, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 21
    .line 22
    sget-object v1, Lcom/intsig/tsapp/account/fragment/DefaultEmailLoginFragment;->〇〇08O:Lcom/intsig/tsapp/account/fragment/DefaultEmailLoginFragment$Companion;

    .line 23
    .line 24
    invoke-virtual {v1, p1, p2, v2, v2}, Lcom/intsig/tsapp/account/fragment/DefaultEmailLoginFragment$Companion;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/intsig/tsapp/account/fragment/DefaultEmailLoginFragment;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 33
    .line 34
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 35
    .line 36
    invoke-static {p1, p2, v2, v2}, Lcom/intsig/tsapp/account/fragment/EmailLoginFragment;->o〇O8OO(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/intsig/tsapp/account/fragment/EmailLoginFragment;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 41
    .line 42
    .line 43
    :goto_0
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public OO0o〇〇〇〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public OO8oO0o〇(ILjava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x65

    .line 8
    .line 9
    iput v1, v0, Landroid/os/Message;->what:I

    .line 10
    .line 11
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 12
    .line 13
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public OOO8o〇〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public Oo0O0o8()V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v1, v0, [Landroid/view/View;

    .line 3
    .line 4
    const/4 v2, 0x0

    .line 5
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 6
    .line 7
    aput-object v3, v1, v2

    .line 8
    .line 9
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->checkTargetNonNull([Landroid/view/View;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    sget v2, Lcom/intsig/camscanner/account/R$color;->cs_color_brand:I

    .line 25
    .line 26
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 34
    .line 35
    sget v1, Lcom/intsig/camscanner/account/R$string;->a_label_reget_verifycode:I

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0〇〇00()V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oo8ooo8O:Lcom/intsig/util/CountdownTimer;

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/util/CountdownTimer;->Oo08()V

    .line 46
    .line 47
    .line 48
    :cond_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public Ooo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 10

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_resend:I

    .line 6
    .line 7
    const-string v2, "VerifyCodeFragment"

    .line 8
    .line 9
    if-ne v0, v1, :cond_d

    .line 10
    .line 11
    const-string p1, "VERIFY CODE RESEND"

    .line 12
    .line 13
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    invoke-static {p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇8(Landroid/content/Context;)Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    const/4 v0, 0x0

    .line 23
    if-nez p1, :cond_0

    .line 24
    .line 25
    const/16 p1, -0x64

    .line 26
    .line 27
    invoke-virtual {p0, p1, v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO8oO0o〇(ILjava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_0
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇00O:Landroid/widget/TextView;

    .line 32
    .line 33
    const-string v1, ""

    .line 34
    .line 35
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o08〇〇0O()V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 42
    .line 43
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O0o〇〇o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-eqz p1, :cond_1

    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 52
    .line 53
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 54
    .line 55
    invoke-interface {p1, v0, v1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_2

    .line 59
    .line 60
    :cond_1
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 61
    .line 62
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8〇8〇O80(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    if-eqz p1, :cond_2

    .line 67
    .line 68
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 73
    .line 74
    invoke-interface {p1, v0, v1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto/16 :goto_2

    .line 78
    .line 79
    :cond_2
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 80
    .line 81
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇O80〇0o(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    if-eqz p1, :cond_3

    .line 86
    .line 87
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 90
    .line 91
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 92
    .line 93
    invoke-interface {p1, v0, v1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    goto/16 :goto_2

    .line 97
    .line 98
    :cond_3
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 99
    .line 100
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 101
    .line 102
    .line 103
    move-result p1

    .line 104
    const/4 v1, 0x1

    .line 105
    if-eqz p1, :cond_4

    .line 106
    .line 107
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 108
    .line 109
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 110
    .line 111
    invoke-interface {p1, v1, v2, v0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->o〇0(ZLjava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    goto/16 :goto_2

    .line 115
    .line 116
    :cond_4
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 117
    .line 118
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 119
    .line 120
    .line 121
    move-result p1

    .line 122
    const/4 v3, 0x0

    .line 123
    if-eqz p1, :cond_5

    .line 124
    .line 125
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 128
    .line 129
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 130
    .line 131
    invoke-interface {p1, v3, v0, v1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->o〇0(ZLjava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    goto/16 :goto_2

    .line 135
    .line 136
    :cond_5
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 137
    .line 138
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 139
    .line 140
    .line 141
    move-result p1

    .line 142
    if-eqz p1, :cond_6

    .line 143
    .line 144
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 145
    .line 146
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 147
    .line 148
    invoke-interface {p1, v1, v2, v0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇〇888(ZLjava/lang/String;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    goto/16 :goto_2

    .line 152
    .line 153
    :cond_6
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 154
    .line 155
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8O0880(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 156
    .line 157
    .line 158
    move-result p1

    .line 159
    if-eqz p1, :cond_7

    .line 160
    .line 161
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 162
    .line 163
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 164
    .line 165
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 166
    .line 167
    invoke-interface {p1, v3, v0, v1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇〇888(ZLjava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    goto/16 :goto_2

    .line 171
    .line 172
    :cond_7
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 173
    .line 174
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_ACCOUNT_VERIFY:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 175
    .line 176
    if-ne p1, v0, :cond_8

    .line 177
    .line 178
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 179
    .line 180
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 181
    .line 182
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 183
    .line 184
    invoke-interface {p1, v3, v0, v1}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇8o8o〇(ZLjava/lang/String;Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    goto/16 :goto_2

    .line 188
    .line 189
    :cond_8
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->SETTING_SUPER_VERIFY_CODE:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 190
    .line 191
    if-ne p1, v0, :cond_9

    .line 192
    .line 193
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 194
    .line 195
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 196
    .line 197
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 198
    .line 199
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 200
    .line 201
    invoke-interface {p1, v0, v1, v2}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    .line 203
    .line 204
    const-string p1, "secondary_validation_open_resend"

    .line 205
    .line 206
    invoke-static {p1}, Lcom/intsig/tsapp/account/verify/SecondaryVerifyTrack;->〇080(Ljava/lang/String;)V

    .line 207
    .line 208
    .line 209
    goto/16 :goto_2

    .line 210
    .line 211
    :cond_9
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 212
    .line 213
    .line 214
    move-result p1

    .line 215
    if-eqz p1, :cond_c

    .line 216
    .line 217
    iget-object v4, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 218
    .line 219
    iget-object v5, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 220
    .line 221
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 222
    .line 223
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->BIND_EMAIL:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 224
    .line 225
    if-ne p1, v0, :cond_a

    .line 226
    .line 227
    const/4 v6, 0x1

    .line 228
    goto :goto_0

    .line 229
    :cond_a
    const/4 v6, 0x0

    .line 230
    :goto_0
    if-ne p1, v0, :cond_b

    .line 231
    .line 232
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 233
    .line 234
    goto :goto_1

    .line 235
    :cond_b
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 236
    .line 237
    :goto_1
    move-object v7, p1

    .line 238
    iget-object v8, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 239
    .line 240
    iget-object v9, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08〇o0O:Ljava/lang/String;

    .line 241
    .line 242
    invoke-interface/range {v4 .. v9}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->Oo08(Landroid/app/Activity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    goto/16 :goto_2

    .line 246
    .line 247
    :cond_c
    const-string p1, "UN DO"

    .line 248
    .line 249
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    .line 251
    .line 252
    goto/16 :goto_2

    .line 253
    .line 254
    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 255
    .line 256
    .line 257
    move-result v0

    .line 258
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_check_activate_mail:I

    .line 259
    .line 260
    if-ne v0, v1, :cond_e

    .line 261
    .line 262
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo80:Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;

    .line 263
    .line 264
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8oOOo:Ljava/lang/String;

    .line 265
    .line 266
    invoke-interface {p1, v0}, Lcom/intsig/tsapp/account/presenter/IVerifyCodePresenter;->oO80(Ljava/lang/String;)V

    .line 267
    .line 268
    .line 269
    goto/16 :goto_2

    .line 270
    .line 271
    :cond_e
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 272
    .line 273
    .line 274
    move-result v0

    .line 275
    sget v1, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_not_receive:I

    .line 276
    .line 277
    if-ne v0, v1, :cond_14

    .line 278
    .line 279
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 280
    .line 281
    invoke-static {p1, v2}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇8〇0〇o〇O(Landroid/app/Activity;Ljava/lang/String;)Z

    .line 282
    .line 283
    .line 284
    move-result p1

    .line 285
    if-eqz p1, :cond_f

    .line 286
    .line 287
    return-void

    .line 288
    :cond_f
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 289
    .line 290
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇〇0(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 291
    .line 292
    .line 293
    move-result p1

    .line 294
    const-string v0, "secondary_validation_login_unreceived"

    .line 295
    .line 296
    if-eqz p1, :cond_10

    .line 297
    .line 298
    const-string p1, "email"

    .line 299
    .line 300
    invoke-static {v0, p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇00O0O0(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    .line 302
    .line 303
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo0O0o8()V

    .line 304
    .line 305
    .line 306
    const-string v1, "email"

    .line 307
    .line 308
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 309
    .line 310
    const/4 v3, 0x0

    .line 311
    const/4 v4, 0x0

    .line 312
    iget-object v5, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 313
    .line 314
    const/4 v6, 0x1

    .line 315
    invoke-static/range {v1 .. v6}, Lcom/intsig/tsapp/account/fragment/VerifyCodeNoneReceiveFragment;->oooO888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/intsig/tsapp/account/fragment/VerifyCodeNoneReceiveFragment;

    .line 316
    .line 317
    .line 318
    move-result-object p1

    .line 319
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 320
    .line 321
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 322
    .line 323
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 324
    .line 325
    .line 326
    goto/16 :goto_2

    .line 327
    .line 328
    :cond_10
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 329
    .line 330
    invoke-static {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08O(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 331
    .line 332
    .line 333
    move-result p1

    .line 334
    if-eqz p1, :cond_11

    .line 335
    .line 336
    const-string p1, "mobile"

    .line 337
    .line 338
    invoke-static {v0, p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇00O0O0(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    .line 340
    .line 341
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo0O0o8()V

    .line 342
    .line 343
    .line 344
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 345
    .line 346
    .line 347
    move-result-object p1

    .line 348
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇o00〇〇Oo()V

    .line 349
    .line 350
    .line 351
    const-string v0, "mobile"

    .line 352
    .line 353
    const/4 v1, 0x0

    .line 354
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 355
    .line 356
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 357
    .line 358
    iget-object v4, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 359
    .line 360
    const/4 v5, 0x1

    .line 361
    invoke-static/range {v0 .. v5}, Lcom/intsig/tsapp/account/fragment/VerifyCodeNoneReceiveFragment;->oooO888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/intsig/tsapp/account/fragment/VerifyCodeNoneReceiveFragment;

    .line 362
    .line 363
    .line 364
    move-result-object p1

    .line 365
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 366
    .line 367
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 368
    .line 369
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 370
    .line 371
    .line 372
    goto :goto_2

    .line 373
    :cond_11
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 374
    .line 375
    invoke-direct {p0, p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0oO〇oo00(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 376
    .line 377
    .line 378
    move-result p1

    .line 379
    if-eqz p1, :cond_12

    .line 380
    .line 381
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo0O0o8()V

    .line 382
    .line 383
    .line 384
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 385
    .line 386
    .line 387
    move-result-object p1

    .line 388
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇o00〇〇Oo()V

    .line 389
    .line 390
    .line 391
    const-string v0, "mobile"

    .line 392
    .line 393
    const/4 v1, 0x0

    .line 394
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 395
    .line 396
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 397
    .line 398
    iget-object v4, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 399
    .line 400
    const/4 v5, 0x0

    .line 401
    invoke-static/range {v0 .. v5}, Lcom/intsig/tsapp/account/fragment/VerifyCodeNoneReceiveFragment;->oooO888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/intsig/tsapp/account/fragment/VerifyCodeNoneReceiveFragment;

    .line 402
    .line 403
    .line 404
    move-result-object p1

    .line 405
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 406
    .line 407
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 408
    .line 409
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 410
    .line 411
    .line 412
    goto :goto_2

    .line 413
    :cond_12
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 414
    .line 415
    invoke-direct {p0, p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O8〇8000(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 416
    .line 417
    .line 418
    move-result p1

    .line 419
    if-eqz p1, :cond_13

    .line 420
    .line 421
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo0O0o8()V

    .line 422
    .line 423
    .line 424
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 425
    .line 426
    .line 427
    move-result-object p1

    .line 428
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇o00〇〇Oo()V

    .line 429
    .line 430
    .line 431
    const-string v0, "mobile"

    .line 432
    .line 433
    const/4 v1, 0x0

    .line 434
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 435
    .line 436
    iget-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 437
    .line 438
    iget-object v4, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 439
    .line 440
    const/4 v5, 0x0

    .line 441
    invoke-static/range {v0 .. v5}, Lcom/intsig/tsapp/account/fragment/VerifyCodeNoneReceiveFragment;->oooO888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/intsig/tsapp/account/fragment/VerifyCodeNoneReceiveFragment;

    .line 442
    .line 443
    .line 444
    move-result-object p1

    .line 445
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 446
    .line 447
    check-cast v0, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 448
    .line 449
    invoke-virtual {v0, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 450
    .line 451
    .line 452
    goto :goto_2

    .line 453
    :cond_13
    const-string p1, "NOT RECEIVE>>> UN DO"

    .line 454
    .line 455
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    .line 457
    .line 458
    goto :goto_2

    .line 459
    :cond_14
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 460
    .line 461
    .line 462
    move-result p1

    .line 463
    sget v0, Lcom/intsig/camscanner/account/R$id;->tv_verify_code_contact_us:I

    .line 464
    .line 465
    if-ne p1, v0, :cond_15

    .line 466
    .line 467
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 468
    .line 469
    .line 470
    move-result-object p1

    .line 471
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇o00〇〇Oo()V

    .line 472
    .line 473
    .line 474
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 475
    .line 476
    invoke-static {p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->OO0o〇〇(Landroid/app/Activity;)V

    .line 477
    .line 478
    .line 479
    :cond_15
    :goto_2
    return-void
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public getIntentData(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->getIntentData(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const-string v0, "args_from_where"

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 13
    .line 14
    const-string v0, "args_phone_v_code"

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 21
    .line 22
    const-string v0, "args_area_code"

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 29
    .line 30
    const-string v0, "args_phone_number"

    .line 31
    .line 32
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 37
    .line 38
    const-string v0, "args_email"

    .line 39
    .line 40
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 45
    .line 46
    const-string v0, "args_pwd"

    .line 47
    .line 48
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O0O:Ljava/lang/String;

    .line 53
    .line 54
    const-string v0, "args_email_postal"

    .line 55
    .line 56
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8oOOo:Ljava/lang/String;

    .line 61
    .line 62
    const-string v0, "args_error_type_for_pwd_login"

    .line 63
    .line 64
    const/4 v1, -0x1

    .line 65
    invoke-virtual {p1, v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;I)I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    iput v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o0O:I

    .line 70
    .line 71
    const-string v0, "args_token"

    .line 72
    .line 73
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O88O:Ljava/lang/String;

    .line 78
    .line 79
    const-string v0, "args_sms_token"

    .line 80
    .line 81
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOO〇〇:Ljava/lang/String;

    .line 86
    .line 87
    const-string v0, "args_domain"

    .line 88
    .line 89
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    iput-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇08〇o0O:Ljava/lang/String;

    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->handleMessage(Landroid/os/Message;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    .line 12
    .line 13
    packed-switch v0, :pswitch_data_0

    .line 14
    .line 15
    .line 16
    goto/16 :goto_1

    .line 17
    .line 18
    :pswitch_0
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0〇〇00()V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇o08〇()V

    .line 22
    .line 23
    .line 24
    goto/16 :goto_1

    .line 25
    .line 26
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v1, "get verity code errorCode="

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const-string v1, "VerifyCodeFragment"

    .line 46
    .line 47
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 51
    .line 52
    instance-of v2, v0, Ljava/lang/String;

    .line 53
    .line 54
    if-eqz v2, :cond_1

    .line 55
    .line 56
    check-cast v0, Ljava/lang/String;

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    const/4 v0, 0x0

    .line 60
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    if-eqz v2, :cond_2

    .line 65
    .line 66
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 67
    .line 68
    invoke-direct {p0, p1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇80o(I)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    :cond_2
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0〇〇00()V

    .line 73
    .line 74
    .line 75
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    if-eqz p1, :cond_3

    .line 80
    .line 81
    const-string p1, "errorMsg is empty"

    .line 82
    .line 83
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_3
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇00O:Landroid/widget/TextView;

    .line 88
    .line 89
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    .line 91
    .line 92
    goto :goto_1

    .line 93
    :pswitch_2
    iget p1, p1, Landroid/os/Message;->arg1:I

    .line 94
    .line 95
    const/4 v0, 0x1

    .line 96
    if-gtz p1, :cond_4

    .line 97
    .line 98
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 99
    .line 100
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 101
    .line 102
    .line 103
    const p1, -0xe64364

    .line 104
    .line 105
    .line 106
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 107
    .line 108
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 112
    .line 113
    sget v0, Lcom/intsig/camscanner/account/R$string;->a_label_reget_verifycode:I

    .line 114
    .line 115
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 116
    .line 117
    .line 118
    goto :goto_1

    .line 119
    :cond_4
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 120
    .line 121
    sget v2, Lcom/intsig/camscanner/account/R$string;->cs_542_renew_33:I

    .line 122
    .line 123
    new-array v0, v0, [Ljava/lang/Object;

    .line 124
    .line 125
    new-instance v3, Ljava/lang/StringBuilder;

    .line 126
    .line 127
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    const-string p1, ""

    .line 134
    .line 135
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object p1

    .line 142
    const/4 v3, 0x0

    .line 143
    aput-object p1, v0, v3

    .line 144
    .line 145
    invoke-virtual {p0, v2, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object p1

    .line 149
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    .line 151
    .line 152
    :goto_1
    return-void

    .line 153
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Ooo8o()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇oO88o()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0ooOOo()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇OOoooo()V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/util/CountdownTimer;->o〇0()Lcom/intsig/util/CountdownTimer;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    iput-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oo8ooo8O:Lcom/intsig/util/CountdownTimer;

    .line 18
    .line 19
    const/16 v0, 0x3c

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/util/CountdownTimer;->〇80〇808〇O(I)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oo8ooo8O:Lcom/intsig/util/CountdownTimer;

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O〇08oOOO0:Lcom/intsig/util/CountdownTimer$OnCountdownListener;

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Lcom/intsig/util/CountdownTimer;->OO0o〇〇〇〇0(Lcom/intsig/util/CountdownTimer$OnCountdownListener;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇088O()V

    .line 32
    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO0O()V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇08oO80o()V

    .line 38
    .line 39
    .line 40
    const/4 p1, 0x4

    .line 41
    new-array p1, p1, [Landroid/view/View;

    .line 42
    .line 43
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇080OO8〇0:Landroid/widget/TextView;

    .line 45
    .line 46
    aput-object v1, p1, v0

    .line 47
    .line 48
    const/4 v0, 0x1

    .line 49
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 50
    .line 51
    aput-object v1, p1, v0

    .line 52
    .line 53
    const/4 v0, 0x2

    .line 54
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇0O:Landroid/widget/TextView;

    .line 55
    .line 56
    aput-object v1, p1, v0

    .line 57
    .line 58
    const/4 v0, 0x3

    .line 59
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 60
    .line 61
    aput-object v1, p1, v0

    .line 62
    .line 63
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 64
    .line 65
    .line 66
    new-instance p1, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    const-string v0, "initialize >>> mFromWhere = "

    .line 72
    .line 73
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 77
    .line 78
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    const-string v0, " mAreaCode = "

    .line 82
    .line 83
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 87
    .line 88
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v0, " mPhoneNumber = "

    .line 92
    .line 93
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 97
    .line 98
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    const-string v0, " mEmail = "

    .line 102
    .line 103
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇〇08O:Ljava/lang/String;

    .line 107
    .line 108
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v0, " mEmailPostal = "

    .line 112
    .line 113
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8oOOo:Ljava/lang/String;

    .line 117
    .line 118
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    const-string v0, " mErrorTypeForPwdLogin = "

    .line 122
    .line 123
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    iget v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o0O:I

    .line 127
    .line 128
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    const-string v0, "VerifyCodeFragment"

    .line 136
    .line 137
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    return-void
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public interceptBackPressed()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->〇o00〇〇Oo()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    new-instance v1, LoO8o〇o〇8/〇O〇80o08O;

    .line 11
    .line 12
    invoke-direct {v1, p0}, LoO8o〇o〇8/〇O〇80o08O;-><init>(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/tsapp/account/helper/LogOutHelper;->〇o00〇〇Oo(Landroid/app/Activity;Lkotlin/jvm/functions/Function0;)V

    .line 16
    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDestroyed()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 27
    :goto_1
    return v0
    .line 28
    .line 29
    .line 30
.end method

.method public o08〇〇0O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 2
    .line 3
    const/16 v1, 0x66

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public oO80(ILjava/lang/String;)V
    .locals 1

    .line 1
    new-instance p2, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-direct {p2, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    sget v0, Lcom/intsig/camscanner/account/R$string;->dlg_title:I

    .line 11
    .line 12
    invoke-virtual {p2, v0}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {p2, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 16
    .line 17
    .line 18
    sget p1, Lcom/intsig/camscanner/account/R$string;->a_btn_change_email:I

    .line 19
    .line 20
    new-instance v0, LoO8o〇o〇8/OO8oO0o〇;

    .line 21
    .line 22
    invoke-direct {v0, p0}, LoO8o〇o〇8/OO8oO0o〇;-><init>(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p2, p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 26
    .line 27
    .line 28
    sget p1, Lcom/intsig/camscanner/account/R$string;->login_btn:I

    .line 29
    .line 30
    new-instance v0, LoO8o〇o〇8/o0O0;

    .line 31
    .line 32
    invoke-direct {v0, p0}, LoO8o〇o〇8/o0O0;-><init>(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p2, p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 36
    .line 37
    .line 38
    :try_start_0
    invoke-virtual {p2}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :catch_0
    move-exception p1

    .line 47
    const-string p2, "VerifyCodeFragment"

    .line 48
    .line 49
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 50
    .line 51
    .line 52
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    const-string p1, "VerifyCodeFragment"

    .line 5
    .line 6
    const-string p2, "onActivityResult"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public onDestroy()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇00O0:[I

    .line 4
    .line 5
    const-string v2, "VerifyCodeFragment"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-static {v2, v0, v1, v3}, Lcom/intsig/comm/recycle/HandlerMsglerRecycle;->〇o〇(Ljava/lang/String;Landroid/os/Handler;[I[Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oo8ooo8O:Lcom/intsig/util/CountdownTimer;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/util/CountdownTimer;->Oo08()V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oo8ooo8O:Lcom/intsig/util/CountdownTimer;

    .line 19
    .line 20
    invoke-virtual {v0, v3}, Lcom/intsig/util/CountdownTimer;->OO0o〇〇〇〇0(Lcom/intsig/util/CountdownTimer$OnCountdownListener;)V

    .line 21
    .line 22
    .line 23
    iput-object v3, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oo8ooo8O:Lcom/intsig/util/CountdownTimer;

    .line 24
    .line 25
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->O80〇O〇080()V

    .line 26
    .line 27
    .line 28
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public onDestroyView()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇oO:Z

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o808o8o08()V

    .line 5
    .line 6
    .line 7
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public onResume()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_VERIFY_CODE_LOGIN:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 7
    .line 8
    if-eq v0, v1, :cond_3

    .line 9
    .line 10
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 11
    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->CN_EMAIL_REGISTER:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 16
    .line 17
    if-eq v0, v1, :cond_2

    .line 18
    .line 19
    sget-object v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;->PHONE_ACCOUNT_VERIFY:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 20
    .line 21
    if-eq v0, v1, :cond_2

    .line 22
    .line 23
    invoke-static {v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇o08(Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    sget v0, Lcom/intsig/camscanner/account/R$string;->cs_542_renew_29:I

    .line 31
    .line 32
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-direct {p0, v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇oO〇08o(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    goto :goto_2

    .line 40
    :cond_2
    :goto_0
    const-string v0, " "

    .line 41
    .line 42
    invoke-direct {p0, v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇oO〇08o(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    goto :goto_2

    .line 46
    :cond_3
    :goto_1
    sget v0, Lcom/intsig/camscanner/account/R$string;->a_title_verifycode_login:I

    .line 47
    .line 48
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-direct {p0, v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇oO〇08o(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    :goto_2
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public o〇O8OO()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Ooo08:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->〇OO8ooO8〇:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$OneLineKeyBoard;

    .line 11
    .line 12
    invoke-direct {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$OneLineKeyBoard;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Ooo08:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    new-instance v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$SixBoxKeyBoard;

    .line 19
    .line 20
    invoke-direct {v0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$SixBoxKeyBoard;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Ooo08:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 24
    .line 25
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Ooo08:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 26
    .line 27
    instance-of v0, v0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$OneLineKeyBoard;

    .line 28
    .line 29
    const/4 v1, 0x0

    .line 30
    const/16 v2, 0x8

    .line 31
    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO〇00〇8oO:Landroid/widget/EditText;

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOo0:Lcom/intsig/tsapp/account/widget/VerifyCodeView;

    .line 40
    .line 41
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Ooo08:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO〇00〇8oO:Landroid/widget/EditText;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->O8(Ljava/lang/Object;)V

    .line 49
    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_2
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->OO〇00〇8oO:Landroid/widget/EditText;

    .line 53
    .line 54
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOo0:Lcom/intsig/tsapp/account/widget/VerifyCodeView;

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 60
    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Ooo08:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->oOo0:Lcom/intsig/tsapp/account/widget/VerifyCodeView;

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;->O8(Ljava/lang/Object;)V

    .line 67
    .line 68
    .line 69
    :goto_1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Ooo08:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$AbsKeyBoardStrategy;

    .line 70
    .line 71
    return-object v0
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public o〇oO(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8〇OO:Lcom/intsig/app/ProgressDialogClient;

    .line 19
    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-static {v0, p1}, Lcom/intsig/app/ProgressDialogClient;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)Lcom/intsig/app/ProgressDialogClient;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iput-object p1, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8〇OO:Lcom/intsig/app/ProgressDialogClient;

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/app/ProgressDialogClient;->O8()V

    .line 38
    .line 39
    .line 40
    :cond_2
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/account/R$layout;->fragment_verify_code:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇080()Landroid/app/Activity;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇80(Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Landroid/view/View;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    iget-object v2, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇00O:Landroid/widget/TextView;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->checkTargetNonNull([Landroid/view/View;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    sget p1, Lcom/intsig/camscanner/account/R$string;->c_msg_error_validate_number:I

    .line 22
    .line 23
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o〇00O:Landroid/widget/TextView;

    .line 28
    .line 29
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o0〇〇00()V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
.end method

.method public 〇oOO8O8()Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->o8o:Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇o〇8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const-string v1, "VerifyCodeFragment"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇〇0o(Landroid/app/Activity;Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v2, Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;->PHONE_REGISTER:Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    const/4 v8, 0x0

    .line 15
    const/4 v9, 0x0

    .line 16
    move-object v4, p1

    .line 17
    move-object v5, p2

    .line 18
    move-object v6, p3

    .line 19
    move-object v7, p4

    .line 20
    invoke-static/range {v2 .. v9}, Lcom/intsig/tsapp/account/fragment/SettingPwdFragment;->Ooo8o(Lcom/intsig/tsapp/account/fragment/SettingPwdFragment$FromWhere;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tsapp/account/fragment/SettingPwdFragment;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    if-eqz p1, :cond_1

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/fragment/VerifyCodeFragment;->Oo0O0o8()V

    .line 27
    .line 28
    .line 29
    iget-object p2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 30
    .line 31
    check-cast p2, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 32
    .line 33
    invoke-virtual {p2, p1}, Lcom/intsig/tsapp/account/LoginMainActivity;->〇〇0〇0o8(Landroidx/fragment/app/Fragment;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const-string p1, "go2SetPwd >>> NOTHING TO DO"

    .line 38
    .line 39
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    :cond_1
    :goto_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

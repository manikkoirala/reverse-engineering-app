.class public interface abstract Lcom/intsig/tsapp/account/login_task/BaseLoginTaskListener;
.super Ljava/lang/Object;
.source "BaseLoginTaskListener.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract isSafeVerifyConsumed(I)Z
.end method

.method public abstract onLoginFinish()V
.end method

.method public abstract operationLogin()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/tianshu/exception/TianShuException;
        }
    .end annotation
.end method

.method public abstract showErrorDialog(Ljava/lang/String;ILjava/lang/String;)V
.end method

.class public Lcom/intsig/tsapp/account/widget/VerifyCodeView;
.super Landroid/widget/RelativeLayout;
.source "VerifyCodeView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/widget/VerifyCodeView$InputCompleteListener;
    }
.end annotation


# static fields
.field private static o〇00O:I = 0x6


# instance fields
.field private OO:Ljava/lang/String;

.field private o0:Landroid/widget/EditText;

.field private 〇08O〇00〇o:Lcom/intsig/tsapp/account/widget/VerifyCodeView$InputCompleteListener;

.field private 〇OOo8〇0:[Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/tsapp/account/widget/VerifyCodeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 3
    sget p2, Lcom/intsig/camscanner/account/R$layout;->view_verify_code:I

    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 4
    sget p1, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->o〇00O:I

    new-array p1, p1, [Landroid/widget/TextView;

    iput-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇OOo8〇0:[Landroid/widget/TextView;

    .line 5
    sget p2, Lcom/intsig/camscanner/account/R$id;->tv_0:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const/4 p3, 0x0

    aput-object p2, p1, p3

    .line 6
    iget-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇OOo8〇0:[Landroid/widget/TextView;

    sget p2, Lcom/intsig/camscanner/account/R$id;->tv_1:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const/4 v0, 0x1

    aput-object p2, p1, v0

    .line 7
    iget-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇OOo8〇0:[Landroid/widget/TextView;

    sget p2, Lcom/intsig/camscanner/account/R$id;->tv_2:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const/4 v0, 0x2

    aput-object p2, p1, v0

    .line 8
    iget-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇OOo8〇0:[Landroid/widget/TextView;

    sget p2, Lcom/intsig/camscanner/account/R$id;->tv_3:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const/4 v0, 0x3

    aput-object p2, p1, v0

    .line 9
    iget-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇OOo8〇0:[Landroid/widget/TextView;

    sget p2, Lcom/intsig/camscanner/account/R$id;->tv_4:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const/4 v0, 0x4

    aput-object p2, p1, v0

    .line 10
    iget-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇OOo8〇0:[Landroid/widget/TextView;

    sget p2, Lcom/intsig/camscanner/account/R$id;->tv_5:I

    invoke-virtual {p0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    const/4 v0, 0x5

    aput-object p2, p1, v0

    .line 11
    sget p1, Lcom/intsig/camscanner/account/R$id;->edit_text_view:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->o0:Landroid/widget/EditText;

    .line 12
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 13
    invoke-direct {p0}, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇80〇808〇O()V

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/tsapp/account/widget/VerifyCodeView;)[Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇OOo8〇0:[Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic Oo08(Lcom/intsig/tsapp/account/widget/VerifyCodeView;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->OO:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic o〇0()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static bridge synthetic 〇080(Lcom/intsig/tsapp/account/widget/VerifyCodeView;)Landroid/widget/EditText;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->o0:Landroid/widget/EditText;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private 〇80〇808〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->o0:Landroid/widget/EditText;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/tsapp/account/widget/VerifyCodeView$1;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/tsapp/account/widget/VerifyCodeView$1;-><init>(Lcom/intsig/tsapp/account/widget/VerifyCodeView;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/tsapp/account/widget/VerifyCodeView;)Lcom/intsig/tsapp/account/widget/VerifyCodeView$InputCompleteListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇08O〇00〇o:Lcom/intsig/tsapp/account/widget/VerifyCodeView$InputCompleteListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/tsapp/account/widget/VerifyCodeView;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->OO:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public OO0o〇〇〇〇0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->o0:Landroid/widget/EditText;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0}, Lcom/intsig/utils/KeyboardUtils;->〇8o8o〇(Landroid/view/View;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEditContent()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->OO:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public oO80()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->o0:Landroid/widget/EditText;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0}, Lcom/intsig/utils/KeyboardUtils;->〇〇888(Landroid/view/View;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setInputCompleteListener(Lcom/intsig/tsapp/account/widget/VerifyCodeView$InputCompleteListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇08O〇00〇o:Lcom/intsig/tsapp/account/widget/VerifyCodeView$InputCompleteListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public 〇〇888()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->o0:Landroid/widget/EditText;

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/VerifyCodeView;->〇OOo8〇0:[Landroid/widget/TextView;

    .line 9
    .line 10
    array-length v2, v0

    .line 11
    const/4 v3, 0x0

    .line 12
    :goto_0
    if-ge v3, v2, :cond_0

    .line 13
    .line 14
    aget-object v4, v0, v3

    .line 15
    .line 16
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 17
    .line 18
    .line 19
    add-int/lit8 v3, v3, 0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

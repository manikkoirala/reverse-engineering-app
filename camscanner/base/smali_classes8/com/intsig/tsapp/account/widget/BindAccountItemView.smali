.class public final Lcom/intsig/tsapp/account/widget/BindAccountItemView;
.super Landroid/widget/RelativeLayout;
.source "BindAccountItemView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private o0:Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

.field private 〇OOo8〇0:Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/tsapp/account/widget/BindAccountItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget p2, Lcom/intsig/camscanner/account/R$layout;->item_view_bind_account:I

    const/4 p3, 0x1

    invoke-virtual {p1, p2, p0, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 5
    invoke-static {p0}, Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

    move-result-object p1

    const-string p2, "bind(this)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->o0:Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/tsapp/account/widget/BindAccountItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static synthetic 〇080(Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->〇o〇(Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇o〇(Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "$params"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;->〇080()Lkotlin/jvm/functions/Function0;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    if-eqz p0, :cond_0

    .line 11
    .line 12
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public final getParams()Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->〇OOo8〇0:Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final 〇o00〇〇Oo(Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;)V
    .locals 5
    .param p1    # Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "params"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->〇OOo8〇0:Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->o0:Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

    .line 9
    .line 10
    const-string v1, "mBinding"

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    move-object v0, v2

    .line 19
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;->O8()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;->〇o〇()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const/4 v3, 0x0

    .line 33
    if-eqz v0, :cond_3

    .line 34
    .line 35
    iget-object v4, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->o0:Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

    .line 36
    .line 37
    if-nez v4, :cond_1

    .line 38
    .line 39
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    move-object v4, v2

    .line 43
    :cond_1
    iget-object v4, v4, Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;->OO:Landroid/widget/TextView;

    .line 44
    .line 45
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 46
    .line 47
    .line 48
    iget-object v4, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->o0:Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

    .line 49
    .line 50
    if-nez v4, :cond_2

    .line 51
    .line 52
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    move-object v4, v2

    .line 56
    :cond_2
    iget-object v4, v4, Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;->OO:Landroid/widget/TextView;

    .line 57
    .line 58
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    .line 60
    .line 61
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    if-eqz v0, :cond_6

    .line 66
    .line 67
    iget-object v4, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->o0:Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

    .line 68
    .line 69
    if-nez v4, :cond_4

    .line 70
    .line 71
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    move-object v4, v2

    .line 75
    :cond_4
    iget-object v4, v4, Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 76
    .line 77
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 78
    .line 79
    .line 80
    iget-object v4, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->o0:Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

    .line 81
    .line 82
    if-nez v4, :cond_5

    .line 83
    .line 84
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    move-object v4, v2

    .line 88
    :cond_5
    iget-object v4, v4, Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 89
    .line 90
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    .line 92
    .line 93
    :cond_6
    iget-object v0, p0, Lcom/intsig/tsapp/account/widget/BindAccountItemView;->o0:Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;

    .line 94
    .line 95
    if-nez v0, :cond_7

    .line 96
    .line 97
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    move-object v0, v2

    .line 101
    :cond_7
    iget-object v0, v0, Lcom/intsig/camscanner/account/databinding/ItemViewBindAccountBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 102
    .line 103
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v1

    .line 107
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 108
    .line 109
    .line 110
    move-result v1

    .line 111
    xor-int/lit8 v1, v1, 0x1

    .line 112
    .line 113
    if-eqz v1, :cond_8

    .line 114
    .line 115
    move-object v2, v0

    .line 116
    :cond_8
    if-eqz v2, :cond_9

    .line 117
    .line 118
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1}, Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    .line 127
    .line 128
    new-instance v0, Lo008/〇080;

    .line 129
    .line 130
    invoke-direct {v0, p1}, Lo008/〇080;-><init>(Lcom/intsig/tsapp/account/widget/BindAccountItemView$BindAccountItemParams;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    .line 135
    .line 136
    :cond_9
    return-void
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

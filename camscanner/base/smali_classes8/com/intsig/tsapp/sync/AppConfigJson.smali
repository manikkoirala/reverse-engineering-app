.class public Lcom/intsig/tsapp/sync/AppConfigJson;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/tsapp/sync/AppConfigJson$WordEdit;,
        Lcom/intsig/tsapp/sync/AppConfigJson$AppListCollect;,
        Lcom/intsig/tsapp/sync/AppConfigJson$DocEdit;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ExperienceOptimization;,
        Lcom/intsig/tsapp/sync/AppConfigJson$GoogleGdpr;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CountModeV2Collect;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ExcelOptimize;,
        Lcom/intsig/tsapp/sync/AppConfigJson$DefaultPdfConfig;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CaptureGuideResource;,
        Lcom/intsig/tsapp/sync/AppConfigJson$AdFeedBack;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CountMode;,
        Lcom/intsig/tsapp/sync/AppConfigJson$PopupShowUpdate;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CamExamGuideDocList;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CamExamGuideFun;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CamExamGuideConfig;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CloudDiskFit;,
        Lcom/intsig/tsapp/sync/AppConfigJson$GuideReduceV1;,
        Lcom/intsig/tsapp/sync/AppConfigJson$UserBindOpt;,
        Lcom/intsig/tsapp/sync/AppConfigJson$WatermarkConfig;,
        Lcom/intsig/tsapp/sync/AppConfigJson$PadFrameItem;,
        Lcom/intsig/tsapp/sync/AppConfigJson$PadShareFrame;,
        Lcom/intsig/tsapp/sync/AppConfigJson$DisPlayItem;,
        Lcom/intsig/tsapp/sync/AppConfigJson$DisPlayEffect;,
        Lcom/intsig/tsapp/sync/AppConfigJson$WritePad;,
        Lcom/intsig/tsapp/sync/AppConfigJson$WhiteBoardConfig;,
        Lcom/intsig/tsapp/sync/AppConfigJson$StreamFreeCfg;,
        Lcom/intsig/tsapp/sync/AppConfigJson$EleSignV6230;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CreateDirContent;,
        Lcom/intsig/tsapp/sync/AppConfigJson$EnterpriseMall;,
        Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;,
        Lcom/intsig/tsapp/sync/AppConfigJson$BarcodeScan;,
        Lcom/intsig/tsapp/sync/AppConfigJson$InnovationLab;,
        Lcom/intsig/tsapp/sync/AppConfigJson$FunctionCfg;,
        Lcom/intsig/tsapp/sync/AppConfigJson$RewardFunctionCfg;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ShareDirV2;,
        Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTry;,
        Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;,
        Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ScenarioDirEnTry;,
        Lcom/intsig/tsapp/sync/AppConfigJson$NpsInfo;,
        Lcom/intsig/tsapp/sync/AppConfigJson$NpsMultiChoiceStruct;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ShareDonePopup;,
        Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ScanToolEntity;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ScanToolsSheetEntity;,
        Lcom/intsig/tsapp/sync/AppConfigJson$LocalBalance;,
        Lcom/intsig/tsapp/sync/AppConfigJson$BookModelInfo;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CertificateIdPhoto;,
        Lcom/intsig/tsapp/sync/AppConfigJson$VipPopup;,
        Lcom/intsig/tsapp/sync/AppConfigJson$LeftAds;,
        Lcom/intsig/tsapp/sync/AppConfigJson$WaterMark;,
        Lcom/intsig/tsapp/sync/AppConfigJson$AppUrl;,
        Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;,
        Lcom/intsig/tsapp/sync/AppConfigJson$OcrTemplate;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ShareDoc;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ChristmasDay;,
        Lcom/intsig/tsapp/sync/AppConfigJson$WX;,
        Lcom/intsig/tsapp/sync/AppConfigJson$WxApp;,
        Lcom/intsig/tsapp/sync/AppConfigJson$DirConfig;,
        Lcom/intsig/tsapp/sync/AppConfigJson$UserRateInfo;,
        Lcom/intsig/tsapp/sync/AppConfigJson$Signature;,
        Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;,
        Lcom/intsig/tsapp/sync/AppConfigJson$PrivateFolderConfig;,
        Lcom/intsig/tsapp/sync/AppConfigJson$DirActivity;,
        Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;,
        Lcom/intsig/tsapp/sync/AppConfigJson$EraseIntellect;,
        Lcom/intsig/tsapp/sync/AppConfigJson$DocumentClassifyAndroid;,
        Lcom/intsig/tsapp/sync/AppConfigJson$AdVideo;
    }
.end annotation


# static fields
.field private static final ONE_SECOND:I = 0x3e8

.field public static final OPEN_ANDROID_REVIEW:I = 0x1

.field private static SHOW_WORD:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AppConfigJson"

.field private static s_android_camera_revision:I = -0x1

.field public static s_scan_process:I = -0x1


# instance fields
.field public active_function_642_android:I

.field public ad_applist_collecting:Lcom/intsig/tsapp/sync/AppConfigJson$AppListCollect;

.field public ad_feedback:Lcom/intsig/tsapp/sync/AppConfigJson$AdFeedBack;

.field public ad_track_cs:I

.field public ad_track_other:I

.field public ad_video:Lcom/intsig/tsapp/sync/AppConfigJson$AdVideo;

.field public add_panel_android:I

.field public ai_filter_compress_percent:I

.field public ai_filter_default:I

.field public ai_filter_os_v2:I

.field public ai_filter_v2:I

.field public ai_image_filter:I

.field public ai_image_filter_loading:I

.field public ai_image_filter_trace:I

.field public ai_image_filter_v3_5:I

.field public alg_engine_cpu:I

.field public alg_orgin_image_compare:I

.field public and_sign_magnify:I

.field public android_camera_revision:I

.field public android_clear_and_correct:I

.field public android_forbid_ncnn:I

.field public android_multi_process:I

.field public android_multi_process_no_retry:I

.field public android_review:I

.field public app_mall_business:Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;

.field public app_url:Lcom/intsig/tsapp/sync/AppConfigJson$AppUrl;

.field public appsflyer:I

.field public appsflyer_set:I

.field public auto_roam_property:Lcom/intsig/tsapp/sync/AutoRoamProperty;

.field public auto_roam_third:I

.field public auto_sync:I

.field public auto_sync_inform:I

.field public badcase_upload_show:I

.field public bankbills_mobile_v1:I

.field public barcode_scan:Lcom/intsig/tsapp/sync/AppConfigJson$BarcodeScan;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public barcode_scan_test:I

.field public batch_handle_image:I

.field public batch_ocr_pages:I

.field public book_mode_info:Lcom/intsig/tsapp/sync/AppConfigJson$BookModelInfo;

.field public bugly_ratio:Ljava/lang/String;

.field public camera_guide_optimization:I

.field public camera_guide_optimization_source:Lcom/intsig/tsapp/sync/AppConfigJson$CaptureGuideResource;

.field public camera_layout_enter_adjust:I

.field public can_use_camera2:I

.field public can_use_camerax:I

.field public card_mode_style:I

.field public card_mode_watermark:I

.field public card_photo:Lcom/intsig/tsapp/sync/AppConfigJson$CertificateIdPhoto;

.field public certificate_camera_frame:I

.field public certificate_new_list:I

.field public check_space_share:I

.field public christmas_activity:I

.field public christmas_delay:Ljava/lang/String;

.field public clarity_all:I

.field public clarity_wechat:I

.field public client_import_style:I

.field public close_ad_crash_catch:I

.field public cloud_disk_fit:Lcom/intsig/tsapp/sync/AppConfigJson$CloudDiskFit;

.field public cn_sync_auth_popup:I

.field public comment_popup:I

.field public comment_popup_cn:I

.field public completion_page:I

.field public compress_image:I

.field public copy_self_share:I

.field public count_mode:Lcom/intsig/tsapp/sync/AppConfigJson$CountMode;

.field public count_mode_v2_collect:Lcom/intsig/tsapp/sync/AppConfigJson$CountModeV2Collect;

.field public crash_fix_6410:I

.field public create_dir_cate:Lcom/intsig/tsapp/sync/AppConfigJson$CreateDirContent;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public cs_gp_product_android:I

.field public cs_oversea_first_pay:I

.field public cscrop_scan_again:I

.field public cstools_camexam:I

.field public customize_dir_entry:Lcom/intsig/tsapp/sync/AppConfigJson$ScenarioDirEnTry;

.field public data_dn:Ljava/lang/String;

.field public default_pdf_reader:Lcom/intsig/tsapp/sync/AppConfigJson$DefaultPdfConfig;

.field public demoire_count:I

.field public destroy_main_page:I

.field public detect_direction_batch:I

.field public detect_image_upload:I

.field public detect_moire:I

.field public dewarp_post_engine:I

.field public dir:Lcom/intsig/tsapp/sync/AppConfigJson$DirConfig;

.field public disk_use_record:I

.field public doc_classify_local_ocr:I

.field public doc_edit:Lcom/intsig/tsapp/sync/AppConfigJson$DocEdit;

.field public doc_format_selection:I

.field public doc_intellect_discern:I

.field public doc_qrcode_share:I

.field public doc_restore_export:I

.field public doc_tag_recommend:Lcom/intsig/tianshu/base/BaseJsonObj;

.field public doc_title_style:I

.field public doclist_camexam:Lcom/intsig/tsapp/sync/AppConfigJson$CamExamGuideDocList;

.field public doclist_vip_guide:I

.field public document_classify_android:Lcom/intsig/tsapp/sync/AppConfigJson$DocumentClassifyAndroid;

.field public edu_ad_invite_me:I

.field public edu_community:I

.field public edu_verify:I

.field public ele_sign_v6230:Lcom/intsig/tsapp/sync/AppConfigJson$EleSignV6230;

.field public elec_evidence:I

.field public email_sign_msg:Ljava/lang/String;

.field public enable_change_workbench_orientation:I

.field public enable_detect_screen_shot:I

.field public enable_gallery_cache:I

.field public enable_import_doc_opt:I

.field public enable_import_pic_opt:I

.field public enable_invoice_mode:I

.field public enable_make_backup:I

.field public engine_crash_analyze:I

.field public engine_superfilter_dewarp:I

.field public enhance_image_upload:I

.field public enhance_with_sharpen:I

.field public entrance_guide_2word:I

.field public erase_intellect:Lcom/intsig/tsapp/sync/AppConfigJson$EraseIntellect;

.field public esign_priority_queue:I

.field public esign_v1:I

.field public experience_optimization:Lcom/intsig/tsapp/sync/AppConfigJson$ExperienceOptimization;

.field public export_excel_optimize:Lcom/intsig/tsapp/sync/AppConfigJson$ExcelOptimize;

.field public exposure_ocr_doc_num:I

.field public exposure_ocr_second:I

.field public external_import_optimization:Lcom/intsig/tsapp/sync/configbean/ExternalImportOptimization;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public filename_proposal_auto:I

.field public free_create_dir:I

.field public free_high_definition:I

.field public fs_image:I

.field public fs_image_cache_optimize:I

.field public fs_image_page:I

.field public func_lab_v1:Lcom/intsig/tsapp/sync/AppConfigJson$InnovationLab;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public google_gdpr:Lcom/intsig/tsapp/sync/AppConfigJson$GoogleGdpr;

.field public google_login_retry_times:I

.field public gray_mind_map:I

.field public handwrite_optimize_fonts_v6510:I

.field public handwrite_optimize_v6510:I

.field public highpic_card_photo:I

.field public home_login_guide_popup:I

.field public id_mod_enhance_image_upload:I

.field public id_mod_pagescan_image_upload:I

.field public id_mode_engine:I

.field public image2excel_batch_import:I

.field public image2json_color:I

.field public image_compress_max_side:I

.field public image_demoire_deblur_upload:I

.field public image_discern_tag:Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;

.field public image_quality_restore:I

.field public image_quality_restore_count:I

.field public image_recognition_optimize:I

.field public image_silver_bullet_collect:I

.field public indonesia_pdf_watermark_style:I

.field public intelligent_rename:I

.field public ip_country:Ljava/lang/String;

.field public keep_sign_authenticity:I

.field public layout_restoration_view_cs:I

.field public lifetime_purchase_style:I

.field public list_detail_page_change:I

.field public listmore_camexam:Lcom/intsig/tsapp/sync/AppConfigJson$CamExamGuideConfig;

.field public loading_optimize:I

.field public loading_ui_optimize:I

.field public local_balance:Lcom/intsig/tsapp/sync/AppConfigJson$LocalBalance;

.field public local_db_encrypt:I

.field public local_ocr:I

.field public main_tools_ele_sign:I

.field public me_page_login_entrance_opt:I

.field public me_view_enter_switch:I

.field public multi_capture_handle_image:I

.field public multifile_priority_queue:I

.field public netty_machine_id_mac:I

.field public new_camera:I

.field public new_translation_process:I

.field public non_doc_clist_style:I

.field public nps_plus_info:Lcom/intsig/tsapp/sync/AppConfigJson$NpsInfo;

.field public nps_plus_popup:Ljava/lang/String;

.field public nps_popup_style:I

.field public ocr_free_paragraph:I

.field public ocr_inverse:I

.field public office2pdf_cloud:I

.field public open_post_storage_permission_req:I

.field public open_sharelink_ads:I

.field public operation_lottie_source:Lcom/intsig/tsapp/sync/OperationLottieSource;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public optimize_share_request:I

.field public page_spanning_seal:I

.field public pagedetail_web_login:I

.field public pagelist_show_word:I

.field public paint_test:I

.field public pay_incentive_config:Lcom/intsig/tsapp/sync/AppConfigJson$RewardFunctionCfg;

.field public payment_optimize:I

.field public pdf2img_ratio:I

.field public pdf2word_count:I

.field public pdf2word_process:I

.field public pdf_app:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTry;

.field public pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

.field public pdf_hyperlink_watermark:I

.field public pdf_hyperlink_watermark_str:I

.field public pdf_import_guide:I

.field public pdf_select_page:I

.field public pdf_share_type_switch:I

.field public pdf_strengthen:I

.field public pdf_tool_edit:I

.field public person_dir:Lcom/intsig/tsapp/sync/AppConfigJson$PrivateFolderConfig;

.field public photo_after_animation:I

.field public popup_show_update:Lcom/intsig/tsapp/sync/AppConfigJson$PopupShowUpdate;

.field public printer_buy_entry:Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;

.field public printer_service:I

.field public priority_use_camera_api:I

.field public purchase_fail_tips:I

.field public purchase_reconfirm:I

.field public purchase_reconfirm_interval:I

.field public qr_bar_google_engine:I

.field public query_local_doc_opt:I

.field public recent_file_funcs:I

.field public recycle_log_ratio:Ljava/lang/String;

.field public redpoint_flag:I

.field public reg_flow_style:I

.field public report_network_speed:I

.field public rest_email_bind_mobile:I

.field public right_top_point_enter_switch:I

.field public roadmap_export_mode:I

.field public safety_statement_show:I

.field public scan_done_vip_task_flag:I

.field public scan_down_process:I

.field public scan_new_guide_show:I

.field public scan_process:I

.field public scan_tools:I

.field public scan_tools_sheet:Lcom/intsig/tsapp/sync/AppConfigJson$ScanToolsSheetEntity;

.field public scandone_camexam:Lcom/intsig/tsapp/sync/AppConfigJson$CamExamGuideConfig;

.field public scandone_view_switch:I

.field public search_recmd_close_key:Ljava/lang/String;

.field public search_recmd_key:Ljava/lang/String;

.field public server_guide_new_user:I

.field public service_time:J

.field public share_channel_optimize:I

.field public share_dir:I

.field public share_dir_v2:Lcom/intsig/tsapp/sync/AppConfigJson$ShareDirV2;

.field public share_done_popup:Lcom/intsig/tsapp/sync/AppConfigJson$ShareDonePopup;

.field public share_limit_optimize:I

.field public share_link_loading_time:I

.field public share_link_style:I

.field public share_order:Ljava/lang/String;

.field public share_page_style:I

.field public share_pdf:Lcom/intsig/tsapp/sync/SharePdf;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public share_pdf_watermark:I

.field public share_preview_style:I

.field public shared_link_optimize:I

.field public shoot_camexam:I

.field public shot_one_animation:I

.field public show_backup:I

.field public show_buy_fax:I

.field public show_camexam:Ljava/lang/String;

.field public show_check_login_protocol:I

.field public show_country:I

.field public show_data_transfer_entrance:I

.field public show_float_windows:I

.field public show_gpt:I

.field public show_gpt_2:I

.field public show_local_ocr_layout:I

.field public show_login_dialog_for_compliance:I

.field public show_login_guide_bubble:I

.field public show_login_ways_page:I

.field public show_msword:I

.field public show_multi_import:I

.field public show_send_fax:I

.field public show_subscribe_manage:I

.field public show_tag_search:I

.field public show_wxmp_import:I

.field public sign:Lcom/intsig/tsapp/sync/AppConfigJson$Signature;

.field public sign_count:I

.field public sign_seal:I

.field public silent_ocr:I

.field public single_multi_enhance_upload:I

.field public single_multi_feature_recommend:I

.field public single_multi_fixed_thumb:I

.field public single_multi_shot_filter:I

.field public single_multi_shot_worktable:I

.field public single_purchase:I

.field public single_raw_enhance_timeout:J

.field public single_scan_qrcode_skip:I

.field public single_thumb_enhance_timeout:J

.field public skip_login:I

.field public slice_draw:I

.field public smart_erase_capture_switch:I

.field public soft_keyboard_style:I

.field public special_share_style:I

.field public split_new_pdf_engine:I

.field public super_filter_upload:I

.field public support_applink:I

.field public surface_correction:I

.field public surface_image_upload:I

.field public test_paper_photo_count:I

.field public test_paper_show:I

.field public toggle_large_view_mode:I

.field public trim_image_upload:I

.field public trim_image_upload_android:I

.field public umeng_apm:I

.field public unlogin_share_jpg_num:I

.field public update_privacy_show:J

.field public upload_slice_size:J

.field public upload_time:J

.field public us_share_watermark_free:I

.field public user_bind_opt:Lcom/intsig/tsapp/sync/AppConfigJson$UserBindOpt;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public user_rate:Lcom/intsig/tsapp/sync/AppConfigJson$UserRateInfo;

.field public user_source_collect:I

.field public watermark:Lcom/intsig/tsapp/sync/AppConfigJson$WaterMark;

.field public watermark_style_lang:I

.field public watermark_style_update:Lcom/intsig/tsapp/sync/configbean/WatermarkStyleUpdate;

.field public web_auto_convert:I

.field public whatsapp_share_show:I

.field public white_board_config:Lcom/intsig/tsapp/sync/AppConfigJson$WhiteBoardConfig;

.field public word_edit:Lcom/intsig/tsapp/sync/AppConfigJson$WordEdit;

.field public write_pad_config:Lcom/intsig/tsapp/sync/AppConfigJson$WritePad;

.field public wx:Lcom/intsig/tsapp/sync/AppConfigJson$WX;

.field public wxapp:Lcom/intsig/tsapp/sync/AppConfigJson$WxApp;

.field public wxmp_import_style:I

.field public wxmp_thumbnail_style:I

.field public xiaomi_camera:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->elec_evidence:I

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->doc_title_style:I

    .line 9
    .line 10
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->whatsapp_share_show:I

    .line 11
    .line 12
    const/16 v2, 0x14

    .line 13
    .line 14
    iput v2, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf2word_count:I

    .line 15
    .line 16
    const/4 v2, 0x3

    .line 17
    iput v2, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->purchase_reconfirm_interval:I

    .line 18
    .line 19
    const/16 v2, 0x1e

    .line 20
    .line 21
    iput v2, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->batch_ocr_pages:I

    .line 22
    .line 23
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->paint_test:I

    .line 24
    .line 25
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->redpoint_flag:I

    .line 26
    .line 27
    const/4 v3, -0x1

    .line 28
    iput v3, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->umeng_apm:I

    .line 29
    .line 30
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->netty_machine_id_mac:I

    .line 31
    .line 32
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->scan_done_vip_task_flag:I

    .line 33
    .line 34
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->doc_format_selection:I

    .line 35
    .line 36
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->doc_restore_export:I

    .line 37
    .line 38
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->show_gpt:I

    .line 39
    .line 40
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->loading_ui_optimize:I

    .line 41
    .line 42
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_channel_optimize:I

    .line 43
    .line 44
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_page_style:I

    .line 45
    .line 46
    iput v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->alg_engine_cpu:I

    .line 47
    .line 48
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->crash_fix_6410:I

    .line 49
    .line 50
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->free_high_definition:I

    .line 51
    .line 52
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->free_create_dir:I

    .line 53
    .line 54
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->camera_layout_enter_adjust:I

    .line 55
    .line 56
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->active_function_642_android:I

    .line 57
    .line 58
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->cs_gp_product_android:I

    .line 59
    .line 60
    iput v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->open_post_storage_permission_req:I

    .line 61
    .line 62
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->barcode_scan_test:I

    .line 63
    .line 64
    iput v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->engine_superfilter_dewarp:I

    .line 65
    .line 66
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->auto_roam_third:I

    .line 67
    .line 68
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->single_multi_fixed_thumb:I

    .line 69
    .line 70
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->camera_guide_optimization:I

    .line 71
    .line 72
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->web_auto_convert:I

    .line 73
    .line 74
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->layout_restoration_view_cs:I

    .line 75
    .line 76
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->toggle_large_view_mode:I

    .line 77
    .line 78
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->esign_priority_queue:I

    .line 79
    .line 80
    iput v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->enable_change_workbench_orientation:I

    .line 81
    .line 82
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->ai_image_filter_loading:I

    .line 83
    .line 84
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->destroy_main_page:I

    .line 85
    .line 86
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->recent_file_funcs:I

    .line 87
    .line 88
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->super_filter_upload:I

    .line 89
    .line 90
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->certificate_camera_frame:I

    .line 91
    .line 92
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->certificate_new_list:I

    .line 93
    .line 94
    iput v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->enable_make_backup:I

    .line 95
    .line 96
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->fs_image_cache_optimize:I

    .line 97
    .line 98
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->disk_use_record:I

    .line 99
    .line 100
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->check_space_share:I

    .line 101
    .line 102
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->support_applink:I

    .line 103
    .line 104
    iput v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->optimize_share_request:I

    .line 105
    .line 106
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->split_new_pdf_engine:I

    .line 107
    .line 108
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->ai_image_filter_v3_5:I

    .line 109
    .line 110
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->open_sharelink_ads:I

    .line 111
    .line 112
    iput v2, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->unlogin_share_jpg_num:I

    .line 113
    .line 114
    const/4 v0, 0x5

    .line 115
    iput v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_link_loading_time:I

    .line 116
    .line 117
    iput v1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_share_type_switch:I

    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private isImageDiscernMainSwitchOn()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->image_discern_tag:Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;->tag_optimization:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v1, 0x0

    .line 12
    :goto_0
    return v1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public enableDeMoire()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->detect_moire:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public enableImageRestore()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->image_quality_restore:I

    .line 2
    .line 3
    if-lez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public enablePriorityUpload()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->esign_priority_queue:I

    .line 10
    .line 11
    if-ne v0, v1, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/4 v1, 0x0

    .line 15
    :goto_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public forbidNcnnLibForBookScene()Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->android_forbid_ncnn:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_1

    .line 5
    .line 6
    const/4 v2, 0x2

    .line 7
    if-ne v0, v2, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v1, 0x0

    .line 11
    :cond_1
    :goto_0
    return v1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public forbidNcnnLibForICEngine()Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->android_forbid_ncnn:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_1

    .line 5
    .line 6
    const/4 v2, 0x3

    .line 7
    if-ne v0, v2, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v1, 0x0

    .line 11
    :cond_1
    :goto_0
    return v1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getAiPurchaseStrRes()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->show_gpt:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    sget v0, Lcom/intsig/comm/R$string;->cs_644_ai_01:I

    .line 7
    .line 8
    return v0

    .line 9
    :cond_0
    sget v0, Lcom/intsig/comm/R$string;->cs_542_renew_197:I

    .line 10
    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getAndroidCameraRevision()I
    .locals 2

    .line 1
    sget v0, Lcom/intsig/tsapp/sync/AppConfigJson;->s_android_camera_revision:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->android_camera_revision:I

    .line 7
    .line 8
    sput v0, Lcom/intsig/tsapp/sync/AppConfigJson;->s_android_camera_revision:I

    .line 9
    .line 10
    :cond_0
    sget v0, Lcom/intsig/tsapp/sync/AppConfigJson;->s_android_camera_revision:I

    .line 11
    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEngineDewarpPostType()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->dewarp_post_engine:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    const/4 v0, 0x2

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEngineUseCupCount()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->alg_engine_cpu:I

    .line 2
    .line 3
    if-lez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-gt v0, v1, :cond_0

    .line 14
    .line 15
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->alg_engine_cpu:I

    .line 16
    .line 17
    return v0

    .line 18
    :cond_0
    const/4 v0, 0x1

    .line 19
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIndonesiaPdfWatermarkStyle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->indonesia_pdf_watermark_style:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getScanProcess()I
    .locals 2

    .line 1
    sget v0, Lcom/intsig/tsapp/sync/AppConfigJson;->s_scan_process:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->scan_process:I

    .line 7
    .line 8
    sput v0, Lcom/intsig/tsapp/sync/AppConfigJson;->s_scan_process:I

    .line 9
    .line 10
    :cond_0
    sget v0, Lcom/intsig/tsapp/sync/AppConfigJson;->s_scan_process:I

    .line 11
    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSupportAppLink()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->support_applink:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isBadCaseUploadOn()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->badcase_upload_show:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isBuyFaxOn()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isSendFaxOn()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->show_buy_fax:I

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    if-ne v0, v1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v1, 0x0

    .line 14
    :goto_0
    return v1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isCameraXForXiaomiAndroidR()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->xiaomi_camera:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isColorEnhanceForLocalSuper()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->ai_image_filter_v3_5:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isCropDeWrapOn()Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->android_clear_and_correct:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_1

    .line 5
    .line 6
    const/4 v2, 0x2

    .line 7
    if-ne v0, v2, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v1, 0x0

    .line 11
    :cond_1
    :goto_0
    return v1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDeBlurOn()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDocLoadingOn()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->experience_optimization:Lcom/intsig/tsapp/sync/AppConfigJson$ExperienceOptimization;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ExperienceOptimization;->doc_loading:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v1, 0x0

    .line 12
    :goto_0
    return v1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isEnlargeIndonesiaWatermarkFontSize()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->indonesia_pdf_watermark_style:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isImageDiscernTagOpen()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernMainSwitchOn()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isImageDiscernTagTest2()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernMainSwitchOn()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->image_discern_tag:Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;

    .line 8
    .line 9
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;->image_discarn_tag_test_2:I

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v1, 0x0

    .line 16
    :goto_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isKepSignAuthenticity()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->keep_sign_authenticity:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isLoadingNewStyle()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->loading_ui_optimize:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isNewLoading()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->loading_optimize:I

    .line 10
    .line 11
    if-ne v0, v1, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/4 v1, 0x0

    .line 15
    :goto_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOcrSilentOn()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->silent_ocr:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOnlyIndonesiaWatermarkAddLogo()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->indonesia_pdf_watermark_style:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOpenAndroidReview()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->android_review:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOpenBankCardJournal()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->bankbills_mobile_v1:I

    .line 10
    .line 11
    if-ne v0, v1, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/4 v1, 0x0

    .line 15
    :goto_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOpenCaptureWorkbenchForThree()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOpenCoverage()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->active_function_642_android:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOpenDocToExcel()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->image2excel_batch_import:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOpenInnovationLab()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->func_lab_v1:Lcom/intsig/tsapp/sync/AppConfigJson$InnovationLab;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$InnovationLab;->main_func:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v1, 0x0

    .line 12
    :goto_0
    return v1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOpenPrinter()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->printer_service:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isPaymentOptimized()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->payment_optimize:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isPriorityQueueInShareLink()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->multifile_priority_queue:I

    .line 10
    .line 11
    if-ne v0, v1, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/4 v1, 0x0

    .line 15
    :goto_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSendFaxOn()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->show_send_fax:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isServerNewUser()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->server_guide_new_user:I

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const/4 v1, 0x2

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    goto :goto_1

    .line 11
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 12
    :goto_1
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShareDirOpen()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_dir:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSharedLinkOptimizeOpen()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->shared_link_optimize:I

    .line 10
    .line 11
    if-ne v0, v1, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/4 v1, 0x0

    .line 15
    :goto_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowGptEntrance()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->show_gpt:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowPdfEnhance()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_strengthen:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowScanFirstDocForAll()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->scan_new_guide_show:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowScanFirstDocForDemo()Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->scan_new_guide_show:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_1

    .line 5
    .line 6
    const/4 v2, 0x2

    .line 7
    if-ne v0, v2, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v1, 0x0

    .line 11
    :cond_1
    :goto_0
    return v1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowTag()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagTest2()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowWord()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->show_msword:I

    .line 2
    .line 3
    sget v1, Lcom/intsig/tsapp/sync/AppConfigJson;->SHOW_WORD:I

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShowingWechatMiniImport()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->show_wxmp_import:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isUsingNewModel()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->android_clear_and_correct:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public needAlgorithmForTag()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagOpen()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->image_discern_tag:Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;

    .line 8
    .line 9
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;->image_discern_tag_algorithm:I

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v1, 0x0

    .line 16
    :goto_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public needDewarpBySuperfilter()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->engine_superfilter_dewarp:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public needShowOcrInMoreDialog()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_preview_style:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-lt v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public needUploadClassifyImage()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernMainSwitchOn()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->image_discern_tag:Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;

    .line 8
    .line 9
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ImageDiscernTag;->upload_image_discern_tag:I

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v1, 0x0

    .line 16
    :goto_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public openEduAuth()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->edu_verify:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public openEduV2Auth()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->edu_community:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public openNewESign()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v2, "key_new_signature_open"

    .line 13
    .line 14
    invoke-virtual {v0, v2, v1}, Lcom/intsig/utils/PreferenceUtil;->Oo08(Ljava/lang/String;Z)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    return v0

    .line 19
    :cond_0
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->esign_v1:I

    .line 20
    .line 21
    if-ne v0, v1, :cond_1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 v1, 0x0

    .line 25
    :goto_0
    return v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public openOcrInverse()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->ocr_inverse:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public openSuperFilter()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->ai_image_filter:I

    .line 10
    .line 11
    if-lez v0, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/4 v1, 0x0

    .line 15
    :goto_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public removeCopyPage()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->paint_test:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public resetAndroidCameraRevision()V
    .locals 1

    .line 1
    const/4 v0, -0x1

    .line 2
    sput v0, Lcom/intsig/tsapp/sync/AppConfigJson;->s_android_camera_revision:I

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public resetStaticScanProcess()V
    .locals 1

    .line 1
    const/4 v0, -0x1

    .line 2
    sput v0, Lcom/intsig/tsapp/sync/AppConfigJson;->s_scan_process:I

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public showOcrFreeParagraphTips()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->ocr_free_paragraph:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public showScanTools()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->scan_tools:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public showSharePreviewStyle()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->share_preview_style:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-lt v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public useSuperFilterNewLoading(Z)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    if-eqz p1, :cond_1

    .line 11
    .line 12
    return v0

    .line 13
    :cond_1
    iget p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson;->ai_image_filter_loading:I

    .line 14
    .line 15
    if-ne p1, v1, :cond_2

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_2
    const/4 v1, 0x0

    .line 19
    :goto_0
    return v1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public usingAdapterClient()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/tsapp/sync/AppConfigJson;->getAndroidCameraRevision()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x3

    .line 6
    if-lt v0, v1, :cond_0

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

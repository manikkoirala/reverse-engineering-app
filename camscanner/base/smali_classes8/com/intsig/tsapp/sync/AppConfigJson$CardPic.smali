.class public Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CardPic"
.end annotation


# instance fields
.field public br:Ljava/lang/String;

.field public de:Ljava/lang/String;

.field public es:Ljava/lang/String;

.field public fr:Ljava/lang/String;

.field public hk:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public in:Ljava/lang/String;

.field public mo:Ljava/lang/String;

.field public my:Ljava/lang/String;

.field public th:Ljava/lang/String;

.field public tw:Ljava/lang/String;

.field public upload_time:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public getUrlByCoun(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, -0x1

    .line 9
    sparse-switch v0, :sswitch_data_0

    .line 10
    .line 11
    .line 12
    goto/16 :goto_0

    .line 13
    .line 14
    :sswitch_0
    const-string v0, "tw"

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    if-nez p1, :cond_0

    .line 21
    .line 22
    goto/16 :goto_0

    .line 23
    .line 24
    :cond_0
    const/16 v1, 0xa

    .line 25
    .line 26
    goto/16 :goto_0

    .line 27
    .line 28
    :sswitch_1
    const-string v0, "th"

    .line 29
    .line 30
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-nez p1, :cond_1

    .line 35
    .line 36
    goto/16 :goto_0

    .line 37
    .line 38
    :cond_1
    const/16 v1, 0x9

    .line 39
    .line 40
    goto/16 :goto_0

    .line 41
    .line 42
    :sswitch_2
    const-string v0, "my"

    .line 43
    .line 44
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-nez p1, :cond_2

    .line 49
    .line 50
    goto/16 :goto_0

    .line 51
    .line 52
    :cond_2
    const/16 v1, 0x8

    .line 53
    .line 54
    goto/16 :goto_0

    .line 55
    .line 56
    :sswitch_3
    const-string v0, "mo"

    .line 57
    .line 58
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    if-nez p1, :cond_3

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_3
    const/4 v1, 0x7

    .line 66
    goto :goto_0

    .line 67
    :sswitch_4
    const-string v0, "in"

    .line 68
    .line 69
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    if-nez p1, :cond_4

    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_4
    const/4 v1, 0x6

    .line 77
    goto :goto_0

    .line 78
    :sswitch_5
    const-string v0, "id"

    .line 79
    .line 80
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 81
    .line 82
    .line 83
    move-result p1

    .line 84
    if-nez p1, :cond_5

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_5
    const/4 v1, 0x5

    .line 88
    goto :goto_0

    .line 89
    :sswitch_6
    const-string v0, "hk"

    .line 90
    .line 91
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-nez p1, :cond_6

    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_6
    const/4 v1, 0x4

    .line 99
    goto :goto_0

    .line 100
    :sswitch_7
    const-string v0, "fr"

    .line 101
    .line 102
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    move-result p1

    .line 106
    if-nez p1, :cond_7

    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_7
    const/4 v1, 0x3

    .line 110
    goto :goto_0

    .line 111
    :sswitch_8
    const-string v0, "es"

    .line 112
    .line 113
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    move-result p1

    .line 117
    if-nez p1, :cond_8

    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_8
    const/4 v1, 0x2

    .line 121
    goto :goto_0

    .line 122
    :sswitch_9
    const-string v0, "de"

    .line 123
    .line 124
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 125
    .line 126
    .line 127
    move-result p1

    .line 128
    if-nez p1, :cond_9

    .line 129
    .line 130
    goto :goto_0

    .line 131
    :cond_9
    const/4 v1, 0x1

    .line 132
    goto :goto_0

    .line 133
    :sswitch_a
    const-string v0, "br"

    .line 134
    .line 135
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 136
    .line 137
    .line 138
    move-result p1

    .line 139
    if-nez p1, :cond_a

    .line 140
    .line 141
    goto :goto_0

    .line 142
    :cond_a
    const/4 v1, 0x0

    .line 143
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 144
    .line 145
    .line 146
    const-string p1, ""

    .line 147
    .line 148
    return-object p1

    .line 149
    :pswitch_0
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->tw:Ljava/lang/String;

    .line 150
    .line 151
    return-object p1

    .line 152
    :pswitch_1
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->th:Ljava/lang/String;

    .line 153
    .line 154
    return-object p1

    .line 155
    :pswitch_2
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->my:Ljava/lang/String;

    .line 156
    .line 157
    return-object p1

    .line 158
    :pswitch_3
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->mo:Ljava/lang/String;

    .line 159
    .line 160
    return-object p1

    .line 161
    :pswitch_4
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->in:Ljava/lang/String;

    .line 162
    .line 163
    return-object p1

    .line 164
    :pswitch_5
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->id:Ljava/lang/String;

    .line 165
    .line 166
    return-object p1

    .line 167
    :pswitch_6
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->hk:Ljava/lang/String;

    .line 168
    .line 169
    return-object p1

    .line 170
    :pswitch_7
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->fr:Ljava/lang/String;

    .line 171
    .line 172
    return-object p1

    .line 173
    :pswitch_8
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->es:Ljava/lang/String;

    .line 174
    .line 175
    return-object p1

    .line 176
    :pswitch_9
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->de:Ljava/lang/String;

    .line 177
    .line 178
    return-object p1

    .line 179
    :pswitch_a
    iget-object p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->br:Ljava/lang/String;

    .line 180
    .line 181
    return-object p1

    .line 182
    nop

    :sswitch_data_0
    .sparse-switch
        0xc50 -> :sswitch_a
        0xc81 -> :sswitch_9
        0xcae -> :sswitch_8
        0xccc -> :sswitch_7
        0xd03 -> :sswitch_6
        0xd1b -> :sswitch_5
        0xd25 -> :sswitch_4
        0xda2 -> :sswitch_3
        0xdac -> :sswitch_2
        0xe74 -> :sswitch_1
        0xe83 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

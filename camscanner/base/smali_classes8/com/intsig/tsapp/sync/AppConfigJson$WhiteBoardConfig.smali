.class public Lcom/intsig/tsapp/sync/AppConfigJson$WhiteBoardConfig;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WhiteBoardConfig"
.end annotation


# instance fields
.field public function_page_switch:I

.field public image_exp:Ljava/lang/String;

.field public image_exp_bg:Ljava/lang/String;

.field public image_exp_enh:Ljava/lang/String;

.field public image_exp_inv:Ljava/lang/String;

.field public pic:Ljava/lang/String;

.field public pic_height:I

.field public pic_width:I

.field public shooting_mode_switch:I

.field public tool_box_switch:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

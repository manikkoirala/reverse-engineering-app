.class public Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PdfAppEnTryV2"
.end annotation


# instance fields
.field public cs_sign:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public cslist_mark:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public cslist_mark_add_text:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public extract_pdf_pages:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public import_files:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public merge_pdfs:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public open_pdf:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public pagelist_bubble:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public pdf_app_edit:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public pdf_merge:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public pdf_tarbar:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public pdf_to_jpg:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public pdf_tools_redirect:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public reorder_pdf_pages:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public save_to_cs:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public send_with_pdf:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

.field public share_via_pdf:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.class public Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PrinterBuyEntry"
.end annotation


# instance fields
.field public driver_download_url:Ljava/lang/String;

.field public latest_driver_version:Ljava/lang/String;

.field public link:Ljava/lang/String;

.field public link_link:Ljava/lang/String;

.field public link_mini_app:Ljava/lang/String;

.field public link_text:Ljava/lang/String;

.field public link_text_des:Ljava/lang/String;

.field public link_use_youzan_webview:I

.field public preview_link:Ljava/lang/String;

.field public preview_mini_app:Ljava/lang/String;

.field public preview_text:Ljava/lang/String;

.field public preview_text_des:Ljava/lang/String;

.field public preview_use_youzan_webview:I

.field public printer_product_pic:Ljava/lang/String;

.field public priview_button_text:Ljava/lang/String;

.field public product_pic_height:I

.field public product_pic_width:I

.field public purchase_paper_link:Ljava/lang/String;

.field public purchase_paper_mini_app:Ljava/lang/String;

.field public purchase_paper_text:Ljava/lang/String;

.field public purchase_use_youzan_webview:I

.field public switch_button_text:Ljava/lang/String;

.field public text:Ljava/lang/String;

.field public toolbox_link:Ljava/lang/String;

.field public toolbox_mini_app:Ljava/lang/String;

.field public toolbox_text:Ljava/lang/String;

.field public toolbox_use_youzan_webview:I

.field public user_name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

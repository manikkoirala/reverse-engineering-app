.class public Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScanServiceEntity"
.end annotation


# instance fields
.field public administrative:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public blackboard:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public card_photo:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public case_file:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public certificates:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public contract:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public draft:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public drawing:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public exercises:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public image_quality:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public legal_papers:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public others:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public ppt:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public references:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public screenshot:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public ticket:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.class public Lcom/intsig/tsapp/sync/AppConfigJson$AppUrl;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppUrl"
.end annotation


# instance fields
.field public accurate_translation:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public anti_ads:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public anti_fake:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public auto_sync:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public book_guide:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public book_mode:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public card_mode:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public card_mode_driver:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public card_mode_idcard:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public card_mode_passport:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public card_photo:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public card_pic:Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;

.field public cloud_space:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public doc_encrypt:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public excel_ocr:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public hd_mode:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public jigsaw_mode:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public jigsaw_mode_2:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public localfolder:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public long_pic:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public me_web:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public ocr_proof:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public ocr_style:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public ocr_template:Lcom/intsig/tsapp/sync/AppConfigJson$OcrTemplate;

.field public patting_mode:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public pdf_encrypt:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public pdf_watermark:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public premium_icon:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public signature:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;

.field public signature2:Lcom/intsig/tsapp/sync/AppConfigJson$TopResource;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

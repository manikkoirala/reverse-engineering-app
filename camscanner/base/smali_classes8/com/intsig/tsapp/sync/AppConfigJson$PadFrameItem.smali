.class public Lcom/intsig/tsapp/sync/AppConfigJson$PadFrameItem;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PadFrameItem"
.end annotation


# instance fields
.field public acr_padding_bottom:I

.field public acr_padding_left:I

.field public acr_padding_right:I

.field public acr_padding_top:I

.field public across_url:Ljava/lang/String;

.field public des:Ljava/lang/String;

.field public focus:Z

.field public frame:Ljava/lang/String;

.field public index:I

.field public mul_padding_bottom:I

.field public mul_padding_left:I

.field public mul_padding_right:I

.field public mul_padding_top:I

.field public mullion_url:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public thumbnails:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/tsapp/sync/AppConfigJson$PadFrameItem;->focus:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 5
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$PadFrameItem;->focus:Z

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    const/4 p1, 0x0

    .line 4
    iput-boolean p1, p0, Lcom/intsig/tsapp/sync/AppConfigJson$PadFrameItem;->focus:Z

    return-void
.end method

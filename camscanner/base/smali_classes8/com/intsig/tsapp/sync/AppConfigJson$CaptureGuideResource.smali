.class public Lcom/intsig/tsapp/sync/AppConfigJson$CaptureGuideResource;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CaptureGuideResource"
.end annotation


# instance fields
.field public bank_bill_us:Ljava/lang/String;

.field public bank_cn:Ljava/lang/String;

.field public book_cn:Ljava/lang/String;

.field public book_us:Ljava/lang/String;

.field public class_cn:Ljava/lang/String;

.field public class_us:Ljava/lang/String;

.field public count_mod:Ljava/lang/String;

.field public eraser_cn:Ljava/lang/String;

.field public eraser_us:Ljava/lang/String;

.field public image_edit_guide_cn:Ljava/lang/String;

.field public image_edit_guide_en:Ljava/lang/String;

.field public invoice_bank_cn:Ljava/lang/String;

.field public invoice_cn:Ljava/lang/String;

.field public paper_cn:Ljava/lang/String;

.field public paper_us:Ljava/lang/String;

.field public sign_cn:Ljava/lang/String;

.field public sign_us:Ljava/lang/String;

.field public whiteboard_cn:Ljava/lang/String;

.field public whiteboard_us:Ljava/lang/String;

.field public writing_pad:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

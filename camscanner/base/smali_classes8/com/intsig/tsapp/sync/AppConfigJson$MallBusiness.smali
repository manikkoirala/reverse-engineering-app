.class public Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MallBusiness"
.end annotation


# instance fields
.field public enterprise_show:Lcom/intsig/tsapp/sync/AppConfigJson$EnterpriseMall;

.field public mall_entry:I

.field public mall_entry_img:Ljava/lang/String;

.field public mall_entry_main_paperwork:Ljava/lang/String;

.field public mall_entry_url:Ljava/lang/String;

.field public mall_entry_vice_paperwork:Ljava/lang/String;

.field public show_authorize_setting:I

.field public use_youzan_webview:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

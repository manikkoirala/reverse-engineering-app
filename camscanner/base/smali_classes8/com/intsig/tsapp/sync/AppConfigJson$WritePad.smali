.class public Lcom/intsig/tsapp/sync/AppConfigJson$WritePad;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "AppConfigJson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/tsapp/sync/AppConfigJson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WritePad"
.end annotation


# instance fields
.field public button_des:Ljava/lang/String;

.field public des_pic:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public display_effect:Lcom/intsig/tsapp/sync/AppConfigJson$DisPlayEffect;

.field public func_switch:I

.field public height:I

.field public pad_share_frame:Lcom/intsig/tsapp/sync/AppConfigJson$PadShareFrame;

.field public pop_switch:I

.field public theme_des:Ljava/lang/String;

.field public url:Ljava/lang/String;

.field public use_button_des:Ljava/lang/String;

.field public use_youzan_webview:I

.field public watermark_config:Lcom/intsig/tsapp/sync/AppConfigJson$WatermarkConfig;

.field public width:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    return-void
.end method

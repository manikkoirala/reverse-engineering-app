.class public final Lcom/intsig/okgo/utils/CommonHeaderInterceptor;
.super Ljava/lang/Object;
.source "CommonHeaderInterceptor.kt"

# interfaces
.implements Lokhttp3/Interceptor;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/okgo/OkGoUtils$IInfoProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/okgo/OkGoUtils$IInfoProvider;)V
    .locals 1
    .param p1    # Lcom/intsig/okgo/OkGoUtils$IInfoProvider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "iInfoProvider"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/okgo/utils/CommonHeaderInterceptor;->〇080:Lcom/intsig/okgo/OkGoUtils$IInfoProvider;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 4
    .param p1    # Lokhttp3/Interceptor$Chain;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "chain"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->Oo08()Lokhttp3/Request;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lokhttp3/Request;->〇80〇808〇O()Lokhttp3/Request$Builder;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->Oo08()Lokhttp3/Request;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1}, Lokhttp3/Request;->o〇0()Lokhttp3/Headers;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "use_private_ua"

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Lokhttp3/Headers;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    invoke-virtual {v0, v2}, Lokhttp3/Request$Builder;->OO0o〇〇(Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    iget-object v1, p0, Lcom/intsig/okgo/utils/CommonHeaderInterceptor;->〇080:Lcom/intsig/okgo/OkGoUtils$IInfoProvider;

    .line 35
    .line 36
    invoke-interface {v1}, Lcom/intsig/okgo/OkGoUtils$IInfoProvider;->getAppName()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    const-string v2, "iInfoProvider.appName"

    .line 41
    .line 42
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    :goto_0
    const-string v2, "User-Agent"

    .line 46
    .line 47
    invoke-virtual {v0, v2}, Lokhttp3/Request$Builder;->OO0o〇〇(Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->〇080(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 51
    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇888()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    const-string v2, "x-is-cs-ept-d"

    .line 70
    .line 71
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->〇080(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 72
    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/okgo/utils/CommonHeaderInterceptor;->〇080:Lcom/intsig/okgo/OkGoUtils$IInfoProvider;

    .line 75
    .line 76
    invoke-interface {v1}, Lcom/intsig/okgo/OkGoUtils$IInfoProvider;->〇080()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    const-string v2, "iInfoProvider.token"

    .line 81
    .line 82
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    const-string v2, "x-is-token"

    .line 86
    .line 87
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->〇080(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 88
    .line 89
    .line 90
    iget-object v1, p0, Lcom/intsig/okgo/utils/CommonHeaderInterceptor;->〇080:Lcom/intsig/okgo/OkGoUtils$IInfoProvider;

    .line 91
    .line 92
    invoke-interface {v1}, Lcom/intsig/okgo/OkGoUtils$IInfoProvider;->〇o00〇〇Oo()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    const-string v2, "iInfoProvider.uuid"

    .line 97
    .line 98
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    const-string v2, "x-is-request-id"

    .line 102
    .line 103
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->〇080(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 104
    .line 105
    .line 106
    iget-object v1, p0, Lcom/intsig/okgo/utils/CommonHeaderInterceptor;->〇080:Lcom/intsig/okgo/OkGoUtils$IInfoProvider;

    .line 107
    .line 108
    invoke-interface {v1}, Lcom/intsig/okgo/OkGoUtils$IInfoProvider;->getAccount()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    const/4 v2, 0x1

    .line 113
    if-eqz v1, :cond_2

    .line 114
    .line 115
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 116
    .line 117
    .line 118
    move-result v3

    .line 119
    if-eqz v3, :cond_1

    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_1
    const/4 v3, 0x0

    .line 123
    goto :goto_2

    .line 124
    :cond_2
    :goto_1
    const/4 v3, 0x1

    .line 125
    :goto_2
    xor-int/2addr v2, v3

    .line 126
    if-eqz v2, :cond_3

    .line 127
    .line 128
    goto :goto_3

    .line 129
    :cond_3
    const/4 v1, 0x0

    .line 130
    :goto_3
    if-eqz v1, :cond_4

    .line 131
    .line 132
    const-string v2, "x-is-account-encrypt"

    .line 133
    .line 134
    invoke-virtual {v0, v2, v1}, Lokhttp3/Request$Builder;->〇080(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 135
    .line 136
    .line 137
    :cond_4
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->〇o00〇〇Oo()Lokhttp3/Request;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->〇o00〇〇Oo(Lokhttp3/Request;)Lokhttp3/Response;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    return-object p1
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

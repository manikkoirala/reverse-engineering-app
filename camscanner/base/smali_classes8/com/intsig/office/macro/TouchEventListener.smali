.class public interface abstract Lcom/intsig/office/macro/TouchEventListener;
.super Ljava/lang/Object;
.source "TouchEventListener.java"


# static fields
.field public static final EVENT_CLICK:B = 0xat

.field public static final EVENT_DOUBLE_TAP:B = 0x8t

.field public static final EVENT_DOUBLE_TAP_EVENT:B = 0x9t

.field public static final EVENT_DOWN:B = 0x1t

.field public static final EVENT_FLING:B = 0x6t

.field public static final EVENT_LONG_PRESS:B = 0x5t

.field public static final EVENT_SCROLL:B = 0x4t

.field public static final EVENT_SHOW_PRESS:B = 0x2t

.field public static final EVENT_SINGLE_TAP_CONFIRMED:B = 0x7t

.field public static final EVENT_SINGLE_TAP_UP:B = 0x3t

.field public static final EVENT_TOUCH:B


# virtual methods
.method public abstract onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z
.end method

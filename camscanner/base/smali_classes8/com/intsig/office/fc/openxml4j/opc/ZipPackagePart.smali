.class public Lcom/intsig/office/fc/openxml4j/opc/ZipPackagePart;
.super Lcom/intsig/office/fc/openxml4j/opc/PackagePart;
.source "ZipPackagePart.java"


# instance fields
.field private zipEntry:Ljava/util/zip/ZipEntry;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;-><init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Ljava/util/zip/ZipEntry;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p3, p4}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;-><init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    .line 3
    iput-object p2, p0, Lcom/intsig/office/fc/openxml4j/opc/ZipPackagePart;->zipEntry:Ljava/util/zip/ZipEntry;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    .line 2
    .line 3
    const-string v1, "Method not implemented !"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public flush()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    .line 2
    .line 3
    const-string v1, "Method not implemented !"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getInputStreamImpl()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_container:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getZipArchive()Lcom/intsig/office/fc/openxml4j/util/ZipEntrySource;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/ZipPackagePart;->zipEntry:Ljava/util/zip/ZipEntry;

    .line 8
    .line 9
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/openxml4j/util/ZipEntrySource;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getOutputStreamImpl()Ljava/io/OutputStream;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZipArchive()Ljava/util/zip/ZipEntry;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/ZipPackagePart;->zipEntry:Ljava/util/zip/ZipEntry;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public load(Ljava/io/InputStream;)Z
    .locals 1

    .line 1
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    .line 2
    .line 3
    const-string v0, "Method not implemented !"

    .line 4
    .line 5
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    throw p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public save(Ljava/io/OutputStream;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/ZipPartMarshaller;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/ZipPartMarshaller;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/ZipPartMarshaller;->marshall(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/io/OutputStream;)Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

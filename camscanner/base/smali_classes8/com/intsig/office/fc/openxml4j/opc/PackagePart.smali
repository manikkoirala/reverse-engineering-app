.class public abstract Lcom/intsig/office/fc/openxml4j/opc/PackagePart;
.super Ljava/lang/Object;
.source "PackagePart.java"

# interfaces
.implements Lcom/intsig/office/fc/openxml4j/opc/RelationshipSource;


# instance fields
.field protected _container:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

.field protected _contentType:Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;

.field private _isDeleted:Z

.field private _isRelationshipPart:Z

.field protected _partName:Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

.field private _relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;-><init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;Z)V

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p2, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_partName:Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    .line 4
    iput-object p3, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_contentType:Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_container:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 6
    invoke-virtual {p2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;->isRelationshipPartURI()Z

    move-result p1

    iput-boolean p1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    if-eqz p4, :cond_0

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->loadRelationships()V

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 8
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;

    invoke-direct {v0, p3}, Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;-><init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;)V

    return-void
.end method

.method private getRelationshipsCore(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->throwExceptionIfRelationship()V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;-><init>(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 14
    .line 15
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 18
    .line 19
    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;-><init>(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private throwExceptionIfRelationship()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    .line 7
    .line 8
    const-string v1, "Can do this operation on a relationship part !"

    .line 9
    .line 10
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    throw v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public addExternalRelationship(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->addExternalRelationship(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    move-result-object p1

    return-object p1
.end method

.method public addExternalRelationship(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    .locals 2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 4
    :cond_0
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    iget-object p1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    sget-object v1, Lcom/intsig/office/fc/openxml4j/opc/TargetMode;->EXTERNAL:Lcom/intsig/office/fc/openxml4j/opc/TargetMode;

    invoke-virtual {p1, v0, v1, p2, p3}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->addRelationship(Ljava/net/URI;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 6
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid target - "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 7
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "relationshipType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 8
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "target"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addRelationship(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->addRelationship(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    move-result-object p1

    return-object p1
.end method

.method public addRelationship(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    .locals 1

    if-eqz p1, :cond_4

    if-eqz p2, :cond_3

    if-eqz p3, :cond_2

    .line 2
    iget-boolean v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;->isRelationshipPartURI()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;->getURI()Ljava/net/URI;

    move-result-object p1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->addRelationship(Ljava/net/URI;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    move-result-object p1

    return-object p1

    .line 6
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    const-string p2, "Rule M1.25: The Relationships part shall not have relationships to any other part."

    invoke-direct {p1, p2}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 7
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "relationshipType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 8
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "targetMode"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 9
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "targetPartName"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addRelationship(Ljava/net/URI;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->addRelationship(Ljava/net/URI;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    move-result-object p1

    return-object p1
.end method

.method public addRelationship(Ljava/net/URI;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    .locals 1

    if-eqz p1, :cond_4

    if-eqz p2, :cond_3

    if-eqz p3, :cond_2

    .line 11
    iget-boolean v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagingURIHelper;->isRelationshipPartURI(Ljava/net/URI;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 12
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    invoke-direct {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->addRelationship(Ljava/net/URI;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    move-result-object p1

    return-object p1

    .line 15
    :cond_1
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    const-string p2, "Rule M1.25: The Relationships part shall not have relationships to any other part."

    invoke-direct {p1, p2}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 16
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "relationshipType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 17
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "targetMode"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 18
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "targetPartName"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public clearRelationships()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->clear()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract close()V
.end method

.method public abstract flush()V
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_contentType:Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;->toString()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStreamImpl()Ljava/io/InputStream;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    new-instance v0, Ljava/io/IOException;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "Can\'t obtain the input stream from "

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_partName:Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    .line 21
    .line 22
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    throw v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected abstract getInputStreamImpl()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 4

    .line 1
    instance-of v0, p0, Lcom/intsig/office/fc/openxml4j/opc/ZipPackagePart;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_container:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_partName:Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->removePart(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_container:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_partName:Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_contentType:Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;

    .line 17
    .line 18
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const/4 v3, 0x0

    .line 23
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->createPart(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Ljava/lang/String;Z)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 30
    .line 31
    iput-object v1, v0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getOutputStreamImpl()Ljava/io/OutputStream;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    .line 39
    .line 40
    const-string v1, "Can\'t create a temporary part !"

    .line 41
    .line 42
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    throw v0

    .line 46
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getOutputStreamImpl()Ljava/io/OutputStream;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    :goto_0
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected abstract getOutputStreamImpl()Ljava/io/OutputStream;
.end method

.method public getPackage()Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_container:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPartName()Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_partName:Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationshipByID(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getRelationships()Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsCore(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsCore(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public hasRelationships()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->size()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-lez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDeleted()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isDeleted:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isRelationshipExists(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;)Z
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationships()Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    :try_end_0
    .catch Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    if-ne v1, p1, :cond_0

    .line 22
    .line 23
    const/4 p1, 0x1

    .line 24
    return p1

    .line 25
    :catch_0
    :cond_1
    const/4 p1, 0x0

    .line 26
    return p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public isRelationshipPart()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract load(Ljava/io/InputStream;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation
.end method

.method public loadRelationships()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isRelationshipPart:Z

    .line 12
    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->throwExceptionIfRelationship()V

    .line 16
    .line 17
    .line 18
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;-><init>(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 24
    .line 25
    :cond_1
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public removeRelationship(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_relationships:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->removeRelationship(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public abstract save(Ljava/io/OutputStream;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_container:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;

    .line 6
    .line 7
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_contentType:Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    .line 14
    .line 15
    const-string v0, "You can\'t change the content type of a part."

    .line 16
    .line 17
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    throw p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDeleted(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_isDeleted:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Name: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_partName:Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, " - Content Type: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->_contentType:Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/ContentType;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

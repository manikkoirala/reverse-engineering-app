.class public Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;
.super Ljava/lang/Object;
.source "PackagePropertiesUnmarshaller.java"

# interfaces
.implements Lcom/intsig/office/fc/openxml4j/opc/internal/PartUnmarshaller;


# static fields
.field protected static final KEYWORD_CATEGORY:Ljava/lang/String; = "category"

.field protected static final KEYWORD_CONTENT_STATUS:Ljava/lang/String; = "contentStatus"

.field protected static final KEYWORD_CONTENT_TYPE:Ljava/lang/String; = "contentType"

.field protected static final KEYWORD_CREATED:Ljava/lang/String; = "created"

.field protected static final KEYWORD_CREATOR:Ljava/lang/String; = "creator"

.field protected static final KEYWORD_DESCRIPTION:Ljava/lang/String; = "description"

.field protected static final KEYWORD_IDENTIFIER:Ljava/lang/String; = "identifier"

.field protected static final KEYWORD_KEYWORDS:Ljava/lang/String; = "keywords"

.field protected static final KEYWORD_LANGUAGE:Ljava/lang/String; = "language"

.field protected static final KEYWORD_LAST_MODIFIED_BY:Ljava/lang/String; = "lastModifiedBy"

.field protected static final KEYWORD_LAST_PRINTED:Ljava/lang/String; = "lastPrinted"

.field protected static final KEYWORD_MODIFIED:Ljava/lang/String; = "modified"

.field protected static final KEYWORD_REVISION:Ljava/lang/String; = "revision"

.field protected static final KEYWORD_SUBJECT:Ljava/lang/String; = "subject"

.field protected static final KEYWORD_TITLE:Ljava/lang/String; = "title"

.field protected static final KEYWORD_VERSION:Ljava/lang/String; = "version"

.field private static final namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

.field private static final namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

.field private static final namespaceDcTerms:Lcom/intsig/office/fc/dom4j/Namespace;

.field private static final namespaceXML:Lcom/intsig/office/fc/dom4j/Namespace;

.field private static final namespaceXSI:Lcom/intsig/office/fc/dom4j/Namespace;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 2
    .line 3
    const-string v1, "dc"

    .line 4
    .line 5
    const-string v2, "http://purl.org/dc/elements/1.1/"

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 13
    .line 14
    const-string v1, "cp"

    .line 15
    .line 16
    const-string v2, "http://schemas.openxmlformats.org/package/2006/metadata/core-properties"

    .line 17
    .line 18
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 24
    .line 25
    const-string v1, "dcterms"

    .line 26
    .line 27
    const-string v2, "http://purl.org/dc/terms/"

    .line 28
    .line 29
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDcTerms:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 33
    .line 34
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 35
    .line 36
    const-string v1, "xml"

    .line 37
    .line 38
    const-string v2, "http://www.w3.org/XML/1998/namespace"

    .line 39
    .line 40
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceXML:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 44
    .line 45
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 46
    .line 47
    const-string v1, "xsi"

    .line 48
    .line 49
    const-string v2, "http://www.w3.org/2001/XMLSchema-instance"

    .line 50
    .line 51
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceXSI:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private loadCategory(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "category"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadContentStatus(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "contentStatus"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadContentType(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "contentType"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadCreated(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "created"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDcTerms:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadCreator(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "creator"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadDescription(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "description"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadIdentifier(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "identifier"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadKeywords(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "keywords"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadLanguage(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "language"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadLastModifiedBy(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "lastModifiedBy"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadLastPrinted(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "lastPrinted"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadModified(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "modified"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDcTerms:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadRevision(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "revision"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadSubject(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "subject"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadTitle(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "title"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private loadVersion(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 6
    .line 7
    const-string v1, "version"

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceCP:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return-object p1

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getStringValue()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public checkElementForOPCCompliance(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->declaredNamespaces()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const-string v2, "http://schemas.openxmlformats.org/markup-compatibility/2006"

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-nez v1, :cond_0

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;

    .line 35
    .line 36
    const-string v0, "OPC Compliance error [M4.2]: A format consumer shall consider the use of the Markup Compatibility namespace to be an error."

    .line 37
    .line 38
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    throw p1

    .line 42
    :cond_1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const-string v1, "http://purl.org/dc/terms/"

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    const-string v2, "modified"

    .line 57
    .line 58
    const-string v3, "created"

    .line 59
    .line 60
    if-eqz v0, :cond_3

    .line 61
    .line 62
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    if-nez v0, :cond_3

    .line 71
    .line 72
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    if-eqz v0, :cond_2

    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_2
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;

    .line 84
    .line 85
    const-string v0, "OPC Compliance error [M4.3]: Producers shall not create a document element that contains refinements to the Dublin Core elements, except for the two specified in the schema: <dcterms:created> and <dcterms:modified> Consumers shall consider a document element that violates this constraint to be an error."

    .line 86
    .line 87
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    throw p1

    .line 91
    :cond_3
    :goto_1
    new-instance v0, Lcom/intsig/office/fc/dom4j/QName;

    .line 92
    .line 93
    const-string v4, "lang"

    .line 94
    .line 95
    sget-object v5, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceXML:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 96
    .line 97
    invoke-direct {v0, v4, v5}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 98
    .line 99
    .line 100
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    if-nez v0, :cond_a

    .line 105
    .line 106
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getNamespace()Lcom/intsig/office/fc/dom4j/Namespace;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/Namespace;->getURI()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    if-eqz v0, :cond_8

    .line 119
    .line 120
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 125
    .line 126
    .line 127
    move-result v3

    .line 128
    if-nez v3, :cond_5

    .line 129
    .line 130
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 131
    .line 132
    .line 133
    move-result v2

    .line 134
    if-eqz v2, :cond_4

    .line 135
    .line 136
    goto :goto_2

    .line 137
    :cond_4
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;

    .line 138
    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    .line 140
    .line 141
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    .line 143
    .line 144
    const-string v3, "Namespace error : "

    .line 145
    .line 146
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    const-string v0, " shouldn\'t have the following naemspace -> "

    .line 153
    .line 154
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object v0

    .line 164
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    throw p1

    .line 168
    :cond_5
    :goto_2
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 169
    .line 170
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->namespaceXSI:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 171
    .line 172
    const-string v3, "type"

    .line 173
    .line 174
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 175
    .line 176
    .line 177
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 178
    .line 179
    .line 180
    move-result-object v1

    .line 181
    const-string v3, "\' must have the \'"

    .line 182
    .line 183
    const-string v4, "The element \'"

    .line 184
    .line 185
    if-eqz v1, :cond_7

    .line 186
    .line 187
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    const-string v5, "dcterms:W3CDTF"

    .line 192
    .line 193
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 194
    .line 195
    .line 196
    move-result v1

    .line 197
    if-eqz v1, :cond_6

    .line 198
    .line 199
    goto :goto_3

    .line 200
    :cond_6
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;

    .line 201
    .line 202
    new-instance v1, Ljava/lang/StringBuilder;

    .line 203
    .line 204
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    .line 215
    .line 216
    invoke-virtual {v2}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    .line 222
    .line 223
    const-string v0, ":type\' attribute with the value \'dcterms:W3CDTF\' !"

    .line 224
    .line 225
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    .line 227
    .line 228
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 229
    .line 230
    .line 231
    move-result-object v0

    .line 232
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    .line 233
    .line 234
    .line 235
    throw p1

    .line 236
    :cond_7
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;

    .line 237
    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    .line 239
    .line 240
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    .line 242
    .line 243
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    .line 245
    .line 246
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    .line 248
    .line 249
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    .line 251
    .line 252
    invoke-virtual {v2}, Lcom/intsig/office/fc/dom4j/Namespace;->getPrefix()Ljava/lang/String;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    const-string v0, ":type\' attribute present !"

    .line 260
    .line 261
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    .line 263
    .line 264
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 265
    .line 266
    .line 267
    move-result-object v0

    .line 268
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    throw p1

    .line 272
    :cond_8
    :goto_3
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    .line 273
    .line 274
    .line 275
    move-result-object p1

    .line 276
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 277
    .line 278
    .line 279
    move-result v0

    .line 280
    if-eqz v0, :cond_9

    .line 281
    .line 282
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 283
    .line 284
    .line 285
    move-result-object v0

    .line 286
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 287
    .line 288
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->checkElementForOPCCompliance(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 289
    .line 290
    .line 291
    goto :goto_4

    .line 292
    :cond_9
    return-void

    .line 293
    :cond_a
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;

    .line 294
    .line 295
    const-string v0, "OPC Compliance error [M4.4]: Producers shall not create a document element that contains the xml:lang attribute. Consumers shall consider a document element that violates this constraint to be an error."

    .line 296
    .line 297
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    .line 298
    .line 299
    .line 300
    throw p1
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public unmarshall(Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;Ljava/io/InputStream;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;->getPackage()Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;->getPartName()Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;-><init>(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;)V

    .line 12
    .line 13
    .line 14
    if-nez p2, :cond_2

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;->getZipEntry()Ljava/util/zip/ZipEntry;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    if-eqz p2, :cond_0

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;->getPackage()Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    invoke-virtual {p2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getZipArchive()Lcom/intsig/office/fc/openxml4j/util/ZipEntrySource;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;->getZipEntry()Ljava/util/zip/ZipEntry;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-interface {p2, p1}, Lcom/intsig/office/fc/openxml4j/util/ZipEntrySource;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;->getPackage()Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 40
    .line 41
    .line 42
    move-result-object p2

    .line 43
    if-eqz p2, :cond_1

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;->getPackage()Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    invoke-static {p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/ZipHelper;->getCorePropertiesZipEntry(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;)Ljava/util/zip/ZipEntry;

    .line 50
    .line 51
    .line 52
    move-result-object p2

    .line 53
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/UnmarshallContext;->getPackage()Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getZipArchive()Lcom/intsig/office/fc/openxml4j/util/ZipEntrySource;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-interface {p1, p2}, Lcom/intsig/office/fc/openxml4j/util/ZipEntrySource;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    goto :goto_0

    .line 66
    :cond_1
    new-instance p1, Ljava/io/IOException;

    .line 67
    .line 68
    const-string p2, "Error while trying to get the part input stream."

    .line 69
    .line 70
    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    throw p1

    .line 74
    :cond_2
    :goto_0
    new-instance p1, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 75
    .line 76
    invoke-direct {p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 77
    .line 78
    .line 79
    :try_start_0
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 84
    .line 85
    .line 86
    move-result-object p2

    .line 87
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->checkElementForOPCCompliance(Lcom/intsig/office/fc/dom4j/Element;)V
    :try_end_0
    .catch Lcom/intsig/office/fc/dom4j/DocumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    .line 89
    .line 90
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadCategory(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object p2

    .line 94
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setCategoryProperty(Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadContentStatus(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setContentStatusProperty(Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadContentType(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p2

    .line 108
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setContentTypeProperty(Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadCreated(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setCreatedProperty(Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadCreator(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object p2

    .line 122
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setCreatorProperty(Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadDescription(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object p2

    .line 129
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setDescriptionProperty(Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadIdentifier(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object p2

    .line 136
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setIdentifierProperty(Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadKeywords(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object p2

    .line 143
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setKeywordsProperty(Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadLanguage(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object p2

    .line 150
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setLanguageProperty(Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadLastModifiedBy(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object p2

    .line 157
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setLastModifiedByProperty(Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadLastPrinted(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object p2

    .line 164
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setLastPrintedProperty(Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadModified(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object p2

    .line 171
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setModifiedProperty(Ljava/lang/String;)V

    .line 172
    .line 173
    .line 174
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadRevision(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object p2

    .line 178
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setRevisionProperty(Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadSubject(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object p2

    .line 185
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setSubjectProperty(Ljava/lang/String;)V

    .line 186
    .line 187
    .line 188
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadTitle(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 189
    .line 190
    .line 191
    move-result-object p2

    .line 192
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setTitleProperty(Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/unmarshallers/PackagePropertiesUnmarshaller;->loadVersion(Lcom/intsig/office/fc/dom4j/Document;)Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->setVersionProperty(Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    return-object v0

    .line 203
    :catch_0
    move-exception p1

    .line 204
    new-instance p2, Ljava/io/IOException;

    .line 205
    .line 206
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/DocumentException;->getMessage()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object p1

    .line 210
    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    throw p2
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

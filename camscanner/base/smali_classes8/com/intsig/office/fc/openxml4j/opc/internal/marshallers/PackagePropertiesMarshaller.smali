.class public Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;
.super Ljava/lang/Object;
.source "PackagePropertiesMarshaller.java"

# interfaces
.implements Lcom/intsig/office/fc/openxml4j/opc/internal/PartMarshaller;


# static fields
.field protected static final KEYWORD_CATEGORY:Ljava/lang/String; = "category"

.field protected static final KEYWORD_CONTENT_STATUS:Ljava/lang/String; = "contentStatus"

.field protected static final KEYWORD_CONTENT_TYPE:Ljava/lang/String; = "contentType"

.field protected static final KEYWORD_CREATED:Ljava/lang/String; = "created"

.field protected static final KEYWORD_CREATOR:Ljava/lang/String; = "creator"

.field protected static final KEYWORD_DESCRIPTION:Ljava/lang/String; = "description"

.field protected static final KEYWORD_IDENTIFIER:Ljava/lang/String; = "identifier"

.field protected static final KEYWORD_KEYWORDS:Ljava/lang/String; = "keywords"

.field protected static final KEYWORD_LANGUAGE:Ljava/lang/String; = "language"

.field protected static final KEYWORD_LAST_MODIFIED_BY:Ljava/lang/String; = "lastModifiedBy"

.field protected static final KEYWORD_LAST_PRINTED:Ljava/lang/String; = "lastPrinted"

.field protected static final KEYWORD_MODIFIED:Ljava/lang/String; = "modified"

.field protected static final KEYWORD_REVISION:Ljava/lang/String; = "revision"

.field protected static final KEYWORD_SUBJECT:Ljava/lang/String; = "subject"

.field protected static final KEYWORD_TITLE:Ljava/lang/String; = "title"

.field protected static final KEYWORD_VERSION:Ljava/lang/String; = "version"

.field private static final namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

.field private static final namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

.field private static final namespaceDcTerms:Lcom/intsig/office/fc/dom4j/Namespace;

.field private static final namespaceXSI:Lcom/intsig/office/fc/dom4j/Namespace;


# instance fields
.field propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

.field xmlDoc:Lcom/intsig/office/fc/dom4j/Document;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 2
    .line 3
    const-string v1, "dc"

    .line 4
    .line 5
    const-string v2, "http://purl.org/dc/elements/1.1/"

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 13
    .line 14
    const-string v1, ""

    .line 15
    .line 16
    const-string v2, "http://schemas.openxmlformats.org/package/2006/metadata/core-properties"

    .line 17
    .line 18
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 24
    .line 25
    const-string v1, "dcterms"

    .line 26
    .line 27
    const-string v2, "http://purl.org/dc/terms/"

    .line 28
    .line 29
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDcTerms:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 33
    .line 34
    new-instance v0, Lcom/intsig/office/fc/dom4j/Namespace;

    .line 35
    .line 36
    const-string v1, "xsi"

    .line 37
    .line 38
    const-string v2, "http://www.w3.org/2001/XMLSchema-instance"

    .line 39
    .line 40
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Namespace;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    sput-object v0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceXSI:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private addCategory()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getCategoryProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "category"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getCategoryProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addContentStatus()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getContentStatusProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "contentStatus"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getContentStatusProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addContentType()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getContentTypeProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "contentType"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getContentTypeProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addCreated()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getCreatedProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDcTerms:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "created"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 55
    .line 56
    const-string v2, "type"

    .line 57
    .line 58
    sget-object v3, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceXSI:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 59
    .line 60
    invoke-direct {v1, v2, v3}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 61
    .line 62
    .line 63
    const-string v2, "dcterms:W3CDTF"

    .line 64
    .line 65
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->addAttribute(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 66
    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 69
    .line 70
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getCreatedPropertyString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addCreator()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getCreatorProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "creator"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getCreatorProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addDescription()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getDescriptionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "description"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getDescriptionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addIdentifier()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getIdentifierProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "identifier"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getIdentifierProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addKeywords()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getKeywordsProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "keywords"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getKeywordsProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addLanguage()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getLanguageProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "language"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getLanguageProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addLastModifiedBy()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getLastModifiedByProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "lastModifiedBy"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getLastModifiedByProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addLastPrinted()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getLastPrintedProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "lastPrinted"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getLastPrintedPropertyString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addModified()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getModifiedProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDcTerms:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "modified"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 55
    .line 56
    const-string v2, "type"

    .line 57
    .line 58
    sget-object v3, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceXSI:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 59
    .line 60
    invoke-direct {v1, v2, v3}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 61
    .line 62
    .line 63
    const-string v2, "dcterms:W3CDTF"

    .line 64
    .line 65
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->addAttribute(Lcom/intsig/office/fc/dom4j/QName;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 66
    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 69
    .line 70
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getModifiedPropertyString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addRevision()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getRevisionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "revision"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getRevisionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addSubject()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getSubjectProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "subject"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getSubjectProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addTitle()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getTitleProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceDC:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "title"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getTitleProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private addVersion()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getVersionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->hasValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 23
    .line 24
    const-string v3, "version"

    .line 25
    .line 26
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lcom/intsig/office/fc/dom4j/QName;

    .line 42
    .line 43
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Branch;->clearContent()V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;->getVersionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/util/Nullable;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->addText(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method


# virtual methods
.method public marshall(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/io/OutputStream;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation

    .line 1
    instance-of p2, p1, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->propsPart:Lcom/intsig/office/fc/openxml4j/opc/internal/PackagePropertiesPart;

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/office/fc/dom4j/DocumentHelper;->createDocument()Lcom/intsig/office/fc/dom4j/Document;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->xmlDoc:Lcom/intsig/office/fc/dom4j/Document;

    .line 14
    .line 15
    new-instance p2, Lcom/intsig/office/fc/dom4j/QName;

    .line 16
    .line 17
    const-string v0, "coreProperties"

    .line 18
    .line 19
    sget-object v1, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->namespaceCoreProperties:Lcom/intsig/office/fc/dom4j/Namespace;

    .line 20
    .line 21
    invoke-direct {p2, v0, v1}, Lcom/intsig/office/fc/dom4j/QName;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/Namespace;)V

    .line 22
    .line 23
    .line 24
    invoke-interface {p1, p2}, Lcom/intsig/office/fc/dom4j/Branch;->addElement(Lcom/intsig/office/fc/dom4j/QName;)Lcom/intsig/office/fc/dom4j/Element;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string p2, "cp"

    .line 29
    .line 30
    const-string v0, "http://schemas.openxmlformats.org/package/2006/metadata/core-properties"

    .line 31
    .line 32
    invoke-interface {p1, p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->addNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 33
    .line 34
    .line 35
    const-string p2, "dc"

    .line 36
    .line 37
    const-string v0, "http://purl.org/dc/elements/1.1/"

    .line 38
    .line 39
    invoke-interface {p1, p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->addNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 40
    .line 41
    .line 42
    const-string p2, "dcterms"

    .line 43
    .line 44
    const-string v0, "http://purl.org/dc/terms/"

    .line 45
    .line 46
    invoke-interface {p1, p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->addNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    const-string p2, "xsi"

    .line 50
    .line 51
    const-string v0, "http://www.w3.org/2001/XMLSchema-instance"

    .line 52
    .line 53
    invoke-interface {p1, p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->addNamespace(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 54
    .line 55
    .line 56
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addCategory()V

    .line 57
    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addContentStatus()V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addContentType()V

    .line 63
    .line 64
    .line 65
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addCreated()V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addCreator()V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addDescription()V

    .line 72
    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addIdentifier()V

    .line 75
    .line 76
    .line 77
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addKeywords()V

    .line 78
    .line 79
    .line 80
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addLanguage()V

    .line 81
    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addLastModifiedBy()V

    .line 84
    .line 85
    .line 86
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addLastPrinted()V

    .line 87
    .line 88
    .line 89
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addModified()V

    .line 90
    .line 91
    .line 92
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addRevision()V

    .line 93
    .line 94
    .line 95
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addSubject()V

    .line 96
    .line 97
    .line 98
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addTitle()V

    .line 99
    .line 100
    .line 101
    invoke-direct {p0}, Lcom/intsig/office/fc/openxml4j/opc/internal/marshallers/PackagePropertiesMarshaller;->addVersion()V

    .line 102
    .line 103
    .line 104
    const/4 p1, 0x1

    .line 105
    return p1

    .line 106
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 107
    .line 108
    const-string p2, "\'part\' must be a PackagePropertiesPart instance."

    .line 109
    .line 110
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    throw p1
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

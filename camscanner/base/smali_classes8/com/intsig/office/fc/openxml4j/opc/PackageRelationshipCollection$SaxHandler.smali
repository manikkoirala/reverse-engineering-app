.class Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection$SaxHandler;
.super Ljava/lang/Object;
.source "PackageRelationshipCollection.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/ElementHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SaxHandler"
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection$SaxHandler;->〇080:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public onEnd(Lcom/intsig/office/fc/dom4j/ElementPath;)V
    .locals 6

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/ElementPath;->getCurrent()Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :try_start_0
    const-string v1, "Relationship"

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_4

    .line 16
    .line 17
    const-string v0, "Id"

    .line 18
    .line 19
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "Type"

    .line 28
    .line 29
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    const-string v2, "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"

    .line 38
    .line 39
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_1

    .line 44
    .line 45
    iget-object v2, p0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection$SaxHandler;->〇080:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 46
    .line 47
    iget-boolean v3, v2, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->fCorePropertiesRelationship:Z

    .line 48
    .line 49
    if-nez v3, :cond_0

    .line 50
    .line 51
    const/4 v3, 0x1

    .line 52
    iput-boolean v3, v2, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->fCorePropertiesRelationship:Z

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;

    .line 56
    .line 57
    const-string v1, "OPC Compliance error [M4.1]: there is more than one core properties relationship in the package !"

    .line 58
    .line 59
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;-><init>(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    throw v0

    .line 63
    :cond_1
    :goto_0
    const-string v2, "TargetMode"

    .line 64
    .line 65
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    sget-object v3, Lcom/intsig/office/fc/openxml4j/opc/TargetMode;->INTERNAL:Lcom/intsig/office/fc/openxml4j/opc/TargetMode;

    .line 70
    .line 71
    if-eqz v2, :cond_3

    .line 72
    .line 73
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    const-string v4, "internal"

    .line 82
    .line 83
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    if-eqz v2, :cond_2

    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_2
    sget-object v2, Lcom/intsig/office/fc/openxml4j/opc/TargetMode;->EXTERNAL:Lcom/intsig/office/fc/openxml4j/opc/TargetMode;

    .line 91
    .line 92
    move-object v3, v2

    .line 93
    :cond_3
    :goto_1
    const-string v2, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 94
    .line 95
    :try_start_1
    const-string v4, "Target"

    .line 96
    .line 97
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 98
    .line 99
    .line 100
    move-result-object v4

    .line 101
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Attribute;->getValue()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    invoke-static {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagingURIHelper;->toURI(Ljava/lang/String;)Ljava/net/URI;

    .line 106
    .line 107
    .line 108
    move-result-object v4

    .line 109
    iget-object v5, p0, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection$SaxHandler;->〇080:Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 110
    .line 111
    invoke-virtual {v5, v4, v3, v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->addRelationship(Ljava/net/URI;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 112
    .line 113
    .line 114
    goto :goto_2

    .line 115
    :catch_0
    move-exception v0

    .line 116
    :try_start_2
    invoke-static {}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->〇080()Lcom/intsig/office/fc/util/POILogger;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    sget v3, Lcom/intsig/office/fc/util/POILogger;->ERROR:I

    .line 121
    .line 122
    new-instance v4, Ljava/lang/StringBuilder;

    .line 123
    .line 124
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .line 126
    .line 127
    const-string v5, "Cannot convert "

    .line 128
    .line 129
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    const-string v2, " in a valid relationship URI-> ignored"

    .line 136
    .line 137
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v2

    .line 144
    invoke-virtual {v1, v3, v2, v0}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 145
    .line 146
    .line 147
    goto :goto_2

    .line 148
    :catch_1
    move-exception v0

    .line 149
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 150
    .line 151
    .line 152
    :cond_4
    :goto_2
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->detach()Lcom/intsig/office/fc/dom4j/Node;

    .line 153
    .line 154
    .line 155
    return-void
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public onStart(Lcom/intsig/office/fc/dom4j/ElementPath;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

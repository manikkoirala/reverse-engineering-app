.class public final Lcom/intsig/office/fc/openxml4j/opc/PackagePartCollection;
.super Ljava/util/TreeMap;
.source "PackagePartCollection.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/TreeMap<",
        "Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;",
        "Lcom/intsig/office/fc/openxml4j/opc/PackagePart;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x22e72f7e31cd51bbL


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/util/TreeMap;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-super {p0}, Ljava/util/TreeMap;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public put(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;
    .locals 1

    .line 2
    invoke-virtual {p0, p1}, Ljava/util/AbstractMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    invoke-super {p0, p1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    return-object p1

    .line 4
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;

    const-string p2, "You can\'t add a part with a part name derived from another part ! [M1.11]"

    invoke-direct {p1, p2}, Lcom/intsig/office/fc/openxml4j/exceptions/InvalidOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    check-cast p2, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePartCollection;->put(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    move-result-object p1

    return-object p1
.end method

.method public remove(Ljava/lang/Object;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;
    .locals 0

    .line 2
    invoke-super {p0, p1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    return-object p1
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePartCollection;->remove(Ljava/lang/Object;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    move-result-object p1

    return-object p1
.end method

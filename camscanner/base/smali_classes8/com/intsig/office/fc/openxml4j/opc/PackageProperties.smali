.class public interface abstract Lcom/intsig/office/fc/openxml4j/opc/PackageProperties;
.super Ljava/lang/Object;
.source "PackageProperties.java"


# static fields
.field public static final NAMESPACE_DC:Ljava/lang/String; = "http://purl.org/dc/elements/1.1/"

.field public static final NAMESPACE_DCTERMS:Ljava/lang/String; = "http://purl.org/dc/terms/"


# virtual methods
.method public abstract getCategoryProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getContentStatusProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getContentTypeProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCreatedProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCreatorProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDescriptionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getIdentifierProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getKeywordsProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLanguageProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLastModifiedByProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLastPrintedProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getModifiedProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRevisionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSubjectProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTitleProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVersionProperty()Lcom/intsig/office/fc/openxml4j/util/Nullable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCategoryProperty(Ljava/lang/String;)V
.end method

.method public abstract setContentStatusProperty(Ljava/lang/String;)V
.end method

.method public abstract setContentTypeProperty(Ljava/lang/String;)V
.end method

.method public abstract setCreatedProperty(Lcom/intsig/office/fc/openxml4j/util/Nullable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setCreatedProperty(Ljava/lang/String;)V
.end method

.method public abstract setCreatorProperty(Ljava/lang/String;)V
.end method

.method public abstract setDescriptionProperty(Ljava/lang/String;)V
.end method

.method public abstract setIdentifierProperty(Ljava/lang/String;)V
.end method

.method public abstract setKeywordsProperty(Ljava/lang/String;)V
.end method

.method public abstract setLanguageProperty(Ljava/lang/String;)V
.end method

.method public abstract setLastModifiedByProperty(Ljava/lang/String;)V
.end method

.method public abstract setLastPrintedProperty(Lcom/intsig/office/fc/openxml4j/util/Nullable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setLastPrintedProperty(Ljava/lang/String;)V
.end method

.method public abstract setModifiedProperty(Lcom/intsig/office/fc/openxml4j/util/Nullable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/openxml4j/util/Nullable<",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setModifiedProperty(Ljava/lang/String;)V
.end method

.method public abstract setRevisionProperty(Ljava/lang/String;)V
.end method

.method public abstract setSubjectProperty(Ljava/lang/String;)V
.end method

.method public abstract setTitleProperty(Ljava/lang/String;)V
.end method

.method public abstract setVersionProperty(Ljava/lang/String;)V
.end method

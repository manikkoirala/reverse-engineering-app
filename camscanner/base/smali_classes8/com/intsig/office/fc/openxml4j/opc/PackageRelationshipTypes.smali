.class public interface abstract Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipTypes;
.super Ljava/lang/Object;
.source "PackageRelationshipTypes.java"


# static fields
.field public static final BULLET_NUMBER_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"

.field public static final CHARTSHEET_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chartsheet"

.field public static final CHART_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart"

.field public static final CHART_TYPE:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/chart"

.field public static final COMMENT_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/comments"

.field public static final CORE_DOCUMENT:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

.field public static final CORE_PROPERTIES:Ljava/lang/String; = "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"

.field public static final CORE_PROPERTIES_ECMA376:Ljava/lang/String; = "http://schemas.openxmlformats.org/officedocument/2006/relationships/metadata/core-properties"

.field public static final CUSTOM_PROPERTIES:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties"

.field public static final CUSTOM_XML:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXml"

.field public static final DIAGRAM_DATA:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/diagramData"

.field public static final DIAGRAM_DRAWING:Ljava/lang/String; = "http://schemas.microsoft.com/office/2007/relationships/diagramDrawing"

.field public static final DIAGRAM_TYPE:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/diagram"

.field public static final DIGITAL_SIGNATURE:Ljava/lang/String; = "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/signature"

.field public static final DIGITAL_SIGNATURE_CERTIFICATE:Ljava/lang/String; = "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/certificate"

.field public static final DIGITAL_SIGNATURE_ORIGIN:Ljava/lang/String; = "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/origin"

.field public static final DRAWING_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing"

.field public static final EXTENDED_PROPERTIES:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"

.field public static final FOOTER_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer"

.field public static final HEADER_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/header"

.field public static final HYPERLINK_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink"

.field public static final IMAGE_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

.field public static final LAYOUT_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout"

.field public static final NOTES_SLIDE:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/notesSlide"

.field public static final OLE_TYPE:Ljava/lang/String; = "http://schemas.openxmlformats.org/presentationml/2006/ole"

.field public static final SHAREDSTRINGS_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings"

.field public static final SLIDE_MASTER:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster"

.field public static final SLIDE_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide"

.field public static final STYLE_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"

.field public static final TABLE_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/table"

.field public static final TABLE_STYLE:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/tableStyles"

.field public static final TABLE_TYPE:Ljava/lang/String; = "http://schemas.openxmlformats.org/drawingml/2006/table"

.field public static final THEME_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"

.field public static final THUMBNAIL:Ljava/lang/String; = "http://schemas.openxmlformats.org/package/2006/relationships/metadata/thumbnail"

.field public static final VMLDRAWING_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/vmlDrawing"

.field public static final WORKSHEET_PART:Ljava/lang/String; = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet"

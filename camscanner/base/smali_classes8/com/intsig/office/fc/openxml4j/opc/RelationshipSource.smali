.class public interface abstract Lcom/intsig/office/fc/openxml4j/opc/RelationshipSource;
.super Ljava/lang/Object;
.source "RelationshipSource.java"


# virtual methods
.method public abstract addExternalRelationship(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
.end method

.method public abstract addExternalRelationship(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
.end method

.method public abstract addRelationship(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
.end method

.method public abstract addRelationship(Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;Lcom/intsig/office/fc/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
.end method

.method public abstract clearRelationships()V
.end method

.method public abstract getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;
.end method

.method public abstract getRelationships()Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;,
            Lcom/intsig/office/fc/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation
.end method

.method public abstract getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/openxml4j/exceptions/InvalidFormatException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/intsig/office/fc/openxml4j/exceptions/OpenXML4JException;
        }
    .end annotation
.end method

.method public abstract hasRelationships()Z
.end method

.method public abstract isRelationshipExists(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;)Z
.end method

.method public abstract removeRelationship(Ljava/lang/String;)V
.end method

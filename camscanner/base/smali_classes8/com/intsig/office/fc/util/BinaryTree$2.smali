.class Lcom/intsig/office/fc/util/BinaryTree$2;
.super Ljava/util/AbstractSet;
.source "BinaryTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/office/fc/util/BinaryTree;->keySetByValue()Ljava/util/Set;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/office/fc/util/BinaryTree;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/util/BinaryTree;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/util/BinaryTree$2;->o0:Lcom/intsig/office/fc/util/BinaryTree;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$2;->o0:Lcom/intsig/office/fc/util/BinaryTree;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/util/BinaryTree;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$2;->o0:Lcom/intsig/office/fc/util/BinaryTree;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BinaryTree;->containsKey(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/util/BinaryTree$2$1;

    .line 2
    .line 3
    sget v1, Lcom/intsig/office/fc/util/BinaryTree;->_VALUE:I

    .line 4
    .line 5
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/fc/util/BinaryTree$2$1;-><init>(Lcom/intsig/office/fc/util/BinaryTree$2;I)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$2;->o0:Lcom/intsig/office/fc/util/BinaryTree;

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/office/fc/util/BinaryTree;->_size:I

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BinaryTree;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/util/BinaryTree$2;->o0:Lcom/intsig/office/fc/util/BinaryTree;

    .line 9
    .line 10
    iget p1, p1, Lcom/intsig/office/fc/util/BinaryTree;->_size:I

    .line 11
    .line 12
    if-eq p1, v1, :cond_0

    .line 13
    .line 14
    const/4 p1, 0x1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    :goto_0
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public size()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$2;->o0:Lcom/intsig/office/fc/util/BinaryTree;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/util/BinaryTree;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public final Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;
.super Ljava/lang/Object;
.source "LittleEndianByteArrayOutputStream.java"

# interfaces
.implements Lcom/intsig/office/fc/util/LittleEndianOutput;
.implements Lcom/intsig/office/fc/util/DelayableLittleEndianOutput;


# instance fields
.field private final _buf:[B

.field private final _endIndex:I

.field private _writeIndex:I


# direct methods
.method public constructor <init>([BI)V
    .locals 1

    .line 9
    array-length v0, p1

    sub-int/2addr v0, p2

    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;-><init>([BII)V

    return-void
.end method

.method public constructor <init>([BII)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ")"

    if-ltz p2, :cond_1

    .line 2
    array-length v1, p1

    if-gt p2, v1, :cond_1

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_buf:[B

    .line 4
    iput p2, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    add-int/2addr p3, p2

    .line 5
    iput p3, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_endIndex:I

    if-lt p3, p2, :cond_0

    .line 6
    array-length p2, p1

    if-gt p3, p2, :cond_0

    return-void

    .line 7
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calculated end index ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ") is out of allowable range ("

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, ".."

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 8
    :cond_1
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Specified startOffset ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ") is out of allowable range (0.."

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3
.end method

.method private checkPosition(I)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_endIndex:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 4
    .line 5
    sub-int/2addr v0, v1

    .line 6
    if-gt p1, v0, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 10
    .line 11
    const-string v0, "Buffer overrun"

    .line 12
    .line 13
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    throw p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public createDelayedOutput(I)Lcom/intsig/office/fc/util/LittleEndianOutput;
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->checkPosition(I)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_buf:[B

    .line 7
    .line 8
    iget v2, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 9
    .line 10
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;-><init>([BII)V

    .line 11
    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 14
    .line 15
    add-int/2addr v1, p1

    .line 16
    iput v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getWriteIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public write([B)V
    .locals 4

    .line 1
    array-length v0, p1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->checkPosition(I)V

    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_buf:[B

    iget v2, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    const/4 v3, 0x0

    invoke-static {p1, v3, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4
    iget p1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    return-void
.end method

.method public write([BII)V
    .locals 2

    .line 5
    invoke-direct {p0, p3}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->checkPosition(I)V

    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_buf:[B

    iget v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7
    iget p1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    return-void
.end method

.method public writeByte(I)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->checkPosition(I)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_buf:[B

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 8
    .line 9
    add-int/lit8 v2, v1, 0x1

    .line 10
    .line 11
    iput v2, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 12
    .line 13
    int-to-byte p1, p1

    .line 14
    aput-byte p1, v0, v1

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public writeDouble(D)V
    .locals 0

    .line 1
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 2
    .line 3
    .line 4
    move-result-wide p1

    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->writeLong(J)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public writeInt(I)V
    .locals 4

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->checkPosition(I)V

    .line 3
    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_buf:[B

    .line 8
    .line 9
    add-int/lit8 v2, v0, 0x1

    .line 10
    .line 11
    ushr-int/lit8 v3, p1, 0x0

    .line 12
    .line 13
    and-int/lit16 v3, v3, 0xff

    .line 14
    .line 15
    int-to-byte v3, v3

    .line 16
    aput-byte v3, v1, v0

    .line 17
    .line 18
    add-int/lit8 v0, v2, 0x1

    .line 19
    .line 20
    ushr-int/lit8 v3, p1, 0x8

    .line 21
    .line 22
    and-int/lit16 v3, v3, 0xff

    .line 23
    .line 24
    int-to-byte v3, v3

    .line 25
    aput-byte v3, v1, v2

    .line 26
    .line 27
    add-int/lit8 v2, v0, 0x1

    .line 28
    .line 29
    ushr-int/lit8 v3, p1, 0x10

    .line 30
    .line 31
    and-int/lit16 v3, v3, 0xff

    .line 32
    .line 33
    int-to-byte v3, v3

    .line 34
    aput-byte v3, v1, v0

    .line 35
    .line 36
    add-int/lit8 v0, v2, 0x1

    .line 37
    .line 38
    ushr-int/lit8 p1, p1, 0x18

    .line 39
    .line 40
    and-int/lit16 p1, p1, 0xff

    .line 41
    .line 42
    int-to-byte p1, p1

    .line 43
    aput-byte p1, v1, v2

    .line 44
    .line 45
    iput v0, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public writeLong(J)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    shr-long v0, p1, v0

    .line 3
    .line 4
    long-to-int v1, v0

    .line 5
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->writeInt(I)V

    .line 6
    .line 7
    .line 8
    const/16 v0, 0x20

    .line 9
    .line 10
    shr-long/2addr p1, v0

    .line 11
    long-to-int p2, p1

    .line 12
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->writeInt(I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public writeShort(I)V
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->checkPosition(I)V

    .line 3
    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_buf:[B

    .line 8
    .line 9
    add-int/lit8 v2, v0, 0x1

    .line 10
    .line 11
    ushr-int/lit8 v3, p1, 0x0

    .line 12
    .line 13
    and-int/lit16 v3, v3, 0xff

    .line 14
    .line 15
    int-to-byte v3, v3

    .line 16
    aput-byte v3, v1, v0

    .line 17
    .line 18
    add-int/lit8 v0, v2, 0x1

    .line 19
    .line 20
    ushr-int/lit8 p1, p1, 0x8

    .line 21
    .line 22
    and-int/lit16 p1, p1, 0xff

    .line 23
    .line 24
    int-to-byte p1, p1

    .line 25
    aput-byte p1, v1, v2

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/office/fc/util/LittleEndianByteArrayOutputStream;->_writeIndex:I

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

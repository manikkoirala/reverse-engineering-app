.class public abstract Lcom/intsig/office/fc/util/LZWDecompresser;
.super Ljava/lang/Object;
.source "LZWDecompresser.java"


# instance fields
.field private final codeLengthIncrease:I

.field private final maskMeansCompressed:Z

.field private final positionIsBigEndian:Z


# direct methods
.method protected constructor <init>(ZIZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-boolean p1, p0, Lcom/intsig/office/fc/util/LZWDecompresser;->maskMeansCompressed:Z

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/fc/util/LZWDecompresser;->codeLengthIncrease:I

    .line 7
    .line 8
    iput-boolean p3, p0, Lcom/intsig/office/fc/util/LZWDecompresser;->positionIsBigEndian:Z

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static fromByte(B)I
    .locals 0

    .line 1
    if-ltz p0, :cond_0

    .line 2
    .line 3
    return p0

    .line 4
    :cond_0
    add-int/lit16 p0, p0, 0x100

    .line 5
    .line 6
    return p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static fromInt(I)B
    .locals 1

    .line 1
    const/16 v0, 0x80

    .line 2
    .line 3
    if-ge p0, v0, :cond_0

    .line 4
    .line 5
    int-to-byte p0, p0

    .line 6
    return p0

    .line 7
    :cond_0
    add-int/lit16 p0, p0, -0x100

    .line 8
    .line 9
    int-to-byte p0, p0

    .line 10
    return p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method protected abstract adjustDictionaryOffset(I)I
.end method

.method public decompress(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/util/LZWDecompresser;->populateDictionary([B)I

    move-result v1

    .line 5
    iget v2, p0, Lcom/intsig/office/fc/util/LZWDecompresser;->codeLengthIncrease:I

    add-int/lit8 v2, v2, 0x10

    new-array v2, v2, [B

    .line 6
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_7

    const/4 v5, 0x1

    const/4 v6, 0x1

    :goto_1
    const/16 v7, 0x100

    if-ge v6, v7, :cond_0

    and-int v7, v3, v6

    const/4 v8, 0x0

    if-lez v7, :cond_1

    const/4 v7, 0x1

    goto :goto_2

    :cond_1
    const/4 v7, 0x0

    .line 7
    :goto_2
    iget-boolean v9, p0, Lcom/intsig/office/fc/util/LZWDecompresser;->maskMeansCompressed:Z

    xor-int/2addr v7, v9

    if-eqz v7, :cond_2

    .line 8
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v7

    if-eq v7, v4, :cond_6

    and-int/lit16 v9, v1, 0xfff

    .line 9
    invoke-static {v7}, Lcom/intsig/office/fc/util/LZWDecompresser;->fromInt(I)B

    move-result v10

    aput-byte v10, v0, v9

    add-int/lit8 v1, v1, 0x1

    new-array v9, v5, [B

    .line 10
    invoke-static {v7}, Lcom/intsig/office/fc/util/LZWDecompresser;->fromInt(I)B

    move-result v7

    aput-byte v7, v9, v8

    invoke-virtual {p2, v9}, Ljava/io/OutputStream;->write([B)V

    goto :goto_5

    .line 11
    :cond_2
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v7

    .line 12
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v9

    if-eq v7, v4, :cond_0

    if-ne v9, v4, :cond_3

    goto :goto_0

    :cond_3
    and-int/lit8 v10, v9, 0xf

    .line 13
    iget v11, p0, Lcom/intsig/office/fc/util/LZWDecompresser;->codeLengthIncrease:I

    add-int/2addr v10, v11

    .line 14
    iget-boolean v11, p0, Lcom/intsig/office/fc/util/LZWDecompresser;->positionIsBigEndian:Z

    if-eqz v11, :cond_4

    shl-int/lit8 v7, v7, 0x4

    shr-int/lit8 v9, v9, 0x4

    goto :goto_3

    :cond_4
    and-int/lit16 v9, v9, 0xf0

    shl-int/lit8 v9, v9, 0x4

    :goto_3
    add-int/2addr v7, v9

    .line 15
    invoke-virtual {p0, v7}, Lcom/intsig/office/fc/util/LZWDecompresser;->adjustDictionaryOffset(I)I

    move-result v7

    const/4 v9, 0x0

    :goto_4
    if-ge v9, v10, :cond_5

    add-int v11, v7, v9

    and-int/lit16 v11, v11, 0xfff

    .line 16
    aget-byte v11, v0, v11

    aput-byte v11, v2, v9

    add-int v12, v1, v9

    and-int/lit16 v12, v12, 0xfff

    .line 17
    aput-byte v11, v0, v12

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 18
    :cond_5
    invoke-virtual {p2, v2, v8, v10}, Ljava/io/OutputStream;->write([BII)V

    add-int/2addr v1, v10

    :cond_6
    :goto_5
    shl-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_7
    return-void
.end method

.method public decompress(Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/util/LZWDecompresser;->decompress(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method

.method protected abstract populateDictionary([B)I
.end method

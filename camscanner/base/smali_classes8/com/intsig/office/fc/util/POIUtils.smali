.class public Lcom/intsig/office/fc/util/POIUtils;
.super Ljava/lang/Object;
.source "POIUtils.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static copyNodeRecursively(Lcom/intsig/office/fc/poifs/filesystem/Entry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/poifs/filesystem/Entry;->isDirectoryEntry()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-interface {p0}, Lcom/intsig/office/fc/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;->createDirectory(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;

    .line 16
    .line 17
    invoke-interface {p0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object p0

    .line 21
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Lcom/intsig/office/fc/poifs/filesystem/Entry;

    .line 32
    .line 33
    invoke-static {v0, p1}, Lcom/intsig/office/fc/util/POIUtils;->copyNodeRecursively(Lcom/intsig/office/fc/poifs/filesystem/Entry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    check-cast p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    .line 38
    .line 39
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 40
    .line 41
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;)V

    .line 42
    .line 43
    .line 44
    invoke-interface {p0}, Lcom/intsig/office/fc/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    invoke-interface {p1, p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;->createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->close()V

    .line 52
    .line 53
    .line 54
    :cond_1
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static copyNodes(Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;",
            "Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;->getEntries()Ljava/util/Iterator;

    move-result-object p0

    .line 2
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/poifs/filesystem/Entry;

    .line 4
    invoke-interface {v0}, Lcom/intsig/office/fc/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5
    invoke-static {v0, p1}, Lcom/intsig/office/fc/util/POIUtils;->copyNodeRecursively(Lcom/intsig/office/fc/poifs/filesystem/Entry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static copyNodes(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;",
            "Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object p0

    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object p1

    invoke-static {p0, p1, p2}, Lcom/intsig/office/fc/util/POIUtils;->copyNodes(Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;Ljava/util/List;)V

    return-void
.end method

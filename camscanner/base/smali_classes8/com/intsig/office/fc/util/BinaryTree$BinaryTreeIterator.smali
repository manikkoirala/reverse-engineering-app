.class abstract Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;
.super Ljava/lang/Object;
.source "BinaryTree.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/util/BinaryTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BinaryTreeIterator"
.end annotation


# instance fields
.field private OO:Lcom/intsig/office/fc/util/BinaryTree$Node;

.field private o0:I

.field final synthetic o〇00O:Lcom/intsig/office/fc/util/BinaryTree;

.field private 〇08O〇00〇o:I

.field protected 〇OOo8〇0:Lcom/intsig/office/fc/util/BinaryTree$Node;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/util/BinaryTree;I)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->o〇00O:Lcom/intsig/office/fc/util/BinaryTree;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->〇08O〇00〇o:I

    .line 7
    .line 8
    iget v0, p1, Lcom/intsig/office/fc/util/BinaryTree;->_modifications:I

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->o0:I

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->〇OOo8〇0:Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 14
    .line 15
    iget-object p1, p1, Lcom/intsig/office/fc/util/BinaryTree;->_root:[Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 16
    .line 17
    aget-object p1, p1, p2

    .line 18
    .line 19
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/BinaryTree;->leastNode(Lcom/intsig/office/fc/util/BinaryTree$Node;I)Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    iput-object p1, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->OO:Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->OO:Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;,
            Ljava/util/ConcurrentModificationException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->OO:Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->o〇00O:Lcom/intsig/office/fc/util/BinaryTree;

    .line 6
    .line 7
    iget v1, v1, Lcom/intsig/office/fc/util/BinaryTree;->_modifications:I

    .line 8
    .line 9
    iget v2, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->o0:I

    .line 10
    .line 11
    if-ne v1, v2, :cond_0

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->〇OOo8〇0:Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->〇08O〇00〇o:I

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/BinaryTree;->nextGreater(Lcom/intsig/office/fc/util/BinaryTree$Node;I)Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iput-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->OO:Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->〇080()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0

    .line 28
    :cond_0
    new-instance v0, Ljava/util/ConcurrentModificationException;

    .line 29
    .line 30
    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    .line 31
    .line 32
    .line 33
    throw v0

    .line 34
    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    .line 35
    .line 36
    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    .line 37
    .line 38
    .line 39
    throw v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public remove()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/util/ConcurrentModificationException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->〇OOo8〇0:Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->o〇00O:Lcom/intsig/office/fc/util/BinaryTree;

    .line 6
    .line 7
    iget v2, v1, Lcom/intsig/office/fc/util/BinaryTree;->_modifications:I

    .line 8
    .line 9
    iget v3, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->o0:I

    .line 10
    .line 11
    if-ne v2, v3, :cond_0

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/util/BinaryTree;->doRedBlackDelete(Lcom/intsig/office/fc/util/BinaryTree$Node;)V

    .line 14
    .line 15
    .line 16
    iget v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->o0:I

    .line 17
    .line 18
    add-int/lit8 v0, v0, 0x1

    .line 19
    .line 20
    iput v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->o0:I

    .line 21
    .line 22
    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/util/BinaryTree$BinaryTreeIterator;->〇OOo8〇0:Lcom/intsig/office/fc/util/BinaryTree$Node;

    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    new-instance v0, Ljava/util/ConcurrentModificationException;

    .line 27
    .line 28
    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    .line 29
    .line 30
    .line 31
    throw v0

    .line 32
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    .line 35
    .line 36
    .line 37
    throw v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected abstract 〇080()Ljava/lang/Object;
.end method

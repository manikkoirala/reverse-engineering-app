.class public Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;
.super Ljava/lang/Object;
.source "ChartReader.java"


# static fields
.field private static final AxisPosition_Bottom:S = 0x0s

.field private static final AxisPosition_Left:S = 0x1s

.field private static final AxisPosition_Right:S = 0x2s

.field private static final AxisPosition_Top:S = 0x3s

.field private static final TAG:Ljava/lang/String; = "ChartReader"

.field private static reader:Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;

.field private static final themeIndex:[Ljava/lang/String;

.field private static final tints:[D


# instance fields
.field private chart:Lcom/intsig/office/fc/dom4j/Element;

.field private hasMaxX:Z

.field private hasMaxY:Z

.field private hasMinX:Z

.field private hasMinY:Z

.field private maxX:D

.field private maxY:D

.field private minX:D

.field private minY:D

.field private schemeColor:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private type:S


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const-string v0, "accent1"

    .line 2
    .line 3
    const-string v1, "accent2"

    .line 4
    .line 5
    const-string v2, "accent3"

    .line 6
    .line 7
    const-string v3, "accent4"

    .line 8
    .line 9
    const-string v4, "accent5"

    .line 10
    .line 11
    const-string v5, "accent6"

    .line 12
    .line 13
    filled-new-array/range {v0 .. v5}, [Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->themeIndex:[Ljava/lang/String;

    .line 18
    .line 19
    const/4 v0, 0x6

    .line 20
    new-array v0, v0, [D

    .line 21
    .line 22
    fill-array-data v0, :array_0

    .line 23
    .line 24
    .line 25
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->tints:[D

    .line 26
    .line 27
    new-instance v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;

    .line 28
    .line 29
    invoke-direct {v0}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;-><init>()V

    .line 30
    .line 31
    .line 32
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->reader:Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;

    .line 33
    .line 34
    return-void

    .line 35
    :array_0
    .array-data 8
        -0x4030000000000000L    # -0.25
        0x0
        0x3fd999999999999aL    # 0.4
        0x3fe3333333333333L    # 0.6
        0x3fe999999999999aL    # 0.8
        -0x4020000000000000L    # -0.5
    .end array-data
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private buildAChart(Lcom/intsig/office/fc/dom4j/Element;FB)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move/from16 v2, p2

    .line 6
    .line 7
    move/from16 v3, p3

    .line 8
    .line 9
    const-string v4, "plotArea"

    .line 10
    .line 11
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 12
    .line 13
    .line 14
    move-result-object v4

    .line 15
    const/4 v5, 0x5

    .line 16
    new-array v5, v5, [Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 17
    .line 18
    sget-object v6, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->DIAMOND:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 19
    .line 20
    const/4 v7, 0x0

    .line 21
    aput-object v6, v5, v7

    .line 22
    .line 23
    sget-object v6, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->SQUARE:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 24
    .line 25
    const/4 v8, 0x1

    .line 26
    aput-object v6, v5, v8

    .line 27
    .line 28
    const/4 v6, 0x2

    .line 29
    sget-object v9, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->TRIANGLE:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 30
    .line 31
    aput-object v9, v5, v6

    .line 32
    .line 33
    const/4 v6, 0x3

    .line 34
    sget-object v9, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->X:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 35
    .line 36
    aput-object v9, v5, v6

    .line 37
    .line 38
    const/4 v6, 0x4

    .line 39
    sget-object v9, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->CIRCLE:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 40
    .line 41
    aput-object v9, v5, v6

    .line 42
    .line 43
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getChartInfo(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 44
    .line 45
    .line 46
    iget-short v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 47
    .line 48
    const/16 v9, 0xa

    .line 49
    .line 50
    const/4 v10, 0x0

    .line 51
    packed-switch v6, :pswitch_data_0

    .line 52
    .line 53
    .line 54
    move-object v3, v10

    .line 55
    move-object v4, v3

    .line 56
    move-object v5, v4

    .line 57
    goto/16 :goto_2

    .line 58
    .line 59
    :pswitch_0
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->buildXYMultipleSeriesRenderer(Lcom/intsig/office/fc/dom4j/Element;FB)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 60
    .line 61
    .line 62
    move-result-object v3

    .line 63
    iget-object v4, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 64
    .line 65
    iget-short v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 66
    .line 67
    invoke-direct {v0, v4, v6, v3, v5}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getXYMultipleSeriesDataset(Lcom/intsig/office/fc/dom4j/Element;SLcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 68
    .line 69
    .line 70
    move-result-object v4

    .line 71
    invoke-virtual {v3, v9}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXLabels(I)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v3, v9}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYLabels(I)V

    .line 75
    .line 76
    .line 77
    const/4 v5, 0x0

    .line 78
    :goto_0
    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 79
    .line 80
    .line 81
    move-result v6

    .line 82
    if-ge v5, v6, :cond_0

    .line 83
    .line 84
    invoke-virtual {v3, v5}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 85
    .line 86
    .line 87
    move-result-object v6

    .line 88
    check-cast v6, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;

    .line 89
    .line 90
    invoke-virtual {v6, v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->setFillPoints(Z)V

    .line 91
    .line 92
    .line 93
    add-int/lit8 v5, v5, 0x1

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_0
    invoke-static {v4, v3}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->getScatterChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 97
    .line 98
    .line 99
    move-result-object v5

    .line 100
    goto :goto_1

    .line 101
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->buildDefaultRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    invoke-virtual {v3, v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setZoomEnabled(Z)V

    .line 106
    .line 107
    .line 108
    iget-object v4, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 109
    .line 110
    invoke-virtual {v0, v4, v3}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->buildCategoryDataset(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 111
    .line 112
    .line 113
    move-result-object v4

    .line 114
    invoke-static {v4, v3}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->getPieChart(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 115
    .line 116
    .line 117
    move-result-object v5

    .line 118
    move-object/from16 v16, v10

    .line 119
    .line 120
    move-object v10, v3

    .line 121
    move-object v3, v4

    .line 122
    move-object/from16 v4, v16

    .line 123
    .line 124
    goto :goto_2

    .line 125
    :pswitch_2
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->buildXYMultipleSeriesRenderer(Lcom/intsig/office/fc/dom4j/Element;FB)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 126
    .line 127
    .line 128
    move-result-object v3

    .line 129
    iget-object v4, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 130
    .line 131
    iget-short v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 132
    .line 133
    invoke-direct {v0, v4, v6, v3, v5}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getXYMultipleSeriesDataset(Lcom/intsig/office/fc/dom4j/Element;SLcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 134
    .line 135
    .line 136
    move-result-object v4

    .line 137
    invoke-virtual {v3, v9}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYLabels(I)V

    .line 138
    .line 139
    .line 140
    invoke-static {v4, v3}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->getLineChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 141
    .line 142
    .line 143
    move-result-object v5

    .line 144
    goto :goto_1

    .line 145
    :pswitch_3
    invoke-direct {v0, v4, v2, v3}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->buildXYMultipleSeriesRenderer(Lcom/intsig/office/fc/dom4j/Element;FB)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 146
    .line 147
    .line 148
    move-result-object v3

    .line 149
    iget-object v4, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 150
    .line 151
    iget-short v5, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 152
    .line 153
    invoke-direct {v0, v4, v5, v3}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getXYMultipleSeriesDataset(Lcom/intsig/office/fc/dom4j/Element;SLcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    sget-object v5, Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;->DEFAULT:Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;

    .line 158
    .line 159
    invoke-static {v4, v3, v5}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->getColumnBarChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 160
    .line 161
    .line 162
    move-result-object v5

    .line 163
    :goto_1
    move-object/from16 v16, v10

    .line 164
    .line 165
    move-object v10, v3

    .line 166
    move-object/from16 v3, v16

    .line 167
    .line 168
    :goto_2
    const-string v6, "title"

    .line 169
    .line 170
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 171
    .line 172
    .line 173
    move-result-object v6

    .line 174
    if-eqz v6, :cond_8

    .line 175
    .line 176
    invoke-direct {v0, v6}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getTitle(Lcom/intsig/office/fc/dom4j/Element;)Ljava/util/List;

    .line 177
    .line 178
    .line 179
    move-result-object v6

    .line 180
    const/4 v9, 0x0

    .line 181
    const-string v11, ""

    .line 182
    .line 183
    if-eqz v6, :cond_2

    .line 184
    .line 185
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 186
    .line 187
    .line 188
    move-result v12

    .line 189
    if-lez v12, :cond_2

    .line 190
    .line 191
    const/4 v12, 0x0

    .line 192
    const/4 v13, 0x0

    .line 193
    :goto_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 194
    .line 195
    .line 196
    move-result v14

    .line 197
    if-ge v12, v14, :cond_3

    .line 198
    .line 199
    new-instance v14, Ljava/lang/StringBuilder;

    .line 200
    .line 201
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    .line 206
    .line 207
    invoke-interface {v6, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 208
    .line 209
    .line 210
    move-result-object v11

    .line 211
    check-cast v11, Lcom/intsig/office/ss/model/drawing/TextParagraph;

    .line 212
    .line 213
    invoke-virtual {v11}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->getTextRun()Ljava/lang/String;

    .line 214
    .line 215
    .line 216
    move-result-object v11

    .line 217
    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 221
    .line 222
    .line 223
    move-result-object v11

    .line 224
    invoke-interface {v6, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 225
    .line 226
    .line 227
    move-result-object v14

    .line 228
    check-cast v14, Lcom/intsig/office/ss/model/drawing/TextParagraph;

    .line 229
    .line 230
    invoke-virtual {v14}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->getFont()Lcom/intsig/office/simpletext/font/Font;

    .line 231
    .line 232
    .line 233
    move-result-object v14

    .line 234
    if-eqz v14, :cond_1

    .line 235
    .line 236
    invoke-interface {v6, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 237
    .line 238
    .line 239
    move-result-object v14

    .line 240
    check-cast v14, Lcom/intsig/office/ss/model/drawing/TextParagraph;

    .line 241
    .line 242
    invoke-virtual {v14}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->getFont()Lcom/intsig/office/simpletext/font/Font;

    .line 243
    .line 244
    .line 245
    move-result-object v14

    .line 246
    invoke-virtual {v14}, Lcom/intsig/office/simpletext/font/Font;->getFontSize()D

    .line 247
    .line 248
    .line 249
    move-result-wide v14

    .line 250
    double-to-int v14, v14

    .line 251
    int-to-float v14, v14

    .line 252
    invoke-static {v13, v14}, Ljava/lang/Math;->max(FF)F

    .line 253
    .line 254
    .line 255
    move-result v13

    .line 256
    :cond_1
    add-int/lit8 v12, v12, 0x1

    .line 257
    .line 258
    goto :goto_3

    .line 259
    :cond_2
    const/4 v13, 0x0

    .line 260
    :cond_3
    invoke-virtual {v10, v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowChartTitle(Z)V

    .line 261
    .line 262
    .line 263
    cmpl-float v6, v13, v9

    .line 264
    .line 265
    if-nez v6, :cond_4

    .line 266
    .line 267
    move v13, v2

    .line 268
    :cond_4
    invoke-virtual {v10, v13}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setChartTitleTextSize(F)V

    .line 269
    .line 270
    .line 271
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    .line 272
    .line 273
    .line 274
    move-result v2

    .line 275
    if-nez v2, :cond_7

    .line 276
    .line 277
    if-eqz v4, :cond_6

    .line 278
    .line 279
    invoke-virtual {v4}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    .line 280
    .line 281
    .line 282
    move-result v2

    .line 283
    if-ne v2, v8, :cond_5

    .line 284
    .line 285
    invoke-virtual {v4, v7}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 286
    .line 287
    .line 288
    move-result-object v2

    .line 289
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getTitle()Ljava/lang/String;

    .line 290
    .line 291
    .line 292
    move-result-object v11

    .line 293
    goto :goto_4

    .line 294
    :cond_5
    const-string v11, "Chart Title"

    .line 295
    .line 296
    goto :goto_4

    .line 297
    :cond_6
    if-eqz v3, :cond_7

    .line 298
    .line 299
    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->getTitle()Ljava/lang/String;

    .line 300
    .line 301
    .line 302
    move-result-object v11

    .line 303
    :cond_7
    :goto_4
    invoke-virtual {v10, v11}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setChartTitle(Ljava/lang/String;)V

    .line 304
    .line 305
    .line 306
    goto :goto_5

    .line 307
    :cond_8
    invoke-virtual {v10, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowChartTitle(Z)V

    .line 308
    .line 309
    .line 310
    :goto_5
    const-string v2, "legend"

    .line 311
    .line 312
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 313
    .line 314
    .line 315
    move-result-object v3

    .line 316
    if-eqz v3, :cond_9

    .line 317
    .line 318
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 319
    .line 320
    .line 321
    move-result-object v1

    .line 322
    invoke-direct {v0, v1, v10, v5}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->processLegend(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V

    .line 323
    .line 324
    .line 325
    goto :goto_6

    .line 326
    :cond_9
    invoke-virtual {v10, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowLegend(Z)V

    .line 327
    .line 328
    .line 329
    :goto_6
    if-eqz v5, :cond_a

    .line 330
    .line 331
    iget-object v1, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 332
    .line 333
    const-string v2, "tx1"

    .line 334
    .line 335
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    .line 337
    .line 338
    move-result-object v1

    .line 339
    check-cast v1, Ljava/lang/Integer;

    .line 340
    .line 341
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 342
    .line 343
    .line 344
    move-result v1

    .line 345
    invoke-virtual {v5, v1}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->setCategoryAxisTextColor(I)V

    .line 346
    .line 347
    .line 348
    :cond_a
    return-object v5

    .line 349
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private buildXYMultipleSeriesRenderer(Lcom/intsig/office/fc/dom4j/Element;FB)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;
    .locals 9

    .line 1
    new-instance p3, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 2
    .line 3
    invoke-direct {p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p3, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXTitleTextSize(F)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p3, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYTitleTextSize(F)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p3, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setLabelsTextSize(F)V

    .line 13
    .line 14
    .line 15
    const-string p2, "valAx"

    .line 16
    .line 17
    invoke-interface {p1, p2}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    const-string v0, "catAx"

    .line 22
    .line 23
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .line 31
    .line 32
    const/4 v1, 0x0

    .line 33
    const/4 v2, 0x0

    .line 34
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    if-ge v2, v3, :cond_0

    .line 39
    .line 40
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 45
    .line 46
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    add-int/lit8 v2, v2, 0x1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const/4 p2, 0x0

    .line 53
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    if-ge p2, v2, :cond_1

    .line 58
    .line 59
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    check-cast v2, Lcom/intsig/office/fc/dom4j/Element;

    .line 64
    .line 65
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    add-int/lit8 p2, p2, 0x1

    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_1
    const/4 p1, 0x0

    .line 72
    move-object p2, p1

    .line 73
    const/4 v2, 0x0

    .line 74
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 75
    .line 76
    .line 77
    move-result v3

    .line 78
    if-ge v2, v3, :cond_8

    .line 79
    .line 80
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    move-result-object v3

    .line 84
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 85
    .line 86
    const-string v4, "axPos"

    .line 87
    .line 88
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 89
    .line 90
    .line 91
    move-result-object v3

    .line 92
    if-eqz v3, :cond_7

    .line 93
    .line 94
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getAxisPosition(Lcom/intsig/office/fc/dom4j/Element;)S

    .line 95
    .line 96
    .line 97
    move-result v3

    .line 98
    const-string v4, "minorGridlines"

    .line 99
    .line 100
    const-string v5, "majorGridlines"

    .line 101
    .line 102
    const-string v6, "title"

    .line 103
    .line 104
    const/4 v7, 0x1

    .line 105
    if-eqz v3, :cond_4

    .line 106
    .line 107
    if-eq v3, v7, :cond_2

    .line 108
    .line 109
    const/4 v8, 0x2

    .line 110
    if-eq v3, v8, :cond_2

    .line 111
    .line 112
    const/4 v8, 0x3

    .line 113
    if-eq v3, v8, :cond_4

    .line 114
    .line 115
    goto :goto_3

    .line 116
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 117
    .line 118
    .line 119
    move-result-object p2

    .line 120
    check-cast p2, Lcom/intsig/office/fc/dom4j/Element;

    .line 121
    .line 122
    invoke-interface {p2, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 123
    .line 124
    .line 125
    move-result-object p2

    .line 126
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getTitle(Lcom/intsig/office/fc/dom4j/Element;)Ljava/util/List;

    .line 127
    .line 128
    .line 129
    move-result-object p2

    .line 130
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    move-result-object v6

    .line 134
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 135
    .line 136
    invoke-interface {v6, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 137
    .line 138
    .line 139
    move-result-object v5

    .line 140
    if-nez v5, :cond_3

    .line 141
    .line 142
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 143
    .line 144
    .line 145
    move-result-object v5

    .line 146
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 147
    .line 148
    invoke-interface {v5, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 149
    .line 150
    .line 151
    move-result-object v4

    .line 152
    if-eqz v4, :cond_6

    .line 153
    .line 154
    :cond_3
    invoke-virtual {p3, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowGridH(Z)V

    .line 155
    .line 156
    .line 157
    goto :goto_3

    .line 158
    :cond_4
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 159
    .line 160
    .line 161
    move-result-object p1

    .line 162
    check-cast p1, Lcom/intsig/office/fc/dom4j/Element;

    .line 163
    .line 164
    invoke-interface {p1, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 165
    .line 166
    .line 167
    move-result-object p1

    .line 168
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getTitle(Lcom/intsig/office/fc/dom4j/Element;)Ljava/util/List;

    .line 169
    .line 170
    .line 171
    move-result-object p1

    .line 172
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 173
    .line 174
    .line 175
    move-result-object v6

    .line 176
    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    .line 177
    .line 178
    invoke-interface {v6, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 179
    .line 180
    .line 181
    move-result-object v5

    .line 182
    if-nez v5, :cond_5

    .line 183
    .line 184
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 185
    .line 186
    .line 187
    move-result-object v5

    .line 188
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 189
    .line 190
    invoke-interface {v5, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 191
    .line 192
    .line 193
    move-result-object v4

    .line 194
    if-eqz v4, :cond_6

    .line 195
    .line 196
    :cond_5
    invoke-virtual {p3, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowGridV(Z)V

    .line 197
    .line 198
    .line 199
    :cond_6
    :goto_3
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 200
    .line 201
    .line 202
    move-result-object v4

    .line 203
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 204
    .line 205
    const-string v5, "scaling"

    .line 206
    .line 207
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 208
    .line 209
    .line 210
    move-result-object v4

    .line 211
    invoke-direct {p0, v4, v3}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getMaxMinValue(Lcom/intsig/office/fc/dom4j/Element;S)V

    .line 212
    .line 213
    .line 214
    :cond_7
    add-int/lit8 v2, v2, 0x1

    .line 215
    .line 216
    goto/16 :goto_2

    .line 217
    .line 218
    :cond_8
    if-eqz p1, :cond_9

    .line 219
    .line 220
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 221
    .line 222
    .line 223
    move-result v0

    .line 224
    if-lez v0, :cond_9

    .line 225
    .line 226
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    check-cast p1, Lcom/intsig/office/ss/model/drawing/TextParagraph;

    .line 231
    .line 232
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->getTextRun()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object p1

    .line 236
    invoke-virtual {p3, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXTitle(Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    :cond_9
    if-eqz p2, :cond_a

    .line 240
    .line 241
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 242
    .line 243
    .line 244
    move-result p1

    .line 245
    if-lez p1, :cond_a

    .line 246
    .line 247
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 248
    .line 249
    .line 250
    move-result-object p1

    .line 251
    check-cast p1, Lcom/intsig/office/ss/model/drawing/TextParagraph;

    .line 252
    .line 253
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->getTextRun()Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object p1

    .line 257
    invoke-virtual {p3, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYTitle(Ljava/lang/String;)V

    .line 258
    .line 259
    .line 260
    :cond_a
    iget-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 261
    .line 262
    const-string p2, "tx1"

    .line 263
    .line 264
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    .line 266
    .line 267
    move-result-object p1

    .line 268
    check-cast p1, Ljava/lang/Integer;

    .line 269
    .line 270
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 271
    .line 272
    .line 273
    move-result p1

    .line 274
    invoke-virtual {p3, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setLabelsColor(I)V

    .line 275
    .line 276
    .line 277
    iget-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 278
    .line 279
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    .line 281
    .line 282
    move-result-object p1

    .line 283
    check-cast p1, Ljava/lang/Integer;

    .line 284
    .line 285
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 286
    .line 287
    .line 288
    move-result p1

    .line 289
    invoke-virtual {p3, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setGridColor(I)V

    .line 290
    .line 291
    .line 292
    iget-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 293
    .line 294
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    .line 296
    .line 297
    move-result-object p1

    .line 298
    check-cast p1, Ljava/lang/Integer;

    .line 299
    .line 300
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 301
    .line 302
    .line 303
    move-result p1

    .line 304
    invoke-virtual {p3, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setAxesColor(I)V

    .line 305
    .line 306
    .line 307
    return-object p3
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method private dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getAxisPosition(Lcom/intsig/office/fc/dom4j/Element;)S
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p1, :cond_3

    .line 3
    .line 4
    const-string v1, "val"

    .line 5
    .line 6
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_3

    .line 11
    .line 12
    const-string v1, "b"

    .line 13
    .line 14
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return p1

    .line 22
    :cond_0
    const-string v1, "l"

    .line 23
    .line 24
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    return v0

    .line 31
    :cond_1
    const-string v1, "r"

    .line 32
    .line 33
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-eqz v1, :cond_2

    .line 38
    .line 39
    const/4 p1, 0x2

    .line 40
    return p1

    .line 41
    :cond_2
    const-string v1, "t"

    .line 42
    .line 43
    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-eqz p1, :cond_3

    .line 48
    .line 49
    const/4 p1, 0x3

    .line 50
    return p1

    .line 51
    :cond_3
    return v0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getChartInfo(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 4

    .line 1
    const-string v0, "barChart"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 17
    .line 18
    goto/16 :goto_0

    .line 19
    .line 20
    :cond_0
    const-string v0, "bar3DChart"

    .line 21
    .line 22
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    if-eqz v1, :cond_1

    .line 27
    .line 28
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 33
    .line 34
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 35
    .line 36
    goto/16 :goto_0

    .line 37
    .line 38
    :cond_1
    const-string v0, "pieChart"

    .line 39
    .line 40
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    const/4 v2, 0x3

    .line 45
    if-eqz v1, :cond_2

    .line 46
    .line 47
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 52
    .line 53
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 54
    .line 55
    goto/16 :goto_0

    .line 56
    .line 57
    :cond_2
    const-string v0, "pie3DChart"

    .line 58
    .line 59
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    if-eqz v1, :cond_3

    .line 64
    .line 65
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 70
    .line 71
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 72
    .line 73
    goto/16 :goto_0

    .line 74
    .line 75
    :cond_3
    const-string v0, "ofPieChart"

    .line 76
    .line 77
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    if-eqz v1, :cond_4

    .line 82
    .line 83
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 88
    .line 89
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 90
    .line 91
    goto/16 :goto_0

    .line 92
    .line 93
    :cond_4
    const-string v0, "lineChart"

    .line 94
    .line 95
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    const/4 v2, 0x2

    .line 100
    if-eqz v1, :cond_5

    .line 101
    .line 102
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 107
    .line 108
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 109
    .line 110
    goto/16 :goto_0

    .line 111
    .line 112
    :cond_5
    const-string v0, "line3DChart"

    .line 113
    .line 114
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    if-eqz v1, :cond_6

    .line 119
    .line 120
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 125
    .line 126
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 127
    .line 128
    goto/16 :goto_0

    .line 129
    .line 130
    :cond_6
    const-string v0, "scatterChart"

    .line 131
    .line 132
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    const/4 v2, 0x4

    .line 137
    if-eqz v1, :cond_7

    .line 138
    .line 139
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 144
    .line 145
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 146
    .line 147
    goto/16 :goto_0

    .line 148
    .line 149
    :cond_7
    const-string v0, "areaChart"

    .line 150
    .line 151
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 152
    .line 153
    .line 154
    move-result-object v1

    .line 155
    const/4 v3, 0x0

    .line 156
    if-eqz v1, :cond_8

    .line 157
    .line 158
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 159
    .line 160
    .line 161
    move-result-object p1

    .line 162
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 163
    .line 164
    iput-short v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 165
    .line 166
    goto/16 :goto_0

    .line 167
    .line 168
    :cond_8
    const-string v0, "area3DChart"

    .line 169
    .line 170
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 171
    .line 172
    .line 173
    move-result-object v1

    .line 174
    if-eqz v1, :cond_9

    .line 175
    .line 176
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 177
    .line 178
    .line 179
    move-result-object p1

    .line 180
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 181
    .line 182
    iput-short v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 183
    .line 184
    goto :goto_0

    .line 185
    :cond_9
    const-string v0, "stockChart"

    .line 186
    .line 187
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    if-eqz v1, :cond_a

    .line 192
    .line 193
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 194
    .line 195
    .line 196
    move-result-object p1

    .line 197
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 198
    .line 199
    const/4 p1, 0x5

    .line 200
    iput-short p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 201
    .line 202
    goto :goto_0

    .line 203
    :cond_a
    const-string v0, "surfaceChart"

    .line 204
    .line 205
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 206
    .line 207
    .line 208
    move-result-object v1

    .line 209
    const/4 v3, 0x6

    .line 210
    if-eqz v1, :cond_b

    .line 211
    .line 212
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 213
    .line 214
    .line 215
    move-result-object p1

    .line 216
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 217
    .line 218
    iput-short v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 219
    .line 220
    goto :goto_0

    .line 221
    :cond_b
    const-string v0, "surface3DChart"

    .line 222
    .line 223
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 224
    .line 225
    .line 226
    move-result-object v1

    .line 227
    if-eqz v1, :cond_c

    .line 228
    .line 229
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 230
    .line 231
    .line 232
    move-result-object p1

    .line 233
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 234
    .line 235
    iput-short v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 236
    .line 237
    goto :goto_0

    .line 238
    :cond_c
    const-string v0, "doughnutChart"

    .line 239
    .line 240
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 241
    .line 242
    .line 243
    move-result-object v1

    .line 244
    if-eqz v1, :cond_d

    .line 245
    .line 246
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 247
    .line 248
    .line 249
    move-result-object p1

    .line 250
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 251
    .line 252
    const/4 p1, 0x7

    .line 253
    iput-short p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 254
    .line 255
    goto :goto_0

    .line 256
    :cond_d
    const-string v0, "bubbleChart"

    .line 257
    .line 258
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 259
    .line 260
    .line 261
    move-result-object v1

    .line 262
    if-eqz v1, :cond_e

    .line 263
    .line 264
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 265
    .line 266
    .line 267
    move-result-object p1

    .line 268
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 269
    .line 270
    iput-short v2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 271
    .line 272
    goto :goto_0

    .line 273
    :cond_e
    const-string v0, "radarChart"

    .line 274
    .line 275
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 276
    .line 277
    .line 278
    move-result-object v1

    .line 279
    if-eqz v1, :cond_f

    .line 280
    .line 281
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 282
    .line 283
    .line 284
    move-result-object p1

    .line 285
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 286
    .line 287
    const/16 p1, 0x9

    .line 288
    .line 289
    iput-short p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 290
    .line 291
    :cond_f
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    .line 292
    .line 293
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 294
    .line 295
    .line 296
    const-string v0, "getChartInfo :"

    .line 297
    .line 298
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    .line 300
    .line 301
    iget-short v0, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 302
    .line 303
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 304
    .line 305
    .line 306
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 307
    .line 308
    .line 309
    move-result-object p1

    .line 310
    const-string v0, "ChartReader"

    .line 311
    .line 312
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    .line 314
    .line 315
    return-void
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private getColor(Lcom/intsig/office/fc/dom4j/Element;)I
    .locals 8

    .line 1
    const-string v0, "srgbClr"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/high16 v2, -0x1000000

    .line 8
    .line 9
    const/16 v3, 0x10

    .line 10
    .line 11
    const-string v4, "val"

    .line 12
    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const/4 v1, 0x6

    .line 28
    if-le v0, v1, :cond_0

    .line 29
    .line 30
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    sub-int/2addr v0, v1

    .line 35
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    :cond_0
    invoke-static {p1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    :goto_0
    or-int/2addr p1, v2

    .line 44
    goto/16 :goto_2

    .line 45
    .line 46
    :cond_1
    const-string v0, "schemeClr"

    .line 47
    .line 48
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    if-eqz v1, :cond_4

    .line 53
    .line 54
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    iget-object v0, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 59
    .line 60
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    check-cast v0, Ljava/lang/Integer;

    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    const-string v1, "lumMod"

    .line 75
    .line 76
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    if-eqz v2, :cond_3

    .line 81
    .line 82
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 83
    .line 84
    .line 85
    move-result-object v1

    .line 86
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    int-to-double v1, v1

    .line 95
    const-wide v5, 0x40f86a0000000000L    # 100000.0

    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    div-double/2addr v1, v5

    .line 101
    const-string v3, "lumOff"

    .line 102
    .line 103
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 104
    .line 105
    .line 106
    move-result-object v7

    .line 107
    if-eqz v7, :cond_2

    .line 108
    .line 109
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    int-to-double v1, p1

    .line 122
    div-double/2addr v1, v5

    .line 123
    goto :goto_1

    .line 124
    :cond_2
    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    .line 125
    .line 126
    sub-double/2addr v1, v3

    .line 127
    :goto_1
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 132
    .line 133
    .line 134
    move-result p1

    .line 135
    goto :goto_2

    .line 136
    :cond_3
    move p1, v0

    .line 137
    goto :goto_2

    .line 138
    :cond_4
    const-string v0, "sysClr"

    .line 139
    .line 140
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    if-eqz v1, :cond_5

    .line 145
    .line 146
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    const-string v0, "lastClr"

    .line 151
    .line 152
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object p1

    .line 156
    invoke-static {p1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    .line 157
    .line 158
    .line 159
    move-result p1

    .line 160
    goto :goto_0

    .line 161
    :cond_5
    const/4 p1, -0x1

    .line 162
    :goto_2
    return p1
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getMaxMinValue(Lcom/intsig/office/fc/dom4j/Element;S)V
    .locals 8

    .line 1
    const-string v0, ", "

    .line 2
    .line 3
    const-string v1, "ChartReader"

    .line 4
    .line 5
    const-string v2, "min"

    .line 6
    .line 7
    const/4 v3, 0x1

    .line 8
    const-string v4, "val"

    .line 9
    .line 10
    const-string v5, "max"

    .line 11
    .line 12
    if-eqz p2, :cond_2

    .line 13
    .line 14
    if-eq p2, v3, :cond_0

    .line 15
    .line 16
    const/4 v6, 0x2

    .line 17
    if-eq p2, v6, :cond_0

    .line 18
    .line 19
    const/4 v6, 0x3

    .line 20
    if-eq p2, v6, :cond_2

    .line 21
    .line 22
    goto/16 :goto_0

    .line 23
    .line 24
    :cond_0
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 25
    .line 26
    .line 27
    move-result-object p2

    .line 28
    if-eqz p2, :cond_1

    .line 29
    .line 30
    iput-boolean v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMaxY:Z

    .line 31
    .line 32
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    invoke-interface {p2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 41
    .line 42
    .line 43
    move-result-wide v6

    .line 44
    iput-wide v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxY:D

    .line 45
    .line 46
    new-instance p2, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string v6, "left or right maxY: "

    .line 52
    .line 53
    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    iget-wide v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxY:D

    .line 57
    .line 58
    invoke-virtual {p2, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-interface {v0, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object p2

    .line 79
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    :cond_1
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 83
    .line 84
    .line 85
    move-result-object p2

    .line 86
    if-eqz p2, :cond_4

    .line 87
    .line 88
    iput-boolean v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMinY:Z

    .line 89
    .line 90
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 99
    .line 100
    .line 101
    move-result-wide p1

    .line 102
    iput-wide p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minY:D

    .line 103
    .line 104
    goto :goto_0

    .line 105
    :cond_2
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    if-eqz p2, :cond_3

    .line 110
    .line 111
    iput-boolean v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMaxX:Z

    .line 112
    .line 113
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    invoke-interface {p2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p2

    .line 121
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 122
    .line 123
    .line 124
    move-result-wide v6

    .line 125
    iput-wide v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxX:D

    .line 126
    .line 127
    new-instance p2, Ljava/lang/StringBuilder;

    .line 128
    .line 129
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .line 131
    .line 132
    const-string v6, "top or bottom maxX: "

    .line 133
    .line 134
    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    iget-wide v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxX:D

    .line 138
    .line 139
    invoke-virtual {p2, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    invoke-interface {v0, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    .line 155
    .line 156
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object p2

    .line 160
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    :cond_3
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 164
    .line 165
    .line 166
    move-result-object p2

    .line 167
    if-eqz p2, :cond_4

    .line 168
    .line 169
    iput-boolean v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMinX:Z

    .line 170
    .line 171
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 180
    .line 181
    .line 182
    move-result-wide p1

    .line 183
    iput-wide p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minX:D

    .line 184
    .line 185
    :cond_4
    :goto_0
    return-void
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private getSeries(Lcom/intsig/office/fc/dom4j/Element;S)Ljava/lang/Object;
    .locals 9

    .line 1
    const-string v0, "tx"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getSeriesTitle(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const-string v2, "val"

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getSeriesTitle(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v1, "Series "

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v1, "order"

    .line 35
    .line 36
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    add-int/lit8 v1, v1, 0x1

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    :goto_0
    const/4 v1, 0x4

    .line 58
    const-string v3, "v"

    .line 59
    .line 60
    const-string v4, "pt"

    .line 61
    .line 62
    const-string v5, "numLit"

    .line 63
    .line 64
    const/4 v6, 0x0

    .line 65
    const-string v7, "numCache"

    .line 66
    .line 67
    const-string v8, "numRef"

    .line 68
    .line 69
    if-eq p2, v1, :cond_4

    .line 70
    .line 71
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 72
    .line 73
    .line 74
    move-result-object p2

    .line 75
    if-eqz p2, :cond_b

    .line 76
    .line 77
    new-instance p2, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 78
    .line 79
    invoke-direct {p2, v0}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-interface {v0, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    if-eqz v0, :cond_1

    .line 91
    .line 92
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    invoke-interface {v0, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    invoke-interface {v0, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 101
    .line 102
    .line 103
    move-result-object v6

    .line 104
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-interface {v0, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    invoke-interface {v0, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    const-string v1, "formatCode"

    .line 117
    .line 118
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    if-eqz v0, :cond_2

    .line 123
    .line 124
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 125
    .line 126
    .line 127
    move-result-object p1

    .line 128
    invoke-interface {p1, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    invoke-interface {p1, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object p1

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    .line 145
    .line 146
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .line 148
    .line 149
    const-string v1, "formatCode:"

    .line 150
    .line 151
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    const-string v1, "ChartReader"

    .line 162
    .line 163
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {p2, p1}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->setFormatCode(Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    goto :goto_1

    .line 170
    :cond_1
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 171
    .line 172
    .line 173
    move-result-object v0

    .line 174
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    if-eqz v0, :cond_2

    .line 179
    .line 180
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 181
    .line 182
    .line 183
    move-result-object p1

    .line 184
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 185
    .line 186
    .line 187
    move-result-object v6

    .line 188
    :cond_2
    :goto_1
    invoke-interface {v6, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 189
    .line 190
    .line 191
    move-result-object p1

    .line 192
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 193
    .line 194
    .line 195
    move-result-object p1

    .line 196
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 197
    .line 198
    .line 199
    move-result v0

    .line 200
    if-eqz v0, :cond_3

    .line 201
    .line 202
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 207
    .line 208
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 209
    .line 210
    .line 211
    move-result-object v0

    .line 212
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v0

    .line 216
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 217
    .line 218
    .line 219
    move-result-wide v0

    .line 220
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(D)V

    .line 221
    .line 222
    .line 223
    goto :goto_2

    .line 224
    :cond_3
    return-object p2

    .line 225
    :cond_4
    const-string p2, "xVal"

    .line 226
    .line 227
    invoke-interface {p1, p2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 228
    .line 229
    .line 230
    move-result-object v1

    .line 231
    if-eqz v1, :cond_5

    .line 232
    .line 233
    invoke-interface {p1, p2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 234
    .line 235
    .line 236
    move-result-object v1

    .line 237
    invoke-interface {v1, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 238
    .line 239
    .line 240
    move-result-object v1

    .line 241
    if-eqz v1, :cond_5

    .line 242
    .line 243
    invoke-interface {p1, p2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 244
    .line 245
    .line 246
    move-result-object p2

    .line 247
    invoke-interface {p2, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 248
    .line 249
    .line 250
    move-result-object p2

    .line 251
    invoke-interface {p2, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 252
    .line 253
    .line 254
    move-result-object p2

    .line 255
    goto :goto_3

    .line 256
    :cond_5
    move-object p2, v6

    .line 257
    :goto_3
    const-string v1, "yVal"

    .line 258
    .line 259
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 260
    .line 261
    .line 262
    move-result-object v2

    .line 263
    if-eqz v2, :cond_7

    .line 264
    .line 265
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 266
    .line 267
    .line 268
    move-result-object v2

    .line 269
    invoke-interface {v2, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 270
    .line 271
    .line 272
    move-result-object v2

    .line 273
    if-eqz v2, :cond_6

    .line 274
    .line 275
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 276
    .line 277
    .line 278
    move-result-object p1

    .line 279
    invoke-interface {p1, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 280
    .line 281
    .line 282
    move-result-object p1

    .line 283
    invoke-interface {p1, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 284
    .line 285
    .line 286
    move-result-object p1

    .line 287
    goto :goto_4

    .line 288
    :cond_6
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 289
    .line 290
    .line 291
    move-result-object v2

    .line 292
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 293
    .line 294
    .line 295
    move-result-object v2

    .line 296
    if-eqz v2, :cond_7

    .line 297
    .line 298
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 299
    .line 300
    .line 301
    move-result-object p1

    .line 302
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 303
    .line 304
    .line 305
    move-result-object p1

    .line 306
    goto :goto_4

    .line 307
    :cond_7
    move-object p1, v6

    .line 308
    :goto_4
    if-eqz p2, :cond_9

    .line 309
    .line 310
    if-eqz p1, :cond_9

    .line 311
    .line 312
    new-instance v1, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 313
    .line 314
    invoke-direct {v1, v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .line 315
    .line 316
    .line 317
    invoke-interface {p2, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 318
    .line 319
    .line 320
    move-result-object p2

    .line 321
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 322
    .line 323
    .line 324
    move-result-object p2

    .line 325
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 326
    .line 327
    .line 328
    move-result-object p1

    .line 329
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 330
    .line 331
    .line 332
    move-result-object p1

    .line 333
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 334
    .line 335
    .line 336
    move-result v0

    .line 337
    if-eqz v0, :cond_8

    .line 338
    .line 339
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 340
    .line 341
    .line 342
    move-result v0

    .line 343
    if-eqz v0, :cond_8

    .line 344
    .line 345
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 346
    .line 347
    .line 348
    move-result-object v0

    .line 349
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 350
    .line 351
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 352
    .line 353
    .line 354
    move-result-object v2

    .line 355
    check-cast v2, Lcom/intsig/office/fc/dom4j/Element;

    .line 356
    .line 357
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 358
    .line 359
    .line 360
    move-result-object v0

    .line 361
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 362
    .line 363
    .line 364
    move-result-object v0

    .line 365
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 366
    .line 367
    .line 368
    move-result-wide v4

    .line 369
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 370
    .line 371
    .line 372
    move-result-object v0

    .line 373
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 374
    .line 375
    .line 376
    move-result-object v0

    .line 377
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 378
    .line 379
    .line 380
    move-result-wide v6

    .line 381
    invoke-virtual {v1, v4, v5, v6, v7}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->add(DD)V

    .line 382
    .line 383
    .line 384
    goto :goto_5

    .line 385
    :cond_8
    return-object v1

    .line 386
    :cond_9
    if-eqz p1, :cond_b

    .line 387
    .line 388
    new-instance p2, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 389
    .line 390
    invoke-direct {p2, v0}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 391
    .line 392
    .line 393
    invoke-interface {p1, v4}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 394
    .line 395
    .line 396
    move-result-object p1

    .line 397
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 398
    .line 399
    .line 400
    move-result-object p1

    .line 401
    :goto_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 402
    .line 403
    .line 404
    move-result v0

    .line 405
    if-eqz v0, :cond_a

    .line 406
    .line 407
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 408
    .line 409
    .line 410
    move-result-object v0

    .line 411
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 412
    .line 413
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 414
    .line 415
    .line 416
    move-result-object v0

    .line 417
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 418
    .line 419
    .line 420
    move-result-object v0

    .line 421
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 422
    .line 423
    .line 424
    move-result-wide v0

    .line 425
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(D)V

    .line 426
    .line 427
    .line 428
    goto :goto_6

    .line 429
    :cond_a
    return-object p2

    .line 430
    :cond_b
    return-object v6
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private getSeriesTitle(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return-object v0

    .line 5
    :cond_0
    const-string v1, "strRef"

    .line 6
    .line 7
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    const-string v3, "v"

    .line 12
    .line 13
    if-eqz v2, :cond_1

    .line 14
    .line 15
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    const-string v1, "strCache"

    .line 20
    .line 21
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    const-string v1, "pt"

    .line 26
    .line 27
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    if-eqz v2, :cond_2

    .line 32
    .line 33
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    return-object p1

    .line 46
    :cond_1
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    if-eqz v1, :cond_2

    .line 51
    .line 52
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    return-object p1

    .line 61
    :cond_2
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getTextSize(Lcom/intsig/office/fc/dom4j/Element;)F
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "p"

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    const-string v0, "pPr"

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    const-string v0, "defRPr"

    .line 20
    .line 21
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    if-eqz p1, :cond_0

    .line 26
    .line 27
    const-string v0, "sz"

    .line 28
    .line 29
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    if-eqz p1, :cond_0

    .line 34
    .line 35
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-lez v0, :cond_0

    .line 40
    .line 41
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    int-to-float p1, p1

    .line 46
    const/high16 v0, 0x42c80000    # 100.0f

    .line 47
    .line 48
    div-float/2addr p1, v0

    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const/high16 p1, 0x41400000    # 12.0f

    .line 51
    .line 52
    :goto_0
    return p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getTitle(Lcom/intsig/office/fc/dom4j/Element;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/dom4j/Element;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/ss/model/drawing/TextParagraph;",
            ">;"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    const-string v0, "tx"

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-eqz v1, :cond_4

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const-string v2, "rich"

    .line 16
    .line 17
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    if-eqz v1, :cond_4

    .line 22
    .line 23
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    const-string v0, "bodyPr"

    .line 32
    .line 33
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    const/4 v1, -0x1

    .line 38
    if-eqz v0, :cond_0

    .line 39
    .line 40
    const-string v2, "anchor"

    .line 41
    .line 42
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-static {v0}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getVerticalByString(Ljava/lang/String;)S

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    goto :goto_0

    .line 51
    :cond_0
    const/4 v0, -0x1

    .line 52
    :goto_0
    const-string v2, "p"

    .line 53
    .line 54
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    new-instance v2, Ljava/util/ArrayList;

    .line 63
    .line 64
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .line 66
    .line 67
    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    if-eqz v3, :cond_3

    .line 72
    .line 73
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v3

    .line 77
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 78
    .line 79
    invoke-static {v3}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getTextParagraph(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/TextParagraph;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    if-eqz v3, :cond_1

    .line 84
    .line 85
    if-le v0, v1, :cond_2

    .line 86
    .line 87
    invoke-virtual {v3, v0}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->setVerticalAlign(S)V

    .line 88
    .line 89
    .line 90
    :cond_2
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_3
    return-object v2

    .line 95
    :cond_4
    const/4 p1, 0x0

    .line 96
    return-object p1
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getXYMultipleSeriesDataset(Lcom/intsig/office/fc/dom4j/Element;SLcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getXYMultipleSeriesDataset(Lcom/intsig/office/fc/dom4j/Element;SLcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    move-result-object p1

    return-object p1
.end method

.method private getXYMultipleSeriesDataset(Lcom/intsig/office/fc/dom4j/Element;SLcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;
    .locals 10

    .line 2
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    invoke-direct {v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    const-string v1, "ser"

    .line 3
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v5, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_5

    .line 5
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    invoke-direct {p0, p3, v6, p4}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->setSeriesRendererProp(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/fc/dom4j/Element;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)V

    .line 6
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/intsig/office/fc/dom4j/Element;

    invoke-direct {p0, v6, p2}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getSeries(Lcom/intsig/office/fc/dom4j/Element;S)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_0

    return-object v2

    .line 7
    :cond_0
    instance-of v7, v6, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    if-eqz v7, :cond_1

    .line 8
    check-cast v6, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    invoke-virtual {v6}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->getFormatCode()Ljava/lang/String;

    move-result-object v5

    .line 9
    invoke-virtual {v6}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->toXYSeries()Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;)V

    goto :goto_1

    .line 10
    :cond_1
    instance-of v7, v6, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    if-eqz v7, :cond_2

    .line 11
    check-cast v6, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    invoke-virtual {v0, v6}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;)V

    .line 12
    :cond_2
    :goto_1
    iget-boolean v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMaxY:Z

    if-nez v6, :cond_3

    .line 13
    iget-wide v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxY:D

    invoke-virtual {v0, v4}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object v8

    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMaxY()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxY:D

    .line 14
    :cond_3
    iget-boolean v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMinY:Z

    if-nez v6, :cond_4

    .line 15
    iget-wide v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minY:D

    invoke-virtual {v0, v4}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object v8

    invoke-virtual {v8}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMinY()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minY:D

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    const-wide v1, 0x41dfffffffc00000L    # 2.147483647E9

    const-wide/high16 v6, -0x3e20000000000000L    # -2.147483648E9

    .line 16
    :goto_2
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    move-result p1

    if-ge v3, p1, :cond_6

    .line 17
    invoke-virtual {v0, v3}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object p1

    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMinX()D

    move-result-wide v8

    invoke-static {v1, v2, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    .line 18
    invoke-virtual {v0, v3}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    move-result-object p1

    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMaxX()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 19
    :cond_6
    iget-boolean p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMinX:Z

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    const/4 p4, 0x4

    if-eqz p1, :cond_7

    .line 20
    iget-wide v1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minX:D

    invoke-virtual {p3, v1, v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    goto :goto_3

    :cond_7
    if-eq p2, p4, :cond_8

    .line 21
    invoke-virtual {p3, v3, v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    goto :goto_3

    .line 22
    :cond_8
    invoke-virtual {p3, v1, v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 23
    :goto_3
    iget-boolean p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMaxX:Z

    if-eqz p1, :cond_9

    .line 24
    iget-wide p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxX:D

    invoke-virtual {p3, p1, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    goto :goto_4

    :cond_9
    if-eq p2, p4, :cond_a

    add-double/2addr v6, v3

    .line 25
    invoke-virtual {p3, v6, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    goto :goto_4

    .line 26
    :cond_a
    invoke-virtual {p3, v6, v7}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 27
    :goto_4
    iget-wide p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minY:D

    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    sub-double/2addr p1, v1

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    const-wide v1, 0x3fb99999a0000000L    # 0.10000000149011612

    const-wide/16 v3, 0x0

    cmpg-double p4, p1, v1

    if-gez p4, :cond_b

    .line 28
    iput-wide v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minY:D

    .line 29
    :cond_b
    iget-wide p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxY:D

    const-wide/16 v1, 0x1

    sub-double/2addr p1, v1

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    const-wide v1, 0x3f50624de0000000L    # 0.0010000000474974513

    cmpg-double p4, p1, v1

    if-gez p4, :cond_c

    const-string p1, "ChartReader"

    const-string p2, "set maxY = 0"

    .line 30
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iput-wide v3, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxY:D

    .line 32
    :cond_c
    iget-wide p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minY:D

    invoke-virtual {p3, p1, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 33
    iget-wide p1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxY:D

    invoke-virtual {p3, p1, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 34
    invoke-virtual {p3, v5}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setFormatCode(Ljava/lang/String;)V

    return-object v0
.end method

.method public static instance()Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->reader:Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private processLegend(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    if-eqz p2, :cond_4

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    invoke-virtual {p2, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowLegend(Z)V

    .line 7
    .line 8
    .line 9
    const-string v1, "legendPos"

    .line 10
    .line 11
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    if-eqz v2, :cond_2

    .line 16
    .line 17
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const-string v2, "val"

    .line 22
    .line 23
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const-string v2, "l"

    .line 28
    .line 29
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    if-eqz v2, :cond_0

    .line 34
    .line 35
    const/4 v0, 0x0

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const-string v2, "t"

    .line 38
    .line 39
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    const-string v0, "b"

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    if-eqz v0, :cond_2

    .line 53
    .line 54
    const/4 v0, 0x3

    .line 55
    goto :goto_0

    .line 56
    :cond_2
    const/4 v0, 0x2

    .line 57
    :goto_0
    invoke-virtual {p3, v0}, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;->setLegendPosition(B)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getDefaultFontSize()F

    .line 61
    .line 62
    .line 63
    move-result p3

    .line 64
    const-string v0, "txPr"

    .line 65
    .line 66
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    if-eqz v1, :cond_3

    .line 71
    .line 72
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getTextSize(Lcom/intsig/office/fc/dom4j/Element;)F

    .line 77
    .line 78
    .line 79
    move-result p3

    .line 80
    :cond_3
    invoke-virtual {p2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setLegendTextSize(F)V

    .line 81
    .line 82
    .line 83
    :cond_4
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private setSeriesRendererProp(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/fc/dom4j/Element;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)V
    .locals 10

    .line 1
    const-string v0, "ChartReader"

    .line 2
    .line 3
    const-string v1, "order"

    .line 4
    .line 5
    invoke-interface {p2, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const-string v2, "val"

    .line 10
    .line 11
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    const-string v3, "spPr"

    .line 20
    .line 21
    invoke-interface {p2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    const/4 v4, 0x0

    .line 26
    if-eqz v3, :cond_2

    .line 27
    .line 28
    const-string v5, "solidFill"

    .line 29
    .line 30
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v6

    .line 34
    const-string v7, "schemeClr"

    .line 35
    .line 36
    if-eqz v6, :cond_0

    .line 37
    .line 38
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 39
    .line 40
    .line 41
    move-result-object v6

    .line 42
    invoke-interface {v6, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 43
    .line 44
    .line 45
    move-result-object v6

    .line 46
    if-eqz v6, :cond_0

    .line 47
    .line 48
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    invoke-interface {v3, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    :goto_0
    move-object v3, v4

    .line 61
    goto :goto_1

    .line 62
    :cond_0
    const-string v6, "ln"

    .line 63
    .line 64
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 65
    .line 66
    .line 67
    move-result-object v8

    .line 68
    if-eqz v8, :cond_2

    .line 69
    .line 70
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 71
    .line 72
    .line 73
    move-result-object v8

    .line 74
    invoke-interface {v8, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 75
    .line 76
    .line 77
    move-result-object v8

    .line 78
    if-eqz v8, :cond_2

    .line 79
    .line 80
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 81
    .line 82
    .line 83
    move-result-object v3

    .line 84
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 85
    .line 86
    .line 87
    move-result-object v3

    .line 88
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 89
    .line 90
    .line 91
    move-result-object v5

    .line 92
    if-eqz v5, :cond_1

    .line 93
    .line 94
    invoke-interface {v3, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 95
    .line 96
    .line 97
    move-result-object v3

    .line 98
    invoke-interface {v3, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    goto :goto_0

    .line 103
    :cond_1
    const-string v5, "srgbClr"

    .line 104
    .line 105
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 106
    .line 107
    .line 108
    move-result-object v6

    .line 109
    if-eqz v6, :cond_2

    .line 110
    .line 111
    invoke-interface {v3, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 112
    .line 113
    .line 114
    move-result-object v3

    .line 115
    invoke-interface {v3, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v2

    .line 119
    move-object v3, v2

    .line 120
    move-object v2, v4

    .line 121
    goto :goto_1

    .line 122
    :cond_2
    move-object v2, v4

    .line 123
    move-object v3, v2

    .line 124
    :goto_1
    new-instance v5, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;

    .line 125
    .line 126
    invoke-direct {v5}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;-><init>()V

    .line 127
    .line 128
    .line 129
    const/4 v6, 0x0

    .line 130
    :try_start_0
    iget-object v7, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 131
    .line 132
    sget-object v8, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->themeIndex:[Ljava/lang/String;

    .line 133
    .line 134
    aget-object v9, v8, v6

    .line 135
    .line 136
    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    .line 138
    .line 139
    move-result-object v7

    .line 140
    check-cast v7, Ljava/lang/Integer;

    .line 141
    .line 142
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    .line 143
    .line 144
    .line 145
    move-result v6

    .line 146
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 147
    .line 148
    .line 149
    move-result-object v7

    .line 150
    sget-object v9, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->tints:[D

    .line 151
    .line 152
    array-length v8, v8

    .line 153
    div-int v8, v1, v8

    .line 154
    .line 155
    aget-wide v8, v9, v8

    .line 156
    .line 157
    invoke-virtual {v7, v6, v8, v9}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 158
    .line 159
    .line 160
    move-result v6

    .line 161
    if-eqz v2, :cond_3

    .line 162
    .line 163
    iget-object v7, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 164
    .line 165
    if-eqz v7, :cond_3

    .line 166
    .line 167
    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    .line 169
    .line 170
    move-result-object v2

    .line 171
    check-cast v2, Ljava/lang/Integer;

    .line 172
    .line 173
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 174
    .line 175
    .line 176
    move-result v2

    .line 177
    invoke-virtual {v5, v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 178
    .line 179
    .line 180
    goto :goto_2

    .line 181
    :cond_3
    if-eqz v3, :cond_4

    .line 182
    .line 183
    new-instance v2, Ljava/lang/StringBuilder;

    .line 184
    .line 185
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .line 187
    .line 188
    const-string v7, "colorString:"

    .line 189
    .line 190
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object v2

    .line 200
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    new-instance v2, Ljava/lang/StringBuilder;

    .line 204
    .line 205
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    .line 207
    .line 208
    const-string v7, "#"

    .line 209
    .line 210
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    .line 215
    .line 216
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object v2

    .line 220
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 221
    .line 222
    .line 223
    move-result v2

    .line 224
    invoke-virtual {v5, v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 225
    .line 226
    .line 227
    goto :goto_2

    .line 228
    :cond_4
    invoke-virtual {v5, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    .line 230
    .line 231
    goto :goto_2

    .line 232
    :catch_0
    move-exception v2

    .line 233
    new-instance v3, Ljava/lang/StringBuilder;

    .line 234
    .line 235
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    .line 237
    .line 238
    const-string v7, "setSeriesRendererProp color error:"

    .line 239
    .line 240
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    .line 242
    .line 243
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 244
    .line 245
    .line 246
    move-result-object v2

    .line 247
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    .line 249
    .line 250
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 251
    .line 252
    .line 253
    move-result-object v2

    .line 254
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    .line 256
    .line 257
    invoke-virtual {v5, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 258
    .line 259
    .line 260
    :goto_2
    const/4 v0, 0x1

    .line 261
    if-eqz p3, :cond_5

    .line 262
    .line 263
    array-length v2, p3

    .line 264
    if-lez v2, :cond_5

    .line 265
    .line 266
    array-length v2, p3

    .line 267
    sub-int/2addr v2, v0

    .line 268
    rem-int/2addr v1, v2

    .line 269
    aget-object p3, p3, v1

    .line 270
    .line 271
    invoke-virtual {v5, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->setPointStyle(Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)V

    .line 272
    .line 273
    .line 274
    :cond_5
    invoke-virtual {p1, v5}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->addSeriesRenderer(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)V

    .line 275
    .line 276
    .line 277
    const-string p3, ""

    .line 278
    .line 279
    invoke-virtual {p1, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXFormatCode(Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    const-string p3, "cat"

    .line 283
    .line 284
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 285
    .line 286
    .line 287
    move-result-object v1

    .line 288
    const-string v2, "strCache"

    .line 289
    .line 290
    const-string v3, "strRef"

    .line 291
    .line 292
    if-eqz v1, :cond_7

    .line 293
    .line 294
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 295
    .line 296
    .line 297
    move-result-object v1

    .line 298
    const-string v5, "numRef"

    .line 299
    .line 300
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 301
    .line 302
    .line 303
    move-result-object v1

    .line 304
    if-eqz v1, :cond_6

    .line 305
    .line 306
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 307
    .line 308
    .line 309
    move-result-object p2

    .line 310
    invoke-interface {p2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 311
    .line 312
    .line 313
    move-result-object p2

    .line 314
    const-string p3, "numCache"

    .line 315
    .line 316
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 317
    .line 318
    .line 319
    move-result-object v4

    .line 320
    const-string p2, "formatCode"

    .line 321
    .line 322
    invoke-interface {v4, p2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 323
    .line 324
    .line 325
    move-result-object p3

    .line 326
    if-eqz p3, :cond_8

    .line 327
    .line 328
    invoke-interface {v4, p2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 329
    .line 330
    .line 331
    move-result-object p2

    .line 332
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 333
    .line 334
    .line 335
    move-result-object p2

    .line 336
    invoke-virtual {p1, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXFormatCode(Ljava/lang/String;)V

    .line 337
    .line 338
    .line 339
    goto :goto_3

    .line 340
    :cond_6
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 341
    .line 342
    .line 343
    move-result-object v1

    .line 344
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 345
    .line 346
    .line 347
    move-result-object v1

    .line 348
    if-eqz v1, :cond_8

    .line 349
    .line 350
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 351
    .line 352
    .line 353
    move-result-object p2

    .line 354
    invoke-interface {p2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 355
    .line 356
    .line 357
    move-result-object p2

    .line 358
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 359
    .line 360
    .line 361
    move-result-object v4

    .line 362
    goto :goto_3

    .line 363
    :cond_7
    const-string p3, "xVal"

    .line 364
    .line 365
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 366
    .line 367
    .line 368
    move-result-object v1

    .line 369
    if-eqz v1, :cond_8

    .line 370
    .line 371
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 372
    .line 373
    .line 374
    move-result-object v1

    .line 375
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 376
    .line 377
    .line 378
    move-result-object v1

    .line 379
    if-eqz v1, :cond_8

    .line 380
    .line 381
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 382
    .line 383
    .line 384
    move-result-object p2

    .line 385
    invoke-interface {p2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 386
    .line 387
    .line 388
    move-result-object p2

    .line 389
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 390
    .line 391
    .line 392
    move-result-object v4

    .line 393
    :cond_8
    :goto_3
    if-eqz v4, :cond_a

    .line 394
    .line 395
    const-string p2, "pt"

    .line 396
    .line 397
    invoke-interface {v4, p2}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 398
    .line 399
    .line 400
    move-result-object p2

    .line 401
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 402
    .line 403
    .line 404
    move-result-object p2

    .line 405
    :goto_4
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 406
    .line 407
    .line 408
    move-result p3

    .line 409
    if-eqz p3, :cond_a

    .line 410
    .line 411
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 412
    .line 413
    .line 414
    move-result-object p3

    .line 415
    check-cast p3, Lcom/intsig/office/fc/dom4j/Element;

    .line 416
    .line 417
    const-string v1, "v"

    .line 418
    .line 419
    invoke-interface {p3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 420
    .line 421
    .line 422
    move-result-object p3

    .line 423
    invoke-interface {p3}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 424
    .line 425
    .line 426
    move-result-object p3

    .line 427
    const-string v1, "yyyy/m/d"

    .line 428
    .line 429
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXFormatCode()Ljava/lang/String;

    .line 430
    .line 431
    .line 432
    move-result-object v2

    .line 433
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 434
    .line 435
    .line 436
    move-result v1

    .line 437
    if-eqz v1, :cond_9

    .line 438
    .line 439
    invoke-static {p3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 440
    .line 441
    .line 442
    move-result-wide v1

    .line 443
    invoke-static {v1, v2}, Lcom/intsig/office/ss/util/DateUtil;->getJavaDate(D)Ljava/util/Date;

    .line 444
    .line 445
    .line 446
    move-result-object p3

    .line 447
    invoke-static {}, Lcom/intsig/office/ss/util/format/NumericFormatter;->instance()Lcom/intsig/office/ss/util/format/NumericFormatter;

    .line 448
    .line 449
    .line 450
    move-result-object v1

    .line 451
    invoke-virtual {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->getXFormatCode()Ljava/lang/String;

    .line 452
    .line 453
    .line 454
    move-result-object v2

    .line 455
    invoke-virtual {v1, v2, p3}, Lcom/intsig/office/ss/util/format/NumericFormatter;->getFormatContents(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    .line 456
    .line 457
    .line 458
    move-result-object p3

    .line 459
    add-int/lit8 v1, v0, 0x1

    .line 460
    .line 461
    int-to-double v2, v0

    .line 462
    invoke-virtual {p1, v2, v3, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 463
    .line 464
    .line 465
    add-int/lit8 v0, v1, -0x1

    .line 466
    .line 467
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 468
    .line 469
    .line 470
    move-result p3

    .line 471
    invoke-virtual {p1, v0, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setMaxLenXTextLabel(II)V

    .line 472
    .line 473
    .line 474
    goto :goto_5

    .line 475
    :cond_9
    add-int/lit8 v1, v0, 0x1

    .line 476
    .line 477
    int-to-double v2, v0

    .line 478
    invoke-virtual {p1, v2, v3, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    .line 479
    .line 480
    .line 481
    :goto_5
    move v0, v1

    .line 482
    goto :goto_4

    .line 483
    :cond_a
    return-void
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method


# virtual methods
.method protected buildCategoryDataset(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;
    .locals 10

    .line 1
    const-string v0, "ser"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    if-eqz v1, :cond_5

    .line 8
    .line 9
    new-instance v1, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 10
    .line 11
    const-string v2, ""

    .line 12
    .line 13
    invoke-direct {v1, v2}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string v0, "tx"

    .line 21
    .line 22
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    if-eqz v1, :cond_0

    .line 27
    .line 28
    new-instance v1, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 29
    .line 30
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getSeriesTitle(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-direct {v1, v0}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    new-instance v1, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 43
    .line 44
    invoke-direct {v1, v2}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    .line 48
    .line 49
    const/16 v2, 0xa

    .line 50
    .line 51
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 52
    .line 53
    .line 54
    const-string v3, "cat"

    .line 55
    .line 56
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    const-string v5, "v"

    .line 61
    .line 62
    const-string v6, "pt"

    .line 63
    .line 64
    if-eqz v4, :cond_1

    .line 65
    .line 66
    invoke-interface {p1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    const-string v4, "strRef"

    .line 71
    .line 72
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    const-string v4, "strCache"

    .line 77
    .line 78
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    invoke-interface {v3, v6}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 87
    .line 88
    .line 89
    move-result-object v3

    .line 90
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 91
    .line 92
    .line 93
    move-result v4

    .line 94
    if-eqz v4, :cond_1

    .line 95
    .line 96
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 97
    .line 98
    .line 99
    move-result-object v4

    .line 100
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 101
    .line 102
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 103
    .line 104
    .line 105
    move-result-object v4

    .line 106
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v4

    .line 110
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    .line 112
    .line 113
    goto :goto_1

    .line 114
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    .line 115
    .line 116
    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 117
    .line 118
    .line 119
    const-string v2, "val"

    .line 120
    .line 121
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 122
    .line 123
    .line 124
    move-result-object v4

    .line 125
    if-eqz v4, :cond_2

    .line 126
    .line 127
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    const-string v2, "numRef"

    .line 132
    .line 133
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    const-string v2, "numCache"

    .line 138
    .line 139
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    invoke-interface {p1, v6}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 144
    .line 145
    .line 146
    move-result-object p1

    .line 147
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 148
    .line 149
    .line 150
    move-result-object p1

    .line 151
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 152
    .line 153
    .line 154
    move-result v2

    .line 155
    if-eqz v2, :cond_2

    .line 156
    .line 157
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    check-cast v2, Lcom/intsig/office/fc/dom4j/Element;

    .line 162
    .line 163
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v2

    .line 171
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 172
    .line 173
    .line 174
    move-result-wide v6

    .line 175
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 176
    .line 177
    .line 178
    move-result-object v2

    .line 179
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    .line 181
    .line 182
    goto :goto_2

    .line 183
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 184
    .line 185
    .line 186
    move-result p1

    .line 187
    const/4 v2, 0x0

    .line 188
    if-lez p1, :cond_3

    .line 189
    .line 190
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 191
    .line 192
    .line 193
    move-result p1

    .line 194
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 195
    .line 196
    .line 197
    move-result v4

    .line 198
    if-ne p1, v4, :cond_3

    .line 199
    .line 200
    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 201
    .line 202
    .line 203
    move-result p1

    .line 204
    if-ge v2, p1, :cond_4

    .line 205
    .line 206
    new-instance p1, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 207
    .line 208
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;-><init>()V

    .line 209
    .line 210
    .line 211
    sget-object v4, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->themeIndex:[Ljava/lang/String;

    .line 212
    .line 213
    array-length v5, v4

    .line 214
    rem-int v5, v2, v5

    .line 215
    .line 216
    iget-object v6, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 217
    .line 218
    aget-object v5, v4, v5

    .line 219
    .line 220
    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    .line 222
    .line 223
    move-result-object v5

    .line 224
    check-cast v5, Ljava/lang/Integer;

    .line 225
    .line 226
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    .line 227
    .line 228
    .line 229
    move-result v5

    .line 230
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 231
    .line 232
    .line 233
    move-result-object v6

    .line 234
    sget-object v7, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->tints:[D

    .line 235
    .line 236
    array-length v4, v4

    .line 237
    div-int v4, v2, v4

    .line 238
    .line 239
    aget-wide v8, v7, v4

    .line 240
    .line 241
    invoke-virtual {v6, v5, v8, v9}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 242
    .line 243
    .line 244
    move-result v4

    .line 245
    invoke-virtual {p1, v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 246
    .line 247
    .line 248
    invoke-virtual {p2, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->addSeriesRenderer(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)V

    .line 249
    .line 250
    .line 251
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 252
    .line 253
    .line 254
    move-result-object p1

    .line 255
    check-cast p1, Ljava/lang/String;

    .line 256
    .line 257
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 258
    .line 259
    .line 260
    move-result-object v4

    .line 261
    check-cast v4, Ljava/lang/Double;

    .line 262
    .line 263
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    .line 264
    .line 265
    .line 266
    move-result-wide v4

    .line 267
    invoke-virtual {v1, p1, v4, v5}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V

    .line 268
    .line 269
    .line 270
    add-int/lit8 v2, v2, 0x1

    .line 271
    .line 272
    goto :goto_3

    .line 273
    :cond_3
    :goto_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 274
    .line 275
    .line 276
    move-result p1

    .line 277
    if-ge v2, p1, :cond_4

    .line 278
    .line 279
    new-instance p1, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 280
    .line 281
    invoke-direct {p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;-><init>()V

    .line 282
    .line 283
    .line 284
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->themeIndex:[Ljava/lang/String;

    .line 285
    .line 286
    array-length v4, v0

    .line 287
    rem-int v4, v2, v4

    .line 288
    .line 289
    iget-object v5, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 290
    .line 291
    aget-object v4, v0, v4

    .line 292
    .line 293
    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    .line 295
    .line 296
    move-result-object v4

    .line 297
    check-cast v4, Ljava/lang/Integer;

    .line 298
    .line 299
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    .line 300
    .line 301
    .line 302
    move-result v4

    .line 303
    invoke-static {}, Lcom/intsig/office/ss/util/ColorUtil;->instance()Lcom/intsig/office/ss/util/ColorUtil;

    .line 304
    .line 305
    .line 306
    move-result-object v5

    .line 307
    sget-object v6, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->tints:[D

    .line 308
    .line 309
    array-length v0, v0

    .line 310
    div-int v0, v2, v0

    .line 311
    .line 312
    aget-wide v7, v6, v0

    .line 313
    .line 314
    invoke-virtual {v5, v4, v7, v8}, Lcom/intsig/office/ss/util/ColorUtil;->getColorWithTint(ID)I

    .line 315
    .line 316
    .line 317
    move-result v0

    .line 318
    invoke-virtual {p1, v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 319
    .line 320
    .line 321
    invoke-virtual {p2, p1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->addSeriesRenderer(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)V

    .line 322
    .line 323
    .line 324
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 325
    .line 326
    .line 327
    move-result-object p1

    .line 328
    check-cast p1, Ljava/lang/Double;

    .line 329
    .line 330
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    .line 331
    .line 332
    .line 333
    move-result-wide v4

    .line 334
    invoke-virtual {v1, v4, v5}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(D)V

    .line 335
    .line 336
    .line 337
    add-int/lit8 v2, v2, 0x1

    .line 338
    .line 339
    goto :goto_4

    .line 340
    :cond_4
    return-object v1

    .line 341
    :cond_5
    const/4 p1, 0x0

    .line 342
    return-object p1
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method protected buildDefaultRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowGridH(Z)V

    .line 8
    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 11
    .line 12
    const-string v2, "tx1"

    .line 13
    .line 14
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    check-cast v1, Ljava/lang/Integer;

    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setLabelsColor(I)V

    .line 25
    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 28
    .line 29
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Ljava/lang/Integer;

    .line 34
    .line 35
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setAxesColor(I)V

    .line 40
    .line 41
    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public read(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/util/Map;B)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/system/IControl;",
            "Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;",
            "Lcom/intsig/office/fc/openxml4j/opc/PackagePart;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;B)",
            "Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    move-object/from16 v3, p3

    .line 8
    .line 9
    move-object/from16 v4, p4

    .line 10
    .line 11
    iput-object v4, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->schemeColor:Ljava/util/Map;

    .line 12
    .line 13
    const/16 v5, 0xa

    .line 14
    .line 15
    iput-short v5, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->type:S

    .line 16
    .line 17
    const/4 v5, 0x0

    .line 18
    iput-object v5, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->chart:Lcom/intsig/office/fc/dom4j/Element;

    .line 19
    .line 20
    const-wide/16 v6, 0x1

    .line 21
    .line 22
    iput-wide v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxX:D

    .line 23
    .line 24
    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    iput-wide v8, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minX:D

    .line 30
    .line 31
    iput-wide v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->maxY:D

    .line 32
    .line 33
    iput-wide v8, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->minY:D

    .line 34
    .line 35
    const/4 v6, 0x0

    .line 36
    iput-boolean v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMaxX:Z

    .line 37
    .line 38
    iput-boolean v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMinX:Z

    .line 39
    .line 40
    iput-boolean v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMaxY:Z

    .line 41
    .line 42
    iput-boolean v6, v0, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->hasMinY:Z

    .line 43
    .line 44
    new-instance v7, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 45
    .line 46
    invoke-direct {v7}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 47
    .line 48
    .line 49
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 50
    .line 51
    .line 52
    move-result-object v8

    .line 53
    invoke-virtual {v7, v8}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 54
    .line 55
    .line 56
    move-result-object v7

    .line 57
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    .line 58
    .line 59
    .line 60
    invoke-interface {v7}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 61
    .line 62
    .line 63
    move-result-object v7

    .line 64
    const-string v8, "spPr"

    .line 65
    .line 66
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 67
    .line 68
    .line 69
    move-result-object v9

    .line 70
    const-string v10, "noFill"

    .line 71
    .line 72
    const/4 v11, 0x1

    .line 73
    const v12, -0x8b8b8c

    .line 74
    .line 75
    .line 76
    const/4 v13, -0x1

    .line 77
    const-string v14, "ln"

    .line 78
    .line 79
    if-eqz v9, :cond_3

    .line 80
    .line 81
    invoke-interface {v9, v10}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 82
    .line 83
    .line 84
    move-result-object v15

    .line 85
    if-nez v15, :cond_0

    .line 86
    .line 87
    invoke-static {v1, v2, v3, v9, v4}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 88
    .line 89
    .line 90
    move-result-object v15

    .line 91
    if-nez v15, :cond_1

    .line 92
    .line 93
    new-instance v15, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 94
    .line 95
    invoke-direct {v15}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v15, v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v15, v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 102
    .line 103
    .line 104
    goto :goto_0

    .line 105
    :cond_0
    move-object v15, v5

    .line 106
    :cond_1
    :goto_0
    invoke-interface {v9, v14}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 107
    .line 108
    .line 109
    move-result-object v13

    .line 110
    if-eqz v13, :cond_2

    .line 111
    .line 112
    invoke-interface {v9, v14}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 113
    .line 114
    .line 115
    move-result-object v6

    .line 116
    invoke-static {v1, v2, v3, v6, v4}, Lcom/intsig/office/fc/LineKit;->createChartLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/borders/Line;

    .line 117
    .line 118
    .line 119
    move-result-object v6

    .line 120
    goto :goto_2

    .line 121
    :cond_2
    new-instance v9, Lcom/intsig/office/common/borders/Line;

    .line 122
    .line 123
    invoke-direct {v9}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 124
    .line 125
    .line 126
    new-instance v13, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 127
    .line 128
    invoke-direct {v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 129
    .line 130
    .line 131
    invoke-virtual {v13, v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v13, v12}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {v9, v13}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v9, v11}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 141
    .line 142
    .line 143
    goto :goto_1

    .line 144
    :cond_3
    new-instance v15, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 145
    .line 146
    invoke-direct {v15}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v15, v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 150
    .line 151
    .line 152
    invoke-virtual {v15, v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 153
    .line 154
    .line 155
    new-instance v9, Lcom/intsig/office/common/borders/Line;

    .line 156
    .line 157
    invoke-direct {v9}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 158
    .line 159
    .line 160
    new-instance v13, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 161
    .line 162
    invoke-direct {v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v13, v6}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 166
    .line 167
    .line 168
    invoke-virtual {v13, v12}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 169
    .line 170
    .line 171
    invoke-virtual {v9, v13}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v9, v11}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    .line 175
    .line 176
    .line 177
    :goto_1
    move-object v6, v9

    .line 178
    :goto_2
    const-string v9, "txPr"

    .line 179
    .line 180
    invoke-interface {v7, v9}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 181
    .line 182
    .line 183
    move-result-object v9

    .line 184
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->getTextSize(Lcom/intsig/office/fc/dom4j/Element;)F

    .line 185
    .line 186
    .line 187
    move-result v9

    .line 188
    const-string v11, "chart"

    .line 189
    .line 190
    invoke-interface {v7, v11}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 191
    .line 192
    .line 193
    move-result-object v12

    .line 194
    move/from16 v13, p5

    .line 195
    .line 196
    invoke-direct {v0, v12, v9, v13}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->buildAChart(Lcom/intsig/office/fc/dom4j/Element;FB)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 197
    .line 198
    .line 199
    move-result-object v12

    .line 200
    instance-of v13, v12, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;

    .line 201
    .line 202
    if-eqz v13, :cond_6

    .line 203
    .line 204
    invoke-interface {v7, v11}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 205
    .line 206
    .line 207
    move-result-object v7

    .line 208
    const-string v11, "plotArea"

    .line 209
    .line 210
    invoke-interface {v7, v11}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 211
    .line 212
    .line 213
    move-result-object v7

    .line 214
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 215
    .line 216
    .line 217
    move-result-object v7

    .line 218
    if-eqz v7, :cond_5

    .line 219
    .line 220
    invoke-interface {v7, v10}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 221
    .line 222
    .line 223
    move-result-object v8

    .line 224
    if-nez v8, :cond_4

    .line 225
    .line 226
    invoke-static {v1, v2, v3, v7, v4}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 227
    .line 228
    .line 229
    move-result-object v5

    .line 230
    :cond_4
    invoke-interface {v7, v14}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 231
    .line 232
    .line 233
    move-result-object v7

    .line 234
    invoke-static {v1, v2, v3, v7, v4}, Lcom/intsig/office/fc/LineKit;->createChartLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/borders/Line;

    .line 235
    .line 236
    .line 237
    move-result-object v1

    .line 238
    goto :goto_3

    .line 239
    :cond_5
    move-object v1, v5

    .line 240
    :goto_3
    move-object v2, v12

    .line 241
    check-cast v2, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;

    .line 242
    .line 243
    invoke-virtual {v2}, Lcom/intsig/office/thirdpart/achartengine/chart/XYChart;->getRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 244
    .line 245
    .line 246
    move-result-object v2

    .line 247
    invoke-virtual {v2, v5}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setSeriesBackgroundColor(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v2, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setSeriesFrame(Lcom/intsig/office/common/borders/Line;)V

    .line 251
    .line 252
    .line 253
    move-object v5, v2

    .line 254
    goto :goto_4

    .line 255
    :cond_6
    instance-of v1, v12, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;

    .line 256
    .line 257
    if-eqz v1, :cond_7

    .line 258
    .line 259
    move-object v1, v12

    .line 260
    check-cast v1, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;

    .line 261
    .line 262
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/chart/RoundChart;->getRenderer()Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;

    .line 263
    .line 264
    .line 265
    move-result-object v5

    .line 266
    :cond_7
    :goto_4
    if-eqz v5, :cond_8

    .line 267
    .line 268
    invoke-virtual {v5, v9}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setDefaultFontSize(F)V

    .line 269
    .line 270
    .line 271
    invoke-virtual {v5, v15}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 272
    .line 273
    .line 274
    invoke-virtual {v5, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setChartFrame(Lcom/intsig/office/common/borders/Line;)V

    .line 275
    .line 276
    .line 277
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->dispose()V

    .line 278
    .line 279
    .line 280
    return-object v12
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

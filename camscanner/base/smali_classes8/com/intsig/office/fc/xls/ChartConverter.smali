.class public Lcom/intsig/office/fc/xls/ChartConverter;
.super Ljava/lang/Object;
.source "ChartConverter.java"


# static fields
.field private static converter:Lcom/intsig/office/fc/xls/ChartConverter;


# instance fields
.field private chart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

.field private chartSeriesText:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;",
            "Lcom/intsig/office/fc/hssf/record/Record;",
            ">;"
        }
    .end annotation
.end field

.field maxY:D

.field minY:D

.field private workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/xls/ChartConverter;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/xls/ChartConverter;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/xls/ChartConverter;->converter:Lcom/intsig/office/fc/xls/ChartConverter;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private buildXYMultipleSeriesRenderer(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/xls/ChartConverter;->buildXYMultipleSeriesRenderer(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    move-result-object p1

    return-object p1
.end method

.method private buildXYMultipleSeriesRenderer(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;
    .locals 11

    .line 2
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    invoke-direct {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;-><init>()V

    const/high16 v1, 0x41800000    # 16.0f

    .line 3
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXTitleTextSize(F)V

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYTitleTextSize(F)V

    const/high16 v1, 0x41a00000    # 20.0f

    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setChartTitleTextSize(F)V

    const/high16 v1, 0x41700000    # 15.0f

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setLabelsTextSize(F)V

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setLegendTextSize(F)V

    const/4 v1, 0x1

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowGridH(Z)V

    .line 9
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->getChartType(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)S

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    if-eqz v2, :cond_1

    if-eq v2, v1, :cond_1

    if-eq v2, v4, :cond_0

    if-eq v2, v3, :cond_1

    const/4 v6, 0x4

    if-eq v2, v6, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x20

    goto :goto_0

    :cond_1
    const/16 v2, 0x18

    .line 10
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getSeries()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    move-result-object p2

    if-eqz p3, :cond_2

    .line 11
    array-length v6, p3

    if-lez v6, :cond_2

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 12
    :goto_1
    array-length v8, p2

    if-ge v6, v8, :cond_3

    .line 13
    new-instance v8, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;

    invoke-direct {v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;-><init>()V

    .line 14
    aget-object v9, p3, v7

    invoke-virtual {v8, v9}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->setPointStyle(Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)V

    add-int/2addr v7, v1

    .line 15
    array-length v9, p3

    rem-int/2addr v7, v9

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    move-result-object v9

    add-int v10, v6, v2

    invoke-virtual {v9, v10}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    move-result v9

    .line 17
    invoke-virtual {v8, v9}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 18
    invoke-virtual {v0, v8}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->addSeriesRenderer(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    const/4 p3, 0x0

    .line 19
    :goto_2
    array-length v2, p2

    if-ge p3, v2, :cond_3

    .line 20
    new-instance v2, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;

    invoke-direct {v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;-><init>()V

    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    move-result-object v6

    add-int/lit8 v7, p3, 0x18

    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    move-result v6

    .line 22
    invoke-virtual {v2, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 23
    invoke-virtual {v0, v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->addSeriesRenderer(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)V

    add-int/lit8 p3, p3, 0x1

    goto :goto_2

    .line 24
    :cond_3
    array-length p3, p2

    if-lez p3, :cond_8

    .line 25
    aget-object p2, p2, v5

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getDataCategoryLabels()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    move-result-object p2

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    move-result-object p2

    .line 26
    array-length p3, p2

    if-lez p3, :cond_8

    aget-object p2, p2, v5

    instance-of p3, p2, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    if-eqz p3, :cond_8

    .line 27
    check-cast p2, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 28
    iget-object p3, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    move-result v2

    invoke-virtual {p3, v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;

    move-result-object p3

    if-nez p3, :cond_4

    .line 29
    iget-object p3, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    move-result v2

    invoke-virtual {p3, v2}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndexFromExternSheetIndex(I)I

    move-result p3

    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    move-result-object p1

    goto :goto_3

    .line 31
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    move-result-object p1

    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;->getSheetName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(Ljava/lang/String;)Lcom/intsig/office/ss/model/baseModel/Sheet;

    move-result-object p1

    .line 32
    :goto_3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    move-result p3

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    move-result v2

    if-ne p3, v2, :cond_6

    .line 33
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    move-result-object p3

    .line 34
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    move-result v2

    const/4 v5, 0x1

    :goto_4
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    move-result v6

    if-gt v2, v6, :cond_8

    if-eqz p3, :cond_5

    .line 35
    invoke-virtual {p3, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 36
    invoke-virtual {p3, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    move-result-object v6

    invoke-direct {p0, p1, v6}, Lcom/intsig/office/fc/xls/ChartConverter;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    .line 37
    :cond_5
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    move-result v6

    sub-int v6, v2, v6

    add-int/2addr v6, v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    :goto_5
    add-int/lit8 v7, v5, 0x1

    int-to-double v8, v5

    .line 38
    invoke-virtual {v0, v8, v9, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    move v5, v7

    goto :goto_4

    .line 39
    :cond_6
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    move-result p3

    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    move-result v2

    if-ne p3, v2, :cond_8

    .line 40
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    move-result p3

    const/4 v2, 0x1

    :goto_6
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    move-result v5

    if-gt p3, v5, :cond_8

    .line 41
    invoke-virtual {p1, p3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 42
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 43
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lcom/intsig/office/fc/xls/ChartConverter;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    move-result-object v5

    goto :goto_7

    .line 44
    :cond_7
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    move-result v5

    sub-int v5, p3, v5

    add-int/2addr v5, v1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    :goto_7
    add-int/lit8 v6, v2, 0x1

    int-to-double v7, v2

    .line 45
    invoke-virtual {v0, v7, v8, v5}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->addXTextLabel(DLjava/lang/String;)V

    add-int/lit8 p3, p3, 0x1

    move v2, v6

    goto :goto_6

    .line 46
    :cond_8
    iget-object p1, p0, Lcom/intsig/office/fc/xls/ChartConverter;->chartSeriesText:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 47
    :cond_9
    :goto_8
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_d

    .line 48
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 49
    iget-object p3, p0, Lcom/intsig/office/fc/xls/ChartConverter;->chartSeriesText:Ljava/util/Map;

    invoke-interface {p3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/intsig/office/fc/hssf/record/Record;

    .line 50
    instance-of v2, p3, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;

    if-eqz v2, :cond_9

    .line 51
    check-cast p3, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;

    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;->getAnchorId()S

    move-result p3

    if-eq p3, v1, :cond_c

    if-eq p3, v4, :cond_b

    if-eq p3, v3, :cond_a

    goto :goto_8

    .line 52
    :cond_a
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXTitle(Ljava/lang/String;)V

    goto :goto_8

    .line 53
    :cond_b
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYTitle(Ljava/lang/String;)V

    goto :goto_8

    .line 54
    :cond_c
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setChartTitle(Ljava/lang/String;)V

    goto :goto_8

    :cond_d
    return-object v0
.end method

.method private convertToAChart(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 8

    .line 1
    const/4 v0, 0x5

    .line 2
    new-array v0, v0, [Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 3
    .line 4
    sget-object v1, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->DIAMOND:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    aput-object v1, v0, v2

    .line 8
    .line 9
    sget-object v1, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->SQUARE:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 10
    .line 11
    const/4 v3, 0x1

    .line 12
    aput-object v1, v0, v3

    .line 13
    .line 14
    sget-object v1, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->TRIANGLE:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 15
    .line 16
    const/4 v4, 0x2

    .line 17
    aput-object v1, v0, v4

    .line 18
    .line 19
    sget-object v1, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->X:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 20
    .line 21
    const/4 v5, 0x3

    .line 22
    aput-object v1, v0, v5

    .line 23
    .line 24
    sget-object v1, Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;->CIRCLE:Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;

    .line 25
    .line 26
    const/4 v6, 0x4

    .line 27
    aput-object v1, v0, v6

    .line 28
    .line 29
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->getChartType(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)S

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    const/4 v7, 0x0

    .line 34
    if-eq v1, v4, :cond_9

    .line 35
    .line 36
    if-eq v1, v5, :cond_6

    .line 37
    .line 38
    if-eq v1, v6, :cond_2

    .line 39
    .line 40
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->buildXYMultipleSeriesRenderer(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    if-nez v0, :cond_0

    .line 45
    .line 46
    return-object v7

    .line 47
    :cond_0
    invoke-direct {p0, p1, p2, v0, v3}, Lcom/intsig/office/fc/xls/ChartConverter;->getXYMultipleSeriesDataset(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;S)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    if-nez p1, :cond_1

    .line 52
    .line 53
    return-object v7

    .line 54
    :cond_1
    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->setChartSettings(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)V

    .line 55
    .line 56
    .line 57
    sget-object p2, Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;->DEFAULT:Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;

    .line 58
    .line 59
    invoke-static {p1, v0, p2}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->getColumnBarChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/chart/ColumnBarChart$Type;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 60
    .line 61
    .line 62
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    return-object p1

    .line 64
    :catch_0
    return-object v7

    .line 65
    :cond_2
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/xls/ChartConverter;->buildXYMultipleSeriesRenderer(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    if-nez v0, :cond_3

    .line 70
    .line 71
    return-object v7

    .line 72
    :cond_3
    invoke-direct {p0, p1, p2, v0, v6}, Lcom/intsig/office/fc/xls/ChartConverter;->getXYMultipleSeriesDataset(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;S)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    if-nez p1, :cond_4

    .line 77
    .line 78
    return-object v7

    .line 79
    :cond_4
    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->setChartSettings(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)V

    .line 80
    .line 81
    .line 82
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererCount()I

    .line 83
    .line 84
    .line 85
    move-result p2

    .line 86
    if-ge v2, p2, :cond_5

    .line 87
    .line 88
    invoke-virtual {v0, v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRendererAt(I)Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    check-cast p2, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;

    .line 93
    .line 94
    invoke-virtual {p2, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYSeriesRenderer;->setFillPoints(Z)V

    .line 95
    .line 96
    .line 97
    add-int/lit8 v2, v2, 0x1

    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_5
    invoke-static {p1, v0}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->getScatterChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    return-object p1

    .line 105
    :cond_6
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->buildDefaultRenderer(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    if-nez v0, :cond_7

    .line 110
    .line 111
    return-object v7

    .line 112
    :cond_7
    invoke-virtual {v0, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setZoomEnabled(Z)V

    .line 113
    .line 114
    .line 115
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->buildCategoryDataset(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    if-nez p1, :cond_8

    .line 120
    .line 121
    return-object v7

    .line 122
    :cond_8
    invoke-static {p1, v0}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->getPieChart(Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 123
    .line 124
    .line 125
    move-result-object p1

    .line 126
    return-object p1

    .line 127
    :cond_9
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/xls/ChartConverter;->buildXYMultipleSeriesRenderer(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;[Lcom/intsig/office/thirdpart/achartengine/chart/PointStyle;)Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    if-nez v0, :cond_a

    .line 132
    .line 133
    return-object v7

    .line 134
    :cond_a
    invoke-direct {p0, p1, p2, v0, v4}, Lcom/intsig/office/fc/xls/ChartConverter;->getXYMultipleSeriesDataset(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;S)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    if-nez p1, :cond_b

    .line 139
    .line 140
    return-object v7

    .line 141
    :cond_b
    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->setChartSettings(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)V

    .line 142
    .line 143
    .line 144
    const/16 p2, 0xa

    .line 145
    .line 146
    invoke-virtual {v0, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYLabels(I)V

    .line 147
    .line 148
    .line 149
    invoke-static {p1, v0}, Lcom/intsig/office/thirdpart/achartengine/ChartFactory;->getLineChart(Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 150
    .line 151
    .line 152
    move-result-object p1

    .line 153
    return-object p1
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->chart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->chartSeriesText:Ljava/util/Map;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private evaluate(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFName;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getFirstRowNum()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getLastRowNum()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    :goto_0
    const/4 v2, 0x0

    .line 10
    if-gt v0, v1, :cond_3

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getLastRowNum()I

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    invoke-virtual {p1, v3}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    check-cast v3, Lcom/intsig/office/ss/model/XLSModel/ARow;

    .line 21
    .line 22
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getFirstCol()I

    .line 23
    .line 24
    .line 25
    move-result v4

    .line 26
    if-lez v4, :cond_0

    .line 27
    .line 28
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getFirstCol()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    add-int/lit8 v3, v3, -0x1

    .line 33
    .line 34
    :goto_1
    int-to-short v3, v3

    .line 35
    goto :goto_2

    .line 36
    :cond_0
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getLastCol()I

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    sget v5, Lcom/intsig/office/fc/hssf/usermodel/HSSFCell;->LAST_COLUMN_NUMBER:I

    .line 41
    .line 42
    if-ge v4, v5, :cond_1

    .line 43
    .line 44
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getLastCol()I

    .line 45
    .line 46
    .line 47
    move-result v3

    .line 48
    add-int/lit8 v3, v3, 0x1

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_1
    const/4 v3, -0x1

    .line 52
    :goto_2
    if-ltz v3, :cond_2

    .line 53
    .line 54
    new-instance v1, Lcom/intsig/office/ss/model/XLSModel/ACell;

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    check-cast v4, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 61
    .line 62
    invoke-direct {v1, v4, p1, v0, v3}, Lcom/intsig/office/ss/model/XLSModel/ACell;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;Lcom/intsig/office/ss/model/XLSModel/ASheet;IS)V

    .line 63
    .line 64
    .line 65
    goto :goto_3

    .line 66
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_3
    move-object v1, v2

    .line 70
    :goto_3
    if-eqz v1, :cond_4

    .line 71
    .line 72
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFName;->getRefersToFormulaDefinition()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    invoke-virtual {v1, p2}, Lcom/intsig/office/ss/model/XLSModel/ACell;->setCellFormula([Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)V

    .line 77
    .line 78
    .line 79
    new-instance p2, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;

    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    check-cast p1, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 86
    .line 87
    invoke-direct {p2, p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;-><init>(Lcom/intsig/office/ss/model/XLSModel/AWorkbook;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p2, v1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFFormulaEvaluator;->evaluateFormulaValueEval(Lcom/intsig/office/ss/model/XLSModel/ACell;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/XLSModel/ACell;->dispose()V

    .line 95
    .line 96
    .line 97
    return-object p1

    .line 98
    :cond_4
    return-object v2
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private getCategory(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;I)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-ne v0, v1, :cond_1

    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    add-int/2addr v0, p3

    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    invoke-virtual {v0, p2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    if-eqz p2, :cond_0

    .line 31
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    add-int/lit8 p3, p3, 0x1

    .line 38
    .line 39
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-ne v0, v1, :cond_3

    .line 53
    .line 54
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    if-eqz v0, :cond_2

    .line 63
    .line 64
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    add-int/2addr p2, p3

    .line 69
    invoke-virtual {v0, p2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    if-eqz p2, :cond_2

    .line 74
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->getFormatContents(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    goto :goto_0

    .line 80
    :cond_2
    add-int/lit8 p3, p3, 0x1

    .line 81
    .line 82
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    goto :goto_0

    .line 87
    :cond_3
    const-string p1, ""

    .line 88
    .line 89
    :goto_0
    return-object p1
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D
    .locals 2

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    if-nez p2, :cond_0

    .line 4
    .line 5
    return-wide v0

    .line 6
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-nez p1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getNumberValue()D

    .line 13
    .line 14
    .line 15
    move-result-wide p1

    .line 16
    return-wide p1

    .line 17
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 18
    .line 19
    .line 20
    return-wide v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private getData(Lcom/intsig/office/ss/model/XLSModel/ASheet;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/ss/model/XLSModel/ASheet;",
            "[",
            "Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p2, :cond_f

    .line 3
    .line 4
    array-length v1, p2

    .line 5
    if-gtz v1, :cond_0

    .line 6
    .line 7
    goto/16 :goto_b

    .line 8
    .line 9
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    aget-object v3, p2, v2

    .line 16
    .line 17
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 18
    .line 19
    const-wide/16 v5, 0x0

    .line 20
    .line 21
    if-eqz v4, :cond_5

    .line 22
    .line 23
    check-cast v3, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 24
    .line 25
    iget-object p2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 26
    .line 27
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    if-nez p2, :cond_1

    .line 36
    .line 37
    iget-object p2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 38
    .line 39
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndexFromExternSheetIndex(I)I

    .line 44
    .line 45
    .line 46
    move-result p2

    .line 47
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;->getSheetName()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p2

    .line 64
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(Ljava/lang/String;)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    :goto_0
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 69
    .line 70
    .line 71
    move-result p2

    .line 72
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-ne p2, v0, :cond_3

    .line 77
    .line 78
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 79
    .line 80
    .line 81
    move-result p2

    .line 82
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 83
    .line 84
    .line 85
    move-result-object p2

    .line 86
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    :goto_1
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    if-gt v0, v2, :cond_e

    .line 95
    .line 96
    if-eqz p2, :cond_2

    .line 97
    .line 98
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    invoke-direct {p0, p1, v2}, Lcom/intsig/office/fc/xls/ChartConverter;->getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D

    .line 103
    .line 104
    .line 105
    move-result-wide v7

    .line 106
    goto :goto_2

    .line 107
    :cond_2
    move-wide v7, v5

    .line 108
    :goto_2
    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    add-int/lit8 v0, v0, 0x1

    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_3
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 119
    .line 120
    .line 121
    move-result p2

    .line 122
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    if-ne p2, v0, :cond_e

    .line 127
    .line 128
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 129
    .line 130
    .line 131
    move-result p2

    .line 132
    :goto_3
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    if-gt p2, v0, :cond_e

    .line 137
    .line 138
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    if-eqz v0, :cond_4

    .line 143
    .line 144
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 145
    .line 146
    .line 147
    move-result v2

    .line 148
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/xls/ChartConverter;->getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D

    .line 153
    .line 154
    .line 155
    move-result-wide v7

    .line 156
    goto :goto_4

    .line 157
    :cond_4
    move-wide v7, v5

    .line 158
    :goto_4
    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    .line 164
    .line 165
    add-int/lit8 p2, p2, 0x1

    .line 166
    .line 167
    goto :goto_3

    .line 168
    :cond_5
    instance-of v4, v3, Lcom/intsig/office/fc/hssf/formula/ptg/MemFuncPtg;

    .line 169
    .line 170
    if-eqz v4, :cond_9

    .line 171
    .line 172
    :goto_5
    array-length v0, p2

    .line 173
    if-ge v2, v0, :cond_e

    .line 174
    .line 175
    aget-object v0, p2, v2

    .line 176
    .line 177
    instance-of v3, v0, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;

    .line 178
    .line 179
    if-eqz v3, :cond_8

    .line 180
    .line 181
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;

    .line 182
    .line 183
    iget-object v3, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 184
    .line 185
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    .line 186
    .line 187
    .line 188
    move-result v4

    .line 189
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    if-nez v3, :cond_6

    .line 194
    .line 195
    iget-object v3, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 196
    .line 197
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    .line 198
    .line 199
    .line 200
    move-result v4

    .line 201
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndexFromExternSheetIndex(I)I

    .line 202
    .line 203
    .line 204
    move-result v3

    .line 205
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 206
    .line 207
    .line 208
    move-result-object v4

    .line 209
    invoke-virtual {v4, v3}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 210
    .line 211
    .line 212
    move-result-object v3

    .line 213
    goto :goto_6

    .line 214
    :cond_6
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 215
    .line 216
    .line 217
    move-result-object v4

    .line 218
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;->getSheetName()Ljava/lang/String;

    .line 219
    .line 220
    .line 221
    move-result-object v3

    .line 222
    invoke-virtual {v4, v3}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(Ljava/lang/String;)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 223
    .line 224
    .line 225
    move-result-object v3

    .line 226
    :goto_6
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->getRow()I

    .line 227
    .line 228
    .line 229
    move-result v4

    .line 230
    invoke-virtual {v3, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 231
    .line 232
    .line 233
    move-result-object v4

    .line 234
    if-eqz v4, :cond_7

    .line 235
    .line 236
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->getColumn()I

    .line 237
    .line 238
    .line 239
    move-result v0

    .line 240
    invoke-virtual {v4, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 241
    .line 242
    .line 243
    move-result-object v0

    .line 244
    invoke-direct {p0, v3, v0}, Lcom/intsig/office/fc/xls/ChartConverter;->getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D

    .line 245
    .line 246
    .line 247
    move-result-wide v3

    .line 248
    goto :goto_7

    .line 249
    :cond_7
    move-wide v3, v5

    .line 250
    :goto_7
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 251
    .line 252
    .line 253
    move-result-object v0

    .line 254
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    .line 256
    .line 257
    :cond_8
    add-int/lit8 v2, v2, 0x1

    .line 258
    .line 259
    goto :goto_5

    .line 260
    :cond_9
    instance-of v2, v3, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;

    .line 261
    .line 262
    if-eqz v2, :cond_b

    .line 263
    .line 264
    :try_start_0
    check-cast v3, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;

    .line 265
    .line 266
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 267
    .line 268
    .line 269
    move-result-object p2

    .line 270
    check-cast p2, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 271
    .line 272
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/NameXPtg;->getNameIndex()I

    .line 273
    .line 274
    .line 275
    move-result v2

    .line 276
    invoke-virtual {p2, v2}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getNameAt(I)Lcom/intsig/office/fc/hssf/usermodel/HSSFName;

    .line 277
    .line 278
    .line 279
    move-result-object p2

    .line 280
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->evaluate(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFName;)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 281
    .line 282
    .line 283
    move-result-object p1

    .line 284
    instance-of p2, p1, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;

    .line 285
    .line 286
    if-eqz p2, :cond_e

    .line 287
    .line 288
    check-cast p1, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;

    .line 289
    .line 290
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getFirstRow()I

    .line 291
    .line 292
    .line 293
    move-result p2

    .line 294
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getLastRow()I

    .line 295
    .line 296
    .line 297
    move-result v2

    .line 298
    if-ne p2, v2, :cond_a

    .line 299
    .line 300
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getFirstColumn()I

    .line 301
    .line 302
    .line 303
    move-result p2

    .line 304
    :goto_8
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getLastColumn()I

    .line 305
    .line 306
    .line 307
    move-result v2

    .line 308
    if-gt p2, v2, :cond_e

    .line 309
    .line 310
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getFirstRow()I

    .line 311
    .line 312
    .line 313
    move-result v2

    .line 314
    invoke-interface {p1, v2, p2}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getAbsoluteValue(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 315
    .line 316
    .line 317
    move-result-object v2

    .line 318
    check-cast v2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 319
    .line 320
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 321
    .line 322
    .line 323
    move-result-wide v2

    .line 324
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 325
    .line 326
    .line 327
    move-result-object v2

    .line 328
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    .line 330
    .line 331
    add-int/lit8 p2, p2, 0x1

    .line 332
    .line 333
    goto :goto_8

    .line 334
    :cond_a
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getFirstColumn()I

    .line 335
    .line 336
    .line 337
    move-result p2

    .line 338
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getLastColumn()I

    .line 339
    .line 340
    .line 341
    move-result v2

    .line 342
    if-ne p2, v2, :cond_e

    .line 343
    .line 344
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getFirstRow()I

    .line 345
    .line 346
    .line 347
    move-result p2

    .line 348
    :goto_9
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getLastRow()I

    .line 349
    .line 350
    .line 351
    move-result v2

    .line 352
    if-gt p2, v2, :cond_e

    .line 353
    .line 354
    invoke-interface {p1}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getFirstColumn()I

    .line 355
    .line 356
    .line 357
    move-result v2

    .line 358
    invoke-interface {p1, p2, v2}, Lcom/intsig/office/fc/hssf/formula/eval/AreaEval;->getAbsoluteValue(II)Lcom/intsig/office/fc/hssf/formula/eval/ValueEval;

    .line 359
    .line 360
    .line 361
    move-result-object v2

    .line 362
    check-cast v2, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;

    .line 363
    .line 364
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/eval/NumberEval;->getNumberValue()D

    .line 365
    .line 366
    .line 367
    move-result-wide v2

    .line 368
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 369
    .line 370
    .line 371
    move-result-object v2

    .line 372
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    .line 374
    .line 375
    add-int/lit8 p2, p2, 0x1

    .line 376
    .line 377
    goto :goto_9

    .line 378
    :catch_0
    move-exception p1

    .line 379
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 380
    .line 381
    .line 382
    return-object v0

    .line 383
    :cond_b
    array-length p2, p2

    .line 384
    if-lez p2, :cond_e

    .line 385
    .line 386
    instance-of p2, v3, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;

    .line 387
    .line 388
    if-eqz p2, :cond_e

    .line 389
    .line 390
    check-cast v3, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;

    .line 391
    .line 392
    iget-object p2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 393
    .line 394
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    .line 395
    .line 396
    .line 397
    move-result v0

    .line 398
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;

    .line 399
    .line 400
    .line 401
    move-result-object p2

    .line 402
    if-nez p2, :cond_c

    .line 403
    .line 404
    iget-object p2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 405
    .line 406
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/Ref3DPtg;->getExternSheetIndex()I

    .line 407
    .line 408
    .line 409
    move-result v0

    .line 410
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndexFromExternSheetIndex(I)I

    .line 411
    .line 412
    .line 413
    move-result p2

    .line 414
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 415
    .line 416
    .line 417
    move-result-object p1

    .line 418
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 419
    .line 420
    .line 421
    move-result-object p1

    .line 422
    goto :goto_a

    .line 423
    :cond_c
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 424
    .line 425
    .line 426
    move-result-object p1

    .line 427
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;->getSheetName()Ljava/lang/String;

    .line 428
    .line 429
    .line 430
    move-result-object p2

    .line 431
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(Ljava/lang/String;)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 432
    .line 433
    .line 434
    move-result-object p1

    .line 435
    :goto_a
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->getRow()I

    .line 436
    .line 437
    .line 438
    move-result p2

    .line 439
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 440
    .line 441
    .line 442
    move-result-object p2

    .line 443
    if-eqz p2, :cond_d

    .line 444
    .line 445
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/formula/ptg/RefPtgBase;->getColumn()I

    .line 446
    .line 447
    .line 448
    move-result v0

    .line 449
    invoke-virtual {p2, v0}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 450
    .line 451
    .line 452
    move-result-object p2

    .line 453
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D

    .line 454
    .line 455
    .line 456
    move-result-wide v5

    .line 457
    :cond_d
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 458
    .line 459
    .line 460
    move-result-object p1

    .line 461
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    .line 463
    .line 464
    :cond_e
    return-object v1

    .line 465
    :cond_f
    :goto_b
    return-object v0
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private getFormatContents(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)Ljava/lang/String;
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellType()S

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v1, :cond_3

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    if-eq v1, v0, :cond_1

    .line 13
    .line 14
    const/4 p1, 0x4

    .line 15
    if-eq v1, p1, :cond_0

    .line 16
    .line 17
    const-string p1, ""

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getBooleanValue()Z

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    goto :goto_0

    .line 29
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getStringCellValueIndex()I

    .line 34
    .line 35
    .line 36
    move-result p2

    .line 37
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSharedItem(I)Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    instance-of p2, p1, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 42
    .line 43
    if-eqz p2, :cond_2

    .line 44
    .line 45
    check-cast p1, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 46
    .line 47
    const/4 p2, 0x0

    .line 48
    invoke-virtual {p1, p2}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    goto :goto_0

    .line 53
    :cond_2
    check-cast p1, Ljava/lang/String;

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 57
    .line 58
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/style/CellStyle;->getNumberFormatID()S

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    invoke-static {v1, v0}, Lcom/intsig/office/fc/hssf/usermodel/HSSFDataFormat;->getFormatCode(Lcom/intsig/office/fc/hssf/model/InternalWorkbook;S)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-static {}, Lcom/intsig/office/ss/util/format/NumericFormatter;->instance()Lcom/intsig/office/ss/util/format/NumericFormatter;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/util/format/NumericFormatter;->getNumericCellType(Ljava/lang/String;)S

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    const/16 v2, 0xa

    .line 75
    .line 76
    if-ne v1, v2, :cond_4

    .line 77
    .line 78
    invoke-static {}, Lcom/intsig/office/ss/util/format/NumericFormatter;->instance()Lcom/intsig/office/ss/util/format/NumericFormatter;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isUsing1904DateWindowing()Z

    .line 87
    .line 88
    .line 89
    move-result p1

    .line 90
    invoke-virtual {p2, p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getDateCellValue(Z)Ljava/util/Date;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    invoke-virtual {v1, v0, p1}, Lcom/intsig/office/ss/util/format/NumericFormatter;->getFormatContents(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    goto :goto_0

    .line 99
    :cond_4
    invoke-static {}, Lcom/intsig/office/ss/util/format/NumericFormatter;->instance()Lcom/intsig/office/ss/util/format/NumericFormatter;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-virtual {p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->getNumberValue()D

    .line 104
    .line 105
    .line 106
    move-result-wide v2

    .line 107
    invoke-virtual {p1, v0, v2, v3, v1}, Lcom/intsig/office/ss/util/format/NumericFormatter;->getFormatContents(Ljava/lang/String;DS)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    :goto_0
    return-object p1
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private getXYMultipleSeriesDataset(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;S)Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move/from16 v2, p4

    .line 6
    .line 7
    new-instance v3, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;

    .line 8
    .line 9
    invoke-direct {v3}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getSeries()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 13
    .line 14
    .line 15
    move-result-object v4

    .line 16
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->getSeriesRenderers()[Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 17
    .line 18
    .line 19
    move-result-object v5

    .line 20
    array-length v6, v4

    .line 21
    const/4 v8, 0x0

    .line 22
    :goto_0
    if-ge v8, v6, :cond_9

    .line 23
    .line 24
    aget-object v9, v4, v8

    .line 25
    .line 26
    invoke-virtual {v9}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTitle()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v9

    .line 30
    if-eqz v9, :cond_0

    .line 31
    .line 32
    aget-object v9, v4, v8

    .line 33
    .line 34
    invoke-virtual {v9}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTitle()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v9

    .line 38
    goto :goto_1

    .line 39
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v10, "Series "

    .line 45
    .line 46
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    add-int/lit8 v10, v8, 0x1

    .line 50
    .line 51
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v9

    .line 58
    :goto_1
    const/4 v10, 0x4

    .line 59
    const/4 v11, 0x1

    .line 60
    if-ne v2, v10, :cond_2

    .line 61
    .line 62
    new-instance v12, Ljava/util/ArrayList;

    .line 63
    .line 64
    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .line 66
    .line 67
    aget-object v13, v4, v8

    .line 68
    .line 69
    invoke-virtual {v13}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getDataCategoryLabels()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 70
    .line 71
    .line 72
    move-result-object v13

    .line 73
    invoke-virtual {v13}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 74
    .line 75
    .line 76
    move-result-object v13

    .line 77
    array-length v14, v13

    .line 78
    if-lez v14, :cond_3

    .line 79
    .line 80
    invoke-direct {v0, v1, v13}, Lcom/intsig/office/fc/xls/ChartConverter;->getData(Lcom/intsig/office/ss/model/XLSModel/ASheet;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Ljava/util/List;

    .line 81
    .line 82
    .line 83
    move-result-object v12

    .line 84
    const/4 v13, 0x0

    .line 85
    :cond_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    .line 86
    .line 87
    .line 88
    move-result v14

    .line 89
    sub-int/2addr v14, v11

    .line 90
    if-ge v13, v14, :cond_3

    .line 91
    .line 92
    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object v14

    .line 96
    check-cast v14, Ljava/lang/Double;

    .line 97
    .line 98
    invoke-virtual {v14}, Ljava/lang/Double;->doubleValue()D

    .line 99
    .line 100
    .line 101
    move-result-wide v14

    .line 102
    add-int/lit8 v13, v13, 0x1

    .line 103
    .line 104
    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 105
    .line 106
    .line 107
    move-result-object v16

    .line 108
    check-cast v16, Ljava/lang/Double;

    .line 109
    .line 110
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Double;->doubleValue()D

    .line 111
    .line 112
    .line 113
    move-result-wide v16

    .line 114
    sub-double v14, v14, v16

    .line 115
    .line 116
    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    .line 117
    .line 118
    .line 119
    move-result-wide v14

    .line 120
    const-wide v16, 0x3e112e0be0000000L    # 9.999999717180685E-10

    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    cmpg-double v18, v14, v16

    .line 126
    .line 127
    if-gez v18, :cond_1

    .line 128
    .line 129
    const/4 v11, 0x0

    .line 130
    goto :goto_2

    .line 131
    :cond_2
    const/4 v12, 0x0

    .line 132
    :cond_3
    :goto_2
    aget-object v13, v4, v8

    .line 133
    .line 134
    invoke-virtual {v13}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getDataValues()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 135
    .line 136
    .line 137
    move-result-object v13

    .line 138
    invoke-virtual {v13}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 139
    .line 140
    .line 141
    move-result-object v13

    .line 142
    array-length v14, v13

    .line 143
    if-gtz v14, :cond_5

    .line 144
    .line 145
    aget-object v9, v5, v8

    .line 146
    .line 147
    move-object/from16 v14, p3

    .line 148
    .line 149
    invoke-virtual {v14, v9}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->removeSeriesRenderer(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)V

    .line 150
    .line 151
    .line 152
    aget-object v9, v4, v8

    .line 153
    .line 154
    move-object/from16 v15, p2

    .line 155
    .line 156
    invoke-virtual {v15, v9}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->removeSeries(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;)V

    .line 157
    .line 158
    .line 159
    :cond_4
    move/from16 v17, v8

    .line 160
    .line 161
    goto/16 :goto_5

    .line 162
    .line 163
    :cond_5
    move-object/from16 v15, p2

    .line 164
    .line 165
    move-object/from16 v14, p3

    .line 166
    .line 167
    invoke-direct {v0, v1, v13}, Lcom/intsig/office/fc/xls/ChartConverter;->getData(Lcom/intsig/office/ss/model/XLSModel/ASheet;[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;)Ljava/util/List;

    .line 168
    .line 169
    .line 170
    move-result-object v13

    .line 171
    if-ne v2, v10, :cond_7

    .line 172
    .line 173
    if-eqz v11, :cond_7

    .line 174
    .line 175
    if-eqz v12, :cond_4

    .line 176
    .line 177
    if-eqz v13, :cond_4

    .line 178
    .line 179
    invoke-interface {v12}, Ljava/util/List;->size()I

    .line 180
    .line 181
    .line 182
    move-result v10

    .line 183
    invoke-interface {v13}, Ljava/util/List;->size()I

    .line 184
    .line 185
    .line 186
    move-result v11

    .line 187
    if-ne v10, v11, :cond_4

    .line 188
    .line 189
    new-instance v10, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 190
    .line 191
    invoke-direct {v10, v9}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;-><init>(Ljava/lang/String;)V

    .line 192
    .line 193
    .line 194
    const/4 v9, 0x0

    .line 195
    :goto_3
    invoke-interface {v12}, Ljava/util/List;->size()I

    .line 196
    .line 197
    .line 198
    move-result v11

    .line 199
    if-ge v9, v11, :cond_6

    .line 200
    .line 201
    invoke-interface {v12, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 202
    .line 203
    .line 204
    move-result-object v11

    .line 205
    check-cast v11, Ljava/lang/Double;

    .line 206
    .line 207
    move/from16 v17, v8

    .line 208
    .line 209
    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    .line 210
    .line 211
    .line 212
    move-result-wide v7

    .line 213
    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 214
    .line 215
    .line 216
    move-result-object v11

    .line 217
    check-cast v11, Ljava/lang/Double;

    .line 218
    .line 219
    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    .line 220
    .line 221
    .line 222
    move-result-wide v1

    .line 223
    invoke-virtual {v10, v7, v8, v1, v2}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->add(DD)V

    .line 224
    .line 225
    .line 226
    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 227
    .line 228
    .line 229
    move-result-object v1

    .line 230
    check-cast v1, Ljava/lang/Double;

    .line 231
    .line 232
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    .line 233
    .line 234
    .line 235
    move-result-wide v1

    .line 236
    iget-wide v7, v0, Lcom/intsig/office/fc/xls/ChartConverter;->minY:D

    .line 237
    .line 238
    invoke-static {v1, v2, v7, v8}, Ljava/lang/Math;->min(DD)D

    .line 239
    .line 240
    .line 241
    move-result-wide v1

    .line 242
    iput-wide v1, v0, Lcom/intsig/office/fc/xls/ChartConverter;->minY:D

    .line 243
    .line 244
    invoke-interface {v13, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 245
    .line 246
    .line 247
    move-result-object v1

    .line 248
    check-cast v1, Ljava/lang/Double;

    .line 249
    .line 250
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    .line 251
    .line 252
    .line 253
    move-result-wide v1

    .line 254
    iget-wide v7, v0, Lcom/intsig/office/fc/xls/ChartConverter;->maxY:D

    .line 255
    .line 256
    invoke-static {v1, v2, v7, v8}, Ljava/lang/Math;->max(DD)D

    .line 257
    .line 258
    .line 259
    move-result-wide v1

    .line 260
    iput-wide v1, v0, Lcom/intsig/office/fc/xls/ChartConverter;->maxY:D

    .line 261
    .line 262
    add-int/lit8 v9, v9, 0x1

    .line 263
    .line 264
    move-object/from16 v1, p1

    .line 265
    .line 266
    move/from16 v2, p4

    .line 267
    .line 268
    move/from16 v8, v17

    .line 269
    .line 270
    goto :goto_3

    .line 271
    :cond_6
    move/from16 v17, v8

    .line 272
    .line 273
    invoke-virtual {v3, v10}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;)V

    .line 274
    .line 275
    .line 276
    goto :goto_5

    .line 277
    :cond_7
    move/from16 v17, v8

    .line 278
    .line 279
    new-instance v1, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 280
    .line 281
    invoke-direct {v1, v9}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 282
    .line 283
    .line 284
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 285
    .line 286
    .line 287
    move-result-object v2

    .line 288
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 289
    .line 290
    .line 291
    move-result v7

    .line 292
    if-eqz v7, :cond_8

    .line 293
    .line 294
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 295
    .line 296
    .line 297
    move-result-object v7

    .line 298
    check-cast v7, Ljava/lang/Double;

    .line 299
    .line 300
    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    .line 301
    .line 302
    .line 303
    move-result-wide v8

    .line 304
    invoke-virtual {v1, v8, v9}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(D)V

    .line 305
    .line 306
    .line 307
    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    .line 308
    .line 309
    .line 310
    move-result-wide v8

    .line 311
    iget-wide v10, v0, Lcom/intsig/office/fc/xls/ChartConverter;->minY:D

    .line 312
    .line 313
    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(DD)D

    .line 314
    .line 315
    .line 316
    move-result-wide v8

    .line 317
    iput-wide v8, v0, Lcom/intsig/office/fc/xls/ChartConverter;->minY:D

    .line 318
    .line 319
    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    .line 320
    .line 321
    .line 322
    move-result-wide v7

    .line 323
    iget-wide v9, v0, Lcom/intsig/office/fc/xls/ChartConverter;->maxY:D

    .line 324
    .line 325
    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->max(DD)D

    .line 326
    .line 327
    .line 328
    move-result-wide v7

    .line 329
    iput-wide v7, v0, Lcom/intsig/office/fc/xls/ChartConverter;->maxY:D

    .line 330
    .line 331
    goto :goto_4

    .line 332
    :cond_8
    invoke-virtual {v1}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->toXYSeries()Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 333
    .line 334
    .line 335
    move-result-object v1

    .line 336
    invoke-virtual {v3, v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->addSeries(Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;)V

    .line 337
    .line 338
    .line 339
    :goto_5
    add-int/lit8 v8, v17, 0x1

    .line 340
    .line 341
    move-object/from16 v1, p1

    .line 342
    .line 343
    move/from16 v2, p4

    .line 344
    .line 345
    goto/16 :goto_0

    .line 346
    .line 347
    :cond_9
    return-object v3
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static instance()Lcom/intsig/office/fc/xls/ChartConverter;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/xls/ChartConverter;->converter:Lcom/intsig/office/fc/xls/ChartConverter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private setChartSettings(Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)V
    .locals 7

    .line 1
    const/4 v0, -0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesCount()I

    .line 5
    .line 6
    .line 7
    move-result v3

    .line 8
    if-ge v2, v3, :cond_0

    .line 9
    .line 10
    invoke-virtual {p2, v2}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 11
    .line 12
    .line 13
    move-result-object v3

    .line 14
    invoke-virtual {v3}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getItemCount()I

    .line 15
    .line 16
    .line 17
    move-result v3

    .line 18
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    add-int/lit8 v2, v2, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {p3}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getValueRangeRecord()Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    const/4 v4, 0x4

    .line 34
    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    .line 35
    .line 36
    if-lez v3, :cond_5

    .line 37
    .line 38
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/xls/ChartConverter;->getChartType(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)S

    .line 39
    .line 40
    .line 41
    move-result p3

    .line 42
    if-eq p3, v4, :cond_1

    .line 43
    .line 44
    invoke-virtual {p1, v5, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 45
    .line 46
    .line 47
    int-to-double p2, v0

    .line 48
    add-double/2addr p2, v5

    .line 49
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 50
    .line 51
    .line 52
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    check-cast p2, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_1
    invoke-virtual {p2, v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 60
    .line 61
    .line 62
    move-result-object p3

    .line 63
    invoke-virtual {p3}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMinX()D

    .line 64
    .line 65
    .line 66
    move-result-wide v3

    .line 67
    invoke-virtual {p2, v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 68
    .line 69
    .line 70
    move-result-object p2

    .line 71
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMaxX()D

    .line 72
    .line 73
    .line 74
    move-result-wide p2

    .line 75
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    check-cast v0, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 80
    .line 81
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->isAutomaticMinimum()Z

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    if-nez v1, :cond_2

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->getMinimumAxisValue()D

    .line 88
    .line 89
    .line 90
    move-result-wide v3

    .line 91
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->isAutomaticMaximum()Z

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    if-nez v1, :cond_3

    .line 96
    .line 97
    invoke-virtual {v0}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->getMaximumAxisValue()D

    .line 98
    .line 99
    .line 100
    move-result-wide p2

    .line 101
    :cond_3
    invoke-virtual {p1, v3, v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 105
    .line 106
    .line 107
    const/4 p2, 0x1

    .line 108
    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object p2

    .line 112
    check-cast p2, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;

    .line 113
    .line 114
    :goto_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->isAutomaticMinimum()Z

    .line 115
    .line 116
    .line 117
    move-result p3

    .line 118
    if-nez p3, :cond_4

    .line 119
    .line 120
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->getMinimumAxisValue()D

    .line 121
    .line 122
    .line 123
    move-result-wide v0

    .line 124
    iput-wide v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->minY:D

    .line 125
    .line 126
    :cond_4
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->isAutomaticMaximum()Z

    .line 127
    .line 128
    .line 129
    move-result p3

    .line 130
    if-nez p3, :cond_7

    .line 131
    .line 132
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/ValueRangeRecord;->getMaximumAxisValue()D

    .line 133
    .line 134
    .line 135
    move-result-wide p2

    .line 136
    iput-wide p2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->maxY:D

    .line 137
    .line 138
    goto :goto_2

    .line 139
    :cond_5
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/xls/ChartConverter;->getChartType(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)S

    .line 140
    .line 141
    .line 142
    move-result p3

    .line 143
    if-eq p3, v4, :cond_6

    .line 144
    .line 145
    invoke-virtual {p1, v5, v6}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 146
    .line 147
    .line 148
    int-to-double p2, v0

    .line 149
    add-double/2addr p2, v5

    .line 150
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 151
    .line 152
    .line 153
    goto :goto_2

    .line 154
    :cond_6
    invoke-virtual {p2, v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 155
    .line 156
    .line 157
    move-result-object p3

    .line 158
    invoke-virtual {p3}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMinX()D

    .line 159
    .line 160
    .line 161
    move-result-wide v2

    .line 162
    invoke-virtual {p2, v1}, Lcom/intsig/office/thirdpart/achartengine/model/XYMultipleSeriesDataset;->getSeriesAt(I)Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;

    .line 163
    .line 164
    .line 165
    move-result-object p2

    .line 166
    invoke-virtual {p2}, Lcom/intsig/office/thirdpart/achartengine/model/XYSeries;->getMaxX()D

    .line 167
    .line 168
    .line 169
    move-result-wide p2

    .line 170
    invoke-virtual {p1, v2, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMin(D)V

    .line 171
    .line 172
    .line 173
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setXAxisMax(D)V

    .line 174
    .line 175
    .line 176
    :cond_7
    :goto_2
    iget-wide p2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->minY:D

    .line 177
    .line 178
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYAxisMin(D)V

    .line 179
    .line 180
    .line 181
    iget-wide p2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->maxY:D

    .line 182
    .line 183
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/thirdpart/achartengine/renderers/XYMultipleSeriesRenderer;->setYAxisMax(D)V

    .line 184
    .line 185
    .line 186
    return-void
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method


# virtual methods
.method protected buildCategoryDataset(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;
    .locals 9

    .line 1
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getSeries()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    array-length v0, v0

    .line 13
    const/4 v2, 0x0

    .line 14
    if-lez v0, :cond_b

    .line 15
    .line 16
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getSeries()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    const/4 v0, 0x0

    .line 21
    aget-object p2, p2, v0

    .line 22
    .line 23
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTitle()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    if-eqz v3, :cond_0

    .line 28
    .line 29
    new-instance v1, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 30
    .line 31
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTitle()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-direct {v1, v3}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    new-instance v3, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;

    .line 40
    .line 41
    invoke-direct {v3, v1}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;-><init>(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    move-object v1, v3

    .line 45
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getDataCategoryLabels()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getDataValues()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 54
    .line 55
    .line 56
    move-result-object p2

    .line 57
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    array-length v4, p2

    .line 62
    if-lez v4, :cond_b

    .line 63
    .line 64
    aget-object p2, p2, v0

    .line 65
    .line 66
    instance-of v4, p2, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 67
    .line 68
    if-nez v4, :cond_1

    .line 69
    .line 70
    goto/16 :goto_a

    .line 71
    .line 72
    :cond_1
    check-cast p2, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 75
    .line 76
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    .line 77
    .line 78
    .line 79
    move-result v4

    .line 80
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getExternalSheet(I)Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    if-nez v2, :cond_2

    .line 85
    .line 86
    iget-object v2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 87
    .line 88
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;->getExternSheetIndex()I

    .line 89
    .line 90
    .line 91
    move-result v4

    .line 92
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/hssf/model/InternalWorkbook;->getSheetIndexFromExternSheetIndex(I)I

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-virtual {p1, v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(I)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    goto :goto_1

    .line 105
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/formula/EvaluationWorkbook$ExternalSheet;->getSheetName()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v2

    .line 113
    invoke-virtual {p1, v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSheet(Ljava/lang/String;)Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    :goto_1
    array-length v2, v3

    .line 118
    const-wide/16 v4, 0x0

    .line 119
    .line 120
    if-lez v2, :cond_6

    .line 121
    .line 122
    aget-object v0, v3, v0

    .line 123
    .line 124
    instance-of v2, v0, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 125
    .line 126
    if-eqz v2, :cond_6

    .line 127
    .line 128
    check-cast v0, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 129
    .line 130
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 131
    .line 132
    .line 133
    move-result v2

    .line 134
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 135
    .line 136
    .line 137
    move-result v3

    .line 138
    if-ne v2, v3, :cond_4

    .line 139
    .line 140
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 141
    .line 142
    .line 143
    move-result v2

    .line 144
    :goto_2
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 145
    .line 146
    .line 147
    move-result v3

    .line 148
    if-gt v2, v3, :cond_a

    .line 149
    .line 150
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 151
    .line 152
    .line 153
    move-result v3

    .line 154
    sub-int v3, v2, v3

    .line 155
    .line 156
    invoke-direct {p0, p1, v0, v3}, Lcom/intsig/office/fc/xls/ChartConverter;->getCategory(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;I)Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v3

    .line 160
    invoke-virtual {p1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 161
    .line 162
    .line 163
    move-result-object v6

    .line 164
    if-eqz v6, :cond_3

    .line 165
    .line 166
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 167
    .line 168
    .line 169
    move-result v7

    .line 170
    invoke-virtual {v6, v7}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 171
    .line 172
    .line 173
    move-result-object v6

    .line 174
    invoke-direct {p0, p1, v6}, Lcom/intsig/office/fc/xls/ChartConverter;->getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D

    .line 175
    .line 176
    .line 177
    move-result-wide v6

    .line 178
    goto :goto_3

    .line 179
    :cond_3
    move-wide v6, v4

    .line 180
    :goto_3
    invoke-virtual {v1, v3, v6, v7}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V

    .line 181
    .line 182
    .line 183
    add-int/lit8 v2, v2, 0x1

    .line 184
    .line 185
    goto :goto_2

    .line 186
    :cond_4
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 187
    .line 188
    .line 189
    move-result v2

    .line 190
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 191
    .line 192
    .line 193
    move-result v3

    .line 194
    if-ne v2, v3, :cond_a

    .line 195
    .line 196
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 197
    .line 198
    .line 199
    move-result v2

    .line 200
    invoke-virtual {p1, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 201
    .line 202
    .line 203
    move-result-object v2

    .line 204
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 205
    .line 206
    .line 207
    move-result v3

    .line 208
    :goto_4
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 209
    .line 210
    .line 211
    move-result v6

    .line 212
    if-gt v3, v6, :cond_a

    .line 213
    .line 214
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 215
    .line 216
    .line 217
    move-result v6

    .line 218
    sub-int v6, v3, v6

    .line 219
    .line 220
    invoke-direct {p0, p1, v0, v6}, Lcom/intsig/office/fc/xls/ChartConverter;->getCategory(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;I)Ljava/lang/String;

    .line 221
    .line 222
    .line 223
    move-result-object v6

    .line 224
    if-eqz v2, :cond_5

    .line 225
    .line 226
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 227
    .line 228
    .line 229
    move-result-object v7

    .line 230
    invoke-direct {p0, p1, v7}, Lcom/intsig/office/fc/xls/ChartConverter;->getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D

    .line 231
    .line 232
    .line 233
    move-result-wide v7

    .line 234
    goto :goto_5

    .line 235
    :cond_5
    move-wide v7, v4

    .line 236
    :goto_5
    invoke-virtual {v1, v6, v7, v8}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(Ljava/lang/String;D)V

    .line 237
    .line 238
    .line 239
    add-int/lit8 v3, v3, 0x1

    .line 240
    .line 241
    goto :goto_4

    .line 242
    :cond_6
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 243
    .line 244
    .line 245
    move-result v0

    .line 246
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 247
    .line 248
    .line 249
    move-result v2

    .line 250
    if-ne v0, v2, :cond_8

    .line 251
    .line 252
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 253
    .line 254
    .line 255
    move-result v0

    .line 256
    :goto_6
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 257
    .line 258
    .line 259
    move-result v2

    .line 260
    if-gt v0, v2, :cond_a

    .line 261
    .line 262
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 263
    .line 264
    .line 265
    move-result-object v2

    .line 266
    if-eqz v2, :cond_7

    .line 267
    .line 268
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 269
    .line 270
    .line 271
    move-result v3

    .line 272
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 273
    .line 274
    .line 275
    move-result-object v2

    .line 276
    invoke-direct {p0, p1, v2}, Lcom/intsig/office/fc/xls/ChartConverter;->getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D

    .line 277
    .line 278
    .line 279
    move-result-wide v2

    .line 280
    goto :goto_7

    .line 281
    :cond_7
    move-wide v2, v4

    .line 282
    :goto_7
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(D)V

    .line 283
    .line 284
    .line 285
    add-int/lit8 v0, v0, 0x1

    .line 286
    .line 287
    goto :goto_6

    .line 288
    :cond_8
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 289
    .line 290
    .line 291
    move-result v0

    .line 292
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 293
    .line 294
    .line 295
    move-result v2

    .line 296
    if-ne v0, v2, :cond_a

    .line 297
    .line 298
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 299
    .line 300
    .line 301
    move-result v0

    .line 302
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 303
    .line 304
    .line 305
    move-result-object v0

    .line 306
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 307
    .line 308
    .line 309
    move-result v2

    .line 310
    :goto_8
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 311
    .line 312
    .line 313
    move-result v3

    .line 314
    if-gt v2, v3, :cond_a

    .line 315
    .line 316
    if-eqz v0, :cond_9

    .line 317
    .line 318
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/baseModel/Row;->getCell(I)Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 319
    .line 320
    .line 321
    move-result-object v3

    .line 322
    invoke-direct {p0, p1, v3}, Lcom/intsig/office/fc/xls/ChartConverter;->getCellNumericValue(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/baseModel/Cell;)D

    .line 323
    .line 324
    .line 325
    move-result-wide v6

    .line 326
    goto :goto_9

    .line 327
    :cond_9
    move-wide v6, v4

    .line 328
    :goto_9
    invoke-virtual {v1, v6, v7}, Lcom/intsig/office/thirdpart/achartengine/model/CategorySeries;->add(D)V

    .line 329
    .line 330
    .line 331
    add-int/lit8 v2, v2, 0x1

    .line 332
    .line 333
    goto :goto_8

    .line 334
    :cond_a
    return-object v1

    .line 335
    :cond_b
    :goto_a
    return-object v2
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method protected buildDefaultRenderer(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;-><init>()V

    .line 4
    .line 5
    .line 6
    const/high16 v1, 0x41700000    # 15.0f

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setLabelsTextSize(F)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setLegendTextSize(F)V

    .line 12
    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setShowGridH(Z)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getMarginColorFormat()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getMarginColorFormat()Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    invoke-virtual {v3}, Lcom/intsig/office/fc/hssf/record/chart/AreaFormatRecord;->getForecolorIndex()S

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    invoke-virtual {v2, v3}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    invoke-virtual {v0, v2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setBackgroundColor(I)V

    .line 41
    .line 42
    .line 43
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getSeries()[Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    array-length v2, p2

    .line 48
    if-lez v2, :cond_2

    .line 49
    .line 50
    const/4 v2, 0x0

    .line 51
    aget-object p2, p2, v2

    .line 52
    .line 53
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFSeries;->getDataValues()Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;

    .line 54
    .line 55
    .line 56
    move-result-object p2

    .line 57
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/LinkedDataRecord;->getFormulaOfLink()[Lcom/intsig/office/fc/hssf/formula/ptg/Ptg;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    array-length v3, p2

    .line 62
    if-lez v3, :cond_2

    .line 63
    .line 64
    aget-object p2, p2, v2

    .line 65
    .line 66
    instance-of v2, p2, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 67
    .line 68
    if-eqz v2, :cond_2

    .line 69
    .line 70
    check-cast p2, Lcom/intsig/office/fc/hssf/formula/ptg/Area3DPtg;

    .line 71
    .line 72
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 73
    .line 74
    .line 75
    move-result v2

    .line 76
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 77
    .line 78
    .line 79
    move-result v3

    .line 80
    if-ne v2, v3, :cond_1

    .line 81
    .line 82
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    if-gt v2, v3, :cond_2

    .line 91
    .line 92
    new-instance v3, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 93
    .line 94
    invoke-direct {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;-><init>()V

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 98
    .line 99
    .line 100
    move-result-object v4

    .line 101
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 102
    .line 103
    .line 104
    move-result v5

    .line 105
    sub-int v5, v2, v5

    .line 106
    .line 107
    add-int/lit8 v5, v5, 0x18

    .line 108
    .line 109
    invoke-virtual {v4, v5}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 110
    .line 111
    .line 112
    move-result v4

    .line 113
    invoke-virtual {v3, v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->addSeriesRenderer(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)V

    .line 117
    .line 118
    .line 119
    add-int/lit8 v2, v2, 0x1

    .line 120
    .line 121
    goto :goto_0

    .line 122
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstRow()I

    .line 123
    .line 124
    .line 125
    move-result v2

    .line 126
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastRow()I

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    if-ne v2, v3, :cond_2

    .line 131
    .line 132
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 133
    .line 134
    .line 135
    move-result v2

    .line 136
    :goto_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getLastColumn()I

    .line 137
    .line 138
    .line 139
    move-result v3

    .line 140
    if-gt v2, v3, :cond_2

    .line 141
    .line 142
    new-instance v3, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;

    .line 143
    .line 144
    invoke-direct {v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;-><init>()V

    .line 145
    .line 146
    .line 147
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 148
    .line 149
    .line 150
    move-result-object v4

    .line 151
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/formula/ptg/AreaPtgBase;->getFirstColumn()I

    .line 152
    .line 153
    .line 154
    move-result v5

    .line 155
    sub-int v5, v2, v5

    .line 156
    .line 157
    add-int/lit8 v5, v5, 0x18

    .line 158
    .line 159
    invoke-virtual {v4, v5}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getColor(I)I

    .line 160
    .line 161
    .line 162
    move-result v4

    .line 163
    invoke-virtual {v3, v4}, Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;->setColor(I)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v0, v3}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->addSeriesRenderer(Lcom/intsig/office/thirdpart/achartengine/renderers/SimpleSeriesRenderer;)V

    .line 167
    .line 168
    .line 169
    add-int/lit8 v2, v2, 0x1

    .line 170
    .line 171
    goto :goto_1

    .line 172
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/fc/xls/ChartConverter;->chartSeriesText:Ljava/util/Map;

    .line 173
    .line 174
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 175
    .line 176
    .line 177
    move-result-object p1

    .line 178
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 179
    .line 180
    .line 181
    move-result-object p1

    .line 182
    :cond_3
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 183
    .line 184
    .line 185
    move-result p2

    .line 186
    if-eqz p2, :cond_5

    .line 187
    .line 188
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 189
    .line 190
    .line 191
    move-result-object p2

    .line 192
    check-cast p2, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;

    .line 193
    .line 194
    iget-object v2, p0, Lcom/intsig/office/fc/xls/ChartConverter;->chartSeriesText:Ljava/util/Map;

    .line 195
    .line 196
    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    .line 198
    .line 199
    move-result-object v2

    .line 200
    check-cast v2, Lcom/intsig/office/fc/hssf/record/Record;

    .line 201
    .line 202
    instance-of v3, v2, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;

    .line 203
    .line 204
    if-eqz v3, :cond_3

    .line 205
    .line 206
    check-cast v2, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;

    .line 207
    .line 208
    invoke-virtual {v2}, Lcom/intsig/office/fc/hssf/record/chart/ObjectLinkRecord;->getAnchorId()S

    .line 209
    .line 210
    .line 211
    move-result v2

    .line 212
    if-eq v2, v1, :cond_4

    .line 213
    .line 214
    goto :goto_2

    .line 215
    :cond_4
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/record/chart/SeriesTextRecord;->getText()Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object p2

    .line 219
    invoke-virtual {v0, p2}, Lcom/intsig/office/thirdpart/achartengine/renderers/DefaultRenderer;->setChartTitle(Ljava/lang/String;)V

    .line 220
    .line 221
    .line 222
    goto :goto_2

    .line 223
    :cond_5
    return-object v0
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public converter(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/XLSModel/ASheet;->getAWorkbook()Lcom/intsig/office/ss/model/XLSModel/AWorkbook;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/XLSModel/AWorkbook;->getInternalWorkbook()Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->workbook:Lcom/intsig/office/fc/hssf/model/InternalWorkbook;

    .line 10
    .line 11
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    iput-wide v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->minY:D

    .line 17
    .line 18
    const-wide v0, -0x10000000000001L

    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    iput-wide v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->maxY:D

    .line 24
    .line 25
    invoke-virtual {p2}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getSeriesText()Ljava/util/Map;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iput-object v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->chartSeriesText:Ljava/util/Map;

    .line 30
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/ChartConverter;->convertToAChart(Lcom/intsig/office/ss/model/XLSModel/ASheet;Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-direct {p0}, Lcom/intsig/office/fc/xls/ChartConverter;->dispose()V

    .line 36
    .line 37
    .line 38
    return-object p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getAChart()Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/xls/ChartConverter;->chart:Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getChartType(Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;)S
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart;->getType()Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    sget-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->Area:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-ne p1, v0, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x0

    .line 18
    return p1

    .line 19
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->Bar:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-ne p1, v0, :cond_1

    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    return p1

    .line 29
    :cond_1
    sget-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->Line:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 30
    .line 31
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-ne p1, v0, :cond_2

    .line 36
    .line 37
    const/4 p1, 0x2

    .line 38
    return p1

    .line 39
    :cond_2
    sget-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->Pie:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-ne p1, v0, :cond_3

    .line 46
    .line 47
    const/4 p1, 0x3

    .line 48
    return p1

    .line 49
    :cond_3
    sget-object v0, Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;->Scatter:Lcom/intsig/office/fc/hssf/usermodel/HSSFChart$HSSFChartType;

    .line 50
    .line 51
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-ne p1, v0, :cond_4

    .line 56
    .line 57
    const/4 p1, 0x4

    .line 58
    return p1

    .line 59
    :cond_4
    const/16 p1, 0xa

    .line 60
    .line 61
    return p1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.class public Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;
.super Ljava/lang/Object;
.source "DrawingReader.java"


# static fields
.field private static reader:Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;


# instance fields
.field private chartList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;",
            ">;"
        }
    .end annotation
.end field

.field private drawingList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private offset:I

.field private sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

.field private smartArtList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/common/shape/SmartArt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->reader:Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->drawingList:Ljava/util/Map;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->drawingList:Ljava/util/Map;

    .line 12
    .line 13
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->chartList:Ljava/util/Map;

    .line 14
    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->chartList:Ljava/util/Map;

    .line 21
    .line 22
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->smartArtList:Ljava/util/Map;

    .line 23
    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->smartArtList:Ljava/util/Map;

    .line 30
    .line 31
    :cond_2
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private getCellAnchor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/AnchorPoint;
    .locals 6

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return-object p1

    .line 5
    :cond_0
    new-instance v0, Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v1, "col"

    .line 11
    .line 12
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    int-to-short v1, v1

    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setColumn(S)V

    .line 26
    .line 27
    .line 28
    const-string v1, "colOff"

    .line 29
    .line 30
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 39
    .line 40
    .line 41
    move-result-wide v1

    .line 42
    long-to-float v1, v1

    .line 43
    const/high16 v2, 0x42c00000    # 96.0f

    .line 44
    .line 45
    mul-float v1, v1, v2

    .line 46
    .line 47
    const v3, 0x495f3e00    # 914400.0f

    .line 48
    .line 49
    .line 50
    div-float/2addr v1, v3

    .line 51
    float-to-int v1, v1

    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setDX(I)V

    .line 53
    .line 54
    .line 55
    const-string v1, "row"

    .line 56
    .line 57
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setRow(I)V

    .line 70
    .line 71
    .line 72
    const-string v1, "rowOff"

    .line 73
    .line 74
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 83
    .line 84
    .line 85
    move-result-wide v4

    .line 86
    long-to-float p1, v4

    .line 87
    mul-float p1, p1, v2

    .line 88
    .line 89
    div-float/2addr p1, v3

    .line 90
    float-to-int p1, p1

    .line 91
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/drawing/AnchorPoint;->setDY(I)V

    .line 92
    .line 93
    .line 94
    return-object v0
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getCellAnchors(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object v9, p0

    .line 2
    if-eqz p4, :cond_4

    .line 3
    .line 4
    invoke-interface/range {p4 .. p4}, Lcom/intsig/office/fc/dom4j/Node;->hasContent()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    goto :goto_3

    .line 11
    :cond_0
    invoke-interface/range {p4 .. p4}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v10

    .line 15
    const/4 v0, 0x0

    .line 16
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_4

    .line 21
    .line 22
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/office/fc/dom4j/Element;

    .line 27
    .line 28
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    const-string v3, "twoCellAnchor"

    .line 33
    .line 34
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    if-eqz v2, :cond_1

    .line 39
    .line 40
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getTwoCellAnchor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/CellAnchor;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    goto :goto_1

    .line 45
    :cond_1
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    const-string v3, "oneCellAnchor"

    .line 50
    .line 51
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    if-eqz v2, :cond_2

    .line 56
    .line 57
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getOneCellAnchor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/CellAnchor;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    :cond_2
    :goto_1
    move-object v11, v0

    .line 62
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    .line 63
    .line 64
    .line 65
    move-result-object v12

    .line 66
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    if-eqz v0, :cond_3

    .line 71
    .line 72
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    move-object v4, v0

    .line 77
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 78
    .line 79
    const/4 v5, 0x0

    .line 80
    const/high16 v6, 0x3f800000    # 1.0f

    .line 81
    .line 82
    const/high16 v7, 0x3f800000    # 1.0f

    .line 83
    .line 84
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    iget-object v1, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 89
    .line 90
    invoke-virtual {v0, v1, v11}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/drawing/CellAnchor;)Lcom/intsig/office/java/awt/Rectangle;

    .line 91
    .line 92
    .line 93
    move-result-object v8

    .line 94
    move-object v0, p0

    .line 95
    move-object v1, p1

    .line 96
    move-object v2, p2

    .line 97
    move-object/from16 v3, p3

    .line 98
    .line 99
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FFLcom/intsig/office/java/awt/Rectangle;)V

    .line 100
    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_3
    move-object v0, v11

    .line 104
    goto :goto_0

    .line 105
    :cond_4
    :goto_3
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private getChart(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/AChart;
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "id"

    .line 4
    .line 5
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/office/common/shape/AChart;

    .line 12
    .line 13
    invoke-direct {v0}, Lcom/intsig/office/common/shape/AChart;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p2}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 17
    .line 18
    .line 19
    iget-object p2, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->chartList:Ljava/util/Map;

    .line 20
    .line 21
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    check-cast p1, Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/shape/AChart;->setAChart(Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V

    .line 28
    .line 29
    .line 30
    return-object v0

    .line 31
    :cond_0
    const/4 p1, 0x0

    .line 32
    return-object p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static getFont(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/simpletext/font/Font;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/simpletext/font/Font;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/simpletext/font/Font;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "sz"

    .line 7
    .line 8
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    div-int/lit8 v1, v1, 0x64

    .line 23
    .line 24
    int-to-double v1, v1

    .line 25
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/font/Font;->setFontSize(D)V

    .line 26
    .line 27
    .line 28
    :cond_0
    const-string v1, "b"

    .line 29
    .line 30
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    const/4 v3, 0x1

    .line 35
    if-eqz v2, :cond_1

    .line 36
    .line 37
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-eqz v1, :cond_1

    .line 46
    .line 47
    invoke-virtual {v0, v3}, Lcom/intsig/office/simpletext/font/Font;->setBold(Z)V

    .line 48
    .line 49
    .line 50
    :cond_1
    const-string v1, "i"

    .line 51
    .line 52
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    if-eqz v2, :cond_2

    .line 57
    .line 58
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-eqz v1, :cond_2

    .line 67
    .line 68
    invoke-virtual {v0, v3}, Lcom/intsig/office/simpletext/font/Font;->setItalic(Z)V

    .line 69
    .line 70
    .line 71
    :cond_2
    const-string v1, "u"

    .line 72
    .line 73
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    if-eqz v2, :cond_4

    .line 78
    .line 79
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    const-string v4, "sng"

    .line 84
    .line 85
    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 86
    .line 87
    .line 88
    move-result v2

    .line 89
    if-eqz v2, :cond_3

    .line 90
    .line 91
    invoke-virtual {v0, v3}, Lcom/intsig/office/simpletext/font/Font;->setUnderline(I)V

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_3
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    const-string v2, "dbl"

    .line 100
    .line 101
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 102
    .line 103
    .line 104
    move-result v1

    .line 105
    if-eqz v1, :cond_4

    .line 106
    .line 107
    const/4 v1, 0x2

    .line 108
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/font/Font;->setUnderline(I)V

    .line 109
    .line 110
    .line 111
    :cond_4
    :goto_0
    const-string v1, "strike"

    .line 112
    .line 113
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v2

    .line 117
    if-eqz v2, :cond_5

    .line 118
    .line 119
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    const-string v2, "noStrike"

    .line 124
    .line 125
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    if-nez v1, :cond_5

    .line 130
    .line 131
    invoke-virtual {v0, v3}, Lcom/intsig/office/simpletext/font/Font;->setStrikeline(Z)V

    .line 132
    .line 133
    .line 134
    :cond_5
    const-string v1, "solidFill"

    .line 135
    .line 136
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 137
    .line 138
    .line 139
    const/16 p0, 0x8

    .line 140
    .line 141
    invoke-virtual {v0, p0}, Lcom/intsig/office/simpletext/font/Font;->setColorIndex(I)V

    .line 142
    .line 143
    .line 144
    return-object v0
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static getHorizontalByString(Ljava/lang/String;)S
    .locals 1

    .line 1
    if-eqz p0, :cond_4

    .line 2
    .line 3
    const-string v0, "l"

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "ctr"

    .line 13
    .line 14
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    const/4 p0, 0x2

    .line 21
    return p0

    .line 22
    :cond_1
    const-string v0, "r"

    .line 23
    .line 24
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_2

    .line 29
    .line 30
    const/4 p0, 0x3

    .line 31
    return p0

    .line 32
    :cond_2
    const-string v0, "just"

    .line 33
    .line 34
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 35
    .line 36
    .line 37
    move-result p0

    .line 38
    if-eqz p0, :cond_3

    .line 39
    .line 40
    const/4 p0, 0x5

    .line 41
    return p0

    .line 42
    :cond_3
    const/4 p0, 0x0

    .line 43
    return p0

    .line 44
    :cond_4
    :goto_0
    const/4 p0, 0x1

    .line 45
    return p0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getImageData(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/PictureShape;
    .locals 6

    .line 1
    const-string v0, "blipFill"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-object v1

    .line 11
    :cond_0
    invoke-static {v0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getPictureEffectInfor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v3, "blip"

    .line 16
    .line 17
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    const-string v3, "embed"

    .line 24
    .line 25
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    if-eqz v4, :cond_1

    .line 30
    .line 31
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    if-eqz v4, :cond_1

    .line 36
    .line 37
    iget-object v4, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->drawingList:Ljava/util/Map;

    .line 38
    .line 39
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v5

    .line 43
    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    if-eqz v4, :cond_1

    .line 48
    .line 49
    new-instance v1, Lcom/intsig/office/common/shape/PictureShape;

    .line 50
    .line 51
    invoke-direct {v1}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 52
    .line 53
    .line 54
    iget-object v4, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->drawingList:Ljava/util/Map;

    .line 55
    .line 56
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    check-cast v0, Ljava/lang/Integer;

    .line 65
    .line 66
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    invoke-virtual {v1, p2}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 77
    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    const-string v0, "spPr"

    .line 84
    .line 85
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-virtual {p2, p1, v1}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/IShape;)V

    .line 90
    .line 91
    .line 92
    :cond_1
    return-object v1
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private getOneCellAnchor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/CellAnchor;
    .locals 6

    .line 1
    const-string v0, "from"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getCellAnchor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Lcom/intsig/office/ss/model/drawing/CellAnchor;

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-direct {v1, v2}, Lcom/intsig/office/ss/model/drawing/CellAnchor;-><init>(S)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/drawing/CellAnchor;->setStart(Lcom/intsig/office/ss/model/drawing/AnchorPoint;)V

    .line 18
    .line 19
    .line 20
    const-string v0, "ext"

    .line 21
    .line 22
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const-string v0, "cx"

    .line 27
    .line 28
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 33
    .line 34
    .line 35
    move-result-wide v2

    .line 36
    long-to-float v0, v2

    .line 37
    const/high16 v2, 0x42c00000    # 96.0f

    .line 38
    .line 39
    mul-float v0, v0, v2

    .line 40
    .line 41
    const v3, 0x495f3e00    # 914400.0f

    .line 42
    .line 43
    .line 44
    div-float/2addr v0, v3

    .line 45
    float-to-int v0, v0

    .line 46
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/drawing/CellAnchor;->setWidth(I)V

    .line 47
    .line 48
    .line 49
    const-string v0, "cy"

    .line 50
    .line 51
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 56
    .line 57
    .line 58
    move-result-wide v4

    .line 59
    long-to-float p1, v4

    .line 60
    mul-float p1, p1, v2

    .line 61
    .line 62
    div-float/2addr p1, v3

    .line 63
    float-to-int p1, p1

    .line 64
    invoke-virtual {v1, p1}, Lcom/intsig/office/ss/model/drawing/CellAnchor;->setHeight(I)V

    .line 65
    .line 66
    .line 67
    return-object v1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getSmartArt(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/SmartArt;
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    :try_start_0
    const-string v1, "relIds"

    .line 5
    .line 6
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const-string v1, "dm"

    .line 11
    .line 12
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const/4 v1, 0x3

    .line 17
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 22
    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->smartArtList:Ljava/util/Map;

    .line 25
    .line 26
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    check-cast p1, Lcom/intsig/office/common/shape/SmartArt;

    .line 31
    .line 32
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    .line 34
    .line 35
    return-object p1

    .line 36
    :catch_0
    :cond_0
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private getTextBoxData(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/TextBox;
    .locals 10

    .line 1
    new-instance v0, Lcom/intsig/office/common/shape/TextBox;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/common/shape/TextBox;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 7
    .line 8
    invoke-direct {v1}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 9
    .line 10
    .line 11
    const-wide/16 v2, 0x0

    .line 12
    .line 13
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/TextBox;->setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 20
    .line 21
    .line 22
    move-result-object v6

    .line 23
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    iget v3, p3, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 28
    .line 29
    int-to-float v3, v3

    .line 30
    const/high16 v4, 0x41700000    # 15.0f

    .line 31
    .line 32
    mul-float v3, v3, v4

    .line 33
    .line 34
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    invoke-virtual {v2, v6, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 39
    .line 40
    .line 41
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    iget v3, p3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 46
    .line 47
    int-to-float v3, v3

    .line 48
    mul-float v3, v3, v4

    .line 49
    .line 50
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    invoke-virtual {v2, v6, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 55
    .line 56
    .line 57
    const-string v2, "txBody"

    .line 58
    .line 59
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    if-eqz v2, :cond_3

    .line 64
    .line 65
    new-instance v7, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 66
    .line 67
    invoke-direct {v7}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 68
    .line 69
    .line 70
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    const/high16 v4, 0x43100000    # 144.0f

    .line 75
    .line 76
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 77
    .line 78
    .line 79
    move-result v5

    .line 80
    invoke-virtual {v3, v7, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 81
    .line 82
    .line 83
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 84
    .line 85
    .line 86
    move-result-object v3

    .line 87
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    invoke-virtual {v3, v7, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 92
    .line 93
    .line 94
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 95
    .line 96
    .line 97
    move-result-object v3

    .line 98
    const/high16 v4, 0x42900000    # 72.0f

    .line 99
    .line 100
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    invoke-virtual {v3, v7, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 105
    .line 106
    .line 107
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    .line 112
    .line 113
    .line 114
    move-result v4

    .line 115
    invoke-virtual {v3, v7, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 116
    .line 117
    .line 118
    const-string v3, "bodyPr"

    .line 119
    .line 120
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 121
    .line 122
    .line 123
    move-result-object v3

    .line 124
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/SectionAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/SectionAttr;

    .line 125
    .line 126
    .line 127
    move-result-object v4

    .line 128
    const/4 v8, 0x0

    .line 129
    const/4 v9, 0x0

    .line 130
    move-object v5, v3

    .line 131
    invoke-virtual/range {v4 .. v9}, Lcom/intsig/office/fc/ppt/attribute/SectionAttr;->setSectionAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 132
    .line 133
    .line 134
    if-eqz v3, :cond_2

    .line 135
    .line 136
    const-string v4, "wrap"

    .line 137
    .line 138
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v3

    .line 142
    if-eqz v3, :cond_1

    .line 143
    .line 144
    const-string v4, "square"

    .line 145
    .line 146
    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 147
    .line 148
    .line 149
    move-result v3

    .line 150
    if-eqz v3, :cond_0

    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_0
    const/4 v3, 0x0

    .line 154
    goto :goto_1

    .line 155
    :cond_1
    :goto_0
    const/4 v3, 0x1

    .line 156
    :goto_1
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/TextBox;->setWrapLine(Z)V

    .line 157
    .line 158
    .line 159
    :cond_2
    invoke-direct {p0, p1, v1, v2}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->processParagraph(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;)I

    .line 160
    .line 161
    .line 162
    move-result p1

    .line 163
    int-to-long v2, p1

    .line 164
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 165
    .line 166
    .line 167
    :cond_3
    invoke-virtual {v0, p3}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    const/4 p3, 0x0

    .line 175
    if-eqz p1, :cond_4

    .line 176
    .line 177
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 178
    .line 179
    .line 180
    move-result-object p1

    .line 181
    invoke-virtual {p1, p3}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object p1

    .line 185
    if-eqz p1, :cond_4

    .line 186
    .line 187
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 188
    .line 189
    .line 190
    move-result-object p1

    .line 191
    invoke-virtual {p1, p3}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object p1

    .line 195
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 196
    .line 197
    .line 198
    move-result p1

    .line 199
    if-lez p1, :cond_4

    .line 200
    .line 201
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 202
    .line 203
    .line 204
    move-result-object p1

    .line 205
    invoke-virtual {p1, p3}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object p1

    .line 209
    const-string v1, "\n"

    .line 210
    .line 211
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 212
    .line 213
    .line 214
    move-result p1

    .line 215
    if-nez p1, :cond_4

    .line 216
    .line 217
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 218
    .line 219
    .line 220
    move-result-object p1

    .line 221
    const-string p3, "spPr"

    .line 222
    .line 223
    invoke-interface {p2, p3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 224
    .line 225
    .line 226
    move-result-object p2

    .line 227
    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/IShape;)V

    .line 228
    .line 229
    .line 230
    return-object v0

    .line 231
    :cond_4
    return-object p3
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method public static getTextParagraph(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/TextParagraph;
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/office/ss/model/drawing/TextParagraph;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/ss/model/drawing/TextParagraph;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "pPr"

    .line 7
    .line 8
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    const-string v2, "algn"

    .line 15
    .line 16
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v1}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getHorizontalByString(Ljava/lang/String;)S

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->setHorizontalAlign(S)V

    .line 25
    .line 26
    .line 27
    :cond_0
    const-string v1, "r"

    .line 28
    .line 29
    invoke-interface {p0, v1}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object p0

    .line 33
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 34
    .line 35
    .line 36
    move-result-object p0

    .line 37
    const/4 v1, 0x0

    .line 38
    const-string v2, ""

    .line 39
    .line 40
    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-eqz v3, :cond_3

    .line 45
    .line 46
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    check-cast v3, Lcom/intsig/office/fc/dom4j/Element;

    .line 51
    .line 52
    if-nez v1, :cond_2

    .line 53
    .line 54
    const-string v4, "rPr"

    .line 55
    .line 56
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    if-eqz v4, :cond_2

    .line 61
    .line 62
    invoke-static {v4}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getFont(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/simpletext/font/Font;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    :cond_2
    const-string v4, "t"

    .line 67
    .line 68
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 69
    .line 70
    .line 71
    move-result-object v5

    .line 72
    if-eqz v5, :cond_1

    .line 73
    .line 74
    new-instance v5, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    invoke-interface {v2}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    goto :goto_0

    .line 98
    :cond_3
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->setFont(Lcom/intsig/office/simpletext/font/Font;)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/drawing/TextParagraph;->setTextRun(Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    return-object v0
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getTwoCellAnchor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/CellAnchor;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/ss/model/drawing/CellAnchor;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/office/ss/model/drawing/CellAnchor;-><init>(S)V

    .line 5
    .line 6
    .line 7
    const-string v1, "from"

    .line 8
    .line 9
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getCellAnchor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/drawing/CellAnchor;->setStart(Lcom/intsig/office/ss/model/drawing/AnchorPoint;)V

    .line 18
    .line 19
    .line 20
    const-string v1, "to"

    .line 21
    .line 22
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getCellAnchor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/drawing/AnchorPoint;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/drawing/CellAnchor;->setEnd(Lcom/intsig/office/ss/model/drawing/AnchorPoint;)V

    .line 31
    .line 32
    .line 33
    return-object v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static getVerticalByString(Ljava/lang/String;)S
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p0, :cond_4

    .line 3
    .line 4
    const-string v1, "ctr"

    .line 5
    .line 6
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const-string v1, "t"

    .line 14
    .line 15
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    const/4 p0, 0x0

    .line 22
    return p0

    .line 23
    :cond_1
    const-string v1, "b"

    .line 24
    .line 25
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_2

    .line 30
    .line 31
    const/4 p0, 0x2

    .line 32
    return p0

    .line 33
    :cond_2
    const-string v1, "just"

    .line 34
    .line 35
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    const/4 v2, 0x3

    .line 40
    if-eqz v1, :cond_3

    .line 41
    .line 42
    return v2

    .line 43
    :cond_3
    const-string v1, "dist"

    .line 44
    .line 45
    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 46
    .line 47
    .line 48
    move-result p0

    .line 49
    if-eqz p0, :cond_4

    .line 50
    .line 51
    return v2

    .line 52
    :cond_4
    :goto_0
    return v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static instance()Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->reader:Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getOffX()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v0, v1

    .line 10
    iput v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 11
    .line 12
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getOffY()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    add-int/2addr v0, p1

    .line 19
    iput v0, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 20
    .line 21
    :cond_0
    return-object p2
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private processParagraph(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;)I
    .locals 18

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    iput v0, v6, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 5
    .line 6
    const-string v0, "p"

    .line 7
    .line 8
    move-object/from16 v1, p3

    .line 9
    .line 10
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v7

    .line 18
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    move-object v4, v0

    .line 29
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    const-string v0, "pPr"

    .line 32
    .line 33
    invoke-interface {v4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 34
    .line 35
    .line 36
    move-result-object v10

    .line 37
    new-instance v3, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 38
    .line 39
    invoke-direct {v3}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 40
    .line 41
    .line 42
    iget v0, v6, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 43
    .line 44
    int-to-long v0, v0

    .line 45
    invoke-virtual {v3, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 46
    .line 47
    .line 48
    const/4 v5, 0x0

    .line 49
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 50
    .line 51
    .line 52
    move-result-object v8

    .line 53
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 54
    .line 55
    .line 56
    move-result-object v11

    .line 57
    const/4 v13, -0x1

    .line 58
    const/4 v14, -0x1

    .line 59
    const/4 v15, 0x0

    .line 60
    const/16 v16, 0x0

    .line 61
    .line 62
    const/16 v17, 0x0

    .line 63
    .line 64
    const/4 v12, 0x0

    .line 65
    move-object/from16 v9, p1

    .line 66
    .line 67
    invoke-virtual/range {v8 .. v17}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIIZZ)V

    .line 68
    .line 69
    .line 70
    move-object/from16 v0, p0

    .line 71
    .line 72
    move-object/from16 v1, p1

    .line 73
    .line 74
    move-object/from16 v2, p2

    .line 75
    .line 76
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->processRun(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    iget v1, v6, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 81
    .line 82
    int-to-long v1, v1

    .line 83
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 84
    .line 85
    .line 86
    const-wide/16 v1, 0x0

    .line 87
    .line 88
    move-object/from16 v3, p2

    .line 89
    .line 90
    invoke-virtual {v3, v0, v1, v2}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_0
    iget v0, v6, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 95
    .line 96
    return v0
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private processRun(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)Lcom/intsig/office/simpletext/model/ParagraphElement;
    .locals 24

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p3

    .line 4
    .line 5
    move-object/from16 v2, p4

    .line 6
    .line 7
    const-string v3, "r"

    .line 8
    .line 9
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v4

    .line 13
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v5

    .line 17
    const-string v6, "rPr"

    .line 18
    .line 19
    const-string v7, "pPr"

    .line 20
    .line 21
    const-string v8, "\n"

    .line 22
    .line 23
    if-nez v5, :cond_1

    .line 24
    .line 25
    new-instance v3, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 26
    .line 27
    invoke-direct {v3, v8}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-interface {v2, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    if-eqz v2, :cond_0

    .line 35
    .line 36
    invoke-interface {v2, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    if-eqz v2, :cond_0

    .line 41
    .line 42
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 43
    .line 44
    .line 45
    move-result-object v4

    .line 46
    iget-object v5, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 47
    .line 48
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 49
    .line 50
    .line 51
    move-result-object v6

    .line 52
    move-object/from16 v9, p5

    .line 53
    .line 54
    invoke-virtual {v4, v5, v2, v6, v9}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 55
    .line 56
    .line 57
    :cond_0
    iget v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 58
    .line 59
    int-to-long v4, v2

    .line 60
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 61
    .line 62
    .line 63
    iget v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 64
    .line 65
    add-int/lit8 v2, v2, 0x1

    .line 66
    .line 67
    iput v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 68
    .line 69
    int-to-long v4, v2

    .line 70
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v3}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 74
    .line 75
    .line 76
    return-object v1

    .line 77
    :cond_1
    move-object/from16 v9, p5

    .line 78
    .line 79
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 80
    .line 81
    .line 82
    move-result-object v4

    .line 83
    const/4 v5, 0x0

    .line 84
    move-object v10, v5

    .line 85
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 86
    .line 87
    .line 88
    move-result v11

    .line 89
    if-eqz v11, :cond_5

    .line 90
    .line 91
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    move-result-object v11

    .line 95
    check-cast v11, Lcom/intsig/office/fc/dom4j/Element;

    .line 96
    .line 97
    invoke-interface {v11}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v12

    .line 101
    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 102
    .line 103
    .line 104
    move-result v12

    .line 105
    if-eqz v12, :cond_2

    .line 106
    .line 107
    const-string v12, "t"

    .line 108
    .line 109
    invoke-interface {v11, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 110
    .line 111
    .line 112
    move-result-object v12

    .line 113
    if-eqz v12, :cond_4

    .line 114
    .line 115
    invoke-interface {v12}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v12

    .line 119
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    .line 120
    .line 121
    .line 122
    move-result v13

    .line 123
    if-ltz v13, :cond_4

    .line 124
    .line 125
    new-instance v10, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 126
    .line 127
    invoke-direct {v10, v12}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 131
    .line 132
    .line 133
    move-result-object v12

    .line 134
    iget-object v14, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 135
    .line 136
    invoke-interface {v11, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 137
    .line 138
    .line 139
    move-result-object v11

    .line 140
    invoke-virtual {v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 141
    .line 142
    .line 143
    move-result-object v15

    .line 144
    invoke-virtual {v12, v14, v11, v15, v9}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 145
    .line 146
    .line 147
    iget v11, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 148
    .line 149
    int-to-long v11, v11

    .line 150
    invoke-virtual {v10, v11, v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 151
    .line 152
    .line 153
    iget v11, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 154
    .line 155
    add-int/2addr v11, v13

    .line 156
    iput v11, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 157
    .line 158
    int-to-long v11, v11

    .line 159
    invoke-virtual {v10, v11, v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v1, v10}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 163
    .line 164
    .line 165
    goto :goto_1

    .line 166
    :cond_2
    invoke-interface {v11}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v11

    .line 170
    const-string v12, "br"

    .line 171
    .line 172
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 173
    .line 174
    .line 175
    move-result v11

    .line 176
    if-eqz v11, :cond_4

    .line 177
    .line 178
    if-eqz v10, :cond_3

    .line 179
    .line 180
    new-instance v9, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v10, v5}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v11

    .line 189
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v9

    .line 199
    invoke-virtual {v10, v9}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    iget v9, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 203
    .line 204
    add-int/lit8 v9, v9, 0x1

    .line 205
    .line 206
    iput v9, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 207
    .line 208
    :cond_3
    iget v9, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 209
    .line 210
    int-to-long v11, v9

    .line 211
    invoke-virtual {v1, v11, v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 212
    .line 213
    .line 214
    const-wide/16 v11, 0x0

    .line 215
    .line 216
    move-object/from16 v13, p2

    .line 217
    .line 218
    invoke-virtual {v13, v1, v11, v12}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 219
    .line 220
    .line 221
    new-instance v1, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 222
    .line 223
    invoke-direct {v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 224
    .line 225
    .line 226
    iget v9, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 227
    .line 228
    int-to-long v11, v9

    .line 229
    invoke-virtual {v1, v11, v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 230
    .line 231
    .line 232
    const/4 v9, 0x0

    .line 233
    invoke-interface {v2, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 234
    .line 235
    .line 236
    move-result-object v16

    .line 237
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 238
    .line 239
    .line 240
    move-result-object v14

    .line 241
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 242
    .line 243
    .line 244
    move-result-object v17

    .line 245
    const/16 v19, -0x1

    .line 246
    .line 247
    const/16 v20, -0x1

    .line 248
    .line 249
    const/16 v21, 0x0

    .line 250
    .line 251
    const/16 v22, 0x0

    .line 252
    .line 253
    const/16 v23, 0x0

    .line 254
    .line 255
    move-object/from16 v15, p1

    .line 256
    .line 257
    move-object/from16 v18, v9

    .line 258
    .line 259
    invoke-virtual/range {v14 .. v23}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIIZZ)V

    .line 260
    .line 261
    .line 262
    goto/16 :goto_0

    .line 263
    .line 264
    :cond_4
    :goto_1
    move-object/from16 v13, p2

    .line 265
    .line 266
    goto/16 :goto_0

    .line 267
    .line 268
    :cond_5
    if-eqz v10, :cond_6

    .line 269
    .line 270
    new-instance v2, Ljava/lang/StringBuilder;

    .line 271
    .line 272
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 273
    .line 274
    .line 275
    invoke-virtual {v10, v5}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 276
    .line 277
    .line 278
    move-result-object v3

    .line 279
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    .line 281
    .line 282
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    .line 284
    .line 285
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object v2

    .line 289
    invoke-virtual {v10, v2}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 290
    .line 291
    .line 292
    iget v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 293
    .line 294
    add-int/lit8 v2, v2, 0x1

    .line 295
    .line 296
    iput v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->offset:I

    .line 297
    .line 298
    :cond_6
    return-object v1
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FFLcom/intsig/office/java/awt/Rectangle;)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object/from16 v9, p0

    .line 2
    .line 3
    move-object/from16 v7, p4

    .line 4
    .line 5
    move-object/from16 v10, p5

    .line 6
    .line 7
    move/from16 v11, p6

    .line 8
    .line 9
    move/from16 v12, p7

    .line 10
    .line 11
    move-object/from16 v0, p8

    .line 12
    .line 13
    invoke-interface/range {p4 .. p4}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const-string v2, "grpSp"

    .line 18
    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    const-string v3, "xfrm"

    .line 24
    .line 25
    const/4 v4, 0x0

    .line 26
    if-eqz v2, :cond_6

    .line 27
    .line 28
    const-string v1, "grpSpPr"

    .line 29
    .line 30
    invoke-interface {v7, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const/4 v13, 0x1

    .line 35
    const/4 v14, 0x0

    .line 36
    if-eqz v1, :cond_2

    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 43
    .line 44
    .line 45
    move-result-object v4

    .line 46
    invoke-virtual {v2, v4, v11, v12}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    iget v4, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 51
    .line 52
    if-eqz v4, :cond_1

    .line 53
    .line 54
    iget v4, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 55
    .line 56
    if-nez v4, :cond_0

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    invoke-direct {v9, v10, v2}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 68
    .line 69
    .line 70
    move-result-object v5

    .line 71
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getAnchorFitZoom(Lcom/intsig/office/fc/dom4j/Element;)[F

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 76
    .line 77
    .line 78
    move-result-object v5

    .line 79
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    aget v6, v2, v14

    .line 84
    .line 85
    mul-float v6, v6, v11

    .line 86
    .line 87
    aget v8, v2, v13

    .line 88
    .line 89
    mul-float v8, v8, v12

    .line 90
    .line 91
    invoke-virtual {v5, v3, v6, v8}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getChildShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 92
    .line 93
    .line 94
    move-result-object v3

    .line 95
    new-instance v5, Lcom/intsig/office/common/shape/GroupShape;

    .line 96
    .line 97
    invoke-direct {v5}, Lcom/intsig/office/common/shape/GroupShape;-><init>()V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v5, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 101
    .line 102
    .line 103
    iget v6, v4, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 104
    .line 105
    iget v8, v3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 106
    .line 107
    sub-int/2addr v6, v8

    .line 108
    iget v8, v4, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 109
    .line 110
    iget v3, v3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 111
    .line 112
    sub-int/2addr v8, v3

    .line 113
    invoke-virtual {v5, v6, v8}, Lcom/intsig/office/common/shape/GroupShape;->setOffPostion(II)V

    .line 114
    .line 115
    .line 116
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 117
    .line 118
    .line 119
    move-result-object v3

    .line 120
    invoke-virtual {v3, v1, v5}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/IShape;)V

    .line 121
    .line 122
    .line 123
    move-object/from16 v16, v2

    .line 124
    .line 125
    move-object v15, v5

    .line 126
    goto :goto_1

    .line 127
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    move-object v15, v4

    .line 129
    move-object/from16 v16, v15

    .line 130
    .line 131
    :goto_1
    if-nez v10, :cond_3

    .line 132
    .line 133
    move-object v13, v0

    .line 134
    goto :goto_2

    .line 135
    :cond_3
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    .line 136
    .line 137
    invoke-direct {v1}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 138
    .line 139
    .line 140
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 141
    .line 142
    .line 143
    move-result-object v2

    .line 144
    iget v3, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 145
    .line 146
    iget v5, v4, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 147
    .line 148
    iget v6, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 149
    .line 150
    sub-int/2addr v5, v6

    .line 151
    iget v6, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 152
    .line 153
    mul-int v5, v5, v6

    .line 154
    .line 155
    iget v8, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 156
    .line 157
    div-int/2addr v5, v8

    .line 158
    add-int/2addr v3, v5

    .line 159
    iput v3, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 160
    .line 161
    iget v3, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 162
    .line 163
    iget v5, v4, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 164
    .line 165
    iget v13, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 166
    .line 167
    sub-int/2addr v5, v13

    .line 168
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 169
    .line 170
    mul-int v5, v5, v0

    .line 171
    .line 172
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 173
    .line 174
    div-int/2addr v5, v2

    .line 175
    add-int/2addr v3, v5

    .line 176
    iput v3, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 177
    .line 178
    iget v3, v4, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 179
    .line 180
    mul-int v6, v6, v3

    .line 181
    .line 182
    div-int/2addr v6, v8

    .line 183
    iput v6, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 184
    .line 185
    iget v3, v4, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 186
    .line 187
    mul-int v0, v0, v3

    .line 188
    .line 189
    div-int/2addr v0, v2

    .line 190
    iput v0, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 191
    .line 192
    move-object v13, v1

    .line 193
    :goto_2
    invoke-interface/range {p4 .. p4}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    .line 194
    .line 195
    .line 196
    move-result-object v18

    .line 197
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    .line 198
    .line 199
    .line 200
    move-result v0

    .line 201
    if-eqz v0, :cond_4

    .line 202
    .line 203
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 204
    .line 205
    .line 206
    move-result-object v0

    .line 207
    move-object v4, v0

    .line 208
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 209
    .line 210
    aget v0, v16, v14

    .line 211
    .line 212
    mul-float v6, v0, v11

    .line 213
    .line 214
    const/16 v17, 0x1

    .line 215
    .line 216
    aget v0, v16, v17

    .line 217
    .line 218
    mul-float v7, v0, v12

    .line 219
    .line 220
    move-object/from16 v0, p0

    .line 221
    .line 222
    move-object/from16 v1, p1

    .line 223
    .line 224
    move-object/from16 v2, p2

    .line 225
    .line 226
    move-object/from16 v3, p3

    .line 227
    .line 228
    move-object v5, v15

    .line 229
    move-object v8, v13

    .line 230
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FFLcom/intsig/office/java/awt/Rectangle;)V

    .line 231
    .line 232
    .line 233
    goto :goto_3

    .line 234
    :cond_4
    invoke-virtual {v15, v13}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 235
    .line 236
    .line 237
    if-nez v10, :cond_5

    .line 238
    .line 239
    iget-object v0, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 240
    .line 241
    invoke-virtual {v0, v15}, Lcom/intsig/office/ss/model/baseModel/Sheet;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 242
    .line 243
    .line 244
    goto/16 :goto_a

    .line 245
    .line 246
    :cond_5
    invoke-virtual {v10, v15}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 247
    .line 248
    .line 249
    goto/16 :goto_a

    .line 250
    .line 251
    :cond_6
    const-string v2, "AlternateContent"

    .line 252
    .line 253
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 254
    .line 255
    .line 256
    move-result v2

    .line 257
    if-eqz v2, :cond_7

    .line 258
    .line 259
    const-string v0, "Choice"

    .line 260
    .line 261
    invoke-interface {v7, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 262
    .line 263
    .line 264
    move-result-object v0

    .line 265
    if-eqz v0, :cond_16

    .line 266
    .line 267
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    .line 268
    .line 269
    .line 270
    move-result-object v13

    .line 271
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    .line 272
    .line 273
    .line 274
    move-result v0

    .line 275
    if-eqz v0, :cond_16

    .line 276
    .line 277
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 278
    .line 279
    .line 280
    move-result-object v0

    .line 281
    move-object v4, v0

    .line 282
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 283
    .line 284
    const/4 v8, 0x0

    .line 285
    move-object/from16 v0, p0

    .line 286
    .line 287
    move-object/from16 v1, p1

    .line 288
    .line 289
    move-object/from16 v2, p2

    .line 290
    .line 291
    move-object/from16 v3, p3

    .line 292
    .line 293
    move-object/from16 v5, p5

    .line 294
    .line 295
    move/from16 v6, p6

    .line 296
    .line 297
    move/from16 v7, p7

    .line 298
    .line 299
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FFLcom/intsig/office/java/awt/Rectangle;)V

    .line 300
    .line 301
    .line 302
    goto :goto_4

    .line 303
    :cond_7
    const-string v2, "spPr"

    .line 304
    .line 305
    if-nez v10, :cond_8

    .line 306
    .line 307
    move-object v8, v0

    .line 308
    goto :goto_5

    .line 309
    :cond_8
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 310
    .line 311
    .line 312
    move-result-object v5

    .line 313
    if-nez v5, :cond_9

    .line 314
    .line 315
    return-void

    .line 316
    :cond_9
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 317
    .line 318
    .line 319
    move-result-object v6

    .line 320
    invoke-interface {v5, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 321
    .line 322
    .line 323
    move-result-object v3

    .line 324
    invoke-virtual {v6, v3, v11, v12}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 325
    .line 326
    .line 327
    move-result-object v3

    .line 328
    invoke-direct {v9, v10, v3}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 329
    .line 330
    .line 331
    move-result-object v3

    .line 332
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 333
    .line 334
    .line 335
    move-result-object v5

    .line 336
    iget v6, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 337
    .line 338
    iget v8, v3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 339
    .line 340
    iget v11, v5, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 341
    .line 342
    sub-int/2addr v8, v11

    .line 343
    iget v11, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 344
    .line 345
    mul-int v8, v8, v11

    .line 346
    .line 347
    iget v12, v5, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 348
    .line 349
    div-int/2addr v8, v12

    .line 350
    add-int/2addr v6, v8

    .line 351
    iput v6, v3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 352
    .line 353
    iget v6, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 354
    .line 355
    iget v8, v3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 356
    .line 357
    iget v13, v5, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 358
    .line 359
    sub-int/2addr v8, v13

    .line 360
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 361
    .line 362
    mul-int v8, v8, v0

    .line 363
    .line 364
    iget v5, v5, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 365
    .line 366
    div-int/2addr v8, v5

    .line 367
    add-int/2addr v6, v8

    .line 368
    iput v6, v3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 369
    .line 370
    iget v6, v3, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 371
    .line 372
    mul-int v11, v11, v6

    .line 373
    .line 374
    div-int/2addr v11, v12

    .line 375
    iput v11, v3, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 376
    .line 377
    iget v6, v3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 378
    .line 379
    mul-int v0, v0, v6

    .line 380
    .line 381
    div-int/2addr v0, v5

    .line 382
    iput v0, v3, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 383
    .line 384
    move-object v8, v3

    .line 385
    :goto_5
    iget-object v0, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 386
    .line 387
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getSheetType()S

    .line 388
    .line 389
    .line 390
    move-result v0

    .line 391
    if-nez v0, :cond_a

    .line 392
    .line 393
    if-nez v8, :cond_a

    .line 394
    .line 395
    return-void

    .line 396
    :cond_a
    const-string v0, "sp"

    .line 397
    .line 398
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 399
    .line 400
    .line 401
    move-result v0

    .line 402
    if-nez v0, :cond_11

    .line 403
    .line 404
    const-string v0, "cxnSp"

    .line 405
    .line 406
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 407
    .line 408
    .line 409
    move-result v0

    .line 410
    if-eqz v0, :cond_b

    .line 411
    .line 412
    goto/16 :goto_7

    .line 413
    .line 414
    :cond_b
    const-string v0, "pic"

    .line 415
    .line 416
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 417
    .line 418
    .line 419
    move-result v0

    .line 420
    if-eqz v0, :cond_d

    .line 421
    .line 422
    invoke-direct {v9, v7, v8}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getImageData(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/PictureShape;

    .line 423
    .line 424
    .line 425
    move-result-object v4

    .line 426
    if-eqz v4, :cond_15

    .line 427
    .line 428
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 429
    .line 430
    .line 431
    move-result-object v14

    .line 432
    iget-object v0, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 433
    .line 434
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 435
    .line 436
    .line 437
    move-result-object v0

    .line 438
    invoke-static {v0}, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->getSchemeColor(Lcom/intsig/office/ss/model/baseModel/Workbook;)Ljava/util/Map;

    .line 439
    .line 440
    .line 441
    move-result-object v15

    .line 442
    move-object/from16 v11, p1

    .line 443
    .line 444
    move-object/from16 v12, p2

    .line 445
    .line 446
    move-object/from16 v13, p3

    .line 447
    .line 448
    move-object/from16 v16, v4

    .line 449
    .line 450
    invoke-static/range {v11 .. v16}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processPictureShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;Lcom/intsig/office/common/shape/PictureShape;)V

    .line 451
    .line 452
    .line 453
    if-nez v10, :cond_c

    .line 454
    .line 455
    iget-object v0, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 456
    .line 457
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 458
    .line 459
    .line 460
    goto/16 :goto_9

    .line 461
    .line 462
    :cond_c
    invoke-virtual {v10, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 463
    .line 464
    .line 465
    goto/16 :goto_9

    .line 466
    .line 467
    :cond_d
    const-string v0, "graphicFrame"

    .line 468
    .line 469
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 470
    .line 471
    .line 472
    move-result v0

    .line 473
    if-eqz v0, :cond_15

    .line 474
    .line 475
    const-string v0, "graphic"

    .line 476
    .line 477
    invoke-interface {v7, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 478
    .line 479
    .line 480
    move-result-object v0

    .line 481
    if-eqz v0, :cond_15

    .line 482
    .line 483
    const-string v1, "graphicData"

    .line 484
    .line 485
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 486
    .line 487
    .line 488
    move-result-object v0

    .line 489
    if-eqz v0, :cond_15

    .line 490
    .line 491
    const-string v1, "uri"

    .line 492
    .line 493
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 494
    .line 495
    .line 496
    move-result-object v3

    .line 497
    if-eqz v3, :cond_15

    .line 498
    .line 499
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 500
    .line 501
    .line 502
    move-result-object v1

    .line 503
    const-string v3, "http://schemas.openxmlformats.org/drawingml/2006/chart"

    .line 504
    .line 505
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 506
    .line 507
    .line 508
    move-result v3

    .line 509
    if-eqz v3, :cond_e

    .line 510
    .line 511
    const-string v1, "chart"

    .line 512
    .line 513
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 514
    .line 515
    .line 516
    move-result-object v0

    .line 517
    invoke-direct {v9, v0, v8}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getChart(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/AChart;

    .line 518
    .line 519
    .line 520
    move-result-object v4

    .line 521
    goto :goto_6

    .line 522
    :cond_e
    const-string v3, "http://schemas.openxmlformats.org/drawingml/2006/diagram"

    .line 523
    .line 524
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 525
    .line 526
    .line 527
    move-result v1

    .line 528
    if-eqz v1, :cond_f

    .line 529
    .line 530
    invoke-direct {v9, v0, v8}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getSmartArt(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/SmartArt;

    .line 531
    .line 532
    .line 533
    move-result-object v4

    .line 534
    :cond_f
    :goto_6
    if-eqz v4, :cond_15

    .line 535
    .line 536
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 537
    .line 538
    .line 539
    move-result-object v0

    .line 540
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 541
    .line 542
    .line 543
    move-result-object v1

    .line 544
    invoke-virtual {v0, v1, v4}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/IShape;)V

    .line 545
    .line 546
    .line 547
    if-nez v10, :cond_10

    .line 548
    .line 549
    iget-object v0, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 550
    .line 551
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 552
    .line 553
    .line 554
    goto :goto_9

    .line 555
    :cond_10
    invoke-virtual {v10, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 556
    .line 557
    .line 558
    goto :goto_9

    .line 559
    :cond_11
    :goto_7
    iget-object v0, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 560
    .line 561
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 562
    .line 563
    .line 564
    move-result-object v0

    .line 565
    invoke-static {v0}, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->getSchemeColor(Lcom/intsig/office/ss/model/baseModel/Workbook;)Ljava/util/Map;

    .line 566
    .line 567
    .line 568
    move-result-object v5

    .line 569
    const/4 v6, 0x1

    .line 570
    move-object/from16 v0, p1

    .line 571
    .line 572
    move-object/from16 v1, p2

    .line 573
    .line 574
    move-object/from16 v2, p3

    .line 575
    .line 576
    move-object/from16 v3, p4

    .line 577
    .line 578
    move-object v4, v8

    .line 579
    invoke-static/range {v0 .. v6}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->getAutoShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;Ljava/util/Map;I)Lcom/intsig/office/common/shape/AbstractShape;

    .line 580
    .line 581
    .line 582
    move-result-object v4

    .line 583
    if-eqz v4, :cond_13

    .line 584
    .line 585
    if-nez v10, :cond_12

    .line 586
    .line 587
    iget-object v0, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 588
    .line 589
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 590
    .line 591
    .line 592
    goto :goto_8

    .line 593
    :cond_12
    invoke-virtual {v10, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 594
    .line 595
    .line 596
    :cond_13
    :goto_8
    move-object/from16 v0, p1

    .line 597
    .line 598
    invoke-direct {v9, v0, v7, v8}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getTextBoxData(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/TextBox;

    .line 599
    .line 600
    .line 601
    move-result-object v0

    .line 602
    if-eqz v0, :cond_15

    .line 603
    .line 604
    if-nez v10, :cond_14

    .line 605
    .line 606
    iget-object v1, v9, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 607
    .line 608
    invoke-virtual {v1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 609
    .line 610
    .line 611
    goto :goto_9

    .line 612
    :cond_14
    invoke-virtual {v10, v0}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 613
    .line 614
    .line 615
    :cond_15
    :goto_9
    if-eqz v4, :cond_16

    .line 616
    .line 617
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/AbstractShape;->getRotation()F

    .line 618
    .line 619
    .line 620
    move-result v0

    .line 621
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 622
    .line 623
    .line 624
    move-result v0

    .line 625
    const/high16 v1, 0x3f800000    # 1.0f

    .line 626
    .line 627
    cmpl-float v0, v0, v1

    .line 628
    .line 629
    if-lez v0, :cond_16

    .line 630
    .line 631
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 632
    .line 633
    .line 634
    move-result-object v0

    .line 635
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/AbstractShape;->getRotation()F

    .line 636
    .line 637
    .line 638
    move-result v1

    .line 639
    invoke-static {v0, v1}, Lcom/intsig/office/ss/util/ModelUtil;->processRect(Lcom/intsig/office/java/awt/Rectangle;F)Lcom/intsig/office/java/awt/Rectangle;

    .line 640
    .line 641
    .line 642
    move-result-object v0

    .line 643
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 644
    .line 645
    .line 646
    :cond_16
    :goto_a
    return-void
.end method


# virtual methods
.method public processOLEPicture(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iput-object p4, p0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 2
    .line 3
    if-eqz p5, :cond_1

    .line 4
    .line 5
    const-string v0, "oleObject"

    .line 6
    .line 7
    invoke-interface {p5, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object p5

    .line 11
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object p5

    .line 15
    :cond_0
    :goto_0
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 26
    .line 27
    const-string v1, "shapeId"

    .line 28
    .line 29
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/PictureReader;->instance()Lcom/intsig/office/fc/ppt/reader/PictureReader;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 40
    .line 41
    invoke-virtual {v1, p2, p3, v0, v2}, Lcom/intsig/office/fc/ppt/reader/PictureReader;->getOLEPart(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    if-eqz v1, :cond_0

    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/PictureReader;->instance()Lcom/intsig/office/fc/ppt/reader/PictureReader;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/ppt/reader/PictureReader;->getExcelShapeAnchor(Ljava/lang/String;)Lcom/intsig/office/ss/model/drawing/CellAnchor;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    .line 57
    new-instance v2, Lcom/intsig/office/common/shape/PictureShape;

    .line 58
    .line 59
    invoke-direct {v2}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 60
    .line 61
    .line 62
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    invoke-virtual {v3}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)I

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 75
    .line 76
    .line 77
    invoke-static {}, Lcom/intsig/office/ss/util/ModelUtil;->instance()Lcom/intsig/office/ss/util/ModelUtil;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-virtual {v1, p4, v0}, Lcom/intsig/office/ss/util/ModelUtil;->getCellAnchor(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/ss/model/drawing/CellAnchor;)Lcom/intsig/office/java/awt/Rectangle;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p4, v2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 89
    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_1
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method public read(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/ss/model/baseModel/Sheet;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v8, p2

    .line 4
    .line 5
    move-object/from16 v9, p3

    .line 6
    .line 7
    move-object/from16 v10, p4

    .line 8
    .line 9
    iput-object v10, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 10
    .line 11
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-static {v1}, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->getSchemeColor(Lcom/intsig/office/ss/model/baseModel/Workbook;)Ljava/util/Map;

    .line 16
    .line 17
    .line 18
    move-result-object v11

    .line 19
    const-string v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart"

    .line 20
    .line 21
    invoke-virtual {v9, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    new-instance v2, Ljava/util/HashMap;

    .line 26
    .line 27
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->chartList:Ljava/util/Map;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    .line 33
    .line 34
    .line 35
    move-result-object v7

    .line 36
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-eqz v1, :cond_0

    .line 41
    .line 42
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    check-cast v1, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 47
    .line 48
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-virtual {v8, v2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    iget-object v12, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->chartList:Ljava/util/Map;

    .line 57
    .line 58
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v13

    .line 62
    invoke-static {}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->instance()Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    const/4 v6, 0x1

    .line 67
    move-object/from16 v2, p1

    .line 68
    .line 69
    move-object/from16 v3, p2

    .line 70
    .line 71
    move-object v5, v11

    .line 72
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->read(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/util/Map;B)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    invoke-interface {v12, v13, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    .line 81
    .line 82
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 83
    .line 84
    .line 85
    iput-object v1, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->smartArtList:Ljava/util/Map;

    .line 86
    .line 87
    const-string v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/diagramData"

    .line 88
    .line 89
    invoke-virtual {v9, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 90
    .line 91
    .line 92
    move-result-object v12

    .line 93
    if-eqz v12, :cond_1

    .line 94
    .line 95
    invoke-virtual {v12}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->size()I

    .line 96
    .line 97
    .line 98
    move-result v1

    .line 99
    if-lez v1, :cond_1

    .line 100
    .line 101
    invoke-virtual {v12}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->size()I

    .line 102
    .line 103
    .line 104
    move-result v13

    .line 105
    const/4 v1, 0x0

    .line 106
    const/4 v14, 0x0

    .line 107
    :goto_1
    if-ge v14, v13, :cond_1

    .line 108
    .line 109
    invoke-virtual {v12, v14}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 110
    .line 111
    .line 112
    move-result-object v1

    .line 113
    iget-object v15, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->smartArtList:Ljava/util/Map;

    .line 114
    .line 115
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v7

    .line 119
    invoke-static {}, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->instance()Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    invoke-virtual {v8, v1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 128
    .line 129
    .line 130
    move-result-object v5

    .line 131
    move-object v1, v2

    .line 132
    move-object/from16 v2, p1

    .line 133
    .line 134
    move-object/from16 v3, p2

    .line 135
    .line 136
    move-object/from16 v4, p3

    .line 137
    .line 138
    move-object v6, v11

    .line 139
    move-object v10, v7

    .line 140
    move-object/from16 v7, p4

    .line 141
    .line 142
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->read(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/util/Map;Lcom/intsig/office/ss/model/baseModel/Sheet;)Lcom/intsig/office/common/shape/SmartArt;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-interface {v15, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    add-int/lit8 v14, v14, 0x1

    .line 150
    .line 151
    move-object/from16 v10, p4

    .line 152
    .line 153
    goto :goto_1

    .line 154
    :cond_1
    const-string v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"

    .line 155
    .line 156
    invoke-virtual {v9, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 157
    .line 158
    .line 159
    move-result-object v1

    .line 160
    new-instance v2, Ljava/util/HashMap;

    .line 161
    .line 162
    const/16 v3, 0xa

    .line 163
    .line 164
    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 165
    .line 166
    .line 167
    iput-object v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->drawingList:Ljava/util/Map;

    .line 168
    .line 169
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    .line 170
    .line 171
    .line 172
    move-result-object v1

    .line 173
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 174
    .line 175
    .line 176
    move-result v2

    .line 177
    if-eqz v2, :cond_2

    .line 178
    .line 179
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 180
    .line 181
    .line 182
    move-result-object v2

    .line 183
    check-cast v2, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 184
    .line 185
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 186
    .line 187
    .line 188
    move-result-object v3

    .line 189
    invoke-virtual {v8, v3}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    iget-object v4, v0, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->drawingList:Ljava/util/Map;

    .line 194
    .line 195
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getId()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v2

    .line 199
    invoke-interface/range {p1 .. p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 200
    .line 201
    .line 202
    move-result-object v5

    .line 203
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 204
    .line 205
    .line 206
    move-result-object v5

    .line 207
    invoke-virtual {v5, v3}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)I

    .line 208
    .line 209
    .line 210
    move-result v3

    .line 211
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 212
    .line 213
    .line 214
    move-result-object v3

    .line 215
    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    .line 217
    .line 218
    goto :goto_2

    .line 219
    :cond_2
    new-instance v1, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 220
    .line 221
    invoke-direct {v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 222
    .line 223
    .line 224
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 225
    .line 226
    .line 227
    move-result-object v2

    .line 228
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 229
    .line 230
    .line 231
    move-result-object v1

    .line 232
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 233
    .line 234
    .line 235
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 236
    .line 237
    .line 238
    move-result-object v1

    .line 239
    move-object/from16 v2, p1

    .line 240
    .line 241
    invoke-direct {v0, v2, v8, v9, v1}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->getCellAnchors(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 242
    .line 243
    .line 244
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/fc/xls/Reader/drawing/DrawingReader;->dispose()V

    .line 245
    .line 246
    .line 247
    return-void
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

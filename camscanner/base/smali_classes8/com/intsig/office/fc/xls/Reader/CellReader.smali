.class public Lcom/intsig/office/fc/xls/Reader/CellReader;
.super Ljava/lang/Object;
.source "CellReader.java"


# static fields
.field private static final CELLTYPE_BOOLEAN:S = 0x0s

.field private static final CELLTYPE_ERROR:S = 0x2s

.field private static final CELLTYPE_INLINESTRING:S = 0x5s

.field private static final CELLTYPE_NUMBER:S = 0x1s

.field private static final CELLTYPE_SHAREDSTRING:S = 0x3s

.field private static final CELLTYPE_STRING:S = 0x4s

.field private static reader:Lcom/intsig/office/fc/xls/Reader/CellReader;


# instance fields
.field private attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

.field private leaf:Lcom/intsig/office/simpletext/model/LeafElement;

.field private offset:I

.field private paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/xls/Reader/CellReader;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/xls/Reader/CellReader;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/CellReader;->reader:Lcom/intsig/office/fc/xls/Reader/CellReader;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getCellType(Ljava/lang/String;)S
    .locals 1

    .line 1
    if-eqz p1, :cond_5

    .line 2
    .line 3
    const-string v0, "n"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "b"

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    return p1

    .line 22
    :cond_1
    const-string v0, "s"

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_2

    .line 29
    .line 30
    const/4 p1, 0x3

    .line 31
    return p1

    .line 32
    :cond_2
    const-string v0, "str"

    .line 33
    .line 34
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_3

    .line 39
    .line 40
    const/4 p1, 0x4

    .line 41
    return p1

    .line 42
    :cond_3
    const-string v0, "inlineStr"

    .line 43
    .line 44
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_4

    .line 49
    .line 50
    const/4 p1, 0x5

    .line 51
    return p1

    .line 52
    :cond_4
    const/4 p1, 0x2

    .line 53
    return p1

    .line 54
    :cond_5
    :goto_0
    const/4 p1, 0x1

    .line 55
    return p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static instance()Lcom/intsig/office/fc/xls/Reader/CellReader;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/CellReader;->reader:Lcom/intsig/office/fc/xls/Reader/CellReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private isValidateCell(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/dom4j/Element;)Z
    .locals 4

    .line 1
    const-string v0, "t"

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    const-string v0, "v"

    .line 12
    .line 13
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    return v1

    .line 20
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v2, "s"

    .line 25
    .line 26
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    if-eqz v3, :cond_2

    .line 31
    .line 32
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-static {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isValidateStyle(Lcom/intsig/office/ss/model/style/CellStyle;)Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_5

    .line 49
    .line 50
    return v1

    .line 51
    :cond_2
    const-string v2, "r"

    .line 52
    .line 53
    invoke-interface {p2, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p2

    .line 57
    invoke-static {}, Lcom/intsig/office/ss/util/ReferenceUtil;->instance()Lcom/intsig/office/ss/util/ReferenceUtil;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    invoke-virtual {v2, p2}, Lcom/intsig/office/ss/util/ReferenceUtil;->getColumnIndex(Ljava/lang/String;)I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    invoke-static {}, Lcom/intsig/office/ss/util/ReferenceUtil;->instance()Lcom/intsig/office/ss/util/ReferenceUtil;

    .line 66
    .line 67
    .line 68
    move-result-object v3

    .line 69
    invoke-virtual {v3, p2}, Lcom/intsig/office/ss/util/ReferenceUtil;->getRowIndex(Ljava/lang/String;)I

    .line 70
    .line 71
    .line 72
    move-result p2

    .line 73
    invoke-virtual {p1, p2}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getRow(I)Lcom/intsig/office/ss/model/baseModel/Row;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    if-eqz p1, :cond_3

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Row;->getRowStyle()I

    .line 80
    .line 81
    .line 82
    move-result p1

    .line 83
    invoke-virtual {v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    invoke-static {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isValidateStyle(Lcom/intsig/office/ss/model/style/CellStyle;)Z

    .line 88
    .line 89
    .line 90
    move-result p1

    .line 91
    if-nez p1, :cond_4

    .line 92
    .line 93
    :cond_3
    invoke-virtual {v0, v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getCellStyle(I)Lcom/intsig/office/ss/model/style/CellStyle;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    invoke-static {p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->isValidateStyle(Lcom/intsig/office/ss/model/style/CellStyle;)Z

    .line 98
    .line 99
    .line 100
    move-result p1

    .line 101
    if-eqz p1, :cond_5

    .line 102
    .line 103
    :cond_4
    return v1

    .line 104
    :cond_5
    const/4 p1, 0x0

    .line 105
    return p1
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processBreakLine(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/simpletext/model/SectionElement;ILcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;)V
    .locals 20

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move-object/from16 v2, p2

    .line 4
    .line 5
    move-object/from16 v4, p4

    .line 6
    .line 7
    move-object/from16 v0, p5

    .line 8
    .line 9
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    if-eqz v0, :cond_5

    .line 18
    .line 19
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    if-nez v3, :cond_0

    .line 24
    .line 25
    goto/16 :goto_2

    .line 26
    .line 27
    :cond_0
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    const/4 v5, 0x0

    .line 32
    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    .line 33
    .line 34
    .line 35
    move-result v7

    .line 36
    const/16 v8, 0xa

    .line 37
    .line 38
    const/4 v13, 0x0

    .line 39
    const-wide/16 v14, 0x0

    .line 40
    .line 41
    const-string v12, "rPr"

    .line 42
    .line 43
    const-string v11, "\n"

    .line 44
    .line 45
    const/4 v10, 0x1

    .line 46
    if-ne v7, v8, :cond_2

    .line 47
    .line 48
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 49
    .line 50
    if-eqz v3, :cond_1

    .line 51
    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    .line 53
    .line 54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    .line 56
    .line 57
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 58
    .line 59
    invoke-virtual {v5, v13}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v5

    .line 63
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-virtual {v3, v1}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    iget v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 77
    .line 78
    add-int/2addr v1, v10

    .line 79
    iput v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 80
    .line 81
    const/4 v1, 0x1

    .line 82
    goto :goto_0

    .line 83
    :cond_1
    new-instance v3, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 84
    .line 85
    invoke-direct {v3, v11}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    iput-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 89
    .line 90
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 91
    .line 92
    .line 93
    move-result-object v7

    .line 94
    invoke-interface {v4, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 95
    .line 96
    .line 97
    move-result-object v3

    .line 98
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 99
    .line 100
    invoke-virtual {v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 101
    .line 102
    .line 103
    move-result-object v11

    .line 104
    iget-object v12, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 105
    .line 106
    move-object v8, v1

    .line 107
    move/from16 v9, p3

    .line 108
    .line 109
    const/4 v1, 0x1

    .line 110
    move-object v10, v3

    .line 111
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 112
    .line 113
    .line 114
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 115
    .line 116
    iget v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 117
    .line 118
    int-to-long v7, v5

    .line 119
    invoke-virtual {v3, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 120
    .line 121
    .line 122
    iget v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 123
    .line 124
    add-int/2addr v3, v1

    .line 125
    iput v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 126
    .line 127
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 128
    .line 129
    int-to-long v7, v3

    .line 130
    invoke-virtual {v5, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 131
    .line 132
    .line 133
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 134
    .line 135
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 136
    .line 137
    invoke-virtual {v3, v5}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 138
    .line 139
    .line 140
    :goto_0
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 141
    .line 142
    iget v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 143
    .line 144
    int-to-long v7, v5

    .line 145
    invoke-virtual {v3, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 146
    .line 147
    .line 148
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 149
    .line 150
    invoke-virtual {v2, v3, v14, v15}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 151
    .line 152
    .line 153
    iput-object v13, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 154
    .line 155
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v5

    .line 159
    new-instance v0, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 160
    .line 161
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 162
    .line 163
    .line 164
    iput-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 165
    .line 166
    iget v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 167
    .line 168
    int-to-long v7, v1

    .line 169
    invoke-virtual {v0, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 170
    .line 171
    .line 172
    new-instance v0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 173
    .line 174
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 175
    .line 176
    .line 177
    iput-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 178
    .line 179
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 180
    .line 181
    .line 182
    move-result-object v0

    .line 183
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 184
    .line 185
    .line 186
    move-result-object v1

    .line 187
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 188
    .line 189
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    iget-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 194
    .line 195
    invoke-virtual {v0, v1, v3, v7}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 196
    .line 197
    .line 198
    move-object/from16 v0, p0

    .line 199
    .line 200
    move-object/from16 v1, p1

    .line 201
    .line 202
    move-object/from16 v2, p2

    .line 203
    .line 204
    move/from16 v3, p3

    .line 205
    .line 206
    move-object/from16 v4, p4

    .line 207
    .line 208
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/xls/Reader/CellReader;->processBreakLine(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/simpletext/model/SectionElement;ILcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    goto/16 :goto_2

    .line 212
    .line 213
    :cond_2
    const/16 v16, 0x1

    .line 214
    .line 215
    add-int/lit8 v3, v3, -0x1

    .line 216
    .line 217
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    .line 218
    .line 219
    .line 220
    move-result v3

    .line 221
    if-ne v3, v8, :cond_3

    .line 222
    .line 223
    new-instance v3, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 224
    .line 225
    invoke-virtual {v0, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 226
    .line 227
    .line 228
    move-result v7

    .line 229
    add-int/lit8 v7, v7, 0x1

    .line 230
    .line 231
    invoke-virtual {v0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v5

    .line 235
    invoke-direct {v3, v5}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    iput-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 239
    .line 240
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 241
    .line 242
    .line 243
    move-result-object v7

    .line 244
    invoke-interface {v4, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 245
    .line 246
    .line 247
    move-result-object v10

    .line 248
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 249
    .line 250
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 251
    .line 252
    .line 253
    move-result-object v3

    .line 254
    iget-object v12, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 255
    .line 256
    move-object v8, v1

    .line 257
    move/from16 v9, p3

    .line 258
    .line 259
    move-object v1, v11

    .line 260
    move-object v11, v3

    .line 261
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 262
    .line 263
    .line 264
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 265
    .line 266
    iget v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 267
    .line 268
    int-to-long v7, v5

    .line 269
    invoke-virtual {v3, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 270
    .line 271
    .line 272
    iget v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 273
    .line 274
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 275
    .line 276
    invoke-virtual {v5, v13}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 277
    .line 278
    .line 279
    move-result-object v5

    .line 280
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 281
    .line 282
    .line 283
    move-result v5

    .line 284
    add-int/2addr v3, v5

    .line 285
    iput v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 286
    .line 287
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 288
    .line 289
    int-to-long v7, v3

    .line 290
    invoke-virtual {v5, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 291
    .line 292
    .line 293
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 294
    .line 295
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 296
    .line 297
    invoke-virtual {v3, v5}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 298
    .line 299
    .line 300
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 301
    .line 302
    iget v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 303
    .line 304
    int-to-long v7, v5

    .line 305
    invoke-virtual {v3, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 306
    .line 307
    .line 308
    iget-object v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 309
    .line 310
    invoke-virtual {v2, v3, v14, v15}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 311
    .line 312
    .line 313
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 314
    .line 315
    .line 316
    move-result v1

    .line 317
    add-int/lit8 v1, v1, 0x1

    .line 318
    .line 319
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 320
    .line 321
    .line 322
    move-result-object v5

    .line 323
    move-object/from16 v0, p0

    .line 324
    .line 325
    move-object/from16 v1, p1

    .line 326
    .line 327
    move-object/from16 v2, p2

    .line 328
    .line 329
    move/from16 v3, p3

    .line 330
    .line 331
    move-object/from16 v4, p4

    .line 332
    .line 333
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/xls/Reader/CellReader;->processBreakLine(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/simpletext/model/SectionElement;ILcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;)V

    .line 334
    .line 335
    .line 336
    goto/16 :goto_2

    .line 337
    .line 338
    :cond_3
    move-object v3, v11

    .line 339
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 340
    .line 341
    .line 342
    move-result-object v0

    .line 343
    array-length v13, v0

    .line 344
    new-instance v7, Ljava/lang/StringBuilder;

    .line 345
    .line 346
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 347
    .line 348
    .line 349
    aget-object v5, v0, v5

    .line 350
    .line 351
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    .line 353
    .line 354
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    .line 356
    .line 357
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 358
    .line 359
    .line 360
    move-result-object v5

    .line 361
    new-instance v7, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 362
    .line 363
    invoke-direct {v7, v5}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 364
    .line 365
    .line 366
    iput-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 367
    .line 368
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 369
    .line 370
    .line 371
    move-result-object v7

    .line 372
    invoke-interface {v4, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 373
    .line 374
    .line 375
    move-result-object v10

    .line 376
    iget-object v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 377
    .line 378
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 379
    .line 380
    .line 381
    move-result-object v11

    .line 382
    iget-object v9, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 383
    .line 384
    move-object v8, v1

    .line 385
    move-object/from16 v17, v9

    .line 386
    .line 387
    move/from16 v9, p3

    .line 388
    .line 389
    move-object/from16 v18, v12

    .line 390
    .line 391
    move-object/from16 v12, v17

    .line 392
    .line 393
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 394
    .line 395
    .line 396
    iget-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 397
    .line 398
    iget v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 399
    .line 400
    int-to-long v8, v8

    .line 401
    invoke-virtual {v7, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 402
    .line 403
    .line 404
    iget v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 405
    .line 406
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 407
    .line 408
    .line 409
    move-result v5

    .line 410
    add-int/2addr v7, v5

    .line 411
    iput v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 412
    .line 413
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 414
    .line 415
    int-to-long v7, v7

    .line 416
    invoke-virtual {v5, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 417
    .line 418
    .line 419
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 420
    .line 421
    iget-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 422
    .line 423
    invoke-virtual {v5, v7}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 424
    .line 425
    .line 426
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 427
    .line 428
    iget v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 429
    .line 430
    int-to-long v7, v7

    .line 431
    invoke-virtual {v5, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 432
    .line 433
    .line 434
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 435
    .line 436
    invoke-virtual {v2, v5, v14, v15}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 437
    .line 438
    .line 439
    const/4 v5, 0x1

    .line 440
    :goto_1
    add-int/lit8 v7, v13, -0x1

    .line 441
    .line 442
    if-ge v5, v7, :cond_4

    .line 443
    .line 444
    new-instance v7, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 445
    .line 446
    invoke-direct {v7}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 447
    .line 448
    .line 449
    iput-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 450
    .line 451
    iget v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 452
    .line 453
    int-to-long v8, v8

    .line 454
    invoke-virtual {v7, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 455
    .line 456
    .line 457
    new-instance v7, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 458
    .line 459
    invoke-direct {v7}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 460
    .line 461
    .line 462
    iput-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 463
    .line 464
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 465
    .line 466
    .line 467
    move-result-object v7

    .line 468
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 469
    .line 470
    .line 471
    move-result-object v8

    .line 472
    iget-object v9, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 473
    .line 474
    invoke-virtual {v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 475
    .line 476
    .line 477
    move-result-object v9

    .line 478
    iget-object v10, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 479
    .line 480
    invoke-virtual {v7, v8, v9, v10}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 481
    .line 482
    .line 483
    new-instance v7, Ljava/lang/StringBuilder;

    .line 484
    .line 485
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 486
    .line 487
    .line 488
    aget-object v8, v0, v5

    .line 489
    .line 490
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491
    .line 492
    .line 493
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    .line 495
    .line 496
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 497
    .line 498
    .line 499
    move-result-object v12

    .line 500
    new-instance v7, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 501
    .line 502
    invoke-direct {v7, v12}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 503
    .line 504
    .line 505
    iput-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 506
    .line 507
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 508
    .line 509
    .line 510
    move-result-object v7

    .line 511
    move-object/from16 v11, v18

    .line 512
    .line 513
    invoke-interface {v4, v11}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 514
    .line 515
    .line 516
    move-result-object v10

    .line 517
    iget-object v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 518
    .line 519
    invoke-virtual {v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 520
    .line 521
    .line 522
    move-result-object v17

    .line 523
    iget-object v9, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 524
    .line 525
    move-object v8, v1

    .line 526
    move-object/from16 v18, v9

    .line 527
    .line 528
    move/from16 v9, p3

    .line 529
    .line 530
    move-object/from16 v19, v11

    .line 531
    .line 532
    move-object/from16 v11, v17

    .line 533
    .line 534
    move-object/from16 v17, v12

    .line 535
    .line 536
    move-object/from16 v12, v18

    .line 537
    .line 538
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 539
    .line 540
    .line 541
    iget-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 542
    .line 543
    iget v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 544
    .line 545
    int-to-long v8, v8

    .line 546
    invoke-virtual {v7, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 547
    .line 548
    .line 549
    iget v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 550
    .line 551
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    .line 552
    .line 553
    .line 554
    move-result v8

    .line 555
    add-int/2addr v7, v8

    .line 556
    iput v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 557
    .line 558
    iget-object v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 559
    .line 560
    int-to-long v9, v7

    .line 561
    invoke-virtual {v8, v9, v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 562
    .line 563
    .line 564
    iget-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 565
    .line 566
    iget-object v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 567
    .line 568
    invoke-virtual {v7, v8}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 569
    .line 570
    .line 571
    iget-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 572
    .line 573
    iget v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 574
    .line 575
    int-to-long v8, v8

    .line 576
    invoke-virtual {v7, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 577
    .line 578
    .line 579
    iget-object v7, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 580
    .line 581
    invoke-virtual {v2, v7, v14, v15}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 582
    .line 583
    .line 584
    add-int/lit8 v5, v5, 0x1

    .line 585
    .line 586
    move-object/from16 v18, v19

    .line 587
    .line 588
    goto/16 :goto_1

    .line 589
    .line 590
    :cond_4
    move-object/from16 v19, v18

    .line 591
    .line 592
    new-instance v2, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 593
    .line 594
    invoke-direct {v2}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 595
    .line 596
    .line 597
    iput-object v2, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 598
    .line 599
    iget v3, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 600
    .line 601
    int-to-long v8, v3

    .line 602
    invoke-virtual {v2, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 603
    .line 604
    .line 605
    new-instance v2, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 606
    .line 607
    invoke-direct {v2}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 608
    .line 609
    .line 610
    iput-object v2, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 611
    .line 612
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 613
    .line 614
    .line 615
    move-result-object v2

    .line 616
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 617
    .line 618
    .line 619
    move-result-object v3

    .line 620
    iget-object v5, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 621
    .line 622
    invoke-virtual {v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 623
    .line 624
    .line 625
    move-result-object v5

    .line 626
    iget-object v8, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 627
    .line 628
    invoke-virtual {v2, v3, v5, v8}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 629
    .line 630
    .line 631
    aget-object v0, v0, v7

    .line 632
    .line 633
    new-instance v2, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 634
    .line 635
    invoke-direct {v2, v0}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 636
    .line 637
    .line 638
    iput-object v2, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 639
    .line 640
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 641
    .line 642
    .line 643
    move-result-object v7

    .line 644
    move-object/from16 v2, v19

    .line 645
    .line 646
    invoke-interface {v4, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 647
    .line 648
    .line 649
    move-result-object v10

    .line 650
    iget-object v2, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 651
    .line 652
    invoke-virtual {v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 653
    .line 654
    .line 655
    move-result-object v11

    .line 656
    iget-object v12, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 657
    .line 658
    move-object v8, v1

    .line 659
    move/from16 v9, p3

    .line 660
    .line 661
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 662
    .line 663
    .line 664
    iget-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 665
    .line 666
    iget v2, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 667
    .line 668
    int-to-long v2, v2

    .line 669
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 670
    .line 671
    .line 672
    iget v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 673
    .line 674
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 675
    .line 676
    .line 677
    move-result v0

    .line 678
    add-int/2addr v1, v0

    .line 679
    iput v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 680
    .line 681
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 682
    .line 683
    int-to-long v1, v1

    .line 684
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 685
    .line 686
    .line 687
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 688
    .line 689
    iget-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 690
    .line 691
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 692
    .line 693
    .line 694
    :cond_5
    :goto_2
    return-void
.end method

.method private processComplexSST(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/simpletext/model/SectionElement;
    .locals 9

    .line 1
    new-instance v0, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 4
    .line 5
    .line 6
    const-wide/16 v1, 0x0

    .line 7
    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 12
    .line 13
    .line 14
    move-result-object v3

    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v4

    .line 19
    const/high16 v5, 0x41f00000    # 30.0f

    .line 20
    .line 21
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 22
    .line 23
    .line 24
    move-result v6

    .line 25
    invoke-virtual {v4, v3, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 26
    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 29
    .line 30
    .line 31
    move-result-object v4

    .line 32
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    invoke-virtual {v4, v3, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 37
    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    const/4 v5, 0x0

    .line 44
    invoke-virtual {v4, v3, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 45
    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    invoke-virtual {v4, v3, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    invoke-virtual {v4}, Lcom/intsig/office/ss/model/style/CellStyle;->getVerticalAlign()S

    .line 59
    .line 60
    .line 61
    move-result v4

    .line 62
    if-eqz v4, :cond_0

    .line 63
    .line 64
    const/4 v6, 0x1

    .line 65
    if-eq v4, v6, :cond_1

    .line 66
    .line 67
    const/4 v6, 0x2

    .line 68
    if-eq v4, v6, :cond_1

    .line 69
    .line 70
    const/4 v6, 0x3

    .line 71
    if-eq v4, v6, :cond_1

    .line 72
    .line 73
    :cond_0
    const/4 v6, 0x0

    .line 74
    :cond_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 75
    .line 76
    .line 77
    move-result-object v4

    .line 78
    invoke-virtual {v4, v3, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    invoke-virtual {v3}, Lcom/intsig/office/ss/model/style/CellStyle;->getFontIndex()S

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    iput v5, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 90
    .line 91
    new-instance v4, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 92
    .line 93
    invoke-direct {v4}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 94
    .line 95
    .line 96
    iput-object v4, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 97
    .line 98
    iget v6, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 99
    .line 100
    int-to-long v6, v6

    .line 101
    invoke-virtual {v4, v6, v7}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 102
    .line 103
    .line 104
    new-instance v4, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 105
    .line 106
    invoke-direct {v4}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 107
    .line 108
    .line 109
    iput-object v4, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 110
    .line 111
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 112
    .line 113
    .line 114
    move-result-object v4

    .line 115
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 116
    .line 117
    .line 118
    move-result-object v6

    .line 119
    iget-object v7, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 120
    .line 121
    invoke-virtual {v7}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 122
    .line 123
    .line 124
    move-result-object v7

    .line 125
    iget-object v8, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 126
    .line 127
    invoke-virtual {v4, v6, v7, v8}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 128
    .line 129
    .line 130
    invoke-direct {p0, p1, v0, p2, v3}, Lcom/intsig/office/fc/xls/Reader/CellReader;->processRun(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;I)Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 131
    .line 132
    .line 133
    move-result-object p1

    .line 134
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 135
    .line 136
    iget p2, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 137
    .line 138
    int-to-long v3, p2

    .line 139
    invoke-virtual {p1, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 140
    .line 141
    .line 142
    iget-object p1, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 143
    .line 144
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 145
    .line 146
    .line 147
    iget p1, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 148
    .line 149
    int-to-long p1, p1

    .line 150
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 151
    .line 152
    .line 153
    iput v5, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 154
    .line 155
    const/4 p1, 0x0

    .line 156
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 157
    .line 158
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 159
    .line 160
    iput-object p1, p0, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 161
    .line 162
    return-object v0
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processRun(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;I)Lcom/intsig/office/simpletext/model/ParagraphElement;
    .locals 17

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getSheet()Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 8
    .line 9
    .line 10
    move-result-object v13

    .line 11
    invoke-interface/range {p3 .. p3}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->getCellStyle()Lcom/intsig/office/ss/model/style/CellStyle;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/ss/model/style/CellStyle;->isWrapText()Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    xor-int/lit8 v14, v1, 0x1

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    const-string v15, "\n"

    .line 30
    .line 31
    if-nez v1, :cond_0

    .line 32
    .line 33
    new-instance v0, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 34
    .line 35
    invoke-direct {v0, v15}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iput-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 39
    .line 40
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 41
    .line 42
    .line 43
    move-result-object v7

    .line 44
    const/4 v10, 0x0

    .line 45
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 48
    .line 49
    .line 50
    move-result-object v11

    .line 51
    iget-object v12, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 52
    .line 53
    move-object v8, v13

    .line 54
    move/from16 v9, p4

    .line 55
    .line 56
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 60
    .line 61
    iget v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 62
    .line 63
    int-to-long v1, v1

    .line 64
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 65
    .line 66
    .line 67
    iget v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 68
    .line 69
    add-int/lit8 v0, v0, 0x1

    .line 70
    .line 71
    iput v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 72
    .line 73
    iget-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 74
    .line 75
    int-to-long v2, v0

    .line 76
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 77
    .line 78
    .line 79
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 80
    .line 81
    iget-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 82
    .line 83
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 84
    .line 85
    .line 86
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 87
    .line 88
    return-object v0

    .line 89
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 90
    .line 91
    .line 92
    move-result-object v16

    .line 93
    :cond_1
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-eqz v0, :cond_4

    .line 98
    .line 99
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    move-object v4, v0

    .line 104
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 105
    .line 106
    invoke-interface {v4}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    const-string v1, "r"

    .line 111
    .line 112
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    if-eqz v0, :cond_1

    .line 117
    .line 118
    const-string v0, "t"

    .line 119
    .line 120
    invoke-interface {v4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    if-eqz v0, :cond_1

    .line 125
    .line 126
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v5

    .line 130
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 131
    .line 132
    .line 133
    move-result v0

    .line 134
    if-lez v0, :cond_1

    .line 135
    .line 136
    const-string v0, "rPr"

    .line 137
    .line 138
    if-eqz v14, :cond_2

    .line 139
    .line 140
    const-string v1, ""

    .line 141
    .line 142
    invoke-virtual {v5, v15, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    new-instance v2, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 147
    .line 148
    invoke-direct {v2, v1}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    iput-object v2, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 152
    .line 153
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 154
    .line 155
    .line 156
    move-result-object v7

    .line 157
    invoke-interface {v4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 158
    .line 159
    .line 160
    move-result-object v10

    .line 161
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 162
    .line 163
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 164
    .line 165
    .line 166
    move-result-object v11

    .line 167
    iget-object v12, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 168
    .line 169
    move-object v8, v13

    .line 170
    move/from16 v9, p4

    .line 171
    .line 172
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 173
    .line 174
    .line 175
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 176
    .line 177
    iget v2, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 178
    .line 179
    int-to-long v2, v2

    .line 180
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 181
    .line 182
    .line 183
    iget v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 184
    .line 185
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 186
    .line 187
    .line 188
    move-result v1

    .line 189
    add-int/2addr v0, v1

    .line 190
    iput v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 191
    .line 192
    iget-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 193
    .line 194
    int-to-long v2, v0

    .line 195
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 196
    .line 197
    .line 198
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 199
    .line 200
    iget-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 201
    .line 202
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 203
    .line 204
    .line 205
    goto :goto_0

    .line 206
    :cond_2
    invoke-virtual {v5, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 207
    .line 208
    .line 209
    move-result v1

    .line 210
    if-nez v1, :cond_3

    .line 211
    .line 212
    new-instance v1, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 213
    .line 214
    invoke-direct {v1, v5}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 215
    .line 216
    .line 217
    iput-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 218
    .line 219
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 220
    .line 221
    .line 222
    move-result-object v7

    .line 223
    invoke-interface {v4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 224
    .line 225
    .line 226
    move-result-object v10

    .line 227
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 228
    .line 229
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 230
    .line 231
    .line 232
    move-result-object v11

    .line 233
    iget-object v12, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->attrLayout:Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 234
    .line 235
    move-object v8, v13

    .line 236
    move/from16 v9, p4

    .line 237
    .line 238
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Workbook;ILcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 239
    .line 240
    .line 241
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 242
    .line 243
    iget v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 244
    .line 245
    int-to-long v1, v1

    .line 246
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 247
    .line 248
    .line 249
    iget v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 250
    .line 251
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 252
    .line 253
    .line 254
    move-result v1

    .line 255
    add-int/2addr v0, v1

    .line 256
    iput v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 257
    .line 258
    iget-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 259
    .line 260
    int-to-long v2, v0

    .line 261
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 262
    .line 263
    .line 264
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 265
    .line 266
    iget-object v1, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 267
    .line 268
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 269
    .line 270
    .line 271
    goto/16 :goto_0

    .line 272
    .line 273
    :cond_3
    move-object/from16 v0, p0

    .line 274
    .line 275
    move-object/from16 v1, p1

    .line 276
    .line 277
    move-object/from16 v2, p2

    .line 278
    .line 279
    move/from16 v3, p4

    .line 280
    .line 281
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/xls/Reader/CellReader;->processBreakLine(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/simpletext/model/SectionElement;ILcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;)V

    .line 282
    .line 283
    .line 284
    goto/16 :goto_0

    .line 285
    .line 286
    :cond_4
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 287
    .line 288
    if-eqz v0, :cond_5

    .line 289
    .line 290
    new-instance v1, Ljava/lang/StringBuilder;

    .line 291
    .line 292
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 293
    .line 294
    .line 295
    iget-object v2, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->leaf:Lcom/intsig/office/simpletext/model/LeafElement;

    .line 296
    .line 297
    const/4 v3, 0x0

    .line 298
    invoke-virtual {v2, v3}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 299
    .line 300
    .line 301
    move-result-object v2

    .line 302
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    .line 304
    .line 305
    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    .line 307
    .line 308
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 309
    .line 310
    .line 311
    move-result-object v1

    .line 312
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 313
    .line 314
    .line 315
    iget v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 316
    .line 317
    add-int/lit8 v0, v0, 0x1

    .line 318
    .line 319
    iput v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->offset:I

    .line 320
    .line 321
    :cond_5
    iget-object v0, v6, Lcom/intsig/office/fc/xls/Reader/CellReader;->paraElem:Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 322
    .line 323
    return-object v0
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method


# virtual methods
.method public getCell(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/ss/model/baseModel/Cell;
    .locals 11

    .line 1
    const-string v0, "s"

    .line 2
    .line 3
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/Reader/CellReader;->isValidateCell(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/dom4j/Element;)Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_0

    .line 9
    .line 10
    return-object v2

    .line 11
    :cond_0
    :try_start_0
    const-string v1, "t"

    .line 12
    .line 13
    invoke-interface {p2, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/xls/Reader/CellReader;->getCellType(Ljava/lang/String;)S

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    const/4 v3, 0x0

    .line 22
    const/4 v4, 0x2

    .line 23
    const/4 v5, 0x4

    .line 24
    const/4 v6, 0x3

    .line 25
    const/4 v7, 0x1

    .line 26
    if-eqz v1, :cond_3

    .line 27
    .line 28
    if-eq v1, v7, :cond_2

    .line 29
    .line 30
    if-eq v1, v4, :cond_1

    .line 31
    .line 32
    if-eq v1, v6, :cond_1

    .line 33
    .line 34
    if-eq v1, v5, :cond_1

    .line 35
    .line 36
    const/4 v8, 0x5

    .line 37
    if-eq v1, v8, :cond_1

    .line 38
    .line 39
    new-instance v8, Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 40
    .line 41
    invoke-direct {v8, v6}, Lcom/intsig/office/ss/model/baseModel/Cell;-><init>(S)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    new-instance v8, Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 46
    .line 47
    invoke-direct {v8, v7}, Lcom/intsig/office/ss/model/baseModel/Cell;-><init>(S)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_2
    new-instance v8, Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 52
    .line 53
    invoke-direct {v8, v3}, Lcom/intsig/office/ss/model/baseModel/Cell;-><init>(S)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_3
    new-instance v8, Lcom/intsig/office/ss/model/baseModel/Cell;

    .line 58
    .line 59
    invoke-direct {v8, v5}, Lcom/intsig/office/ss/model/baseModel/Cell;-><init>(S)V

    .line 60
    .line 61
    .line 62
    :goto_0
    const-string v9, "r"

    .line 63
    .line 64
    invoke-interface {p2, v9}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v9

    .line 68
    invoke-static {}, Lcom/intsig/office/ss/util/ReferenceUtil;->instance()Lcom/intsig/office/ss/util/ReferenceUtil;

    .line 69
    .line 70
    .line 71
    move-result-object v10

    .line 72
    invoke-virtual {v10, v9}, Lcom/intsig/office/ss/util/ReferenceUtil;->getColumnIndex(Ljava/lang/String;)I

    .line 73
    .line 74
    .line 75
    move-result v10

    .line 76
    invoke-virtual {v8, v10}, Lcom/intsig/office/ss/model/baseModel/Cell;->setColNumber(I)V

    .line 77
    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/office/ss/util/ReferenceUtil;->instance()Lcom/intsig/office/ss/util/ReferenceUtil;

    .line 80
    .line 81
    .line 82
    move-result-object v10

    .line 83
    invoke-virtual {v10, v9}, Lcom/intsig/office/ss/util/ReferenceUtil;->getRowIndex(Ljava/lang/String;)I

    .line 84
    .line 85
    .line 86
    move-result v9

    .line 87
    invoke-virtual {v8, v9}, Lcom/intsig/office/ss/model/baseModel/Cell;->setRowNumber(I)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getWorkbook()Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 91
    .line 92
    .line 93
    move-result-object v9

    .line 94
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v10

    .line 98
    if-eqz v10, :cond_4

    .line 99
    .line 100
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    goto :goto_1

    .line 109
    :cond_4
    invoke-virtual {v8}, Lcom/intsig/office/ss/model/baseModel/Cell;->getColNumber()I

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    invoke-virtual {p1, v0}, Lcom/intsig/office/ss/model/baseModel/Sheet;->getColumnStyle(I)I

    .line 114
    .line 115
    .line 116
    move-result v0

    .line 117
    :goto_1
    invoke-virtual {v8, v0}, Lcom/intsig/office/ss/model/baseModel/Cell;->setCellStyle(I)V

    .line 118
    .line 119
    .line 120
    const-string v0, "v"

    .line 121
    .line 122
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 123
    .line 124
    .line 125
    move-result-object p2

    .line 126
    if-eqz p2, :cond_c

    .line 127
    .line 128
    invoke-interface {p2}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object p2

    .line 132
    if-ne v1, v6, :cond_6

    .line 133
    .line 134
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 135
    .line 136
    .line 137
    move-result p2

    .line 138
    invoke-virtual {v9, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSharedItem(I)Ljava/lang/Object;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    instance-of v1, v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 143
    .line 144
    if-eqz v1, :cond_5

    .line 145
    .line 146
    invoke-virtual {v8, p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->setSheet(Lcom/intsig/office/ss/model/baseModel/Sheet;)V

    .line 147
    .line 148
    .line 149
    check-cast v0, Lcom/intsig/office/fc/dom4j/Element;

    .line 150
    .line 151
    invoke-direct {p0, v8, v0}, Lcom/intsig/office/fc/xls/Reader/CellReader;->processComplexSST(Lcom/intsig/office/ss/model/baseModel/Cell;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/simpletext/model/SectionElement;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    invoke-virtual {v9, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSharedString(Ljava/lang/Object;)I

    .line 156
    .line 157
    .line 158
    move-result p2

    .line 159
    :cond_5
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 160
    .line 161
    .line 162
    move-result-object p1

    .line 163
    invoke-virtual {v8, p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->setCellValue(Ljava/lang/Object;)V

    .line 164
    .line 165
    .line 166
    goto :goto_3

    .line 167
    :cond_6
    if-eq v1, v5, :cond_b

    .line 168
    .line 169
    if-ne v1, v4, :cond_7

    .line 170
    .line 171
    goto :goto_2

    .line 172
    :cond_7
    if-ne v1, v7, :cond_8

    .line 173
    .line 174
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 175
    .line 176
    .line 177
    move-result-wide p1

    .line 178
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 179
    .line 180
    .line 181
    move-result-object p1

    .line 182
    invoke-virtual {v8, p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->setCellValue(Ljava/lang/Object;)V

    .line 183
    .line 184
    .line 185
    goto :goto_3

    .line 186
    :cond_8
    if-nez v1, :cond_a

    .line 187
    .line 188
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 189
    .line 190
    .line 191
    move-result p1

    .line 192
    if-eqz p1, :cond_9

    .line 193
    .line 194
    const/4 v3, 0x1

    .line 195
    :cond_9
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    invoke-virtual {v8, p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->setCellValue(Ljava/lang/Object;)V

    .line 200
    .line 201
    .line 202
    goto :goto_3

    .line 203
    :cond_a
    invoke-virtual {v8, p2}, Lcom/intsig/office/ss/model/baseModel/Cell;->setCellValue(Ljava/lang/Object;)V

    .line 204
    .line 205
    .line 206
    goto :goto_3

    .line 207
    :cond_b
    :goto_2
    invoke-virtual {v9, p2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSharedString(Ljava/lang/Object;)I

    .line 208
    .line 209
    .line 210
    move-result p1

    .line 211
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 212
    .line 213
    .line 214
    move-result-object p1

    .line 215
    invoke-virtual {v8, p1}, Lcom/intsig/office/ss/model/baseModel/Cell;->setCellValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    .line 217
    .line 218
    :cond_c
    :goto_3
    return-object v8

    .line 219
    :catch_0
    move-exception p1

    .line 220
    new-instance p2, Ljava/lang/StringBuilder;

    .line 221
    .line 222
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 223
    .line 224
    .line 225
    const-string v0, "getCell error: "

    .line 226
    .line 227
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    .line 229
    .line 230
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 231
    .line 232
    .line 233
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object p1

    .line 237
    const-string p2, "CellReader"

    .line 238
    .line 239
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    .line 241
    .line 242
    return-object v2
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public searchContent(Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;)Z
    .locals 2

    .line 1
    const-string v0, "v"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v1, "t"

    .line 10
    .line 11
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/xls/Reader/CellReader;->getCellType(Ljava/lang/String;)S

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    const/4 v1, 0x3

    .line 20
    if-eq p1, v1, :cond_0

    .line 21
    .line 22
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-eqz p1, :cond_0

    .line 35
    .line 36
    const/4 p1, 0x1

    .line 37
    return p1

    .line 38
    :cond_0
    const/4 p1, 0x0

    .line 39
    return p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.class public Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;
.super Ljava/lang/Object;
.source "ThemeColorReader.java"


# static fields
.field private static reader:Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->reader:Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I
    .locals 4

    .line 1
    const-string v0, "srgbClr"

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/16 v2, 0x10

    .line 8
    .line 9
    const/high16 v3, -0x1000000

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const-string v0, "val"

    .line 18
    .line 19
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-static {p1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const-string v0, "sysClr"

    .line 29
    .line 30
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    if-eqz v1, :cond_1

    .line 35
    .line 36
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    const-string v0, "lastClr"

    .line 41
    .line 42
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-static {p1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    const/high16 p1, -0x1000000

    .line 52
    .line 53
    :goto_0
    or-int/2addr p1, v3

    .line 54
    invoke-virtual {p2, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addColor(I)I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    return p1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static instance()Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->reader:Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getThemeColor(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/ss/model/baseModel/Workbook;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 15
    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const-string v0, "themeElements"

    .line 22
    .line 23
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    const-string v0, "clrScheme"

    .line 28
    .line 29
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    const-string v0, "lt1"

    .line 34
    .line 35
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 44
    .line 45
    .line 46
    const-string v0, "bg1"

    .line 47
    .line 48
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 49
    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 53
    .line 54
    .line 55
    const-string v0, "dk1"

    .line 56
    .line 57
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 66
    .line 67
    .line 68
    const-string v0, "tx1"

    .line 69
    .line 70
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 71
    .line 72
    .line 73
    const/4 v0, 0x1

    .line 74
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 75
    .line 76
    .line 77
    const-string v0, "lt2"

    .line 78
    .line 79
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 88
    .line 89
    .line 90
    const-string v0, "bg2"

    .line 91
    .line 92
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 93
    .line 94
    .line 95
    const/4 v0, 0x2

    .line 96
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 97
    .line 98
    .line 99
    const-string v0, "dk2"

    .line 100
    .line 101
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 110
    .line 111
    .line 112
    const-string v0, "tx2"

    .line 113
    .line 114
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 115
    .line 116
    .line 117
    const/4 v0, 0x3

    .line 118
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 119
    .line 120
    .line 121
    const-string v0, "accent1"

    .line 122
    .line 123
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 128
    .line 129
    .line 130
    move-result v1

    .line 131
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 132
    .line 133
    .line 134
    const/4 v0, 0x4

    .line 135
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 136
    .line 137
    .line 138
    const-string v0, "accent2"

    .line 139
    .line 140
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 145
    .line 146
    .line 147
    move-result v1

    .line 148
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 149
    .line 150
    .line 151
    const/4 v0, 0x5

    .line 152
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 153
    .line 154
    .line 155
    const-string v0, "accent3"

    .line 156
    .line 157
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 158
    .line 159
    .line 160
    move-result-object v1

    .line 161
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 162
    .line 163
    .line 164
    move-result v1

    .line 165
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 166
    .line 167
    .line 168
    const/4 v0, 0x6

    .line 169
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 170
    .line 171
    .line 172
    const-string v0, "accent4"

    .line 173
    .line 174
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 175
    .line 176
    .line 177
    move-result-object v1

    .line 178
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 179
    .line 180
    .line 181
    move-result v1

    .line 182
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 183
    .line 184
    .line 185
    const/4 v0, 0x7

    .line 186
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 187
    .line 188
    .line 189
    const-string v0, "accent5"

    .line 190
    .line 191
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 196
    .line 197
    .line 198
    move-result v1

    .line 199
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 200
    .line 201
    .line 202
    const/16 v0, 0x8

    .line 203
    .line 204
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 205
    .line 206
    .line 207
    const-string v0, "accent6"

    .line 208
    .line 209
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 210
    .line 211
    .line 212
    move-result-object v1

    .line 213
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 214
    .line 215
    .line 216
    move-result v1

    .line 217
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 218
    .line 219
    .line 220
    const/16 v0, 0x9

    .line 221
    .line 222
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 223
    .line 224
    .line 225
    const-string v0, "hlink"

    .line 226
    .line 227
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 228
    .line 229
    .line 230
    move-result-object v1

    .line 231
    invoke-direct {p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 232
    .line 233
    .line 234
    move-result v1

    .line 235
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 236
    .line 237
    .line 238
    const/16 v0, 0xa

    .line 239
    .line 240
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 241
    .line 242
    .line 243
    const-string v0, "folHlink"

    .line 244
    .line 245
    invoke-interface {p1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 246
    .line 247
    .line 248
    move-result-object p1

    .line 249
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getColorIndex(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/ss/model/baseModel/Workbook;)I

    .line 250
    .line 251
    .line 252
    move-result p1

    .line 253
    invoke-virtual {p2, v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addSchemeColorIndex(Ljava/lang/String;I)V

    .line 254
    .line 255
    .line 256
    const/16 v0, 0xb

    .line 257
    .line 258
    invoke-virtual {p2, v0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addThemeColorIndex(II)V

    .line 259
    .line 260
    .line 261
    return-void
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

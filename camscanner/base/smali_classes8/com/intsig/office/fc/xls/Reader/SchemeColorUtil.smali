.class public Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;
.super Ljava/lang/Object;
.source "SchemeColorUtil.java"


# static fields
.field private static schemeClrName:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static schemeColor:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static getSchemeColor(Lcom/intsig/office/ss/model/baseModel/Workbook;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/ss/model/baseModel/Workbook;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->init(Lcom/intsig/office/ss/model/baseModel/Workbook;)V

    .line 2
    .line 3
    .line 4
    sget-object p0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeColor:Ljava/util/Map;

    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static getThemeColor(Lcom/intsig/office/ss/model/baseModel/Workbook;I)I
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->init(Lcom/intsig/office/ss/model/baseModel/Workbook;)V

    .line 2
    .line 3
    .line 4
    if-ltz p1, :cond_1

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-lt p1, v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    check-cast p1, Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {p0, p1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSchemeColor(Ljava/lang/String;)I

    .line 24
    .line 25
    .line 26
    move-result p0

    .line 27
    return p0

    .line 28
    :cond_1
    :goto_0
    const/4 p0, -0x1

    .line 29
    return p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static init(Lcom/intsig/office/ss/model/baseModel/Workbook;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeColor:Ljava/util/Map;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 11
    .line 12
    const-string v1, "bg1"

    .line 13
    .line 14
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 18
    .line 19
    const-string v1, "tx1"

    .line 20
    .line 21
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 25
    .line 26
    const-string v1, "bg2"

    .line 27
    .line 28
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 32
    .line 33
    const-string v1, "tx2"

    .line 34
    .line 35
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 39
    .line 40
    const-string v1, "accent1"

    .line 41
    .line 42
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 46
    .line 47
    const-string v1, "accent2"

    .line 48
    .line 49
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 53
    .line 54
    const-string v1, "accent3"

    .line 55
    .line 56
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 60
    .line 61
    const-string v1, "accent4"

    .line 62
    .line 63
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 67
    .line 68
    const-string v1, "accent5"

    .line 69
    .line 70
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 74
    .line 75
    const-string v1, "accent6"

    .line 76
    .line 77
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 81
    .line 82
    const-string v1, "hlink"

    .line 83
    .line 84
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 88
    .line 89
    const-string v1, "folHlink"

    .line 90
    .line 91
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 95
    .line 96
    const-string v1, "dk1"

    .line 97
    .line 98
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 102
    .line 103
    const-string v1, "lt1"

    .line 104
    .line 105
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 109
    .line 110
    const-string v1, "dk2"

    .line 111
    .line 112
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 116
    .line 117
    const-string v1, "lt2"

    .line 118
    .line 119
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    .line 121
    .line 122
    new-instance v0, Ljava/util/HashMap;

    .line 123
    .line 124
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 125
    .line 126
    .line 127
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeColor:Ljava/util/Map;

    .line 128
    .line 129
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeColor:Ljava/util/Map;

    .line 130
    .line 131
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 132
    .line 133
    .line 134
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeClrName:Ljava/util/List;

    .line 135
    .line 136
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 141
    .line 142
    .line 143
    move-result v1

    .line 144
    if-eqz v1, :cond_1

    .line 145
    .line 146
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    check-cast v1, Ljava/lang/String;

    .line 151
    .line 152
    sget-object v2, Lcom/intsig/office/fc/xls/Reader/SchemeColorUtil;->schemeColor:Ljava/util/Map;

    .line 153
    .line 154
    invoke-virtual {p0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;->getSchemeColor(Ljava/lang/String;)I

    .line 155
    .line 156
    .line 157
    move-result v3

    .line 158
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 159
    .line 160
    .line 161
    move-result-object v3

    .line 162
    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    .line 164
    .line 165
    goto :goto_0

    .line 166
    :cond_1
    return-void
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

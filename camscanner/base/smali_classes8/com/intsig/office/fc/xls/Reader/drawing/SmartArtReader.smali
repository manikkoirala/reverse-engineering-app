.class public Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;
.super Ljava/lang/Object;
.source "SmartArtReader.java"


# static fields
.field private static reader:Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;


# instance fields
.field private offset:I

.field private sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->reader:Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getTextBoxData(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/shape/TextBox;
    .locals 16

    .line 1
    move-object/from16 v0, p2

    .line 2
    .line 3
    const-string v1, "txXfrm"

    .line 4
    .line 5
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    const/4 v3, 0x0

    .line 10
    if-eqz v2, :cond_0

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 13
    .line 14
    .line 15
    move-result-object v4

    .line 16
    const/high16 v5, 0x3f800000    # 1.0f

    .line 17
    .line 18
    invoke-virtual {v4, v2, v5, v5}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    move-object v4, v3

    .line 24
    :goto_0
    const-string v5, "txBody"

    .line 25
    .line 26
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 27
    .line 28
    .line 29
    move-result-object v5

    .line 30
    if-eqz v5, :cond_5

    .line 31
    .line 32
    new-instance v6, Lcom/intsig/office/common/shape/TextBox;

    .line 33
    .line 34
    invoke-direct {v6}, Lcom/intsig/office/common/shape/TextBox;-><init>()V

    .line 35
    .line 36
    .line 37
    new-instance v7, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 38
    .line 39
    invoke-direct {v7}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 40
    .line 41
    .line 42
    const-wide/16 v8, 0x0

    .line 43
    .line 44
    invoke-virtual {v7, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v6, v7}, Lcom/intsig/office/common/shape/TextBox;->setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v7}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 51
    .line 52
    .line 53
    move-result-object v12

    .line 54
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 55
    .line 56
    .line 57
    move-result-object v8

    .line 58
    iget v9, v4, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 59
    .line 60
    int-to-float v9, v9

    .line 61
    const/high16 v10, 0x41700000    # 15.0f

    .line 62
    .line 63
    mul-float v9, v9, v10

    .line 64
    .line 65
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 66
    .line 67
    .line 68
    move-result v9

    .line 69
    invoke-virtual {v8, v12, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 70
    .line 71
    .line 72
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 73
    .line 74
    .line 75
    move-result-object v8

    .line 76
    iget v9, v4, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 77
    .line 78
    int-to-float v9, v9

    .line 79
    mul-float v9, v9, v10

    .line 80
    .line 81
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 82
    .line 83
    .line 84
    move-result v9

    .line 85
    invoke-virtual {v8, v12, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 86
    .line 87
    .line 88
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 89
    .line 90
    .line 91
    move-result-object v8

    .line 92
    const/high16 v9, 0x41f00000    # 30.0f

    .line 93
    .line 94
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 95
    .line 96
    .line 97
    move-result v10

    .line 98
    invoke-virtual {v8, v12, v10}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 99
    .line 100
    .line 101
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 102
    .line 103
    .line 104
    move-result-object v8

    .line 105
    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    .line 106
    .line 107
    .line 108
    move-result v9

    .line 109
    invoke-virtual {v8, v12, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 110
    .line 111
    .line 112
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 113
    .line 114
    .line 115
    move-result-object v8

    .line 116
    const/4 v9, 0x0

    .line 117
    invoke-virtual {v8, v12, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 118
    .line 119
    .line 120
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 121
    .line 122
    .line 123
    move-result-object v8

    .line 124
    invoke-virtual {v8, v12, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 125
    .line 126
    .line 127
    const-string v8, "bodyPr"

    .line 128
    .line 129
    invoke-interface {v2, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 130
    .line 131
    .line 132
    move-result-object v2

    .line 133
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/SectionAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/SectionAttr;

    .line 134
    .line 135
    .line 136
    move-result-object v10

    .line 137
    const/4 v13, 0x0

    .line 138
    const/4 v14, 0x0

    .line 139
    const/4 v15, 0x0

    .line 140
    move-object v11, v2

    .line 141
    invoke-virtual/range {v10 .. v15}, Lcom/intsig/office/fc/ppt/attribute/SectionAttr;->setSectionAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 142
    .line 143
    .line 144
    if-eqz v2, :cond_3

    .line 145
    .line 146
    const-string v8, "wrap"

    .line 147
    .line 148
    invoke-interface {v2, v8}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v2

    .line 152
    if-eqz v2, :cond_1

    .line 153
    .line 154
    const-string v8, "square"

    .line 155
    .line 156
    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 157
    .line 158
    .line 159
    move-result v2

    .line 160
    if-eqz v2, :cond_2

    .line 161
    .line 162
    :cond_1
    const/4 v9, 0x1

    .line 163
    :cond_2
    invoke-virtual {v6, v9}, Lcom/intsig/office/common/shape/TextBox;->setWrapLine(Z)V

    .line 164
    .line 165
    .line 166
    :cond_3
    move-object/from16 v2, p0

    .line 167
    .line 168
    move-object/from16 v8, p1

    .line 169
    .line 170
    invoke-direct {v2, v8, v7, v5}, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->processParagraph(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;)I

    .line 171
    .line 172
    .line 173
    move-result v5

    .line 174
    int-to-long v8, v5

    .line 175
    invoke-virtual {v7, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 176
    .line 177
    .line 178
    invoke-virtual {v6, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 179
    .line 180
    .line 181
    invoke-virtual {v6}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 182
    .line 183
    .line 184
    move-result-object v4

    .line 185
    if-eqz v4, :cond_4

    .line 186
    .line 187
    invoke-virtual {v6}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 188
    .line 189
    .line 190
    move-result-object v4

    .line 191
    invoke-virtual {v4, v3}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v4

    .line 195
    if-eqz v4, :cond_4

    .line 196
    .line 197
    invoke-virtual {v6}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 198
    .line 199
    .line 200
    move-result-object v4

    .line 201
    invoke-virtual {v4, v3}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v4

    .line 205
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 206
    .line 207
    .line 208
    move-result v4

    .line 209
    if-lez v4, :cond_4

    .line 210
    .line 211
    invoke-virtual {v6}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 212
    .line 213
    .line 214
    move-result-object v4

    .line 215
    invoke-virtual {v4, v3}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v3

    .line 219
    const-string v4, "\n"

    .line 220
    .line 221
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 222
    .line 223
    .line 224
    move-result v3

    .line 225
    if-nez v3, :cond_4

    .line 226
    .line 227
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 228
    .line 229
    .line 230
    move-result-object v3

    .line 231
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 232
    .line 233
    .line 234
    move-result-object v0

    .line 235
    invoke-virtual {v3, v6, v0}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 236
    .line 237
    .line 238
    :cond_4
    return-object v6

    .line 239
    :cond_5
    move-object/from16 v2, p0

    .line 240
    .line 241
    return-object v3
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public static instance()Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->reader:Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private processParagraph(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;)I
    .locals 18

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    iput v0, v6, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 5
    .line 6
    const-string v0, "p"

    .line 7
    .line 8
    move-object/from16 v1, p3

    .line 9
    .line 10
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v7

    .line 18
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    move-object v4, v0

    .line 29
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    const-string v0, "pPr"

    .line 32
    .line 33
    invoke-interface {v4, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 34
    .line 35
    .line 36
    move-result-object v10

    .line 37
    new-instance v3, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 38
    .line 39
    invoke-direct {v3}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 40
    .line 41
    .line 42
    iget v0, v6, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 43
    .line 44
    int-to-long v0, v0

    .line 45
    invoke-virtual {v3, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 46
    .line 47
    .line 48
    const/4 v5, 0x0

    .line 49
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 50
    .line 51
    .line 52
    move-result-object v8

    .line 53
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 54
    .line 55
    .line 56
    move-result-object v11

    .line 57
    const/4 v13, -0x1

    .line 58
    const/4 v14, -0x1

    .line 59
    const/4 v15, 0x0

    .line 60
    const/16 v16, 0x0

    .line 61
    .line 62
    const/16 v17, 0x0

    .line 63
    .line 64
    const/4 v12, 0x0

    .line 65
    move-object/from16 v9, p1

    .line 66
    .line 67
    invoke-virtual/range {v8 .. v17}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIIZZ)V

    .line 68
    .line 69
    .line 70
    move-object/from16 v0, p0

    .line 71
    .line 72
    move-object/from16 v1, p1

    .line 73
    .line 74
    move-object/from16 v2, p2

    .line 75
    .line 76
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->processRun(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    iget v1, v6, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 81
    .line 82
    int-to-long v1, v1

    .line 83
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 84
    .line 85
    .line 86
    const-wide/16 v1, 0x0

    .line 87
    .line 88
    move-object/from16 v3, p2

    .line 89
    .line 90
    invoke-virtual {v3, v0, v1, v2}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_0
    iget v0, v6, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 95
    .line 96
    return v0
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private processRun(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)Lcom/intsig/office/simpletext/model/ParagraphElement;
    .locals 24

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p3

    .line 4
    .line 5
    move-object/from16 v2, p4

    .line 6
    .line 7
    const-string v3, "r"

    .line 8
    .line 9
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v4

    .line 13
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v5

    .line 17
    const-string v6, "rPr"

    .line 18
    .line 19
    const-string v7, "pPr"

    .line 20
    .line 21
    const-string v8, "\n"

    .line 22
    .line 23
    if-nez v5, :cond_1

    .line 24
    .line 25
    new-instance v3, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 26
    .line 27
    invoke-direct {v3, v8}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-interface {v2, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    if-eqz v2, :cond_0

    .line 35
    .line 36
    invoke-interface {v2, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    if-eqz v2, :cond_0

    .line 41
    .line 42
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 43
    .line 44
    .line 45
    move-result-object v4

    .line 46
    iget-object v5, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 47
    .line 48
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 49
    .line 50
    .line 51
    move-result-object v6

    .line 52
    move-object/from16 v9, p5

    .line 53
    .line 54
    invoke-virtual {v4, v5, v2, v6, v9}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 55
    .line 56
    .line 57
    :cond_0
    iget v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 58
    .line 59
    int-to-long v4, v2

    .line 60
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 61
    .line 62
    .line 63
    iget v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 64
    .line 65
    add-int/lit8 v2, v2, 0x1

    .line 66
    .line 67
    iput v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 68
    .line 69
    int-to-long v4, v2

    .line 70
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v3}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 74
    .line 75
    .line 76
    return-object v1

    .line 77
    :cond_1
    move-object/from16 v9, p5

    .line 78
    .line 79
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 80
    .line 81
    .line 82
    move-result-object v4

    .line 83
    const/4 v5, 0x0

    .line 84
    move-object v10, v5

    .line 85
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 86
    .line 87
    .line 88
    move-result v11

    .line 89
    if-eqz v11, :cond_5

    .line 90
    .line 91
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    move-result-object v11

    .line 95
    check-cast v11, Lcom/intsig/office/fc/dom4j/Element;

    .line 96
    .line 97
    invoke-interface {v11}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v12

    .line 101
    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 102
    .line 103
    .line 104
    move-result v12

    .line 105
    if-eqz v12, :cond_2

    .line 106
    .line 107
    const-string v12, "t"

    .line 108
    .line 109
    invoke-interface {v11, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 110
    .line 111
    .line 112
    move-result-object v12

    .line 113
    if-eqz v12, :cond_4

    .line 114
    .line 115
    invoke-interface {v12}, Lcom/intsig/office/fc/dom4j/Element;->getText()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v12

    .line 119
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    .line 120
    .line 121
    .line 122
    move-result v13

    .line 123
    if-ltz v13, :cond_4

    .line 124
    .line 125
    new-instance v10, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 126
    .line 127
    invoke-direct {v10, v12}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 131
    .line 132
    .line 133
    move-result-object v12

    .line 134
    iget-object v14, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 135
    .line 136
    invoke-interface {v11, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 137
    .line 138
    .line 139
    move-result-object v11

    .line 140
    invoke-virtual {v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 141
    .line 142
    .line 143
    move-result-object v15

    .line 144
    invoke-virtual {v12, v14, v11, v15, v9}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/ss/model/baseModel/Sheet;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 145
    .line 146
    .line 147
    iget v11, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 148
    .line 149
    int-to-long v11, v11

    .line 150
    invoke-virtual {v10, v11, v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 151
    .line 152
    .line 153
    iget v11, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 154
    .line 155
    add-int/2addr v11, v13

    .line 156
    iput v11, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 157
    .line 158
    int-to-long v11, v11

    .line 159
    invoke-virtual {v10, v11, v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v1, v10}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 163
    .line 164
    .line 165
    goto :goto_1

    .line 166
    :cond_2
    invoke-interface {v11}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v11

    .line 170
    const-string v12, "br"

    .line 171
    .line 172
    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 173
    .line 174
    .line 175
    move-result v11

    .line 176
    if-eqz v11, :cond_4

    .line 177
    .line 178
    if-eqz v10, :cond_3

    .line 179
    .line 180
    new-instance v9, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v10, v5}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v11

    .line 189
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v9

    .line 199
    invoke-virtual {v10, v9}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    iget v9, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 203
    .line 204
    add-int/lit8 v9, v9, 0x1

    .line 205
    .line 206
    iput v9, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 207
    .line 208
    :cond_3
    iget v9, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 209
    .line 210
    int-to-long v11, v9

    .line 211
    invoke-virtual {v1, v11, v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 212
    .line 213
    .line 214
    const-wide/16 v11, 0x0

    .line 215
    .line 216
    move-object/from16 v13, p2

    .line 217
    .line 218
    invoke-virtual {v13, v1, v11, v12}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 219
    .line 220
    .line 221
    new-instance v1, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 222
    .line 223
    invoke-direct {v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 224
    .line 225
    .line 226
    iget v9, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 227
    .line 228
    int-to-long v11, v9

    .line 229
    invoke-virtual {v1, v11, v12}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 230
    .line 231
    .line 232
    const/4 v9, 0x0

    .line 233
    invoke-interface {v2, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 234
    .line 235
    .line 236
    move-result-object v16

    .line 237
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 238
    .line 239
    .line 240
    move-result-object v14

    .line 241
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 242
    .line 243
    .line 244
    move-result-object v17

    .line 245
    const/16 v19, -0x1

    .line 246
    .line 247
    const/16 v20, -0x1

    .line 248
    .line 249
    const/16 v21, 0x0

    .line 250
    .line 251
    const/16 v22, 0x0

    .line 252
    .line 253
    const/16 v23, 0x0

    .line 254
    .line 255
    move-object/from16 v15, p1

    .line 256
    .line 257
    move-object/from16 v18, v9

    .line 258
    .line 259
    invoke-virtual/range {v14 .. v23}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIIZZ)V

    .line 260
    .line 261
    .line 262
    goto/16 :goto_0

    .line 263
    .line 264
    :cond_4
    :goto_1
    move-object/from16 v13, p2

    .line 265
    .line 266
    goto/16 :goto_0

    .line 267
    .line 268
    :cond_5
    if-eqz v10, :cond_6

    .line 269
    .line 270
    new-instance v2, Ljava/lang/StringBuilder;

    .line 271
    .line 272
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 273
    .line 274
    .line 275
    invoke-virtual {v10, v5}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 276
    .line 277
    .line 278
    move-result-object v3

    .line 279
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    .line 281
    .line 282
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    .line 284
    .line 285
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object v2

    .line 289
    invoke-virtual {v10, v2}, Lcom/intsig/office/simpletext/model/LeafElement;->setText(Ljava/lang/String;)V

    .line 290
    .line 291
    .line 292
    iget v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 293
    .line 294
    add-int/lit8 v2, v2, 0x1

    .line 295
    .line 296
    iput v2, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->offset:I

    .line 297
    .line 298
    :cond_6
    return-object v1
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method


# virtual methods
.method public read(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/util/Map;Lcom/intsig/office/ss/model/baseModel/Sheet;)Lcom/intsig/office/common/shape/SmartArt;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/system/IControl;",
            "Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;",
            "Lcom/intsig/office/fc/openxml4j/opc/PackagePart;",
            "Lcom/intsig/office/fc/openxml4j/opc/PackagePart;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/intsig/office/ss/model/baseModel/Sheet;",
            ")",
            "Lcom/intsig/office/common/shape/SmartArt;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v8, p1

    .line 4
    .line 5
    move-object/from16 v9, p2

    .line 6
    .line 7
    move-object/from16 v10, p4

    .line 8
    .line 9
    move-object/from16 v11, p5

    .line 10
    .line 11
    move-object/from16 v1, p6

    .line 12
    .line 13
    iput-object v1, v0, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->sheet:Lcom/intsig/office/ss/model/baseModel/Sheet;

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 16
    .line 17
    invoke-direct {v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual/range {p4 .. p4}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 29
    .line 30
    .line 31
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    const-string v3, "bg"

    .line 36
    .line 37
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-static {v8, v9, v10, v3, v11}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    const-string v4, "whole"

    .line 46
    .line 47
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    const-string v5, "ln"

    .line 52
    .line 53
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    invoke-static {v8, v9, v10, v4, v11}, Lcom/intsig/office/fc/LineKit;->createLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Ljava/util/Map;)Lcom/intsig/office/common/borders/Line;

    .line 58
    .line 59
    .line 60
    move-result-object v4

    .line 61
    const-string v5, "extLst"

    .line 62
    .line 63
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    const/4 v12, 0x0

    .line 68
    if-eqz v2, :cond_0

    .line 69
    .line 70
    const-string v5, "ext"

    .line 71
    .line 72
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    if-eqz v2, :cond_0

    .line 77
    .line 78
    const-string v5, "dataModelExt"

    .line 79
    .line 80
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    if-eqz v2, :cond_0

    .line 85
    .line 86
    const-string v5, "relId"

    .line 87
    .line 88
    invoke-interface {v2, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    if-eqz v2, :cond_0

    .line 93
    .line 94
    move-object/from16 v5, p3

    .line 95
    .line 96
    invoke-virtual {v5, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    invoke-virtual {v9, v2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 105
    .line 106
    .line 107
    move-result-object v2

    .line 108
    goto :goto_0

    .line 109
    :cond_0
    move-object v2, v12

    .line 110
    :goto_0
    if-nez v2, :cond_1

    .line 111
    .line 112
    return-object v12

    .line 113
    :cond_1
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 114
    .line 115
    .line 116
    move-result-object v2

    .line 117
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 118
    .line 119
    .line 120
    move-result-object v1

    .line 121
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 122
    .line 123
    .line 124
    new-instance v13, Lcom/intsig/office/common/shape/SmartArt;

    .line 125
    .line 126
    invoke-direct {v13}, Lcom/intsig/office/common/shape/SmartArt;-><init>()V

    .line 127
    .line 128
    .line 129
    invoke-virtual {v13, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {v13, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 133
    .line 134
    .line 135
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 136
    .line 137
    .line 138
    move-result-object v1

    .line 139
    const-string v2, "spTree"

    .line 140
    .line 141
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    const-string v2, "sp"

    .line 146
    .line 147
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator(Ljava/lang/String;)Ljava/util/Iterator;

    .line 148
    .line 149
    .line 150
    move-result-object v14

    .line 151
    :cond_2
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    .line 152
    .line 153
    .line 154
    move-result v1

    .line 155
    if-eqz v1, :cond_5

    .line 156
    .line 157
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 158
    .line 159
    .line 160
    move-result-object v1

    .line 161
    move-object v15, v1

    .line 162
    check-cast v15, Lcom/intsig/office/fc/dom4j/Element;

    .line 163
    .line 164
    const-string v1, "spPr"

    .line 165
    .line 166
    invoke-interface {v15, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 167
    .line 168
    .line 169
    move-result-object v1

    .line 170
    if-eqz v1, :cond_3

    .line 171
    .line 172
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 173
    .line 174
    .line 175
    move-result-object v2

    .line 176
    const-string v3, "xfrm"

    .line 177
    .line 178
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 179
    .line 180
    .line 181
    move-result-object v1

    .line 182
    const/high16 v3, 0x3f800000    # 1.0f

    .line 183
    .line 184
    invoke-virtual {v2, v1, v3, v3}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 185
    .line 186
    .line 187
    move-result-object v1

    .line 188
    move-object v5, v1

    .line 189
    goto :goto_2

    .line 190
    :cond_3
    move-object v5, v12

    .line 191
    :goto_2
    const/4 v7, 0x1

    .line 192
    move-object/from16 v1, p1

    .line 193
    .line 194
    move-object/from16 v2, p2

    .line 195
    .line 196
    move-object/from16 v3, p4

    .line 197
    .line 198
    move-object v4, v15

    .line 199
    move-object/from16 v6, p5

    .line 200
    .line 201
    invoke-static/range {v1 .. v7}, Lcom/intsig/office/common/autoshape/AutoShapeDataKit;->getAutoShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;Ljava/util/Map;I)Lcom/intsig/office/common/shape/AbstractShape;

    .line 202
    .line 203
    .line 204
    move-result-object v1

    .line 205
    if-eqz v1, :cond_4

    .line 206
    .line 207
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/SmartArt;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 208
    .line 209
    .line 210
    :cond_4
    invoke-direct {v0, v8, v15}, Lcom/intsig/office/fc/xls/Reader/drawing/SmartArtReader;->getTextBoxData(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/shape/TextBox;

    .line 211
    .line 212
    .line 213
    move-result-object v1

    .line 214
    if-eqz v1, :cond_2

    .line 215
    .line 216
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/SmartArt;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 217
    .line 218
    .line 219
    goto :goto_1

    .line 220
    :cond_5
    return-object v13
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

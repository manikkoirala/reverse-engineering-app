.class public Lcom/intsig/office/fc/xls/XLSXReader;
.super Lcom/intsig/office/fc/xls/SSReader;
.source "XLSXReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/xls/XLSXReader$SearchSharedStringSaxHandler;,
        Lcom/intsig/office/fc/xls/XLSXReader$SharedStringSaxHandler;
    }
.end annotation


# instance fields
.field private book:Lcom/intsig/office/ss/model/baseModel/Workbook;

.field private filePath:Ljava/lang/String;

.field private key:Ljava/lang/String;

.field private packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

.field private searched:Z

.field private sharedStringIndex:I

.field private uri:Landroid/net/Uri;

.field private zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/xls/SSReader;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/fc/xls/XLSXReader;->filePath:Ljava/lang/String;

    .line 7
    .line 8
    iput-object p3, p0, Lcom/intsig/office/fc/xls/XLSXReader;->uri:Landroid/net/Uri;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method static bridge synthetic O8(Lcom/intsig/office/fc/xls/XLSXReader;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->sharedStringIndex:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic Oo08(Lcom/intsig/office/fc/xls/XLSXReader;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->searched:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static synthetic access$000(Lcom/intsig/office/fc/xls/XLSXReader;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static synthetic access$100(Lcom/intsig/office/fc/xls/XLSXReader;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getPaletteColor()V
    .locals 8

    .line 1
    new-instance v0, Lcom/intsig/office/ss/model/sheetProperty/Palette;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/ss/model/sheetProperty/Palette;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x8

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/office/ss/model/sheetProperty/Palette;->getColor(I)[B

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    :goto_0
    if-eqz v2, :cond_0

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/office/fc/xls/XLSXReader;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 15
    .line 16
    add-int/lit8 v4, v1, 0x1

    .line 17
    .line 18
    const/4 v5, 0x0

    .line 19
    aget-byte v5, v2, v5

    .line 20
    .line 21
    const/4 v6, 0x1

    .line 22
    aget-byte v6, v2, v6

    .line 23
    .line 24
    const/4 v7, 0x2

    .line 25
    aget-byte v2, v2, v7

    .line 26
    .line 27
    invoke-static {v5, v6, v2}, Lcom/intsig/office/ss/util/ColorUtil;->rgb(BBB)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    invoke-virtual {v3, v1, v2}, Lcom/intsig/office/ss/model/baseModel/Workbook;->addColor(II)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v4}, Lcom/intsig/office/ss/model/sheetProperty/Palette;->getColor(I)[B

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    move v1, v4

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/ss/model/sheetProperty/Palette;->dispose()V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private getSharedString(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    const-string v0, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-gtz v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    iget-object v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iput v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->sharedStringIndex:I

    .line 30
    .line 31
    new-instance v0, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 32
    .line 33
    invoke-direct {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 34
    .line 35
    .line 36
    :try_start_0
    const-string v1, "/sst/si"

    .line 37
    .line 38
    new-instance v2, Lcom/intsig/office/fc/xls/XLSXReader$SharedStringSaxHandler;

    .line 39
    .line 40
    invoke-direct {v2, p0}, Lcom/intsig/office/fc/xls/XLSXReader$SharedStringSaxHandler;-><init>(Lcom/intsig/office/fc/xls/XLSXReader;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 57
    .line 58
    .line 59
    return-void

    .line 60
    :catchall_0
    move-exception p1

    .line 61
    invoke-virtual {v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 62
    .line 63
    .line 64
    throw p1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getStyles(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    const-string v0, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-gtz v1, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-static {}, Lcom/intsig/office/fc/xls/Reader/shared/StyleReader;->instance()Lcom/intsig/office/fc/xls/Reader/shared/StyleReader;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 38
    .line 39
    invoke-virtual {v0, p1, v1, p0}, Lcom/intsig/office/fc/xls/Reader/shared/StyleReader;->getWorkBookStyle(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/system/IReader;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getThemeColor(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    const-string v0, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-gtz v1, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-static {}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->instance()Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 38
    .line 39
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/fc/xls/Reader/shared/ThemeColorReader;->getThemeColor(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/ss/model/baseModel/Workbook;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getWorkBookSharedObjects()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/xls/XLSXReader;->getPaletteColor()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/xls/XLSXReader;->getThemeColor(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 10
    .line 11
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/xls/XLSXReader;->getStyles(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 15
    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/xls/XLSXReader;->getSharedString(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private initPackagePart()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 2
    .line 3
    const-string v1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "/xl/workbook.xml"

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 31
    .line 32
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iput-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 37
    .line 38
    return-void

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    .line 40
    .line 41
    const-string v1, "Format error"

    .line 42
    .line 43
    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    throw v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method static bridge synthetic o〇0(Lcom/intsig/office/fc/xls/XLSXReader;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->sharedStringIndex:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private processWorkbook()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/xls/XLSXReader;->getWorkBookSharedObjects()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/office/fc/xls/Reader/WorkbookReader;->instance()Lcom/intsig/office/fc/xls/Reader/WorkbookReader;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/fc/xls/XLSXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 11
    .line 12
    iget-object v3, p0, Lcom/intsig/office/fc/xls/XLSXReader;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 13
    .line 14
    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/intsig/office/fc/xls/Reader/WorkbookReader;->read(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/ss/model/baseModel/Workbook;Lcom/intsig/office/fc/xls/SSReader;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private searchContent_SharedString(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/lang/String;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    const-string v0, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    if-gtz v0, :cond_0

    .line 13
    .line 14
    return v1

    .line 15
    :cond_0
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iput-object p2, p0, Lcom/intsig/office/fc/xls/XLSXReader;->key:Ljava/lang/String;

    .line 30
    .line 31
    iput-boolean v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->searched:Z

    .line 32
    .line 33
    new-instance p2, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 34
    .line 35
    invoke-direct {p2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 36
    .line 37
    .line 38
    :try_start_0
    const-string v0, "/sst/si"

    .line 39
    .line 40
    new-instance v1, Lcom/intsig/office/fc/xls/XLSXReader$SearchSharedStringSaxHandler;

    .line 41
    .line 42
    invoke-direct {v1, p0}, Lcom/intsig/office/fc/xls/XLSXReader$SearchSharedStringSaxHandler;-><init>(Lcom/intsig/office/fc/xls/XLSXReader;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-virtual {p2, p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Lcom/intsig/office/system/StopReaderError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    .line 57
    .line 58
    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 59
    .line 60
    .line 61
    iget-boolean p1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->searched:Z

    .line 62
    .line 63
    return p1

    .line 64
    :catchall_0
    move-exception p1

    .line 65
    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 66
    .line 67
    .line 68
    throw p1

    .line 69
    :catch_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 70
    .line 71
    .line 72
    const/4 p1, 0x1

    .line 73
    return p1
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/fc/xls/XLSXReader;)Lcom/intsig/office/ss/model/baseModel/Workbook;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/fc/xls/XLSXReader;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->key:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/fc/xls/XLSXReader;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->searched:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/system/AbstractReader;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->filePath:Ljava/lang/String;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->key:Ljava/lang/String;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getModel()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/office/ss/model/baseModel/Workbook;-><init>(Z)V

    .line 5
    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 10
    .line 11
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getActivity()Landroid/app/Activity;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->uri:Landroid/net/Uri;

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/office/ss/util/StreamUtils;->getInputStream(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/InputStream;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    new-instance v1, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 22
    .line 23
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;-><init>(Ljava/io/InputStream;)V

    .line 24
    .line 25
    .line 26
    iput-object v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/office/fc/xls/XLSXReader;->initPackagePart()V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/office/fc/xls/XLSXReader;->processWorkbook()V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->book:Lcom/intsig/office/ss/model/baseModel/Workbook;

    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public searchContent(Ljava/io/File;Ljava/lang/String;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    new-instance v0, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;-><init>(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 15
    .line 16
    const-string p1, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"

    .line 17
    .line 18
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getRelationshipsByType(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const/4 v0, 0x0

    .line 23
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationshipCollection;->getRelationship(I)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 28
    .line 29
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iput-object p1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 34
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/xls/XLSXReader;->searchContent_SharedString(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-eqz p1, :cond_0

    .line 40
    .line 41
    const/4 p1, 0x1

    .line 42
    goto :goto_0

    .line 43
    :cond_0
    invoke-static {}, Lcom/intsig/office/fc/xls/Reader/WorkbookReader;->instance()Lcom/intsig/office/fc/xls/Reader/WorkbookReader;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    iget-object v0, p0, Lcom/intsig/office/fc/xls/XLSXReader;->zipPackage:Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/office/fc/xls/XLSXReader;->packagePart:Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 50
    .line 51
    invoke-virtual {p1, v0, p0, v1, p2}, Lcom/intsig/office/fc/xls/Reader/WorkbookReader;->searchContent(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/system/IReader;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/lang/String;)Z

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/xls/XLSXReader;->dispose()V

    .line 56
    .line 57
    .line 58
    return p1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

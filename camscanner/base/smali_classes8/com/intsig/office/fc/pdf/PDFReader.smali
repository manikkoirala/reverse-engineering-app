.class public Lcom/intsig/office/fc/pdf/PDFReader;
.super Lcom/intsig/office/system/AbstractReader;
.source "PDFReader.java"


# instance fields
.field private filePath:Ljava/lang/String;

.field private lib:Lcom/intsig/office/fc/pdf/PDFLib;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/system/AbstractReader;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/fc/pdf/PDFReader;->filePath:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/system/AbstractReader;->dispose()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/pdf/PDFReader;->lib:Lcom/intsig/office/fc/pdf/PDFLib;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getModel()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    const/16 v1, 0x1a

    .line 4
    .line 5
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 6
    .line 7
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/office/fc/pdf/PDFLib;->getPDFLib()Lcom/intsig/office/fc/pdf/PDFLib;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iput-object v0, p0, Lcom/intsig/office/fc/pdf/PDFReader;->lib:Lcom/intsig/office/fc/pdf/PDFLib;

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/pdf/PDFReader;->filePath:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/pdf/PDFLib;->openFileSync(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/office/fc/pdf/PDFReader;->lib:Lcom/intsig/office/fc/pdf/PDFLib;

    .line 22
    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public Lcom/intsig/office/fc/poifs/property/DocumentProperty;
.super Lcom/intsig/office/fc/poifs/property/Property;
.source "DocumentProperty.java"


# instance fields
.field private _document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;


# direct methods
.method protected constructor <init>(I[BI)V
    .locals 0

    .line 7
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/property/Property;-><init>(I[BI)V

    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/DocumentProperty;->_document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/property/Property;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/property/DocumentProperty;->_document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setName(Ljava/lang/String;)V

    .line 4
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/poifs/property/Property;->setSize(I)V

    const/4 p1, 0x1

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setNodeColor(B)V

    const/4 p1, 0x2

    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setPropertyType(B)V

    return-void
.end method


# virtual methods
.method public getDocument()Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/DocumentProperty;->_document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDirectory()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected preWrite()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setDocument(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/DocumentProperty;->_document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public shouldUseSmallBlocks()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/poifs/property/Property;->shouldUseSmallBlocks()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public abstract Lcom/intsig/office/fc/poifs/property/PropertyTableBase;
.super Ljava/lang/Object;
.source "PropertyTableBase.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/filesystem/BATManaged;


# instance fields
.field private final _header_block:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

.field protected final _properties:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/poifs/property/Property;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_header_block:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 3
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 4
    new-instance p1, Lcom/intsig/office/fc/poifs/property/RootProperty;

    invoke-direct {p1}, Lcom/intsig/office/fc/poifs/property/RootProperty;-><init>()V

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->addProperty(Lcom/intsig/office/fc/poifs/property/Property;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/poifs/storage/HeaderBlock;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/poifs/property/Property;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_header_block:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 7
    iput-object p2, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    const/4 p1, 0x0

    .line 8
    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->populatePropertyTree(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;)V

    return-void
.end method

.method private populatePropertyTree(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/Property;->getChildIndex()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {v0}, Lcom/intsig/office/fc/poifs/property/Property;->isValidIndex(I)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    new-instance v1, Ljava/util/Stack;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    .line 15
    .line 16
    .line 17
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 18
    .line 19
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Lcom/intsig/office/fc/poifs/property/Property;

    .line 24
    .line 25
    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-nez v0, :cond_4

    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    check-cast v0, Lcom/intsig/office/fc/poifs/property/Property;

    .line 39
    .line 40
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->addChild(Lcom/intsig/office/fc/poifs/property/Property;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->isDirectory()Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-eqz v2, :cond_2

    .line 48
    .line 49
    move-object v2, v0

    .line 50
    check-cast v2, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    .line 51
    .line 52
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->populatePropertyTree(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;)V

    .line 53
    .line 54
    .line 55
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getPreviousChildIndex()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    invoke-static {v2}, Lcom/intsig/office/fc/poifs/property/Property;->isValidIndex(I)Z

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    if-eqz v3, :cond_3

    .line 64
    .line 65
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 66
    .line 67
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    check-cast v2, Lcom/intsig/office/fc/poifs/property/Property;

    .line 72
    .line 73
    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getNextChildIndex()I

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    invoke-static {v0}, Lcom/intsig/office/fc/poifs/property/Property;->isValidIndex(I)Z

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    if-eqz v2, :cond_1

    .line 85
    .line 86
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 87
    .line 88
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    check-cast v0, Lcom/intsig/office/fc/poifs/property/Property;

    .line 93
    .line 94
    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_4
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public addProperty(Lcom/intsig/office/fc/poifs/property/Property;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getRoot()Lcom/intsig/office/fc/poifs/property/RootProperty;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStartBlock()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_header_block:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getPropertyStart()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public removeProperty(Lcom/intsig/office/fc/poifs/property/Property;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setStartBlock(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_header_block:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->setPropertyStart(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

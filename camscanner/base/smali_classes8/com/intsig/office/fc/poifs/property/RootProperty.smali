.class public final Lcom/intsig/office/fc/poifs/property/RootProperty;
.super Lcom/intsig/office/fc/poifs/property/DirectoryProperty;
.source "RootProperty.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "Root Entry"


# direct methods
.method constructor <init>()V
    .locals 1

    const-string v0, "Root Entry"

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/property/Property;->setNodeColor(B)V

    const/4 v0, 0x5

    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/property/Property;->setPropertyType(B)V

    const/4 v0, -0x2

    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/property/Property;->setStartBlock(I)V

    return-void
.end method

.method protected constructor <init>(I[BI)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;-><init>(I[BI)V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "Root Entry"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setSize(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->calcSize(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    invoke-super {p0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setSize(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

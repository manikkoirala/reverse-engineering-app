.class public interface abstract Lcom/intsig/office/fc/poifs/filesystem/Entry;
.super Ljava/lang/Object;
.source "Entry.java"


# virtual methods
.method public abstract delete()Z
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getParent()Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;
.end method

.method public abstract isDirectoryEntry()Z
.end method

.method public abstract isDocumentEntry()Z
.end method

.method public abstract renameTo(Ljava/lang/String;)Z
.end method

.class public Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;
.super Ljava/lang/Object;
.source "POIFSReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader$SampleListener;
    }
.end annotation


# instance fields
.field private registry:Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;

.field private registryClosed:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registry:Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registryClosed:Z

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static main([Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    array-length v0, p0

    .line 2
    if-nez v0, :cond_0

    .line 3
    .line 4
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 5
    .line 6
    const-string v1, "at least one argument required: input filename(s)"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 13
    .line 14
    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    array-length v1, p0

    .line 17
    if-ge v0, v1, :cond_1

    .line 18
    .line 19
    new-instance v1, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;

    .line 20
    .line 21
    invoke-direct {v1}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;-><init>()V

    .line 22
    .line 23
    .line 24
    new-instance v2, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader$SampleListener;

    .line 25
    .line 26
    invoke-direct {v2}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader$SampleListener;-><init>()V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registerListener(Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;)V

    .line 30
    .line 31
    .line 32
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 33
    .line 34
    new-instance v3, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v4, "reading "

    .line 40
    .line 41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    aget-object v4, p0, v0

    .line 45
    .line 46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    new-instance v2, Ljava/io/FileInputStream;

    .line 57
    .line 58
    aget-object v3, p0, v0

    .line 59
    .line 60
    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->read(Ljava/io/InputStream;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 67
    .line 68
    .line 69
    add-int/lit8 v0, v0, 0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_1
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private processProperties(Lcom/intsig/office/fc/poifs/storage/BlockList;Lcom/intsig/office/fc/poifs/storage/BlockList;Ljava/util/Iterator;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_5

    .line 6
    .line 7
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/fc/poifs/property/Property;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->isDirectory()Z

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    new-instance v2, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 24
    .line 25
    const/4 v3, 0x1

    .line 26
    new-array v3, v3, [Ljava/lang/String;

    .line 27
    .line 28
    const/4 v4, 0x0

    .line 29
    aput-object v1, v3, v4

    .line 30
    .line 31
    invoke-direct {v2, p4, v3}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;-><init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;[Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    check-cast v0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->getChildren()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-direct {p0, p1, p2, v0, v2}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->processProperties(Lcom/intsig/office/fc/poifs/storage/BlockList;Lcom/intsig/office/fc/poifs/storage/BlockList;Ljava/util/Iterator;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getStartBlock()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registry:Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;

    .line 49
    .line 50
    invoke-virtual {v3, p4, v1}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;->〇o00〇〇Oo(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;)Ljava/util/Iterator;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    const/4 v5, -0x1

    .line 59
    if-eqz v4, :cond_3

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getSize()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->shouldUseSmallBlocks()Z

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    if-eqz v0, :cond_2

    .line 70
    .line 71
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 72
    .line 73
    invoke-interface {p1, v2, v5}, Lcom/intsig/office/fc/poifs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-direct {v0, v1, v2, v4}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;I)V

    .line 78
    .line 79
    .line 80
    goto :goto_1

    .line 81
    :cond_2
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 82
    .line 83
    invoke-interface {p2, v2, v5}, Lcom/intsig/office/fc/poifs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-direct {v0, v1, v2, v4}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;I)V

    .line 88
    .line 89
    .line 90
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    if-eqz v2, :cond_0

    .line 95
    .line 96
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    check-cast v2, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;

    .line 101
    .line 102
    new-instance v4, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderEvent;

    .line 103
    .line 104
    new-instance v5, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 105
    .line 106
    invoke-direct {v5, v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)V

    .line 107
    .line 108
    .line 109
    invoke-direct {v4, v5, p4, v1}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderEvent;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;->processPOIFSReaderEvent(Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderEvent;)V

    .line 113
    .line 114
    .line 115
    goto :goto_1

    .line 116
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->shouldUseSmallBlocks()Z

    .line 117
    .line 118
    .line 119
    move-result v0

    .line 120
    if-eqz v0, :cond_4

    .line 121
    .line 122
    invoke-interface {p1, v2, v5}, Lcom/intsig/office/fc/poifs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;

    .line 123
    .line 124
    .line 125
    goto :goto_0

    .line 126
    :cond_4
    invoke-interface {p2, v2, v5}, Lcom/intsig/office/fc/poifs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;

    .line 127
    .line 128
    .line 129
    goto/16 :goto_0

    .line 130
    .line 131
    :cond_5
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registryClosed:Z

    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 5
    .line 6
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;-><init>(Ljava/io/InputStream;)V

    .line 7
    .line 8
    .line 9
    new-instance v8, Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-direct {v8, p1, v1}, Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;-><init>(Ljava/io/InputStream;Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableReader;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBATCount()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBATArray()[I

    .line 29
    .line 30
    .line 31
    move-result-object v4

    .line 32
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getXBATCount()I

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getXBATIndex()I

    .line 37
    .line 38
    .line 39
    move-result v6

    .line 40
    move-object v7, v8

    .line 41
    invoke-direct/range {v1 .. v7}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableReader;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;I[IIILcom/intsig/office/fc/poifs/storage/BlockList;)V

    .line 42
    .line 43
    .line 44
    new-instance p1, Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 45
    .line 46
    invoke-direct {p1, v0, v8}, Lcom/intsig/office/fc/poifs/property/PropertyTable;-><init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getRoot()Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getSBATStart()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    invoke-static {v1, v8, v2, v0}, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableReader;->getSmallDocumentBlocks(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;Lcom/intsig/office/fc/poifs/property/RootProperty;I)Lcom/intsig/office/fc/poifs/storage/BlockList;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getRoot()Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->getChildren()Ljava/util/Iterator;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 74
    .line 75
    invoke-direct {v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;-><init>()V

    .line 76
    .line 77
    .line 78
    invoke-direct {p0, v0, v8, p1, v1}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->processProperties(Lcom/intsig/office/fc/poifs/storage/BlockList;Lcom/intsig/office/fc/poifs/storage/BlockList;Ljava/util/Iterator;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;)V

    .line 79
    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public registerListener(Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    iget-boolean v0, p0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registryClosed:Z

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registry:Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;->〇o〇(Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;)V

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public registerListener(Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_2

    if-eqz p3, :cond_2

    .line 6
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 7
    iget-boolean v0, p0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registryClosed:Z

    if-nez v0, :cond_1

    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registry:Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;

    if-nez p2, :cond_0

    .line 9
    new-instance p2, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    invoke-direct {p2}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;-><init>()V

    .line 10
    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderRegistry;->O8(Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;)V

    return-void

    .line 11
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_2
    const/4 p1, 0x0

    .line 12
    throw p1
.end method

.method public registerListener(Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 5
    invoke-virtual {p0, p1, v0, p2}, Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReader;->registerListener(Lcom/intsig/office/fc/poifs/eventfilesystem/POIFSReaderListener;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;)V

    return-void
.end method

.class public Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;
.super Ljava/io/InputStream;
.source "DocumentInputStream.java"

# interfaces
.implements Lcom/intsig/office/fc/util/LittleEndianInput;


# static fields
.field protected static final EOF:I = -0x1

.field protected static final SIZE_INT:I = 0x4

.field protected static final SIZE_LONG:I = 0x8

.field protected static final SIZE_SHORT:I = 0x2


# instance fields
.field private delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 3
    instance-of v0, p1, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;

    if-eqz v0, :cond_3

    .line 4
    move-object v0, p1

    check-cast v0, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/poifs/filesystem/Entry;->getParent()Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;->getDocument()Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;)V

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getFileSystem()Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 9
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;)V

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    goto :goto_0

    .line 10
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getNFileSystem()Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 11
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/NDocumentInputStream;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/NDocumentInputStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;)V

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    :goto_0
    return-void

    .line 12
    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string v0, "No FileSystem bound on the parent, can\'t read contents"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 13
    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Cannot open internal document storage"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSDocument;)V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 17
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/NDocumentInputStream;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/NDocumentInputStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSDocument;)V

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 15
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)V

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    return-void
.end method


# virtual methods
.method public available()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->available()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public close()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->close()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mark(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->mark(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public markSupported()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->read()I

    move-result v0

    return v0
.end method

.method public read([B)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->read([BII)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->read([BII)I

    move-result p1

    return p1
.end method

.method public readByte()B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readByte()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readDouble()D
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readDouble()D

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    return-wide v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readFully([B)V
    .locals 2

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readFully([BII)V

    return-void
.end method

.method public readFully([BII)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readFully([BII)V

    return-void
.end method

.method public readInt()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readInt()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readLong()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readLong()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    return-wide v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readShort()S
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readUShort()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-short v0, v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readUByte()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readUByte()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readUShort()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readUShort()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public reset()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->reset()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public skip(J)J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->delegate:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->skip(J)J

    .line 4
    .line 5
    .line 6
    move-result-wide p1

    .line 7
    return-wide p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

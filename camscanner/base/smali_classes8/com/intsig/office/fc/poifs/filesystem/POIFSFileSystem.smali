.class public Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;
.super Ljava/lang/Object;
.source "POIFSFileSystem.java"


# static fields
.field private static final _logger:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field private _documents:Ljava/util/List;

.field private _property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

.field private _root:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

.field private bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/intsig/office/fc/poifs/common/POIFSConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 3
    new-instance v0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    invoke-direct {v0, v1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V

    .line 4
    new-instance v1, Lcom/intsig/office/fc/poifs/property/PropertyTable;

    invoke-direct {v1, v0}, Lcom/intsig/office/fc/poifs/property/PropertyTable;-><init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;)V

    iput-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_root:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;-><init>()V

    .line 8
    :try_start_0
    new-instance v0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;-><init>(Ljava/io/InputStream;)V

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 10
    new-instance v9, Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;

    invoke-direct {v9, p1, v1}, Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;-><init>(Ljava/io/InputStream;Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    .line 11
    invoke-direct {p0, p1, v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->closeInputStream(Ljava/io/InputStream;Z)V

    .line 12
    new-instance v2, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableReader;

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    move-result-object v3

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBATCount()I

    move-result v4

    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBATArray()[I

    move-result-object v5

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getXBATCount()I

    move-result v6

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getXBATIndex()I

    move-result v7

    move-object v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableReader;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;I[IIILcom/intsig/office/fc/poifs/storage/BlockList;)V

    .line 14
    new-instance p1, Lcom/intsig/office/fc/poifs/property/PropertyTable;

    invoke-direct {p1, v0, v9}, Lcom/intsig/office/fc/poifs/property/PropertyTable;-><init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;)V

    .line 15
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getRoot()Lcom/intsig/office/fc/poifs/property/RootProperty;

    move-result-object v2

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getSBATStart()I

    move-result v3

    .line 17
    invoke-static {v1, v9, v2, v3}, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableReader;->getSmallDocumentBlocks(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;Lcom/intsig/office/fc/poifs/property/RootProperty;I)Lcom/intsig/office/fc/poifs/storage/BlockList;

    move-result-object v3

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getRoot()Lcom/intsig/office/fc/poifs/property/RootProperty;

    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->getChildren()Ljava/util/Iterator;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getPropertyStart()I

    move-result v7

    move-object v2, p0

    move-object v4, v9

    .line 20
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->processProperties(Lcom/intsig/office/fc/poifs/storage/BlockList;Lcom/intsig/office/fc/poifs/storage/BlockList;Ljava/util/Iterator;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;I)V

    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getRoot()Lcom/intsig/office/fc/poifs/property/RootProperty;

    move-result-object p1

    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/Property;->getStorageClsid()Lcom/intsig/office/fc/hpsf/ClassID;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->setStorageClsid(Lcom/intsig/office/fc/hpsf/ClassID;)V

    return-void

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    .line 22
    invoke-direct {p0, p1, v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->closeInputStream(Ljava/io/InputStream;Z)V

    .line 23
    throw v0
.end method

.method private closeInputStream(Ljava/io/InputStream;Z)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    instance-of v0, p1, Ljava/io/ByteArrayInputStream;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "POIFS is closing the supplied input stream of type ("

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v1, ") which supports mark/reset.  This will be a problem for the caller if the stream will still be used.  If that is the case the caller should wrap the input stream to avoid this close logic.  This warning is only temporary and will not be present in future versions of POI."

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    sget-object v1, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 42
    .line 43
    sget v2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 44
    .line 45
    invoke-virtual {v1, v2, v0}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :catch_0
    move-exception p1

    .line 53
    if-nez p2, :cond_1

    .line 54
    .line 55
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 56
    .line 57
    .line 58
    :goto_0
    return-void

    .line 59
    :cond_1
    new-instance p2, Ljava/lang/RuntimeException;

    .line 60
    .line 61
    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 62
    .line 63
    .line 64
    throw p2
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static createNonClosingInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/util/CloseIgnoringInputStream;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/util/CloseIgnoringInputStream;-><init>(Ljava/io/InputStream;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static hasPOIFSHeader(Ljava/io/InputStream;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->mark(I)V

    .line 4
    .line 5
    .line 6
    new-array v0, v0, [B

    .line 7
    .line 8
    invoke-static {p0, v0}, Lcom/intsig/office/fc/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    .line 9
    .line 10
    .line 11
    new-instance v1, Lcom/intsig/office/fc/util/LongField;

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-direct {v1, v2, v0}, Lcom/intsig/office/fc/util/LongField;-><init>(I[B)V

    .line 15
    .line 16
    .line 17
    instance-of v3, p0, Ljava/io/PushbackInputStream;

    .line 18
    .line 19
    if-eqz v3, :cond_0

    .line 20
    .line 21
    check-cast p0, Ljava/io/PushbackInputStream;

    .line 22
    .line 23
    invoke-virtual {p0, v0}, Ljava/io/PushbackInputStream;->unread([B)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    .line 28
    .line 29
    .line 30
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/util/LongField;->get()J

    .line 31
    .line 32
    .line 33
    move-result-wide v0

    .line 34
    const-wide v3, -0x1ee54e5e1fee3030L    # -5.8639378995972355E159

    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    cmp-long p0, v0, v3

    .line 40
    .line 41
    if-nez p0, :cond_1

    .line 42
    .line 43
    const/4 v2, 0x1

    .line 44
    :cond_1
    return v2
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static main([Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    array-length v0, p0

    .line 2
    const/4 v1, 0x2

    .line 3
    const/4 v2, 0x1

    .line 4
    if-eq v0, v1, :cond_0

    .line 5
    .line 6
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    .line 7
    .line 8
    const-string v1, "two arguments required: input filename and output filename"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {v2}, Ljava/lang/System;->exit(I)V

    .line 14
    .line 15
    .line 16
    :cond_0
    new-instance v0, Ljava/io/FileInputStream;

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    aget-object v1, p0, v1

    .line 20
    .line 21
    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    new-instance v1, Ljava/io/FileOutputStream;

    .line 25
    .line 26
    aget-object p0, p0, v2

    .line 27
    .line 28
    invoke-direct {v1, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    new-instance p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 32
    .line 33
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->writeFilesystem(Ljava/io/OutputStream;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private processProperties(Lcom/intsig/office/fc/poifs/storage/BlockList;Lcom/intsig/office/fc/poifs/storage/BlockList;Ljava/util/Iterator;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Lcom/intsig/office/fc/poifs/property/Property;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    if-nez p4, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    goto :goto_1

    .line 24
    :cond_0
    move-object v2, p4

    .line 25
    :goto_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->isDirectory()Z

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    if-eqz v3, :cond_1

    .line 30
    .line 31
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDirectory(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    move-object v6, v1

    .line 36
    check-cast v6, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getStorageClsid()Lcom/intsig/office/fc/hpsf/ClassID;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v6, v1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->setStorageClsid(Lcom/intsig/office/fc/hpsf/ClassID;)V

    .line 43
    .line 44
    .line 45
    check-cast v0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->getChildren()Ljava/util/Iterator;

    .line 48
    .line 49
    .line 50
    move-result-object v5

    .line 51
    move-object v2, p0

    .line 52
    move-object v3, p1

    .line 53
    move-object v4, p2

    .line 54
    move v7, p5

    .line 55
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->processProperties(Lcom/intsig/office/fc/poifs/storage/BlockList;Lcom/intsig/office/fc/poifs/storage/BlockList;Ljava/util/Iterator;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;I)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getStartBlock()I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getSize()I

    .line 64
    .line 65
    .line 66
    move-result v4

    .line 67
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->shouldUseSmallBlocks()Z

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    if-eqz v0, :cond_2

    .line 72
    .line 73
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 74
    .line 75
    invoke-interface {p1, v3, p5}, Lcom/intsig/office/fc/poifs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;

    .line 76
    .line 77
    .line 78
    move-result-object v3

    .line 79
    invoke-direct {v0, v1, v3, v4}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;I)V

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_2
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 84
    .line 85
    invoke-interface {p2, v3, p5}, Lcom/intsig/office/fc/poifs/storage/BlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    invoke-direct {v0, v1, v3, v4}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;I)V

    .line 90
    .line 91
    .line 92
    :goto_2
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocument(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    .line 93
    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_3
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method


# virtual methods
.method addDirectory(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->addProperty(Lcom/intsig/office/fc/poifs/property/Property;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method addDocument(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;->getDocumentProperty()Lcom/intsig/office/fc/poifs/property/DocumentProperty;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->addProperty(Lcom/intsig/office/fc/poifs/property/Property;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public createDirectory(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDirectory(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public createDocument(Ljava/io/InputStream;Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    move-result-object p1

    return-object p1
.end method

.method public createDocument(Ljava/lang/String;ILcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocument(Ljava/lang/String;ILcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    move-result-object p1

    return-object p1
.end method

.method public createDocumentInputStream(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getBigBlockSize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBigBlockSizeDetails()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_root:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getRoot()Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-direct {v0, v1, p0, v2}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;-><init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_root:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_root:Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 20
    .line 21
    return-object v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShortDescription()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "POIFS FileSystem"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method remove(Lcom/intsig/office/fc/poifs/filesystem/EntryNode;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->removeProperty(Lcom/intsig/office/fc/poifs/property/Property;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->isDocumentEntry()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    .line 17
    .line 18
    check-cast p1, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;->getDocument()Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    :cond_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public writeFilesystem(Ljava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/PropertyTable;->preWrite()V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    .line 11
    .line 12
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 13
    .line 14
    invoke-virtual {v3}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getRoot()Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Ljava/util/List;Lcom/intsig/office/fc/poifs/property/RootProperty;)V

    .line 19
    .line 20
    .line 21
    new-instance v1, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 24
    .line 25
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V

    .line 26
    .line 27
    .line 28
    new-instance v2, Ljava/util/ArrayList;

    .line 29
    .line 30
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .line 32
    .line 33
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 36
    .line 37
    .line 38
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 39
    .line 40
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->getSBAT()Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-eqz v3, :cond_1

    .line 62
    .line 63
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    check-cast v3, Lcom/intsig/office/fc/poifs/filesystem/BATManaged;

    .line 68
    .line 69
    invoke-interface {v3}, Lcom/intsig/office/fc/poifs/filesystem/BATManaged;->countBlocks()I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    if-eqz v4, :cond_0

    .line 74
    .line 75
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->allocateSpace(I)I

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/poifs/filesystem/BATManaged;->setStartBlock(I)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->createBlocks()I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    new-instance v3, Lcom/intsig/office/fc/poifs/storage/HeaderBlockWriter;

    .line 88
    .line 89
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 90
    .line 91
    invoke-direct {v3, v4}, Lcom/intsig/office/fc/poifs/storage/HeaderBlockWriter;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->countBlocks()I

    .line 95
    .line 96
    .line 97
    move-result v4

    .line 98
    invoke-virtual {v3, v4, v2}, Lcom/intsig/office/fc/poifs/storage/HeaderBlockWriter;->setBATBlocks(II)[Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 103
    .line 104
    invoke-virtual {v4}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getStartBlock()I

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/poifs/storage/HeaderBlockWriter;->setPropertyStart(I)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->getSBAT()Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 112
    .line 113
    .line 114
    move-result-object v4

    .line 115
    invoke-virtual {v4}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->getStartBlock()I

    .line 116
    .line 117
    .line 118
    move-result v4

    .line 119
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/poifs/storage/HeaderBlockWriter;->setSBATStart(I)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->getSBATBlockCount()I

    .line 123
    .line 124
    .line 125
    move-result v4

    .line 126
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/poifs/storage/HeaderBlockWriter;->setSBATBlockCount(I)V

    .line 127
    .line 128
    .line 129
    new-instance v4, Ljava/util/ArrayList;

    .line 130
    .line 131
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .line 133
    .line 134
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_documents:Ljava/util/List;

    .line 138
    .line 139
    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 140
    .line 141
    .line 142
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->_property_table:Lcom/intsig/office/fc/poifs/property/PropertyTable;

    .line 143
    .line 144
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    .line 146
    .line 147
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->getSBAT()Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    .line 156
    .line 157
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    .line 159
    .line 160
    const/4 v0, 0x0

    .line 161
    :goto_1
    array-length v1, v2

    .line 162
    if-ge v0, v1, :cond_2

    .line 163
    .line 164
    aget-object v1, v2, v0

    .line 165
    .line 166
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    .line 168
    .line 169
    add-int/lit8 v0, v0, 0x1

    .line 170
    .line 171
    goto :goto_1

    .line 172
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 173
    .line 174
    .line 175
    move-result-object v0

    .line 176
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 177
    .line 178
    .line 179
    move-result v1

    .line 180
    if-eqz v1, :cond_3

    .line 181
    .line 182
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 183
    .line 184
    .line 185
    move-result-object v1

    .line 186
    check-cast v1, Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    .line 187
    .line 188
    invoke-interface {v1, p1}, Lcom/intsig/office/fc/poifs/storage/BlockWritable;->writeBlocks(Ljava/io/OutputStream;)V

    .line 189
    .line 190
    .line 191
    goto :goto_2

    .line 192
    :cond_3
    return-void
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

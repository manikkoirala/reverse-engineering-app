.class public Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;
.super Ljava/lang/Object;
.source "NPOIFSStream.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "StreamBlockByteBufferIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Ljava/nio/ByteBuffer;",
        ">;"
    }
.end annotation


# instance fields
.field private loopDetector:Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;

.field private nextBlock:I

.field final synthetic this$0:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->this$0:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->nextBlock:I

    .line 7
    .line 8
    :try_start_0
    invoke-static {p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->〇080(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;)Lcom/intsig/office/fc/poifs/filesystem/BlockStore;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/BlockStore;->getChainLoopDetector()Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->loopDetector:Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    .line 18
    return-void

    .line 19
    :catch_0
    move-exception p1

    .line 20
    new-instance p2, Ljava/lang/RuntimeException;

    .line 21
    .line 22
    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 23
    .line 24
    .line 25
    throw p2
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->nextBlock:I

    .line 2
    .line 3
    const/4 v1, -0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    return v0

    .line 8
    :cond_0
    const/4 v0, 0x1

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->next()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/nio/ByteBuffer;
    .locals 3

    .line 2
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->nextBlock:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    .line 3
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->loopDetector:Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;

    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;->claim(I)V

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->this$0:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

    invoke-static {v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->〇080(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;)Lcom/intsig/office/fc/poifs/filesystem/BlockStore;

    move-result-object v0

    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->nextBlock:I

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/poifs/filesystem/BlockStore;->getBlockAt(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->this$0:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

    invoke-static {v1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->〇080(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;)Lcom/intsig/office/fc/poifs/filesystem/BlockStore;

    move-result-object v1

    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->nextBlock:I

    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/poifs/filesystem/BlockStore;->getNextBlock(I)I

    move-result v1

    iput v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream$StreamBlockByteBufferIterator;->nextBlock:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 6
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "Can\'t read past the end of the stream"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public remove()V
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 4
    .line 5
    .line 6
    throw v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public abstract Lcom/intsig/office/fc/poifs/crypt/Decryptor;
.super Ljava/lang/Object;
.source "Decryptor.java"


# static fields
.field public static final DEFAULT_PASSWORD:Ljava/lang/String; = "VelvetSweatshop"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected static getBlockSize(I)I
    .locals 1

    .line 1
    packed-switch p0, :pswitch_data_0

    .line 2
    .line 3
    .line 4
    new-instance p0, Lcom/intsig/office/fc/EncryptedDocumentException;

    .line 5
    .line 6
    const-string v0, "Cannot process encrypted office files!"

    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    throw p0

    .line 12
    :pswitch_0
    const/16 p0, 0x20

    .line 13
    .line 14
    return p0

    .line 15
    :pswitch_1
    const/16 p0, 0x18

    .line 16
    .line 17
    return p0

    .line 18
    :pswitch_2
    const/16 p0, 0x10

    .line 19
    .line 20
    return p0

    .line 21
    :pswitch_data_0
    .packed-switch 0x660e
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static getInstance(Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;)Lcom/intsig/office/fc/poifs/crypt/Decryptor;
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getVersionMajor()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getVersionMinor()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x4

    .line 10
    if-ne v0, v2, :cond_0

    .line 11
    .line 12
    if-ne v1, v2, :cond_0

    .line 13
    .line 14
    new-instance v0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;

    .line 15
    .line 16
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;-><init>(Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;)V

    .line 17
    .line 18
    .line 19
    return-object v0

    .line 20
    :cond_0
    const/4 v3, 0x2

    .line 21
    if-ne v1, v3, :cond_2

    .line 22
    .line 23
    const/4 v1, 0x3

    .line 24
    if-eq v0, v1, :cond_1

    .line 25
    .line 26
    if-ne v0, v2, :cond_2

    .line 27
    .line 28
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/poifs/crypt/EcmaDecryptor;

    .line 29
    .line 30
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/poifs/crypt/EcmaDecryptor;-><init>(Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;)V

    .line 31
    .line 32
    .line 33
    return-object v0

    .line 34
    :cond_2
    new-instance p0, Lcom/intsig/office/fc/EncryptedDocumentException;

    .line 35
    .line 36
    const-string v0, "Cannot process encrypted office files!"

    .line 37
    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    throw p0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public abstract getDataStream(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation
.end method

.method public getDataStream(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;)Ljava/io/InputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/crypt/Decryptor;->getDataStream(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)Ljava/io/InputStream;

    move-result-object p1

    return-object p1
.end method

.method public getDataStream(Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;)Ljava/io/InputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->getRoot()Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/crypt/Decryptor;->getDataStream(Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)Ljava/io/InputStream;

    move-result-object p1

    return-object p1
.end method

.method protected hashPassword(Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;Ljava/lang/String;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .line 1
    const-string v0, "SHA-1"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :try_start_0
    const-string v1, "UTF-16LE"

    .line 8
    .line 9
    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    .line 10
    .line 11
    .line 12
    move-result-object p2
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getVerifier()Lcom/intsig/office/fc/poifs/crypt/EncryptionVerifier;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/crypt/EncryptionVerifier;->getSalt()[B

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, p2}, Ljava/security/MessageDigest;->digest([B)[B

    .line 25
    .line 26
    .line 27
    move-result-object p2

    .line 28
    const/4 v1, 0x4

    .line 29
    new-array v1, v1, [B

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getVerifier()Lcom/intsig/office/fc/poifs/crypt/EncryptionVerifier;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-virtual {v3}, Lcom/intsig/office/fc/poifs/crypt/EncryptionVerifier;->getSpinCount()I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    if-ge v2, v3, :cond_0

    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    .line 43
    .line 44
    .line 45
    invoke-static {v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, p2}, Ljava/security/MessageDigest;->digest([B)[B

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    add-int/lit8 v2, v2, 0x1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_0
    return-object p2

    .line 59
    :catch_0
    new-instance p1, Lcom/intsig/office/fc/EncryptedDocumentException;

    .line 60
    .line 61
    const-string p2, "Cannot process encrypted office files!"

    .line 62
    .line 63
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    throw p1
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public abstract verifyPassword(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation
.end method

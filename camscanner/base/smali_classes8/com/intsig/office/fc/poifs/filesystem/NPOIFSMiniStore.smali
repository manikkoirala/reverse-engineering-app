.class public Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;
.super Lcom/intsig/office/fc/poifs/filesystem/BlockStore;
.source "NPOIFSMiniStore.java"


# instance fields
.field private _filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

.field private _header:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

.field private _mini_stream:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

.field private _root:Lcom/intsig/office/fc/poifs/property/RootProperty;

.field private _sbat_blocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/poifs/storage/BATBlock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;Lcom/intsig/office/fc/poifs/property/RootProperty;Ljava/util/List;Lcom/intsig/office/fc/poifs/storage/HeaderBlock;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;",
            "Lcom/intsig/office/fc/poifs/property/RootProperty;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/poifs/storage/BATBlock;",
            ">;",
            "Lcom/intsig/office/fc/poifs/storage/HeaderBlock;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/BlockStore;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 5
    .line 6
    iput-object p3, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_sbat_blocks:Ljava/util/List;

    .line 7
    .line 8
    iput-object p4, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_header:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 9
    .line 10
    iput-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_root:Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 11
    .line 12
    new-instance p3, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

    .line 13
    .line 14
    invoke-virtual {p2}, Lcom/intsig/office/fc/poifs/property/Property;->getStartBlock()I

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    invoke-direct {p3, p1, p2}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/BlockStore;I)V

    .line 19
    .line 20
    .line 21
    iput-object p3, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_mini_stream:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method


# virtual methods
.method protected createBlockIfNeeded(I)Ljava/nio/ByteBuffer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->getBlockAt(I)Ljava/nio/ByteBuffer;

    .line 2
    .line 3
    .line 4
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    return-object p1

    .line 6
    :catch_0
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getFreeBlock()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 13
    .line 14
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->createBlockIfNeeded(I)Ljava/nio/ByteBuffer;

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getChainLoopDetector()Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_mini_stream:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->getStartBlock()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    :goto_0
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;->claim(I)V

    .line 30
    .line 31
    .line 32
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 33
    .line 34
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getNextBlock(I)I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    const/4 v4, -0x2

    .line 39
    if-ne v3, v4, :cond_0

    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 42
    .line 43
    invoke-virtual {v1, v2, v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->setNextBlock(II)V

    .line 44
    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 47
    .line 48
    invoke-virtual {v1, v0, v4}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->setNextBlock(II)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->createBlockIfNeeded(I)Ljava/nio/ByteBuffer;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    return-object p1

    .line 56
    :cond_0
    move v2, v3

    .line 57
    goto :goto_0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method protected getBATBlockAndIndex(I)Lcom/intsig/office/fc/poifs/storage/BATBlock$BATBlockAndIndex;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_header:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_sbat_blocks:Ljava/util/List;

    .line 4
    .line 5
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->getSBATBlockAndIndex(ILcom/intsig/office/fc/poifs/storage/HeaderBlock;Ljava/util/List;)Lcom/intsig/office/fc/poifs/storage/BATBlock$BATBlockAndIndex;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected getBlockAt(I)Ljava/nio/ByteBuffer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/16 v0, 0x40

    .line 2
    .line 3
    mul-int/lit8 p1, p1, 0x40

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getBigBlockSize()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    div-int v1, p1, v1

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getBigBlockSize()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    rem-int/2addr p1, v2

    .line 20
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_mini_stream:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

    .line 21
    .line 22
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->getBlockIterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    const/4 v3, 0x0

    .line 27
    :goto_0
    if-ge v3, v1, :cond_0

    .line 28
    .line 29
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    add-int/lit8 v3, v3, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Ljava/nio/ByteBuffer;

    .line 40
    .line 41
    if-eqz v2, :cond_1

    .line 42
    .line 43
    invoke-virtual {v2}, Ljava/nio/Buffer;->position()I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    add-int/2addr v1, p1

    .line 48
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 56
    .line 57
    .line 58
    return-object p1

    .line 59
    :cond_1
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    .line 60
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    .line 62
    .line 63
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    const-string v2, "Big block "

    .line 67
    .line 68
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    const-string v1, " outside stream"

    .line 75
    .line 76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-direct {p1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    throw p1
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method protected getBlockStoreBlockSize()I
    .locals 1

    .line 1
    const/16 v0, 0x40

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getChainLoopDetector()Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_root:Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/property/Property;->getSize()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    int-to-long v1, v1

    .line 10
    invoke-direct {v0, p0, v1, v2}, Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;-><init>(Lcom/intsig/office/fc/poifs/filesystem/BlockStore;J)V

    .line 11
    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected getFreeBlock()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getBigBlockSizeDetails()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBATEntriesPerBlock()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x0

    .line 13
    const/4 v3, 0x0

    .line 14
    :goto_0
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_sbat_blocks:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 17
    .line 18
    .line 19
    move-result v4

    .line 20
    if-ge v2, v4, :cond_2

    .line 21
    .line 22
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_sbat_blocks:Ljava/util/List;

    .line 23
    .line 24
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    check-cast v4, Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 29
    .line 30
    invoke-virtual {v4}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->hasFreeSectors()Z

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    if-eqz v5, :cond_1

    .line 35
    .line 36
    const/4 v5, 0x0

    .line 37
    :goto_1
    if-ge v5, v0, :cond_1

    .line 38
    .line 39
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->getValueAt(I)I

    .line 40
    .line 41
    .line 42
    move-result v6

    .line 43
    const/4 v7, -0x1

    .line 44
    if-ne v6, v7, :cond_0

    .line 45
    .line 46
    add-int/2addr v3, v5

    .line 47
    return v3

    .line 48
    :cond_0
    add-int/lit8 v5, v5, 0x1

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_1
    add-int/2addr v3, v0

    .line 52
    add-int/lit8 v2, v2, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getBigBlockSizeDetails()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-static {v0, v1}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->createEmptyBATBlock(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Z)Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getFreeBlock()I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->setOurBlockIndex(I)V

    .line 72
    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_header:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 75
    .line 76
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getSBATCount()I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    const/4 v4, -0x2

    .line 81
    const/4 v5, 0x1

    .line 82
    if-nez v2, :cond_3

    .line 83
    .line 84
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_header:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 85
    .line 86
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->setSBATStart(I)V

    .line 87
    .line 88
    .line 89
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_header:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 90
    .line 91
    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->setSBATBlockCount(I)V

    .line 92
    .line 93
    .line 94
    goto :goto_3

    .line 95
    :cond_3
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 96
    .line 97
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getChainLoopDetector()Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;

    .line 98
    .line 99
    .line 100
    move-result-object v2

    .line 101
    iget-object v6, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_header:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 102
    .line 103
    invoke-virtual {v6}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getSBATStart()I

    .line 104
    .line 105
    .line 106
    move-result v6

    .line 107
    :goto_2
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/poifs/filesystem/BlockStore$ChainLoopDetector;->claim(I)V

    .line 108
    .line 109
    .line 110
    iget-object v7, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 111
    .line 112
    invoke-virtual {v7, v6}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getNextBlock(I)I

    .line 113
    .line 114
    .line 115
    move-result v7

    .line 116
    if-ne v7, v4, :cond_4

    .line 117
    .line 118
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 119
    .line 120
    invoke-virtual {v2, v6, v1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->setNextBlock(II)V

    .line 121
    .line 122
    .line 123
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_header:Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 124
    .line 125
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getSBATCount()I

    .line 126
    .line 127
    .line 128
    move-result v6

    .line 129
    add-int/2addr v6, v5

    .line 130
    invoke-virtual {v2, v6}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->setSBATBlockCount(I)V

    .line 131
    .line 132
    .line 133
    :goto_3
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 134
    .line 135
    invoke-virtual {v2, v1, v4}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->setNextBlock(II)V

    .line 136
    .line 137
    .line 138
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_sbat_blocks:Ljava/util/List;

    .line 139
    .line 140
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    return v3

    .line 144
    :cond_4
    move v6, v7

    .line 145
    goto :goto_2
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected getNextBlock(I)I
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->getBATBlockAndIndex(I)Lcom/intsig/office/fc/poifs/storage/BATBlock$BATBlockAndIndex;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/BATBlock$BATBlockAndIndex;->getBlock()Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/BATBlock$BATBlockAndIndex;->getIndex()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->getValueAt(I)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected setNextBlock(II)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->getBATBlockAndIndex(I)Lcom/intsig/office/fc/poifs/storage/BATBlock$BATBlockAndIndex;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/BATBlock$BATBlockAndIndex;->getBlock()Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/BATBlock$BATBlockAndIndex;->getIndex()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->setValueAt(II)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method protected syncWithDataSource()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_sbat_blocks:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSMiniStore;->_filesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->getOurBlockIndex()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->getBlockAt(I)Ljava/nio/ByteBuffer;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-static {v1, v2}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->writeBlock(Lcom/intsig/office/fc/poifs/storage/BATBlock;Ljava/nio/ByteBuffer;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

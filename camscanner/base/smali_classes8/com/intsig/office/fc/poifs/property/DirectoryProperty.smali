.class public Lcom/intsig/office/fc/poifs/property/DirectoryProperty;
.super Lcom/intsig/office/fc/poifs/property/Property;
.source "DirectoryProperty.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/property/Parent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/poifs/property/DirectoryProperty$PropertyComparator;
    }
.end annotation


# instance fields
.field private _children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/poifs/property/Property;",
            ">;"
        }
    .end annotation
.end field

.field private _children_names:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(I[BI)V
    .locals 0

    .line 9
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/property/Property;-><init>(I[BI)V

    .line 10
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 11
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/property/Property;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setName(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setSize(I)V

    const/4 v0, 0x1

    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/property/Property;->setPropertyType(B)V

    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setStartBlock(I)V

    .line 8
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/property/Property;->setNodeColor(B)V

    return-void
.end method


# virtual methods
.method public addChild(Lcom/intsig/office/fc/poifs/property/Property;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 6
    .line 7
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 14
    .line 15
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 19
    .line 20
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    return-void

    .line 24
    :cond_0
    new-instance p1, Ljava/io/IOException;

    .line 25
    .line 26
    new-instance v1, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v2, "Duplicate name \""

    .line 32
    .line 33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v0, "\""

    .line 40
    .line 41
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    throw p1
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public changeName(Lcom/intsig/office/fc/poifs/property/Property;Ljava/lang/String;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/poifs/property/Property;->setName(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p2

    .line 12
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 13
    .line 14
    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/poifs/property/Property;->setName(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 p1, 0x0

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 26
    .line 27
    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 31
    .line 32
    invoke-interface {p1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    :goto_0
    return p1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public deleteChild(Lcom/intsig/office/fc/poifs/property/Property;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children_names:Ljava/util/Set;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    :cond_0
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getChildren()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/poifs/property/Property;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDirectory()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected preWrite()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_4

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->_children:Ljava/util/List;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    new-array v2, v1, [Lcom/intsig/office/fc/poifs/property/Property;

    .line 13
    .line 14
    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, [Lcom/intsig/office/fc/poifs/property/Property;

    .line 19
    .line 20
    new-instance v2, Lcom/intsig/office/fc/poifs/property/DirectoryProperty$PropertyComparator;

    .line 21
    .line 22
    invoke-direct {v2}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty$PropertyComparator;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-static {v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 26
    .line 27
    .line 28
    array-length v2, v0

    .line 29
    div-int/lit8 v2, v2, 0x2

    .line 30
    .line 31
    aget-object v3, v0, v2

    .line 32
    .line 33
    invoke-virtual {v3}, Lcom/intsig/office/fc/poifs/property/Property;->getIndex()I

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/poifs/property/Property;->setChildProperty(I)V

    .line 38
    .line 39
    .line 40
    aget-object v3, v0, v1

    .line 41
    .line 42
    const/4 v4, 0x0

    .line 43
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/poifs/property/Property;->setPreviousChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 44
    .line 45
    .line 46
    aget-object v1, v0, v1

    .line 47
    .line 48
    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/poifs/property/Property;->setNextChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 49
    .line 50
    .line 51
    const/4 v1, 0x1

    .line 52
    const/4 v3, 0x1

    .line 53
    :goto_0
    if-ge v3, v2, :cond_0

    .line 54
    .line 55
    aget-object v5, v0, v3

    .line 56
    .line 57
    add-int/lit8 v6, v3, -0x1

    .line 58
    .line 59
    aget-object v6, v0, v6

    .line 60
    .line 61
    invoke-virtual {v5, v6}, Lcom/intsig/office/fc/poifs/property/Property;->setPreviousChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 62
    .line 63
    .line 64
    aget-object v5, v0, v3

    .line 65
    .line 66
    invoke-virtual {v5, v4}, Lcom/intsig/office/fc/poifs/property/Property;->setNextChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 67
    .line 68
    .line 69
    add-int/lit8 v3, v3, 0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_0
    if-eqz v2, :cond_1

    .line 73
    .line 74
    aget-object v3, v0, v2

    .line 75
    .line 76
    add-int/lit8 v5, v2, -0x1

    .line 77
    .line 78
    aget-object v5, v0, v5

    .line 79
    .line 80
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/poifs/property/Property;->setPreviousChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 81
    .line 82
    .line 83
    :cond_1
    array-length v3, v0

    .line 84
    sub-int/2addr v3, v1

    .line 85
    if-eq v2, v3, :cond_3

    .line 86
    .line 87
    aget-object v3, v0, v2

    .line 88
    .line 89
    add-int/2addr v2, v1

    .line 90
    aget-object v5, v0, v2

    .line 91
    .line 92
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/poifs/property/Property;->setNextChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 93
    .line 94
    .line 95
    :goto_1
    array-length v3, v0

    .line 96
    sub-int/2addr v3, v1

    .line 97
    if-ge v2, v3, :cond_2

    .line 98
    .line 99
    aget-object v3, v0, v2

    .line 100
    .line 101
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/poifs/property/Property;->setPreviousChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 102
    .line 103
    .line 104
    aget-object v3, v0, v2

    .line 105
    .line 106
    add-int/lit8 v2, v2, 0x1

    .line 107
    .line 108
    aget-object v5, v0, v2

    .line 109
    .line 110
    invoke-virtual {v3, v5}, Lcom/intsig/office/fc/poifs/property/Property;->setNextChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 111
    .line 112
    .line 113
    goto :goto_1

    .line 114
    :cond_2
    array-length v2, v0

    .line 115
    sub-int/2addr v2, v1

    .line 116
    aget-object v2, v0, v2

    .line 117
    .line 118
    invoke-virtual {v2, v4}, Lcom/intsig/office/fc/poifs/property/Property;->setPreviousChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 119
    .line 120
    .line 121
    array-length v2, v0

    .line 122
    sub-int/2addr v2, v1

    .line 123
    aget-object v0, v0, v2

    .line 124
    .line 125
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/poifs/property/Property;->setNextChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 126
    .line 127
    .line 128
    goto :goto_2

    .line 129
    :cond_3
    aget-object v0, v0, v2

    .line 130
    .line 131
    invoke-virtual {v0, v4}, Lcom/intsig/office/fc/poifs/property/Property;->setNextChild(Lcom/intsig/office/fc/poifs/property/Child;)V

    .line 132
    .line 133
    .line 134
    :cond_4
    :goto_2
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.class public interface abstract Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;
.super Ljava/lang/Object;
.source "DirectoryEntry.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/filesystem/Entry;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/intsig/office/fc/poifs/filesystem/Entry;",
        "Ljava/lang/Iterable<",
        "Lcom/intsig/office/fc/poifs/filesystem/Entry;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract createDirectory(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract createDocument(Ljava/lang/String;ILcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getEntries()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract getEntryCount()I
.end method

.method public abstract getStorageClsid()Lcom/intsig/office/fc/hpsf/ClassID;
.end method

.method public abstract hasEntry(Ljava/lang/String;)Z
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract setStorageClsid(Lcom/intsig/office/fc/hpsf/ClassID;)V
.end method

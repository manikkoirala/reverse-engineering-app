.class public Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;
.super Lcom/intsig/office/fc/poifs/filesystem/EntryNode;
.source "DirectoryNode.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/office/fc/poifs/filesystem/EntryNode;",
        "Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;",
        "Ljava/lang/Iterable<",
        "Lcom/intsig/office/fc/poifs/filesystem/Entry;",
        ">;"
    }
.end annotation


# instance fields
.field private _byname:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/fc/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation
.end field

.field private _entries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation
.end field

.field private _nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

.field private _ofilesystem:Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

.field private _path:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;


# direct methods
.method private constructor <init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;)V
    .locals 2

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;-><init>(Lcom/intsig/office/fc/poifs/property/Property;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 4
    iput-object p3, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_ofilesystem:Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 5
    iput-object p4, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    if-nez p2, :cond_0

    .line 6
    new-instance p2, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    invoke-direct {p2}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;-><init>()V

    iput-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_path:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    goto :goto_0

    .line 7
    :cond_0
    new-instance p3, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    iget-object p2, p2, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_path:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    const/4 p4, 0x1

    new-array p4, p4, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p4, v0

    invoke-direct {p3, p2, p4}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;-><init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;[Ljava/lang/String;)V

    iput-object p3, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_path:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 9
    :goto_0
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 10
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->getChildren()Ljava/util/Iterator;

    move-result-object p1

    .line 12
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 13
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/intsig/office/fc/poifs/property/Property;

    .line 14
    invoke-virtual {p2}, Lcom/intsig/office/fc/poifs/property/Property;->isDirectory()Z

    move-result p3

    if-eqz p3, :cond_2

    .line 15
    check-cast p2, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    .line 16
    iget-object p3, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_ofilesystem:Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    if-eqz p3, :cond_1

    .line 17
    new-instance p4, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    invoke-direct {p4, p2, p3, p0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;-><init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    goto :goto_2

    .line 18
    :cond_1
    new-instance p4, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    iget-object p3, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    invoke-direct {p4, p2, p3, p0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;-><init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    goto :goto_2

    .line 19
    :cond_2
    new-instance p4, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;

    check-cast p2, Lcom/intsig/office/fc/poifs/property/DocumentProperty;

    invoke-direct {p4, p2, p0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;-><init>(Lcom/intsig/office/fc/poifs/property/DocumentProperty;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 20
    :goto_2
    iget-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    iget-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-interface {p4}, Lcom/intsig/office/fc/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p2, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-void
.end method

.method constructor <init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p3, v0, p2}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;-><init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;)V

    return-void
.end method

.method constructor <init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p3, p2, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;-><init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;)V

    return-void
.end method


# virtual methods
.method changeName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {v1, v2, p2}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->changeName(Lcom/intsig/office/fc/poifs/property/Property;Ljava/lang/String;)Z

    .line 22
    .line 23
    .line 24
    move-result p2

    .line 25
    if-eqz p2, :cond_1

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 28
    .line 29
    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    const/4 p2, 0x0

    .line 47
    :cond_1
    :goto_0
    return p2
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public createDirectory(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_ofilesystem:Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    new-instance v2, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 11
    .line 12
    invoke-direct {v2, v0, v1, p0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;-><init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 13
    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_ofilesystem:Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->addDirectory(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    new-instance v2, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 24
    .line 25
    invoke-direct {v2, v0, v1, p0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;-><init>(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 29
    .line 30
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->addDirectory(Lcom/intsig/office/fc/poifs/property/DirectoryProperty;)V

    .line 31
    .line 32
    .line 33
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    check-cast v1, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    .line 38
    .line 39
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->addChild(Lcom/intsig/office/fc/poifs/property/Property;)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    .line 43
    .line 44
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 48
    .line 49
    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    return-object v2
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method createDocument(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSDocument;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSDocument;->getDocumentProperty()Lcom/intsig/office/fc/poifs/property/DocumentProperty;

    move-result-object v0

    .line 8
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;

    invoke-direct {v1, v0, p0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;-><init>(Lcom/intsig/office/fc/poifs/property/DocumentProperty;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->addChild(Lcom/intsig/office/fc/poifs/property/Property;)V

    .line 10
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->addDocument(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSDocument;)V

    .line 11
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 12
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method

.method createDocument(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;->getDocumentProperty()Lcom/intsig/office/fc/poifs/property/DocumentProperty;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;

    invoke-direct {v1, v0, p0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;-><init>(Lcom/intsig/office/fc/poifs/property/DocumentProperty;Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;)V

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->addChild(Lcom/intsig/office/fc/poifs/property/Property;)V

    .line 4
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_ofilesystem:Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->addDocument(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)V

    .line 5
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method

.method public createDocument(Ljava/lang/String;ILcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 16
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_path:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;ILcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocument(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    move-result-object p1

    return-object p1
.end method

.method public createDocument(Ljava/lang/String;Ljava/io/InputStream;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    if-eqz v0, :cond_0

    .line 14
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSDocument;

    invoke-direct {v1, p1, v0, p2}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSDocument;-><init>(Ljava/lang/String;Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocument(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSDocument;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    move-result-object p1

    return-object p1

    .line 15
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocument(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    move-result-object p1

    return-object p1
.end method

.method public createDocumentInputStream(Lcom/intsig/office/fc/poifs/filesystem/Entry;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    invoke-interface {p1}, Lcom/intsig/office/fc/poifs/filesystem/Entry;->isDocumentEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    check-cast p1, Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;

    .line 4
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;)V

    return-object v0

    .line 5
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entry \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lcom/intsig/office/fc/poifs/filesystem/Entry;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' is not a DocumentEntry"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createDocumentInputStream(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->createDocumentInputStream(Lcom/intsig/office/fc/poifs/filesystem/Entry;)Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    move-result-object p1

    return-object p1
.end method

.method deleteEntry(Lcom/intsig/office/fc/poifs/filesystem/EntryNode;)Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/poifs/property/DirectoryProperty;->deleteChild(Lcom/intsig/office/fc/poifs/property/Property;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getName()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_ofilesystem:Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 32
    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;->remove(Lcom/intsig/office/fc/poifs/filesystem/EntryNode;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 40
    .line 41
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;->remove(Lcom/intsig/office/fc/poifs/filesystem/EntryNode;)V

    .line 42
    .line 43
    .line 44
    :cond_1
    :goto_0
    return v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getEntries()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 4
    .line 5
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/office/fc/poifs/filesystem/Entry;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    if-eqz v0, :cond_1

    .line 14
    .line 15
    return-object v0

    .line 16
    :cond_1
    new-instance v0, Ljava/io/FileNotFoundException;

    .line 17
    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v2, "no such entry: \""

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string p1, "\""

    .line 32
    .line 33
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-direct {v0, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    throw v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getEntryCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFileSystem()Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_ofilesystem:Lcom/intsig/office/fc/poifs/filesystem/POIFSFileSystem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNFileSystem()Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_nfilesystem:Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPath()Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_path:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShortDescription()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getName()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStorageClsid()Lcom/intsig/office/fc/hpsf/ClassID;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/property/Property;->getStorageClsid()Lcom/intsig/office/fc/hpsf/ClassID;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getViewableArray()[Ljava/lang/Object;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    return-object v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getViewableIterator()Ljava/util/Iterator;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public hasEntry(Ljava/lang/String;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_byname:Ljava/util/Map;

    .line 4
    .line 5
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    :goto_0
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected isDeleteOK()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->isEmpty()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDirectoryEntry()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isEmpty()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->_entries:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/poifs/filesystem/Entry;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryNode;->getEntries()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public preferArray()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setStorageClsid(Lcom/intsig/office/fc/hpsf/ClassID;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/EntryNode;->getProperty()Lcom/intsig/office/fc/poifs/property/Property;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setStorageClsid(Lcom/intsig/office/fc/hpsf/ClassID;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public final Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;
.super Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;
.source "ODocumentInputStream.java"


# instance fields
.field private _closed:Z

.field private _currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

.field private _current_offset:I

.field private _document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

.field private _document_size:I

.field private _marked_offset:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;-><init>()V

    .line 2
    instance-of v0, p1, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;

    const-string v1, "Cannot open internal document storage"

    if-eqz v0, :cond_1

    .line 3
    move-object v0, p1

    check-cast v0, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;

    .line 4
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;->getDocument()Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    .line 5
    iput v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 6
    iput v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_marked_offset:I

    .line 7
    invoke-interface {p1}, Lcom/intsig/office/fc/poifs/filesystem/DocumentEntry;->getSize()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 8
    iput-boolean v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_closed:Z

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentNode;->getDocument()Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 10
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    return-void

    .line 11
    :cond_0
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 12
    :cond_1
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;)V
    .locals 2

    .line 13
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;-><init>()V

    const/4 v0, 0x0

    .line 14
    iput v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 15
    iput v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_marked_offset:I

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;->getSize()I

    move-result v1

    iput v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 17
    iput-boolean v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_closed:Z

    .line 18
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 19
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    return-void
.end method

.method private atEOD()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private checkAvaliable(I)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_closed:Z

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 8
    .line 9
    sub-int/2addr v0, v1

    .line 10
    if-gt p1, v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    .line 14
    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "Buffer underrun - requested "

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string p1, " bytes but "

    .line 29
    .line 30
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    iget p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 36
    .line 37
    sub-int/2addr p1, v2

    .line 38
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string p1, " was available"

    .line 42
    .line 43
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    throw v0

    .line 54
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 55
    .line 56
    const-string v0, "cannot perform requested operation on a closed stream"

    .line 57
    .line 58
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    throw p1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private dieIfClosed()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_closed:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v0, Ljava/io/IOException;

    .line 7
    .line 8
    const-string v1, "cannot perform requested operation on a closed stream"

    .line 9
    .line 10
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    throw v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public available()I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_closed:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 8
    .line 9
    sub-int/2addr v0, v1

    .line 10
    return v0

    .line 11
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 12
    .line 13
    const-string v1, "cannot perform requested operation on a closed stream"

    .line 14
    .line 15
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    throw v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public close()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_closed:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mark(I)V
    .locals 0

    .line 1
    iget p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 2
    .line 3
    iput p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_marked_offset:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->dieIfClosed()V

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->atEOD()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readUByte()I

    move-result v0

    .line 4
    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->available()I

    move-result v1

    if-ge v1, v2, :cond_1

    .line 6
    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    invoke-direct {p0, v1}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    :cond_1
    return v0
.end method

.method public read([BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->dieIfClosed()V

    if-eqz p1, :cond_3

    if-ltz p2, :cond_2

    if-ltz p3, :cond_2

    .line 8
    array-length v0, p1

    add-int v1, p2, p3

    if-lt v0, v1, :cond_2

    if-nez p3, :cond_0

    const/4 p1, 0x0

    return p1

    .line 9
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->atEOD()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, -0x1

    return p1

    .line 10
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->available()I

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 11
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->readFully([BII)V

    return p3

    .line 12
    :cond_2
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    const-string p2, "can\'t read past buffer boundaries"

    invoke-direct {p1, p2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 13
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "buffer must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public readByte()B
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->readUByte()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-byte v0, v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readDouble()D
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->readLong()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    return-wide v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readFully([BII)V
    .locals 4

    .line 1
    invoke-direct {p0, p3}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->checkAvaliable(I)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->available()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-le v0, p3, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 13
    .line 14
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readFully([BII)V

    .line 15
    .line 16
    .line 17
    iget p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 18
    .line 19
    add-int/2addr p1, p3

    .line 20
    iput p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 21
    .line 22
    return-void

    .line 23
    :cond_0
    :goto_0
    if-lez p3, :cond_5

    .line 24
    .line 25
    if-lt p3, v0, :cond_1

    .line 26
    .line 27
    const/4 v1, 0x1

    .line 28
    goto :goto_1

    .line 29
    :cond_1
    const/4 v1, 0x0

    .line 30
    :goto_1
    if-eqz v1, :cond_2

    .line 31
    .line 32
    move v2, v0

    .line 33
    goto :goto_2

    .line 34
    :cond_2
    move v2, p3

    .line 35
    :goto_2
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 36
    .line 37
    invoke-virtual {v3, p1, p2, v2}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readFully([BII)V

    .line 38
    .line 39
    .line 40
    sub-int/2addr p3, v2

    .line 41
    add-int/2addr p2, v2

    .line 42
    iget v3, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 43
    .line 44
    add-int/2addr v3, v2

    .line 45
    iput v3, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 46
    .line 47
    if-eqz v1, :cond_0

    .line 48
    .line 49
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 50
    .line 51
    if-ne v3, v0, :cond_4

    .line 52
    .line 53
    if-gtz p3, :cond_3

    .line 54
    .line 55
    const/4 p1, 0x0

    .line 56
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 57
    .line 58
    goto :goto_3

    .line 59
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 60
    .line 61
    const-string p2, "reached end of document stream unexpectedly"

    .line 62
    .line 63
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    throw p1

    .line 67
    :cond_4
    invoke-direct {p0, v3}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 72
    .line 73
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->available()I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    goto :goto_0

    .line 78
    :cond_5
    :goto_3
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public readInt()I
    .locals 4

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->checkAvaliable(I)V

    .line 3
    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->available()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-le v1, v0, :cond_0

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readIntLE()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    goto :goto_1

    .line 20
    :cond_0
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 21
    .line 22
    add-int/2addr v2, v1

    .line 23
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    if-ne v1, v0, :cond_1

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readIntLE()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 37
    .line 38
    invoke-virtual {v2, v3, v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readIntLE(Lcom/intsig/office/fc/poifs/storage/DataInputBlock;I)I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    :goto_0
    iput-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 43
    .line 44
    :goto_1
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 45
    .line 46
    add-int/2addr v2, v0

    .line 47
    iput v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 48
    .line 49
    return v1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public readLong()J
    .locals 5

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->checkAvaliable(I)V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->available()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-le v1, v0, :cond_0

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 15
    .line 16
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readLongLE()J

    .line 17
    .line 18
    .line 19
    move-result-wide v1

    .line 20
    goto :goto_1

    .line 21
    :cond_0
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 22
    .line 23
    add-int/2addr v2, v1

    .line 24
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    if-ne v1, v0, :cond_1

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 31
    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readLongLE()J

    .line 33
    .line 34
    .line 35
    move-result-wide v3

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 38
    .line 39
    invoke-virtual {v2, v3, v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readLongLE(Lcom/intsig/office/fc/poifs/storage/DataInputBlock;I)J

    .line 40
    .line 41
    .line 42
    move-result-wide v3

    .line 43
    :goto_0
    iput-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 44
    .line 45
    move-wide v1, v3

    .line 46
    :goto_1
    iget v3, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 47
    .line 48
    add-int/2addr v3, v0

    .line 49
    iput v3, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 50
    .line 51
    return-wide v1
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public readShort()S
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->readUShort()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-short v0, v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public readUByte()I
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->checkAvaliable(I)V

    .line 3
    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readUByte()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 12
    .line 13
    add-int/2addr v2, v0

    .line 14
    iput v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 17
    .line 18
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->available()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-ge v2, v0, :cond_0

    .line 23
    .line 24
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 25
    .line 26
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 31
    .line 32
    :cond_0
    return v1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public readUShort()I
    .locals 3

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->checkAvaliable(I)V

    .line 3
    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->available()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-le v1, v0, :cond_0

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readUShortLE()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    goto :goto_1

    .line 20
    :cond_0
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 21
    .line 22
    add-int/2addr v2, v1

    .line 23
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    if-ne v1, v0, :cond_1

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readUShortLE()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 37
    .line 38
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;->readUShortLE(Lcom/intsig/office/fc/poifs/storage/DataInputBlock;)I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    :goto_0
    iput-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 43
    .line 44
    :goto_1
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 45
    .line 46
    add-int/2addr v2, v0

    .line 47
    iput v2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 48
    .line 49
    return v1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public reset()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_marked_offset:I

    .line 2
    .line 3
    iput v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 4
    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public skip(J)J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->dieIfClosed()V

    .line 2
    .line 3
    .line 4
    const-wide/16 v0, 0x0

    .line 5
    .line 6
    cmp-long v2, p1, v0

    .line 7
    .line 8
    if-gez v2, :cond_0

    .line 9
    .line 10
    return-wide v0

    .line 11
    :cond_0
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 12
    .line 13
    long-to-int p2, p1

    .line 14
    add-int/2addr p2, v0

    .line 15
    if-ge p2, v0, :cond_1

    .line 16
    .line 17
    iget p2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_1
    iget p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_document_size:I

    .line 21
    .line 22
    if-le p2, p1, :cond_2

    .line 23
    .line 24
    move p2, p1

    .line 25
    :cond_2
    :goto_0
    sub-int p1, p2, v0

    .line 26
    .line 27
    int-to-long v0, p1

    .line 28
    iput p2, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_current_offset:I

    .line 29
    .line 30
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->getDataInputBlock(I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/ODocumentInputStream;->_currentBlock:Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 35
    .line 36
    return-wide v0
    .line 37
    .line 38
.end method

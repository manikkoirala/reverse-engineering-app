.class public final Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;
.super Ljava/lang/Object;
.source "BlockAllocationTableWriter.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/storage/BlockWritable;
.implements Lcom/intsig/office/fc/poifs/filesystem/BATManaged;


# instance fields
.field private _bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

.field private _blocks:[Lcom/intsig/office/fc/poifs/storage/BATBlock;

.field private _entries:Lcom/intsig/office/fc/util/IntList;

.field private _start_block:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 5
    .line 6
    const/4 p1, -0x2

    .line 7
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_start_block:I

    .line 8
    .line 9
    new-instance p1, Lcom/intsig/office/fc/util/IntList;

    .line 10
    .line 11
    invoke-direct {p1}, Lcom/intsig/office/fc/util/IntList;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_entries:Lcom/intsig/office/fc/util/IntList;

    .line 15
    .line 16
    const/4 p1, 0x0

    .line 17
    new-array p1, p1, [Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static writeBlock(Lcom/intsig/office/fc/poifs/storage/BATBlock;Ljava/nio/ByteBuffer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->writeData(Ljava/nio/ByteBuffer;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public allocateSpace(I)I
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_entries:Lcom/intsig/office/fc/util/IntList;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez p1, :cond_1

    .line 8
    .line 9
    add-int/lit8 p1, p1, -0x1

    .line 10
    .line 11
    add-int/lit8 v1, v0, 0x1

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    :goto_0
    if-ge v2, p1, :cond_0

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_entries:Lcom/intsig/office/fc/util/IntList;

    .line 17
    .line 18
    add-int/lit8 v4, v1, 0x1

    .line 19
    .line 20
    invoke-virtual {v3, v1}, Lcom/intsig/office/fc/util/IntList;->add(I)Z

    .line 21
    .line 22
    .line 23
    add-int/lit8 v2, v2, 0x1

    .line 24
    .line 25
    move v1, v4

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_entries:Lcom/intsig/office/fc/util/IntList;

    .line 28
    .line 29
    const/4 v1, -0x2

    .line 30
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/util/IntList;->add(I)Z

    .line 31
    .line 32
    .line 33
    :cond_1
    return v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public countBlocks()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public createBlocks()I
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 4
    .line 5
    add-int v3, v0, v1

    .line 6
    .line 7
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_entries:Lcom/intsig/office/fc/util/IntList;

    .line 8
    .line 9
    invoke-virtual {v4}, Lcom/intsig/office/fc/util/IntList;->size()I

    .line 10
    .line 11
    .line 12
    move-result v4

    .line 13
    add-int/2addr v3, v4

    .line 14
    invoke-static {v2, v3}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->calculateStorageRequirements(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;I)I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 19
    .line 20
    invoke-static {v3, v2}, Lcom/intsig/office/fc/poifs/storage/HeaderBlockWriter;->calculateXBATStorageRequirements(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;I)I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    if-ne v0, v2, :cond_0

    .line 25
    .line 26
    if-ne v1, v3, :cond_0

    .line 27
    .line 28
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->allocateSpace(I)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->allocateSpace(I)I

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->simpleCreateBlocks()V

    .line 36
    .line 37
    .line 38
    return v0

    .line 39
    :cond_0
    move v0, v2

    .line 40
    move v1, v3

    .line 41
    goto :goto_0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getStartBlock()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_start_block:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setStartBlock(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_start_block:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method simpleCreateBlocks()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_entries:Lcom/intsig/office/fc/util/IntList;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/util/IntList;->toArray()[I

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->createBATBlocks(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[I)[Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public writeBlocks(Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BATBlock;

    .line 3
    .line 4
    array-length v2, v1

    .line 5
    if-ge v0, v2, :cond_0

    .line 6
    .line 7
    aget-object v1, v1, v0

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/poifs/storage/BATBlock;->writeBlocks(Ljava/io/OutputStream;)V

    .line 10
    .line 11
    .line 12
    add-int/lit8 v0, v0, 0x1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

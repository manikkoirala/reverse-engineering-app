.class public interface abstract Lcom/intsig/office/fc/poifs/common/POIFSConstants;
.super Ljava/lang/Object;
.source "POIFSConstants.java"


# static fields
.field public static final BIG_BLOCK_MINIMUM_DOCUMENT_SIZE:I = 0x1000

.field public static final DIFAT_SECTOR_BLOCK:I = -0x4

.field public static final END_OF_CHAIN:I = -0x2

.field public static final FAT_SECTOR_BLOCK:I = -0x3

.field public static final LARGER_BIG_BLOCK_SIZE:I = 0x1000

.field public static final LARGER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

.field public static final LARGEST_REGULAR_SECTOR_NUMBER:I = -0x5

.field public static final OOXML_FILE_HEADER:[B

.field public static final PROPERTY_SIZE:I = 0x80

.field public static final SMALLER_BIG_BLOCK_SIZE:I = 0x200

.field public static final SMALLER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

.field public static final SMALL_BLOCK_SIZE:I = 0x40

.field public static final UNUSED_BLOCK:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 2
    .line 3
    const/16 v1, 0x200

    .line 4
    .line 5
    const/16 v2, 0x9

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;-><init>(IS)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/fc/poifs/common/POIFSConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 13
    .line 14
    const/16 v1, 0x1000

    .line 15
    .line 16
    const/16 v2, 0xc

    .line 17
    .line 18
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;-><init>(IS)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/fc/poifs/common/POIFSConstants;->LARGER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 22
    .line 23
    const/4 v0, 0x4

    .line 24
    new-array v0, v0, [B

    .line 25
    .line 26
    fill-array-data v0, :array_0

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/office/fc/poifs/common/POIFSConstants;->OOXML_FILE_HEADER:[B

    .line 30
    .line 31
    return-void

    .line 32
    nop

    .line 33
    :array_0
    .array-data 1
        0x50t
        0x4bt
        0x3t
        0x4t
    .end array-data
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

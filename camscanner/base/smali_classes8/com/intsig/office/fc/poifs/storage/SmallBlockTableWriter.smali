.class public Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;
.super Ljava/lang/Object;
.source "SmallBlockTableWriter.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/storage/BlockWritable;
.implements Lcom/intsig/office/fc/poifs/filesystem/BATManaged;


# instance fields
.field private _big_block_count:I

.field private _root:Lcom/intsig/office/fc/poifs/property/RootProperty;

.field private _sbat:Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

.field private _small_blocks:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Ljava/util/List;Lcom/intsig/office/fc/poifs/property/RootProperty;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 5
    .line 6
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_sbat:Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_small_blocks:Ljava/util/List;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_root:Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 19
    .line 20
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    if-eqz p3, :cond_2

    .line 29
    .line 30
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object p3

    .line 34
    check-cast p3, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;

    .line 35
    .line 36
    invoke-virtual {p3}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;->getSmallBlocks()[Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    array-length v1, v0

    .line 41
    if-eqz v1, :cond_1

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_sbat:Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 44
    .line 45
    array-length v2, v0

    .line 46
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->allocateSpace(I)I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    invoke-virtual {p3, v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;->setStartBlock(I)V

    .line 51
    .line 52
    .line 53
    const/4 p3, 0x0

    .line 54
    :goto_1
    array-length v1, v0

    .line 55
    if-ge p3, v1, :cond_0

    .line 56
    .line 57
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_small_blocks:Ljava/util/List;

    .line 58
    .line 59
    aget-object v2, v0, p3

    .line 60
    .line 61
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    add-int/lit8 p3, p3, 0x1

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_1
    const/4 v0, -0x2

    .line 68
    invoke-virtual {p3, v0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;->setStartBlock(I)V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_2
    iget-object p2, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_sbat:Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 73
    .line 74
    invoke-virtual {p2}, Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;->simpleCreateBlocks()V

    .line 75
    .line 76
    .line 77
    iget-object p2, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_root:Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 78
    .line 79
    iget-object p3, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_small_blocks:Ljava/util/List;

    .line 80
    .line 81
    invoke-interface {p3}, Ljava/util/List;->size()I

    .line 82
    .line 83
    .line 84
    move-result p3

    .line 85
    invoke-virtual {p2, p3}, Lcom/intsig/office/fc/poifs/property/RootProperty;->setSize(I)V

    .line 86
    .line 87
    .line 88
    iget-object p2, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_small_blocks:Ljava/util/List;

    .line 89
    .line 90
    invoke-static {p1, p2}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->fill(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Ljava/util/List;)I

    .line 91
    .line 92
    .line 93
    move-result p1

    .line 94
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_big_block_count:I

    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public countBlocks()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_big_block_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSBAT()Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_sbat:Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableWriter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSBATBlockCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_big_block_count:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0xf

    .line 4
    .line 5
    div-int/lit8 v0, v0, 0x10

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setStartBlock(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_root:Lcom/intsig/office/fc/poifs/property/RootProperty;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/poifs/property/Property;->setStartBlock(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public writeBlocks(Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallBlockTableWriter;->_small_blocks:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    .line 18
    .line 19
    invoke-interface {v1, p1}, Lcom/intsig/office/fc/poifs/storage/BlockWritable;->writeBlocks(Ljava/io/OutputStream;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

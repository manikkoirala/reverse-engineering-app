.class final Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;
.super Ljava/lang/Object;
.source "POIFSDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BigBlockStore"
.end annotation


# instance fields
.field private final O8:I

.field private final Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

.field private final o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

.field private 〇080:[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

.field private final 〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

.field private final 〇o〇:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;ILcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    const/4 p1, 0x0

    new-array p1, p1, [Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    .line 10
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    .line 11
    iput-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 12
    iput-object p3, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o〇:Ljava/lang/String;

    .line 13
    iput p4, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 14
    iput-object p5, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    return-void
.end method

.method constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 3
    iput-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o〇:Ljava/lang/String;

    const/4 p2, -0x1

    .line 6
    iput p2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    return-void
.end method


# virtual methods
.method O8(Ljava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 14
    .line 15
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 19
    .line 20
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o〇:Ljava/lang/String;

    .line 25
    .line 26
    iget v4, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 27
    .line 28
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;I)V

    .line 29
    .line 30
    .line 31
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;->processPOIFSWriterEvent(Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇080()I

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 39
    .line 40
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    mul-int p1, p1, v1

    .line 45
    .line 46
    invoke-static {}, Lcom/intsig/office/fc/poifs/storage/DocumentBlock;->getFillByte()B

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;->writeFiller(IB)V

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_0
    const/4 v0, 0x0

    .line 55
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    .line 56
    .line 57
    array-length v2, v1

    .line 58
    if-ge v0, v2, :cond_1

    .line 59
    .line 60
    aget-object v1, v1, v0

    .line 61
    .line 62
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/poifs/storage/DocumentBlock;->writeBlocks(Ljava/io/OutputStream;)V

    .line 63
    .line 64
    .line 65
    add-int/lit8 v0, v0, 0x1

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    :goto_1
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method 〇080()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    .line 12
    .line 13
    array-length v0, v0

    .line 14
    return v0

    .line 15
    :cond_0
    iget v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    add-int/2addr v0, v1

    .line 24
    add-int/lit8 v0, v0, -0x1

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    div-int/2addr v0, v1

    .line 33
    return v0

    .line 34
    :cond_1
    const/4 v0, 0x0

    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method 〇o00〇〇Oo()[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 14
    .line 15
    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;

    .line 19
    .line 20
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 21
    .line 22
    invoke-direct {v1, v0, v2}, Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 23
    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 26
    .line 27
    new-instance v3, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;

    .line 28
    .line 29
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 30
    .line 31
    iget-object v5, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇o〇:Ljava/lang/String;

    .line 32
    .line 33
    iget v6, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 34
    .line 35
    invoke-direct {v3, v1, v4, v5, v6}, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;I)V

    .line 36
    .line 37
    .line 38
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;->processPOIFSWriterEvent(Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;)V

    .line 39
    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 42
    .line 43
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->O8:I

    .line 48
    .line 49
    invoke-static {v1, v0, v2}, Lcom/intsig/office/fc/poifs/storage/DocumentBlock;->convert(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[BI)[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    .line 54
    .line 55
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    .line 56
    .line 57
    return-object v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method 〇o〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/DocumentBlock;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    if-gtz v0, :cond_1

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$BigBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    goto :goto_1

    .line 13
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 14
    :goto_1
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

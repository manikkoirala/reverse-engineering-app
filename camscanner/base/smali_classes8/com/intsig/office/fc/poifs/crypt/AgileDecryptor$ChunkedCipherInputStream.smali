.class Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;
.super Ljava/io/InputStream;
.source "AgileDecryptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChunkedCipherInputStream"
.end annotation


# instance fields
.field private O8o08O8O:Ljavax/crypto/Cipher;

.field private final OO:J

.field private o0:I

.field private o〇00O:[B

.field final synthetic 〇080OO8〇0:Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;

.field private final 〇08O〇00〇o:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

.field private 〇OOo8〇0:J


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇080OO8〇0:Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->o0:I

    .line 8
    .line 9
    const-wide/16 v0, 0x0

    .line 10
    .line 11
    iput-wide v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    .line 12
    .line 13
    iput-wide p3, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->OO:J

    .line 14
    .line 15
    iput-object p2, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇08O〇00〇o:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 16
    .line 17
    invoke-static {p1}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->〇080(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;)Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    invoke-virtual {p2}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getHeader()Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    invoke-virtual {p2}, Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;->getAlgorithm()I

    .line 26
    .line 27
    .line 28
    move-result p2

    .line 29
    invoke-static {p1}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->〇080(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;)Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;

    .line 30
    .line 31
    .line 32
    move-result-object p3

    .line 33
    invoke-virtual {p3}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getHeader()Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;

    .line 34
    .line 35
    .line 36
    move-result-object p3

    .line 37
    invoke-virtual {p3}, Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;->getCipherMode()I

    .line 38
    .line 39
    .line 40
    move-result p3

    .line 41
    invoke-static {p1}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->〇o00〇〇Oo(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;)Ljavax/crypto/SecretKey;

    .line 42
    .line 43
    .line 44
    move-result-object p4

    .line 45
    invoke-static {p1}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->〇080(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;)Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getHeader()Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;->getKeySalt()[B

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-static {p1, p2, p3, p4, v0}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->〇o〇(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;IILjavax/crypto/SecretKey;[B)Ljavax/crypto/Cipher;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->O8o08O8O:Ljavax/crypto/Cipher;

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private Oo08()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    .line 2
    .line 3
    const/16 v2, 0xc

    .line 4
    .line 5
    shr-long/2addr v0, v2

    .line 6
    long-to-int v1, v0

    .line 7
    const/4 v0, 0x4

    .line 8
    new-array v0, v0, [B

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 11
    .line 12
    .line 13
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇080OO8〇0:Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;

    .line 14
    .line 15
    invoke-static {v3}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->〇080(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;)Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;

    .line 16
    .line 17
    .line 18
    move-result-object v4

    .line 19
    invoke-virtual {v4}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getHeader()Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;

    .line 20
    .line 21
    .line 22
    move-result-object v4

    .line 23
    invoke-virtual {v4}, Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;->getAlgorithm()I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    iget-object v5, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇080OO8〇0:Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;

    .line 28
    .line 29
    invoke-static {v5}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->〇080(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;)Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;

    .line 30
    .line 31
    .line 32
    move-result-object v5

    .line 33
    invoke-virtual {v5}, Lcom/intsig/office/fc/poifs/crypt/EncryptionInfo;->getHeader()Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    invoke-virtual {v5}, Lcom/intsig/office/fc/poifs/crypt/EncryptionHeader;->getKeySalt()[B

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    invoke-virtual {v3, v4, v5, v0}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->generateIv(I[B[B)[B

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->O8o08O8O:Ljavax/crypto/Cipher;

    .line 46
    .line 47
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇080OO8〇0:Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;

    .line 48
    .line 49
    invoke-static {v4}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;->〇o00〇〇Oo(Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor;)Ljavax/crypto/SecretKey;

    .line 50
    .line 51
    .line 52
    move-result-object v4

    .line 53
    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    .line 54
    .line 55
    invoke-direct {v5, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 56
    .line 57
    .line 58
    const/4 v0, 0x2

    .line 59
    invoke-virtual {v3, v0, v4, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 60
    .line 61
    .line 62
    iget v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->o0:I

    .line 63
    .line 64
    if-eq v0, v1, :cond_0

    .line 65
    .line 66
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇08O〇00〇o:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 67
    .line 68
    sub-int v0, v1, v0

    .line 69
    .line 70
    shl-int/2addr v0, v2

    .line 71
    int-to-long v4, v0

    .line 72
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->skip(J)J

    .line 73
    .line 74
    .line 75
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇08O〇00〇o:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->available()I

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    const/16 v2, 0x1000

    .line 82
    .line 83
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    .line 84
    .line 85
    .line 86
    move-result v0

    .line 87
    new-array v0, v0, [B

    .line 88
    .line 89
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇08O〇00〇o:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 90
    .line 91
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->readFully([B)V

    .line 92
    .line 93
    .line 94
    add-int/lit8 v1, v1, 0x1

    .line 95
    .line 96
    iput v1, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->o0:I

    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->O8o08O8O:Ljavax/crypto/Cipher;

    .line 99
    .line 100
    invoke-virtual {v1, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    return-object v0
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->OO:J

    .line 2
    .line 3
    iget-wide v2, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    .line 4
    .line 5
    sub-long/2addr v0, v2

    .line 6
    long-to-int v1, v0

    .line 7
    return v1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇08O〇00〇o:Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/DocumentInputStream;->close()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public markSupported()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [B

    .line 1
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->read([B)I

    move-result v2

    if-ne v2, v0, :cond_0

    const/4 v0, 0x0

    aget-byte v0, v1, v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public read([B)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->read([BII)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    if-lez p3, :cond_2

    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->o〇00O:[B

    if-nez v1, :cond_0

    .line 4
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->Oo08()[B

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->o〇00O:[B
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 5
    :catch_0
    new-instance p1, Lcom/intsig/office/fc/EncryptedDocumentException;

    const-string p2, "Cannot process encrypted office files!"

    invoke-direct {p1, p2}, Lcom/intsig/office/fc/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6
    :cond_0
    :goto_1
    iget-wide v1, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    const-wide/16 v3, 0xfff

    and-long/2addr v1, v3

    const-wide/16 v5, 0x1000

    sub-long/2addr v5, v1

    long-to-int v1, v5

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->available()I

    move-result v2

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 8
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->o〇00O:[B

    iget-wide v5, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    and-long/2addr v5, v3

    long-to-int v6, v5

    invoke-static {v2, v6, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v1

    sub-int/2addr p3, v1

    .line 9
    iget-wide v5, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    int-to-long v7, v1

    add-long/2addr v5, v7

    iput-wide v5, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    and-long v2, v5, v3

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    const/4 v2, 0x0

    .line 10
    iput-object v2, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->o〇00O:[B

    :cond_1
    add-int/2addr v0, v1

    goto :goto_0

    :cond_2
    return v0
.end method

.method public skip(J)J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->available()I

    .line 4
    .line 5
    .line 6
    move-result v2

    .line 7
    int-to-long v2, v2

    .line 8
    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->min(JJ)J

    .line 9
    .line 10
    .line 11
    move-result-wide p1

    .line 12
    iget-wide v2, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    .line 13
    .line 14
    add-long v4, v2, p1

    .line 15
    .line 16
    xor-long/2addr v0, v4

    .line 17
    const-wide/16 v4, -0x1000

    .line 18
    .line 19
    and-long/2addr v0, v4

    .line 20
    const-wide/16 v4, 0x0

    .line 21
    .line 22
    cmp-long v6, v0, v4

    .line 23
    .line 24
    if-eqz v6, :cond_0

    .line 25
    .line 26
    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->o〇00O:[B

    .line 28
    .line 29
    :cond_0
    add-long/2addr v2, p1

    .line 30
    iput-wide v2, p0, Lcom/intsig/office/fc/poifs/crypt/AgileDecryptor$ChunkedCipherInputStream;->〇OOo8〇0:J

    .line 31
    .line 32
    return-wide p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

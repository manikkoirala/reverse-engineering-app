.class public interface abstract Lcom/intsig/office/fc/poifs/storage/BlockList;
.super Ljava/lang/Object;
.source "BlockList.java"


# virtual methods
.method public abstract blockCount()I
.end method

.method public abstract fetchBlocks(II)[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract remove(I)Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setBAT(Lcom/intsig/office/fc/poifs/storage/BlockAllocationTableReader;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract zap(I)V
.end method

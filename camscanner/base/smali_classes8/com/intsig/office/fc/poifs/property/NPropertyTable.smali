.class public final Lcom/intsig/office/fc/poifs/property/NPropertyTable;
.super Lcom/intsig/office/fc/poifs/property/PropertyTableBase;
.source "NPropertyTable.java"


# instance fields
.field private _bigBigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;-><init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;)V

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/NPropertyTable;->_bigBigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;Lcom/intsig/office/fc/poifs/filesystem/NPOIFSFileSystem;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3
    new-instance v0, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getPropertyStart()I

    move-result v1

    invoke-direct {v0, p2, v1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;-><init>(Lcom/intsig/office/fc/poifs/filesystem/BlockStore;I)V

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    move-result-object v0

    .line 6
    invoke-static {p2, v0}, Lcom/intsig/office/fc/poifs/property/NPropertyTable;->buildProperties(Ljava/util/Iterator;Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)Ljava/util/List;

    move-result-object p2

    .line 7
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;-><init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;Ljava/util/List;)V

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/NPropertyTable;->_bigBigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    return-void
.end method

.method private static buildProperties(Ljava/util/Iterator;Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator<",
            "Ljava/nio/ByteBuffer;",
            ">;",
            "Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/poifs/property/Property;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    check-cast v1, Ljava/nio/ByteBuffer;

    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasArray()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_0

    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    array-length v2, v2

    .line 35
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-ne v2, v3, :cond_0

    .line 40
    .line 41
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    goto :goto_1

    .line 46
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    new-array v3, v2, [B

    .line 51
    .line 52
    const/4 v4, 0x0

    .line 53
    invoke-virtual {v1, v3, v4, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 54
    .line 55
    .line 56
    move-object v1, v3

    .line 57
    :goto_1
    invoke-static {v1, v0}, Lcom/intsig/office/fc/poifs/property/PropertyFactory;->〇o00〇〇Oo([BLjava/util/List;)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public countBlocks()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    mul-int/lit16 v0, v0, 0x80

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/property/NPropertyTable;->_bigBigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    div-int/2addr v0, v1

    .line 16
    int-to-double v0, v0

    .line 17
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    .line 18
    .line 19
    .line 20
    move-result-wide v0

    .line 21
    double-to-int v0, v0

    .line 22
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public write(Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_1

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/office/fc/poifs/property/Property;

    .line 23
    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/poifs/property/Property;->writeData(Ljava/io/OutputStream;)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->updateContents([B)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->getStartBlock()I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->getStartBlock()I

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-eq v0, v1, :cond_2

    .line 46
    .line 47
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/filesystem/NPOIFSStream;->getStartBlock()I

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->setStartBlock(I)V

    .line 52
    .line 53
    .line 54
    :cond_2
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

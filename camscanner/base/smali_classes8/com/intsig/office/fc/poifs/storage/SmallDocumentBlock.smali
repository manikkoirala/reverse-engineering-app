.class public final Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;
.super Ljava/lang/Object;
.source "SmallDocumentBlock.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/storage/BlockWritable;
.implements Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;


# static fields
.field private static final BLOCK_MASK:I = 0x3f

.field private static final BLOCK_SHIFT:I = 0x6

.field private static final _block_size:I = 0x40

.field private static final _default_fill:B = -0x1t


# instance fields
.field private final _bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

.field private final _blocks_per_big_block:I

.field private _data:[B


# direct methods
.method private constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 5
    invoke-static {p1}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->getBlocksPerBigBlock(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_blocks_per_big_block:I

    const/16 p1, 0x40

    new-array p1, p1, [B

    .line 6
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    return-void
.end method

.method private constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[BI)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V

    const/16 p1, 0x40

    mul-int/lit8 p3, p3, 0x40

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    const/4 v1, 0x0

    invoke-static {p2, p3, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public static calcSize(I)I
    .locals 0

    .line 1
    mul-int/lit8 p0, p0, 0x40

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static convert(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[BI)[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;
    .locals 8

    const/16 v0, 0x40

    add-int/2addr p2, v0

    add-int/lit8 p2, p2, -0x1

    .line 1
    div-int/2addr p2, v0

    new-array v1, p2, [Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v3, p2, :cond_2

    .line 2
    new-instance v5, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    invoke-direct {v5, p0}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V

    aput-object v5, v1, v3

    .line 3
    array-length v6, p1

    const/4 v7, -0x1

    if-ge v4, v6, :cond_0

    .line 4
    array-length v5, p1

    sub-int/2addr v5, v4

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 5
    aget-object v6, v1, v3

    iget-object v6, v6, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    invoke-static {p1, v4, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    if-eq v5, v0, :cond_1

    .line 6
    aget-object v6, v1, v3

    iget-object v6, v6, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    invoke-static {v6, v5, v0, v7}, Ljava/util/Arrays;->fill([BIIB)V

    goto :goto_1

    .line 7
    :cond_0
    iget-object v5, v5, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    invoke-static {v5, v7}, Ljava/util/Arrays;->fill([BB)V

    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x40

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public static convert(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[Lcom/intsig/office/fc/poifs/storage/BlockWritable;I)[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    .line 8
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 9
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 10
    aget-object v3, p1, v2

    invoke-interface {v3, v0}, Lcom/intsig/office/fc/poifs/storage/BlockWritable;->writeBlocks(Ljava/io/OutputStream;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    .line 12
    invoke-static {p2}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->convertToBlockCount(I)I

    move-result p2

    new-array v0, p2, [Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    :goto_1
    if-ge v1, p2, :cond_1

    .line 13
    new-instance v2, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    invoke-direct {v2, p0, p1, v1}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[BI)V

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method private static convertToBlockCount(I)I
    .locals 0

    .line 1
    add-int/lit8 p0, p0, 0x40

    .line 2
    .line 3
    add-int/lit8 p0, p0, -0x1

    .line 4
    .line 5
    div-int/lit8 p0, p0, 0x40

    .line 6
    .line 7
    return p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static extract(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->getBlocksPerBigBlock(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    const/4 v3, 0x0

    .line 12
    :goto_0
    array-length v4, p1

    .line 13
    if-ge v3, v4, :cond_1

    .line 14
    .line 15
    aget-object v4, p1, v3

    .line 16
    .line 17
    invoke-interface {v4}, Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;->getData()[B

    .line 18
    .line 19
    .line 20
    move-result-object v4

    .line 21
    const/4 v5, 0x0

    .line 22
    :goto_1
    if-ge v5, v0, :cond_0

    .line 23
    .line 24
    new-instance v6, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 25
    .line 26
    invoke-direct {v6, p0, v4, v5}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[BI)V

    .line 27
    .line 28
    .line 29
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    add-int/lit8 v5, v5, 0x1

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    return-object v1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static fill(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Ljava/util/List;)I
    .locals 4

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->getBlocksPerBigBlock(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int v2, v1, v0

    .line 10
    .line 11
    add-int/lit8 v2, v2, -0x1

    .line 12
    .line 13
    div-int/2addr v2, v0

    .line 14
    mul-int v0, v0, v2

    .line 15
    .line 16
    :goto_0
    if-ge v1, v0, :cond_0

    .line 17
    .line 18
    invoke-static {p0}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->makeEmptySmallDocumentBlock(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    return v2
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static getBlocksPerBigBlock(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    div-int/lit8 p0, p0, 0x40

    .line 6
    .line 7
    return p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static getDataInputBlock([Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;I)Lcom/intsig/office/fc/poifs/storage/DataInputBlock;
    .locals 2

    .line 1
    shr-int/lit8 v0, p1, 0x6

    .line 2
    .line 3
    and-int/lit8 p1, p1, 0x3f

    .line 4
    .line 5
    new-instance v1, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;

    .line 6
    .line 7
    aget-object p0, p0, v0

    .line 8
    .line 9
    iget-object p0, p0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    .line 10
    .line 11
    invoke-direct {v1, p0, p1}, Lcom/intsig/office/fc/poifs/storage/DataInputBlock;-><init>([BI)V

    .line 12
    .line 13
    .line 14
    return-object v1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static makeEmptySmallDocumentBlock(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;-><init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, v0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    .line 7
    .line 8
    const/4 v1, -0x1

    .line 9
    invoke-static {p0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getData()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public writeBlocks(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->_data:[B

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public final Lcom/intsig/office/fc/poifs/property/PropertyTable;
.super Lcom/intsig/office/fc/poifs/property/PropertyTableBase;
.source "PropertyTable.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/storage/BlockWritable;


# instance fields
.field private _bigBigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

.field private _blocks:[Lcom/intsig/office/fc/poifs/storage/BlockWritable;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;-><init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;)V

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_bigBigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    const/4 p1, 0x0

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getPropertyStart()I

    move-result v0

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Lcom/intsig/office/fc/poifs/storage/RawDataBlockList;->fetchBlocks(II)[Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;

    move-result-object p2

    .line 5
    invoke-static {p2}, Lcom/intsig/office/fc/poifs/property/PropertyFactory;->〇080([Lcom/intsig/office/fc/poifs/storage/ListManagedBlock;)Ljava/util/List;

    move-result-object p2

    .line 6
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;-><init>(Lcom/intsig/office/fc/poifs/storage/HeaderBlock;Ljava/util/List;)V

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_bigBigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    return-void
.end method


# virtual methods
.method public countBlocks()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    array-length v0, v0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public preWrite()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    new-array v1, v1, [Lcom/intsig/office/fc/poifs/property/Property;

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, [Lcom/intsig/office/fc/poifs/property/Property;

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    const/4 v2, 0x0

    .line 17
    :goto_0
    array-length v3, v0

    .line 18
    if-ge v2, v3, :cond_0

    .line 19
    .line 20
    aget-object v3, v0, v2

    .line 21
    .line 22
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/poifs/property/Property;->setIndex(I)V

    .line 23
    .line 24
    .line 25
    add-int/lit8 v2, v2, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_bigBigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 29
    .line 30
    iget-object v3, p0, Lcom/intsig/office/fc/poifs/property/PropertyTableBase;->_properties:Ljava/util/List;

    .line 31
    .line 32
    invoke-static {v2, v3}, Lcom/intsig/office/fc/poifs/storage/PropertyBlock;->createPropertyBlockArray(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Ljava/util/List;)[Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    iput-object v2, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    .line 37
    .line 38
    :goto_1
    array-length v2, v0

    .line 39
    if-ge v1, v2, :cond_1

    .line 40
    .line 41
    aget-object v2, v0, v1

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/office/fc/poifs/property/Property;->preWrite()V

    .line 44
    .line 45
    .line 46
    add-int/lit8 v1, v1, 0x1

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public writeBlocks(Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/property/PropertyTable;->_blocks:[Lcom/intsig/office/fc/poifs/storage/BlockWritable;

    .line 7
    .line 8
    array-length v2, v1

    .line 9
    if-ge v0, v2, :cond_0

    .line 10
    .line 11
    aget-object v1, v1, v0

    .line 12
    .line 13
    invoke-interface {v1, p1}, Lcom/intsig/office/fc/poifs/storage/BlockWritable;->writeBlocks(Ljava/io/OutputStream;)V

    .line 14
    .line 15
    .line 16
    add-int/lit8 v0, v0, 0x1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

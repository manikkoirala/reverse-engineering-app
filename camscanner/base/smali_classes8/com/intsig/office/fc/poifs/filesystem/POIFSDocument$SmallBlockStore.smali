.class final Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;
.super Ljava/lang/Object;
.source "POIFSDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SmallBlockStore"
.end annotation


# instance fields
.field private final O8:I

.field private final Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

.field private final o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

.field private 〇080:[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

.field private final 〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

.field private final 〇o〇:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;ILcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    const/4 p1, 0x0

    new-array p1, p1, [Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 10
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 11
    iput-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 12
    iput-object p3, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇o〇:Ljava/lang/String;

    .line 13
    iput p4, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->O8:I

    .line 14
    iput-object p5, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    return-void
.end method

.method constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 3
    iput-object p2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇o〇:Ljava/lang/String;

    const/4 p2, -0x1

    .line 6
    iput p2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->O8:I

    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    return-void
.end method


# virtual methods
.method 〇080()[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇o00〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->O8:I

    .line 14
    .line 15
    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;

    .line 19
    .line 20
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->O8:I

    .line 21
    .line 22
    invoke-direct {v1, v0, v2}, Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 23
    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 26
    .line 27
    new-instance v3, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;

    .line 28
    .line 29
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇o00〇〇Oo:Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;

    .line 30
    .line 31
    iget-object v5, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇o〇:Ljava/lang/String;

    .line 32
    .line 33
    iget v6, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->O8:I

    .line 34
    .line 35
    invoke-direct {v3, v1, v4, v5, v6}, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;-><init>(Lcom/intsig/office/fc/poifs/filesystem/DocumentOutputStream;Lcom/intsig/office/fc/poifs/filesystem/POIFSDocumentPath;Ljava/lang/String;I)V

    .line 36
    .line 37
    .line 38
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;->processPOIFSWriterEvent(Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterEvent;)V

    .line 39
    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->o〇0:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 42
    .line 43
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iget v2, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->O8:I

    .line 48
    .line 49
    invoke-static {v1, v0, v2}, Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;->convert(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;[BI)[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 54
    .line 55
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 56
    .line 57
    return-object v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method 〇o00〇〇Oo()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->〇080:[Lcom/intsig/office/fc/poifs/storage/SmallDocumentBlock;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    if-gtz v0, :cond_1

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/filesystem/POIFSDocument$SmallBlockStore;->Oo08:Lcom/intsig/office/fc/poifs/filesystem/POIFSWriterListener;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    goto :goto_1

    .line 13
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 14
    :goto_1
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

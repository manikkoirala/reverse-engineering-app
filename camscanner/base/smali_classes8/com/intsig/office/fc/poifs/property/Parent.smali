.class public interface abstract Lcom/intsig/office/fc/poifs/property/Parent;
.super Ljava/lang/Object;
.source "Parent.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/property/Child;


# virtual methods
.method public abstract addChild(Lcom/intsig/office/fc/poifs/property/Property;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getChildren()Ljava/util/Iterator;
.end method

.method public abstract setNextChild(Lcom/intsig/office/fc/poifs/property/Child;)V
.end method

.method public abstract setPreviousChild(Lcom/intsig/office/fc/poifs/property/Child;)V
.end method

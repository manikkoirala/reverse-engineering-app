.class public final Lcom/intsig/office/fc/poifs/storage/HeaderBlock;
.super Ljava/lang/Object;
.source "HeaderBlock.java"

# interfaces
.implements Lcom/intsig/office/fc/poifs/storage/HeaderBlockConstants;


# static fields
.field private static final _default_value:B = -0x1t

.field private static final _logger:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field private _bat_count:I

.field private final _data:[B

.field private _property_start:I

.field private _sbat_count:I

.field private _sbat_start:I

.field private _xbat_count:I

.field private _xbat_start:I

.field private final bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;)V
    .locals 5

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    const/16 v0, 0x200

    new-array v0, v0, [B

    .line 24
    iput-object v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    const/4 v1, -0x1

    .line 25
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 26
    new-instance v1, Lcom/intsig/office/fc/util/LongField;

    const-wide v2, -0x1ee54e5e1fee3030L    # -5.8639378995972355E159

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3, v0}, Lcom/intsig/office/fc/util/LongField;-><init>(IJ[B)V

    .line 27
    new-instance v1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v2, 0x8

    invoke-direct {v1, v2, v4, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 28
    new-instance v1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v2, 0xc

    invoke-direct {v1, v2, v4, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 29
    new-instance v1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v2, 0x10

    invoke-direct {v1, v2, v4, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 30
    new-instance v1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v2, 0x14

    invoke-direct {v1, v2, v4, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 31
    new-instance v1, Lcom/intsig/office/fc/util/ShortField;

    const/16 v2, 0x18

    const/16 v3, 0x3b

    invoke-direct {v1, v2, v3, v0}, Lcom/intsig/office/fc/util/ShortField;-><init>(IS[B)V

    .line 32
    new-instance v1, Lcom/intsig/office/fc/util/ShortField;

    const/16 v2, 0x1a

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3, v0}, Lcom/intsig/office/fc/util/ShortField;-><init>(IS[B)V

    .line 33
    new-instance v1, Lcom/intsig/office/fc/util/ShortField;

    const/16 v2, 0x1c

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3, v0}, Lcom/intsig/office/fc/util/ShortField;-><init>(IS[B)V

    .line 34
    new-instance v1, Lcom/intsig/office/fc/util/ShortField;

    const/16 v2, 0x1e

    invoke-virtual {p1}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getHeaderValue()S

    move-result p1

    invoke-direct {v1, v2, p1, v0}, Lcom/intsig/office/fc/util/ShortField;-><init>(IS[B)V

    .line 35
    new-instance p1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x20

    const/4 v2, 0x6

    invoke-direct {p1, v1, v2, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 36
    new-instance p1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x24

    invoke-direct {p1, v1, v4, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 37
    new-instance p1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x28

    invoke-direct {p1, v1, v4, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 38
    new-instance p1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x34

    invoke-direct {p1, v1, v4, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 39
    new-instance p1, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x38

    const/16 v2, 0x1000

    invoke-direct {p1, v1, v2, v0}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 40
    iput v4, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_bat_count:I

    .line 41
    iput v4, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_count:I

    .line 42
    iput v4, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_count:I

    .line 43
    iput v3, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_property_start:I

    .line 44
    iput v3, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_start:I

    .line 45
    iput v3, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_start:I

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->readFirst512(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;-><init>([B)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v0

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    move-result v0

    sub-int/2addr v0, v1

    .line 4
    new-array v0, v0, [B

    .line 5
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x200

    .line 6
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/IOUtils;->toByteArray(Ljava/nio/ByteBuffer;I)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;-><init>([B)V

    return-void
.end method

.method private constructor <init>([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    const/4 v0, 0x0

    .line 9
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getLong([BI)J

    move-result-wide v0

    const-wide v2, -0x1ee54e5e1fee3030L    # -5.8639378995972355E159

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    const/16 v0, 0x1e

    .line 10
    aget-byte v1, p1, v0

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 11
    sget-object v0, Lcom/intsig/office/fc/poifs/common/POIFSConstants;->LARGER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    goto :goto_0

    :cond_0
    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    .line 12
    sget-object v0, Lcom/intsig/office/fc/poifs/common/POIFSConstants;->SMALLER_BIG_BLOCK_SIZE_DETAILS:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    iput-object v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 13
    :goto_0
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x2c

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/util/IntegerField;-><init>(I[B)V

    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntegerField;->get()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_bat_count:I

    .line 14
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x30

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/util/IntegerField;-><init>(I[B)V

    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntegerField;->get()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_property_start:I

    .line 15
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x3c

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/util/IntegerField;-><init>(I[B)V

    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntegerField;->get()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_start:I

    .line 16
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x40

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/util/IntegerField;-><init>(I[B)V

    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntegerField;->get()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_count:I

    .line 17
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x44

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/util/IntegerField;-><init>(I[B)V

    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntegerField;->get()I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_start:I

    .line 18
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    const/16 v1, 0x48

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/util/IntegerField;-><init>(I[B)V

    invoke-virtual {v0}, Lcom/intsig/office/fc/util/IntegerField;->get()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_count:I

    return-void

    .line 19
    :cond_1
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported blocksize  (2^"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-byte p1, p1, v0

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "). Expected 2^9 or 2^12."

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 20
    :cond_2
    new-instance p1, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid header signature; read "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0, v1}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->longToHex(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", expected "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    invoke-static {v2, v3}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->longToHex(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static alertShortRead(II)Ljava/io/IOException;
    .locals 4

    .line 1
    if-gez p0, :cond_0

    .line 2
    .line 3
    const/4 p0, 0x0

    .line 4
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, " byte"

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    if-ne p0, v1, :cond_1

    .line 16
    .line 17
    const-string v1, ""

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_1
    const-string v1, "s"

    .line 21
    .line 22
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    new-instance v1, Ljava/io/IOException;

    .line 30
    .line 31
    new-instance v2, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v3, "Unable to read entire header; "

    .line 37
    .line 38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string p0, " read; expected "

    .line 48
    .line 49
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    const-string p0, " bytes"

    .line 56
    .line 57
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p0

    .line 64
    invoke-direct {v1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    return-object v1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static longToHex(J)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/HexDump;->longToHex(J)[C

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([C)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static readFirst512(Ljava/io/InputStream;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/16 v0, 0x200

    .line 2
    .line 3
    new-array v1, v0, [B

    .line 4
    .line 5
    invoke-static {p0, v1}, Lcom/intsig/office/fc/util/IOUtils;->readFully(Ljava/io/InputStream;[B)I

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    if-ne p0, v0, :cond_0

    .line 10
    .line 11
    return-object v1

    .line 12
    :cond_0
    invoke-static {p0, v0}, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->alertShortRead(II)Ljava/io/IOException;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    throw p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public getBATArray()[I
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_bat_count:I

    .line 2
    .line 3
    const/16 v1, 0x6d

    .line 4
    .line 5
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    new-array v1, v0, [I

    .line 10
    .line 11
    const/16 v2, 0x4c

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    :goto_0
    if-ge v3, v0, :cond_0

    .line 15
    .line 16
    iget-object v4, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 17
    .line 18
    invoke-static {v4, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    aput v4, v1, v3

    .line 23
    .line 24
    add-int/lit8 v2, v2, 0x4

    .line 25
    .line 26
    add-int/lit8 v3, v3, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    return-object v1
    .line 30
.end method

.method public getBATCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_bat_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBigBlockSize()Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPropertyStart()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_property_start:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSBATCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSBATStart()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_start:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXBATCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXBATIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_start:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setBATArray([I)V
    .locals 7

    .line 1
    array-length v0, p1

    .line 2
    const/16 v1, 0x6d

    .line 3
    .line 4
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    rsub-int/lit8 v1, v0, 0x6d

    .line 9
    .line 10
    const/16 v2, 0x4c

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    const/4 v4, 0x0

    .line 14
    :goto_0
    if-ge v4, v0, :cond_0

    .line 15
    .line 16
    iget-object v5, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 17
    .line 18
    aget v6, p1, v4

    .line 19
    .line 20
    invoke-static {v5, v2, v6}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 21
    .line 22
    .line 23
    add-int/lit8 v2, v2, 0x4

    .line 24
    .line 25
    add-int/lit8 v4, v4, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    :goto_1
    if-ge v3, v1, :cond_1

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 31
    .line 32
    const/4 v0, -0x1

    .line 33
    invoke-static {p1, v2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 34
    .line 35
    .line 36
    add-int/lit8 v2, v2, 0x4

    .line 37
    .line 38
    add-int/lit8 v3, v3, 0x1

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setBATCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_bat_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPropertyStart(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_property_start:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSBATBlockCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSBATStart(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_start:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXBATCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXBATStart(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_start:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method writeData(Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_bat_count:I

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 6
    .line 7
    const/16 v3, 0x2c

    .line 8
    .line 9
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 10
    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    .line 13
    .line 14
    iget v1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_property_start:I

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 17
    .line 18
    const/16 v3, 0x30

    .line 19
    .line 20
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 21
    .line 22
    .line 23
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    .line 24
    .line 25
    iget v1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_start:I

    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 28
    .line 29
    const/16 v3, 0x3c

    .line 30
    .line 31
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 32
    .line 33
    .line 34
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    .line 35
    .line 36
    iget v1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_sbat_count:I

    .line 37
    .line 38
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 39
    .line 40
    const/16 v3, 0x40

    .line 41
    .line 42
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 43
    .line 44
    .line 45
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    .line 46
    .line 47
    iget v1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_start:I

    .line 48
    .line 49
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 50
    .line 51
    const/16 v3, 0x44

    .line 52
    .line 53
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 54
    .line 55
    .line 56
    new-instance v0, Lcom/intsig/office/fc/util/IntegerField;

    .line 57
    .line 58
    iget v1, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_xbat_count:I

    .line 59
    .line 60
    iget-object v2, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 61
    .line 62
    const/16 v3, 0x48

    .line 63
    .line 64
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/office/fc/util/IntegerField;-><init>(II[B)V

    .line 65
    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->_data:[B

    .line 68
    .line 69
    const/4 v1, 0x0

    .line 70
    const/16 v2, 0x200

    .line 71
    .line 72
    invoke-virtual {p1, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 73
    .line 74
    .line 75
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/poifs/storage/HeaderBlock;->bigBlockSize:Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/office/fc/poifs/common/POIFSBigBlockSize;->getBigBlockSize()I

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    if-ge v2, v0, :cond_0

    .line 82
    .line 83
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 84
    .line 85
    .line 86
    add-int/lit8 v2, v2, 0x1

    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_0
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

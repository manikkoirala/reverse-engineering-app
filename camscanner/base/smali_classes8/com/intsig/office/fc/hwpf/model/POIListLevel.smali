.class public final Lcom/intsig/office/fc/hwpf/model/POIListLevel;
.super Ljava/lang/Object;
.source "POIListLevel.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final RGBXCH_NUMS_SIZE:I = 0x9

.field private static _fLegal:Lcom/intsig/office/fc/util/BitField;

.field private static _fNoRestart:Lcom/intsig/office/fc/util/BitField;

.field private static _fPrev:Lcom/intsig/office/fc/util/BitField;

.field private static _fPrevSpace:Lcom/intsig/office/fc/util/BitField;

.field private static _fWord6:Lcom/intsig/office/fc/util/BitField;

.field private static _jc:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field private _cbGrpprlChpx:I

.field private _cbGrpprlPapx:I

.field private _dxaIndent:I

.field private _dxaSpace:I

.field private _grpprlChpx:[B

.field private _grpprlPapx:[B

.field private _iStartAt:I

.field private _info:B

.field private _ixchFollow:B

.field private _nfc:B

.field private _numberText:[C

.field private _reserved:S

.field private _rgbxchNums:[B

.field private _speecialIndent:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_jc:Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(III[B[BLjava/lang/String;)V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 40
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 41
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_iStartAt:I

    int-to-byte p1, p2

    .line 42
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_nfc:B

    .line 43
    sget-object p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_jc:Lcom/intsig/office/fc/util/BitField;

    iget-byte p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_info:B

    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 44
    iput-object p4, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlChpx:[B

    .line 45
    iput-object p5, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    .line 46
    invoke-virtual {p6}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 3

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 32
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_iStartAt:I

    const/4 v1, 0x0

    new-array v2, v1, [B

    .line 33
    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    new-array v2, v1, [B

    .line 34
    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlChpx:[B

    new-array v2, v1, [C

    .line 35
    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    const/16 v2, 0x9

    new-array v2, v2, [B

    .line 36
    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_rgbxchNums:[B

    if-eqz p2, :cond_0

    aput-byte v0, v2, v1

    const/4 p2, 0x2

    new-array p2, p2, [C

    int-to-char p1, p1

    aput-char p1, p2, v1

    const/16 p1, 0x2e

    aput-char p1, p2, v0

    .line 37
    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    goto :goto_0

    :cond_0
    new-array p1, v0, [C

    const/16 p2, 0x2022

    aput-char p2, p1, v1

    .line 38
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    :goto_0
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 3
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_iStartAt:I

    add-int/lit8 p2, p2, 0x4

    add-int/lit8 v0, p2, 0x1

    .line 4
    aget-byte p2, p1, p2

    iput-byte p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_nfc:B

    add-int/lit8 p2, v0, 0x1

    .line 5
    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_info:B

    const/16 v0, 0x9

    new-array v1, v0, [B

    .line 6
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_rgbxchNums:[B

    const/4 v2, 0x0

    .line 7
    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v0

    add-int/lit8 v0, p2, 0x1

    .line 8
    aget-byte p2, p1, p2

    iput-byte p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_ixchFollow:B

    .line 9
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaSpace:I

    add-int/lit8 v0, v0, 0x4

    .line 10
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaIndent:I

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 p2, v0, 0x1

    .line 11
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getUnsignedByte([BI)I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    add-int/lit8 v0, p2, 0x1

    .line 12
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getUnsignedByte([BI)I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    .line 13
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result p2

    iput-short p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_reserved:S

    add-int/lit8 v0, v0, 0x2

    .line 14
    iget p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    new-array v1, p2, [B

    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    .line 15
    iget v3, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlChpx:[B

    .line 16
    invoke-static {p1, v0, v1, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 17
    iget p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    add-int/2addr v0, p2

    .line 18
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlChpx:[B

    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    invoke-static {p1, v0, p2, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19
    iget p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    add-int/2addr v0, p2

    .line 20
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result p2

    if-lez p2, :cond_1

    .line 21
    new-array v1, p2, [C

    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    add-int/lit8 v0, v0, 0x2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_1

    .line 22
    array-length v3, p1

    if-lt v0, v3, :cond_0

    goto :goto_1

    .line 23
    :cond_0
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v4

    int-to-char v4, v4

    aput-char v4, v3, v1

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 24
    :cond_1
    :goto_1
    new-instance p1, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    invoke-direct {p1, p2, v2}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 25
    :cond_2
    :goto_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    move-result-object p2

    .line 27
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 28
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x11

    if-eq v0, v1, :cond_3

    goto :goto_2

    .line 29
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_speecialIndent:I

    goto :goto_2

    .line 30
    :cond_4
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p2

    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaIndent:I

    goto :goto_2

    :cond_5
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    .line 8
    .line 9
    iget v2, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    .line 10
    .line 11
    if-ne v1, v2, :cond_1

    .line 12
    .line 13
    iget v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    .line 14
    .line 15
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    .line 16
    .line 17
    if-ne v1, v2, :cond_1

    .line 18
    .line 19
    iget v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaIndent:I

    .line 20
    .line 21
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaIndent:I

    .line 22
    .line 23
    if-ne v1, v2, :cond_1

    .line 24
    .line 25
    iget v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaSpace:I

    .line 26
    .line 27
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaSpace:I

    .line 28
    .line 29
    if-ne v1, v2, :cond_1

    .line 30
    .line 31
    iget-object v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlChpx:[B

    .line 32
    .line 33
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlChpx:[B

    .line 34
    .line 35
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_1

    .line 40
    .line 41
    iget-object v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    .line 44
    .line 45
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-eqz v1, :cond_1

    .line 50
    .line 51
    iget-byte v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_info:B

    .line 52
    .line 53
    iget-byte v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_info:B

    .line 54
    .line 55
    if-ne v1, v2, :cond_1

    .line 56
    .line 57
    iget v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_iStartAt:I

    .line 58
    .line 59
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_iStartAt:I

    .line 60
    .line 61
    if-ne v1, v2, :cond_1

    .line 62
    .line 63
    iget-byte v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_ixchFollow:B

    .line 64
    .line 65
    iget-byte v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_ixchFollow:B

    .line 66
    .line 67
    if-ne v1, v2, :cond_1

    .line 68
    .line 69
    iget-byte v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_nfc:B

    .line 70
    .line 71
    iget-byte v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_nfc:B

    .line 72
    .line 73
    if-ne v1, v2, :cond_1

    .line 74
    .line 75
    iget-object v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 76
    .line 77
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 78
    .line 79
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([C[C)Z

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    if-eqz v1, :cond_1

    .line 84
    .line 85
    iget-object v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_rgbxchNums:[B

    .line 86
    .line 87
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_rgbxchNums:[B

    .line 88
    .line 89
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    if-eqz v1, :cond_1

    .line 94
    .line 95
    iget-short p1, p1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_reserved:S

    .line 96
    .line 97
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_reserved:S

    .line 98
    .line 99
    if-ne p1, v1, :cond_1

    .line 100
    .line 101
    const/4 v0, 0x1

    .line 102
    :cond_1
    return v0
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getAlignment()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_jc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_info:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLevelProperties()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNumberChar()[C
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNumberFormat()I
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_nfc:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNumberText()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    new-instance v1, Ljava/lang/String;

    .line 8
    .line 9
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    .line 10
    .line 11
    .line 12
    return-object v1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSizeInBytes()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1c

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    .line 6
    .line 7
    add-int/2addr v0, v1

    .line 8
    add-int/lit8 v0, v0, 0x2

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    array-length v1, v1

    .line 15
    mul-int/lit8 v1, v1, 0x2

    .line 16
    .line 17
    add-int/2addr v0, v1

    .line 18
    :cond_0
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSpecialIndnet()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_speecialIndent:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStartAt()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_iStartAt:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTextIndent()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaIndent:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTypeOfCharFollowingTheNumber()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_ixchFollow:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setAlignment(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_jc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_info:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLevelProperties([B)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNumberFormat(I)V
    .locals 0

    .line 1
    int-to-byte p1, p1

    .line 2
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_nfc:B

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNumberProperties([B)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlChpx:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setStartAt(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_iStartAt:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTypeOfCharFollowingTheNumber(B)V
    .locals 0

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_ixchFollow:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toByteArray()[B
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getSizeInBytes()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-array v0, v0, [B

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_iStartAt:I

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 11
    .line 12
    .line 13
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_nfc:B

    .line 14
    .line 15
    const/4 v3, 0x4

    .line 16
    aput-byte v1, v0, v3

    .line 17
    .line 18
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_info:B

    .line 19
    .line 20
    const/4 v3, 0x5

    .line 21
    aput-byte v1, v0, v3

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_rgbxchNums:[B

    .line 24
    .line 25
    const/16 v3, 0x9

    .line 26
    .line 27
    const/4 v4, 0x6

    .line 28
    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 29
    .line 30
    .line 31
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_ixchFollow:B

    .line 32
    .line 33
    const/16 v3, 0xf

    .line 34
    .line 35
    aput-byte v1, v0, v3

    .line 36
    .line 37
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaSpace:I

    .line 38
    .line 39
    const/16 v3, 0x10

    .line 40
    .line 41
    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 42
    .line 43
    .line 44
    const/16 v1, 0x14

    .line 45
    .line 46
    iget v3, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_dxaIndent:I

    .line 47
    .line 48
    invoke-static {v0, v1, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 49
    .line 50
    .line 51
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    .line 52
    .line 53
    int-to-byte v1, v1

    .line 54
    const/16 v3, 0x18

    .line 55
    .line 56
    aput-byte v1, v0, v3

    .line 57
    .line 58
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    .line 59
    .line 60
    int-to-byte v1, v1

    .line 61
    const/16 v3, 0x19

    .line 62
    .line 63
    aput-byte v1, v0, v3

    .line 64
    .line 65
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_reserved:S

    .line 66
    .line 67
    const/16 v3, 0x1a

    .line 68
    .line 69
    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 70
    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlPapx:[B

    .line 73
    .line 74
    iget v3, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    .line 75
    .line 76
    const/16 v4, 0x1c

    .line 77
    .line 78
    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    .line 80
    .line 81
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlPapx:I

    .line 82
    .line 83
    add-int/2addr v4, v1

    .line 84
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_grpprlChpx:[B

    .line 85
    .line 86
    iget v3, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    .line 87
    .line 88
    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    .line 90
    .line 91
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_cbGrpprlChpx:I

    .line 92
    .line 93
    add-int/2addr v4, v1

    .line 94
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 95
    .line 96
    if-nez v1, :cond_0

    .line 97
    .line 98
    invoke-static {v0, v4, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 99
    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_0
    array-length v1, v1

    .line 103
    invoke-static {v0, v4, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 104
    .line 105
    .line 106
    add-int/lit8 v4, v4, 0x2

    .line 107
    .line 108
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->_numberText:[C

    .line 109
    .line 110
    array-length v3, v1

    .line 111
    if-ge v2, v3, :cond_1

    .line 112
    .line 113
    aget-char v1, v1, v2

    .line 114
    .line 115
    invoke-static {v0, v4, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putUShort([BII)V

    .line 116
    .line 117
    .line 118
    add-int/lit8 v4, v4, 0x2

    .line 119
    .line 120
    add-int/lit8 v2, v2, 0x1

    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_1
    :goto_1
    return-object v0
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

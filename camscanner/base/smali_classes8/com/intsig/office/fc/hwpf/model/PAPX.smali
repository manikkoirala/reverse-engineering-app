.class public final Lcom/intsig/office/fc/hwpf/model/PAPX;
.super Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;
.source "PAPX.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/office/fc/hwpf/model/BytePropertyNode<",
        "Lcom/intsig/office/fc/hwpf/model/PAPX;",
        ">;"
    }
.end annotation


# instance fields
.field private _phe:Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;


# direct methods
.method public constructor <init>(IILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;[B)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;-><init>(IILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 10
    new-instance p1, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    invoke-direct {p1}, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PAPX;->_phe:Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    .line 11
    invoke-direct {p0, p4, p5}, Lcom/intsig/office/fc/hwpf/model/PAPX;->findHuge(Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;[B)Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 12
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public constructor <init>(IILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;[BLcom/intsig/office/fc/hwpf/model/ParagraphHeight;[B)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    const/4 v1, 0x2

    invoke-direct {v0, p4, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;-><init>(IILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V

    .line 2
    iput-object p5, p0, Lcom/intsig/office/fc/hwpf/model/PAPX;->_phe:Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    .line 3
    new-instance p1, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    invoke-direct {p1, p4, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, p1, p6}, Lcom/intsig/office/fc/hwpf/model/PAPX;->findHuge(Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;[B)Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public constructor <init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;-><init>(IILjava/lang/Object;)V

    .line 14
    new-instance p1, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    invoke-direct {p1}, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PAPX;->_phe:Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    return-void
.end method

.method public constructor <init>(II[BLcom/intsig/office/fc/hwpf/model/ParagraphHeight;[B)V
    .locals 2

    .line 5
    new-instance v0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    const/4 v1, 0x2

    invoke-direct {v0, p3, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;-><init>(IILjava/lang/Object;)V

    .line 6
    iput-object p4, p0, Lcom/intsig/office/fc/hwpf/model/PAPX;->_phe:Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    .line 7
    new-instance p1, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    invoke-direct {p1, p3, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {p0, p1, p5}, Lcom/intsig/office/fc/hwpf/model/PAPX;->findHuge(Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;[B)Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private findHuge(Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;[B)Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->toByteArray()[B

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    array-length v0, p1

    .line 6
    const/16 v1, 0x8

    .line 7
    .line 8
    if-ne v0, v1, :cond_1

    .line 9
    .line 10
    if-eqz p2, :cond_1

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;-><init>([BI)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    const/16 v3, 0x45

    .line 23
    .line 24
    if-eq v2, v3, :cond_0

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    const/16 v3, 0x46

    .line 31
    .line 32
    if-ne v2, v3, :cond_1

    .line 33
    .line 34
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getSizeCode()I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    const/4 v3, 0x3

    .line 39
    if-ne v2, v3, :cond_1

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    add-int/lit8 v2, v0, 0x1

    .line 46
    .line 47
    array-length v3, p2

    .line 48
    if-ge v2, v3, :cond_1

    .line 49
    .line 50
    invoke-static {p2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    add-int v3, v0, v2

    .line 55
    .line 56
    array-length v4, p2

    .line 57
    if-ge v3, v4, :cond_1

    .line 58
    .line 59
    add-int/lit8 v3, v2, 0x2

    .line 60
    .line 61
    new-array v3, v3, [B

    .line 62
    .line 63
    const/4 v4, 0x0

    .line 64
    aget-byte v5, p1, v4

    .line 65
    .line 66
    aput-byte v5, v3, v4

    .line 67
    .line 68
    const/4 v4, 0x1

    .line 69
    aget-byte p1, p1, v4

    .line 70
    .line 71
    aput-byte p1, v3, v4

    .line 72
    .line 73
    add-int/2addr v0, v1

    .line 74
    invoke-static {p2, v0, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    .line 76
    .line 77
    new-instance p1, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 78
    .line 79
    invoke-direct {p1, v3, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BI)V

    .line 80
    .line 81
    .line 82
    return-object p1

    .line 83
    :cond_1
    const/4 p1, 0x0

    .line 84
    return-object p1
    .line 85
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->equals(Ljava/lang/Object;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PAPX;->_phe:Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    .line 8
    .line 9
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 10
    .line 11
    iget-object p1, p1, Lcom/intsig/office/fc/hwpf/model/PAPX;->_phe:Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->equals(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1

    .line 18
    :cond_0
    const/4 p1, 0x0

    .line 19
    return p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getGrpprl()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->toByteArray()[B

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIstd()S
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getGrpprl()[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    return v2

    .line 10
    :cond_0
    array-length v1, v0

    .line 11
    const/4 v3, 0x1

    .line 12
    if-ne v1, v3, :cond_1

    .line 13
    .line 14
    invoke-static {v0, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getUnsignedByte([BI)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    int-to-short v0, v0

    .line 19
    return v0

    .line 20
    :cond_1
    invoke-static {v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([B)S

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getParagraphHeight()Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PAPX;->_phe:Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getParagraphProperties(Lcom/intsig/office/fc/hwpf/model/StyleSheet;)Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    new-instance p1, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 4
    .line 5
    invoke-direct {p1}, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;-><init>()V

    .line 6
    .line 7
    .line 8
    return-object p1

    .line 9
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getIstd()S

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/StyleSheet;->getParagraphStyle(I)Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getGrpprl()[B

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x2

    .line 22
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmUncompressor;->uncompressPAP(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;[BI)Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getSprmBuf()Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 4
    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "PAPX from "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, " to "

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v2, " (in bytes "

    .line 31
    .line 32
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;->getStartBytes()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;->getEndBytes()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    const-string v1, ")"

    .line 53
    .line 54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

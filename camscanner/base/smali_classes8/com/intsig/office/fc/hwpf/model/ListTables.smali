.class public final Lcom/intsig/office/fc/hwpf/model/ListTables;
.super Ljava/lang/Object;
.source "ListTables.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;
    }
.end annotation


# static fields
.field private static final LIST_DATA_SIZE:I = 0x1c

.field private static final LIST_FORMAT_OVERRIDE_SIZE:I = 0x10

.field private static log:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field _listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

.field _overrideList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/ListTables;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;-><init>(Lcom/intsig/office/fc/hwpf/model/〇080;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>([BII)V
    .locals 8

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;-><init>(Lcom/intsig/office/fc/hwpf/model/〇080;)V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 7
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v0

    add-int/lit8 p2, p2, 0x2

    mul-int/lit8 v1, v0, 0x1c

    add-int/2addr v1, p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    .line 8
    new-instance v4, Lcom/intsig/office/fc/hwpf/model/POIListData;

    invoke-direct {v4, p1, p2}, Lcom/intsig/office/fc/hwpf/model/POIListData;-><init>([BI)V

    .line 9
    iget-object v5, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLsid()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->〇o〇(Ljava/lang/Integer;Lcom/intsig/office/fc/hwpf/model/POIListData;)Lcom/intsig/office/fc/hwpf/model/POIListData;

    add-int/lit8 p2, p2, 0x1c

    .line 10
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/POIListData;->numLevels()I

    move-result v5

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v5, :cond_0

    .line 11
    new-instance v7, Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    invoke-direct {v7, p1, v1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;-><init>([BI)V

    .line 12
    invoke-virtual {v4, v6, v7}, Lcom/intsig/office/fc/hwpf/model/POIListData;->setLevel(ILcom/intsig/office/fc/hwpf/model/POIListLevel;)V

    .line 13
    invoke-virtual {v7}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getSizeInBytes()I

    move-result v7

    add-int/2addr v1, v7

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 14
    :cond_1
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result p2

    add-int/lit8 p3, p3, 0x4

    mul-int/lit8 v0, p2, 0x10

    add-int/2addr v0, p3

    const/4 v1, 0x0

    :goto_2
    if-ge v1, p2, :cond_5

    .line 15
    new-instance v3, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    invoke-direct {v3, p1, p3}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;-><init>([BI)V

    add-int/lit8 p3, p3, 0x10

    .line 16
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;->numOverrides()I

    move-result v4

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v4, :cond_4

    .line 17
    :goto_4
    array-length v6, p1

    if-ge v0, v6, :cond_2

    aget-byte v6, p1, v0

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 18
    :cond_2
    array-length v6, p1

    if-lt v0, v6, :cond_3

    goto :goto_5

    .line 19
    :cond_3
    new-instance v6, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;

    invoke-direct {v6, p1, v0}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;-><init>([BI)V

    .line 20
    invoke-virtual {v3, v5, v6}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;->setOverride(ILcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;)V

    .line 21
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->getSizeInBytes()I

    move-result v6

    add-int/2addr v0, v6

    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 22
    :cond_4
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    return-void
.end method


# virtual methods
.method public addList(Lcom/intsig/office/fc/hwpf/model/POIListData;Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;)I
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLsid()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 6
    .line 7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->〇080(Ljava/lang/Object;)Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->resetListID()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-virtual {p2, v0}, Lcom/intsig/office/fc/hwpf/model/types/LFOAbstractType;->setLsid(I)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 26
    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v1, v2, p1}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->〇o〇(Ljava/lang/Integer;Lcom/intsig/office/fc/hwpf/model/POIListData;)Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 35
    .line 36
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    return v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-object v2, p1, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->size()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    if-ne v1, v2, :cond_5

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->keySet()Ljava/util/Set;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-eqz v2, :cond_2

    .line 36
    .line 37
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    check-cast v2, Ljava/lang/Integer;

    .line 42
    .line 43
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 44
    .line 45
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->〇080(Ljava/lang/Object;)Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    iget-object v4, p1, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 50
    .line 51
    invoke-virtual {v4, v2}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->〇080(Ljava/lang/Object;)Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hwpf/model/POIListData;->equals(Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    if-nez v2, :cond_1

    .line 60
    .line 61
    return v0

    .line 62
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    iget-object v2, p1, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 69
    .line 70
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 71
    .line 72
    .line 73
    move-result v2

    .line 74
    if-ne v1, v2, :cond_5

    .line 75
    .line 76
    const/4 v2, 0x0

    .line 77
    :goto_0
    if-ge v2, v1, :cond_4

    .line 78
    .line 79
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 80
    .line 81
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    check-cast v3, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 86
    .line 87
    iget-object v4, p1, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 88
    .line 89
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    move-result-object v4

    .line 93
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    if-nez v3, :cond_3

    .line 98
    .line 99
    return v0

    .line 100
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_4
    const/4 p1, 0x1

    .line 104
    return p1

    .line 105
    :cond_5
    return v0
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getLevel(II)Lcom/intsig/office/fc/hwpf/model/POIListLevel;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->〇080(Ljava/lang/Object;)Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->numLevels()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-ge p2, v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLevels()[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    aget-object p1, p1, p2

    .line 22
    .line 23
    return-object p1

    .line 24
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/ListTables;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 25
    .line 26
    sget v1, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 27
    .line 28
    new-instance v2, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v3, "Requested level "

    .line 34
    .line 35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string p2, " which was greater than the maximum defined ("

    .line 42
    .line 43
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->numLevels()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string p1, ")"

    .line 54
    .line 55
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 63
    .line 64
    .line 65
    const/4 p1, 0x0

    .line 66
    return-object p1
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getListData(I)Lcom/intsig/office/fc/hwpf/model/POIListData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->〇080(Ljava/lang/Object;)Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getOverride(I)Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lt v0, p1, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 10
    .line 11
    add-int/lit8 p1, p1, -0x1

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 18
    .line 19
    return-object p1

    .line 20
    :cond_0
    const/4 p1, 0x0

    .line 21
    return-object p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getOverrideCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getOverrideIndexFromListID(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    const/4 v2, -0x1

    .line 9
    if-ge v1, v0, :cond_1

    .line 10
    .line 11
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    check-cast v3, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 18
    .line 19
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/types/LFOAbstractType;->getLsid()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    if-ne v3, p1, :cond_0

    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    const/4 v1, -0x1

    .line 32
    :goto_1
    if-eq v1, v2, :cond_2

    .line 33
    .line 34
    return v1

    .line 35
    :cond_2
    new-instance p1, Ljava/util/NoSuchElementException;

    .line 36
    .line 37
    const-string v0, "No list found with the specified ID"

    .line 38
    .line 39
    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public writeListDataTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x2

    .line 13
    new-array v2, v2, [B

    .line 14
    .line 15
    int-to-short v0, v0

    .line 16
    invoke-static {v2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BS)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->Oo08()Ljava/util/List;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    if-eqz v2, :cond_1

    .line 37
    .line 38
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    check-cast v2, Ljava/lang/Integer;

    .line 43
    .line 44
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_listMap:Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;

    .line 45
    .line 46
    invoke-virtual {v3, v2}, Lcom/intsig/office/fc/hwpf/model/ListTables$ListMap;->〇080(Ljava/lang/Object;)Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/POIListData;->toByteArray()[B

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLevels()[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    const/4 v3, 0x0

    .line 62
    :goto_0
    array-length v4, v2

    .line 63
    if-ge v3, v4, :cond_0

    .line 64
    .line 65
    aget-object v4, v2, v3

    .line 66
    .line 67
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->toByteArray()[B

    .line 68
    .line 69
    .line 70
    move-result-object v4

    .line 71
    invoke-virtual {v1, v4}, Ljava/io/OutputStream;->write([B)V

    .line 72
    .line 73
    .line 74
    add-int/lit8 v3, v3, 0x1

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 82
    .line 83
    .line 84
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public writeListOverridesTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v2, 0x4

    .line 13
    new-array v2, v2, [B

    .line 14
    .line 15
    invoke-static {v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 19
    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    const/4 v3, 0x0

    .line 23
    :goto_0
    if-ge v3, v1, :cond_1

    .line 24
    .line 25
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/ListTables;->_overrideList:Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    check-cast v4, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 32
    .line 33
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;->toByteArray()[B

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;->getLevelOverrides()[Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    const/4 v5, 0x0

    .line 45
    :goto_1
    array-length v6, v4

    .line 46
    if-ge v5, v6, :cond_0

    .line 47
    .line 48
    aget-object v6, v4, v5

    .line 49
    .line 50
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->toByteArray()[B

    .line 51
    .line 52
    .line 53
    move-result-object v6

    .line 54
    invoke-virtual {v0, v6}, Ljava/io/OutputStream;->write([B)V

    .line 55
    .line 56
    .line 57
    add-int/lit8 v5, v5, 0x1

    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

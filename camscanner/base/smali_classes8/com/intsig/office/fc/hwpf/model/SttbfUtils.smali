.class Lcom/intsig/office/fc/hwpf/model/SttbfUtils;
.super Ljava/lang/Object;
.source "SttbfUtils.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# direct methods
.method public static 〇080([BI)[Ljava/lang/String;
    .locals 5

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, -0x1

    .line 6
    if-ne v0, v1, :cond_1

    .line 7
    .line 8
    add-int/lit8 p1, p1, 0x2

    .line 9
    .line 10
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    add-int/lit8 p1, p1, 0x4

    .line 15
    .line 16
    new-array v1, v0, [Ljava/lang/String;

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    :goto_0
    if-ge v2, v0, :cond_0

    .line 20
    .line 21
    invoke-static {p0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    add-int/lit8 p1, p1, 0x2

    .line 26
    .line 27
    invoke-static {p0, p1, v3}, Lcom/intsig/office/fc/util/StringUtil;->getFromUnicodeLE([BII)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    mul-int/lit8 v3, v3, 0x2

    .line 32
    .line 33
    add-int/2addr p1, v3

    .line 34
    aput-object v4, v1, v2

    .line 35
    .line 36
    add-int/lit8 v2, v2, 0x1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    return-object v1

    .line 40
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    .line 41
    .line 42
    const-string p1, "Non-extended character Pascal strings are not supported right now. Please, contact POI developers for update."

    .line 43
    .line 44
    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    throw p0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇o00〇〇Oo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;[Ljava/lang/String;)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v1, v0, [B

    .line 3
    .line 4
    const/4 v2, -0x1

    .line 5
    const/4 v3, 0x0

    .line 6
    invoke-static {v1, v3, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 7
    .line 8
    .line 9
    const/4 v2, 0x2

    .line 10
    if-eqz p1, :cond_2

    .line 11
    .line 12
    array-length v4, p1

    .line 13
    if-nez v4, :cond_0

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_0
    array-length v4, p1

    .line 17
    invoke-static {v1, v2, v4}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 21
    .line 22
    .line 23
    array-length v1, p1

    .line 24
    const/4 v4, 0x0

    .line 25
    :goto_0
    if-ge v4, v1, :cond_1

    .line 26
    .line 27
    aget-object v5, p1, v4

    .line 28
    .line 29
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 30
    .line 31
    .line 32
    move-result v6

    .line 33
    mul-int/lit8 v6, v6, 0x2

    .line 34
    .line 35
    add-int/2addr v6, v2

    .line 36
    new-array v7, v6, [B

    .line 37
    .line 38
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 39
    .line 40
    .line 41
    move-result v8

    .line 42
    int-to-short v8, v8

    .line 43
    invoke-static {v7, v3, v8}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 44
    .line 45
    .line 46
    invoke-static {v5, v7, v2}, Lcom/intsig/office/fc/util/StringUtil;->putUnicodeLE(Ljava/lang/String;[BI)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {p0, v7}, Ljava/io/OutputStream;->write([B)V

    .line 50
    .line 51
    .line 52
    add-int/2addr v0, v6

    .line 53
    add-int/lit8 v4, v4, 0x1

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    return v0

    .line 57
    :cond_2
    :goto_1
    invoke-static {v1, v2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 61
    .line 62
    .line 63
    return v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

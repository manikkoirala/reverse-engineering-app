.class public final Lcom/intsig/office/fc/hwpf/model/OldSectionTable;
.super Lcom/intsig/office/fc/hwpf/model/SectionTable;
.source "OldSectionTable.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# direct methods
.method public constructor <init>([BII)V
    .locals 8

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/model/SectionTable;-><init>()V

    .line 3
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    const/16 v1, 0xc

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 4
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    move-result p2

    const/4 p3, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_1

    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    move-result-object v2

    .line 6
    new-instance v3, Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4, p3}, Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;-><init>([BI)V

    .line 7
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;->getFc()I

    move-result v4

    .line 8
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v5

    .line 9
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v2

    const/4 v6, -0x1

    if-ne v4, v6, :cond_0

    .line 10
    new-instance v4, Lcom/intsig/office/fc/hwpf/model/SEPX;

    new-array v6, p3, [B

    invoke-direct {v4, v3, v5, v2, v6}, Lcom/intsig/office/fc/hwpf/model/SEPX;-><init>(Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;II[B)V

    goto :goto_1

    .line 11
    :cond_0
    invoke-static {p1, v4}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v6

    add-int/lit8 v6, v6, 0x2

    .line 12
    new-array v7, v6, [B

    add-int/lit8 v4, v4, 0x2

    .line 13
    invoke-static {p1, v4, v7, p3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 14
    new-instance v4, Lcom/intsig/office/fc/hwpf/model/SEPX;

    invoke-direct {v4, v3, v5, v2, v7}, Lcom/intsig/office/fc/hwpf/model/SEPX;-><init>(Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;II[B)V

    .line 15
    :goto_1
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 16
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    sget-object p2, Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;->instance:Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;

    invoke-static {p1, p2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public constructor <init>([BIIILcom/intsig/office/fc/hwpf/model/TextPieceTable;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hwpf/model/OldSectionTable;-><init>([BII)V

    return-void
.end method

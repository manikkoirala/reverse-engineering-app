.class public Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;
.super Ljava/lang/Object;
.source "PlcfTxbxBkd.java"


# instance fields
.field private CPs:[I

.field private tbkds:[Lcom/intsig/office/fc/hwpf/model/Tbkd;


# direct methods
.method public constructor <init>([BII)V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/office/fc/hwpf/model/Tbkd;->getSize()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    add-int/lit8 v1, p3, -0x4

    .line 9
    .line 10
    add-int/lit8 v2, v0, 0x4

    .line 11
    .line 12
    div-int/2addr v1, v2

    .line 13
    mul-int v2, v1, v0

    .line 14
    .line 15
    sub-int/2addr p3, v2

    .line 16
    div-int/lit8 p3, p3, 0x4

    .line 17
    .line 18
    new-array v2, p3, [I

    .line 19
    .line 20
    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->CPs:[I

    .line 21
    .line 22
    new-array v2, v1, [Lcom/intsig/office/fc/hwpf/model/Tbkd;

    .line 23
    .line 24
    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->tbkds:[Lcom/intsig/office/fc/hwpf/model/Tbkd;

    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    const/4 v3, 0x0

    .line 28
    :goto_0
    if-ge v3, p3, :cond_0

    .line 29
    .line 30
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->CPs:[I

    .line 31
    .line 32
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    aput v5, v4, v3

    .line 37
    .line 38
    add-int/lit8 p2, p2, 0x4

    .line 39
    .line 40
    add-int/lit8 v3, v3, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    :goto_1
    if-ge v2, v1, :cond_1

    .line 44
    .line 45
    iget-object p3, p0, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->tbkds:[Lcom/intsig/office/fc/hwpf/model/Tbkd;

    .line 46
    .line 47
    new-instance v3, Lcom/intsig/office/fc/hwpf/model/Tbkd;

    .line 48
    .line 49
    invoke-direct {v3, p1, p2}, Lcom/intsig/office/fc/hwpf/model/Tbkd;-><init>([BI)V

    .line 50
    .line 51
    .line 52
    aput-object v3, p3, v2

    .line 53
    .line 54
    add-int/2addr p2, v0

    .line 55
    add-int/lit8 v2, v2, 0x1

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_1
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public getCharPosition(I)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->CPs:[I

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    array-length v1, v0

    .line 6
    if-le v1, p1, :cond_0

    .line 7
    .line 8
    aget p1, v0, p1

    .line 9
    .line 10
    return p1

    .line 11
    :cond_0
    const/4 p1, -0x1

    .line 12
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getCharPositions()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->CPs:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTbkds()[Lcom/intsig/office/fc/hwpf/model/Tbkd;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->tbkds:[Lcom/intsig/office/fc/hwpf/model/Tbkd;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

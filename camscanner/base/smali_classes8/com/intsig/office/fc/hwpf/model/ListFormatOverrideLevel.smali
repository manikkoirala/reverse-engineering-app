.class public final Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;
.super Ljava/lang/Object;
.source "ListFormatOverrideLevel.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final BASE_SIZE:I = 0x8

.field private static _fFormatting:Lcom/intsig/office/fc/util/BitField;

.field private static _fStartAt:Lcom/intsig/office/fc/util/BitField;

.field private static _ilvl:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field _iStartAt:I

.field _info:B

.field _lvl:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

.field _reserved:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/16 v0, 0xf

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_ilvl:Lcom/intsig/office/fc/util/BitField;

    .line 8
    .line 9
    const/16 v0, 0x10

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_fStartAt:Lcom/intsig/office/fc/util/BitField;

    .line 16
    .line 17
    const/16 v0, 0x20

    .line 18
    .line 19
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_fFormatting:Lcom/intsig/office/fc/util/BitField;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>([BI)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x3

    .line 5
    new-array v0, v0, [B

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_reserved:[B

    .line 8
    .line 9
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_iStartAt:I

    .line 14
    .line 15
    add-int/lit8 p2, p2, 0x4

    .line 16
    .line 17
    add-int/lit8 v0, p2, 0x1

    .line 18
    .line 19
    aget-byte p2, p1, p2

    .line 20
    .line 21
    iput-byte p2, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_info:B

    .line 22
    .line 23
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_reserved:[B

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    array-length v2, p2

    .line 27
    invoke-static {p1, v0, p2, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 28
    .line 29
    .line 30
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_reserved:[B

    .line 31
    .line 32
    array-length p2, p2

    .line 33
    add-int/2addr v0, p2

    .line 34
    sget-object p2, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_fFormatting:Lcom/intsig/office/fc/util/BitField;

    .line 35
    .line 36
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_info:B

    .line 37
    .line 38
    invoke-virtual {p2, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    if-lez p2, :cond_0

    .line 43
    .line 44
    new-instance p2, Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 45
    .line 46
    invoke-direct {p2, p1, v0}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;-><init>([BI)V

    .line 47
    .line 48
    .line 49
    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_lvl:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 50
    .line 51
    :cond_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_lvl:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    iget-object v3, p1, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_lvl:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 13
    .line 14
    invoke-virtual {v1, v3}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->equals(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    goto :goto_0

    .line 19
    :cond_1
    iget-object v1, p1, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_lvl:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 20
    .line 21
    if-nez v1, :cond_2

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_2
    const/4 v1, 0x0

    .line 26
    :goto_0
    if-eqz v1, :cond_3

    .line 27
    .line 28
    iget v1, p1, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_iStartAt:I

    .line 29
    .line 30
    iget v3, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_iStartAt:I

    .line 31
    .line 32
    if-ne v1, v3, :cond_3

    .line 33
    .line 34
    iget-byte v1, p1, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_info:B

    .line 35
    .line 36
    iget-byte v3, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_info:B

    .line 37
    .line 38
    if-ne v1, v3, :cond_3

    .line 39
    .line 40
    iget-object p1, p1, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_reserved:[B

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_reserved:[B

    .line 43
    .line 44
    invoke-static {p1, v1}, Ljava/util/Arrays;->equals([B[B)Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_3

    .line 49
    .line 50
    const/4 v0, 0x1

    .line 51
    :cond_3
    return v0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getIStartAt()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_iStartAt:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLevel()Lcom/intsig/office/fc/hwpf/model/POIListLevel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_lvl:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLevelNum()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_ilvl:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_info:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSizeInBytes()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_lvl:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->getSizeInBytes()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    add-int/2addr v1, v0

    .line 13
    :goto_0
    return v1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFormatting()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_fFormatting:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_info:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isStartAt()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_fStartAt:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_info:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toByteArray()[B
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->getSizeInBytes()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-array v0, v0, [B

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_iStartAt:I

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 10
    .line 11
    .line 12
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_info:B

    .line 13
    .line 14
    const/4 v2, 0x4

    .line 15
    aput-byte v1, v0, v2

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_reserved:[B

    .line 18
    .line 19
    const/4 v2, 0x3

    .line 20
    const/4 v3, 0x0

    .line 21
    const/4 v4, 0x5

    .line 22
    invoke-static {v1, v3, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;->_lvl:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 26
    .line 27
    if-eqz v1, :cond_0

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->toByteArray()[B

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    array-length v2, v1

    .line 34
    const/16 v4, 0x8

    .line 35
    .line 36
    invoke-static {v1, v3, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    .line 38
    .line 39
    :cond_0
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

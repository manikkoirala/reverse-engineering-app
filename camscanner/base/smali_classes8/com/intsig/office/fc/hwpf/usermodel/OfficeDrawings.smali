.class public interface abstract Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawings;
.super Ljava/lang/Object;
.source "OfficeDrawings.java"


# virtual methods
.method public abstract getOfficeDrawingAt(I)Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;
.end method

.method public abstract getOfficeDrawings()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;",
            ">;"
        }
    .end annotation
.end method

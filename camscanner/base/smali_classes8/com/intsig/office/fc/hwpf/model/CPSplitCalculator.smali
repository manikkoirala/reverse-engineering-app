.class public final Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;
.super Ljava/lang/Object;
.source "CPSplitCalculator.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public getCommentsEnd()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getCommentsStart()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getCcpCommentAtn()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCommentsStart()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getHeaderStoryEnd()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEndNoteEnd()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getEndNoteStart()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getCcpEdn()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEndNoteStart()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getCommentsEnd()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFootnoteEnd()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getFootnoteStart()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getCcpFtn()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFootnoteStart()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getMainDocumentEnd()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeaderStoryEnd()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getHeaderStoryStart()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getCcpHdd()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeaderStoryStart()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getFootnoteEnd()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeaderTextboxEnd()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getHeaderTextboxStart()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getCcpHdrTxtBx()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeaderTextboxStart()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getMainTextboxEnd()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMainDocumentEnd()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getCcpText()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMainDocumentStart()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMainTextboxEnd()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getMainTextboxStart()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getCcpTxtBx()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMainTextboxStart()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getEndNoteEnd()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

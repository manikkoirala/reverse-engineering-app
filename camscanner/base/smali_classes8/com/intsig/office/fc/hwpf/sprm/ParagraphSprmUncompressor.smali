.class public final Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmUncompressor;
.super Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;
.source "ParagraphSprmUncompressor.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final logger:Lcom/intsig/office/fc/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmUncompressor;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmUncompressor;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static handleTabs(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    add-int/lit8 v1, p1, 0x1

    .line 10
    .line 11
    aget-byte p1, v0, p1

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getRgdxaTab()[I

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getRgtbd()[B

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    new-instance v4, Ljava/util/HashMap;

    .line 22
    .line 23
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 24
    .line 25
    .line 26
    const/4 v5, 0x0

    .line 27
    const/4 v6, 0x0

    .line 28
    :goto_0
    array-length v7, v2

    .line 29
    if-ge v6, v7, :cond_0

    .line 30
    .line 31
    aget v7, v2, v6

    .line 32
    .line 33
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object v7

    .line 37
    aget-byte v8, v3, v6

    .line 38
    .line 39
    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    .line 40
    .line 41
    .line 42
    move-result-object v8

    .line 43
    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    add-int/lit8 v6, v6, 0x1

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/4 v2, 0x0

    .line 50
    :goto_1
    if-ge v2, p1, :cond_1

    .line 51
    .line 52
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 57
    .line 58
    .line 59
    move-result-object v6

    .line 60
    invoke-interface {v4, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;->getTabClearPosition()S

    .line 64
    .line 65
    .line 66
    move-result v6

    .line 67
    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    int-to-short v3, v3

    .line 72
    invoke-virtual {p0, v3}, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;->setTabClearPosition(S)V

    .line 73
    .line 74
    .line 75
    add-int/lit8 v1, v1, 0x2

    .line 76
    .line 77
    add-int/lit8 v2, v2, 0x1

    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_1
    add-int/lit8 p1, v1, 0x1

    .line 81
    .line 82
    aget-byte v1, v0, v1

    .line 83
    .line 84
    move v3, p1

    .line 85
    const/4 v2, 0x0

    .line 86
    :goto_2
    if-ge v2, v1, :cond_2

    .line 87
    .line 88
    invoke-static {v0, v3}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 89
    .line 90
    .line 91
    move-result v6

    .line 92
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 93
    .line 94
    .line 95
    move-result-object v6

    .line 96
    mul-int/lit8 v7, v1, 0x2

    .line 97
    .line 98
    add-int/2addr v7, v2

    .line 99
    add-int/2addr v7, p1

    .line 100
    aget-byte v7, v0, v7

    .line 101
    .line 102
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    .line 103
    .line 104
    .line 105
    move-result-object v7

    .line 106
    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    add-int/lit8 v3, v3, 0x2

    .line 110
    .line 111
    add-int/lit8 v2, v2, 0x1

    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_2
    invoke-interface {v4}, Ljava/util/Map;->size()I

    .line 115
    .line 116
    .line 117
    move-result p1

    .line 118
    new-array v0, p1, [I

    .line 119
    .line 120
    new-array v1, p1, [B

    .line 121
    .line 122
    new-instance v2, Ljava/util/ArrayList;

    .line 123
    .line 124
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 125
    .line 126
    .line 127
    move-result-object v3

    .line 128
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 129
    .line 130
    .line 131
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 132
    .line 133
    .line 134
    :goto_3
    if-ge v5, p1, :cond_3

    .line 135
    .line 136
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 137
    .line 138
    .line 139
    move-result-object v3

    .line 140
    check-cast v3, Ljava/lang/Integer;

    .line 141
    .line 142
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 143
    .line 144
    .line 145
    move-result v6

    .line 146
    aput v6, v0, v5

    .line 147
    .line 148
    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    .line 150
    .line 151
    move-result-object v3

    .line 152
    check-cast v3, Ljava/lang/Byte;

    .line 153
    .line 154
    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    .line 155
    .line 156
    .line 157
    move-result v3

    .line 158
    aput-byte v3, v1, v5

    .line 159
    .line 160
    add-int/lit8 v5, v5, 0x1

    .line 161
    .line 162
    goto :goto_3

    .line 163
    :cond_3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setRgdxaTab([I)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setRgtbd([B)V

    .line 167
    .line 168
    .line 169
    return-void
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method static unCompressPAPOperation(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v0

    if-eqz v0, :cond_1d

    const/16 v1, 0x43

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_1b

    const/16 v1, 0x45

    const/4 v4, 0x3

    if-eq v0, v1, :cond_1a

    const/16 v1, 0x61

    if-eq v0, v1, :cond_19

    packed-switch v0, :pswitch_data_0

    packed-switch v0, :pswitch_data_1

    packed-switch v0, :pswitch_data_2

    packed-switch v0, :pswitch_data_3

    packed-switch v0, :pswitch_data_4

    packed-switch v0, :pswitch_data_5

    goto/16 :goto_0

    .line 2
    :pswitch_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFTtpEmbedded(Z)V

    goto/16 :goto_0

    .line 3
    :pswitch_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFInnerTableCell(Z)V

    goto/16 :goto_0

    .line 4
    :pswitch_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getItap()I

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    add-int/2addr v0, p1

    int-to-byte p1, v0

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setItap(I)V

    goto/16 :goto_0

    .line 5
    :pswitch_3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setItap(I)V

    goto/16 :goto_0

    .line 6
    :pswitch_4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFAdjustRight(Z)V

    goto/16 :goto_0

    .line 7
    :pswitch_5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x1

    :cond_3
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFUsePgsuSettings(Z)V

    goto/16 :goto_0

    .line 8
    :pswitch_6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_4

    const/4 v2, 0x1

    :cond_4
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFBiDi(Z)V

    goto/16 :goto_0

    .line 9
    :pswitch_7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setLvl(B)V

    goto/16 :goto_0

    .line 10
    :pswitch_8
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v0

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    .line 12
    aget-byte v1, v0, p1

    if-eqz v1, :cond_5

    const/4 v2, 0x1

    :cond_5
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFPropRMark(Z)V

    add-int/lit8 v1, p1, 0x1

    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v1

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setIbstPropRMark(I)V

    .line 14
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    add-int/2addr p1, v4

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>([BI)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDttmPropRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    .line 16
    :pswitch_9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->size()I

    move-result v0

    sub-int/2addr v0, v4

    new-array v1, v0, [B

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v3

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-static {v1, v2, v3, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 18
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setAnld([B)V

    goto/16 :goto_0

    .line 19
    :pswitch_a
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-short p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFontAlign(S)V

    goto/16 :goto_0

    .line 20
    :pswitch_b
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setWAlignFont(I)V

    goto/16 :goto_0

    .line 21
    :pswitch_c
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_6

    const/4 v2, 0x1

    :cond_6
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFAutoSpaceDN(Z)V

    goto/16 :goto_0

    .line 22
    :pswitch_d
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_7

    const/4 v2, 0x1

    :cond_7
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFAutoSpaceDE(Z)V

    goto/16 :goto_0

    .line 23
    :pswitch_e
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_8

    const/4 v2, 0x1

    :cond_8
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFTopLinePunct(Z)V

    goto/16 :goto_0

    .line 24
    :pswitch_f
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_9

    const/4 v2, 0x1

    :cond_9
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFOverflowPunct(Z)V

    goto/16 :goto_0

    .line 25
    :pswitch_10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_a

    const/4 v2, 0x1

    :cond_a
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFWordWrap(Z)V

    goto/16 :goto_0

    .line 26
    :pswitch_11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_b

    const/4 v2, 0x1

    :cond_b
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFKinsoku(Z)V

    goto/16 :goto_0

    .line 27
    :pswitch_12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_c

    const/4 v2, 0x1

    :cond_c
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFWidowControl(Z)V

    goto/16 :goto_0

    .line 28
    :pswitch_13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_d

    const/4 v2, 0x1

    :cond_d
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFLocked(Z)V

    goto/16 :goto_0

    .line 29
    :pswitch_14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaFromText(I)V

    goto/16 :goto_0

    .line 30
    :pswitch_15
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDyaFromText(I)V

    goto/16 :goto_0

    .line 31
    :pswitch_16
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-short p1, p1

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;-><init>(S)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setShd(Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;)V

    goto/16 :goto_0

    .line 32
    :pswitch_17
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-short p1, p1

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;-><init>(S)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDcs(Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;)V

    goto/16 :goto_0

    .line 33
    :pswitch_18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDyaHeight(I)V

    goto/16 :goto_0

    .line 34
    :pswitch_19
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_e

    const/4 v2, 0x1

    :cond_e
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFNoAutoHyph(Z)V

    goto/16 :goto_0

    .line 35
    :pswitch_1a
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcBar(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 36
    :pswitch_1b
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcBetween(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 37
    :pswitch_1c
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcRight(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 38
    :pswitch_1d
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcBottom(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 39
    :pswitch_1e
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcLeft(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 40
    :pswitch_1f
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcTop(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    goto/16 :goto_0

    .line 41
    :pswitch_20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setWr(B)V

    goto/16 :goto_0

    .line 42
    :pswitch_21
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaFromText(I)V

    goto/16 :goto_0

    .line 43
    :pswitch_22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    and-int/lit8 v0, p1, 0xc

    shr-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    and-int/2addr p1, v4

    int-to-byte p1, p1

    if-eq v0, v4, :cond_f

    .line 44
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setPcVert(B)V

    :cond_f
    if-eq p1, v4, :cond_1e

    .line 45
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setPcHorz(B)V

    goto/16 :goto_0

    .line 46
    :pswitch_23
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaWidth(I)V

    goto/16 :goto_0

    .line 47
    :pswitch_24
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDyaAbs(I)V

    goto/16 :goto_0

    .line 48
    :pswitch_25
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaAbs(I)V

    goto/16 :goto_0

    .line 49
    :pswitch_26
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_10

    const/4 v2, 0x1

    :cond_10
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFTtp(Z)V

    goto/16 :goto_0

    .line 50
    :pswitch_27
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_11

    const/4 v2, 0x1

    :cond_11
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFInTable(Z)V

    goto/16 :goto_0

    .line 51
    :pswitch_28
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDyaAfter(I)V

    goto/16 :goto_0

    .line 52
    :pswitch_29
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDyaBefore(I)V

    goto/16 :goto_0

    .line 53
    :pswitch_2a
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;-><init>([BI)V

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setLspd(Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;)V

    goto/16 :goto_0

    .line 54
    :pswitch_2b
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaLeft1(I)V

    goto/16 :goto_0

    .line 55
    :pswitch_2c
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft()I

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaLeft(I)V

    .line 56
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft()I

    move-result p1

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 57
    :pswitch_2d
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaLeft(I)V

    goto/16 :goto_0

    .line 58
    :pswitch_2e
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaRight(I)V

    goto/16 :goto_0

    .line 59
    :pswitch_2f
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmUncompressor;->handleTabs(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V

    goto/16 :goto_0

    .line 60
    :pswitch_30
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_12

    const/4 v2, 0x1

    :cond_12
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFNoLnn(Z)V

    goto/16 :goto_0

    .line 61
    :pswitch_31
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setIlfo(I)V

    goto/16 :goto_0

    .line 62
    :pswitch_32
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setIlvl(B)V

    goto/16 :goto_0

    .line 63
    :pswitch_33
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcp(B)V

    goto/16 :goto_0

    .line 64
    :pswitch_34
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcl(B)V

    goto/16 :goto_0

    .line 65
    :pswitch_35
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_13

    const/4 v2, 0x1

    :cond_13
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFPageBreakBefore(Z)V

    goto/16 :goto_0

    .line 66
    :pswitch_36
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_14

    const/4 v2, 0x1

    :cond_14
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFKeepFollow(Z)V

    goto/16 :goto_0

    .line 67
    :pswitch_37
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_15

    const/4 v2, 0x1

    :cond_15
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFKeep(Z)V

    goto/16 :goto_0

    .line 68
    :pswitch_38
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_16

    const/4 v2, 0x1

    :cond_16
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFSideBySide(Z)V

    goto/16 :goto_0

    .line 69
    :pswitch_39
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setJc(B)V

    goto/16 :goto_0

    .line 70
    :pswitch_3a
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result v0

    const/16 v1, 0x9

    if-le v0, v1, :cond_17

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result v0

    if-lt v0, v3, :cond_1e

    .line 71
    :cond_17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    .line 72
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setIstd(I)V

    .line 73
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLvl()B

    move-result v0

    add-int/2addr v0, p1

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setLvl(B)V

    shr-int/lit8 p1, p1, 0x7

    and-int/2addr p1, v3

    if-ne p1, v3, :cond_18

    .line 74
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result p1

    invoke-static {p1, v3}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setIstd(I)V

    goto :goto_0

    .line 75
    :cond_18
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result p1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setIstd(I)V

    goto :goto_0

    .line 76
    :cond_19
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;->setJustificationLogical(B)V

    goto :goto_0

    .line 77
    :cond_1a
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getSizeCode()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1e

    .line 78
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->size()I

    move-result v0

    sub-int/2addr v0, v4

    new-array v1, v0, [B

    .line 79
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v3

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    invoke-static {v1, v2, v3, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setNumrm([B)V

    goto :goto_0

    .line 81
    :cond_1b
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    if-eqz p1, :cond_1c

    const/4 v2, 0x1

    :cond_1c
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFNumRMIns(Z)V

    goto :goto_0

    .line 82
    :cond_1d
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setIstd(I)V

    :cond_1e
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x16
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x33
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x3e
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x47
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static uncompressPAP(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;[BI)Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;
    .locals 5

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    check-cast p0, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    .line 8
    .line 9
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 10
    .line 11
    .line 12
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-eqz p1, :cond_1

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    const/4 v1, 0x1

    .line 27
    if-ne p2, v1, :cond_0

    .line 28
    .line 29
    :try_start_1
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmUncompressor;->unCompressPAPOperation(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :catch_0
    move-exception p2

    .line 34
    sget-object v1, Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmUncompressor;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 35
    .line 36
    sget v2, Lcom/intsig/office/fc/util/POILogger;->ERROR:I

    .line 37
    .line 38
    new-instance v3, Ljava/lang/StringBuilder;

    .line 39
    .line 40
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .line 42
    .line 43
    const-string v4, "Unable to apply SPRM operation \'"

    .line 44
    .line 45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    const-string p1, "\': "

    .line 56
    .line 57
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-virtual {v1, v2, p1, p2}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Throwable;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    return-object p0

    .line 69
    :catch_1
    new-instance p0, Ljava/lang/RuntimeException;

    .line 70
    .line 71
    const-string p1, "There is no way this exception should happen!!"

    .line 72
    .line 73
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    throw p0
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

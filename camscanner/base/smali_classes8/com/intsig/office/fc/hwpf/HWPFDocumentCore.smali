.class public abstract Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;
.super Ljava/lang/Object;
.source "HWPFDocumentCore.java"


# static fields
.field protected static final STREAM_OBJECT_POOL:Ljava/lang/String; = "ObjectPool"

.field protected static final STREAM_WORD_DOCUMENT:Ljava/lang/String; = "WordDocument"


# instance fields
.field protected _cbt:Lcom/intsig/office/fc/hwpf/model/CHPBinTable;

.field protected _fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

.field protected _ft:Lcom/intsig/office/fc/hwpf/model/FontTable;

.field protected _lt:Lcom/intsig/office/fc/hwpf/model/ListTables;

.field protected _mainStream:[B

.field protected _objectPool:Lcom/intsig/office/fc/hwpf/usermodel/ObjectPoolImpl;

.field protected _pbt:Lcom/intsig/office/fc/hwpf/model/PAPBinTable;

.field protected _ss:Lcom/intsig/office/fc/hwpf/model/StyleSheet;

.field protected _st:Lcom/intsig/office/fc/hwpf/model/SectionTable;

.field protected cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->verifyAndBuildPOIFS(Ljava/io/InputStream;)Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 9
    .line 10
    const-string v0, "WordDocument"

    .line 11
    .line 12
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getPropertyRawData(Ljava/lang/String;)[B

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 17
    .line 18
    new-instance p1, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 21
    .line 22
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;-><init>([B)V

    .line 23
    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/FIBAbstractType;->isFEncrypted()Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-nez p1, :cond_0

    .line 32
    .line 33
    return-void

    .line 34
    :cond_0
    new-instance p1, Lcom/intsig/office/fc/EncryptedDocumentException;

    .line 35
    .line 36
    const-string v0, "Cannot process encrypted office files!"

    .line 37
    .line 38
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    throw p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static verifyAndBuildPOIFS(Ljava/io/InputStream;)Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/PushbackInputStream;

    .line 2
    .line 3
    const/4 v1, 0x6

    .line 4
    invoke-direct {v0, p0, v1}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 5
    .line 6
    .line 7
    new-array p0, v1, [B

    .line 8
    .line 9
    invoke-virtual {v0, p0}, Ljava/io/InputStream;->read([B)I

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    aget-byte v1, p0, v1

    .line 14
    .line 15
    const/16 v2, 0x7b

    .line 16
    .line 17
    if-ne v1, v2, :cond_1

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    aget-byte v1, p0, v1

    .line 21
    .line 22
    const/16 v2, 0x5c

    .line 23
    .line 24
    if-ne v1, v2, :cond_1

    .line 25
    .line 26
    const/4 v1, 0x2

    .line 27
    aget-byte v1, p0, v1

    .line 28
    .line 29
    const/16 v2, 0x72

    .line 30
    .line 31
    if-ne v1, v2, :cond_1

    .line 32
    .line 33
    const/4 v1, 0x3

    .line 34
    aget-byte v1, p0, v1

    .line 35
    .line 36
    const/16 v2, 0x74

    .line 37
    .line 38
    if-ne v1, v2, :cond_1

    .line 39
    .line 40
    const/4 v1, 0x4

    .line 41
    aget-byte v1, p0, v1

    .line 42
    .line 43
    const/16 v2, 0x66

    .line 44
    .line 45
    if-eq v1, v2, :cond_0

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 49
    .line 50
    const-string v0, "The document is really a RTF file"

    .line 51
    .line 52
    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    throw p0

    .line 56
    :cond_1
    :goto_0
    invoke-virtual {v0, p0}, Ljava/io/PushbackInputStream;->unread([B)V

    .line 57
    .line 58
    .line 59
    new-instance p0, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 60
    .line 61
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;-><init>(Ljava/io/InputStream;)V

    .line 62
    .line 63
    .line 64
    return-object p0
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public getCharacterTable()Lcom/intsig/office/fc/hwpf/model/CHPBinTable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_cbt:Lcom/intsig/office/fc/hwpf/model/CHPBinTable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocumentText()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getText()Ljava/lang/StringBuilder;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFileInformationBlock()Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFontTable()Lcom/intsig/office/fc/hwpf/model/FontTable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_ft:Lcom/intsig/office/fc/hwpf/model/FontTable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getListTables()Lcom/intsig/office/fc/hwpf/model/ListTables;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_lt:Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getObjectsPool()Lcom/intsig/office/fc/hwpf/usermodel/ObjectsPool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_objectPool:Lcom/intsig/office/fc/hwpf/usermodel/ObjectPoolImpl;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getOverallRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
.end method

.method public getParagraphTable()Lcom/intsig/office/fc/hwpf/model/PAPBinTable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_pbt:Lcom/intsig/office/fc/hwpf/model/PAPBinTable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
.end method

.method public getSectionTable()Lcom/intsig/office/fc/hwpf/model/SectionTable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_st:Lcom/intsig/office/fc/hwpf/model/SectionTable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStyleSheet()Lcom/intsig/office/fc/hwpf/model/StyleSheet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_ss:Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getText()Ljava/lang/StringBuilder;
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation
.end method

.method public abstract getTextTable()Lcom/intsig/office/fc/hwpf/model/TextPieceTable;
.end method

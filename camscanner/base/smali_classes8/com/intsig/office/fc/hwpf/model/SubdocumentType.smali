.class public final enum Lcom/intsig/office/fc/hwpf/model/SubdocumentType;
.super Ljava/lang/Enum;
.source "SubdocumentType.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/office/fc/hwpf/model/SubdocumentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final enum ANNOTATION:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final enum ENDNOTE:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final enum FOOTNOTE:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final enum HEADER:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final enum HEADER_TEXTBOX:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final enum MACRO:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final enum MAIN:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final ORDERED:[Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

.field public static final enum TEXTBOX:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;


# instance fields
.field private final fibLongFieldIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 2
    .line 3
    const-string v1, "MAIN"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x3

    .line 7
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;II)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->MAIN:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 13
    .line 14
    const-string v4, "FOOTNOTE"

    .line 15
    .line 16
    const/4 v5, 0x1

    .line 17
    const/4 v6, 0x4

    .line 18
    invoke-direct {v1, v4, v5, v6}, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;II)V

    .line 19
    .line 20
    .line 21
    sput-object v1, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->FOOTNOTE:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 22
    .line 23
    new-instance v4, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 24
    .line 25
    const-string v7, "HEADER"

    .line 26
    .line 27
    const/4 v8, 0x2

    .line 28
    const/4 v9, 0x5

    .line 29
    invoke-direct {v4, v7, v8, v9}, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;II)V

    .line 30
    .line 31
    .line 32
    sput-object v4, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->HEADER:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 33
    .line 34
    new-instance v7, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 35
    .line 36
    const-string v10, "MACRO"

    .line 37
    .line 38
    const/4 v11, 0x6

    .line 39
    invoke-direct {v7, v10, v3, v11}, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;II)V

    .line 40
    .line 41
    .line 42
    sput-object v7, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->MACRO:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 43
    .line 44
    new-instance v10, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 45
    .line 46
    const-string v12, "ANNOTATION"

    .line 47
    .line 48
    const/4 v13, 0x7

    .line 49
    invoke-direct {v10, v12, v6, v13}, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;II)V

    .line 50
    .line 51
    .line 52
    sput-object v10, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->ANNOTATION:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 53
    .line 54
    new-instance v12, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 55
    .line 56
    const-string v14, "ENDNOTE"

    .line 57
    .line 58
    const/16 v15, 0x8

    .line 59
    .line 60
    invoke-direct {v12, v14, v9, v15}, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;II)V

    .line 61
    .line 62
    .line 63
    sput-object v12, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->ENDNOTE:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 64
    .line 65
    new-instance v14, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 66
    .line 67
    const-string v9, "TEXTBOX"

    .line 68
    .line 69
    const/16 v6, 0x9

    .line 70
    .line 71
    invoke-direct {v14, v9, v11, v6}, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;II)V

    .line 72
    .line 73
    .line 74
    sput-object v14, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->TEXTBOX:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 75
    .line 76
    new-instance v6, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 77
    .line 78
    const-string v9, "HEADER_TEXTBOX"

    .line 79
    .line 80
    const/16 v11, 0xa

    .line 81
    .line 82
    invoke-direct {v6, v9, v13, v11}, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;-><init>(Ljava/lang/String;II)V

    .line 83
    .line 84
    .line 85
    sput-object v6, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->HEADER_TEXTBOX:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 86
    .line 87
    new-array v9, v15, [Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 88
    .line 89
    aput-object v0, v9, v2

    .line 90
    .line 91
    aput-object v1, v9, v5

    .line 92
    .line 93
    aput-object v4, v9, v8

    .line 94
    .line 95
    aput-object v7, v9, v3

    .line 96
    .line 97
    const/4 v11, 0x4

    .line 98
    aput-object v10, v9, v11

    .line 99
    .line 100
    const/16 v16, 0x5

    .line 101
    .line 102
    aput-object v12, v9, v16

    .line 103
    .line 104
    const/16 v17, 0x6

    .line 105
    .line 106
    aput-object v14, v9, v17

    .line 107
    .line 108
    aput-object v6, v9, v13

    .line 109
    .line 110
    sput-object v9, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->$VALUES:[Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 111
    .line 112
    new-array v9, v15, [Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 113
    .line 114
    aput-object v0, v9, v2

    .line 115
    .line 116
    aput-object v1, v9, v5

    .line 117
    .line 118
    aput-object v4, v9, v8

    .line 119
    .line 120
    aput-object v7, v9, v3

    .line 121
    .line 122
    aput-object v10, v9, v11

    .line 123
    .line 124
    aput-object v12, v9, v16

    .line 125
    .line 126
    aput-object v14, v9, v17

    .line 127
    .line 128
    aput-object v6, v9, v13

    .line 129
    .line 130
    sput-object v9, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->ORDERED:[Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 131
    .line 132
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->fibLongFieldIndex:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/office/fc/hwpf/model/SubdocumentType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static values()[Lcom/intsig/office/fc/hwpf/model/SubdocumentType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->$VALUES:[Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getFibLongFieldIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->fibLongFieldIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

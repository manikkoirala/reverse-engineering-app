.class public final Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;
.super Ljava/lang/Object;
.source "RevisionMarkAuthorTable.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# instance fields
.field private cData:S

.field private cbExtra:S

.field private entries:[Ljava/lang/String;

.field private fExtend:S


# direct methods
.method public constructor <init>([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 p3, -0x1

    .line 5
    iput-short p3, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->fExtend:S

    .line 6
    .line 7
    const/4 p3, 0x0

    .line 8
    iput-short p3, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cData:S

    .line 9
    .line 10
    iput-short p3, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cbExtra:S

    .line 11
    .line 12
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->fExtend:S

    .line 17
    .line 18
    add-int/lit8 p2, p2, 0x2

    .line 19
    .line 20
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cData:S

    .line 25
    .line 26
    add-int/lit8 p2, p2, 0x2

    .line 27
    .line 28
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cbExtra:S

    .line 33
    .line 34
    add-int/lit8 p2, p2, 0x2

    .line 35
    .line 36
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cData:S

    .line 37
    .line 38
    new-array v0, v0, [Ljava/lang/String;

    .line 39
    .line 40
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    .line 41
    .line 42
    :goto_0
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cData:S

    .line 43
    .line 44
    if-ge p3, v0, :cond_0

    .line 45
    .line 46
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    add-int/lit8 p2, p2, 0x2

    .line 51
    .line 52
    invoke-static {p1, p2, v0}, Lcom/intsig/office/fc/util/StringUtil;->getFromUnicodeLE([BII)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    mul-int/lit8 v0, v0, 0x2

    .line 57
    .line 58
    add-int/2addr p2, v0

    .line 59
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    .line 60
    .line 61
    aput-object v1, v0, p3

    .line 62
    .line 63
    add-int/lit8 p3, p3, 0x1

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_0
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public getAuthor(I)Ljava/lang/String;
    .locals 2

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    .line 4
    .line 5
    array-length v1, v0

    .line 6
    if-ge p1, v1, :cond_0

    .line 7
    .line 8
    aget-object p1, v0, p1

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSize()I
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cData:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v0, v0, [B

    .line 3
    .line 4
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->fExtend:S

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-static {v0, v2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 8
    .line 9
    .line 10
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cData:S

    .line 11
    .line 12
    const/4 v3, 0x2

    .line 13
    invoke-static {v0, v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 14
    .line 15
    .line 16
    const/4 v1, 0x4

    .line 17
    iget-short v4, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->cbExtra:S

    .line 18
    .line 19
    invoke-static {v0, v1, v4}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/RevisionMarkAuthorTable;->entries:[Ljava/lang/String;

    .line 26
    .line 27
    array-length v1, v0

    .line 28
    const/4 v4, 0x0

    .line 29
    :goto_0
    if-ge v4, v1, :cond_0

    .line 30
    .line 31
    aget-object v5, v0, v4

    .line 32
    .line 33
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    mul-int/lit8 v6, v6, 0x2

    .line 38
    .line 39
    add-int/2addr v6, v3

    .line 40
    new-array v6, v6, [B

    .line 41
    .line 42
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 43
    .line 44
    .line 45
    move-result v7

    .line 46
    int-to-short v7, v7

    .line 47
    invoke-static {v6, v2, v7}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 48
    .line 49
    .line 50
    invoke-static {v5, v6, v3}, Lcom/intsig/office/fc/util/StringUtil;->putUnicodeLE(Ljava/lang/String;[BI)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write([B)V

    .line 54
    .line 55
    .line 56
    add-int/lit8 v4, v4, 0x1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

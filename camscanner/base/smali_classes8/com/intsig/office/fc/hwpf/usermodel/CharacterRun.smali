.class public final Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;
.super Lcom/intsig/office/fc/hwpf/usermodel/Range;
.source "CharacterRun.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SPRM_BRC:S = 0x6865s

.field public static final SPRM_CHARSCALE:S = 0x4852s

.field public static final SPRM_CPG:S = 0x486bs

.field public static final SPRM_DISPFLDRMARK:S = -0x359es

.field public static final SPRM_DTTMRMARK:S = 0x6805s

.field public static final SPRM_DTTMRMARKDEL:S = 0x6864s

.field public static final SPRM_DXASPACE:S = -0x77c0s

.field public static final SPRM_FBOLD:S = 0x835s

.field public static final SPRM_FCAPS:S = 0x83bs

.field public static final SPRM_FDATA:S = 0x806s

.field public static final SPRM_FDSTRIKE:S = 0x2a53s

.field public static final SPRM_FELID:S = 0x486es

.field public static final SPRM_FEMBOSS:S = 0x858s

.field public static final SPRM_FFLDVANISH:S = 0x802s

.field public static final SPRM_FIMPRINT:S = 0x854s

.field public static final SPRM_FITALIC:S = 0x836s

.field public static final SPRM_FOBJ:S = 0x856s

.field public static final SPRM_FOLE2:S = 0x80as

.field public static final SPRM_FOUTLINE:S = 0x838s

.field public static final SPRM_FRMARK:S = 0x801s

.field public static final SPRM_FRMARKDEL:S = 0x800s

.field public static final SPRM_FSHADOW:S = 0x839s

.field public static final SPRM_FSMALLCAPS:S = 0x83as

.field public static final SPRM_FSPEC:S = 0x855s

.field public static final SPRM_FSTRIKE:S = 0x837s

.field public static final SPRM_FVANISH:S = 0x83cs

.field public static final SPRM_HIGHLIGHT:S = 0x2a0cs

.field public static final SPRM_HPS:S = 0x4a43s

.field public static final SPRM_HPSKERN:S = 0x484bs

.field public static final SPRM_HPSPOS:S = 0x4845s

.field public static final SPRM_IBSTRMARK:S = 0x4804s

.field public static final SPRM_IBSTRMARKDEL:S = 0x4863s

.field public static final SPRM_ICO:S = 0x2a42s

.field public static final SPRM_IDCTHINT:S = 0x286fs

.field public static final SPRM_IDSIRMARKDEL:S = 0x4867s

.field public static final SPRM_ISS:S = 0x2a48s

.field public static final SPRM_ISTD:S = 0x4a30s

.field public static final SPRM_KUL:S = 0x2a3es

.field public static final SPRM_LID:S = 0x4a41s

.field public static final SPRM_NONFELID:S = 0x486ds

.field public static final SPRM_OBJLOCATION:S = 0x680es

.field public static final SPRM_PICLOCATION:S = 0x6a03s

.field public static final SPRM_PROPRMARK:S = -0x35a9s

.field public static final SPRM_RGFTCASCII:S = 0x4a4fs

.field public static final SPRM_RGFTCFAREAST:S = 0x4a50s

.field public static final SPRM_RGFTCNOTFAREAST:S = 0x4a51s

.field public static final SPRM_SFXTEXT:S = 0x2859s

.field public static final SPRM_SHD:S = 0x4866s

.field public static final SPRM_SYMBOL:S = 0x6a09s

.field public static final SPRM_YSRI:S = 0x484es


# instance fields
.field _chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

.field _props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

.field private text:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/hwpf/model/CHPX;Lcom/intsig/office/fc/hwpf/model/StyleSheet;SLcom/intsig/office/fc/hwpf/usermodel/Range;)V
    .locals 3

    .line 1
    iget v0, p4, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_start:I

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iget v1, p4, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_end:I

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-direct {p0, v0, v1, p4}, Lcom/intsig/office/fc/hwpf/usermodel/Range;-><init>(IILcom/intsig/office/fc/hwpf/usermodel/Range;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/hwpf/model/CHPX;->getCharacterProperties(Lcom/intsig/office/fc/hwpf/model/StyleSheet;S)Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 25
    .line 26
    .line 27
    move-result-object p2

    .line 28
    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/CHPX;->getSprmBuf()Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    check-cast v2, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 22
    .line 23
    .line 24
    iget-object v1, v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmRMarkDel()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    check-cast v2, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 37
    .line 38
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmRMarkDel(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 39
    .line 40
    .line 41
    iget-object v1, v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 44
    .line 45
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmPropRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    check-cast v2, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 54
    .line 55
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmPropRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 56
    .line 57
    .line 58
    iget-object v1, v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 59
    .line 60
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 61
    .line 62
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmDispFldRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    check-cast v2, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 71
    .line 72
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmDispFldRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 73
    .line 74
    .line 75
    iget-object v1, v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 76
    .line 77
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 78
    .line 79
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getXstDispFldRMark()[B

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    invoke-virtual {v2}, [B->clone()Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    check-cast v2, [B

    .line 88
    .line 89
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setXstDispFldRMark([B)V

    .line 90
    .line 91
    .line 92
    iget-object v1, v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 93
    .line 94
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 95
    .line 96
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;->clone()Ljava/lang/Object;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    check-cast v2, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 105
    .line 106
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setShd(Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;)V

    .line 107
    .line 108
    .line 109
    return-object v0
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public cloneProperties()Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    return-object v0

    .line 10
    :catch_0
    move-exception v0

    .line 11
    new-instance v1, Ljava/lang/RuntimeException;

    .line 12
    .line 13
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 14
    .line 15
    .line 16
    throw v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getBrc()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCharacterSpacing()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDxaSpace()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIco()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFontName()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_doc:Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getFontTable()Lcom/intsig/office/fc/hwpf/model/FontTable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_doc:Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getFontTable()Lcom/intsig/office/fc/hwpf/model/FontTable;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFtcAscii()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/FontTable;->getMainFont(I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
.end method

.method public getFontSize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHighlightedColor()B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIcoHighlight()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIco24()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;->getIco24()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getKerning()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsKern()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLanguageCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getLidDefault()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getObjOffset()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFcObj()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPicOffset()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFcPic()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSubSuperScriptIndex()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIss()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-short v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSymbolCharacter()C
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isSymbol()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getXchSym()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-char v0, v0

    .line 14
    return v0

    .line 15
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 16
    .line 17
    const-string v1, "Not a symbol CharacterRun"

    .line 18
    .line 19
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    throw v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSymbolFont()Lcom/intsig/office/fc/hwpf/model/Ffn;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isSymbol()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_doc:Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getFontTable()Lcom/intsig/office/fc/hwpf/model/FontTable;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/FontTable;->getFontNames()[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    array-length v1, v0

    .line 18
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFtcSym()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-gt v1, v2, :cond_0

    .line 25
    .line 26
    const/4 v0, 0x0

    .line 27
    return-object v0

    .line 28
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFtcSym()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    aget-object v0, v0, v1

    .line 35
    .line 36
    return-object v0

    .line 37
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 38
    .line 39
    const-string v1, "Not a symbol CharacterRun"

    .line 40
    .line 41
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    throw v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getUnderlineCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getKul()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getUnderlineColor()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getUnderlineColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getVerticalOffset()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isBold()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFBold()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isCapitalized()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFCaps()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isData()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFData()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDoubleStrikeThrough()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFDStrike()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isEmbossed()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFEmboss()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFldVanished()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFFldVanish()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isHighlighted()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFHighlight()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isImprinted()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFImprint()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isItalic()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFItalic()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isMarkedDeleted()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFRMarkDel()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isMarkedInserted()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFRMark()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isObj()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFObj()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOle2()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFOle2()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOutlined()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFOutline()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShadowed()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFShadow()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSmallCaps()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFSmallCaps()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSpecialCharacter()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFSpec()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isStrikeThrough()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFStrike()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSymbol()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->isSpecialCharacter()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const-string v1, "("

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isVanished()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFVanish()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public markDeleted(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFRMarkDel(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x800

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public markInserted(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFRMark(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x801

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBold(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFBold(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x835

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCapitalized(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFCaps(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x83b

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCharacterSpacing(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDxaSpace(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, -0x77c0

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setColor(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    int-to-byte p1, p1

    .line 4
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIco(B)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x2a42

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setData(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFData(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x856

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDoubleStrikethrough(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFDStrike(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x2a53

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setEmbossed(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFEmboss(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x858

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFldVanish(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFFldVanish(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x802

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFontSize(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x4a43

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFtcAscii(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFtcAscii(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x4a4f

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFtcFE(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFtcFE(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x4a50

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFtcOther(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFtcOther(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x4a51

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHighlighted(B)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFHighlight(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIcoHighlight(B)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 13
    .line 14
    const/16 v1, 0x2a0c

    .line 15
    .line 16
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIco24(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;->setIco24(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setImprinted(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFImprint(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x854

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setItalic(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFItalic(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x836

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setKerning(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHpsKern(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x484b

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setObj(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFObj(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x856

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setObjOffset(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFcObj(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x680e

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOle2(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFOle2(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x856

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOutline(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFOutline(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x838

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPicOffset(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFcPic(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x6a03

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShadow(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFShadow(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x839

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSmallCaps(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSmallCaps(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x83a

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSpecialCharacter(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSpec(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x855

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSubSuperScriptIndex(S)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDxaSpace(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, -0x77c0

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnderlineCode(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    int-to-byte p1, p1

    .line 4
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setKul(B)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x2a3e

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setVanished(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFVanish(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x83c

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setVerticalOffset(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    int-to-short v1, p1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHpsPos(S)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x4845

    .line 10
    .line 11
    int-to-byte p1, p1

    .line 12
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public strikeThrough(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_props:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFStrike(Z)V

    .line 4
    .line 5
    .line 6
    int-to-byte p1, p1

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->_chpx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 8
    .line 9
    const/16 v1, 0x837

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public text()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-super {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->text()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text:Ljava/lang/String;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text:Ljava/lang/String;

    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;->text()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "CharacterRun of "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v2, " characters - "

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public type()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

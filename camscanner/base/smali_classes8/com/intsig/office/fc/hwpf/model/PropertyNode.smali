.class public abstract Lcom/intsig/office/fc/hwpf/model/PropertyNode;
.super Ljava/lang/Object;
.source "PropertyNode.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;,
        Lcom/intsig/office/fc/hwpf/model/PropertyNode$EndComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/intsig/office/fc/hwpf/model/PropertyNode<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "TT;>;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final _logger:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field protected _buf:Ljava/lang/Object;

.field private _cpEnd:I

.field private _cpStart:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected constructor <init>(IILjava/lang/Object;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 7
    .line 8
    iput-object p3, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 9
    .line 10
    if-gez p1, :cond_0

    .line 11
    .line 12
    sget-object p1, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 13
    .line 14
    sget p2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 15
    .line 16
    new-instance p3, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v0, "A property claimed to start before zero, at "

    .line 22
    .line 23
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 27
    .line 28
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v0, "! Resetting it to zero, and hoping for the best"

    .line 32
    .line 33
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p3

    .line 40
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 41
    .line 42
    .line 43
    const/4 p1, 0x0

    .line 44
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 45
    .line 46
    :cond_0
    iget p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 47
    .line 48
    iget p2, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 49
    .line 50
    if-ge p1, p2, :cond_1

    .line 51
    .line 52
    sget-object p1, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 53
    .line 54
    sget p2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 55
    .line 56
    new-instance p3, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    const-string v0, "A property claimed to end ("

    .line 62
    .line 63
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 67
    .line 68
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    const-string v0, ") before start! Resetting end to start, and hoping for the best"

    .line 72
    .line 73
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p3

    .line 80
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 81
    .line 82
    .line 83
    iget p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 84
    .line 85
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 86
    .line 87
    :cond_1
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public adjustForDelete(II)V
    .locals 3

    .line 1
    add-int v0, p1, p2

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 4
    .line 5
    if-le v1, p1, :cond_2

    .line 6
    .line 7
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 8
    .line 9
    if-ge v2, v0, :cond_1

    .line 10
    .line 11
    if-lt v0, v1, :cond_0

    .line 12
    .line 13
    move v1, p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    sub-int/2addr v1, p2

    .line 16
    :goto_0
    iput v1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 17
    .line 18
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    sub-int/2addr v1, p2

    .line 26
    iput v1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 27
    .line 28
    sub-int/2addr v2, p2

    .line 29
    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 30
    .line 31
    :cond_2
    :goto_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public clone()Lcom/intsig/office/fc/hwpf/model/PropertyNode;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 2
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->clone()Lcom/intsig/office/fc/hwpf/model/PropertyNode;

    move-result-object v0

    return-object v0
.end method

.method public compareTo(Lcom/intsig/office/fc/hwpf/model/PropertyNode;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result p1

    .line 3
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    if-ge v0, p1, :cond_1

    const/4 p1, -0x1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PropertyNode;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->compareTo(Lcom/intsig/office/fc/hwpf/model/PropertyNode;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->limitsAreEqual(Ljava/lang/Object;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PropertyNode;

    .line 8
    .line 9
    iget-object p1, p1, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 10
    .line 11
    instance-of v0, p1, [B

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 16
    .line 17
    instance-of v1, v0, [B

    .line 18
    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    check-cast p1, [B

    .line 22
    .line 23
    check-cast v0, [B

    .line 24
    .line 25
    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    return p1

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 31
    .line 32
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    return p1

    .line 37
    :cond_1
    const/4 p1, 0x0

    .line 38
    return p1
.end method

.method public getEnd()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStart()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 2
    .line 3
    mul-int/lit8 v0, v0, 0x1f

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_buf:Ljava/lang/Object;

    .line 6
    .line 7
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected limitsAreEqual(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PropertyNode;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 8
    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 16
    .line 17
    if-ne p1, v0, :cond_0

    .line 18
    .line 19
    const/4 p1, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 p1, 0x0

    .line 22
    :goto_0
    return p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setEnd(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpEnd:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setStart(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->_cpStart:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

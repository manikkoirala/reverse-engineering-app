.class final Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;
.super Ljava/lang/Object;
.source "BookmarksImpl.java"

# interfaces
.implements Lcom/intsig/office/fc/hwpf/usermodel/POIBookmark;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BookmarkImpl"
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

.field final synthetic 〇o00〇〇Oo:Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;


# direct methods
.method private constructor <init>(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇o00〇〇Oo:Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;Lcom/intsig/office/fc/hwpf/usermodel/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;-><init>(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    if-nez p1, :cond_1

    .line 7
    .line 8
    return v1

    .line 9
    :cond_1
    const-class v2, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;

    .line 10
    .line 11
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 12
    .line 13
    .line 14
    move-result-object v3

    .line 15
    if-eq v2, v3, :cond_2

    .line 16
    .line 17
    return v1

    .line 18
    :cond_2
    check-cast p1, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;

    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 21
    .line 22
    if-nez v2, :cond_3

    .line 23
    .line 24
    iget-object p1, p1, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 25
    .line 26
    if-eqz p1, :cond_4

    .line 27
    .line 28
    return v1

    .line 29
    :cond_3
    iget-object p1, p1, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 30
    .line 31
    invoke-virtual {v2, p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->equals(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    if-nez p1, :cond_4

    .line 36
    .line 37
    return v1

    .line 38
    :cond_4
    return v0
.end method

.method public getEnd()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇o00〇〇Oo:Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;->〇080(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;)Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->getDescriptorFirstIndex(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇o00〇〇Oo:Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;

    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;->〇080(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;)Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->getDescriptorLim(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 24
    .line 25
    .line 26
    move-result v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    return v0

    .line 28
    :catch_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇o00〇〇Oo:Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;->〇080(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;)Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->getDescriptorFirstIndex(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇o00〇〇Oo:Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;

    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;->〇080(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;)Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->getName(I)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    return-object v0

    .line 24
    :catch_0
    const-string v0, ""

    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStart()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->hashCode()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 12
    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇o00〇〇Oo:Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;->〇080(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;)Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇080:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->getDescriptorFirstIndex(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->〇o00〇〇Oo:Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;

    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;->〇080(Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;)Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1, v0, p1}, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->setName(ILjava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Bookmark ["

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->getStart()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, "; "

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->getEnd()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v1, "): name: "

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl$BookmarkImpl;->getName()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.class public final Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;
.super Ljava/lang/Object;
.source "FIBFieldHandler.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field public static final AUTOSAVESOURCE:I = 0x23

.field public static final BKDEDN:I = 0x44

.field public static final BKDFTN:I = 0x42

.field public static final BKDMOTHER:I = 0x40

.field public static final CLX:I = 0x21

.field public static final CMDS:I = 0x18

.field public static final DGGINFO:I = 0x32

.field public static final DOCUNDO:I = 0x4d

.field public static final DOP:I = 0x1f

.field private static final FIELD_SIZE:I = 0x8

.field public static final FORMFLDSTTBS:I = 0x2d

.field public static final GRPXSTATNOWNERS:I = 0x24

.field public static final MODIFIED:I = 0x57

.field public static final PGDEDN:I = 0x43

.field public static final PGDFTN:I = 0x41

.field public static final PGDMOTHER:I = 0x3f

.field public static final PLCASUMY:I = 0x59

.field public static final PLCDOAHDR:I = 0x27

.field public static final PLCFANDREF:I = 0x4

.field public static final PLCFANDTXT:I = 0x5

.field public static final PLCFATNBKF:I = 0x2a

.field public static final PLCFATNBKL:I = 0x2b

.field public static final PLCFBKF:I = 0x16

.field public static final PLCFBKL:I = 0x17

.field public static final PLCFBTECHPX:I = 0xc

.field public static final PLCFBTELVC:I = 0x56

.field public static final PLCFBTEPAPX:I = 0xd

.field public static final PLCFDOAMOM:I = 0x26

.field public static final PLCFENDREF:I = 0x2e

.field public static final PLCFENDTXT:I = 0x2f

.field public static final PLCFFLDATN:I = 0x13

.field public static final PLCFFLDEDN:I = 0x30

.field public static final PLCFFLDFTN:I = 0x12

.field public static final PLCFFLDHDR:I = 0x11

.field public static final PLCFFLDHDRTXBX:I = 0x3b

.field public static final PLCFFLDMCR:I = 0x14

.field public static final PLCFFLDMOM:I = 0x10

.field public static final PLCFFLDTXBX:I = 0x39

.field public static final PLCFFNDREF:I = 0x2

.field public static final PLCFFNDTXT:I = 0x3

.field public static final PLCFGLSY:I = 0xa

.field public static final PLCFGRAM:I = 0x5a

.field public static final PLCFHDD:I = 0xb

.field public static final PLCFHDRTXBXTXT:I = 0x3a

.field public static final PLCFLST:I = 0x49

.field public static final PLCFLVC:I = 0x58

.field public static final PLCFPAD:I = 0x7

.field public static final PLCFPGDEDN:I = 0x31

.field public static final PLCFPGDFTN:I = 0x22

.field public static final PLCFPHE:I = 0x8

.field public static final PLCFSEA:I = 0xe

.field public static final PLCFSED:I = 0x6

.field public static final PLCFSPL:I = 0x37

.field public static final PLCFTXBXBKD:I = 0x4b

.field public static final PLCFTXBXHDRBKD:I = 0x4c

.field public static final PLCFTXBXTXT:I = 0x38

.field public static final PLCFWKB:I = 0x36

.field public static final PLCMCR:I = 0x19

.field public static final PLCOCX:I = 0x55

.field public static final PLCSPAHDR:I = 0x29

.field public static final PLCSPAMOM:I = 0x28

.field public static final PLCUPCRGBUSE:I = 0x51

.field public static final PLCUPCUSP:I = 0x52

.field public static final PLFLFO:I = 0x4a

.field public static final PLGOSL:I = 0x54

.field public static final PMS:I = 0x2c

.field public static final PRDRVR:I = 0x1b

.field public static final PRENVLAND:I = 0x1d

.field public static final PRENVPORT:I = 0x1c

.field public static final RGBUSE:I = 0x4e

.field public static final ROUTESLIP:I = 0x46

.field public static final STSHF:I = 0x1

.field public static final STSHFORIG:I = 0x0

.field public static final STTBAUTOCAPTION:I = 0x35

.field public static final STTBCAPTION:I = 0x34

.field public static final STTBFASSOC:I = 0x20

.field public static final STTBFATNBKMK:I = 0x25

.field public static final STTBFBKMK:I = 0x15

.field public static final STTBFFFN:I = 0xf

.field public static final STTBFINTFLD:I = 0x45

.field public static final STTBFMCR:I = 0x1a

.field public static final STTBFNM:I = 0x48

.field public static final STTBFRMARK:I = 0x33

.field public static final STTBFUSSR:I = 0x5c

.field public static final STTBGLSY:I = 0x9

.field public static final STTBGLSYSTYLE:I = 0x53

.field public static final STTBLISTNAMES:I = 0x5b

.field public static final STTBSAVEDBY:I = 0x47

.field public static final STTBTTMBD:I = 0x3d

.field public static final STWUSER:I = 0x3c

.field public static final UNUSED:I = 0x3e

.field public static final USKF:I = 0x50

.field public static final USP:I = 0x4f

.field public static final WSS:I = 0x1e

.field private static log:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field private _fields:[I

.field private _unknownMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/fc/hwpf/model/UnhandledDataStructure;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>([BI[BLjava/util/HashSet;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BI[B",
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/Map;

    .line 10
    .line 11
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    add-int/lit8 p2, p2, 0x2

    .line 16
    .line 17
    mul-int/lit8 v1, v0, 0x2

    .line 18
    .line 19
    new-array v1, v1, [I

    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 22
    .line 23
    const/4 v1, 0x0

    .line 24
    :goto_0
    if-ge v1, v0, :cond_2

    .line 25
    .line 26
    mul-int/lit8 v2, v1, 0x8

    .line 27
    .line 28
    add-int/2addr v2, p2

    .line 29
    invoke-static {p1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    add-int/lit8 v2, v2, 0x4

    .line 34
    .line 35
    invoke-static {p1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    invoke-virtual {p4, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    xor-int/2addr v4, p5

    .line 48
    if-eqz v4, :cond_1

    .line 49
    .line 50
    if-lez v2, :cond_1

    .line 51
    .line 52
    add-int v4, v3, v2

    .line 53
    .line 54
    array-length v5, p3

    .line 55
    if-le v4, v5, :cond_0

    .line 56
    .line 57
    sget-object v4, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 58
    .line 59
    sget v5, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 60
    .line 61
    new-instance v6, Ljava/lang/StringBuilder;

    .line 62
    .line 63
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    const-string v7, "Unhandled data structure points to outside the buffer. offset = "

    .line 67
    .line 68
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    const-string v7, ", length = "

    .line 75
    .line 76
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    const-string v7, ", buffer length = "

    .line 83
    .line 84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    array-length v7, p3

    .line 88
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v6

    .line 95
    invoke-virtual {v4, v5, v6}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 96
    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_0
    new-instance v4, Lcom/intsig/office/fc/hwpf/model/UnhandledDataStructure;

    .line 100
    .line 101
    invoke-direct {v4, p3, v3, v2}, Lcom/intsig/office/fc/hwpf/model/UnhandledDataStructure;-><init>([BII)V

    .line 102
    .line 103
    .line 104
    iget-object v5, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/Map;

    .line 105
    .line 106
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 107
    .line 108
    .line 109
    move-result-object v6

    .line 110
    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    .line 112
    .line 113
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 114
    .line 115
    mul-int/lit8 v5, v1, 0x2

    .line 116
    .line 117
    aput v3, v4, v5

    .line 118
    .line 119
    add-int/lit8 v5, v5, 0x1

    .line 120
    .line 121
    aput v2, v4, v5

    .line 122
    .line 123
    add-int/lit8 v1, v1, 0x1

    .line 124
    .line 125
    goto :goto_0

    .line 126
    :cond_2
    return-void
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method


# virtual methods
.method public clearFields()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFieldOffset(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 2
    .line 3
    mul-int/lit8 p1, p1, 0x2

    .line 4
    .line 5
    aget p1, v0, p1

    .line 6
    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getFieldSize(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 2
    .line 3
    mul-int/lit8 p1, p1, 0x2

    .line 4
    .line 5
    add-int/lit8 p1, p1, 0x1

    .line 6
    .line 7
    aget p1, v0, p1

    .line 8
    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFieldOffset(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 2
    .line 3
    mul-int/lit8 p1, p1, 0x2

    .line 4
    .line 5
    aput p2, v0, p1

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFieldSize(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 2
    .line 3
    mul-int/lit8 p1, p1, 0x2

    .line 4
    .line 5
    add-int/lit8 p1, p1, 0x1

    .line 6
    .line 7
    aput p2, v0, p1

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public sizeInBytes()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    mul-int/lit8 v0, v0, 0x4

    .line 5
    .line 6
    add-int/lit8 v0, v0, 0x2

    .line 7
    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method writeTo([BILcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    div-int/lit8 v0, v0, 0x2

    .line 5
    .line 6
    int-to-short v1, v0

    .line 7
    invoke-static {p1, p2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 8
    .line 9
    .line 10
    add-int/lit8 p2, p2, 0x2

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    :goto_0
    if-ge v1, v0, :cond_1

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_unknownMap:Ljava/util/Map;

    .line 16
    .line 17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/UnhandledDataStructure;

    .line 26
    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 30
    .line 31
    mul-int/lit8 v4, v1, 0x2

    .line 32
    .line 33
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 34
    .line 35
    .line 36
    move-result v5

    .line 37
    aput v5, v3, v4

    .line 38
    .line 39
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    invoke-static {p1, p2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 44
    .line 45
    .line 46
    add-int/lit8 p2, p2, 0x4

    .line 47
    .line 48
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/UnhandledDataStructure;->getBuf()[B

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-virtual {p3, v2}, Ljava/io/OutputStream;->write([B)V

    .line 53
    .line 54
    .line 55
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 56
    .line 57
    add-int/lit8 v4, v4, 0x1

    .line 58
    .line 59
    array-length v5, v2

    .line 60
    aput v5, v3, v4

    .line 61
    .line 62
    array-length v2, v2

    .line 63
    invoke-static {p1, p2, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 64
    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 68
    .line 69
    mul-int/lit8 v3, v1, 0x2

    .line 70
    .line 71
    aget v2, v2, v3

    .line 72
    .line 73
    invoke-static {p1, p2, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 74
    .line 75
    .line 76
    add-int/lit8 p2, p2, 0x4

    .line 77
    .line 78
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/FIBFieldHandler;->_fields:[I

    .line 79
    .line 80
    add-int/lit8 v3, v3, 0x1

    .line 81
    .line 82
    aget v2, v2, v3

    .line 83
    .line 84
    invoke-static {p1, p2, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 85
    .line 86
    .line 87
    :goto_1
    add-int/lit8 p2, p2, 0x4

    .line 88
    .line 89
    add-int/lit8 v1, v1, 0x1

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_1
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

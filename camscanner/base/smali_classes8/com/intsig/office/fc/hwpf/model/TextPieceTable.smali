.class public Lcom/intsig/office/fc/hwpf/model/TextPieceTable;
.super Ljava/lang/Object;
.source "TextPieceTable.java"

# interfaces
.implements Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hwpf/model/TextPieceTable$FCComparator;
    }
.end annotation


# instance fields
.field _cpMin:I

.field protected _textPieces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field

.field protected _textPiecesFCOrder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>([B[BIII)V
    .locals 9

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 7
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 8
    invoke-static {}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getSizeInBytes()I

    move-result v1

    invoke-direct {v0, p2, p3, p4, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    move-result p2

    .line 10
    new-array p3, p2, [Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    const/4 p4, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    move-result-object v2

    .line 12
    new-instance v3, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v2

    invoke-direct {v3, v2, p4}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;-><init>([BI)V

    aput-object v3, p3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13
    :cond_0
    aget-object v1, p3, p4

    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v1

    sub-int/2addr v1, p5

    iput v1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_cpMin:I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p2, :cond_2

    .line 14
    aget-object v2, p3, v1

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v2

    sub-int/2addr v2, p5

    .line 15
    iget v3, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_cpMin:I

    if-ge v2, v3, :cond_1

    .line 16
    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_cpMin:I

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    if-ge p4, p2, :cond_4

    .line 17
    aget-object p5, p3, p4

    invoke-virtual {p5}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v5

    .line 18
    invoke-virtual {v0, p4}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    move-result-object p5

    .line 19
    invoke-virtual {p5}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v2

    .line 20
    invoke-virtual {p5}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    .line 21
    aget-object p5, p3, p4

    invoke-virtual {p5}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->isUnicode()Z

    move-result p5

    if-eqz p5, :cond_3

    const/4 p5, 0x2

    goto :goto_3

    :cond_3
    const/4 p5, 0x1

    :goto_3
    sub-int v1, v3, v2

    mul-int v6, v1, p5

    .line 22
    iget-object p5, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    new-instance v8, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    aget-object v7, p3, p4

    move-object v1, v8

    move-object v4, p1

    invoke-direct/range {v1 .. v7}, Lcom/intsig/office/fc/hwpf/model/TextPiece;-><init>(II[BIILcom/intsig/office/fc/hwpf/model/PieceDescriptor;)V

    invoke-virtual {p5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p4, p4, 0x1

    goto :goto_2

    .line 23
    :cond_4
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 24
    new-instance p1, Ljava/util/ArrayList;

    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 25
    new-instance p2, Lcom/intsig/office/fc/hwpf/model/TextPieceTable$FCComparator;

    const/4 p3, 0x0

    invoke-direct {p2, p3}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable$FCComparator;-><init>(Lcom/intsig/office/fc/hwpf/model/〇o00〇〇Oo;)V

    invoke-static {p1, p2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method


# virtual methods
.method public add(Lcom/intsig/office/fc/hwpf/model/TextPiece;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable$FCComparator;

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable$FCComparator;-><init>(Lcom/intsig/office/fc/hwpf/model/〇o00〇〇Oo;)V

    .line 22
    .line 23
    .line 24
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public adjustForInsert(II)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    add-int/2addr v2, p2

    .line 20
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 21
    .line 22
    .line 23
    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 24
    .line 25
    if-ge p1, v0, :cond_0

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    add-int/2addr v2, p2

    .line 40
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    add-int/2addr v2, p2

    .line 48
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    return p2
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    .line 2
    .line 3
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/4 v2, 0x0

    .line 16
    if-ne v0, v1, :cond_2

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    :goto_0
    if-ge v1, v0, :cond_1

    .line 20
    .line 21
    iget-object v3, p1, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    check-cast v3, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 28
    .line 29
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 30
    .line 31
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->equals(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-nez v3, :cond_0

    .line 40
    .line 41
    return v2

    .line 42
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    const/4 p1, 0x1

    .line 46
    return p1

    .line 47
    :cond_2
    return v2
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getByteIndex(I)I
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_4

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    const/4 v4, 0x2

    .line 25
    const/4 v5, 0x1

    .line 26
    if-lt p1, v3, :cond_2

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    sub-int/2addr v3, v6

    .line 45
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->isUnicode()Z

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    if-eqz v6, :cond_1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    const/4 v4, 0x1

    .line 53
    :goto_0
    mul-int v3, v3, v4

    .line 54
    .line 55
    add-int/2addr v1, v3

    .line 56
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    if-ne p1, v2, :cond_0

    .line 61
    .line 62
    goto :goto_2

    .line 63
    :cond_2
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 64
    .line 65
    .line 66
    move-result v3

    .line 67
    if-ge p1, v3, :cond_0

    .line 68
    .line 69
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    sub-int/2addr p1, v0

    .line 74
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->isUnicode()Z

    .line 83
    .line 84
    .line 85
    move-result v1

    .line 86
    if-eqz v1, :cond_3

    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_3
    const/4 v4, 0x1

    .line 90
    :goto_1
    mul-int p1, p1, v4

    .line 91
    .line 92
    add-int v1, v0, p1

    .line 93
    .line 94
    :cond_4
    :goto_2
    return v1
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getCharIndex(I)I
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->getCharIndex(II)I

    move-result p1

    return p1
.end method

.method public getCharIndex(II)I
    .locals 7

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->lookIndexForward(I)I

    move-result p1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 4
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v3

    .line 5
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->bytesLength()I

    move-result v4

    add-int v5, v3, v4

    if-lt p1, v3, :cond_3

    if-le p1, v5, :cond_1

    goto :goto_0

    :cond_1
    if-le p1, v3, :cond_2

    if-ge p1, v5, :cond_2

    sub-int v4, p1, v3

    goto :goto_0

    :cond_2
    sub-int v6, v5, p1

    sub-int/2addr v4, v6

    .line 6
    :cond_3
    :goto_0
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->isUnicode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 7
    div-int/lit8 v4, v4, 0x2

    :cond_4
    add-int/2addr v1, v4

    if-lt p1, v3, :cond_0

    if-gt p1, v5, :cond_0

    if-lt v1, p2, :cond_0

    :cond_5
    return v1
.end method

.method public getCpMin()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_cpMin:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getText()Ljava/lang/StringBuilder;
    .locals 6

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    if-eqz v2, :cond_0

    .line 20
    .line 21
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 26
    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getStringBuilder()Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 36
    .line 37
    .line 38
    move-result v4

    .line 39
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 43
    .line 44
    .line 45
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 46
    .line 47
    .line 48
    move-result v5

    .line 49
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    add-int/2addr v2, v4

    .line 54
    invoke-virtual {v0, v5, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_0
    return-object v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getTextPieces()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isIndexInTable(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 2
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v3

    .line 3
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->bytesLength()I

    move-result v1

    add-int/2addr v1, v3

    if-le p1, v1, :cond_0

    goto :goto_0

    :cond_0
    if-le v3, p1, :cond_1

    return v2

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    return v2
.end method

.method isIndexInTable(II)Z
    .locals 5

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 5
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    move-result v3

    .line 6
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->bytesLength()I

    move-result v4

    add-int/2addr v4, v3

    if-lt p1, v4, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    invoke-static {p1, v3}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->bytesLength()I

    move-result v0

    add-int/2addr v3, v0

    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result p2

    if-lt p1, p2, :cond_1

    return v2

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    return v2
.end method

.method public lookIndexBackward(I)I
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_1

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->bytesLength()I

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    add-int/2addr v4, v3

    .line 33
    if-le p1, v4, :cond_0

    .line 34
    .line 35
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->bytesLength()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    add-int/2addr v1, v3

    .line 40
    goto :goto_0

    .line 41
    :cond_0
    if-le v3, p1, :cond_1

    .line 42
    .line 43
    move p1, v1

    .line 44
    :cond_1
    return p1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public lookIndexForward(I)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPiecesFCOrder:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getFilePosition()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->bytesLength()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    add-int/2addr v1, v2

    .line 32
    if-lt p1, v1, :cond_0

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    if-le v2, p1, :cond_1

    .line 36
    .line 37
    move p1, v2

    .line 38
    :cond_1
    return p1
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)[B
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getSizeInBytes()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>(I)V

    .line 8
    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    const/4 v2, 0x0

    .line 17
    :goto_0
    if-ge v2, v1, :cond_1

    .line 18
    .line 19
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->_textPieces:Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    check-cast v3, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 26
    .line 27
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    rem-int/lit16 v5, v5, 0x200

    .line 36
    .line 37
    if-eqz v5, :cond_0

    .line 38
    .line 39
    rsub-int v5, v5, 0x200

    .line 40
    .line 41
    new-array v5, v5, [B

    .line 42
    .line 43
    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V

    .line 44
    .line 45
    .line 46
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    invoke-virtual {v4, v5}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->setFilePosition(I)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getRawBytes()[B

    .line 54
    .line 55
    .line 56
    move-result-object v5

    .line 57
    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 61
    .line 62
    .line 63
    move-result v5

    .line 64
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 65
    .line 66
    .line 67
    move-result v3

    .line 68
    new-instance v6, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 69
    .line 70
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->toByteArray()[B

    .line 71
    .line 72
    .line 73
    move-result-object v4

    .line 74
    invoke-direct {v6, v5, v3, v4}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->addProperty(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)V

    .line 78
    .line 79
    .line 80
    add-int/lit8 v2, v2, 0x1

    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->toByteArray()[B

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    return-object p1
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

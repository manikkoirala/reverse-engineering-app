.class public final Lcom/intsig/office/fc/hwpf/model/PlexOfCps;
.super Ljava/lang/Object;
.source "PlexOfCps.java"


# instance fields
.field private _cbStruct:I

.field private _iMac:I

.field private _offset:I

.field private _props:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 3
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_cbStruct:I

    return-void
.end method

.method public constructor <init>([BIII)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    add-int/lit8 p3, p3, -0x4

    add-int/lit8 v0, p4, 0x4

    .line 5
    div-int/2addr p3, v0

    iput p3, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_iMac:I

    .line 6
    iput p4, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 7
    new-instance p3, Ljava/util/ArrayList;

    iget p4, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_iMac:I

    invoke-direct {p3, p4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p3, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    const/4 p3, 0x0

    .line 8
    :goto_0
    iget p4, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_iMac:I

    if-ge p3, p4, :cond_0

    .line 9
    iget-object p4, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-direct {p0, p3, p1, p2}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I[BI)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getProperty(I[BI)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;
    .locals 4

    mul-int/lit8 v0, p1, 0x4

    add-int/2addr v0, p3

    .line 2
    invoke-static {p2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v1, p3

    .line 3
    invoke-static {p2, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v1

    .line 4
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_cbStruct:I

    new-array v2, v2, [B

    .line 5
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getStructOffset(I)I

    move-result p1

    add-int/2addr p3, p1

    const/4 p1, 0x0

    iget v3, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_cbStruct:I

    invoke-static {p2, p3, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6
    new-instance p1, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    invoke-direct {p1, v0, v1, v2}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    return-object p1
.end method

.method private getStructOffset(I)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_iMac:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    mul-int/lit8 v0, v0, 0x4

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 8
    .line 9
    mul-int v1, v1, p1

    .line 10
    .line 11
    add-int/2addr v0, v1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public addProperty(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    return-object p1
.end method

.method public length()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_iMac:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toByteArray()[B
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit8 v1, v0, 0x1

    .line 8
    .line 9
    mul-int/lit8 v1, v1, 0x4

    .line 10
    .line 11
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 12
    .line 13
    mul-int v2, v2, v0

    .line 14
    .line 15
    add-int/2addr v2, v1

    .line 16
    new-array v2, v2, [B

    .line 17
    .line 18
    const/4 v3, 0x0

    .line 19
    const/4 v4, 0x0

    .line 20
    const/4 v5, 0x0

    .line 21
    :goto_0
    if-ge v5, v0, :cond_0

    .line 22
    .line 23
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 24
    .line 25
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    check-cast v3, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 30
    .line 31
    mul-int/lit8 v6, v5, 0x4

    .line 32
    .line 33
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 34
    .line 35
    .line 36
    move-result v7

    .line 37
    invoke-static {v2, v6, v7}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;->getBytes()[B

    .line 41
    .line 42
    .line 43
    move-result-object v6

    .line 44
    iget v7, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 45
    .line 46
    mul-int v8, v5, v7

    .line 47
    .line 48
    add-int/2addr v8, v1

    .line 49
    invoke-static {v6, v4, v2, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50
    .line 51
    .line 52
    add-int/lit8 v5, v5, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    mul-int/lit8 v0, v0, 0x4

    .line 56
    .line 57
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    invoke-static {v2, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 62
    .line 63
    .line 64
    return-object v2
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method toPropertiesArray()[Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_props:Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    new-array v1, v1, [Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    check-cast v0, [Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 25
    .line 26
    return-object v0

    .line 27
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 28
    new-array v0, v0, [Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 29
    .line 30
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "PLCF (cbStruct: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_cbStruct:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, "; iMac: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->_iMac:I

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, ")"

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

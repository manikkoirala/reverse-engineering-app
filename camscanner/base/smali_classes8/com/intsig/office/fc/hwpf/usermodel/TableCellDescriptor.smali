.class public final Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;
.super Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;
.source "TableCellDescriptor.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SIZE:I = 0x14


# instance fields
.field protected field_x_unused:S


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcTop(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 10
    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 13
    .line 14
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcLeft(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 18
    .line 19
    .line 20
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 21
    .line 22
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcBottom(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 26
    .line 27
    .line 28
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 29
    .line 30
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcRight(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public static convertBytesToTC([BI)Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;->fillFields([BI)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->getBrcTop()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcTop(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->getBrcLeft()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcLeft(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->getBrcBottom()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcBottom(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->getBrcRight()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 55
    .line 56
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcRight(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 57
    .line 58
    .line 59
    return-object v0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method protected fillFields([BI)V
    .locals 2

    .line 1
    add-int/lit8 v0, p2, 0x0

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->field_1_rgf:S

    .line 8
    .line 9
    add-int/lit8 v0, p2, 0x2

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;->field_x_unused:S

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 18
    .line 19
    add-int/lit8 v1, p2, 0x4

    .line 20
    .line 21
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcTop(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 25
    .line 26
    .line 27
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 28
    .line 29
    add-int/lit8 v1, p2, 0x8

    .line 30
    .line 31
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcLeft(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 35
    .line 36
    .line 37
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 38
    .line 39
    add-int/lit8 v1, p2, 0xc

    .line 40
    .line 41
    invoke-direct {v0, p1, v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcBottom(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 45
    .line 46
    .line 47
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 48
    .line 49
    add-int/lit8 p2, p2, 0x10

    .line 50
    .line 51
    invoke-direct {v0, p1, p2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setBrcRight(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public serialize([BI)V
    .locals 2

    .line 1
    add-int/lit8 v0, p2, 0x0

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->field_1_rgf:S

    .line 4
    .line 5
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 6
    .line 7
    .line 8
    add-int/lit8 v0, p2, 0x2

    .line 9
    .line 10
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;->field_x_unused:S

    .line 11
    .line 12
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->getBrcTop()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    add-int/lit8 v1, p2, 0x4

    .line 20
    .line 21
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->getBrcLeft()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    add-int/lit8 v1, p2, 0x8

    .line 29
    .line 30
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->getBrcBottom()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    add-int/lit8 v1, p2, 0xc

    .line 38
    .line 39
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->getBrcRight()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    add-int/lit8 p2, p2, 0x10

    .line 47
    .line 48
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->serialize([BI)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

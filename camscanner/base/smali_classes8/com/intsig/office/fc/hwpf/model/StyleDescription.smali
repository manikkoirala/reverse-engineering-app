.class public final Lcom/intsig/office/fc/hwpf/model/StyleDescription;
.super Ljava/lang/Object;
.source "StyleDescription.java"

# interfaces
.implements Lcom/intsig/office/fc/hwpf/model/HDFType;


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final CHARACTER_STYLE:I = 0x2

.field private static final PARAGRAPH_STYLE:I = 0x1

.field private static _baseStyle:Lcom/intsig/office/fc/util/BitField;

.field private static _fAutoRedef:Lcom/intsig/office/fc/util/BitField;

.field private static _fHasUpe:Lcom/intsig/office/fc/util/BitField;

.field private static _fHidden:Lcom/intsig/office/fc/util/BitField;

.field private static _fInvalHeight:Lcom/intsig/office/fc/util/BitField;

.field private static _fMassCopy:Lcom/intsig/office/fc/util/BitField;

.field private static _fScratch:Lcom/intsig/office/fc/util/BitField;

.field private static _nextStyle:Lcom/intsig/office/fc/util/BitField;

.field private static _numUPX:Lcom/intsig/office/fc/util/BitField;

.field private static _sti:Lcom/intsig/office/fc/util/BitField;

.field private static _styleTypeCode:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field private _baseLength:I

.field private _bchUpe:S

.field _chp:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

.field private _infoShort:S

.field private _infoShort2:S

.field private _infoShort3:S

.field private _infoShort4:S

.field private _istd:I

.field _name:Ljava/lang/String;

.field _pap:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

.field _upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    const/16 v0, 0xfff

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_sti:Lcom/intsig/office/fc/util/BitField;

    .line 8
    .line 9
    const/16 v0, 0x1000

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_fScratch:Lcom/intsig/office/fc/util/BitField;

    .line 16
    .line 17
    const/16 v0, 0x2000

    .line 18
    .line 19
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_fInvalHeight:Lcom/intsig/office/fc/util/BitField;

    .line 24
    .line 25
    const/16 v0, 0x4000

    .line 26
    .line 27
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_fHasUpe:Lcom/intsig/office/fc/util/BitField;

    .line 32
    .line 33
    const v0, 0x8000

    .line 34
    .line 35
    .line 36
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_fMassCopy:Lcom/intsig/office/fc/util/BitField;

    .line 41
    .line 42
    const/16 v0, 0xf

    .line 43
    .line 44
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    sput-object v1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_styleTypeCode:Lcom/intsig/office/fc/util/BitField;

    .line 49
    .line 50
    const v1, 0xfff0

    .line 51
    .line 52
    .line 53
    invoke-static {v1}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    sput-object v2, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_baseStyle:Lcom/intsig/office/fc/util/BitField;

    .line 58
    .line 59
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_numUPX:Lcom/intsig/office/fc/util/BitField;

    .line 64
    .line 65
    invoke-static {v1}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_nextStyle:Lcom/intsig/office/fc/util/BitField;

    .line 70
    .line 71
    const/4 v0, 0x1

    .line 72
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_fAutoRedef:Lcom/intsig/office/fc/util/BitField;

    .line 77
    .line 78
    const/4 v0, 0x2

    .line 79
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_fHidden:Lcom/intsig/office/fc/util/BitField;

    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>([BIIZ)V
    .locals 7

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_baseLength:I

    add-int/2addr p2, p3

    .line 4
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort:S

    const/4 v0, 0x2

    add-int/2addr p3, v0

    .line 5
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v1

    iput-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort2:S

    add-int/2addr p3, v0

    .line 6
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v1

    iput-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort3:S

    add-int/2addr p3, v0

    .line 7
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v1

    iput-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_bchUpe:S

    add-int/2addr p3, v0

    .line 8
    invoke-static {p1, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result p3

    iput-short p3, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort4:S

    const/4 p3, 0x1

    if-eqz p4, :cond_0

    .line 9
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result p4

    add-int/lit8 p2, p2, 0x2

    const/4 v1, 0x2

    goto :goto_0

    .line 10
    :cond_0
    aget-byte p4, p1, p2

    const/4 v1, 0x1

    .line 11
    :goto_0
    :try_start_0
    new-instance v2, Ljava/lang/String;

    mul-int v3, p4, v1

    const-string v4, "UTF-16LE"

    invoke-direct {v2, p1, p2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_name:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    add-int/2addr p4, p3

    mul-int p4, p4, v1

    add-int/2addr p4, p2

    .line 12
    sget-object p2, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_numUPX:Lcom/intsig/office/fc/util/BitField;

    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort3:S

    invoke-virtual {p2, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    move-result p2

    .line 13
    new-array v1, p2, [Lcom/intsig/office/fc/hwpf/model/UPX;

    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, p2, :cond_2

    .line 14
    invoke-static {p1, p4}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v3

    add-int/2addr p4, v0

    .line 15
    new-array v4, v3, [B

    .line 16
    invoke-static {p1, p4, v4, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 17
    iget-object v5, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    new-instance v6, Lcom/intsig/office/fc/hwpf/model/UPX;

    invoke-direct {v6, v4}, Lcom/intsig/office/fc/hwpf/model/UPX;-><init>([B)V

    aput-object v6, v5, v2

    add-int/2addr p4, v3

    .line 18
    rem-int/2addr v3, v0

    if-ne v3, p3, :cond_1

    add-int/lit8 p4, p4, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;

    .line 2
    .line 3
    iget-short v0, p1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort:S

    .line 4
    .line 5
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort:S

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-ne v0, v1, :cond_1

    .line 9
    .line 10
    iget-short v0, p1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort2:S

    .line 11
    .line 12
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort2:S

    .line 13
    .line 14
    if-ne v0, v1, :cond_1

    .line 15
    .line 16
    iget-short v0, p1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort3:S

    .line 17
    .line 18
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort3:S

    .line 19
    .line 20
    if-ne v0, v1, :cond_1

    .line 21
    .line 22
    iget-short v0, p1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_bchUpe:S

    .line 23
    .line 24
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_bchUpe:S

    .line 25
    .line 26
    if-ne v0, v1, :cond_1

    .line 27
    .line 28
    iget-short v0, p1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort4:S

    .line 29
    .line 30
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort4:S

    .line 31
    .line 32
    if-ne v0, v1, :cond_1

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    .line 35
    .line 36
    iget-object v1, p1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-eqz v0, :cond_1

    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 45
    .line 46
    iget-object p1, p1, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 47
    .line 48
    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    if-nez p1, :cond_0

    .line 53
    .line 54
    return v2

    .line 55
    :cond_0
    const/4 p1, 0x1

    .line 56
    return p1

    .line 57
    :cond_1
    return v2
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getBaseStyle()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_baseStyle:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort2:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCHP()Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_chp:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCHPX()[B
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_styleTypeCode:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort2:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-eq v0, v2, :cond_1

    .line 12
    .line 13
    const/4 v2, 0x2

    .line 14
    if-eq v0, v2, :cond_0

    .line 15
    .line 16
    return-object v1

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    aget-object v0, v0, v1

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/UPX;->getUPX()[B

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    return-object v0

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 28
    .line 29
    array-length v3, v0

    .line 30
    if-le v3, v2, :cond_2

    .line 31
    .line 32
    aget-object v0, v0, v2

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/UPX;->getUPX()[B

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    return-object v0

    .line 39
    :cond_2
    return-object v1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPAP()Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_pap:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPAPX()[B
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_styleTypeCode:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort2:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-eq v0, v1, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    return-object v0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    aget-object v0, v0, v1

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/UPX;->getUPX()[B

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method setCHP(Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_chp:Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method setPAP(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_pap:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toByteArray()[B
    .locals 7

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_baseLength:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    add-int/2addr v0, v1

    .line 5
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    .line 6
    .line 7
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    const/4 v3, 0x1

    .line 12
    add-int/2addr v2, v3

    .line 13
    mul-int/lit8 v2, v2, 0x2

    .line 14
    .line 15
    add-int/2addr v0, v2

    .line 16
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 17
    .line 18
    const/4 v4, 0x0

    .line 19
    aget-object v2, v2, v4

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/UPX;->size()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    add-int/2addr v2, v1

    .line 26
    add-int/2addr v0, v2

    .line 27
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 28
    .line 29
    array-length v5, v2

    .line 30
    if-ge v3, v5, :cond_0

    .line 31
    .line 32
    add-int/lit8 v5, v3, -0x1

    .line 33
    .line 34
    aget-object v2, v2, v5

    .line 35
    .line 36
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/UPX;->size()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    rem-int/2addr v2, v1

    .line 41
    add-int/2addr v0, v2

    .line 42
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 43
    .line 44
    aget-object v2, v2, v3

    .line 45
    .line 46
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/UPX;->size()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    add-int/2addr v2, v1

    .line 51
    add-int/2addr v0, v2

    .line 52
    add-int/lit8 v3, v3, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    new-array v0, v0, [B

    .line 56
    .line 57
    iget-short v2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort:S

    .line 58
    .line 59
    invoke-static {v0, v4, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 60
    .line 61
    .line 62
    iget-short v2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort2:S

    .line 63
    .line 64
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 65
    .line 66
    .line 67
    const/4 v2, 0x4

    .line 68
    iget-short v3, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort3:S

    .line 69
    .line 70
    invoke-static {v0, v2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 71
    .line 72
    .line 73
    const/4 v2, 0x6

    .line 74
    iget-short v3, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_bchUpe:S

    .line 75
    .line 76
    invoke-static {v0, v2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 77
    .line 78
    .line 79
    const/16 v2, 0x8

    .line 80
    .line 81
    iget-short v3, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_infoShort4:S

    .line 82
    .line 83
    invoke-static {v0, v2, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 84
    .line 85
    .line 86
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_baseLength:I

    .line 87
    .line 88
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_name:Ljava/lang/String;

    .line 89
    .line 90
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    .line 91
    .line 92
    .line 93
    move-result-object v3

    .line 94
    iget v5, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_baseLength:I

    .line 95
    .line 96
    array-length v6, v3

    .line 97
    int-to-short v6, v6

    .line 98
    invoke-static {v0, v5, v6}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 99
    .line 100
    .line 101
    add-int/2addr v2, v1

    .line 102
    const/4 v5, 0x0

    .line 103
    :goto_1
    array-length v6, v3

    .line 104
    if-ge v5, v6, :cond_1

    .line 105
    .line 106
    aget-char v6, v3, v5

    .line 107
    .line 108
    int-to-short v6, v6

    .line 109
    invoke-static {v0, v2, v6}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 110
    .line 111
    .line 112
    add-int/lit8 v2, v2, 0x2

    .line 113
    .line 114
    add-int/lit8 v5, v5, 0x1

    .line 115
    .line 116
    goto :goto_1

    .line 117
    :cond_1
    add-int/2addr v2, v1

    .line 118
    const/4 v3, 0x0

    .line 119
    :goto_2
    iget-object v5, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 120
    .line 121
    array-length v6, v5

    .line 122
    if-ge v3, v6, :cond_2

    .line 123
    .line 124
    aget-object v5, v5, v3

    .line 125
    .line 126
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/UPX;->size()I

    .line 127
    .line 128
    .line 129
    move-result v5

    .line 130
    int-to-short v5, v5

    .line 131
    invoke-static {v0, v2, v5}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 132
    .line 133
    .line 134
    add-int/2addr v2, v1

    .line 135
    iget-object v6, p0, Lcom/intsig/office/fc/hwpf/model/StyleDescription;->_upxs:[Lcom/intsig/office/fc/hwpf/model/UPX;

    .line 136
    .line 137
    aget-object v6, v6, v3

    .line 138
    .line 139
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/model/UPX;->getUPX()[B

    .line 140
    .line 141
    .line 142
    move-result-object v6

    .line 143
    invoke-static {v6, v4, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    .line 145
    .line 146
    rem-int/lit8 v6, v5, 0x2

    .line 147
    .line 148
    add-int/2addr v5, v6

    .line 149
    add-int/2addr v2, v5

    .line 150
    add-int/lit8 v3, v3, 0x1

    .line 151
    .line 152
    goto :goto_2

    .line 153
    :cond_2
    return-object v0
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.class public final Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;
.super Ljava/lang/Object;
.source "ComplexFileTable.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final GRPPRL_TYPE:B = 0x1t

.field private static final TEXT_PIECE_TABLE_TYPE:B = 0x2t


# instance fields
.field private _grpprls:[Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

.field protected _tpt:Lcom/intsig/office/fc/hwpf/model/TextPieceTable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->_tpt:Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    return-void
.end method

.method public constructor <init>([B[BII)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 5
    :goto_0
    aget-byte v1, p2, p3

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    add-int/lit8 p3, p3, 0x1

    .line 6
    invoke-static {p2, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v1

    add-int/2addr p3, v2

    .line 7
    invoke-static {p2, p3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v2

    add-int/2addr p3, v1

    .line 8
    new-instance v1, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v3}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BZI)V

    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 10
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->_grpprls:[Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 11
    aget-byte v0, p2, p3

    if-ne v0, v2, :cond_1

    add-int/2addr p3, v3

    .line 12
    invoke-static {p2, p3}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v4

    add-int/lit8 v3, p3, 0x4

    .line 13
    new-instance p3, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;-><init>([B[BIII)V

    iput-object p3, p0, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->_tpt:Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    return-void

    .line 14
    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "The text piece table is corrupted"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public getGrpprls()[Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->_grpprls:[Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTextPieceTable()Lcom/intsig/office/fc/hwpf/model/TextPieceTable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->_tpt:Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

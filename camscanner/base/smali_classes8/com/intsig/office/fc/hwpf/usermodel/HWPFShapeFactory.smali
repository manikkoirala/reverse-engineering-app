.class public final Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeFactory;
.super Ljava/lang/Object;
.source "HWPFShapeFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static createShape(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ddf/EscherRecord;->getRecordId()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, -0xffd

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeFactory;->createShapeGroup(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;

    .line 10
    .line 11
    .line 12
    move-result-object p0

    .line 13
    return-object p0

    .line 14
    :cond_0
    invoke-static {p0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeFactory;->createSimpeShape(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    return-object p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static createShapeGroup(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChild(I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    check-cast v1, Lcom/intsig/office/fc/ddf/EscherContainerRecord;

    .line 7
    .line 8
    const/16 v2, -0xede

    .line 9
    .line 10
    invoke-static {v1, v2}, Lcom/intsig/office/fc/ShapeKit;->getEscherChild(Lcom/intsig/office/fc/ddf/EscherContainerRecord;I)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    :try_start_0
    new-instance v2, Lcom/intsig/office/fc/ddf/EscherPropertyFactory;

    .line 17
    .line 18
    invoke-direct {v2}, Lcom/intsig/office/fc/ddf/EscherPropertyFactory;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->serialize()[B

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v1}, Lcom/intsig/office/fc/ddf/EscherRecord;->getInstance()S

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    const/16 v4, 0x8

    .line 30
    .line 31
    invoke-virtual {v2, v3, v4, v1}, Lcom/intsig/office/fc/ddf/EscherPropertyFactory;->createProperties([BIS)Ljava/util/List;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherProperty;->getPropertyNumber()S

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    const/16 v2, 0x39f

    .line 46
    .line 47
    if-ne v1, v2, :cond_0

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/office/fc/ddf/EscherSimpleProperty;->getPropertyValue()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    const/4 v1, 0x1

    .line 54
    if-ne v0, v1, :cond_0

    .line 55
    .line 56
    const/4 p0, 0x0

    .line 57
    goto :goto_1

    .line 58
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;

    .line 59
    .line 60
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :catch_0
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;

    .line 65
    .line 66
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)V

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_1
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;

    .line 71
    .line 72
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFShapeGroup;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)V

    .line 73
    .line 74
    .line 75
    :goto_0
    move-object p0, v0

    .line 76
    :goto_1
    return-object p0
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static createSimpeShape(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;
    .locals 1

    .line 1
    const/16 v0, -0xff6

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/ddf/EscherContainerRecord;->getChildById(S)Lcom/intsig/office/fc/ddf/EscherRecord;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/ddf/EscherSpRecord;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;

    .line 12
    .line 13
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFAutoShape;-><init>(Lcom/intsig/office/fc/ddf/EscherContainerRecord;Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;)V

    .line 14
    .line 15
    .line 16
    return-object v0

    .line 17
    :cond_0
    const/4 p0, 0x0

    .line 18
    return-object p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.class public Lcom/intsig/office/fc/hwpf/model/PAPBinTable;
.super Ljava/lang/Object;
.source "PAPBinTable.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected _paragraphs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>([B[B[BIIILcom/intsig/office/fc/hwpf/model/TextPieceTable;)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p7

    .line 3
    invoke-direct/range {v0 .. v6}, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;-><init>([B[B[BIILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    return-void
.end method

.method public constructor <init>([B[B[BIILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V
    .locals 6

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    const/4 v1, 0x4

    invoke-direct {v0, p2, p4, p5, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    move-result p2

    const/4 p4, 0x0

    const/4 p5, 0x0

    :goto_0
    if-ge p5, p2, :cond_2

    .line 9
    invoke-virtual {v0, p5}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    move-result-object v1

    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([B)I

    move-result v1

    mul-int/lit16 v1, v1, 0x200

    .line 11
    new-instance v2, Lcom/intsig/office/fc/hwpf/model/PAPFormattedDiskPage;

    invoke-direct {v2, p1, p3, v1, p6}, Lcom/intsig/office/fc/hwpf/model/PAPFormattedDiskPage;-><init>([B[BILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    .line 12
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_1

    .line 13
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hwpf/model/PAPFormattedDiskPage;->getPAPX(I)Lcom/intsig/office/fc/hwpf/model/PAPX;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 14
    iget-object v5, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public adjustForDelete(III)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int v1, p2, p3

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 16
    .line 17
    move v3, p1

    .line 18
    :goto_0
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-ge v2, v1, :cond_0

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 25
    .line 26
    add-int/lit8 v3, v3, 0x1

    .line 27
    .line 28
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    if-ne p1, v3, :cond_1

    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 38
    .line 39
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    sub-int/2addr v2, v1

    .line 50
    add-int/2addr v2, p2

    .line 51
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 52
    .line 53
    .line 54
    goto :goto_2

    .line 55
    :cond_1
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 56
    .line 57
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 62
    .line 63
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 64
    .line 65
    .line 66
    :goto_1
    add-int/lit8 p1, p1, 0x1

    .line 67
    .line 68
    if-ge p1, v3, :cond_2

    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 71
    .line 72
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 77
    .line 78
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 82
    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 86
    .line 87
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    sub-int/2addr v2, v1

    .line 98
    add-int/2addr v2, p2

    .line 99
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 100
    .line 101
    .line 102
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 103
    .line 104
    if-ge v3, v0, :cond_3

    .line 105
    .line 106
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 107
    .line 108
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 113
    .line 114
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 115
    .line 116
    .line 117
    move-result p2

    .line 118
    sub-int/2addr p2, p3

    .line 119
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 123
    .line 124
    .line 125
    move-result p2

    .line 126
    sub-int/2addr p2, p3

    .line 127
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 128
    .line 129
    .line 130
    goto :goto_2

    .line 131
    :cond_3
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public adjustForInsert(II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    add-int/2addr v2, p2

    .line 20
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 21
    .line 22
    .line 23
    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 24
    .line 25
    if-ge p1, v0, :cond_0

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    add-int/2addr v2, p2

    .line 40
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    add-int/2addr v2, p2

    .line 48
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getParagraphs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/PAPX;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public insert(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1, v1, p3}, Lcom/intsig/office/fc/hwpf/model/PAPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 11
    .line 12
    .line 13
    iget-object p3, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    .line 16
    .line 17
    .line 18
    move-result p3

    .line 19
    if-ne p1, p3, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_0
    iget-object p3, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object p3

    .line 33
    check-cast p3, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 34
    .line 35
    if-eqz p3, :cond_1

    .line 36
    .line 37
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-ge v2, p2, :cond_1

    .line 42
    .line 43
    :try_start_0
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getSprmBuf()Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->clone()Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    check-cast v2, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :catch_0
    move-exception v2

    .line 55
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 56
    .line 57
    .line 58
    const/4 v2, 0x0

    .line 59
    :goto_0
    new-instance v3, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 60
    .line 61
    invoke-direct {v3, v1, v1, v2}, Lcom/intsig/office/fc/hwpf/model/PAPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v3, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    invoke-virtual {v3, v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {p3, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 75
    .line 76
    .line 77
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 78
    .line 79
    add-int/lit8 p3, p1, 0x1

    .line 80
    .line 81
    invoke-virtual {p2, p3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 82
    .line 83
    .line 84
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 85
    .line 86
    add-int/lit8 p1, p1, 0x2

    .line 87
    .line 88
    invoke-virtual {p2, p1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 89
    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_1
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 93
    .line 94
    invoke-virtual {p2, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 95
    .line 96
    .line 97
    :goto_1
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public rebuild(Ljava/lang/StringBuilder;Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;)V
    .locals 13

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x2

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x1

    .line 7
    if-eqz p2, :cond_5

    .line 8
    .line 9
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->getGrpprls()[Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->getTextPieceTable()Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result v4

    .line 29
    if-eqz v4, :cond_5

    .line 30
    .line 31
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    check-cast v4, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 36
    .line 37
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getPrm()Lcom/intsig/office/fc/hwpf/model/PropertyModifier;

    .line 42
    .line 43
    .line 44
    move-result-object v5

    .line 45
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/PropertyModifier;->isComplex()Z

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    if-nez v6, :cond_1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/PropertyModifier;->getIgrpprl()S

    .line 53
    .line 54
    .line 55
    move-result v5

    .line 56
    if-ltz v5, :cond_0

    .line 57
    .line 58
    array-length v6, v3

    .line 59
    if-lt v5, v6, :cond_2

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_2
    aget-object v5, v3, v5

    .line 63
    .line 64
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->iterator()Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    .line 65
    .line 66
    .line 67
    move-result-object v6

    .line 68
    :cond_3
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    .line 69
    .line 70
    .line 71
    move-result v7

    .line 72
    if-eqz v7, :cond_4

    .line 73
    .line 74
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 75
    .line 76
    .line 77
    move-result-object v7

    .line 78
    invoke-virtual {v7}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    .line 79
    .line 80
    .line 81
    move-result v7

    .line 82
    if-ne v7, v2, :cond_3

    .line 83
    .line 84
    const/4 v6, 0x1

    .line 85
    goto :goto_1

    .line 86
    :cond_4
    const/4 v6, 0x0

    .line 87
    :goto_1
    if-eqz v6, :cond_0

    .line 88
    .line 89
    new-instance v6, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 90
    .line 91
    invoke-direct {v6, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->toByteArray()[B

    .line 95
    .line 96
    .line 97
    move-result-object v5

    .line 98
    invoke-virtual {v6, v5}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->append([B)V

    .line 99
    .line 100
    .line 101
    new-instance v5, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 102
    .line 103
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 104
    .line 105
    .line 106
    move-result v7

    .line 107
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    invoke-direct {v5, v7, v4, v6}, Lcom/intsig/office/fc/hwpf/model/PAPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 112
    .line 113
    .line 114
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 115
    .line 116
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_5
    new-instance p2, Ljava/util/ArrayList;

    .line 121
    .line 122
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 123
    .line 124
    invoke-direct {p2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 125
    .line 126
    .line 127
    sget-object v3, Lcom/intsig/office/fc/hwpf/model/PropertyNode$EndComparator;->instance:Lcom/intsig/office/fc/hwpf/model/PropertyNode$EndComparator;

    .line 128
    .line 129
    invoke-static {p2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 130
    .line 131
    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 133
    .line 134
    .line 135
    new-instance v3, Ljava/util/IdentityHashMap;

    .line 136
    .line 137
    invoke-direct {v3}, Ljava/util/IdentityHashMap;-><init>()V

    .line 138
    .line 139
    .line 140
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 141
    .line 142
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 143
    .line 144
    .line 145
    move-result-object v4

    .line 146
    const/4 v5, 0x0

    .line 147
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 148
    .line 149
    .line 150
    move-result v6

    .line 151
    if-eqz v6, :cond_6

    .line 152
    .line 153
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    move-result-object v6

    .line 157
    check-cast v6, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 158
    .line 159
    add-int/lit8 v7, v5, 0x1

    .line 160
    .line 161
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 162
    .line 163
    .line 164
    move-result-object v5

    .line 165
    invoke-interface {v3, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    .line 167
    .line 168
    move v5, v7

    .line 169
    goto :goto_2

    .line 170
    :cond_6
    new-instance v4, Lcom/intsig/office/fc/hwpf/model/PAPBinTable$1;

    .line 171
    .line 172
    invoke-direct {v4, p0, v3}, Lcom/intsig/office/fc/hwpf/model/PAPBinTable$1;-><init>(Lcom/intsig/office/fc/hwpf/model/PAPBinTable;Ljava/util/Map;)V

    .line 173
    .line 174
    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 176
    .line 177
    .line 178
    new-instance v3, Ljava/util/LinkedList;

    .line 179
    .line 180
    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 181
    .line 182
    .line 183
    const/4 v5, 0x0

    .line 184
    const/4 v6, 0x0

    .line 185
    const/4 v7, 0x0

    .line 186
    :goto_3
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    .line 187
    .line 188
    .line 189
    move-result v8

    .line 190
    if-ge v5, v8, :cond_f

    .line 191
    .line 192
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    .line 193
    .line 194
    .line 195
    move-result v8

    .line 196
    const/16 v9, 0xd

    .line 197
    .line 198
    if-eq v8, v9, :cond_7

    .line 199
    .line 200
    const/4 v9, 0x7

    .line 201
    if-eq v8, v9, :cond_7

    .line 202
    .line 203
    const/16 v9, 0xc

    .line 204
    .line 205
    if-eq v8, v9, :cond_7

    .line 206
    .line 207
    goto/16 :goto_8

    .line 208
    .line 209
    :cond_7
    add-int/lit8 v8, v5, 0x1

    .line 210
    .line 211
    new-instance v9, Ljava/util/LinkedList;

    .line 212
    .line 213
    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 214
    .line 215
    .line 216
    move v10, v7

    .line 217
    :goto_4
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 218
    .line 219
    .line 220
    move-result v11

    .line 221
    if-ge v10, v11, :cond_9

    .line 222
    .line 223
    invoke-interface {p2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 224
    .line 225
    .line 226
    move-result-object v11

    .line 227
    check-cast v11, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 228
    .line 229
    invoke-virtual {v11}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 230
    .line 231
    .line 232
    move-result v12

    .line 233
    sub-int/2addr v12, v2

    .line 234
    if-le v12, v5, :cond_8

    .line 235
    .line 236
    move v7, v10

    .line 237
    const/4 v10, 0x1

    .line 238
    goto :goto_5

    .line 239
    :cond_8
    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    .line 241
    .line 242
    add-int/lit8 v10, v10, 0x1

    .line 243
    .line 244
    goto :goto_4

    .line 245
    :cond_9
    const/4 v10, 0x0

    .line 246
    :goto_5
    if-nez v10, :cond_a

    .line 247
    .line 248
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 249
    .line 250
    .line 251
    move-result v7

    .line 252
    sub-int/2addr v7, v2

    .line 253
    :cond_a
    invoke-interface {v9}, Ljava/util/List;->size()I

    .line 254
    .line 255
    .line 256
    move-result v10

    .line 257
    if-nez v10, :cond_b

    .line 258
    .line 259
    new-instance v9, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 260
    .line 261
    new-instance v10, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 262
    .line 263
    invoke-direct {v10, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 264
    .line 265
    .line 266
    invoke-direct {v9, v6, v8, v10}, Lcom/intsig/office/fc/hwpf/model/PAPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 267
    .line 268
    .line 269
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    .line 271
    .line 272
    goto :goto_7

    .line 273
    :cond_b
    invoke-interface {v9}, Ljava/util/List;->size()I

    .line 274
    .line 275
    .line 276
    move-result v10

    .line 277
    if-ne v10, v2, :cond_c

    .line 278
    .line 279
    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 280
    .line 281
    .line 282
    move-result-object v10

    .line 283
    check-cast v10, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 284
    .line 285
    invoke-virtual {v10}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 286
    .line 287
    .line 288
    move-result v11

    .line 289
    if-ne v11, v6, :cond_c

    .line 290
    .line 291
    invoke-virtual {v10}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 292
    .line 293
    .line 294
    move-result v11

    .line 295
    if-ne v11, v8, :cond_c

    .line 296
    .line 297
    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    .line 299
    .line 300
    goto :goto_7

    .line 301
    :cond_c
    invoke-static {v9, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 302
    .line 303
    .line 304
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 305
    .line 306
    .line 307
    move-result-object v9

    .line 308
    const/4 v10, 0x0

    .line 309
    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    .line 310
    .line 311
    .line 312
    move-result v11

    .line 313
    if-eqz v11, :cond_e

    .line 314
    .line 315
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 316
    .line 317
    .line 318
    move-result-object v11

    .line 319
    check-cast v11, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 320
    .line 321
    if-nez v10, :cond_d

    .line 322
    .line 323
    :try_start_0
    invoke-virtual {v11}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getSprmBuf()Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 324
    .line 325
    .line 326
    move-result-object v10

    .line 327
    invoke-virtual {v10}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->clone()Ljava/lang/Object;

    .line 328
    .line 329
    .line 330
    move-result-object v10

    .line 331
    check-cast v10, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    .line 333
    goto :goto_6

    .line 334
    :catch_0
    move-exception p1

    .line 335
    new-instance p2, Ljava/lang/Error;

    .line 336
    .line 337
    invoke-direct {p2, p1}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    .line 338
    .line 339
    .line 340
    throw p2

    .line 341
    :cond_d
    invoke-virtual {v11}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getGrpprl()[B

    .line 342
    .line 343
    .line 344
    move-result-object v11

    .line 345
    invoke-virtual {v10, v11, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->append([BI)V

    .line 346
    .line 347
    .line 348
    goto :goto_6

    .line 349
    :cond_e
    new-instance v9, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 350
    .line 351
    invoke-direct {v9, v6, v8, v10}, Lcom/intsig/office/fc/hwpf/model/PAPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 352
    .line 353
    .line 354
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    .line 356
    .line 357
    :goto_7
    move v6, v8

    .line 358
    :goto_8
    add-int/lit8 v5, v5, 0x1

    .line 359
    .line 360
    goto/16 :goto_3

    .line 361
    .line 362
    :cond_f
    new-instance p1, Ljava/util/ArrayList;

    .line 363
    .line 364
    invoke-direct {p1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 365
    .line 366
    .line 367
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 368
    .line 369
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 370
    .line 371
    .line 372
    return-void
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "WordDocument"

    .line 1
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;

    move-result-object v0

    const-string v1, "1Table"

    .line 2
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;

    move-result-object p1

    .line 3
    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    return-void
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>(I)V

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v2

    .line 6
    rem-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_0

    rsub-int v2, v2, 0x200

    .line 7
    new-array v2, v2, [B

    .line 8
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 9
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v2

    .line 10
    div-int/lit16 v2, v2, 0x200

    .line 11
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    .line 12
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 13
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 14
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v3

    .line 15
    invoke-interface {p3, v3}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v3

    .line 16
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->_paragraphs:Ljava/util/ArrayList;

    :goto_0
    const/4 v5, 0x0

    .line 17
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 18
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v6

    invoke-interface {p3, v6}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v6

    .line 19
    new-instance v7, Lcom/intsig/office/fc/hwpf/model/PAPFormattedDiskPage;

    invoke-direct {v7}, Lcom/intsig/office/fc/hwpf/model/PAPFormattedDiskPage;-><init>()V

    .line 20
    invoke-virtual {v7, v4}, Lcom/intsig/office/fc/hwpf/model/PAPFormattedDiskPage;->fill(Ljava/util/List;)V

    .line 21
    invoke-virtual {v7, p2, p3}, Lcom/intsig/office/fc/hwpf/model/PAPFormattedDiskPage;->toByteArray(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)[B

    move-result-object v4

    .line 22
    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write([B)V

    .line 23
    invoke-virtual {v7}, Lcom/intsig/office/fc/hwpf/model/PAPFormattedDiskPage;->getOverflow()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 24
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/intsig/office/fc/hwpf/model/PAPX;

    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v5

    invoke-interface {p3, v5}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v5

    goto :goto_1

    :cond_1
    move v5, v3

    :goto_1
    new-array v7, v1, [B

    add-int/lit8 v8, v2, 0x1

    .line 25
    invoke-static {v7, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 26
    new-instance v2, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    invoke-direct {v2, v6, v5, v7}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    invoke-virtual {v0, v2}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->addProperty(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)V

    if-nez v4, :cond_2

    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void

    :cond_2
    move v2, v8

    goto :goto_0
.end method

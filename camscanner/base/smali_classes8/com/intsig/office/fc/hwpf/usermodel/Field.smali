.class public interface abstract Lcom/intsig/office/fc/hwpf/usermodel/Field;
.super Ljava/lang/Object;
.source "Field.java"


# static fields
.field public static final ADDIN:B = 0x51t

.field public static final ADDRESSBLOCK:B = 0x5dt

.field public static final ADVANCE:B = 0x54t

.field public static final ASK:B = 0x26t

.field public static final AUTHOR:B = 0x11t

.field public static final AUTONUM:B = 0x36t

.field public static final AUTONUMLGL:B = 0x35t

.field public static final AUTONUMOUT:B = 0x34t

.field public static final AUTOTEXT:B = 0x4ft

.field public static final AUTOTEXTLIST:B = 0x59t

.field public static final BARCODE:B = 0x3ft

.field public static final BIDIOUTLINE:B = 0x5ct

.field public static final COMMENTS:B = 0x13t

.field public static final COMPARE:B = 0x50t

.field public static final CONTROL:B = 0x57t

.field public static final CREATEDATE:B = 0x15t

.field public static final DATA:B = 0x28t

.field public static final DATE:B = 0x1ft

.field public static final DDE:B = 0x2dt

.field public static final DDEAUTO:B = 0x2et

.field public static final DOCPROPERTY:B = 0x55t

.field public static final DOCVARIABLE:B = 0x40t

.field public static final EDITTIME:B = 0x19t

.field public static final EMBED:B = 0x3at

.field public static final EQ:B = 0x31t

.field public static final Equals:B = 0x22t

.field public static final FILENAME:B = 0x1dt

.field public static final FILESIZE:B = 0x45t

.field public static final FILLIN:B = 0x27t

.field public static final FORMCHECKBOX:B = 0x47t

.field public static final FORMDROPDOWN:B = 0x53t

.field public static final FORMTEXT:B = 0x46t

.field public static final FTNREF:B = 0x5t

.field public static final GLOSSARY:B = 0x2ft

.field public static final GOTOBUTTON:B = 0x32t

.field public static final GREETINGLINE:B = 0x5et

.field public static final HTMLCONTROL:B = 0x5bt

.field public static final HYPERLINK:B = 0x58t

.field public static final IF:B = 0x7t

.field public static final IMPORT:B = 0x37t

.field public static final INCLUDE:B = 0x24t

.field public static final INCLUDEPICTURE:B = 0x43t

.field public static final INCLUDETEXT:B = 0x44t

.field public static final INDEX:B = 0x8t

.field public static final INFO:B = 0xet

.field public static final KEYWORDS:B = 0x12t

.field public static final LASTSAVEDBY:B = 0x14t

.field public static final LINK:B = 0x38t

.field public static final LISTNUM:B = 0x5at

.field public static final MACROBUTTON:B = 0x33t

.field public static final MERGEFIELD:B = 0x3bt

.field public static final MERGEREC:B = 0x2ct

.field public static final MERGESEQ:B = 0x4bt

.field public static final NEXT:B = 0x29t

.field public static final NEXTIF:B = 0x2at

.field public static final NOTEREF:B = 0x48t

.field public static final NUMCHARS:B = 0x1ct

.field public static final NUMPAGES:B = 0x1at

.field public static final NUMWORDS:B = 0x1bt

.field public static final PAGE:B = 0x21t

.field public static final PAGEREF:B = 0x25t

.field public static final PRINT:B = 0x30t

.field public static final PRINTDATE:B = 0x17t

.field public static final QUOTE:B = 0x23t

.field public static final REF:B = 0x3t

.field public static final REVNUM:B = 0x18t

.field public static final SAVEDATE:B = 0x16t

.field public static final SECTION:B = 0x41t

.field public static final SECTIONPAGES:B = 0x42t

.field public static final SEQ:B = 0xct

.field public static final SET:B = 0x6t

.field public static final SHAPE:B = 0x5ft

.field public static final SKIPIF:B = 0x2bt

.field public static final STYLEREF:B = 0xat

.field public static final SUBJECT:B = 0x10t

.field public static final SYMBOL:B = 0x39t

.field public static final TEMPLATE:B = 0x1et

.field public static final TIME:B = 0x20t

.field public static final TITLE:B = 0xft

.field public static final TOA:B = 0x49t

.field public static final TOC:B = 0xdt

.field public static final USERADDRESS:B = 0x3et

.field public static final USERINITIALS:B = 0x3dt

.field public static final USERNAME:B = 0x3ct


# virtual methods
.method public abstract firstSubrange(Lcom/intsig/office/fc/hwpf/usermodel/Range;)Lcom/intsig/office/fc/hwpf/usermodel/Range;
.end method

.method public abstract getFieldEndOffset()I
.end method

.method public abstract getFieldStartOffset()I
.end method

.method public abstract getMarkEndCharacterRun(Lcom/intsig/office/fc/hwpf/usermodel/Range;)Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;
.end method

.method public abstract getMarkEndOffset()I
.end method

.method public abstract getMarkSeparatorCharacterRun(Lcom/intsig/office/fc/hwpf/usermodel/Range;)Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;
.end method

.method public abstract getMarkSeparatorOffset()I
.end method

.method public abstract getMarkStartCharacterRun(Lcom/intsig/office/fc/hwpf/usermodel/Range;)Lcom/intsig/office/fc/hwpf/usermodel/CharacterRun;
.end method

.method public abstract getMarkStartOffset()I
.end method

.method public abstract getType()I
.end method

.method public abstract hasSeparator()Z
.end method

.method public abstract isHasSep()Z
.end method

.method public abstract isLocked()Z
.end method

.method public abstract isNested()Z
.end method

.method public abstract isPrivateResult()Z
.end method

.method public abstract isResultDirty()Z
.end method

.method public abstract isResultEdited()Z
.end method

.method public abstract isZombieEmbed()Z
.end method

.method public abstract secondSubrange(Lcom/intsig/office/fc/hwpf/usermodel/Range;)Lcom/intsig/office/fc/hwpf/usermodel/Range;
.end method

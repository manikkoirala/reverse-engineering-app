.class public final Lcom/intsig/office/fc/hwpf/sprm/SectionSprmUncompressor;
.super Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;
.source "SectionSprmUncompressor.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static unCompressSEPOperation(Lcom/intsig/office/fc/hwpf/usermodel/SectionProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    packed-switch v0, :pswitch_data_0

    .line 7
    .line 8
    .line 9
    :pswitch_0
    goto/16 :goto_0

    .line 10
    .line 11
    :pswitch_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    int-to-short p1, p1

    .line 16
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setWTextFlow(I)V

    .line 17
    .line 18
    .line 19
    goto/16 :goto_0

    .line 20
    .line 21
    :pswitch_2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setClm(I)V

    .line 26
    .line 27
    .line 28
    goto/16 :goto_0

    .line 29
    .line 30
    :pswitch_3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDyaLinePitch(I)V

    .line 35
    .line 36
    .line 37
    goto/16 :goto_0

    .line 38
    .line 39
    :pswitch_4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDxtCharSpace(I)V

    .line 44
    .line 45
    .line 46
    goto/16 :goto_0

    .line 47
    .line 48
    :pswitch_5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setPgbProp(I)V

    .line 53
    .line 54
    .line 55
    goto/16 :goto_0

    .line 56
    .line 57
    :pswitch_6
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setBrcRight(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 71
    .line 72
    .line 73
    goto/16 :goto_0

    .line 74
    .line 75
    :pswitch_7
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 76
    .line 77
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setBrcBottom(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 89
    .line 90
    .line 91
    goto/16 :goto_0

    .line 92
    .line 93
    :pswitch_8
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 94
    .line 95
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 100
    .line 101
    .line 102
    move-result p1

    .line 103
    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setBrcLeft(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 107
    .line 108
    .line 109
    goto/16 :goto_0

    .line 110
    .line 111
    :pswitch_9
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 112
    .line 113
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setBrcTop(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 125
    .line 126
    .line 127
    goto/16 :goto_0

    .line 128
    .line 129
    :pswitch_a
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 130
    .line 131
    .line 132
    move-result p1

    .line 133
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 134
    .line 135
    .line 136
    move-result p1

    .line 137
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setFPropMark(Z)V

    .line 138
    .line 139
    .line 140
    goto/16 :goto_0

    .line 141
    .line 142
    :pswitch_b
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 143
    .line 144
    .line 145
    move-result p1

    .line 146
    int-to-short p1, p1

    .line 147
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDmPaperReq(I)V

    .line 148
    .line 149
    .line 150
    goto/16 :goto_0

    .line 151
    .line 152
    :pswitch_c
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 153
    .line 154
    .line 155
    move-result p1

    .line 156
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDzaGutter(I)V

    .line 157
    .line 158
    .line 159
    goto/16 :goto_0

    .line 160
    .line 161
    :pswitch_d
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 162
    .line 163
    .line 164
    move-result p1

    .line 165
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDyaBottom(I)V

    .line 166
    .line 167
    .line 168
    goto/16 :goto_0

    .line 169
    .line 170
    :pswitch_e
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 171
    .line 172
    .line 173
    move-result p1

    .line 174
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDyaTop(I)V

    .line 175
    .line 176
    .line 177
    goto/16 :goto_0

    .line 178
    .line 179
    :pswitch_f
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 180
    .line 181
    .line 182
    move-result p1

    .line 183
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDxaRight(I)V

    .line 184
    .line 185
    .line 186
    goto/16 :goto_0

    .line 187
    .line 188
    :pswitch_10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 189
    .line 190
    .line 191
    move-result p1

    .line 192
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDxaLeft(I)V

    .line 193
    .line 194
    .line 195
    goto/16 :goto_0

    .line 196
    .line 197
    :pswitch_11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 198
    .line 199
    .line 200
    move-result p1

    .line 201
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setYaPage(I)V

    .line 202
    .line 203
    .line 204
    goto/16 :goto_0

    .line 205
    .line 206
    :pswitch_12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 207
    .line 208
    .line 209
    move-result p1

    .line 210
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setXaPage(I)V

    .line 211
    .line 212
    .line 213
    goto/16 :goto_0

    .line 214
    .line 215
    :pswitch_13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 216
    .line 217
    .line 218
    move-result p1

    .line 219
    if-eqz p1, :cond_0

    .line 220
    .line 221
    const/4 v1, 0x1

    .line 222
    :cond_0
    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDmOrientPage(Z)V

    .line 223
    .line 224
    .line 225
    goto/16 :goto_0

    .line 226
    .line 227
    :pswitch_14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 228
    .line 229
    .line 230
    move-result p1

    .line 231
    int-to-short p1, p1

    .line 232
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setPgnStart(I)V

    .line 233
    .line 234
    .line 235
    goto/16 :goto_0

    .line 236
    .line 237
    :pswitch_15
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 238
    .line 239
    .line 240
    move-result p1

    .line 241
    int-to-short p1, p1

    .line 242
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setLnnMin(I)V

    .line 243
    .line 244
    .line 245
    goto/16 :goto_0

    .line 246
    .line 247
    :pswitch_16
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 248
    .line 249
    .line 250
    move-result p1

    .line 251
    int-to-byte p1, p1

    .line 252
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setVjc(B)V

    .line 253
    .line 254
    .line 255
    goto/16 :goto_0

    .line 256
    .line 257
    :pswitch_17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 258
    .line 259
    .line 260
    move-result p1

    .line 261
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 262
    .line 263
    .line 264
    move-result p1

    .line 265
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setFLBetween(Z)V

    .line 266
    .line 267
    .line 268
    goto/16 :goto_0

    .line 269
    .line 270
    :pswitch_18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 271
    .line 272
    .line 273
    move-result p1

    .line 274
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDyaHdrBottom(I)V

    .line 275
    .line 276
    .line 277
    goto/16 :goto_0

    .line 278
    .line 279
    :pswitch_19
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 280
    .line 281
    .line 282
    move-result p1

    .line 283
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDyaHdrTop(I)V

    .line 284
    .line 285
    .line 286
    goto/16 :goto_0

    .line 287
    .line 288
    :pswitch_1a
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 289
    .line 290
    .line 291
    move-result p1

    .line 292
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDxaLnn(I)V

    .line 293
    .line 294
    .line 295
    goto/16 :goto_0

    .line 296
    .line 297
    :pswitch_1b
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 298
    .line 299
    .line 300
    move-result p1

    .line 301
    int-to-short p1, p1

    .line 302
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setNLnnMod(I)V

    .line 303
    .line 304
    .line 305
    goto/16 :goto_0

    .line 306
    .line 307
    :pswitch_1c
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 308
    .line 309
    .line 310
    move-result p1

    .line 311
    int-to-byte p1, p1

    .line 312
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setGrpfIhdt(B)V

    .line 313
    .line 314
    .line 315
    goto/16 :goto_0

    .line 316
    .line 317
    :pswitch_1d
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 318
    .line 319
    .line 320
    move-result p1

    .line 321
    int-to-byte p1, p1

    .line 322
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setLnc(B)V

    .line 323
    .line 324
    .line 325
    goto/16 :goto_0

    .line 326
    .line 327
    :pswitch_1e
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 328
    .line 329
    .line 330
    move-result p1

    .line 331
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 332
    .line 333
    .line 334
    move-result p1

    .line 335
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setFEndNote(Z)V

    .line 336
    .line 337
    .line 338
    goto/16 :goto_0

    .line 339
    .line 340
    :pswitch_1f
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 341
    .line 342
    .line 343
    move-result p1

    .line 344
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 345
    .line 346
    .line 347
    move-result p1

    .line 348
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setFPgnRestart(Z)V

    .line 349
    .line 350
    .line 351
    goto/16 :goto_0

    .line 352
    .line 353
    :pswitch_20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 354
    .line 355
    .line 356
    move-result p1

    .line 357
    int-to-short p1, p1

    .line 358
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDxaPgn(I)V

    .line 359
    .line 360
    .line 361
    goto/16 :goto_0

    .line 362
    .line 363
    :pswitch_21
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 364
    .line 365
    .line 366
    move-result p1

    .line 367
    int-to-short p1, p1

    .line 368
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDyaPgn(I)V

    .line 369
    .line 370
    .line 371
    goto/16 :goto_0

    .line 372
    .line 373
    :pswitch_22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 374
    .line 375
    .line 376
    move-result p1

    .line 377
    int-to-byte p1, p1

    .line 378
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setNfcPgn(B)V

    .line 379
    .line 380
    .line 381
    goto/16 :goto_0

    .line 382
    .line 383
    :pswitch_23
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 384
    .line 385
    .line 386
    move-result p1

    .line 387
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 388
    .line 389
    .line 390
    move-result p1

    .line 391
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setFAutoPgn(Z)V

    .line 392
    .line 393
    .line 394
    goto/16 :goto_0

    .line 395
    .line 396
    :pswitch_24
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 397
    .line 398
    .line 399
    move-result p1

    .line 400
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDxaColumns(I)V

    .line 401
    .line 402
    .line 403
    goto/16 :goto_0

    .line 404
    .line 405
    :pswitch_25
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 406
    .line 407
    .line 408
    move-result p1

    .line 409
    int-to-short p1, p1

    .line 410
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setCcolM1(I)V

    .line 411
    .line 412
    .line 413
    goto :goto_0

    .line 414
    :pswitch_26
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 415
    .line 416
    .line 417
    move-result p1

    .line 418
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 419
    .line 420
    .line 421
    move-result p1

    .line 422
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setFTitlePage(Z)V

    .line 423
    .line 424
    .line 425
    goto :goto_0

    .line 426
    :pswitch_27
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 427
    .line 428
    .line 429
    move-result p1

    .line 430
    int-to-byte p1, p1

    .line 431
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setBkc(B)V

    .line 432
    .line 433
    .line 434
    goto :goto_0

    .line 435
    :pswitch_28
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 436
    .line 437
    .line 438
    move-result p1

    .line 439
    int-to-short p1, p1

    .line 440
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDmBinOther(I)V

    .line 441
    .line 442
    .line 443
    goto :goto_0

    .line 444
    :pswitch_29
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 445
    .line 446
    .line 447
    move-result p1

    .line 448
    int-to-short p1, p1

    .line 449
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setDmBinFirst(I)V

    .line 450
    .line 451
    .line 452
    goto :goto_0

    .line 453
    :pswitch_2a
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 454
    .line 455
    .line 456
    move-result p1

    .line 457
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 458
    .line 459
    .line 460
    move-result p1

    .line 461
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setFUnlocked(Z)V

    .line 462
    .line 463
    .line 464
    goto :goto_0

    .line 465
    :pswitch_2b
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 466
    .line 467
    .line 468
    move-result p1

    .line 469
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 470
    .line 471
    .line 472
    move-result p1

    .line 473
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setFEvenlySpaced(Z)V

    .line 474
    .line 475
    .line 476
    goto :goto_0

    .line 477
    :pswitch_2c
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->size()I

    .line 478
    .line 479
    .line 480
    move-result v0

    .line 481
    add-int/lit8 v0, v0, -0x3

    .line 482
    .line 483
    new-array v2, v0, [B

    .line 484
    .line 485
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 486
    .line 487
    .line 488
    move-result-object v3

    .line 489
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 490
    .line 491
    .line 492
    move-result p1

    .line 493
    invoke-static {v3, p1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 494
    .line 495
    .line 496
    invoke-virtual {p0, v2}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setOlstAnm([B)V

    .line 497
    .line 498
    .line 499
    goto :goto_0

    .line 500
    :pswitch_2d
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 501
    .line 502
    .line 503
    move-result p1

    .line 504
    int-to-byte p1, p1

    .line 505
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setIHeadingPgn(B)V

    .line 506
    .line 507
    .line 508
    goto :goto_0

    .line 509
    :pswitch_2e
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 510
    .line 511
    .line 512
    move-result p1

    .line 513
    int-to-byte p1, p1

    .line 514
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/SEPAbstractType;->setCnsPgn(B)V

    .line 515
    .line 516
    .line 517
    :goto_0
    return-void

    .line 518
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_0
        :pswitch_0
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static uncompressSEP([BI)Lcom/intsig/office/fc/hwpf/usermodel/SectionProperties;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/SectionProperties;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/SectionProperties;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    .line 7
    .line 8
    invoke-direct {v1, p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 9
    .line 10
    .line 11
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result p0

    .line 15
    if-eqz p0, :cond_0

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 18
    .line 19
    .line 20
    move-result-object p0

    .line 21
    invoke-static {v0, p0}, Lcom/intsig/office/fc/hwpf/sprm/SectionSprmUncompressor;->unCompressSEPOperation(Lcom/intsig/office/fc/hwpf/usermodel/SectionProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

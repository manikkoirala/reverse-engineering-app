.class public interface abstract Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing;
.super Ljava/lang/Object;
.source "OfficeDrawing.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing$VerticalRelativeElement;,
        Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing$VerticalPositioning;,
        Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing$HorizontalRelativeElement;,
        Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawing$HorizontalPositioning;
    }
.end annotation


# virtual methods
.method public abstract getAutoShape()Lcom/intsig/office/fc/hwpf/usermodel/HWPFShape;
.end method

.method public abstract getHorizontalPositioning()B
.end method

.method public abstract getHorizontalRelative()B
.end method

.method public abstract getPictureData(Lcom/intsig/office/system/IControl;)[B
.end method

.method public abstract getPictureData(Lcom/intsig/office/system/IControl;I)[B
.end method

.method public abstract getPictureEffectInfor()Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;
.end method

.method public abstract getRectangleBottom()I
.end method

.method public abstract getRectangleLeft()I
.end method

.method public abstract getRectangleRight()I
.end method

.method public abstract getRectangleTop()I
.end method

.method public abstract getShapeId()I
.end method

.method public abstract getTempFilePath(Lcom/intsig/office/system/IControl;)Ljava/lang/String;
.end method

.method public abstract getVerticalPositioning()B
.end method

.method public abstract getVerticalRelativeElement()B
.end method

.method public abstract getWrap()I
.end method

.method public abstract isAnchorLock()Z
.end method

.method public abstract isBelowText()Z
.end method

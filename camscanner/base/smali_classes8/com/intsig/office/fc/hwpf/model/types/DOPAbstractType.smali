.class public abstract Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;
.super Ljava/lang/Object;
.source "DOPAbstractType.java"

# interfaces
.implements Lcom/intsig/office/fc/hwpf/model/HDFType;


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static KeyVirusSession30:Lcom/intsig/office/fc/util/BitField;

.field private static epc:Lcom/intsig/office/fc/util/BitField;

.field private static fAutoHyphen:Lcom/intsig/office/fc/util/BitField;

.field private static fAutoVersions:Lcom/intsig/office/fc/util/BitField;

.field private static fBackup:Lcom/intsig/office/fc/util/BitField;

.field private static fConvMailMergeEsc:Lcom/intsig/office/fc/util/BitField;

.field private static fDfltTrueType:Lcom/intsig/office/fc/util/BitField;

.field private static fDispFormFldSel:Lcom/intsig/office/fc/util/BitField;

.field private static fEmbedFonts:Lcom/intsig/office/fc/util/BitField;

.field private static fExactCWords:Lcom/intsig/office/fc/util/BitField;

.field private static fFacingPages:Lcom/intsig/office/fc/util/BitField;

.field private static fForcePageSizePag:Lcom/intsig/office/fc/util/BitField;

.field private static fFormNoFields:Lcom/intsig/office/fc/util/BitField;

.field private static fGramAllClean:Lcom/intsig/office/fc/util/BitField;

.field private static fGramAllDone:Lcom/intsig/office/fc/util/BitField;

.field private static fHaveVersions:Lcom/intsig/office/fc/util/BitField;

.field private static fHideLastVersion:Lcom/intsig/office/fc/util/BitField;

.field private static fHtmlDoc:Lcom/intsig/office/fc/util/BitField;

.field private static fHyphCapitals:Lcom/intsig/office/fc/util/BitField;

.field private static fIncludeFooter:Lcom/intsig/office/fc/util/BitField;

.field private static fIncludeHeader:Lcom/intsig/office/fc/util/BitField;

.field private static fLabelDoc:Lcom/intsig/office/fc/util/BitField;

.field private static fLinkStyles:Lcom/intsig/office/fc/util/BitField;

.field private static fLockAtn:Lcom/intsig/office/fc/util/BitField;

.field private static fLockRev:Lcom/intsig/office/fc/util/BitField;

.field private static fMWSmallCaps:Lcom/intsig/office/fc/util/BitField;

.field private static fMapPrintTextColor:Lcom/intsig/office/fc/util/BitField;

.field private static fMinFontSizePag:Lcom/intsig/office/fc/util/BitField;

.field private static fMirrorMargins:Lcom/intsig/office/fc/util/BitField;

.field private static fNoColumnBalance:Lcom/intsig/office/fc/util/BitField;

.field private static fNoLeading:Lcom/intsig/office/fc/util/BitField;

.field private static fNoSpaceRaiseLower:Lcom/intsig/office/fc/util/BitField;

.field private static fNoTabForInd:Lcom/intsig/office/fc/util/BitField;

.field private static fOnlyMacPics:Lcom/intsig/office/fc/util/BitField;

.field private static fOnlyWinPics:Lcom/intsig/office/fc/util/BitField;

.field private static fOrigWordTableRules:Lcom/intsig/office/fc/util/BitField;

.field private static fPMHMainDoc:Lcom/intsig/office/fc/util/BitField;

.field private static fPagHidden:Lcom/intsig/office/fc/util/BitField;

.field private static fPagResults:Lcom/intsig/office/fc/util/BitField;

.field private static fPagSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

.field private static fPrintBodyBeforeHdr:Lcom/intsig/office/fc/util/BitField;

.field private static fPrintFormData:Lcom/intsig/office/fc/util/BitField;

.field private static fProtEnabled:Lcom/intsig/office/fc/util/BitField;

.field private static fRMPrint:Lcom/intsig/office/fc/util/BitField;

.field private static fRMView:Lcom/intsig/office/fc/util/BitField;

.field private static fRevMarking:Lcom/intsig/office/fc/util/BitField;

.field private static fRotateFontW6:Lcom/intsig/office/fc/util/BitField;

.field private static fSaveFormData:Lcom/intsig/office/fc/util/BitField;

.field private static fShadeFormData:Lcom/intsig/office/fc/util/BitField;

.field private static fShowBreaksInFrames:Lcom/intsig/office/fc/util/BitField;

.field private static fSnapBorder:Lcom/intsig/office/fc/util/BitField;

.field private static fSubsetFonts:Lcom/intsig/office/fc/util/BitField;

.field private static fSuppressTopSPacingMac5:Lcom/intsig/office/fc/util/BitField;

.field private static fSupressSpdfAfterPageBreak:Lcom/intsig/office/fc/util/BitField;

.field private static fSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

.field private static fSwapBordersFacingPgs:Lcom/intsig/office/fc/util/BitField;

.field private static fTransparentMetafiles:Lcom/intsig/office/fc/util/BitField;

.field private static fTruncDxaExpand:Lcom/intsig/office/fc/util/BitField;

.field private static fVirusLoadSafe:Lcom/intsig/office/fc/util/BitField;

.field private static fVirusPrompted:Lcom/intsig/office/fc/util/BitField;

.field private static fWCFtnEdn:Lcom/intsig/office/fc/util/BitField;

.field private static fWidowControl:Lcom/intsig/office/fc/util/BitField;

.field private static fWrapTrailSpaces:Lcom/intsig/office/fc/util/BitField;

.field private static fpc:Lcom/intsig/office/fc/util/BitField;

.field private static grfSupression:Lcom/intsig/office/fc/util/BitField;

.field private static iGutterPos:Lcom/intsig/office/fc/util/BitField;

.field private static lvl:Lcom/intsig/office/fc/util/BitField;

.field private static nEdn:Lcom/intsig/office/fc/util/BitField;

.field private static nFtn:Lcom/intsig/office/fc/util/BitField;

.field private static nfcEdnRef1:Lcom/intsig/office/fc/util/BitField;

.field private static nfcFtnRef1:Lcom/intsig/office/fc/util/BitField;

.field private static oldfConvMailMergeEsc:Lcom/intsig/office/fc/util/BitField;

.field private static oldfMapPrintTextColor:Lcom/intsig/office/fc/util/BitField;

.field private static oldfNoColumnBalance:Lcom/intsig/office/fc/util/BitField;

.field private static oldfNoSpaceRaiseLower:Lcom/intsig/office/fc/util/BitField;

.field private static oldfNoTabForInd:Lcom/intsig/office/fc/util/BitField;

.field private static oldfOrigWordTableRules:Lcom/intsig/office/fc/util/BitField;

.field private static oldfShowBreaksInFrames:Lcom/intsig/office/fc/util/BitField;

.field private static oldfSuppressSpbfAfterPageBreak:Lcom/intsig/office/fc/util/BitField;

.field private static oldfSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

.field private static oldfSwapBordersFacingPgs:Lcom/intsig/office/fc/util/BitField;

.field private static oldfTransparentMetafiles:Lcom/intsig/office/fc/util/BitField;

.field private static oldfWrapTrailSpaces:Lcom/intsig/office/fc/util/BitField;

.field private static rncEdn:Lcom/intsig/office/fc/util/BitField;

.field private static rncFtn:Lcom/intsig/office/fc/util/BitField;

.field private static unused1:Lcom/intsig/office/fc/util/BitField;

.field private static unused3:Lcom/intsig/office/fc/util/BitField;

.field private static unused4:Lcom/intsig/office/fc/util/BitField;

.field private static unused5:Lcom/intsig/office/fc/util/BitField;

.field private static wScaleSaved:Lcom/intsig/office/fc/util/BitField;

.field private static wvkSaved:Lcom/intsig/office/fc/util/BitField;

.field private static zkSaved:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field protected field_10_wSpare:I

.field protected field_11_dxaHotz:I

.field protected field_12_cConsexHypLim:I

.field protected field_13_wSpare2:I

.field protected field_14_dttmCreated:I

.field protected field_15_dttmRevised:I

.field protected field_16_dttmLastPrint:I

.field protected field_17_nRevision:I

.field protected field_18_tmEdited:I

.field protected field_19_cWords:I

.field protected field_1_formatFlags:B

.field protected field_20_cCh:I

.field protected field_21_cPg:I

.field protected field_22_cParas:I

.field protected field_23_Edn:S

.field protected field_24_Edn1:S

.field protected field_25_cLines:I

.field protected field_26_cWordsFtnEnd:I

.field protected field_27_cChFtnEdn:I

.field protected field_28_cPgFtnEdn:S

.field protected field_29_cParasFtnEdn:I

.field protected field_2_unused2:B

.field protected field_30_cLinesFtnEdn:I

.field protected field_31_lKeyProtDoc:I

.field protected field_32_view:S

.field protected field_33_docinfo4:I

.field protected field_34_adt:S

.field protected field_35_doptypography:[B

.field protected field_36_dogrid:[B

.field protected field_37_docinfo5:S

.field protected field_38_docinfo6:S

.field protected field_39_asumyi:[B

.field protected field_3_footnoteInfo:S

.field protected field_40_cChWS:I

.field protected field_41_cChWSFtnEdn:I

.field protected field_42_grfDocEvents:I

.field protected field_43_virusinfo:I

.field protected field_44_Spare:[B

.field protected field_45_reserved1:I

.field protected field_46_reserved2:I

.field protected field_47_cDBC:I

.field protected field_48_cDBCFtnEdn:I

.field protected field_49_reserved:I

.field protected field_4_fOutlineDirtySave:B

.field protected field_50_nfcFtnRef:S

.field protected field_51_nfcEdnRef:S

.field protected field_52_hpsZoonFontPag:S

.field protected field_53_dywDispPag:S

.field protected field_5_docinfo:B

.field protected field_6_docinfo1:B

.field protected field_7_docinfo2:B

.field protected field_8_docinfo3:S

.field protected field_9_dxaTab:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fFacingPages:Lcom/intsig/office/fc/util/BitField;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 10
    .line 11
    const/4 v2, 0x2

    .line 12
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 13
    .line 14
    .line 15
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWidowControl:Lcom/intsig/office/fc/util/BitField;

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 18
    .line 19
    const/4 v3, 0x4

    .line 20
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 21
    .line 22
    .line 23
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPMHMainDoc:Lcom/intsig/office/fc/util/BitField;

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 26
    .line 27
    const/16 v4, 0x18

    .line 28
    .line 29
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 30
    .line 31
    .line 32
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->grfSupression:Lcom/intsig/office/fc/util/BitField;

    .line 33
    .line 34
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 35
    .line 36
    const/16 v4, 0x60

    .line 37
    .line 38
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fpc:Lcom/intsig/office/fc/util/BitField;

    .line 42
    .line 43
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 44
    .line 45
    const/16 v4, 0x80

    .line 46
    .line 47
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 48
    .line 49
    .line 50
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused1:Lcom/intsig/office/fc/util/BitField;

    .line 51
    .line 52
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 53
    .line 54
    const/4 v5, 0x3

    .line 55
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 56
    .line 57
    .line 58
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->rncFtn:Lcom/intsig/office/fc/util/BitField;

    .line 59
    .line 60
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 61
    .line 62
    const v6, 0xfffc

    .line 63
    .line 64
    .line 65
    invoke-direct {v0, v6}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 66
    .line 67
    .line 68
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nFtn:Lcom/intsig/office/fc/util/BitField;

    .line 69
    .line 70
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 71
    .line 72
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 73
    .line 74
    .line 75
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOnlyMacPics:Lcom/intsig/office/fc/util/BitField;

    .line 76
    .line 77
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 78
    .line 79
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 80
    .line 81
    .line 82
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOnlyWinPics:Lcom/intsig/office/fc/util/BitField;

    .line 83
    .line 84
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 85
    .line 86
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 87
    .line 88
    .line 89
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLabelDoc:Lcom/intsig/office/fc/util/BitField;

    .line 90
    .line 91
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 92
    .line 93
    const/16 v7, 0x8

    .line 94
    .line 95
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 96
    .line 97
    .line 98
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHyphCapitals:Lcom/intsig/office/fc/util/BitField;

    .line 99
    .line 100
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 101
    .line 102
    const/16 v8, 0x10

    .line 103
    .line 104
    invoke-direct {v0, v8}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 105
    .line 106
    .line 107
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fAutoHyphen:Lcom/intsig/office/fc/util/BitField;

    .line 108
    .line 109
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 110
    .line 111
    const/16 v9, 0x20

    .line 112
    .line 113
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 114
    .line 115
    .line 116
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fFormNoFields:Lcom/intsig/office/fc/util/BitField;

    .line 117
    .line 118
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 119
    .line 120
    const/16 v10, 0x40

    .line 121
    .line 122
    invoke-direct {v0, v10}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 123
    .line 124
    .line 125
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLinkStyles:Lcom/intsig/office/fc/util/BitField;

    .line 126
    .line 127
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 128
    .line 129
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 130
    .line 131
    .line 132
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRevMarking:Lcom/intsig/office/fc/util/BitField;

    .line 133
    .line 134
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 135
    .line 136
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 137
    .line 138
    .line 139
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fBackup:Lcom/intsig/office/fc/util/BitField;

    .line 140
    .line 141
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 142
    .line 143
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 144
    .line 145
    .line 146
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fExactCWords:Lcom/intsig/office/fc/util/BitField;

    .line 147
    .line 148
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 149
    .line 150
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 151
    .line 152
    .line 153
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagHidden:Lcom/intsig/office/fc/util/BitField;

    .line 154
    .line 155
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 156
    .line 157
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 158
    .line 159
    .line 160
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagResults:Lcom/intsig/office/fc/util/BitField;

    .line 161
    .line 162
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 163
    .line 164
    invoke-direct {v0, v8}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 165
    .line 166
    .line 167
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLockAtn:Lcom/intsig/office/fc/util/BitField;

    .line 168
    .line 169
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 170
    .line 171
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 172
    .line 173
    .line 174
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMirrorMargins:Lcom/intsig/office/fc/util/BitField;

    .line 175
    .line 176
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 177
    .line 178
    invoke-direct {v0, v10}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 179
    .line 180
    .line 181
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused3:Lcom/intsig/office/fc/util/BitField;

    .line 182
    .line 183
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 184
    .line 185
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 186
    .line 187
    .line 188
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fDfltTrueType:Lcom/intsig/office/fc/util/BitField;

    .line 189
    .line 190
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 191
    .line 192
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 193
    .line 194
    .line 195
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 196
    .line 197
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 198
    .line 199
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 200
    .line 201
    .line 202
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fProtEnabled:Lcom/intsig/office/fc/util/BitField;

    .line 203
    .line 204
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 205
    .line 206
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 207
    .line 208
    .line 209
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fDispFormFldSel:Lcom/intsig/office/fc/util/BitField;

    .line 210
    .line 211
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 212
    .line 213
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 214
    .line 215
    .line 216
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRMView:Lcom/intsig/office/fc/util/BitField;

    .line 217
    .line 218
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 219
    .line 220
    invoke-direct {v0, v8}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 221
    .line 222
    .line 223
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRMPrint:Lcom/intsig/office/fc/util/BitField;

    .line 224
    .line 225
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 226
    .line 227
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 228
    .line 229
    .line 230
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused4:Lcom/intsig/office/fc/util/BitField;

    .line 231
    .line 232
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 233
    .line 234
    invoke-direct {v0, v10}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 235
    .line 236
    .line 237
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLockRev:Lcom/intsig/office/fc/util/BitField;

    .line 238
    .line 239
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 240
    .line 241
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 242
    .line 243
    .line 244
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fEmbedFonts:Lcom/intsig/office/fc/util/BitField;

    .line 245
    .line 246
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 247
    .line 248
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 249
    .line 250
    .line 251
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoTabForInd:Lcom/intsig/office/fc/util/BitField;

    .line 252
    .line 253
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 254
    .line 255
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 256
    .line 257
    .line 258
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoSpaceRaiseLower:Lcom/intsig/office/fc/util/BitField;

    .line 259
    .line 260
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 261
    .line 262
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 263
    .line 264
    .line 265
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSuppressSpbfAfterPageBreak:Lcom/intsig/office/fc/util/BitField;

    .line 266
    .line 267
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 268
    .line 269
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 270
    .line 271
    .line 272
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfWrapTrailSpaces:Lcom/intsig/office/fc/util/BitField;

    .line 273
    .line 274
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 275
    .line 276
    invoke-direct {v0, v8}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 277
    .line 278
    .line 279
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfMapPrintTextColor:Lcom/intsig/office/fc/util/BitField;

    .line 280
    .line 281
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 282
    .line 283
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 284
    .line 285
    .line 286
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoColumnBalance:Lcom/intsig/office/fc/util/BitField;

    .line 287
    .line 288
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 289
    .line 290
    invoke-direct {v0, v10}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 291
    .line 292
    .line 293
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfConvMailMergeEsc:Lcom/intsig/office/fc/util/BitField;

    .line 294
    .line 295
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 296
    .line 297
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 298
    .line 299
    .line 300
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 301
    .line 302
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 303
    .line 304
    const/16 v11, 0x100

    .line 305
    .line 306
    invoke-direct {v0, v11}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 307
    .line 308
    .line 309
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfOrigWordTableRules:Lcom/intsig/office/fc/util/BitField;

    .line 310
    .line 311
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 312
    .line 313
    const/16 v12, 0x200

    .line 314
    .line 315
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 316
    .line 317
    .line 318
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfTransparentMetafiles:Lcom/intsig/office/fc/util/BitField;

    .line 319
    .line 320
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 321
    .line 322
    const/16 v13, 0x400

    .line 323
    .line 324
    invoke-direct {v0, v13}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 325
    .line 326
    .line 327
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfShowBreaksInFrames:Lcom/intsig/office/fc/util/BitField;

    .line 328
    .line 329
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 330
    .line 331
    const/16 v14, 0x800

    .line 332
    .line 333
    invoke-direct {v0, v14}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 334
    .line 335
    .line 336
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSwapBordersFacingPgs:Lcom/intsig/office/fc/util/BitField;

    .line 337
    .line 338
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 339
    .line 340
    const v15, 0xf000

    .line 341
    .line 342
    .line 343
    invoke-direct {v0, v15}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 344
    .line 345
    .line 346
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused5:Lcom/intsig/office/fc/util/BitField;

    .line 347
    .line 348
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 349
    .line 350
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 351
    .line 352
    .line 353
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->rncEdn:Lcom/intsig/office/fc/util/BitField;

    .line 354
    .line 355
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 356
    .line 357
    invoke-direct {v0, v6}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 358
    .line 359
    .line 360
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nEdn:Lcom/intsig/office/fc/util/BitField;

    .line 361
    .line 362
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 363
    .line 364
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 365
    .line 366
    .line 367
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->epc:Lcom/intsig/office/fc/util/BitField;

    .line 368
    .line 369
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 370
    .line 371
    const/16 v5, 0x3c

    .line 372
    .line 373
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 374
    .line 375
    .line 376
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nfcFtnRef1:Lcom/intsig/office/fc/util/BitField;

    .line 377
    .line 378
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 379
    .line 380
    const/16 v5, 0x3c0

    .line 381
    .line 382
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 383
    .line 384
    .line 385
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nfcEdnRef1:Lcom/intsig/office/fc/util/BitField;

    .line 386
    .line 387
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 388
    .line 389
    invoke-direct {v0, v13}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 390
    .line 391
    .line 392
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPrintFormData:Lcom/intsig/office/fc/util/BitField;

    .line 393
    .line 394
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 395
    .line 396
    invoke-direct {v0, v14}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 397
    .line 398
    .line 399
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSaveFormData:Lcom/intsig/office/fc/util/BitField;

    .line 400
    .line 401
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 402
    .line 403
    const/16 v5, 0x1000

    .line 404
    .line 405
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 406
    .line 407
    .line 408
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fShadeFormData:Lcom/intsig/office/fc/util/BitField;

    .line 409
    .line 410
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 411
    .line 412
    const v6, 0x8000

    .line 413
    .line 414
    .line 415
    invoke-direct {v0, v6}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 416
    .line 417
    .line 418
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWCFtnEdn:Lcom/intsig/office/fc/util/BitField;

    .line 419
    .line 420
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 421
    .line 422
    const/4 v15, 0x7

    .line 423
    invoke-direct {v0, v15}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 424
    .line 425
    .line 426
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->wvkSaved:Lcom/intsig/office/fc/util/BitField;

    .line 427
    .line 428
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 429
    .line 430
    const/16 v15, 0xff8

    .line 431
    .line 432
    invoke-direct {v0, v15}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 433
    .line 434
    .line 435
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->wScaleSaved:Lcom/intsig/office/fc/util/BitField;

    .line 436
    .line 437
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 438
    .line 439
    const/16 v15, 0x3000

    .line 440
    .line 441
    invoke-direct {v0, v15}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 442
    .line 443
    .line 444
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->zkSaved:Lcom/intsig/office/fc/util/BitField;

    .line 445
    .line 446
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 447
    .line 448
    const/16 v15, 0x4000

    .line 449
    .line 450
    invoke-direct {v0, v15}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 451
    .line 452
    .line 453
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRotateFontW6:Lcom/intsig/office/fc/util/BitField;

    .line 454
    .line 455
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 456
    .line 457
    invoke-direct {v0, v6}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 458
    .line 459
    .line 460
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->iGutterPos:Lcom/intsig/office/fc/util/BitField;

    .line 461
    .line 462
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 463
    .line 464
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 465
    .line 466
    .line 467
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoTabForInd:Lcom/intsig/office/fc/util/BitField;

    .line 468
    .line 469
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 470
    .line 471
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 472
    .line 473
    .line 474
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoSpaceRaiseLower:Lcom/intsig/office/fc/util/BitField;

    .line 475
    .line 476
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 477
    .line 478
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 479
    .line 480
    .line 481
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSupressSpdfAfterPageBreak:Lcom/intsig/office/fc/util/BitField;

    .line 482
    .line 483
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 484
    .line 485
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 486
    .line 487
    .line 488
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWrapTrailSpaces:Lcom/intsig/office/fc/util/BitField;

    .line 489
    .line 490
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 491
    .line 492
    invoke-direct {v0, v8}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 493
    .line 494
    .line 495
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMapPrintTextColor:Lcom/intsig/office/fc/util/BitField;

    .line 496
    .line 497
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 498
    .line 499
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 500
    .line 501
    .line 502
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoColumnBalance:Lcom/intsig/office/fc/util/BitField;

    .line 503
    .line 504
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 505
    .line 506
    invoke-direct {v0, v10}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 507
    .line 508
    .line 509
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fConvMailMergeEsc:Lcom/intsig/office/fc/util/BitField;

    .line 510
    .line 511
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 512
    .line 513
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 514
    .line 515
    .line 516
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 517
    .line 518
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 519
    .line 520
    invoke-direct {v0, v11}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 521
    .line 522
    .line 523
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOrigWordTableRules:Lcom/intsig/office/fc/util/BitField;

    .line 524
    .line 525
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 526
    .line 527
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 528
    .line 529
    .line 530
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fTransparentMetafiles:Lcom/intsig/office/fc/util/BitField;

    .line 531
    .line 532
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 533
    .line 534
    invoke-direct {v0, v13}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 535
    .line 536
    .line 537
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fShowBreaksInFrames:Lcom/intsig/office/fc/util/BitField;

    .line 538
    .line 539
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 540
    .line 541
    invoke-direct {v0, v14}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 542
    .line 543
    .line 544
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSwapBordersFacingPgs:Lcom/intsig/office/fc/util/BitField;

    .line 545
    .line 546
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 547
    .line 548
    const/high16 v3, 0x10000

    .line 549
    .line 550
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 551
    .line 552
    .line 553
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSuppressTopSPacingMac5:Lcom/intsig/office/fc/util/BitField;

    .line 554
    .line 555
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 556
    .line 557
    const/high16 v3, 0x20000

    .line 558
    .line 559
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 560
    .line 561
    .line 562
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fTruncDxaExpand:Lcom/intsig/office/fc/util/BitField;

    .line 563
    .line 564
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 565
    .line 566
    const/high16 v3, 0x40000

    .line 567
    .line 568
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 569
    .line 570
    .line 571
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPrintBodyBeforeHdr:Lcom/intsig/office/fc/util/BitField;

    .line 572
    .line 573
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 574
    .line 575
    const/high16 v3, 0x80000

    .line 576
    .line 577
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 578
    .line 579
    .line 580
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoLeading:Lcom/intsig/office/fc/util/BitField;

    .line 581
    .line 582
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 583
    .line 584
    const/high16 v3, 0x200000

    .line 585
    .line 586
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 587
    .line 588
    .line 589
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMWSmallCaps:Lcom/intsig/office/fc/util/BitField;

    .line 590
    .line 591
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 592
    .line 593
    const/16 v3, 0x1e

    .line 594
    .line 595
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 596
    .line 597
    .line 598
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->lvl:Lcom/intsig/office/fc/util/BitField;

    .line 599
    .line 600
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 601
    .line 602
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 603
    .line 604
    .line 605
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fGramAllDone:Lcom/intsig/office/fc/util/BitField;

    .line 606
    .line 607
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 608
    .line 609
    invoke-direct {v0, v10}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 610
    .line 611
    .line 612
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fGramAllClean:Lcom/intsig/office/fc/util/BitField;

    .line 613
    .line 614
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 615
    .line 616
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 617
    .line 618
    .line 619
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSubsetFonts:Lcom/intsig/office/fc/util/BitField;

    .line 620
    .line 621
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 622
    .line 623
    invoke-direct {v0, v11}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 624
    .line 625
    .line 626
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHideLastVersion:Lcom/intsig/office/fc/util/BitField;

    .line 627
    .line 628
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 629
    .line 630
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 631
    .line 632
    .line 633
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHtmlDoc:Lcom/intsig/office/fc/util/BitField;

    .line 634
    .line 635
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 636
    .line 637
    invoke-direct {v0, v14}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 638
    .line 639
    .line 640
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSnapBorder:Lcom/intsig/office/fc/util/BitField;

    .line 641
    .line 642
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 643
    .line 644
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 645
    .line 646
    .line 647
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fIncludeHeader:Lcom/intsig/office/fc/util/BitField;

    .line 648
    .line 649
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 650
    .line 651
    const/16 v3, 0x2000

    .line 652
    .line 653
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 654
    .line 655
    .line 656
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fIncludeFooter:Lcom/intsig/office/fc/util/BitField;

    .line 657
    .line 658
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 659
    .line 660
    invoke-direct {v0, v15}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 661
    .line 662
    .line 663
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fForcePageSizePag:Lcom/intsig/office/fc/util/BitField;

    .line 664
    .line 665
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 666
    .line 667
    invoke-direct {v0, v6}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 668
    .line 669
    .line 670
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMinFontSizePag:Lcom/intsig/office/fc/util/BitField;

    .line 671
    .line 672
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 673
    .line 674
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 675
    .line 676
    .line 677
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHaveVersions:Lcom/intsig/office/fc/util/BitField;

    .line 678
    .line 679
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 680
    .line 681
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 682
    .line 683
    .line 684
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fAutoVersions:Lcom/intsig/office/fc/util/BitField;

    .line 685
    .line 686
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 687
    .line 688
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 689
    .line 690
    .line 691
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fVirusPrompted:Lcom/intsig/office/fc/util/BitField;

    .line 692
    .line 693
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 694
    .line 695
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 696
    .line 697
    .line 698
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fVirusLoadSafe:Lcom/intsig/office/fc/util/BitField;

    .line 699
    .line 700
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 701
    .line 702
    const/4 v1, -0x4

    .line 703
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 704
    .line 705
    .line 706
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->KeyVirusSession30:Lcom/intsig/office/fc/util/BitField;

    .line 707
    .line 708
    return-void
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method protected constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    new-array v1, v0, [B

    .line 6
    .line 7
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_35_doptypography:[B

    .line 8
    .line 9
    new-array v1, v0, [B

    .line 10
    .line 11
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_36_dogrid:[B

    .line 12
    .line 13
    new-array v1, v0, [B

    .line 14
    .line 15
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_39_asumyi:[B

    .line 16
    .line 17
    new-array v0, v0, [B

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_44_Spare:[B

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static getSize()I
    .locals 1

    .line 1
    const/16 v0, 0x1f4

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method protected fillFields([BI)V
    .locals 2

    .line 1
    add-int/lit8 v0, p2, 0x0

    .line 2
    .line 3
    aget-byte v0, p1, v0

    .line 4
    .line 5
    iput-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 6
    .line 7
    add-int/lit8 v0, p2, 0x1

    .line 8
    .line 9
    aget-byte v0, p1, v0

    .line 10
    .line 11
    iput-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_2_unused2:B

    .line 12
    .line 13
    add-int/lit8 v0, p2, 0x2

    .line 14
    .line 15
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 20
    .line 21
    add-int/lit8 v0, p2, 0x4

    .line 22
    .line 23
    aget-byte v0, p1, v0

    .line 24
    .line 25
    iput-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_4_fOutlineDirtySave:B

    .line 26
    .line 27
    add-int/lit8 v0, p2, 0x5

    .line 28
    .line 29
    aget-byte v0, p1, v0

    .line 30
    .line 31
    iput-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 32
    .line 33
    add-int/lit8 v0, p2, 0x6

    .line 34
    .line 35
    aget-byte v0, p1, v0

    .line 36
    .line 37
    iput-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 38
    .line 39
    add-int/lit8 v0, p2, 0x7

    .line 40
    .line 41
    aget-byte v0, p1, v0

    .line 42
    .line 43
    iput-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 44
    .line 45
    add-int/lit8 v0, p2, 0x8

    .line 46
    .line 47
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 52
    .line 53
    add-int/lit8 v0, p2, 0xa

    .line 54
    .line 55
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_9_dxaTab:I

    .line 60
    .line 61
    add-int/lit8 v0, p2, 0xc

    .line 62
    .line 63
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_10_wSpare:I

    .line 68
    .line 69
    add-int/lit8 v0, p2, 0xe

    .line 70
    .line 71
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_11_dxaHotz:I

    .line 76
    .line 77
    add-int/lit8 v0, p2, 0x10

    .line 78
    .line 79
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_12_cConsexHypLim:I

    .line 84
    .line 85
    add-int/lit8 v0, p2, 0x12

    .line 86
    .line 87
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_13_wSpare2:I

    .line 92
    .line 93
    add-int/lit8 v0, p2, 0x14

    .line 94
    .line 95
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_14_dttmCreated:I

    .line 100
    .line 101
    add-int/lit8 v0, p2, 0x18

    .line 102
    .line 103
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 104
    .line 105
    .line 106
    move-result v0

    .line 107
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_15_dttmRevised:I

    .line 108
    .line 109
    add-int/lit8 v0, p2, 0x1c

    .line 110
    .line 111
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 112
    .line 113
    .line 114
    move-result v0

    .line 115
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_16_dttmLastPrint:I

    .line 116
    .line 117
    add-int/lit8 v0, p2, 0x20

    .line 118
    .line 119
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_17_nRevision:I

    .line 124
    .line 125
    add-int/lit8 v0, p2, 0x22

    .line 126
    .line 127
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 128
    .line 129
    .line 130
    move-result v0

    .line 131
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_18_tmEdited:I

    .line 132
    .line 133
    add-int/lit8 v0, p2, 0x26

    .line 134
    .line 135
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_19_cWords:I

    .line 140
    .line 141
    add-int/lit8 v0, p2, 0x2a

    .line 142
    .line 143
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 144
    .line 145
    .line 146
    move-result v0

    .line 147
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_20_cCh:I

    .line 148
    .line 149
    add-int/lit8 v0, p2, 0x2e

    .line 150
    .line 151
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 152
    .line 153
    .line 154
    move-result v0

    .line 155
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_21_cPg:I

    .line 156
    .line 157
    add-int/lit8 v0, p2, 0x30

    .line 158
    .line 159
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 160
    .line 161
    .line 162
    move-result v0

    .line 163
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_22_cParas:I

    .line 164
    .line 165
    add-int/lit8 v0, p2, 0x34

    .line 166
    .line 167
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 172
    .line 173
    add-int/lit8 v0, p2, 0x36

    .line 174
    .line 175
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 176
    .line 177
    .line 178
    move-result v0

    .line 179
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 180
    .line 181
    add-int/lit8 v0, p2, 0x38

    .line 182
    .line 183
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 184
    .line 185
    .line 186
    move-result v0

    .line 187
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_25_cLines:I

    .line 188
    .line 189
    add-int/lit8 v0, p2, 0x3c

    .line 190
    .line 191
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 192
    .line 193
    .line 194
    move-result v0

    .line 195
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_26_cWordsFtnEnd:I

    .line 196
    .line 197
    add-int/lit8 v0, p2, 0x40

    .line 198
    .line 199
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 200
    .line 201
    .line 202
    move-result v0

    .line 203
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_27_cChFtnEdn:I

    .line 204
    .line 205
    add-int/lit8 v0, p2, 0x44

    .line 206
    .line 207
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 208
    .line 209
    .line 210
    move-result v0

    .line 211
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_28_cPgFtnEdn:S

    .line 212
    .line 213
    add-int/lit8 v0, p2, 0x46

    .line 214
    .line 215
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 216
    .line 217
    .line 218
    move-result v0

    .line 219
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_29_cParasFtnEdn:I

    .line 220
    .line 221
    add-int/lit8 v0, p2, 0x4a

    .line 222
    .line 223
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 224
    .line 225
    .line 226
    move-result v0

    .line 227
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_30_cLinesFtnEdn:I

    .line 228
    .line 229
    add-int/lit8 v0, p2, 0x4e

    .line 230
    .line 231
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 232
    .line 233
    .line 234
    move-result v0

    .line 235
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_31_lKeyProtDoc:I

    .line 236
    .line 237
    add-int/lit8 v0, p2, 0x52

    .line 238
    .line 239
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 240
    .line 241
    .line 242
    move-result v0

    .line 243
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 244
    .line 245
    add-int/lit8 v0, p2, 0x54

    .line 246
    .line 247
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 248
    .line 249
    .line 250
    move-result v0

    .line 251
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 252
    .line 253
    add-int/lit8 v0, p2, 0x58

    .line 254
    .line 255
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 256
    .line 257
    .line 258
    move-result v0

    .line 259
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_34_adt:S

    .line 260
    .line 261
    add-int/lit8 v0, p2, 0x5a

    .line 262
    .line 263
    const/16 v1, 0x136

    .line 264
    .line 265
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    .line 266
    .line 267
    .line 268
    move-result-object v0

    .line 269
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_35_doptypography:[B

    .line 270
    .line 271
    add-int/lit16 v0, p2, 0x190

    .line 272
    .line 273
    const/16 v1, 0xa

    .line 274
    .line 275
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    .line 276
    .line 277
    .line 278
    move-result-object v0

    .line 279
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_36_dogrid:[B

    .line 280
    .line 281
    add-int/lit16 v0, p2, 0x19a

    .line 282
    .line 283
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 284
    .line 285
    .line 286
    move-result v0

    .line 287
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 288
    .line 289
    add-int/lit16 v0, p2, 0x19c

    .line 290
    .line 291
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 292
    .line 293
    .line 294
    move-result v0

    .line 295
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 296
    .line 297
    add-int/lit16 v0, p2, 0x19e

    .line 298
    .line 299
    const/16 v1, 0xc

    .line 300
    .line 301
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    .line 302
    .line 303
    .line 304
    move-result-object v0

    .line 305
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_39_asumyi:[B

    .line 306
    .line 307
    add-int/lit16 v0, p2, 0x1aa

    .line 308
    .line 309
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 310
    .line 311
    .line 312
    move-result v0

    .line 313
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_40_cChWS:I

    .line 314
    .line 315
    add-int/lit16 v0, p2, 0x1ae

    .line 316
    .line 317
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 318
    .line 319
    .line 320
    move-result v0

    .line 321
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_41_cChWSFtnEdn:I

    .line 322
    .line 323
    add-int/lit16 v0, p2, 0x1b2

    .line 324
    .line 325
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 326
    .line 327
    .line 328
    move-result v0

    .line 329
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_42_grfDocEvents:I

    .line 330
    .line 331
    add-int/lit16 v0, p2, 0x1b6

    .line 332
    .line 333
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 334
    .line 335
    .line 336
    move-result v0

    .line 337
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 338
    .line 339
    add-int/lit16 v0, p2, 0x1ba

    .line 340
    .line 341
    const/16 v1, 0x1e

    .line 342
    .line 343
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    .line 344
    .line 345
    .line 346
    move-result-object v0

    .line 347
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_44_Spare:[B

    .line 348
    .line 349
    add-int/lit16 v0, p2, 0x1d8

    .line 350
    .line 351
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 352
    .line 353
    .line 354
    move-result v0

    .line 355
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_45_reserved1:I

    .line 356
    .line 357
    add-int/lit16 v0, p2, 0x1dc

    .line 358
    .line 359
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 360
    .line 361
    .line 362
    move-result v0

    .line 363
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_46_reserved2:I

    .line 364
    .line 365
    add-int/lit16 v0, p2, 0x1e0

    .line 366
    .line 367
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 368
    .line 369
    .line 370
    move-result v0

    .line 371
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_47_cDBC:I

    .line 372
    .line 373
    add-int/lit16 v0, p2, 0x1e4

    .line 374
    .line 375
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 376
    .line 377
    .line 378
    move-result v0

    .line 379
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_48_cDBCFtnEdn:I

    .line 380
    .line 381
    add-int/lit16 v0, p2, 0x1e8

    .line 382
    .line 383
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 384
    .line 385
    .line 386
    move-result v0

    .line 387
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_49_reserved:I

    .line 388
    .line 389
    add-int/lit16 v0, p2, 0x1ec

    .line 390
    .line 391
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 392
    .line 393
    .line 394
    move-result v0

    .line 395
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_50_nfcFtnRef:S

    .line 396
    .line 397
    add-int/lit16 v0, p2, 0x1ee

    .line 398
    .line 399
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 400
    .line 401
    .line 402
    move-result v0

    .line 403
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_51_nfcEdnRef:S

    .line 404
    .line 405
    add-int/lit16 v0, p2, 0x1f0

    .line 406
    .line 407
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 408
    .line 409
    .line 410
    move-result v0

    .line 411
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_52_hpsZoonFontPag:S

    .line 412
    .line 413
    add-int/lit16 p2, p2, 0x1f2

    .line 414
    .line 415
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 416
    .line 417
    .line 418
    move-result p1

    .line 419
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_53_dywDispPag:S

    .line 420
    .line 421
    return-void
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public getAdt()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_34_adt:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getAsumyi()[B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_39_asumyi:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCCh()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_20_cCh:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCChFtnEdn()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_27_cChFtnEdn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCChWS()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_40_cChWS:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCChWSFtnEdn()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_41_cChWSFtnEdn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCConsexHypLim()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_12_cConsexHypLim:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCDBC()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_47_cDBC:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCDBCFtnEdn()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_48_cDBCFtnEdn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCLines()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_25_cLines:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCLinesFtnEdn()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_30_cLinesFtnEdn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCParas()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_22_cParas:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCParasFtnEdn()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_29_cParasFtnEdn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCPg()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_21_cPg:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCPgFtnEdn()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_28_cPgFtnEdn:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCWords()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_19_cWords:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCWordsFtnEnd()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_26_cWordsFtnEnd:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocinfo()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocinfo1()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocinfo2()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocinfo3()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocinfo4()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocinfo5()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocinfo6()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDogrid()[B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_36_dogrid:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDoptypography()[B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_35_doptypography:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDttmCreated()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_14_dttmCreated:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDttmLastPrint()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_16_dttmLastPrint:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDttmRevised()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_15_dttmRevised:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDxaHotz()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_11_dxaHotz:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDxaTab()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_9_dxaTab:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDywDispPag()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_53_dywDispPag:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEdn()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEdn1()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEpc()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->epc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFOutlineDirtySave()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_4_fOutlineDirtySave:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFootnoteInfo()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFormatFlags()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFpc()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fpc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getGrfDocEvents()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_42_grfDocEvents:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getGrfSupression()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->grfSupression:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHpsZoonFontPag()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_52_hpsZoonFontPag:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getKeyVirusSession30()I
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->KeyVirusSession30:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLKeyProtDoc()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_31_lKeyProtDoc:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLvl()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->lvl:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNEdn()S
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nEdn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-short v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNFtn()S
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nFtn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-short v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNRevision()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_17_nRevision:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNfcEdnRef()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_51_nfcEdnRef:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNfcEdnRef1()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nfcEdnRef1:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNfcFtnRef()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_50_nfcFtnRef:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getNfcFtnRef1()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nfcFtnRef1:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getReserved()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_49_reserved:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getReserved1()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_45_reserved1:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getReserved2()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_46_reserved2:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRncEdn()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->rncEdn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRncFtn()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->rncFtn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSpare()[B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_44_Spare:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTmEdited()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_18_tmEdited:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getUnused2()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_2_unused2:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getUnused5()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused5:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getView()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getVirusinfo()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWScaleSaved()S
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->wScaleSaved:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-short v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWSpare()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_10_wSpare:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWSpare2()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_13_wSpare2:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWvkSaved()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->wvkSaved:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZkSaved()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->zkSaved:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFAutoHyphen()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fAutoHyphen:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFAutoVersions()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fAutoVersions:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFBackup()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fBackup:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFConvMailMergeEsc()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fConvMailMergeEsc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFDfltTrueType()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fDfltTrueType:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFDispFormFldSel()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fDispFormFldSel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFEmbedFonts()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fEmbedFonts:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFExactCWords()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fExactCWords:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFFacingPages()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fFacingPages:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFForcePageSizePag()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fForcePageSizePag:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFFormNoFields()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fFormNoFields:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFGramAllClean()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fGramAllClean:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFGramAllDone()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fGramAllDone:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFHaveVersions()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHaveVersions:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFHideLastVersion()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHideLastVersion:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFHtmlDoc()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHtmlDoc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFHyphCapitals()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHyphCapitals:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFIncludeFooter()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fIncludeFooter:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFIncludeHeader()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fIncludeHeader:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFLabelDoc()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLabelDoc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFLinkStyles()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLinkStyles:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFLockAtn()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLockAtn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFLockRev()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLockRev:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFMWSmallCaps()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMWSmallCaps:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFMapPrintTextColor()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMapPrintTextColor:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFMinFontSizePag()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMinFontSizePag:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFMirrorMargins()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMirrorMargins:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFNoColumnBalance()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoColumnBalance:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFNoLeading()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoLeading:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFNoSpaceRaiseLower()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoSpaceRaiseLower:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFNoTabForInd()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoTabForInd:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFOnlyMacPics()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOnlyMacPics:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFOnlyWinPics()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOnlyWinPics:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFOrigWordTableRules()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOrigWordTableRules:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFPMHMainDoc()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPMHMainDoc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFPagHidden()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagHidden:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFPagResults()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagResults:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFPagSupressTopSpacing()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFPrintBodyBeforeHdr()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPrintBodyBeforeHdr:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFPrintFormData()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPrintFormData:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFProtEnabled()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fProtEnabled:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFRMPrint()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRMPrint:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFRMView()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRMView:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFRevMarking()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRevMarking:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFRotateFontW6()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRotateFontW6:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSaveFormData()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSaveFormData:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFShadeFormData()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fShadeFormData:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFShowBreaksInFrames()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fShowBreaksInFrames:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSnapBorder()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSnapBorder:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSubsetFonts()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSubsetFonts:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSuppressTopSPacingMac5()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSuppressTopSPacingMac5:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSupressSpdfAfterPageBreak()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSupressSpdfAfterPageBreak:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSupressTopSpacing()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSwapBordersFacingPgs()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSwapBordersFacingPgs:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFTransparentMetafiles()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fTransparentMetafiles:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFTruncDxaExpand()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fTruncDxaExpand:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFVirusLoadSafe()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fVirusLoadSafe:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFVirusPrompted()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fVirusPrompted:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFWCFtnEdn()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWCFtnEdn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFWidowControl()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWidowControl:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFWrapTrailSpaces()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWrapTrailSpaces:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isIGutterPos()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->iGutterPos:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfConvMailMergeEsc()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfConvMailMergeEsc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfMapPrintTextColor()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfMapPrintTextColor:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfNoColumnBalance()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoColumnBalance:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfNoSpaceRaiseLower()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoSpaceRaiseLower:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfNoTabForInd()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoTabForInd:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfOrigWordTableRules()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfOrigWordTableRules:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfShowBreaksInFrames()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfShowBreaksInFrames:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfSuppressSpbfAfterPageBreak()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSuppressSpbfAfterPageBreak:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfSupressTopSpacing()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfSwapBordersFacingPgs()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSwapBordersFacingPgs:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfTransparentMetafiles()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfTransparentMetafiles:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOldfWrapTrailSpaces()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfWrapTrailSpaces:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isUnused1()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused1:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isUnused3()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused3:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isUnused4()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused4:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public serialize([BI)V
    .locals 4

    .line 1
    add-int/lit8 v0, p2, 0x0

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    aput-byte v1, p1, v0

    .line 6
    .line 7
    add-int/lit8 v0, p2, 0x1

    .line 8
    .line 9
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_2_unused2:B

    .line 10
    .line 11
    aput-byte v1, p1, v0

    .line 12
    .line 13
    add-int/lit8 v0, p2, 0x2

    .line 14
    .line 15
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 16
    .line 17
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 18
    .line 19
    .line 20
    add-int/lit8 v0, p2, 0x4

    .line 21
    .line 22
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_4_fOutlineDirtySave:B

    .line 23
    .line 24
    aput-byte v1, p1, v0

    .line 25
    .line 26
    add-int/lit8 v0, p2, 0x5

    .line 27
    .line 28
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 29
    .line 30
    aput-byte v1, p1, v0

    .line 31
    .line 32
    add-int/lit8 v0, p2, 0x6

    .line 33
    .line 34
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 35
    .line 36
    aput-byte v1, p1, v0

    .line 37
    .line 38
    add-int/lit8 v0, p2, 0x7

    .line 39
    .line 40
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 41
    .line 42
    aput-byte v1, p1, v0

    .line 43
    .line 44
    add-int/lit8 v0, p2, 0x8

    .line 45
    .line 46
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 47
    .line 48
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 49
    .line 50
    .line 51
    add-int/lit8 v0, p2, 0xa

    .line 52
    .line 53
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_9_dxaTab:I

    .line 54
    .line 55
    int-to-short v1, v1

    .line 56
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 57
    .line 58
    .line 59
    add-int/lit8 v0, p2, 0xc

    .line 60
    .line 61
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_10_wSpare:I

    .line 62
    .line 63
    int-to-short v1, v1

    .line 64
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 65
    .line 66
    .line 67
    add-int/lit8 v0, p2, 0xe

    .line 68
    .line 69
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_11_dxaHotz:I

    .line 70
    .line 71
    int-to-short v1, v1

    .line 72
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 73
    .line 74
    .line 75
    add-int/lit8 v0, p2, 0x10

    .line 76
    .line 77
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_12_cConsexHypLim:I

    .line 78
    .line 79
    int-to-short v1, v1

    .line 80
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 81
    .line 82
    .line 83
    add-int/lit8 v0, p2, 0x12

    .line 84
    .line 85
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_13_wSpare2:I

    .line 86
    .line 87
    int-to-short v1, v1

    .line 88
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 89
    .line 90
    .line 91
    add-int/lit8 v0, p2, 0x14

    .line 92
    .line 93
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_14_dttmCreated:I

    .line 94
    .line 95
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 96
    .line 97
    .line 98
    add-int/lit8 v0, p2, 0x18

    .line 99
    .line 100
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_15_dttmRevised:I

    .line 101
    .line 102
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 103
    .line 104
    .line 105
    add-int/lit8 v0, p2, 0x1c

    .line 106
    .line 107
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_16_dttmLastPrint:I

    .line 108
    .line 109
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 110
    .line 111
    .line 112
    add-int/lit8 v0, p2, 0x20

    .line 113
    .line 114
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_17_nRevision:I

    .line 115
    .line 116
    int-to-short v1, v1

    .line 117
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 118
    .line 119
    .line 120
    add-int/lit8 v0, p2, 0x22

    .line 121
    .line 122
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_18_tmEdited:I

    .line 123
    .line 124
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 125
    .line 126
    .line 127
    add-int/lit8 v0, p2, 0x26

    .line 128
    .line 129
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_19_cWords:I

    .line 130
    .line 131
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 132
    .line 133
    .line 134
    add-int/lit8 v0, p2, 0x2a

    .line 135
    .line 136
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_20_cCh:I

    .line 137
    .line 138
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 139
    .line 140
    .line 141
    add-int/lit8 v0, p2, 0x2e

    .line 142
    .line 143
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_21_cPg:I

    .line 144
    .line 145
    int-to-short v1, v1

    .line 146
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 147
    .line 148
    .line 149
    add-int/lit8 v0, p2, 0x30

    .line 150
    .line 151
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_22_cParas:I

    .line 152
    .line 153
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 154
    .line 155
    .line 156
    add-int/lit8 v0, p2, 0x34

    .line 157
    .line 158
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 159
    .line 160
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 161
    .line 162
    .line 163
    add-int/lit8 v0, p2, 0x36

    .line 164
    .line 165
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 166
    .line 167
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 168
    .line 169
    .line 170
    add-int/lit8 v0, p2, 0x38

    .line 171
    .line 172
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_25_cLines:I

    .line 173
    .line 174
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 175
    .line 176
    .line 177
    add-int/lit8 v0, p2, 0x3c

    .line 178
    .line 179
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_26_cWordsFtnEnd:I

    .line 180
    .line 181
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 182
    .line 183
    .line 184
    add-int/lit8 v0, p2, 0x40

    .line 185
    .line 186
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_27_cChFtnEdn:I

    .line 187
    .line 188
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 189
    .line 190
    .line 191
    add-int/lit8 v0, p2, 0x44

    .line 192
    .line 193
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_28_cPgFtnEdn:S

    .line 194
    .line 195
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 196
    .line 197
    .line 198
    add-int/lit8 v0, p2, 0x46

    .line 199
    .line 200
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_29_cParasFtnEdn:I

    .line 201
    .line 202
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 203
    .line 204
    .line 205
    add-int/lit8 v0, p2, 0x4a

    .line 206
    .line 207
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_30_cLinesFtnEdn:I

    .line 208
    .line 209
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 210
    .line 211
    .line 212
    add-int/lit8 v0, p2, 0x4e

    .line 213
    .line 214
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_31_lKeyProtDoc:I

    .line 215
    .line 216
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 217
    .line 218
    .line 219
    add-int/lit8 v0, p2, 0x52

    .line 220
    .line 221
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 222
    .line 223
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 224
    .line 225
    .line 226
    add-int/lit8 v0, p2, 0x54

    .line 227
    .line 228
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 229
    .line 230
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 231
    .line 232
    .line 233
    add-int/lit8 v0, p2, 0x58

    .line 234
    .line 235
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_34_adt:S

    .line 236
    .line 237
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 238
    .line 239
    .line 240
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_35_doptypography:[B

    .line 241
    .line 242
    add-int/lit8 v1, p2, 0x5a

    .line 243
    .line 244
    array-length v2, v0

    .line 245
    const/4 v3, 0x0

    .line 246
    invoke-static {v0, v3, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 247
    .line 248
    .line 249
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_36_dogrid:[B

    .line 250
    .line 251
    add-int/lit16 v1, p2, 0x190

    .line 252
    .line 253
    array-length v2, v0

    .line 254
    invoke-static {v0, v3, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 255
    .line 256
    .line 257
    add-int/lit16 v0, p2, 0x19a

    .line 258
    .line 259
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 260
    .line 261
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 262
    .line 263
    .line 264
    add-int/lit16 v0, p2, 0x19c

    .line 265
    .line 266
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 267
    .line 268
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 269
    .line 270
    .line 271
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_39_asumyi:[B

    .line 272
    .line 273
    add-int/lit16 v1, p2, 0x19e

    .line 274
    .line 275
    array-length v2, v0

    .line 276
    invoke-static {v0, v3, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 277
    .line 278
    .line 279
    add-int/lit16 v0, p2, 0x1aa

    .line 280
    .line 281
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_40_cChWS:I

    .line 282
    .line 283
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 284
    .line 285
    .line 286
    add-int/lit16 v0, p2, 0x1ae

    .line 287
    .line 288
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_41_cChWSFtnEdn:I

    .line 289
    .line 290
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 291
    .line 292
    .line 293
    add-int/lit16 v0, p2, 0x1b2

    .line 294
    .line 295
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_42_grfDocEvents:I

    .line 296
    .line 297
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 298
    .line 299
    .line 300
    add-int/lit16 v0, p2, 0x1b6

    .line 301
    .line 302
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 303
    .line 304
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 305
    .line 306
    .line 307
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_44_Spare:[B

    .line 308
    .line 309
    add-int/lit16 v1, p2, 0x1ba

    .line 310
    .line 311
    array-length v2, v0

    .line 312
    invoke-static {v0, v3, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 313
    .line 314
    .line 315
    add-int/lit16 v0, p2, 0x1d8

    .line 316
    .line 317
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_45_reserved1:I

    .line 318
    .line 319
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 320
    .line 321
    .line 322
    add-int/lit16 v0, p2, 0x1dc

    .line 323
    .line 324
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_46_reserved2:I

    .line 325
    .line 326
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 327
    .line 328
    .line 329
    add-int/lit16 v0, p2, 0x1e0

    .line 330
    .line 331
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_47_cDBC:I

    .line 332
    .line 333
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 334
    .line 335
    .line 336
    add-int/lit16 v0, p2, 0x1e4

    .line 337
    .line 338
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_48_cDBCFtnEdn:I

    .line 339
    .line 340
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 341
    .line 342
    .line 343
    add-int/lit16 v0, p2, 0x1e8

    .line 344
    .line 345
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_49_reserved:I

    .line 346
    .line 347
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 348
    .line 349
    .line 350
    add-int/lit16 v0, p2, 0x1ec

    .line 351
    .line 352
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_50_nfcFtnRef:S

    .line 353
    .line 354
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 355
    .line 356
    .line 357
    add-int/lit16 v0, p2, 0x1ee

    .line 358
    .line 359
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_51_nfcEdnRef:S

    .line 360
    .line 361
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 362
    .line 363
    .line 364
    add-int/lit16 v0, p2, 0x1f0

    .line 365
    .line 366
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_52_hpsZoonFontPag:S

    .line 367
    .line 368
    invoke-static {p1, v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 369
    .line 370
    .line 371
    add-int/lit16 p2, p2, 0x1f2

    .line 372
    .line 373
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_53_dywDispPag:S

    .line 374
    .line 375
    invoke-static {p1, p2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 376
    .line 377
    .line 378
    return-void
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public setAdt(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_34_adt:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setAsumyi([B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_39_asumyi:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCCh(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_20_cCh:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCChFtnEdn(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_27_cChFtnEdn:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCChWS(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_40_cChWS:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCChWSFtnEdn(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_41_cChWSFtnEdn:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCConsexHypLim(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_12_cConsexHypLim:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCDBC(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_47_cDBC:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCDBCFtnEdn(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_48_cDBCFtnEdn:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCLines(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_25_cLines:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCLinesFtnEdn(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_30_cLinesFtnEdn:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCParas(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_22_cParas:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCParasFtnEdn(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_29_cParasFtnEdn:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCPg(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_21_cPg:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCPgFtnEdn(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_28_cPgFtnEdn:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCWords(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_19_cWords:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCWordsFtnEnd(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_26_cWordsFtnEnd:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDocinfo(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDocinfo1(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDocinfo2(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDocinfo3(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDocinfo4(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDocinfo5(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDocinfo6(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDogrid([B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_36_dogrid:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDoptypography([B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_35_doptypography:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDttmCreated(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_14_dttmCreated:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDttmLastPrint(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_16_dttmLastPrint:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDttmRevised(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_15_dttmRevised:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDxaHotz(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_11_dxaHotz:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDxaTab(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_9_dxaTab:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDywDispPag(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_53_dywDispPag:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setEdn(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setEdn1(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setEpc(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->epc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFAutoHyphen(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fAutoHyphen:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFAutoVersions(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fAutoVersions:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFBackup(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fBackup:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFConvMailMergeEsc(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fConvMailMergeEsc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFDfltTrueType(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fDfltTrueType:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFDispFormFldSel(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fDispFormFldSel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFEmbedFonts(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fEmbedFonts:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFExactCWords(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fExactCWords:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFFacingPages(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fFacingPages:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFForcePageSizePag(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fForcePageSizePag:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFFormNoFields(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fFormNoFields:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFGramAllClean(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fGramAllClean:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFGramAllDone(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fGramAllDone:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFHaveVersions(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHaveVersions:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_38_docinfo6:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFHideLastVersion(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHideLastVersion:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFHtmlDoc(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHtmlDoc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFHyphCapitals(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fHyphCapitals:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFIncludeFooter(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fIncludeFooter:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFIncludeHeader(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fIncludeHeader:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFLabelDoc(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLabelDoc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFLinkStyles(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLinkStyles:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFLockAtn(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLockAtn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFLockRev(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fLockRev:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFMWSmallCaps(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMWSmallCaps:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFMapPrintTextColor(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMapPrintTextColor:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFMinFontSizePag(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMinFontSizePag:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFMirrorMargins(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fMirrorMargins:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFNoColumnBalance(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoColumnBalance:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFNoLeading(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoLeading:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFNoSpaceRaiseLower(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoSpaceRaiseLower:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFNoTabForInd(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fNoTabForInd:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFOnlyMacPics(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOnlyMacPics:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFOnlyWinPics(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOnlyWinPics:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFOrigWordTableRules(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fOrigWordTableRules:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFOutlineDirtySave(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_4_fOutlineDirtySave:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFPMHMainDoc(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPMHMainDoc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFPagHidden(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagHidden:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFPagResults(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagResults:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFPagSupressTopSpacing(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPagSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFPrintBodyBeforeHdr(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPrintBodyBeforeHdr:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFPrintFormData(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fPrintFormData:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFProtEnabled(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fProtEnabled:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFRMPrint(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRMPrint:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFRMView(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRMView:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFRevMarking(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRevMarking:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_5_docinfo:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFRotateFontW6(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fRotateFontW6:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSaveFormData(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSaveFormData:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFShadeFormData(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fShadeFormData:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFShowBreaksInFrames(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fShowBreaksInFrames:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSnapBorder(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSnapBorder:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSubsetFonts(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSubsetFonts:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSuppressTopSPacingMac5(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSuppressTopSPacingMac5:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSupressSpdfAfterPageBreak(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSupressSpdfAfterPageBreak:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSupressTopSpacing(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSwapBordersFacingPgs(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fSwapBordersFacingPgs:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFTransparentMetafiles(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fTransparentMetafiles:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFTruncDxaExpand(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fTruncDxaExpand:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFVirusLoadSafe(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fVirusLoadSafe:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFVirusPrompted(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fVirusPrompted:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFWCFtnEdn(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWCFtnEdn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFWidowControl(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWidowControl:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFWrapTrailSpaces(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fWrapTrailSpaces:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_33_docinfo4:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFootnoteInfo(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFormatFlags(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFpc(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->fpc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setGrfDocEvents(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_42_grfDocEvents:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setGrfSupression(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->grfSupression:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHpsZoonFontPag(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_52_hpsZoonFontPag:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIGutterPos(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->iGutterPos:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setKeyVirusSession30(I)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->KeyVirusSession30:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLKeyProtDoc(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_31_lKeyProtDoc:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLvl(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->lvl:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_37_docinfo5:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNEdn(S)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nEdn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNFtn(S)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nFtn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNRevision(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_17_nRevision:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNfcEdnRef(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_51_nfcEdnRef:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNfcEdnRef1(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nfcEdnRef1:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNfcFtnRef(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_50_nfcFtnRef:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setNfcFtnRef1(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->nfcFtnRef1:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_24_Edn1:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfConvMailMergeEsc(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfConvMailMergeEsc:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfMapPrintTextColor(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfMapPrintTextColor:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfNoColumnBalance(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoColumnBalance:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfNoSpaceRaiseLower(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoSpaceRaiseLower:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfNoTabForInd(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfNoTabForInd:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfOrigWordTableRules(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfOrigWordTableRules:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfShowBreaksInFrames(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfShowBreaksInFrames:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfSuppressSpbfAfterPageBreak(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSuppressSpbfAfterPageBreak:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfSupressTopSpacing(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSupressTopSpacing:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfSwapBordersFacingPgs(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfSwapBordersFacingPgs:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfTransparentMetafiles(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfTransparentMetafiles:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOldfWrapTrailSpaces(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->oldfWrapTrailSpaces:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setReserved(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_49_reserved:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setReserved1(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_45_reserved1:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setReserved2(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_46_reserved2:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRncEdn(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->rncEdn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_23_Edn:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRncFtn(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->rncFtn:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_3_footnoteInfo:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSpare([B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_44_Spare:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTmEdited(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_18_tmEdited:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnused1(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused1:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_1_formatFlags:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnused2(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_2_unused2:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnused3(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused3:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_6_docinfo1:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnused4(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused4:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_7_docinfo2:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnused5(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->unused5:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_8_docinfo3:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setView(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setVirusinfo(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_43_virusinfo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWScaleSaved(S)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->wScaleSaved:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWSpare(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_10_wSpare:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWSpare2(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_13_wSpare2:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWvkSaved(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->wvkSaved:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setZkSaved(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->zkSaved:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->field_32_view:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[DOP]\n"

    .line 2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "    .formatFlags          = "

    .line 3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ("

    .line 4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getFormatFlags()B

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " )\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "         .fFacingPages             = "

    .line 5
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFFacingPages()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    .line 6
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fWidowControl            = "

    .line 7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFWidowControl()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 8
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fPMHMainDoc              = "

    .line 9
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFPMHMainDoc()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 10
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .grfSupression            = "

    .line 11
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getGrfSupression()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fpc                      = "

    .line 13
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getFpc()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .unused1                  = "

    .line 14
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isUnused1()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .unused2              = "

    .line 15
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getUnused2()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .footnoteInfo         = "

    .line 17
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getFootnoteInfo()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .rncFtn                   = "

    .line 19
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getRncFtn()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .nFtn                     = "

    .line 20
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getNFtn()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .fOutlineDirtySave    = "

    .line 21
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getFOutlineDirtySave()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .docinfo              = "

    .line 23
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDocinfo()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .fOnlyMacPics             = "

    .line 25
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFOnlyMacPics()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 26
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fOnlyWinPics             = "

    .line 27
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFOnlyWinPics()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fLabelDoc                = "

    .line 29
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFLabelDoc()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fHyphCapitals            = "

    .line 30
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFHyphCapitals()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 31
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fAutoHyphen              = "

    .line 32
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFAutoHyphen()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 33
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fFormNoFields            = "

    .line 34
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFFormNoFields()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 35
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fLinkStyles              = "

    .line 36
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFLinkStyles()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 37
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fRevMarking              = "

    .line 38
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFRevMarking()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 39
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .docinfo1             = "

    .line 40
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDocinfo1()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .fBackup                  = "

    .line 42
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFBackup()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fExactCWords             = "

    .line 43
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFExactCWords()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 44
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fPagHidden               = "

    .line 45
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFPagHidden()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fPagResults              = "

    .line 46
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFPagResults()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 47
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fLockAtn                 = "

    .line 48
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFLockAtn()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fMirrorMargins           = "

    .line 49
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFMirrorMargins()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 50
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .unused3                  = "

    .line 51
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isUnused3()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fDfltTrueType            = "

    .line 52
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFDfltTrueType()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 53
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .docinfo2             = "

    .line 54
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDocinfo2()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .fPagSupressTopSpacing     = "

    .line 56
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFPagSupressTopSpacing()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 57
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fProtEnabled             = "

    .line 58
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFProtEnabled()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 59
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fDispFormFldSel          = "

    .line 60
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFDispFormFldSel()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fRMView                  = "

    .line 62
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFRMView()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fRMPrint                 = "

    .line 63
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFRMPrint()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .unused4                  = "

    .line 64
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isUnused4()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fLockRev                 = "

    .line 65
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFLockRev()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fEmbedFonts              = "

    .line 66
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFEmbedFonts()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .docinfo3             = "

    .line 68
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDocinfo3()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfNoTabForInd          = "

    .line 70
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfNoTabForInd()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 71
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfNoSpaceRaiseLower     = "

    .line 72
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfNoSpaceRaiseLower()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfSuppressSpbfAfterPageBreak     = "

    .line 74
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfSuppressSpbfAfterPageBreak()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfWrapTrailSpaces      = "

    .line 76
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfWrapTrailSpaces()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfMapPrintTextColor     = "

    .line 78
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfMapPrintTextColor()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfNoColumnBalance      = "

    .line 80
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfNoColumnBalance()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfConvMailMergeEsc     = "

    .line 82
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfConvMailMergeEsc()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 83
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfSupressTopSpacing     = "

    .line 84
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfSupressTopSpacing()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 85
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfOrigWordTableRules     = "

    .line 86
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfOrigWordTableRules()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfTransparentMetafiles     = "

    .line 88
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfTransparentMetafiles()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfShowBreaksInFrames     = "

    .line 90
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfShowBreaksInFrames()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .oldfSwapBordersFacingPgs     = "

    .line 92
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isOldfSwapBordersFacingPgs()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .unused5                  = "

    .line 94
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getUnused5()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .dxaTab               = "

    .line 95
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDxaTab()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .wSpare               = "

    .line 97
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getWSpare()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .dxaHotz              = "

    .line 99
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDxaHotz()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cConsexHypLim        = "

    .line 101
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCConsexHypLim()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .wSpare2              = "

    .line 103
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getWSpare2()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .dttmCreated          = "

    .line 105
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDttmCreated()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .dttmRevised          = "

    .line 107
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDttmRevised()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .dttmLastPrint        = "

    .line 109
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDttmLastPrint()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .nRevision            = "

    .line 111
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getNRevision()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .tmEdited             = "

    .line 113
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getTmEdited()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cWords               = "

    .line 115
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCWords()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cCh                  = "

    .line 117
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCCh()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cPg                  = "

    .line 119
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCPg()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cParas               = "

    .line 121
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCParas()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .Edn                  = "

    .line 123
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getEdn()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .rncEdn                   = "

    .line 125
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getRncEdn()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .nEdn                     = "

    .line 126
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getNEdn()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .Edn1                 = "

    .line 127
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getEdn1()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .epc                      = "

    .line 129
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getEpc()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .nfcFtnRef1               = "

    .line 130
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getNfcFtnRef1()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 131
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .nfcEdnRef1               = "

    .line 132
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getNfcEdnRef1()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fPrintFormData           = "

    .line 134
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFPrintFormData()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSaveFormData            = "

    .line 136
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFSaveFormData()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 137
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fShadeFormData           = "

    .line 138
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFShadeFormData()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fWCFtnEdn                = "

    .line 140
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFWCFtnEdn()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .cLines               = "

    .line 141
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCLines()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cWordsFtnEnd         = "

    .line 143
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCWordsFtnEnd()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cChFtnEdn            = "

    .line 145
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCChFtnEdn()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cPgFtnEdn            = "

    .line 147
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCPgFtnEdn()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cParasFtnEdn         = "

    .line 149
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCParasFtnEdn()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cLinesFtnEdn         = "

    .line 151
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCLinesFtnEdn()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .lKeyProtDoc          = "

    .line 153
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getLKeyProtDoc()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .view                 = "

    .line 155
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getView()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .wvkSaved                 = "

    .line 157
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getWvkSaved()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .wScaleSaved              = "

    .line 158
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getWScaleSaved()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 159
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .zkSaved                  = "

    .line 160
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getZkSaved()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fRotateFontW6            = "

    .line 161
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFRotateFontW6()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 162
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .iGutterPos               = "

    .line 163
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isIGutterPos()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .docinfo4             = "

    .line 164
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDocinfo4()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .fNoTabForInd             = "

    .line 166
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFNoTabForInd()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 167
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fNoSpaceRaiseLower       = "

    .line 168
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFNoSpaceRaiseLower()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 169
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSupressSpdfAfterPageBreak     = "

    .line 170
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFSupressSpdfAfterPageBreak()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fWrapTrailSpaces         = "

    .line 172
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFWrapTrailSpaces()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fMapPrintTextColor       = "

    .line 174
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFMapPrintTextColor()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 175
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fNoColumnBalance         = "

    .line 176
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFNoColumnBalance()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 177
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fConvMailMergeEsc        = "

    .line 178
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFConvMailMergeEsc()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 179
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSupressTopSpacing       = "

    .line 180
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFSupressTopSpacing()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 181
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fOrigWordTableRules      = "

    .line 182
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFOrigWordTableRules()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 183
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fTransparentMetafiles     = "

    .line 184
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFTransparentMetafiles()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 185
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fShowBreaksInFrames      = "

    .line 186
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFShowBreaksInFrames()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSwapBordersFacingPgs     = "

    .line 188
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFSwapBordersFacingPgs()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 189
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSuppressTopSPacingMac5     = "

    .line 190
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFSuppressTopSPacingMac5()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fTruncDxaExpand          = "

    .line 192
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFTruncDxaExpand()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fPrintBodyBeforeHdr      = "

    .line 194
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFPrintBodyBeforeHdr()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 195
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fNoLeading               = "

    .line 196
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFNoLeading()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fMWSmallCaps             = "

    .line 197
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFMWSmallCaps()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .adt                  = "

    .line 199
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getAdt()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .doptypography        = "

    .line 201
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDoptypography()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .dogrid               = "

    .line 203
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDogrid()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .docinfo5             = "

    .line 205
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDocinfo5()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .lvl                      = "

    .line 207
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getLvl()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fGramAllDone             = "

    .line 208
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFGramAllDone()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 209
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fGramAllClean            = "

    .line 210
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFGramAllClean()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSubsetFonts             = "

    .line 212
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFSubsetFonts()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fHideLastVersion         = "

    .line 214
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFHideLastVersion()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 215
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fHtmlDoc                 = "

    .line 216
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFHtmlDoc()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSnapBorder              = "

    .line 217
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFSnapBorder()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 218
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fIncludeHeader           = "

    .line 219
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFIncludeHeader()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 220
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fIncludeFooter           = "

    .line 221
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFIncludeFooter()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 222
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fForcePageSizePag        = "

    .line 223
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFForcePageSizePag()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 224
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fMinFontSizePag          = "

    .line 225
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFMinFontSizePag()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 226
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .docinfo6             = "

    .line 227
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDocinfo6()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .fHaveVersions            = "

    .line 229
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFHaveVersions()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 230
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fAutoVersions            = "

    .line 231
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFAutoVersions()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 232
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .asumyi               = "

    .line 233
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getAsumyi()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cChWS                = "

    .line 235
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCChWS()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cChWSFtnEdn          = "

    .line 237
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCChWSFtnEdn()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .grfDocEvents         = "

    .line 239
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getGrfDocEvents()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .virusinfo            = "

    .line 241
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getVirusinfo()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .fVirusPrompted           = "

    .line 243
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFVirusPrompted()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 244
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fVirusLoadSafe           = "

    .line 245
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->isFVirusLoadSafe()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 246
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .KeyVirusSession30        = "

    .line 247
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getKeyVirusSession30()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 248
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v3, "    .Spare                = "

    .line 249
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getSpare()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .reserved1            = "

    .line 251
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getReserved1()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .reserved2            = "

    .line 253
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getReserved2()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .cDBC                 = "

    .line 255
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCDBC()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .cDBCFtnEdn           = "

    .line 257
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getCDBCFtnEdn()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .reserved             = "

    .line 259
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getReserved()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .nfcFtnRef            = "

    .line 261
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getNfcFtnRef()S

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .nfcEdnRef            = "

    .line 263
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getNfcEdnRef()S

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .hpsZoonFontPag       = "

    .line 265
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getHpsZoonFontPag()S

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .dywDispPag           = "

    .line 267
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/DOPAbstractType;->getDywDispPag()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "[/DOP]\n"

    .line 269
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

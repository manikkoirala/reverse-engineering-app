.class public Lcom/intsig/office/fc/hwpf/model/BookmarksTables;
.super Ljava/lang/Object;
.source "BookmarksTables.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# instance fields
.field private descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

.field private descriptorsLim:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

.field private names:[Ljava/lang/String;


# direct methods
.method public constructor <init>([BLcom/intsig/office/fc/hwpf/model/FileInformationBlock;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 5
    .line 6
    const/4 v1, 0x4

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>(I)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>(I)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsLim:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 19
    .line 20
    new-array v0, v1, [Ljava/lang/String;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 23
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->read([BLcom/intsig/office/fc/hwpf/model/FileInformationBlock;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private read([BLcom/intsig/office/fc/hwpf/model/FileInformationBlock;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcSttbfbkmk()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbSttbfbkmk()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-static {p1, v0}, Lcom/intsig/office/fc/hwpf/model/SttbfUtils;->〇080([BI)[Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 18
    .line 19
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlcfbkf()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbPlcfbkf()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    new-instance v2, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/office/fc/hwpf/model/types/BKFAbstractType;->getSize()I

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    invoke-direct {v2, p1, v0, v1, v3}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 38
    .line 39
    .line 40
    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 41
    .line 42
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlcfbkl()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbPlcfbkl()I

    .line 47
    .line 48
    .line 49
    move-result p2

    .line 50
    if-eqz v0, :cond_2

    .line 51
    .line 52
    if-eqz p2, :cond_2

    .line 53
    .line 54
    new-instance v1, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 55
    .line 56
    const/4 v2, 0x0

    .line 57
    invoke-direct {v1, p1, v0, p2, v2}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 58
    .line 59
    .line 60
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsLim:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 61
    .line 62
    :cond_2
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public getBookmarksCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDescriptorFirst(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getDescriptorFirstIndex(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->toPropertiesArray()[Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getDescriptorLim(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsLim:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getDescriptorsFirstCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDescriptorsLimCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsLim:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getName(I)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArrayIndexOutOfBoundsException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getNamesCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setName(ILjava/lang/String;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    if-ge p1, v1, :cond_0

    .line 5
    .line 6
    add-int/lit8 v1, p1, 0x1

    .line 7
    .line 8
    new-array v1, v1, [Ljava/lang/String;

    .line 9
    .line 10
    array-length v2, v0

    .line 11
    const/4 v3, 0x0

    .line 12
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 13
    .line 14
    .line 15
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 18
    .line 19
    aput-object p2, v0, p1

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public writePlcfBkmkf(Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsFirst:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->toByteArray()[B

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write([B)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 26
    .line 27
    .line 28
    move-result p2

    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setFcPlcfbkf(I)V

    .line 30
    .line 31
    .line 32
    sub-int/2addr p2, v0

    .line 33
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setLcbPlcfbkf(I)V

    .line 34
    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    :goto_0
    const/4 p2, 0x0

    .line 38
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setFcPlcfbkf(I)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setLcbPlcfbkf(I)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public writePlcfBkmkl(Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsLim:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->descriptorsLim:Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->toByteArray()[B

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write([B)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 26
    .line 27
    .line 28
    move-result p2

    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setFcPlcfbkl(I)V

    .line 30
    .line 31
    .line 32
    sub-int/2addr p2, v0

    .line 33
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setLcbPlcfbkl(I)V

    .line 34
    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    :goto_0
    const/4 p2, 0x0

    .line 38
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setFcPlcfbkl(I)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setLcbPlcfbkl(I)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public writeSttbfBkmk(Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    array-length v0, v0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;->names:[Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {p2, v1}, Lcom/intsig/office/fc/hwpf/model/SttbfUtils;->〇o00〇〇Oo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;[Ljava/lang/String;)I

    .line 16
    .line 17
    .line 18
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    .line 19
    .line 20
    .line 21
    move-result p2

    .line 22
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setFcSttbfbkmk(I)V

    .line 23
    .line 24
    .line 25
    sub-int/2addr p2, v0

    .line 26
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setLcbSttbfbkmk(I)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_1
    :goto_0
    const/4 p2, 0x0

    .line 31
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setFcSttbfbkmk(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->setLcbSttbfbkmk(I)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

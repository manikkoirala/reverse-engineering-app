.class public final Lcom/intsig/office/fc/hwpf/model/FontTable;
.super Ljava/lang/Object;
.source "FontTable.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final _logger:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field private _extraDataSz:S

.field private _fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

.field private _stringCount:S

.field private fcSttbfffn:I

.field private lcbSttbfffn:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/model/FontTable;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>([BII)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 6
    .line 7
    iput p3, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->lcbSttbfffn:I

    .line 8
    .line 9
    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->fcSttbfffn:I

    .line 10
    .line 11
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 12
    .line 13
    .line 14
    move-result p3

    .line 15
    iput-short p3, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 16
    .line 17
    add-int/lit8 p2, p2, 0x2

    .line 18
    .line 19
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 20
    .line 21
    .line 22
    move-result p3

    .line 23
    iput-short p3, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_extraDataSz:S

    .line 24
    .line 25
    add-int/lit8 p2, p2, 0x2

    .line 26
    .line 27
    iget-short p3, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 28
    .line 29
    new-array p3, p3, [Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 30
    .line 31
    iput-object p3, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 32
    .line 33
    const/4 p3, 0x0

    .line 34
    :goto_0
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 35
    .line 36
    if-ge p3, v0, :cond_0

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 39
    .line 40
    new-instance v1, Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 41
    .line 42
    invoke-direct {v1, p1, p2}, Lcom/intsig/office/fc/hwpf/model/Ffn;-><init>([BI)V

    .line 43
    .line 44
    .line 45
    aput-object v1, v0, p3

    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 48
    .line 49
    aget-object v0, v0, p3

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/Ffn;->getSize()I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    add-int/2addr p2, v0

    .line 56
    add-int/lit8 p3, p3, 0x1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/FontTable;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/FontTable;->getStringCount()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    if-ne v0, v1, :cond_2

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/FontTable;->getExtraDataSz()S

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_extraDataSz:S

    .line 17
    .line 18
    if-ne v0, v1, :cond_2

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/FontTable;->getFontNames()[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const/4 v0, 0x1

    .line 25
    const/4 v1, 0x0

    .line 26
    :goto_0
    iget-short v3, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 27
    .line 28
    if-ge v1, v3, :cond_1

    .line 29
    .line 30
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 31
    .line 32
    aget-object v3, v3, v1

    .line 33
    .line 34
    aget-object v4, p1, v1

    .line 35
    .line 36
    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/hwpf/model/Ffn;->equals(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    if-nez v3, :cond_0

    .line 41
    .line 42
    const/4 v0, 0x0

    .line 43
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    move v2, v0

    .line 47
    :cond_2
    return v2
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getAltFont(I)Ljava/lang/String;
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 2
    .line 3
    if-lt p1, v0, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/intsig/office/fc/hwpf/model/FontTable;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 6
    .line 7
    sget v0, Lcom/intsig/office/fc/util/POILogger;->INFO:I

    .line 8
    .line 9
    const-string v1, "Mismatch in chpFtc with stringCount"

    .line 10
    .line 11
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    return-object p1

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 17
    .line 18
    aget-object p1, v0, p1

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/Ffn;->getAltFontName()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getExtraDataSz()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_extraDataSz:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFontNames()[Lcom/intsig/office/fc/hwpf/model/Ffn;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMainFont(I)Ljava/lang/String;
    .locals 2

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 2
    .line 3
    if-lt p1, v0, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/intsig/office/fc/hwpf/model/FontTable;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 6
    .line 7
    sget v0, Lcom/intsig/office/fc/util/POILogger;->INFO:I

    .line 8
    .line 9
    const-string v1, "Mismatch in chpFtc with stringCount"

    .line 10
    .line 11
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    return-object p1

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    .line 17
    .line 18
    aget-object p1, v0, p1

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/Ffn;->getMainFontName()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->lcbSttbfffn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStringCount()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setStringCount(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "1Table"

    .line 1
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;

    move-result-object p1

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/FontTable;->writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V

    return-void
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_stringCount:S

    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BS)V

    .line 4
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 5
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_extraDataSz:S

    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BS)V

    .line 6
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    const/4 v0, 0x0

    .line 7
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/FontTable;->_fontNames:[Lcom/intsig/office/fc/hwpf/model/Ffn;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 8
    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/Ffn;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

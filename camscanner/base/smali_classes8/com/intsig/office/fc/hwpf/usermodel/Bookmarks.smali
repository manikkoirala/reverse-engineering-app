.class public interface abstract Lcom/intsig/office/fc/hwpf/usermodel/Bookmarks;
.super Ljava/lang/Object;
.source "Bookmarks.java"


# virtual methods
.method public abstract getBookmark(I)Lcom/intsig/office/fc/hwpf/usermodel/POIBookmark;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation
.end method

.method public abstract getBookmarksCount()I
.end method

.method public abstract getBookmarksStartedBetween(II)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hwpf/usermodel/POIBookmark;",
            ">;>;"
        }
    .end annotation
.end method

.class public Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;
.super Ljava/lang/Object;
.source "PictureDescriptor.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;,
        Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;,
        Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;
    }
.end annotation


# static fields
.field private static final CBHEADER_OFFSET:I = 0x4

.field private static final DXACROPLEFT_OFFSET:I = 0x24

.field private static final DXACROPRIGHT_OFFSET:I = 0x28

.field private static final DXAGOAL_OFFSET:I = 0x1c

.field private static final DYACROPBOTTOM_OFFSET:I = 0x2a

.field private static final DYACROPTOP_OFFSET:I = 0x26

.field private static final DYAGOAL_OFFSET:I = 0x1e

.field private static final LCB_OFFSET:I = 0x0

.field private static final MFP_HMF_OFFSET:I = 0xc

.field private static final MFP_MM_OFFSET:I = 0x6

.field private static final MFP_XEXT_OFFSET:I = 0x8

.field private static final MFP_YEXT_OFFSET:I = 0xa

.field private static final MX_OFFSET:I = 0x20

.field private static final MY_OFFSET:I = 0x22


# instance fields
.field protected bGrayScl:Z

.field protected bSetBright:Z

.field protected bSetContrast:Z

.field protected bSetGrayScl:Z

.field protected bSetThreshold:Z

.field protected cbHeader:I

.field protected dxaCropLeft:F

.field protected dxaCropRight:F

.field protected dxaGoal:S

.field protected dyaCropBottom:F

.field protected dyaCropTop:F

.field protected dyaGoal:S

.field protected fBright:F

.field protected fContrast:F

.field protected fThreshold:F

.field protected lcb:I

.field protected mfp_hMF:I

.field protected mfp_mm:I

.field protected mfp_xExt:I

.field protected mfp_yExt:I

.field protected mx:S

.field protected my:S

.field protected offset14:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xe

    new-array v0, v0, [B

    .line 2
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->offset14:[B

    const/4 v0, 0x0

    .line 3
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaGoal:S

    .line 4
    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaGoal:S

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaCropLeft:F

    .line 6
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaCropTop:F

    .line 7
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaCropRight:F

    .line 8
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaCropBottom:F

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 10

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xe

    new-array v1, v0, [B

    .line 10
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->offset14:[B

    const/4 v1, 0x0

    .line 11
    iput-short v1, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaGoal:S

    .line 12
    iput-short v1, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaGoal:S

    const/4 v2, 0x0

    .line 13
    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaCropLeft:F

    .line 14
    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaCropTop:F

    .line 15
    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaCropRight:F

    .line 16
    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaCropBottom:F

    add-int/lit8 v2, p2, 0x0

    .line 17
    invoke-static {p1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v2

    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->lcb:I

    add-int/lit8 v2, p2, 0x4

    .line 18
    invoke-static {p1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->cbHeader:I

    add-int/lit8 v2, p2, 0x6

    .line 19
    invoke-static {p1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_mm:I

    add-int/lit8 v2, p2, 0x8

    .line 20
    invoke-static {p1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_xExt:I

    add-int/lit8 v2, p2, 0xa

    .line 21
    invoke-static {p1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_yExt:I

    add-int/lit8 v2, p2, 0xc

    .line 22
    invoke-static {p1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    move-result v2

    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_hMF:I

    add-int/lit8 v2, p2, 0xe

    .line 23
    invoke-static {p1, v2, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->offset14:[B

    add-int/lit8 v0, p2, 0x1c

    .line 24
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaGoal:S

    add-int/lit8 v0, p2, 0x1e

    .line 25
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaGoal:S

    add-int/lit8 v0, p2, 0x20

    .line 26
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mx:S

    add-int/lit8 v0, p2, 0x22

    .line 27
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v0

    iput-short v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->my:S

    add-int/lit8 p2, p2, 0x44

    .line 28
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_mm:I

    const/16 v2, 0x66

    const/4 v3, 0x1

    if-ne v0, v2, :cond_0

    .line 29
    aget-byte v0, p1, p2

    const v2, 0xffff

    and-int/2addr v0, v2

    add-int/2addr v0, v3

    add-int/2addr p2, v0

    .line 30
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->readHeader([BI)Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;

    move-result-object v0

    .line 31
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRecVer(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;)S

    move-result v2

    .line 32
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRecInstance(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;)S

    move-result v4

    const/16 v5, 0xf

    if-ne v2, v5, :cond_a

    if-nez v4, :cond_a

    .line 33
    iget v2, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recType:I

    const v4, 0xf004

    if-ne v2, v4, :cond_a

    .line 34
    iget-wide v4, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recLength:J

    add-int/lit8 p2, p2, 0x8

    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_a

    .line 35
    array-length v0, p1

    if-ge p2, v0, :cond_a

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->readHeader([BI)Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;

    move-result-object v0

    .line 37
    iget-wide v6, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recLength:J

    sub-long/2addr v4, v6

    add-int/lit8 p2, p2, 0x8

    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRecVer(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;)S

    move-result v2

    .line 39
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRecInstance(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;)S

    const/4 v6, 0x3

    if-ne v2, v6, :cond_9

    .line 40
    iget v2, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recType:I

    const v6, 0xf00b

    if-ne v2, v6, :cond_9

    .line 41
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRecInstance(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;)S

    move-result v0

    :goto_1
    if-ge v1, v0, :cond_a

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->readOfficeArtOpid([BI)Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;

    move-result-object v2

    .line 43
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getOpid(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;)S

    move-result v4

    .line 44
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isfBid(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;)Z

    move-result v5

    .line 45
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isfComplex(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;)Z

    move-result v2

    const/16 v6, 0x100

    const/4 v7, 0x4

    if-ne v4, v6, :cond_1

    if-nez v5, :cond_1

    if-nez v2, :cond_1

    add-int/lit8 v6, p2, 0x2

    .line 46
    invoke-static {p1, v6, v7}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v6

    .line 47
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRealNumFromFixedPoint([B)F

    move-result v6

    iput v6, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaCropTop:F

    :cond_1
    const/16 v6, 0x101

    if-ne v4, v6, :cond_2

    if-nez v5, :cond_2

    if-nez v2, :cond_2

    add-int/lit8 v6, p2, 0x2

    .line 48
    invoke-static {p1, v6, v7}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v6

    .line 49
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRealNumFromFixedPoint([B)F

    move-result v6

    iput v6, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaCropBottom:F

    :cond_2
    const/16 v6, 0x102

    if-ne v4, v6, :cond_3

    if-nez v5, :cond_3

    if-nez v2, :cond_3

    add-int/lit8 v6, p2, 0x2

    .line 50
    invoke-static {p1, v6, v7}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v6

    .line 51
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRealNumFromFixedPoint([B)F

    move-result v6

    iput v6, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaCropLeft:F

    :cond_3
    const/16 v6, 0x103

    if-ne v4, v6, :cond_4

    if-nez v5, :cond_4

    if-nez v2, :cond_4

    add-int/lit8 v6, p2, 0x2

    .line 52
    invoke-static {p1, v6, v7}, Lcom/intsig/office/fc/util/LittleEndian;->getByteArray([BII)[B

    move-result-object v6

    .line 53
    invoke-direct {p0, v6}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->getRealNumFromFixedPoint([B)F

    move-result v6

    iput v6, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaCropRight:F

    :cond_4
    const/16 v6, 0x109

    if-ne v4, v6, :cond_5

    if-nez v5, :cond_5

    if-nez v2, :cond_5

    add-int/lit8 v6, p2, 0x2

    .line 54
    invoke-static {p1, v6}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v6

    .line 55
    iput-boolean v3, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bSetBright:Z

    int-to-float v6, v6

    const/high16 v7, 0x47000000    # 32768.0f

    div-float/2addr v6, v7

    const/high16 v7, 0x437f0000    # 255.0f

    mul-float v6, v6, v7

    .line 56
    iput v6, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->fBright:F

    :cond_5
    const/16 v6, 0x108

    if-ne v4, v6, :cond_6

    if-nez v5, :cond_6

    if-nez v2, :cond_6

    add-int/lit8 v6, p2, 0x2

    .line 57
    invoke-static {p1, v6}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v6

    .line 58
    iput-boolean v3, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bSetContrast:Z

    int-to-float v6, v6

    const/high16 v7, 0x47800000    # 65536.0f

    div-float/2addr v6, v7

    const/high16 v7, 0x41200000    # 10.0f

    .line 59
    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iput v6, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->fContrast:F

    :cond_6
    const/16 v6, 0x13f

    if-ne v4, v6, :cond_8

    if-nez v5, :cond_8

    if-nez v2, :cond_8

    add-int/lit8 v2, p2, 0x2

    .line 60
    invoke-direct {p0, p1, v2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->readBlipBooleanProperties([BI)Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;

    move-result-object v2

    .line 61
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isfUsefPictureBiLevel(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 62
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isfPictureBiLevel(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 63
    iput-boolean v3, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bSetThreshold:Z

    const/high16 v2, 0x43000000    # 128.0f

    .line 64
    iput v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->fThreshold:F

    goto :goto_2

    .line 65
    :cond_7
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isfUsefPictureGray(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 66
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->isfPictureGray(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 67
    iput-boolean v3, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bSetGrayScl:Z

    .line 68
    iput-boolean v3, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bGrayScl:Z

    :cond_8
    :goto_2
    add-int/lit8 p2, p2, 0x6

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :cond_9
    int-to-long v6, p2

    .line 69
    iget-wide v8, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recLength:J

    add-long/2addr v6, v8

    long-to-int p2, v6

    goto/16 :goto_0

    :cond_a
    return-void
.end method

.method private getOpid(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;)S
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->opid:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->flag:S

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getShortValue(S)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getRealNumFromFixedPoint([B)F
    .locals 2

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {p1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    int-to-float v0, v0

    .line 12
    int-to-float p1, p1

    .line 13
    const/high16 v1, 0x47800000    # 65536.0f

    .line 14
    .line 15
    div-float/2addr p1, v1

    .line 16
    add-float/2addr v0, p1

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getRecInstance(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;)S
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recInstance:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recFlag:S

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getRecVer(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;)S
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recVer:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recFlag:S

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isfBid(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;)Z
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->fBid:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->flag:S

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getShortValue(S)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/4 v0, 0x1

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isfComplex(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;)Z
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->fComplex:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->flag:S

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getShortValue(S)S

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/4 v0, 0x1

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isfPictureBiLevel(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;)Z
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->fPictureBiLevel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->propValue:I

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/4 v0, 0x1

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isfPictureGray(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;)Z
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->fPictureBiLevel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->propValue:I

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/4 v0, 0x1

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isfUsefPictureBiLevel(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;)Z
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->fUsefPictureBiLevel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->propValue:I

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/4 v0, 0x1

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private isfUsefPictureGray(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;)Z
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->fUsefPictureBiLevel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget p1, p1, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->propValue:I

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/4 v0, 0x1

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private readBlipBooleanProperties([BI)Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;-><init>(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    iput p1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->propValue:I

    .line 11
    .line 12
    new-instance p1, Lcom/intsig/office/fc/util/BitField;

    .line 13
    .line 14
    const/high16 p2, 0x20000

    .line 15
    .line 16
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 17
    .line 18
    .line 19
    iput-object p1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->fPictureBiLevel:Lcom/intsig/office/fc/util/BitField;

    .line 20
    .line 21
    new-instance p1, Lcom/intsig/office/fc/util/BitField;

    .line 22
    .line 23
    const/4 p2, 0x2

    .line 24
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 25
    .line 26
    .line 27
    iput-object p1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->fUsefPictureBiLevel:Lcom/intsig/office/fc/util/BitField;

    .line 28
    .line 29
    new-instance p1, Lcom/intsig/office/fc/util/BitField;

    .line 30
    .line 31
    const/high16 p2, 0x40000

    .line 32
    .line 33
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 34
    .line 35
    .line 36
    iput-object p1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->fPictureGray:Lcom/intsig/office/fc/util/BitField;

    .line 37
    .line 38
    new-instance p1, Lcom/intsig/office/fc/util/BitField;

    .line 39
    .line 40
    const/4 p2, 0x4

    .line 41
    invoke-direct {p1, p2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 42
    .line 43
    .line 44
    iput-object p1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$BlipBooleanProperties;->fUsefPictureGray:Lcom/intsig/office/fc/util/BitField;

    .line 45
    .line 46
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private readHeader([BI)Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;-><init>(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;)V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    const/16 v2, 0xf

    .line 9
    .line 10
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 11
    .line 12
    .line 13
    iput-object v1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recVer:Lcom/intsig/office/fc/util/BitField;

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/office/fc/util/BitField;

    .line 16
    .line 17
    const v2, 0xfff0

    .line 18
    .line 19
    .line 20
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 21
    .line 22
    .line 23
    iput-object v1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recInstance:Lcom/intsig/office/fc/util/BitField;

    .line 24
    .line 25
    add-int/lit8 v1, p2, 0x4

    .line 26
    .line 27
    array-length v2, p1

    .line 28
    if-ge v1, v2, :cond_0

    .line 29
    .line 30
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    iput-short v2, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recFlag:S

    .line 35
    .line 36
    add-int/lit8 p2, p2, 0x2

    .line 37
    .line 38
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getUShort([BI)I

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    iput p2, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recType:I

    .line 43
    .line 44
    invoke-static {p1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getUInt([BI)J

    .line 45
    .line 46
    .line 47
    move-result-wide p1

    .line 48
    iput-wide p1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtRecordHeader;->recLength:J

    .line 49
    .line 50
    :cond_0
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private readOfficeArtOpid([BI)Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;-><init>(Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;)V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    const/16 v2, 0x3fff

    .line 9
    .line 10
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 11
    .line 12
    .line 13
    iput-object v1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->opid:Lcom/intsig/office/fc/util/BitField;

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/office/fc/util/BitField;

    .line 16
    .line 17
    const/16 v2, 0x4000

    .line 18
    .line 19
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 20
    .line 21
    .line 22
    iput-object v1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->fBid:Lcom/intsig/office/fc/util/BitField;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/office/fc/util/BitField;

    .line 25
    .line 26
    const v2, 0x8000

    .line 27
    .line 28
    .line 29
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 30
    .line 31
    .line 32
    iput-object v1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->fComplex:Lcom/intsig/office/fc/util/BitField;

    .line 33
    .line 34
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    iput-short p1, v0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor$OfficeArtOpid;->flag:S

    .line 39
    .line 40
    return-object v0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public getBright()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->fBright:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getContrast()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->fContrast:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getThreshold()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->fThreshold:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZoomX()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mx:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getZoomY()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->my:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isGrayScl()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bGrayScl:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSetBright()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bSetBright:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSetContrast()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bSetContrast:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSetGrayScl()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bSetGrayScl:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSetThreshold()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->bSetThreshold:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "[PICF]\n"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v1, "        lcb           = "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->lcb:I

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const/16 v1, 0xa

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v2, "        cbHeader      = "

    .line 27
    .line 28
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->cbHeader:I

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v2, "        mfp.mm        = "

    .line 40
    .line 41
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_mm:I

    .line 45
    .line 46
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    const-string v2, "        mfp.xExt      = "

    .line 53
    .line 54
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_xExt:I

    .line 58
    .line 59
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v2, "        mfp.yExt      = "

    .line 66
    .line 67
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_yExt:I

    .line 71
    .line 72
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const-string v2, "        mfp.hMF       = "

    .line 79
    .line 80
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->mfp_hMF:I

    .line 84
    .line 85
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v2, "        offset14      = "

    .line 92
    .line 93
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->offset14:[B

    .line 97
    .line 98
    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    const-string v2, "        dxaGoal       = "

    .line 109
    .line 110
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    iget-short v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaGoal:S

    .line 114
    .line 115
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    const-string v2, "        dyaGoal       = "

    .line 122
    .line 123
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    iget-short v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaGoal:S

    .line 127
    .line 128
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    const-string v2, "        dxaCropLeft   = "

    .line 135
    .line 136
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaCropLeft:F

    .line 140
    .line 141
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    const-string v2, "        dyaCropTop    = "

    .line 148
    .line 149
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaCropTop:F

    .line 153
    .line 154
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    const-string v2, "        dxaCropRight  = "

    .line 161
    .line 162
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dxaCropRight:F

    .line 166
    .line 167
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    const-string v2, "        dyaCropBottom = "

    .line 174
    .line 175
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/PictureDescriptor;->dyaCropBottom:F

    .line 179
    .line 180
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    const-string v1, "[/PICF]"

    .line 187
    .line 188
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v0

    .line 195
    return-object v0
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

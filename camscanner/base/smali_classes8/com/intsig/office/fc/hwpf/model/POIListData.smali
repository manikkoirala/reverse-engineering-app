.class public final Lcom/intsig/office/fc/hwpf/model/POIListData;
.super Ljava/lang/Object;
.source "POIListData.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static _fRestartHdn:Lcom/intsig/office/fc/util/BitField;

.field private static _fSimpleList:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field private _info:B

.field _levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

.field private _lsid:I

.field private _reserved:B

.field private _rgistd:[S

.field private _tplc:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_fSimpleList:Lcom/intsig/office/fc/util/BitField;

    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_fRestartHdn:Lcom/intsig/office/fc/util/BitField;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(IZ)V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_lsid:I

    const/16 p1, 0x9

    new-array v0, p1, [S

    .line 3
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 4
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    const/16 v3, 0xfff

    aput-short v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-array p1, p1, [Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 6
    :goto_1
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 7
    new-instance v1, Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    invoke-direct {v1, v0, p2}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;-><init>(IZ)V

    aput-object v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method constructor <init>([BI)V
    .locals 4

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_lsid:I

    add-int/lit8 p2, p2, 0x4

    .line 10
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_tplc:I

    add-int/lit8 p2, p2, 0x4

    const/16 v0, 0x9

    new-array v1, v0, [S

    .line 11
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 12
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v3

    aput-short v3, v2, v1

    add-int/lit8 p2, p2, 0x2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v1, p2, 0x1

    .line 13
    aget-byte p2, p1, p2

    iput-byte p2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_info:B

    .line 14
    aget-byte p1, p1, v1

    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_reserved:B

    .line 15
    sget-object p1, Lcom/intsig/office/fc/hwpf/model/POIListData;->_fSimpleList:Lcom/intsig/office/fc/util/BitField;

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    move-result p1

    if-lez p1, :cond_1

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 16
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    goto :goto_1

    :cond_1
    new-array p1, v0, [Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 17
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    :goto_1
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 6
    .line 7
    iget-byte v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListData;->_info:B

    .line 8
    .line 9
    iget-byte v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_info:B

    .line 10
    .line 11
    if-ne v1, v2, :cond_1

    .line 12
    .line 13
    iget-object v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 16
    .line 17
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    iget v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListData;->_lsid:I

    .line 24
    .line 25
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_lsid:I

    .line 26
    .line 27
    if-ne v1, v2, :cond_1

    .line 28
    .line 29
    iget-byte v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListData;->_reserved:B

    .line 30
    .line 31
    iget-byte v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_reserved:B

    .line 32
    .line 33
    if-ne v1, v2, :cond_1

    .line 34
    .line 35
    iget v1, p1, Lcom/intsig/office/fc/hwpf/model/POIListData;->_tplc:I

    .line 36
    .line 37
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_tplc:I

    .line 38
    .line 39
    if-ne v1, v2, :cond_1

    .line 40
    .line 41
    iget-object p1, p1, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    .line 44
    .line 45
    invoke-static {p1, v1}, Ljava/util/Arrays;->equals([S[S)Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    if-eqz p1, :cond_1

    .line 50
    .line 51
    const/4 v0, 0x1

    .line 52
    :cond_1
    return v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getLevel(I)Lcom/intsig/office/fc/hwpf/model/POIListLevel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 2
    .line 3
    add-int/lit8 p1, p1, -0x1

    .line 4
    .line 5
    aget-object p1, v0, p1

    .line 6
    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getLevelStyle(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    .line 2
    .line 3
    aget-short p1, v0, p1

    .line 4
    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getLevels()[Lcom/intsig/office/fc/hwpf/model/POIListLevel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLsid()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_lsid:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public numLevels()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method resetListID()I
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/Math;->random()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    long-to-double v2, v2

    .line 10
    mul-double v0, v0, v2

    .line 11
    .line 12
    double-to-int v0, v0

    .line 13
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_lsid:I

    .line 14
    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setLevel(ILcom/intsig/office/fc/hwpf/model/POIListLevel;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_levels:[Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 2
    .line 3
    aput-object p2, v0, p1

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setLevelStyle(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    .line 2
    .line 3
    int-to-short p2, p2

    .line 4
    aput-short p2, v0, p1

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public toByteArray()[B
    .locals 4

    .line 1
    const/16 v0, 0x1c

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_lsid:I

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 8
    .line 9
    .line 10
    const/4 v1, 0x4

    .line 11
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_tplc:I

    .line 12
    .line 13
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 14
    .line 15
    .line 16
    const/16 v1, 0x8

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    :goto_0
    const/16 v3, 0x9

    .line 20
    .line 21
    if-ge v2, v3, :cond_0

    .line 22
    .line 23
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_rgistd:[S

    .line 24
    .line 25
    aget-short v3, v3, v2

    .line 26
    .line 27
    invoke-static {v0, v1, v3}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 28
    .line 29
    .line 30
    add-int/lit8 v1, v1, 0x2

    .line 31
    .line 32
    add-int/lit8 v2, v2, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    add-int/lit8 v2, v1, 0x1

    .line 36
    .line 37
    iget-byte v3, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_info:B

    .line 38
    .line 39
    aput-byte v3, v0, v1

    .line 40
    .line 41
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/POIListData;->_reserved:B

    .line 42
    .line 43
    aput-byte v1, v0, v2

    .line 44
    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

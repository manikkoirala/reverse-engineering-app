.class public Lcom/intsig/office/fc/hwpf/usermodel/ObjectPoolImpl;
.super Ljava/lang/Object;
.source "ObjectPoolImpl.java"

# interfaces
.implements Lcom/intsig/office/fc/hwpf/usermodel/ObjectsPool;


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# instance fields
.field private _objectPool:Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/ObjectPoolImpl;->_objectPool:Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public getObjectById(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/ObjectPoolImpl;->_objectPool:Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return-object v1

    .line 7
    :cond_0
    :try_start_0
    invoke-interface {v0, p1}, Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;->getEntry(Ljava/lang/String;)Lcom/intsig/office/fc/poifs/filesystem/Entry;

    .line 8
    .line 9
    .line 10
    move-result-object p1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    return-object p1

    .line 12
    :catch_0
    return-object v1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public writeTo(Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/ObjectPoolImpl;->_objectPool:Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0, p1}, Lcom/intsig/office/fc/util/POIUtils;->copyNodeRecursively(Lcom/intsig/office/fc/poifs/filesystem/Entry;Lcom/intsig/office/fc/poifs/filesystem/DirectoryEntry;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public final Lcom/intsig/office/fc/hwpf/sprm/TableSprmUncompressor;
.super Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;
.source "TableSprmUncompressor.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final logger:Lcom/intsig/office/fc/util/POILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/sprm/TableSprmUncompressor;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/sprm/TableSprmUncompressor;->logger:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static unCompressTAPOperation(Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V
    .locals 10

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    move-result v0

    if-eqz v0, :cond_16

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_15

    const/4 v3, 0x2

    if-eq v0, v3, :cond_13

    const/4 v4, 0x3

    if-eq v0, v4, :cond_12

    const/4 v5, 0x4

    if-eq v0, v5, :cond_11

    const/4 v6, 0x5

    if-eq v0, v6, :cond_10

    const/4 v6, 0x7

    if-eq v0, v6, :cond_f

    const/16 v6, 0x8

    if-eq v0, v6, :cond_a

    const/16 v7, 0x21

    if-eq v0, v7, :cond_7

    const/16 v1, 0x61

    if-eq v0, v1, :cond_6

    const/16 v1, 0x33

    if-eq v0, v1, :cond_5

    const/16 v1, 0x34

    if-eq v0, v1, :cond_0

    goto/16 :goto_7

    .line 2
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v1

    aget-byte v0, v0, v1

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v6

    add-int/2addr v6, v2

    aget-byte v1, v1, v6

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v6

    add-int/2addr v6, v3

    aget-byte v2, v2, v6

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v3

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v6

    add-int/2addr v6, v4

    aget-byte v3, v3, v6

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v4

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    add-int/2addr p1, v5

    invoke-static {v4, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result p1

    :goto_0
    if-ge v0, v1, :cond_17

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgtc()[Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    move-result-object v4

    aget-object v4, v4, v0

    and-int/lit8 v5, v2, 0x1

    if-eqz v5, :cond_1

    .line 8
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setFtsCellPaddingTop(B)V

    .line 9
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setWCellPaddingTop(S)V

    :cond_1
    and-int/lit8 v5, v2, 0x2

    if-eqz v5, :cond_2

    .line 10
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setFtsCellPaddingLeft(B)V

    .line 11
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setWCellPaddingLeft(S)V

    :cond_2
    and-int/lit8 v5, v2, 0x4

    if-eqz v5, :cond_3

    .line 12
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setFtsCellPaddingBottom(B)V

    .line 13
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setWCellPaddingBottom(S)V

    :cond_3
    and-int/lit8 v5, v2, 0x8

    if-eqz v5, :cond_4

    .line 14
    invoke-virtual {v4, v3}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setFtsCellPaddingRight(B)V

    .line 15
    invoke-virtual {v4, p1}, Lcom/intsig/office/fc/hwpf/model/types/TCAbstractType;->setWCellPaddingRight(S)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 16
    :cond_5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    add-int/2addr p1, v5

    invoke-static {v0, p1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setTCellSpacingDefault(S)V

    goto/16 :goto_7

    .line 17
    :cond_6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    shr-int/2addr p1, v6

    int-to-short p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setTableIndent(S)V

    goto/16 :goto_7

    .line 18
    :cond_7
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    const/high16 v0, -0x1000000

    and-int/2addr v0, p1

    shr-int/lit8 v0, v0, 0x18

    const/high16 v2, 0xff0000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x10

    const v3, 0xffff

    and-int/2addr p1, v3

    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getItcMac()S

    move-result v3

    add-int v4, v3, v2

    add-int/lit8 v5, v4, 0x1

    .line 20
    new-array v5, v5, [S

    .line 21
    new-array v4, v4, [Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    if-lt v0, v3, :cond_8

    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgdxaCenter()[S

    move-result-object v0

    add-int/lit8 v6, v3, 0x1

    invoke-static {v0, v1, v5, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgtc()[Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    move-result-object p0

    invoke-static {p0, v1, v4, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v0, v3

    goto :goto_1

    .line 24
    :cond_8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgdxaCenter()[S

    move-result-object v6

    add-int/lit8 v7, v0, 0x1

    invoke-static {v6, v1, v5, v1, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 25
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgdxaCenter()[S

    move-result-object v6

    add-int v8, v0, v2

    sub-int/2addr v3, v0

    invoke-static {v6, v7, v5, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgtc()[Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    move-result-object v6

    invoke-static {v6, v1, v4, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 27
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgtc()[Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    move-result-object p0

    invoke-static {p0, v0, v4, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_1
    move p0, v0

    :goto_2
    add-int v1, v0, v2

    if-ge p0, v1, :cond_9

    .line 28
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    invoke-direct {v1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;-><init>()V

    aput-object v1, v4, p0

    add-int/lit8 v1, p0, -0x1

    .line 29
    aget-short v1, v5, v1

    add-int/2addr v1, p1

    int-to-short v1, v1

    aput-short v1, v5, p0

    add-int/lit8 p0, p0, 0x1

    goto :goto_2

    :cond_9
    add-int/lit8 p0, v1, -0x1

    .line 30
    aget-short p0, v5, p0

    add-int/2addr p0, p1

    int-to-short p0, p0

    aput-short p0, v5, v1

    goto/16 :goto_7

    .line 31
    :cond_a
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v0

    .line 32
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v4

    .line 33
    aget-byte v5, v0, v4

    int-to-short v5, v5

    add-int/lit8 v6, v5, 0x1

    .line 34
    new-array v7, v6, [S

    .line 35
    new-array v8, v5, [Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    .line 36
    invoke-virtual {p0, v5}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setItcMac(S)V

    .line 37
    invoke-virtual {p0, v7}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setRgdxaCenter([S)V

    .line 38
    invoke-virtual {p0, v8}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setRgtc([Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;)V

    const/4 p0, 0x0

    :goto_3
    if-ge p0, v5, :cond_b

    mul-int/lit8 v9, p0, 0x2

    add-int/2addr v9, v2

    add-int/2addr v9, v4

    .line 39
    invoke-static {v0, v9}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v9

    aput-short v9, v7, p0

    add-int/lit8 p0, p0, 0x1

    goto :goto_3

    .line 40
    :cond_b
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->size()I

    move-result p0

    add-int/2addr p0, v4

    add-int/lit8 p0, p0, -0x6

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v2

    add-int p1, v4, v6

    if-ge p1, p0, :cond_c

    const/4 p0, 0x1

    goto :goto_4

    :cond_c
    const/4 p0, 0x0

    :goto_4
    if-ge v1, v5, :cond_e

    if-eqz p0, :cond_d

    mul-int/lit8 p1, v1, 0x14

    add-int/2addr p1, v6

    add-int/2addr p1, v4

    .line 41
    array-length v3, v0

    if-ge p1, v3, :cond_d

    .line 42
    invoke-static {v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;->convertBytesToTC([BI)Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    move-result-object p1

    aput-object p1, v8, v1

    goto :goto_5

    .line 43
    :cond_d
    new-instance p1, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;

    invoke-direct {p1}, Lcom/intsig/office/fc/hwpf/usermodel/TableCellDescriptor;-><init>()V

    aput-object p1, v8, v1

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_e
    mul-int/lit8 p0, v5, 0x2

    add-int/2addr p0, v2

    add-int/2addr v4, p0

    .line 44
    invoke-static {v0, v4}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result p0

    aput-short p0, v7, v5

    goto/16 :goto_7

    .line 45
    :cond_f
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setDyaRowHeight(I)V

    goto/16 :goto_7

    .line 46
    :cond_10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v0

    .line 47
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result p1

    .line 48
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setBrcTop(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    add-int/2addr p1, v5

    .line 49
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setBrcLeft(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    add-int/2addr p1, v5

    .line 50
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setBrcBottom(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    add-int/2addr p1, v5

    .line 51
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setBrcRight(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    add-int/2addr p1, v5

    .line 52
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setBrcHorizontal(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    add-int/2addr p1, v5

    .line 53
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    invoke-direct {v1, v0, p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    invoke-virtual {p0, v1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setBrcVertical(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    goto :goto_7

    .line 54
    :cond_11
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setFTableHeader(Z)V

    goto :goto_7

    .line 55
    :cond_12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setFCantSplit(Z)V

    goto :goto_7

    .line 56
    :cond_13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgdxaCenter()[S

    move-result-object v0

    if-eqz v0, :cond_14

    .line 57
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getDxaGapHalf()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result v3

    sub-int/2addr v2, v3

    .line 58
    aget-short v3, v0, v1

    add-int/2addr v3, v2

    int-to-short v2, v3

    aput-short v2, v0, v1

    .line 59
    :cond_14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setDxaGapHalf(I)V

    goto :goto_7

    .line 60
    :cond_15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getRgdxaCenter()[S

    move-result-object v0

    .line 61
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getItcMac()S

    move-result v2

    .line 62
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    aget-short v3, v0, v1

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->getDxaGapHalf()I

    move-result p0

    add-int/2addr v3, p0

    sub-int/2addr p1, v3

    :goto_6
    if-ge v1, v2, :cond_17

    .line 63
    aget-short p0, v0, v1

    add-int/2addr p0, p1

    int-to-short p0, p0

    aput-short p0, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 64
    :cond_16
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    move-result p1

    int-to-short p1, p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/TAPAbstractType;->setJc(S)V

    :cond_17
    :goto_7
    return-void
.end method

.method public static uncompressTAP(Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;
    .locals 10

    const/16 v0, -0x29f8

    .line 8
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->findSprm(S)Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    move-result-object v2

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    move-result v0

    .line 11
    aget-byte v0, v2, v0

    int-to-short v0, v0

    .line 12
    new-instance v2, Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;

    invoke-direct {v2, v0}, Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;-><init>(S)V

    goto :goto_0

    .line 13
    :cond_0
    sget-object v0, Lcom/intsig/office/fc/hwpf/sprm/TableSprmUncompressor;->logger:Lcom/intsig/office/fc/util/POILogger;

    sget v2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    const-string v3, "Some table rows didn\'t specify number of columns in SPRMs"

    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 14
    new-instance v2, Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;

    invoke-direct {v2, v1}, Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;-><init>(S)V

    :goto_0
    const/16 v0, -0x29cd

    .line 15
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->findSprm(S)Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->iterator()Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    move-result-object p0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    move-result-object v6

    .line 18
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    move-result v0

    const/4 v3, 0x5

    if-eq v0, v3, :cond_2

    .line 19
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 20
    :cond_2
    :try_start_0
    invoke-static {v2, v6}, Lcom/intsig/office/fc/hwpf/sprm/TableSprmUncompressor;->unCompressTAPOperation(Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v9

    .line 21
    sget-object v3, Lcom/intsig/office/fc/hwpf/sprm/TableSprmUncompressor;->logger:Lcom/intsig/office/fc/util/POILogger;

    sget v4, Lcom/intsig/office/fc/util/POILogger;->ERROR:I

    const-string v5, "Unable to apply "

    const-string v7, ": "

    move-object v8, v9

    invoke-virtual/range {v3 .. v9}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    return-object v2
.end method

.method public static uncompressTAP([BI)Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;

    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;-><init>()V

    .line 2
    new-instance v1, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    invoke-direct {v1, p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 3
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 4
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    move-result-object v5

    .line 5
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    move-result p0

    const/4 p1, 0x5

    if-ne p0, p1, :cond_0

    .line 6
    :try_start_0
    invoke-static {v0, v5}, Lcom/intsig/office/fc/hwpf/sprm/TableSprmUncompressor;->unCompressTAPOperation(Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    .line 7
    sget-object v2, Lcom/intsig/office/fc/hwpf/sprm/TableSprmUncompressor;->logger:Lcom/intsig/office/fc/util/POILogger;

    sget v3, Lcom/intsig/office/fc/util/POILogger;->ERROR:I

    const-string v4, "Unable to apply "

    const-string v6, ": "

    move-object v7, v8

    invoke-virtual/range {v2 .. v8}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    return-object v0
.end method

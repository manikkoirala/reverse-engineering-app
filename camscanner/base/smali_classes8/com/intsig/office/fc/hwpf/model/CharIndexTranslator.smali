.class public interface abstract Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;
.super Ljava/lang/Object;
.source "CharIndexTranslator.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# virtual methods
.method public abstract getByteIndex(I)I
.end method

.method public abstract getCharIndex(I)I
.end method

.method public abstract getCharIndex(II)I
.end method

.method public abstract isIndexInTable(I)Z
.end method

.method public abstract lookIndexBackward(I)I
.end method

.method public abstract lookIndexForward(I)I
.end method

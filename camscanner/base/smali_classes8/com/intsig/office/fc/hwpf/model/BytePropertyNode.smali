.class public abstract Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;
.super Lcom/intsig/office/fc/hwpf/model/PropertyNode;
.source "BytePropertyNode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/intsig/office/fc/hwpf/model/BytePropertyNode<",
        "TT;>;>",
        "Lcom/intsig/office/fc/hwpf/model/PropertyNode<",
        "TT;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final endBytes:I

.field private final startBytes:I


# direct methods
.method public constructor <init>(IILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;Ljava/lang/Object;)V
    .locals 2

    .line 1
    invoke-interface {p3, p1}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getCharIndex(I)I

    move-result v0

    .line 2
    invoke-interface {p3, p1}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getCharIndex(I)I

    move-result v1

    invoke-interface {p3, p2, v1}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getCharIndex(II)I

    move-result p3

    .line 3
    invoke-direct {p0, v0, p3, p4}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    if-gt p1, p2, :cond_0

    .line 4
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;->startBytes:I

    .line 5
    iput p2, p0, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;->endBytes:I

    return-void

    .line 6
    :cond_0
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "fcStart ("

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ") > fcEnd ("

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3
.end method

.method public constructor <init>(IILjava/lang/Object;)V
    .locals 2

    .line 7
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;-><init>(IILjava/lang/Object;)V

    if-gt p1, p2, :cond_0

    const/4 p1, -0x1

    .line 8
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;->startBytes:I

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;->endBytes:I

    return-void

    .line 10
    :cond_0
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "charStart ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ") > charEnd ("

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3
.end method


# virtual methods
.method public getEndBytes()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;->endBytes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStartBytes()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/BytePropertyNode;->startBytes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

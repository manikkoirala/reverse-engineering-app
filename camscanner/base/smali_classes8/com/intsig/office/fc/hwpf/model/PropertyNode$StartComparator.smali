.class public final Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;
.super Ljava/lang/Object;
.source "PropertyNode.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/hwpf/model/PropertyNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/intsig/office/fc/hwpf/model/PropertyNode<",
        "*>;>;"
    }
.end annotation


# static fields
.field public static instance:Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;->instance:Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public compare(Lcom/intsig/office/fc/hwpf/model/PropertyNode;Lcom/intsig/office/fc/hwpf/model/PropertyNode;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/hwpf/model/PropertyNode<",
            "*>;",
            "Lcom/intsig/office/fc/hwpf/model/PropertyNode<",
            "*>;)I"
        }
    .end annotation

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result p1

    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result p2

    if-ge p1, p2, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    if-ne p1, p2, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PropertyNode;

    check-cast p2, Lcom/intsig/office/fc/hwpf/model/PropertyNode;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;->compare(Lcom/intsig/office/fc/hwpf/model/PropertyNode;Lcom/intsig/office/fc/hwpf/model/PropertyNode;)I

    move-result p1

    return p1
.end method

.class public interface abstract Lcom/intsig/office/fc/hwpf/usermodel/Fields;
.super Ljava/lang/Object;
.source "Fields.java"


# virtual methods
.method public abstract getFieldByStartOffset(Lcom/intsig/office/fc/hwpf/model/FieldsDocumentPart;I)Lcom/intsig/office/fc/hwpf/usermodel/Field;
.end method

.method public abstract getFields(Lcom/intsig/office/fc/hwpf/model/FieldsDocumentPart;)Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/fc/hwpf/model/FieldsDocumentPart;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/intsig/office/fc/hwpf/usermodel/Field;",
            ">;"
        }
    .end annotation
.end method

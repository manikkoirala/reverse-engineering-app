.class public Lcom/intsig/office/fc/hwpf/model/SectionTable;
.super Ljava/lang/Object;
.source "SectionTable.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final SED_SIZE:I = 0xc

.field private static final _logger:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field protected _sections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/SEPX;",
            ">;"
        }
    .end annotation
.end field

.field protected _text:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hwpf/model/TextPiece;",
            ">;"
        }
    .end annotation
.end field

.field private tpt:Lcom/intsig/office/fc/hwpf/model/TextPieceTable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/model/SectionTable;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_logger:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>([B[BIIILcom/intsig/office/fc/hwpf/model/TextPieceTable;Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;)V
    .locals 5

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance p5, Ljava/util/ArrayList;

    invoke-direct {p5}, Ljava/util/ArrayList;-><init>()V

    iput-object p5, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 5
    new-instance p5, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    const/16 v0, 0xc

    invoke-direct {p5, p2, p3, p4, v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 6
    iput-object p6, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->tpt:Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    .line 7
    invoke-virtual {p6}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_text:Ljava/util/List;

    .line 8
    invoke-virtual {p5}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    move-result p2

    const/4 p3, 0x0

    const/4 p4, 0x0

    :goto_0
    if-ge p4, p2, :cond_1

    .line 9
    invoke-virtual {p5, p4}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    move-result-object p6

    .line 10
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;

    invoke-virtual {p6}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;-><init>([BI)V

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;->getFc()I

    move-result v1

    .line 12
    invoke-virtual {p6}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v2

    .line 13
    invoke-virtual {p6}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result p6

    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 14
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    new-instance v3, Lcom/intsig/office/fc/hwpf/model/SEPX;

    new-array v4, p3, [B

    invoke-direct {v3, v0, v2, p6, v4}, Lcom/intsig/office/fc/hwpf/model/SEPX;-><init>(Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;II[B)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 15
    :cond_0
    invoke-static {p1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v3

    .line 16
    new-array v4, v3, [B

    add-int/lit8 v1, v1, 0x2

    .line 17
    invoke-static {p1, v1, v4, p3, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 18
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    new-instance v3, Lcom/intsig/office/fc/hwpf/model/SEPX;

    invoke-direct {v3, v0, v2, p6, v4}, Lcom/intsig/office/fc/hwpf/model/SEPX;-><init>(Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;II[B)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    .line 19
    :cond_1
    invoke-virtual {p7}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;->getMainDocumentEnd()I

    move-result p1

    const/4 p2, 0x0

    const/4 p4, 0x0

    const/4 p6, 0x0

    .line 20
    :goto_2
    iget-object p7, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {p7}, Ljava/util/ArrayList;->size()I

    move-result p7

    if-ge p2, p7, :cond_5

    .line 21
    iget-object p7, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {p7, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p7

    check-cast p7, Lcom/intsig/office/fc/hwpf/model/SEPX;

    .line 22
    invoke-virtual {p7}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, p1, :cond_2

    const/4 p4, 0x1

    goto :goto_3

    .line 23
    :cond_2
    invoke-virtual {p7}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v0

    if-eq v0, p1, :cond_3

    invoke-virtual {p7}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result p7

    add-int/lit8 v0, p1, -0x1

    if-ne p7, v0, :cond_4

    :cond_3
    const/4 p6, 0x1

    :cond_4
    :goto_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_5
    if-nez p4, :cond_6

    if-eqz p6, :cond_6

    .line 24
    sget-object p1, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_logger:Lcom/intsig/office/fc/util/POILogger;

    sget p2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    const-string p4, "Your document seemed to be mostly unicode, but the section definition was in bytes! Trying anyway, but things may well go wrong!"

    .line 25
    invoke-virtual {p1, p2, p4}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 26
    :goto_4
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge p3, p1, :cond_6

    .line 27
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/hwpf/model/SEPX;

    .line 28
    invoke-virtual {p5, p3}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    move-result-object p2

    .line 29
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result p4

    .line 30
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result p2

    .line 31
    invoke-virtual {p1, p4}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 32
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    add-int/lit8 p3, p3, 0x1

    goto :goto_4

    .line 33
    :cond_6
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    sget-object p2, Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;->instance:Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;

    invoke-static {p1, p2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method


# virtual methods
.method public adjustForInsert(II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/SEPX;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    add-int/2addr v2, p2

    .line 20
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 21
    .line 22
    .line 23
    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 24
    .line 25
    if-ge p1, v0, :cond_0

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/SEPX;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    add-int/2addr v2, p2

    .line 40
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    add-int/2addr v2, p2

    .line 48
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getSections()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/SEPX;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string p2, "WordDocument"

    .line 1
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;

    move-result-object p2

    const-string v0, "1Table"

    .line 2
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;

    move-result-object p1

    .line 3
    invoke-virtual {p0, p2, p1}, Lcom/intsig/office/fc/hwpf/model/SectionTable;->writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V

    return-void
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 6
    new-instance v2, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    const/16 v3, 0xc

    invoke-direct {v2, v3}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    .line 7
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/SectionTable;->_sections:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/hwpf/model/SEPX;

    .line 8
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/SEPX;->getGrpprl()[B

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [B

    .line 9
    array-length v7, v5

    int-to-short v7, v7

    invoke-static {v6, v7}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BS)V

    .line 10
    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write([B)V

    .line 11
    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V

    .line 12
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/SEPX;->getSectionDescriptor()Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;

    move-result-object v5

    .line 13
    invoke-virtual {v5, v0}, Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;->setFc(I)V

    .line 14
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v6

    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v4

    .line 15
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/SectionDescriptor;->toByteArray()[B

    move-result-object v5

    invoke-direct {v0, v6, v4, v5}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    .line 16
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->addProperty(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)V

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

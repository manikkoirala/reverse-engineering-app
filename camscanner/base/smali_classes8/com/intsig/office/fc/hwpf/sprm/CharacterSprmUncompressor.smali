.class public final Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;
.super Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;
.source "CharacterSprmUncompressor.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static getCHPFlag(BZ)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p0, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x1

    .line 6
    if-ne p0, v1, :cond_1

    .line 7
    .line 8
    return v1

    .line 9
    :cond_1
    const/16 v2, 0x81

    .line 10
    .line 11
    and-int/2addr p0, v2

    .line 12
    const/16 v3, 0x80

    .line 13
    .line 14
    if-ne p0, v3, :cond_2

    .line 15
    .line 16
    return p1

    .line 17
    :cond_2
    if-ne p0, v2, :cond_3

    .line 18
    .line 19
    xor-int/lit8 p0, p1, 0x1

    .line 20
    .line 21
    return p0

    .line 22
    :cond_3
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static unCompressCHPOperation(Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V
    .locals 6

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0xc

    .line 6
    .line 7
    if-eq v0, v1, :cond_d

    .line 8
    .line 9
    const/16 v1, 0xe

    .line 10
    .line 11
    if-eq v0, v1, :cond_c

    .line 12
    .line 13
    const/16 v1, 0x30

    .line 14
    .line 15
    if-eq v0, v1, :cond_b

    .line 16
    .line 17
    const/16 v1, 0x77

    .line 18
    .line 19
    if-eq v0, v1, :cond_a

    .line 20
    .line 21
    const/16 v1, 0x32

    .line 22
    .line 23
    const/4 v2, 0x0

    .line 24
    if-eq v0, v1, :cond_9

    .line 25
    .line 26
    const/16 v1, 0x33

    .line 27
    .line 28
    if-eq v0, v1, :cond_8

    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    packed-switch v0, :pswitch_data_0

    .line 32
    .line 33
    .line 34
    const/4 v3, 0x2

    .line 35
    packed-switch v0, :pswitch_data_1

    .line 36
    .line 37
    .line 38
    const/16 v4, 0x8

    .line 39
    .line 40
    packed-switch v0, :pswitch_data_2

    .line 41
    .line 42
    .line 43
    packed-switch v0, :pswitch_data_3

    .line 44
    .line 45
    .line 46
    packed-switch v0, :pswitch_data_4

    .line 47
    .line 48
    .line 49
    packed-switch v0, :pswitch_data_5

    .line 50
    .line 51
    .line 52
    packed-switch v0, :pswitch_data_6

    .line 53
    .line 54
    .line 55
    packed-switch v0, :pswitch_data_7

    .line 56
    .line 57
    .line 58
    goto/16 :goto_1

    .line 59
    .line 60
    :pswitch_0
    new-instance p0, Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 61
    .line 62
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 63
    .line 64
    .line 65
    move-result p2

    .line 66
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hwpf/model/Colorref;-><init>(I)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setCv(Lcom/intsig/office/fc/hwpf/model/Colorref;)V

    .line 70
    .line 71
    .line 72
    goto/16 :goto_1

    .line 73
    .line 74
    :pswitch_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 75
    .line 76
    .line 77
    move-result p0

    .line 78
    int-to-byte p0, p0

    .line 79
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIdctHint(B)V

    .line 80
    .line 81
    .line 82
    goto/16 :goto_1

    .line 83
    .line 84
    :pswitch_2
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 85
    .line 86
    .line 87
    move-result p0

    .line 88
    int-to-short p0, p0

    .line 89
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setLidFE(I)V

    .line 90
    .line 91
    .line 92
    goto/16 :goto_1

    .line 93
    .line 94
    :pswitch_3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 95
    .line 96
    .line 97
    move-result p0

    .line 98
    int-to-short p0, p0

    .line 99
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setLidDefault(I)V

    .line 100
    .line 101
    .line 102
    goto/16 :goto_1

    .line 103
    .line 104
    :pswitch_4
    new-instance p0, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 105
    .line 106
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 111
    .line 112
    .line 113
    move-result p2

    .line 114
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;-><init>([BI)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setShd(Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;)V

    .line 118
    .line 119
    .line 120
    goto/16 :goto_1

    .line 121
    .line 122
    :pswitch_5
    new-instance p0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 123
    .line 124
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 129
    .line 130
    .line 131
    move-result p2

    .line 132
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>([BI)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setBrc(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 136
    .line 137
    .line 138
    goto/16 :goto_1

    .line 139
    .line 140
    :pswitch_6
    new-instance p0, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 141
    .line 142
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 147
    .line 148
    .line 149
    move-result p2

    .line 150
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>([BI)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmRMarkDel(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 154
    .line 155
    .line 156
    goto/16 :goto_1

    .line 157
    .line 158
    :pswitch_7
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 159
    .line 160
    .line 161
    move-result p0

    .line 162
    int-to-short p0, p0

    .line 163
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIbstRMarkDel(I)V

    .line 164
    .line 165
    .line 166
    goto/16 :goto_1

    .line 167
    .line 168
    :pswitch_8
    const/16 p0, 0x20

    .line 169
    .line 170
    new-array v0, p0, [B

    .line 171
    .line 172
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 173
    .line 174
    .line 175
    move-result-object v3

    .line 176
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 177
    .line 178
    .line 179
    move-result p2

    .line 180
    aget-byte v4, v3, p2

    .line 181
    .line 182
    if-eqz v4, :cond_0

    .line 183
    .line 184
    goto :goto_0

    .line 185
    :cond_0
    const/4 v1, 0x0

    .line 186
    :goto_0
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFDispFldRMark(Z)V

    .line 187
    .line 188
    .line 189
    add-int/lit8 v1, p2, 0x1

    .line 190
    .line 191
    invoke-static {v3, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 192
    .line 193
    .line 194
    move-result v1

    .line 195
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIbstDispFldRMark(I)V

    .line 196
    .line 197
    .line 198
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 199
    .line 200
    add-int/lit8 v4, p2, 0x3

    .line 201
    .line 202
    invoke-direct {v1, v3, v4}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>([BI)V

    .line 203
    .line 204
    .line 205
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmDispFldRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 206
    .line 207
    .line 208
    add-int/lit8 p2, p2, 0x7

    .line 209
    .line 210
    invoke-static {v3, p2, v0, v2, p0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setXstDispFldRMark([B)V

    .line 214
    .line 215
    .line 216
    goto/16 :goto_1

    .line 217
    .line 218
    :pswitch_9
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 219
    .line 220
    .line 221
    move-result p0

    .line 222
    int-to-byte p0, p0

    .line 223
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setSfxtText(B)V

    .line 224
    .line 225
    .line 226
    goto/16 :goto_1

    .line 227
    .line 228
    :pswitch_a
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 229
    .line 230
    .line 231
    move-result p0

    .line 232
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 233
    .line 234
    .line 235
    move-result p0

    .line 236
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFEmboss(Z)V

    .line 237
    .line 238
    .line 239
    goto/16 :goto_1

    .line 240
    .line 241
    :pswitch_b
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 242
    .line 243
    .line 244
    move-result-object p0

    .line 245
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 246
    .line 247
    .line 248
    move-result p2

    .line 249
    aget-byte v0, p0, p2

    .line 250
    .line 251
    if-eqz v0, :cond_1

    .line 252
    .line 253
    const/4 v2, 0x1

    .line 254
    :cond_1
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFPropRMark(Z)V

    .line 255
    .line 256
    .line 257
    add-int/lit8 v0, p2, 0x1

    .line 258
    .line 259
    invoke-static {p0, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 260
    .line 261
    .line 262
    move-result v0

    .line 263
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIbstPropRMark(I)V

    .line 264
    .line 265
    .line 266
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 267
    .line 268
    add-int/lit8 p2, p2, 0x3

    .line 269
    .line 270
    invoke-direct {v0, p0, p2}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>([BI)V

    .line 271
    .line 272
    .line 273
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmPropRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 274
    .line 275
    .line 276
    goto/16 :goto_1

    .line 277
    .line 278
    :pswitch_c
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 279
    .line 280
    .line 281
    move-result p0

    .line 282
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 283
    .line 284
    .line 285
    move-result p0

    .line 286
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFObj(Z)V

    .line 287
    .line 288
    .line 289
    goto/16 :goto_1

    .line 290
    .line 291
    :pswitch_d
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 292
    .line 293
    .line 294
    move-result p0

    .line 295
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 296
    .line 297
    .line 298
    move-result p0

    .line 299
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSpec(Z)V

    .line 300
    .line 301
    .line 302
    goto/16 :goto_1

    .line 303
    .line 304
    :pswitch_e
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 305
    .line 306
    .line 307
    move-result p0

    .line 308
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 309
    .line 310
    .line 311
    move-result p0

    .line 312
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFImprint(Z)V

    .line 313
    .line 314
    .line 315
    goto/16 :goto_1

    .line 316
    .line 317
    :pswitch_f
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 318
    .line 319
    .line 320
    move-result p0

    .line 321
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 322
    .line 323
    .line 324
    move-result p0

    .line 325
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFDStrike(Z)V

    .line 326
    .line 327
    .line 328
    goto/16 :goto_1

    .line 329
    .line 330
    :pswitch_10
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 331
    .line 332
    .line 333
    move-result p0

    .line 334
    int-to-short p0, p0

    .line 335
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFtcOther(I)V

    .line 336
    .line 337
    .line 338
    goto/16 :goto_1

    .line 339
    .line 340
    :pswitch_11
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 341
    .line 342
    .line 343
    move-result p0

    .line 344
    int-to-short p0, p0

    .line 345
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFtcFE(I)V

    .line 346
    .line 347
    .line 348
    goto/16 :goto_1

    .line 349
    .line 350
    :pswitch_12
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 351
    .line 352
    .line 353
    move-result p0

    .line 354
    int-to-short p0, p0

    .line 355
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFtcAscii(I)V

    .line 356
    .line 357
    .line 358
    goto/16 :goto_1

    .line 359
    .line 360
    :pswitch_13
    new-instance p0, Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 361
    .line 362
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 363
    .line 364
    .line 365
    move-result p2

    .line 366
    int-to-short p2, p2

    .line 367
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hwpf/model/Hyphenation;-><init>(S)V

    .line 368
    .line 369
    .line 370
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHresi(Lcom/intsig/office/fc/hwpf/model/Hyphenation;)V

    .line 371
    .line 372
    .line 373
    goto/16 :goto_1

    .line 374
    .line 375
    :pswitch_14
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 376
    .line 377
    .line 378
    move-result p0

    .line 379
    int-to-float p0, p0

    .line 380
    const/high16 p2, 0x42c80000    # 100.0f

    .line 381
    .line 382
    div-float/2addr p0, p2

    .line 383
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 384
    .line 385
    .line 386
    move-result p2

    .line 387
    int-to-float p2, p2

    .line 388
    mul-float p0, p0, p2

    .line 389
    .line 390
    float-to-int p0, p0

    .line 391
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 392
    .line 393
    .line 394
    move-result p2

    .line 395
    add-int/2addr p2, p0

    .line 396
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 397
    .line 398
    .line 399
    goto/16 :goto_1

    .line 400
    .line 401
    :pswitch_15
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 402
    .line 403
    .line 404
    move-result p0

    .line 405
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHpsKern(I)V

    .line 406
    .line 407
    .line 408
    goto/16 :goto_1

    .line 409
    .line 410
    :pswitch_16
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 411
    .line 412
    .line 413
    move-result-object p0

    .line 414
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 415
    .line 416
    .line 417
    move-result p2

    .line 418
    invoke-static {p0, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 419
    .line 420
    .line 421
    move-result p0

    .line 422
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 423
    .line 424
    .line 425
    move-result p2

    .line 426
    add-int/2addr p2, p0

    .line 427
    invoke-static {p2, v4}, Ljava/lang/Math;->max(II)I

    .line 428
    .line 429
    .line 430
    move-result p0

    .line 431
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 432
    .line 433
    .line 434
    goto/16 :goto_1

    .line 435
    .line 436
    :pswitch_17
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 437
    .line 438
    .line 439
    move-result-object p0

    .line 440
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 441
    .line 442
    .line 443
    move-result p2

    .line 444
    invoke-static {p0, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 445
    .line 446
    .line 447
    move-result p0

    .line 448
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 449
    .line 450
    .line 451
    goto/16 :goto_1

    .line 452
    .line 453
    :pswitch_18
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 454
    .line 455
    .line 456
    move-result p0

    .line 457
    int-to-byte p0, p0

    .line 458
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIss(B)V

    .line 459
    .line 460
    .line 461
    goto/16 :goto_1

    .line 462
    .line 463
    :pswitch_19
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 464
    .line 465
    .line 466
    move-result p2

    .line 467
    if-eqz p2, :cond_2

    .line 468
    .line 469
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    .line 470
    .line 471
    .line 472
    move-result p0

    .line 473
    if-nez p0, :cond_e

    .line 474
    .line 475
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 476
    .line 477
    .line 478
    move-result p0

    .line 479
    add-int/lit8 p0, p0, -0x2

    .line 480
    .line 481
    invoke-static {p0, v3}, Ljava/lang/Math;->max(II)I

    .line 482
    .line 483
    .line 484
    move-result p0

    .line 485
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 486
    .line 487
    .line 488
    goto/16 :goto_1

    .line 489
    .line 490
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    .line 491
    .line 492
    .line 493
    move-result p0

    .line 494
    if-eqz p0, :cond_e

    .line 495
    .line 496
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 497
    .line 498
    .line 499
    move-result p0

    .line 500
    add-int/2addr p0, v3

    .line 501
    invoke-static {p0, v3}, Ljava/lang/Math;->max(II)I

    .line 502
    .line 503
    .line 504
    move-result p0

    .line 505
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 506
    .line 507
    .line 508
    goto/16 :goto_1

    .line 509
    .line 510
    :pswitch_1a
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 511
    .line 512
    .line 513
    move-result p0

    .line 514
    int-to-short p0, p0

    .line 515
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHpsPos(S)V

    .line 516
    .line 517
    .line 518
    goto/16 :goto_1

    .line 519
    .line 520
    :pswitch_1b
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 521
    .line 522
    .line 523
    move-result p0

    .line 524
    int-to-byte p0, p0

    .line 525
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 526
    .line 527
    .line 528
    move-result p2

    .line 529
    mul-int/lit8 p0, p0, 0x2

    .line 530
    .line 531
    add-int/2addr p2, p0

    .line 532
    invoke-static {p2, v3}, Ljava/lang/Math;->max(II)I

    .line 533
    .line 534
    .line 535
    move-result p0

    .line 536
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 537
    .line 538
    .line 539
    goto/16 :goto_1

    .line 540
    .line 541
    :pswitch_1c
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 542
    .line 543
    .line 544
    move-result p0

    .line 545
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 546
    .line 547
    .line 548
    goto/16 :goto_1

    .line 549
    .line 550
    :pswitch_1d
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 551
    .line 552
    .line 553
    move-result p0

    .line 554
    int-to-byte p0, p0

    .line 555
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIco(B)V

    .line 556
    .line 557
    .line 558
    goto/16 :goto_1

    .line 559
    .line 560
    :pswitch_1e
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 561
    .line 562
    .line 563
    move-result p0

    .line 564
    int-to-short p0, p0

    .line 565
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setLidDefault(I)V

    .line 566
    .line 567
    .line 568
    goto/16 :goto_1

    .line 569
    .line 570
    :pswitch_1f
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 571
    .line 572
    .line 573
    move-result p0

    .line 574
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDxaSpace(I)V

    .line 575
    .line 576
    .line 577
    goto/16 :goto_1

    .line 578
    .line 579
    :pswitch_20
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 580
    .line 581
    .line 582
    move-result p2

    .line 583
    and-int/lit16 v0, p2, 0xff

    .line 584
    .line 585
    if-eqz v0, :cond_3

    .line 586
    .line 587
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 588
    .line 589
    .line 590
    :cond_3
    const v0, 0xff00

    .line 591
    .line 592
    .line 593
    and-int/2addr v0, p2

    .line 594
    ushr-int/2addr v0, v4

    .line 595
    int-to-byte v0, v0

    .line 596
    ushr-int/2addr v0, v1

    .line 597
    int-to-byte v0, v0

    .line 598
    if-eqz v0, :cond_4

    .line 599
    .line 600
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 601
    .line 602
    .line 603
    move-result v4

    .line 604
    mul-int/lit8 v0, v0, 0x2

    .line 605
    .line 606
    add-int/2addr v4, v0

    .line 607
    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    .line 608
    .line 609
    .line 610
    move-result v0

    .line 611
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 612
    .line 613
    .line 614
    :cond_4
    const/high16 v0, 0xff0000

    .line 615
    .line 616
    and-int/2addr v0, p2

    .line 617
    ushr-int/lit8 v0, v0, 0x10

    .line 618
    .line 619
    int-to-byte v0, v0

    .line 620
    const/16 v4, 0x80

    .line 621
    .line 622
    if-eq v0, v4, :cond_5

    .line 623
    .line 624
    int-to-short v5, v0

    .line 625
    invoke-virtual {p1, v5}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHpsPos(S)V

    .line 626
    .line 627
    .line 628
    :cond_5
    and-int/lit16 p2, p2, 0x100

    .line 629
    .line 630
    if-lez p2, :cond_6

    .line 631
    .line 632
    const/4 v2, 0x1

    .line 633
    :cond_6
    if-eqz v2, :cond_7

    .line 634
    .line 635
    if-eq v0, v4, :cond_7

    .line 636
    .line 637
    if-eqz v0, :cond_7

    .line 638
    .line 639
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    .line 640
    .line 641
    .line 642
    move-result p2

    .line 643
    if-nez p2, :cond_7

    .line 644
    .line 645
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 646
    .line 647
    .line 648
    move-result p2

    .line 649
    add-int/lit8 p2, p2, -0x2

    .line 650
    .line 651
    invoke-static {p2, v3}, Ljava/lang/Math;->max(II)I

    .line 652
    .line 653
    .line 654
    move-result p2

    .line 655
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 656
    .line 657
    .line 658
    :cond_7
    if-eqz v2, :cond_e

    .line 659
    .line 660
    if-nez v0, :cond_e

    .line 661
    .line 662
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    .line 663
    .line 664
    .line 665
    move-result p0

    .line 666
    if-eqz p0, :cond_e

    .line 667
    .line 668
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 669
    .line 670
    .line 671
    move-result p0

    .line 672
    add-int/2addr p0, v3

    .line 673
    invoke-static {p0, v3}, Ljava/lang/Math;->max(II)I

    .line 674
    .line 675
    .line 676
    move-result p0

    .line 677
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 678
    .line 679
    .line 680
    goto/16 :goto_1

    .line 681
    .line 682
    :pswitch_21
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 683
    .line 684
    .line 685
    move-result p0

    .line 686
    int-to-byte p0, p0

    .line 687
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setKul(B)V

    .line 688
    .line 689
    .line 690
    goto/16 :goto_1

    .line 691
    .line 692
    :pswitch_22
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 693
    .line 694
    .line 695
    move-result p0

    .line 696
    int-to-short p0, p0

    .line 697
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFtcAscii(I)V

    .line 698
    .line 699
    .line 700
    goto/16 :goto_1

    .line 701
    .line 702
    :pswitch_23
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 703
    .line 704
    .line 705
    move-result p2

    .line 706
    int-to-byte p2, p2

    .line 707
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFVanish()Z

    .line 708
    .line 709
    .line 710
    move-result p0

    .line 711
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    .line 712
    .line 713
    .line 714
    move-result p0

    .line 715
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFVanish(Z)V

    .line 716
    .line 717
    .line 718
    goto/16 :goto_1

    .line 719
    .line 720
    :pswitch_24
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 721
    .line 722
    .line 723
    move-result p2

    .line 724
    int-to-byte p2, p2

    .line 725
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFCaps()Z

    .line 726
    .line 727
    .line 728
    move-result p0

    .line 729
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    .line 730
    .line 731
    .line 732
    move-result p0

    .line 733
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFCaps(Z)V

    .line 734
    .line 735
    .line 736
    goto/16 :goto_1

    .line 737
    .line 738
    :pswitch_25
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 739
    .line 740
    .line 741
    move-result p2

    .line 742
    int-to-byte p2, p2

    .line 743
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFSmallCaps()Z

    .line 744
    .line 745
    .line 746
    move-result p0

    .line 747
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    .line 748
    .line 749
    .line 750
    move-result p0

    .line 751
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSmallCaps(Z)V

    .line 752
    .line 753
    .line 754
    goto/16 :goto_1

    .line 755
    .line 756
    :pswitch_26
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 757
    .line 758
    .line 759
    move-result p2

    .line 760
    int-to-byte p2, p2

    .line 761
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFShadow()Z

    .line 762
    .line 763
    .line 764
    move-result p0

    .line 765
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    .line 766
    .line 767
    .line 768
    move-result p0

    .line 769
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFShadow(Z)V

    .line 770
    .line 771
    .line 772
    goto/16 :goto_1

    .line 773
    .line 774
    :pswitch_27
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 775
    .line 776
    .line 777
    move-result p2

    .line 778
    int-to-byte p2, p2

    .line 779
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFOutline()Z

    .line 780
    .line 781
    .line 782
    move-result p0

    .line 783
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    .line 784
    .line 785
    .line 786
    move-result p0

    .line 787
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFOutline(Z)V

    .line 788
    .line 789
    .line 790
    goto/16 :goto_1

    .line 791
    .line 792
    :pswitch_28
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 793
    .line 794
    .line 795
    move-result p2

    .line 796
    int-to-byte p2, p2

    .line 797
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFStrike()Z

    .line 798
    .line 799
    .line 800
    move-result p0

    .line 801
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    .line 802
    .line 803
    .line 804
    move-result p0

    .line 805
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFStrike(Z)V

    .line 806
    .line 807
    .line 808
    goto/16 :goto_1

    .line 809
    .line 810
    :pswitch_29
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 811
    .line 812
    .line 813
    move-result p2

    .line 814
    int-to-byte p2, p2

    .line 815
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFItalic()Z

    .line 816
    .line 817
    .line 818
    move-result p0

    .line 819
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    .line 820
    .line 821
    .line 822
    move-result p0

    .line 823
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFItalic(Z)V

    .line 824
    .line 825
    .line 826
    goto/16 :goto_1

    .line 827
    .line 828
    :pswitch_2a
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 829
    .line 830
    .line 831
    move-result p2

    .line 832
    int-to-byte p2, p2

    .line 833
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFBold()Z

    .line 834
    .line 835
    .line 836
    move-result p0

    .line 837
    invoke-static {p2, p0}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->getCHPFlag(BZ)Z

    .line 838
    .line 839
    .line 840
    move-result p0

    .line 841
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFBold(Z)V

    .line 842
    .line 843
    .line 844
    goto/16 :goto_1

    .line 845
    .line 846
    :pswitch_2b
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 847
    .line 848
    .line 849
    move-result p0

    .line 850
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 851
    .line 852
    .line 853
    move-result p0

    .line 854
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFOle2(Z)V

    .line 855
    .line 856
    .line 857
    goto/16 :goto_1

    .line 858
    .line 859
    :pswitch_2c
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSpec(Z)V

    .line 860
    .line 861
    .line 862
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 863
    .line 864
    .line 865
    move-result-object p0

    .line 866
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 867
    .line 868
    .line 869
    move-result v0

    .line 870
    invoke-static {p0, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 871
    .line 872
    .line 873
    move-result p0

    .line 874
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFtcSym(I)V

    .line 875
    .line 876
    .line 877
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 878
    .line 879
    .line 880
    move-result-object p0

    .line 881
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 882
    .line 883
    .line 884
    move-result p2

    .line 885
    add-int/2addr p2, v3

    .line 886
    invoke-static {p0, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 887
    .line 888
    .line 889
    move-result p0

    .line 890
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setXchSym(I)V

    .line 891
    .line 892
    .line 893
    goto/16 :goto_1

    .line 894
    .line 895
    :pswitch_2d
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 896
    .line 897
    .line 898
    move-result p0

    .line 899
    and-int/lit16 p2, p0, 0xff

    .line 900
    .line 901
    int-to-short p2, p2

    .line 902
    invoke-static {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 903
    .line 904
    .line 905
    move-result p2

    .line 906
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFChsDiff(Z)V

    .line 907
    .line 908
    .line 909
    const p2, 0xffff00

    .line 910
    .line 911
    .line 912
    and-int/2addr p0, p2

    .line 913
    int-to-short p0, p0

    .line 914
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setChse(S)V

    .line 915
    .line 916
    .line 917
    goto/16 :goto_1

    .line 918
    .line 919
    :pswitch_2e
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 920
    .line 921
    .line 922
    move-result p0

    .line 923
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 924
    .line 925
    .line 926
    move-result p0

    .line 927
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFData(Z)V

    .line 928
    .line 929
    .line 930
    goto/16 :goto_1

    .line 931
    .line 932
    :pswitch_2f
    new-instance p0, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 933
    .line 934
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprl()[B

    .line 935
    .line 936
    .line 937
    move-result-object v0

    .line 938
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 939
    .line 940
    .line 941
    move-result p2

    .line 942
    invoke-direct {p0, v0, p2}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>([BI)V

    .line 943
    .line 944
    .line 945
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 946
    .line 947
    .line 948
    goto/16 :goto_1

    .line 949
    .line 950
    :pswitch_30
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 951
    .line 952
    .line 953
    move-result p0

    .line 954
    int-to-short p0, p0

    .line 955
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIbstRMark(I)V

    .line 956
    .line 957
    .line 958
    goto/16 :goto_1

    .line 959
    .line 960
    :pswitch_31
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 961
    .line 962
    .line 963
    move-result p0

    .line 964
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFcPic(I)V

    .line 965
    .line 966
    .line 967
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSpec(Z)V

    .line 968
    .line 969
    .line 970
    goto/16 :goto_1

    .line 971
    .line 972
    :pswitch_32
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 973
    .line 974
    .line 975
    move-result p0

    .line 976
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 977
    .line 978
    .line 979
    move-result p0

    .line 980
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFFldVanish(Z)V

    .line 981
    .line 982
    .line 983
    goto/16 :goto_1

    .line 984
    .line 985
    :pswitch_33
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 986
    .line 987
    .line 988
    move-result p0

    .line 989
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 990
    .line 991
    .line 992
    move-result p0

    .line 993
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFRMark(Z)V

    .line 994
    .line 995
    .line 996
    goto :goto_1

    .line 997
    :pswitch_34
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 998
    .line 999
    .line 1000
    move-result p0

    .line 1001
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 1002
    .line 1003
    .line 1004
    move-result p0

    .line 1005
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFRMarkDel(Z)V

    .line 1006
    .line 1007
    .line 1008
    goto :goto_1

    .line 1009
    :cond_8
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFSpec()Z

    .line 1010
    .line 1011
    .line 1012
    move-result p1

    .line 1013
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;->clone()Ljava/lang/Object;

    .line 1014
    .line 1015
    .line 1016
    move-result-object p0

    .line 1017
    check-cast p0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 1018
    .line 1019
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSpec(Z)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1020
    .line 1021
    .line 1022
    :catch_0
    return-void

    .line 1023
    :cond_9
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFBold(Z)V

    .line 1024
    .line 1025
    .line 1026
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFItalic(Z)V

    .line 1027
    .line 1028
    .line 1029
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFOutline(Z)V

    .line 1030
    .line 1031
    .line 1032
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFStrike(Z)V

    .line 1033
    .line 1034
    .line 1035
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFShadow(Z)V

    .line 1036
    .line 1037
    .line 1038
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSmallCaps(Z)V

    .line 1039
    .line 1040
    .line 1041
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFCaps(Z)V

    .line 1042
    .line 1043
    .line 1044
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFVanish(Z)V

    .line 1045
    .line 1046
    .line 1047
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setKul(B)V

    .line 1048
    .line 1049
    .line 1050
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIco(B)V

    .line 1051
    .line 1052
    .line 1053
    goto :goto_1

    .line 1054
    :cond_a
    new-instance p0, Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 1055
    .line 1056
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 1057
    .line 1058
    .line 1059
    move-result p2

    .line 1060
    invoke-direct {p0, p2}, Lcom/intsig/office/fc/hwpf/model/Colorref;-><init>(I)V

    .line 1061
    .line 1062
    .line 1063
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setUnderlineColor(Lcom/intsig/office/fc/hwpf/model/Colorref;)V

    .line 1064
    .line 1065
    .line 1066
    goto :goto_1

    .line 1067
    :cond_b
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 1068
    .line 1069
    .line 1070
    move-result p0

    .line 1071
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIstd(I)V

    .line 1072
    .line 1073
    .line 1074
    goto :goto_1

    .line 1075
    :cond_c
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 1076
    .line 1077
    .line 1078
    move-result p0

    .line 1079
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFcObj(I)V

    .line 1080
    .line 1081
    .line 1082
    goto :goto_1

    .line 1083
    :cond_d
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 1084
    .line 1085
    .line 1086
    move-result p0

    .line 1087
    int-to-byte p0, p0

    .line 1088
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIcoHighlight(B)V

    .line 1089
    .line 1090
    .line 1091
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperand()I

    .line 1092
    .line 1093
    .line 1094
    move-result p0

    .line 1095
    invoke-static {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUncompressor;->getFlag(I)Z

    .line 1096
    .line 1097
    .line 1098
    move-result p0

    .line 1099
    invoke-virtual {p1, p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFHighlight(Z)V

    .line 1100
    .line 1101
    .line 1102
    :cond_e
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x35
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x48
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x4d
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x53
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x62
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x6d
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static uncompressCHP(Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;[BI)Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    .line 8
    .line 9
    invoke-direct {v1, p1, p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 10
    .line 11
    .line 12
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-eqz p1, :cond_1

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    const/4 v2, 0x2

    .line 27
    if-eq p2, v2, :cond_0

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-static {p0, v0, p1}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmUncompressor;->unCompressCHPOperation(Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    return-object v0

    .line 35
    :catch_0
    new-instance p0, Ljava/lang/RuntimeException;

    .line 36
    .line 37
    const-string p1, "There is no way this exception should happen!!"

    .line 38
    .line 39
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.class public final Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;
.super Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;
.source "CharacterProperties.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SPRM_BRC:S = 0x6865s

.field public static final SPRM_CCV:S = 0x6870s

.field public static final SPRM_CHARSCALE:S = 0x4852s

.field public static final SPRM_CPG:S = 0x486bs

.field public static final SPRM_DISPFLDRMARK:S = -0x359es

.field public static final SPRM_DTTMRMARK:S = 0x6805s

.field public static final SPRM_DTTMRMARKDEL:S = 0x6864s

.field public static final SPRM_DXASPACE:S = -0x77c0s

.field public static final SPRM_FBOLD:S = 0x835s

.field public static final SPRM_FCAPS:S = 0x83bs

.field public static final SPRM_FDATA:S = 0x806s

.field public static final SPRM_FDSTRIKE:S = 0x2a53s

.field public static final SPRM_FELID:S = 0x486es

.field public static final SPRM_FEMBOSS:S = 0x858s

.field public static final SPRM_FFLDVANISH:S = 0x802s

.field public static final SPRM_FIMPRINT:S = 0x854s

.field public static final SPRM_FITALIC:S = 0x836s

.field public static final SPRM_FOBJ:S = 0x856s

.field public static final SPRM_FOLE2:S = 0x80as

.field public static final SPRM_FOUTLINE:S = 0x838s

.field public static final SPRM_FRMARK:S = 0x801s

.field public static final SPRM_FRMARKDEL:S = 0x800s

.field public static final SPRM_FSHADOW:S = 0x839s

.field public static final SPRM_FSMALLCAPS:S = 0x83as

.field public static final SPRM_FSPEC:S = 0x855s

.field public static final SPRM_FSTRIKE:S = 0x837s

.field public static final SPRM_FVANISH:S = 0x83cs

.field public static final SPRM_HIGHLIGHT:S = 0x2a0cs

.field public static final SPRM_HPS:S = 0x4a43s

.field public static final SPRM_HPSKERN:S = 0x484bs

.field public static final SPRM_HPSPOS:S = 0x4845s

.field public static final SPRM_IBSTRMARK:S = 0x4804s

.field public static final SPRM_IBSTRMARKDEL:S = 0x4863s

.field public static final SPRM_ICO:S = 0x2a42s

.field public static final SPRM_IDCTHINT:S = 0x286fs

.field public static final SPRM_IDSIRMARKDEL:S = 0x4867s

.field public static final SPRM_ISS:S = 0x2a48s

.field public static final SPRM_ISTD:S = 0x4a30s

.field public static final SPRM_KUL:S = 0x2a3es

.field public static final SPRM_LID:S = 0x4a41s

.field public static final SPRM_NONFELID:S = 0x486ds

.field public static final SPRM_OBJLOCATION:S = 0x680es

.field public static final SPRM_PICLOCATION:S = 0x6a03s

.field public static final SPRM_PROPRMARK:S = -0x35a9s

.field public static final SPRM_RGFTCASCII:S = 0x4a4fs

.field public static final SPRM_RGFTCFAREAST:S = 0x4a50s

.field public static final SPRM_RGFTCNOTFAREAST:S = 0x4a51s

.field public static final SPRM_SFXTEXT:S = 0x2859s

.field public static final SPRM_SHD:S = 0x4866s

.field public static final SPRM_SYMBOL:S = 0x6a09s

.field public static final SPRM_YSRI:S = 0x484es


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFUsePgsuSettings(Z)V

    .line 6
    .line 7
    .line 8
    const/16 v0, 0x24

    .line 9
    .line 10
    new-array v0, v0, [B

    .line 11
    .line 12
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setXstDispFldRMark([B)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getCv()Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/Colorref;->clone()Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setCv(Lcom/intsig/office/fc/hwpf/model/Colorref;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmRMarkDel()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmRMarkDel(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmPropRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 53
    .line 54
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmPropRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmDispFldRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->clone()Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 66
    .line 67
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDttmDispFldRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getXstDispFldRMark()[B

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    check-cast v1, [B

    .line 79
    .line 80
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setXstDispFldRMark([B)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;->clone()Ljava/lang/Object;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 92
    .line 93
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setShd(Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;)V

    .line 94
    .line 95
    .line 96
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getBrc()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->clone()Ljava/lang/Object;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 105
    .line 106
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setBrc(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 107
    .line 108
    .line 109
    return-object v0
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getCharacterSpacing()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDxaSpace()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColor()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIco()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFontSize()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIco24()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getCv()Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/Colorref;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getCv()Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/Colorref;->getValue()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    return v0

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIco()B

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    packed-switch v0, :pswitch_data_0

    .line 25
    .line 26
    .line 27
    const/4 v0, -0x1

    .line 28
    return v0

    .line 29
    :pswitch_0
    const v0, 0xc0c0c0

    .line 30
    .line 31
    .line 32
    return v0

    .line 33
    :pswitch_1
    const v0, 0x808080

    .line 34
    .line 35
    .line 36
    return v0

    .line 37
    :pswitch_2
    const v0, 0x8080

    .line 38
    .line 39
    .line 40
    return v0

    .line 41
    :pswitch_3
    const/16 v0, 0x80

    .line 42
    .line 43
    return v0

    .line 44
    :pswitch_4
    const v0, 0x800080

    .line 45
    .line 46
    .line 47
    return v0

    .line 48
    :pswitch_5
    const v0, 0x8000

    .line 49
    .line 50
    .line 51
    return v0

    .line 52
    :pswitch_6
    const v0, 0x808000

    .line 53
    .line 54
    .line 55
    return v0

    .line 56
    :pswitch_7
    const/high16 v0, 0x800000

    .line 57
    .line 58
    return v0

    .line 59
    :pswitch_8
    const v0, 0xffffff

    .line 60
    .line 61
    .line 62
    return v0

    .line 63
    :pswitch_9
    const v0, 0xffff

    .line 64
    .line 65
    .line 66
    return v0

    .line 67
    :pswitch_a
    const/16 v0, 0xff

    .line 68
    .line 69
    return v0

    .line 70
    :pswitch_b
    const v0, 0xff00ff

    .line 71
    .line 72
    .line 73
    return v0

    .line 74
    :pswitch_c
    const v0, 0xff00

    .line 75
    .line 76
    .line 77
    return v0

    .line 78
    :pswitch_d
    const v0, 0xffff00

    .line 79
    .line 80
    .line 81
    return v0

    .line 82
    :pswitch_e
    const/high16 v0, 0xff0000

    .line 83
    .line 84
    return v0

    .line 85
    :pswitch_f
    const/4 v0, 0x0

    .line 86
    return v0

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getKerning()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsKern()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSubSuperScriptIndex()S
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIss()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-short v0, v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getUnderlineCode()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getKul()B

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getVerticalOffset()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isBold()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFBold()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isCapitalized()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFCaps()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDoubleStrikeThrough()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFDStrike()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isEmbossed()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFEmboss()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFldVanished()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFFldVanish()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isHighlighted()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFHighlight()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isImprinted()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFImprint()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isItalic()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFItalic()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isMarkedDeleted()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFRMarkDel()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isMarkedInserted()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFRMark()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isOutlined()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFOutline()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isShadowed()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFShadow()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSmallCaps()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFSmallCaps()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isStrikeThrough()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFStrike()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isVanished()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFVanish()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public markDeleted(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFRMarkDel(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public markInserted(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFRMark(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBold(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFBold(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCapitalized(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFCaps(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCharacterSpacing(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDxaSpace(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setColor(I)V
    .locals 0

    .line 1
    int-to-byte p1, p1

    .line 2
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIco(B)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDoubleStrikeThrough(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFDStrike(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setEmbossed(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFEmboss(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFldVanish(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFFldVanish(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFontSize(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHps(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHighlighted(B)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setIcoHighlight(B)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIco24(I)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 2
    .line 3
    const v1, 0xffffff

    .line 4
    .line 5
    .line 6
    and-int/2addr p1, v1

    .line 7
    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hwpf/model/Colorref;-><init>(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setCv(Lcom/intsig/office/fc/hwpf/model/Colorref;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setImprinted(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFImprint(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setItalic(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFItalic(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setKerning(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHpsKern(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setOutline(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFOutline(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShadow(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFShadow(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSmallCaps(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFSmallCaps(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSubSuperScriptIndex(S)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setDxaSpace(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnderlineCode(I)V
    .locals 0

    .line 1
    int-to-byte p1, p1

    .line 2
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setKul(B)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setVanished(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFVanish(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setVerticalOffset(I)V
    .locals 0

    .line 1
    int-to-short p1, p1

    .line 2
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setHpsPos(S)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public strikeThrough(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->setFStrike(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

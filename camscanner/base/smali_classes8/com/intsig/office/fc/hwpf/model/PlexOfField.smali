.class public Lcom/intsig/office/fc/hwpf/model/PlexOfField;
.super Ljava/lang/Object;
.source "PlexOfField.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# instance fields
.field private final fld:Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;

.field private final propertyNode:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;


# direct methods
.method public constructor <init>(II[B)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->propertyNode:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 3
    new-instance p1, Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;

    invoke-direct {p1, p3}, Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;-><init>([B)V

    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->fld:Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->propertyNode:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 6
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;-><init>([B)V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->fld:Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;

    return-void
.end method


# virtual methods
.method public getFcEnd()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->propertyNode:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFcStart()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->propertyNode:Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFld()Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->fld:Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->getFcStart()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const/4 v2, 0x0

    .line 13
    aput-object v1, v0, v2

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->getFcEnd()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const/4 v2, 0x1

    .line 24
    aput-object v1, v0, v2

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->fld:Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;->getBoundaryType()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    and-int/lit16 v1, v1, 0xff

    .line 33
    .line 34
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const/4 v2, 0x2

    .line 39
    aput-object v1, v0, v2

    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/PlexOfField;->fld:Lcom/intsig/office/fc/hwpf/model/FieldDescriptor;

    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/types/FLDAbstractType;->getFlt()B

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    and-int/lit16 v1, v1, 0xff

    .line 48
    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    const/4 v2, 0x3

    .line 54
    aput-object v1, v0, v2

    .line 55
    .line 56
    const-string v1, "[{0}, {1}) - FLD - 0x{2}; 0x{3}"

    .line 57
    .line 58
    invoke-static {v1, v0}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    return-object v0
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

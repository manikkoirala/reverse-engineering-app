.class public final Lcom/intsig/office/fc/hwpf/model/FIBShortHandler;
.super Ljava/lang/Object;
.source "FIBShortHandler.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field public static final LIDFE:I = 0xd

.field public static final MAGICCREATED:I = 0x0

.field public static final MAGICCREATEDPRIVATE:I = 0x2

.field public static final MAGICREVISED:I = 0x1

.field public static final MAGICREVISEDPRIVATE:I = 0x3

.field static final START:I = 0x20


# instance fields
.field _shorts:[S


# direct methods
.method public constructor <init>([B)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x20

    .line 5
    .line 6
    invoke-static {p1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    new-array v1, v0, [S

    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/FIBShortHandler;->_shorts:[S

    .line 13
    .line 14
    const/16 v1, 0x22

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    :goto_0
    if-ge v2, v0, :cond_0

    .line 18
    .line 19
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/FIBShortHandler;->_shorts:[S

    .line 20
    .line 21
    invoke-static {p1, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    aput-short v4, v3, v2

    .line 26
    .line 27
    add-int/lit8 v1, v1, 0x2

    .line 28
    .line 29
    add-int/lit8 v2, v2, 0x1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public getShort(I)S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBShortHandler;->_shorts:[S

    .line 2
    .line 3
    aget-short p1, v0, p1

    .line 4
    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method serialize([B)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBShortHandler;->_shorts:[S

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    int-to-short v0, v0

    .line 5
    const/16 v1, 0x20

    .line 6
    .line 7
    invoke-static {p1, v1, v0}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 8
    .line 9
    .line 10
    const/16 v0, 0x22

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/FIBShortHandler;->_shorts:[S

    .line 14
    .line 15
    array-length v3, v2

    .line 16
    if-ge v1, v3, :cond_0

    .line 17
    .line 18
    aget-short v2, v2, v1

    .line 19
    .line 20
    invoke-static {p1, v0, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 21
    .line 22
    .line 23
    add-int/lit8 v0, v0, 0x2

    .line 24
    .line 25
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method sizeInBytes()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FIBShortHandler;->_shorts:[S

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    mul-int/lit8 v0, v0, 0x2

    .line 5
    .line 6
    add-int/lit8 v0, v0, 0x2

    .line 7
    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

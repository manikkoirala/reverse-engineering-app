.class public final Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;
.super Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;
.source "CHPFormattedDiskPage.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field private static final FC_SIZE:I = 0x4


# instance fields
.field private _chpxList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field

.field private _overFlow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>([BIILcom/intsig/office/fc/hwpf/model/TextPieceTable;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2, p4}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;-><init>([BILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    return-void
.end method

.method public constructor <init>([BILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V
    .locals 5

    .line 4
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;-><init>([BI)V

    .line 5
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    const/4 p1, 0x0

    const/4 p2, 0x0

    .line 6
    :goto_0
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_crun:I

    if-ge p2, v0, :cond_1

    .line 7
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->getStart(I)I

    move-result v0

    .line 8
    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->getEnd(I)I

    move-result v1

    .line 9
    invoke-interface {p3, v0}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getCharIndex(I)I

    move-result v0

    .line 10
    invoke-interface {p3, v1, v0}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getCharIndex(II)I

    move-result v1

    if-le v0, v1, :cond_0

    goto :goto_1

    .line 11
    :cond_0
    new-instance v2, Lcom/intsig/office/fc/hwpf/model/CHPX;

    new-instance v3, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    invoke-virtual {p0, p2}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->getGrpprl(I)[B

    move-result-object v4

    invoke-direct {v3, v4, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BI)V

    invoke-direct {v2, v0, v1, v3}, Lcom/intsig/office/fc/hwpf/model/CHPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 12
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 13
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_crun:I

    return-void
.end method


# virtual methods
.method public fill(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hwpf/model/CHPX;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getCHPX(I)Lcom/intsig/office/fc/hwpf/model/CHPX;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected getGrpprl(I)[B
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_fkp:[B

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_offset:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_crun:I

    .line 6
    .line 7
    add-int/lit8 v2, v2, 0x1

    .line 8
    .line 9
    mul-int/lit8 v2, v2, 0x4

    .line 10
    .line 11
    add-int/2addr v2, p1

    .line 12
    add-int/2addr v1, v2

    .line 13
    invoke-static {v0, v1}, Lcom/intsig/office/fc/util/LittleEndian;->getUnsignedByte([BI)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    mul-int/lit8 p1, p1, 0x2

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    if-nez p1, :cond_0

    .line 21
    .line 22
    new-array p1, v0, [B

    .line 23
    .line 24
    return-object p1

    .line 25
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_fkp:[B

    .line 26
    .line 27
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_offset:I

    .line 28
    .line 29
    add-int/2addr v2, p1

    .line 30
    invoke-static {v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->getUnsignedByte([BI)I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    new-array v2, v1, [B

    .line 35
    .line 36
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_fkp:[B

    .line 37
    .line 38
    iget v4, p0, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->_offset:I

    .line 39
    .line 40
    add-int/lit8 p1, p1, 0x1

    .line 41
    .line 42
    add-int/2addr v4, p1

    .line 43
    invoke-static {v3, v4, v2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 44
    .line 45
    .line 46
    return-object v2
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getOverflow()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method protected toByteArray(Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)[B
    .locals 11

    const/16 v0, 0x200

    new-array v0, v0, [B

    .line 2
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x6

    const/4 v4, 0x0

    :goto_0
    const/16 v5, 0x1ff

    if-ge v4, v1, :cond_2

    .line 3
    iget-object v6, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/intsig/office/fc/hwpf/model/CHPX;

    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v6

    array-length v6, v6

    add-int/lit8 v7, v6, 0x6

    add-int/2addr v3, v7

    .line 4
    rem-int/lit8 v7, v4, 0x2

    add-int/2addr v7, v5

    if-le v3, v7, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 5
    rem-int/lit8 v6, v6, 0x2

    if-lez v6, :cond_1

    add-int/lit8 v3, v3, 0x1

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-eq v4, v1, :cond_3

    .line 6
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_overFlow:Ljava/util/ArrayList;

    .line 7
    iget-object v6, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v6, v4, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_3
    int-to-byte v1, v4

    aput-byte v1, v0, v5

    mul-int/lit8 v1, v4, 0x4

    add-int/lit8 v1, v1, 0x4

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_2
    if-ge v6, v4, :cond_4

    .line 8
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->_chpxList:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 9
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/CHPX;->getGrpprl()[B

    move-result-object v8

    .line 10
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v9

    invoke-interface {p1, v9}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v9

    invoke-static {v0, v7, v9}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 11
    array-length v9, v8

    add-int/lit8 v9, v9, 0x1

    sub-int/2addr v5, v9

    .line 12
    rem-int/lit8 v9, v5, 0x2

    sub-int/2addr v5, v9

    .line 13
    div-int/lit8 v9, v5, 0x2

    int-to-byte v9, v9

    aput-byte v9, v0, v1

    .line 14
    array-length v9, v8

    int-to-byte v9, v9

    aput-byte v9, v0, v5

    add-int/lit8 v9, v5, 0x1

    .line 15
    array-length v10, v8

    invoke-static {v8, v2, v0, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v7, v7, 0x4

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 16
    :cond_4
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v1

    invoke-interface {p1, v1}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result p1

    invoke-static {v0, v7, p1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    return-object v0
.end method

.method protected toByteArray(Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;I)[B
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->toByteArray(Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)[B

    move-result-object p1

    return-object p1
.end method

.class public final Lcom/intsig/office/fc/hwpf/usermodel/ListEntry;
.super Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;
.source "ListEntry.java"


# static fields
.field private static log:Lcom/intsig/office/fc/util/POILogger;


# instance fields
.field _level:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

.field _overrideLevel:Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/hwpf/usermodel/ListEntry;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lcom/intsig/office/fc/util/POILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/usermodel/ListEntry;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method constructor <init>(Lcom/intsig/office/fc/hwpf/model/PAPX;Lcom/intsig/office/fc/hwpf/usermodel/Range;Lcom/intsig/office/fc/hwpf/model/ListTables;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;-><init>(Lcom/intsig/office/fc/hwpf/model/PAPX;Lcom/intsig/office/fc/hwpf/usermodel/Range;)V

    .line 2
    .line 3
    .line 4
    if-eqz p3, :cond_0

    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlfo()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/ListTables;->getOverrideCount()I

    .line 13
    .line 14
    .line 15
    move-result p2

    .line 16
    if-ge p1, p2, :cond_0

    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlfo()I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    invoke-virtual {p3, p1}, Lcom/intsig/office/fc/hwpf/model/ListTables;->getOverride(I)Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 29
    .line 30
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlvl()B

    .line 31
    .line 32
    .line 33
    move-result p2

    .line 34
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;->getOverrideLevel(I)Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/ListEntry;->_overrideLevel:Lcom/intsig/office/fc/hwpf/model/ListFormatOverrideLevel;

    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/LFOAbstractType;->getLsid()I

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 45
    .line 46
    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlvl()B

    .line 47
    .line 48
    .line 49
    move-result p2

    .line 50
    invoke-virtual {p3, p1, p2}, Lcom/intsig/office/fc/hwpf/model/ListTables;->getLevel(II)Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/ListEntry;->_level:Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_0
    sget-object p1, Lcom/intsig/office/fc/hwpf/usermodel/ListEntry;->log:Lcom/intsig/office/fc/util/POILogger;

    .line 58
    .line 59
    sget p2, Lcom/intsig/office/fc/util/POILogger;->WARN:I

    .line 60
    .line 61
    const-string p3, "No ListTables found for ListEntry - document probably partly corrupt, and you may experience problems"

    .line 62
    .line 63
    invoke-virtual {p1, p2, p3}, Lcom/intsig/office/fc/util/POILogger;->log(ILjava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    :goto_0
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public type()I
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public final Lcom/intsig/office/fc/hwpf/HWPFDocument;
.super Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;
.source "HWPFDocument.java"


# static fields
.field private static final PROPERTY_PRESERVE_BIN_TABLES:Ljava/lang/String; = "com.wxiwei.fc.hwpf.preserveBinTables"

.field private static final PROPERTY_PRESERVE_TEXT_TABLE:Ljava/lang/String; = "com.wxiwei.fc.hwpf.preserveTextTable"

.field private static final STREAM_DATA:Ljava/lang/String; = "Data"

.field private static final STREAM_TABLE_0:Ljava/lang/String; = "0Table"

.field private static final STREAM_TABLE_1:Ljava/lang/String; = "1Table"


# instance fields
.field protected _bookmarks:Lcom/intsig/office/fc/hwpf/usermodel/Bookmarks;

.field protected _bookmarksTables:Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

.field protected _cft:Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;

.field protected _dataStream:[B

.field protected _escherRecordHolder:Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

.field protected _fields:Lcom/intsig/office/fc/hwpf/usermodel/Fields;

.field protected _fieldsTables:Lcom/intsig/office/fc/hwpf/model/FieldsTables;

.field private _fspaHeaders:Lcom/intsig/office/fc/hwpf/model/FSPATable;

.field private _fspaMain:Lcom/intsig/office/fc/hwpf/model/FSPATable;

.field protected _officeArts:Lcom/intsig/office/fc/hwpf/model/ShapesTable;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _officeDrawingsHeaders:Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;

.field protected _officeDrawingsMain:Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;

.field protected _pictures:Lcom/intsig/office/fc/hwpf/model/PicturesTable;

.field protected _tableStream:[B

.field protected _text:Ljava/lang/StringBuilder;

.field protected txbxBkd:Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    invoke-direct/range {p0 .. p1}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;-><init>(Ljava/io/InputStream;)V

    .line 4
    .line 5
    .line 6
    new-instance v14, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;

    .line 7
    .line 8
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 9
    .line 10
    invoke-direct {v14, v0}, Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;-><init>(Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/FIBAbstractType;->getNFib()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const/16 v1, 0x6a

    .line 20
    .line 21
    if-ge v0, v1, :cond_1

    .line 22
    .line 23
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/FIBAbstractType;->getNFib()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_0

    .line 30
    .line 31
    const/4 v0, 0x0

    .line 32
    throw v0

    .line 33
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hwpf/OldWordFileFormatException;

    .line 34
    .line 35
    const-string v1, "The document is too old - Word 95 or older. Try HWPFOldDocument instead?"

    .line 36
    .line 37
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/hwpf/OldWordFileFormatException;-><init>(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    throw v0

    .line 41
    :cond_1
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/FIBAbstractType;->isFWhichTblStm()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_2

    .line 48
    .line 49
    const-string v0, "1Table"

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_2
    const-string v0, "0Table"

    .line 53
    .line 54
    :goto_0
    :try_start_0
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 55
    .line 56
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getPropertyRawData(Ljava/lang/String;)[B

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    iput-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 61
    .line 62
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 63
    .line 64
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 65
    .line 66
    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->fillVariableFields([B[B)V

    .line 67
    .line 68
    .line 69
    const/4 v0, 0x0

    .line 70
    :try_start_1
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->cfbFS:Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;

    .line 71
    .line 72
    const-string v2, "Data"

    .line 73
    .line 74
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/fs/filesystem/CFBFileSystem;->getPropertyRawData(Ljava/lang/String;)[B

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    iput-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_dataStream:[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 79
    .line 80
    goto :goto_1

    .line 81
    :catch_0
    new-array v1, v0, [B

    .line 82
    .line 83
    iput-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_dataStream:[B

    .line 84
    .line 85
    :goto_1
    new-instance v1, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;

    .line 86
    .line 87
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 88
    .line 89
    iget-object v3, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 90
    .line 91
    iget-object v4, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 92
    .line 93
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcClx()I

    .line 94
    .line 95
    .line 96
    move-result v4

    .line 97
    invoke-direct {v1, v2, v3, v4, v0}, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;-><init>([B[BII)V

    .line 98
    .line 99
    .line 100
    iput-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_cft:Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;

    .line 101
    .line 102
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->getTextPieceTable()Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    .line 103
    .line 104
    .line 105
    move-result-object v15

    .line 106
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;

    .line 107
    .line 108
    iget-object v8, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 109
    .line 110
    iget-object v9, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 111
    .line 112
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 113
    .line 114
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlcfbteChpx()I

    .line 115
    .line 116
    .line 117
    move-result v10

    .line 118
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 119
    .line 120
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbPlcfbteChpx()I

    .line 121
    .line 122
    .line 123
    move-result v11

    .line 124
    move-object v7, v0

    .line 125
    move-object v12, v15

    .line 126
    invoke-direct/range {v7 .. v12}, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;-><init>([B[BIILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    .line 127
    .line 128
    .line 129
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_cbt:Lcom/intsig/office/fc/hwpf/model/CHPBinTable;

    .line 130
    .line 131
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;

    .line 132
    .line 133
    iget-object v8, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 134
    .line 135
    iget-object v9, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 136
    .line 137
    iget-object v10, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_dataStream:[B

    .line 138
    .line 139
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 140
    .line 141
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlcfbtePapx()I

    .line 142
    .line 143
    .line 144
    move-result v11

    .line 145
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 146
    .line 147
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbPlcfbtePapx()I

    .line 148
    .line 149
    .line 150
    move-result v12

    .line 151
    move-object v7, v0

    .line 152
    move-object v13, v15

    .line 153
    invoke-direct/range {v7 .. v13}, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;-><init>([B[B[BIILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    .line 154
    .line 155
    .line 156
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_pbt:Lcom/intsig/office/fc/hwpf/model/PAPBinTable;

    .line 157
    .line 158
    invoke-virtual {v15}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->getText()Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    .line 163
    .line 164
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_cbt:Lcom/intsig/office/fc/hwpf/model/CHPBinTable;

    .line 165
    .line 166
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_cft:Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;

    .line 167
    .line 168
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->rebuild(Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;)V

    .line 169
    .line 170
    .line 171
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_pbt:Lcom/intsig/office/fc/hwpf/model/PAPBinTable;

    .line 172
    .line 173
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    .line 174
    .line 175
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_cft:Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;

    .line 176
    .line 177
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/fc/hwpf/model/PAPBinTable;->rebuild(Ljava/lang/StringBuilder;Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;)V

    .line 178
    .line 179
    .line 180
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/FSPATable;

    .line 181
    .line 182
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 183
    .line 184
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 185
    .line 186
    sget-object v3, Lcom/intsig/office/fc/hwpf/model/FSPADocumentPart;->HEADER:Lcom/intsig/office/fc/hwpf/model/FSPADocumentPart;

    .line 187
    .line 188
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/model/FSPATable;-><init>([BLcom/intsig/office/fc/hwpf/model/FileInformationBlock;Lcom/intsig/office/fc/hwpf/model/FSPADocumentPart;)V

    .line 189
    .line 190
    .line 191
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fspaHeaders:Lcom/intsig/office/fc/hwpf/model/FSPATable;

    .line 192
    .line 193
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/FSPATable;

    .line 194
    .line 195
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 196
    .line 197
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 198
    .line 199
    sget-object v3, Lcom/intsig/office/fc/hwpf/model/FSPADocumentPart;->MAIN:Lcom/intsig/office/fc/hwpf/model/FSPADocumentPart;

    .line 200
    .line 201
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/model/FSPATable;-><init>([BLcom/intsig/office/fc/hwpf/model/FileInformationBlock;Lcom/intsig/office/fc/hwpf/model/FSPADocumentPart;)V

    .line 202
    .line 203
    .line 204
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fspaMain:Lcom/intsig/office/fc/hwpf/model/FSPATable;

    .line 205
    .line 206
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 207
    .line 208
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcDggInfo()I

    .line 209
    .line 210
    .line 211
    move-result v0

    .line 212
    if-eqz v0, :cond_3

    .line 213
    .line 214
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

    .line 215
    .line 216
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 217
    .line 218
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 219
    .line 220
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcDggInfo()I

    .line 221
    .line 222
    .line 223
    move-result v2

    .line 224
    iget-object v3, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 225
    .line 226
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbDggInfo()I

    .line 227
    .line 228
    .line 229
    move-result v3

    .line 230
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;-><init>([BII)V

    .line 231
    .line 232
    .line 233
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_escherRecordHolder:Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

    .line 234
    .line 235
    goto :goto_2

    .line 236
    :cond_3
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

    .line 237
    .line 238
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;-><init>()V

    .line 239
    .line 240
    .line 241
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_escherRecordHolder:Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

    .line 242
    .line 243
    :goto_2
    new-instance v7, Lcom/intsig/office/fc/hwpf/model/PicturesTable;

    .line 244
    .line 245
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_dataStream:[B

    .line 246
    .line 247
    iget-object v3, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 248
    .line 249
    iget-object v4, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fspaMain:Lcom/intsig/office/fc/hwpf/model/FSPATable;

    .line 250
    .line 251
    iget-object v5, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_escherRecordHolder:Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

    .line 252
    .line 253
    move-object v0, v7

    .line 254
    move-object/from16 v1, p0

    .line 255
    .line 256
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/hwpf/model/PicturesTable;-><init>(Lcom/intsig/office/fc/hwpf/HWPFDocument;[B[BLcom/intsig/office/fc/hwpf/model/FSPATable;Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;)V

    .line 257
    .line 258
    .line 259
    iput-object v7, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_pictures:Lcom/intsig/office/fc/hwpf/model/PicturesTable;

    .line 260
    .line 261
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/ShapesTable;

    .line 262
    .line 263
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 264
    .line 265
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 266
    .line 267
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hwpf/model/ShapesTable;-><init>([BLcom/intsig/office/fc/hwpf/model/FileInformationBlock;)V

    .line 268
    .line 269
    .line 270
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_officeArts:Lcom/intsig/office/fc/hwpf/model/ShapesTable;

    .line 271
    .line 272
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;

    .line 273
    .line 274
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fspaHeaders:Lcom/intsig/office/fc/hwpf/model/FSPATable;

    .line 275
    .line 276
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_escherRecordHolder:Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

    .line 277
    .line 278
    iget-object v3, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 279
    .line 280
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;-><init>(Lcom/intsig/office/fc/hwpf/model/FSPATable;Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;[B)V

    .line 281
    .line 282
    .line 283
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_officeDrawingsHeaders:Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;

    .line 284
    .line 285
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;

    .line 286
    .line 287
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fspaMain:Lcom/intsig/office/fc/hwpf/model/FSPATable;

    .line 288
    .line 289
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_escherRecordHolder:Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

    .line 290
    .line 291
    iget-object v3, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 292
    .line 293
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;-><init>(Lcom/intsig/office/fc/hwpf/model/FSPATable;Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;[B)V

    .line 294
    .line 295
    .line 296
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_officeDrawingsMain:Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;

    .line 297
    .line 298
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/SectionTable;

    .line 299
    .line 300
    iget-object v8, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_mainStream:[B

    .line 301
    .line 302
    iget-object v9, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 303
    .line 304
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 305
    .line 306
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlcfsed()I

    .line 307
    .line 308
    .line 309
    move-result v10

    .line 310
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 311
    .line 312
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbPlcfsed()I

    .line 313
    .line 314
    .line 315
    move-result v11

    .line 316
    const/4 v12, 0x0

    .line 317
    move-object v7, v0

    .line 318
    move-object v13, v15

    .line 319
    invoke-direct/range {v7 .. v14}, Lcom/intsig/office/fc/hwpf/model/SectionTable;-><init>([B[BIIILcom/intsig/office/fc/hwpf/model/TextPieceTable;Lcom/intsig/office/fc/hwpf/model/CPSplitCalculator;)V

    .line 320
    .line 321
    .line 322
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_st:Lcom/intsig/office/fc/hwpf/model/SectionTable;

    .line 323
    .line 324
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    .line 325
    .line 326
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 327
    .line 328
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 329
    .line 330
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcStshf()I

    .line 331
    .line 332
    .line 333
    move-result v2

    .line 334
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hwpf/model/StyleSheet;-><init>([BI)V

    .line 335
    .line 336
    .line 337
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_ss:Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    .line 338
    .line 339
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/FontTable;

    .line 340
    .line 341
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 342
    .line 343
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 344
    .line 345
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcSttbfffn()I

    .line 346
    .line 347
    .line 348
    move-result v2

    .line 349
    iget-object v3, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 350
    .line 351
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbSttbfffn()I

    .line 352
    .line 353
    .line 354
    move-result v3

    .line 355
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/model/FontTable;-><init>([BII)V

    .line 356
    .line 357
    .line 358
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_ft:Lcom/intsig/office/fc/hwpf/model/FontTable;

    .line 359
    .line 360
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 361
    .line 362
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlcfLst()I

    .line 363
    .line 364
    .line 365
    move-result v0

    .line 366
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 367
    .line 368
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlfLfo()I

    .line 369
    .line 370
    .line 371
    if-eqz v0, :cond_4

    .line 372
    .line 373
    iget-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 374
    .line 375
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbPlcfLst()I

    .line 376
    .line 377
    .line 378
    move-result v0

    .line 379
    if-eqz v0, :cond_4

    .line 380
    .line 381
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 382
    .line 383
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 384
    .line 385
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 386
    .line 387
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlcfLst()I

    .line 388
    .line 389
    .line 390
    move-result v2

    .line 391
    iget-object v3, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 392
    .line 393
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlfLfo()I

    .line 394
    .line 395
    .line 396
    move-result v3

    .line 397
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/model/ListTables;-><init>([BII)V

    .line 398
    .line 399
    .line 400
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_lt:Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 401
    .line 402
    :cond_4
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

    .line 403
    .line 404
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 405
    .line 406
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 407
    .line 408
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hwpf/model/BookmarksTables;-><init>([BLcom/intsig/office/fc/hwpf/model/FileInformationBlock;)V

    .line 409
    .line 410
    .line 411
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_bookmarksTables:Lcom/intsig/office/fc/hwpf/model/BookmarksTables;

    .line 412
    .line 413
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;

    .line 414
    .line 415
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hwpf/usermodel/BookmarksImpl;-><init>(Lcom/intsig/office/fc/hwpf/model/BookmarksTables;)V

    .line 416
    .line 417
    .line 418
    iput-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_bookmarks:Lcom/intsig/office/fc/hwpf/usermodel/Bookmarks;

    .line 419
    .line 420
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/FieldsTables;

    .line 421
    .line 422
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 423
    .line 424
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 425
    .line 426
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hwpf/model/FieldsTables;-><init>([BLcom/intsig/office/fc/hwpf/model/FileInformationBlock;)V

    .line 427
    .line 428
    .line 429
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fieldsTables:Lcom/intsig/office/fc/hwpf/model/FieldsTables;

    .line 430
    .line 431
    new-instance v1, Lcom/intsig/office/fc/hwpf/usermodel/FieldsImpl;

    .line 432
    .line 433
    invoke-direct {v1, v0}, Lcom/intsig/office/fc/hwpf/usermodel/FieldsImpl;-><init>(Lcom/intsig/office/fc/hwpf/model/FieldsTables;)V

    .line 434
    .line 435
    .line 436
    iput-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fields:Lcom/intsig/office/fc/hwpf/usermodel/Fields;

    .line 437
    .line 438
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;

    .line 439
    .line 440
    iget-object v1, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 441
    .line 442
    iget-object v2, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 443
    .line 444
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getFcPlcfTxbxBkd()I

    .line 445
    .line 446
    .line 447
    move-result v2

    .line 448
    iget-object v3, v6, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_fib:Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    .line 449
    .line 450
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getLcbPlcfTxbxBkd()I

    .line 451
    .line 452
    .line 453
    move-result v3

    .line 454
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;-><init>([BII)V

    .line 455
    .line 456
    .line 457
    iput-object v0, v6, Lcom/intsig/office/fc/hwpf/HWPFDocument;->txbxBkd:Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;

    .line 458
    .line 459
    return-void

    .line 460
    :catch_1
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 461
    .line 462
    new-instance v2, Ljava/lang/StringBuilder;

    .line 463
    .line 464
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 465
    .line 466
    .line 467
    const-string v3, "Table Stream \'"

    .line 468
    .line 469
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 470
    .line 471
    .line 472
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    .line 474
    .line 475
    const-string v0, "\' wasn\'t found - Either the document is corrupt, or is Word95 (or earlier)"

    .line 476
    .line 477
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    .line 479
    .line 480
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 481
    .line 482
    .line 483
    move-result-object v0

    .line 484
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 485
    .line 486
    .line 487
    throw v1
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private getRange(Lcom/intsig/office/fc/hwpf/model/SubdocumentType;)Lcom/intsig/office/fc/hwpf/usermodel/Range;
    .locals 6

    .line 2
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->ORDERED:[Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v4, v0, v2

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getFileInformationBlock()Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/intsig/office/fc/hwpf/model/FileInformationBlock;->getSubdocumentTextStreamLength(Lcom/intsig/office/fc/hwpf/model/SubdocumentType;)I

    move-result v5

    if-ne p1, v4, :cond_0

    .line 4
    new-instance p1, Lcom/intsig/office/fc/hwpf/usermodel/Range;

    add-int/2addr v5, v3

    invoke-direct {p1, v3, v5, p0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;-><init>(IILcom/intsig/office/fc/hwpf/HWPFDocumentCore;)V

    return-object p1

    :cond_0
    add-int/2addr v3, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 5
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Subdocument type not supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public characterLength()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public delete(II)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 2
    .line 3
    add-int/2addr p2, p1

    .line 4
    invoke-direct {v0, p1, p2, p0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;-><init>(IILcom/intsig/office/fc/hwpf/HWPFDocumentCore;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->delete()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getBookmarks()Lcom/intsig/office/fc/hwpf/usermodel/Bookmarks;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_bookmarks:Lcom/intsig/office/fc/hwpf/usermodel/Bookmarks;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCommentsRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->ANNOTATION:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getRange(Lcom/intsig/office/fc/hwpf/model/SubdocumentType;)Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDataStream()[B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_dataStream:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEndnoteRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->ENDNOTE:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getRange(Lcom/intsig/office/fc/hwpf/model/SubdocumentType;)Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEscherRecordHolder()Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_escherRecordHolder:Lcom/intsig/office/fc/hwpf/model/EscherRecordHolder;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFields()Lcom/intsig/office/fc/hwpf/usermodel/Fields;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fields:Lcom/intsig/office/fc/hwpf/usermodel/Fields;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFieldsTables()Lcom/intsig/office/fc/hwpf/model/FieldsTables;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_fieldsTables:Lcom/intsig/office/fc/hwpf/model/FieldsTables;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFootnoteRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->FOOTNOTE:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getRange(Lcom/intsig/office/fc/hwpf/model/SubdocumentType;)Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeaderStoryRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->HEADER:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getRange(Lcom/intsig/office/fc/hwpf/model/SubdocumentType;)Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMainTextboxRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->TEXTBOX:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getRange(Lcom/intsig/office/fc/hwpf/model/SubdocumentType;)Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getOfficeDrawingsHeaders()Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawings;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_officeDrawingsHeaders:Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getOfficeDrawingsMain()Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawings;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_officeDrawingsMain:Lcom/intsig/office/fc/hwpf/usermodel/OfficeDrawingsImpl;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getOverallRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/Range;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-direct {v0, v2, v1, p0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;-><init>(IILcom/intsig/office/fc/hwpf/HWPFDocumentCore;)V

    .line 11
    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPicturesTable()Lcom/intsig/office/fc/hwpf/model/PicturesTable;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_pictures:Lcom/intsig/office/fc/hwpf/model/PicturesTable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRange()Lcom/intsig/office/fc/hwpf/usermodel/Range;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/SubdocumentType;->MAIN:Lcom/intsig/office/fc/hwpf/model/SubdocumentType;

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/HWPFDocument;->getRange(Lcom/intsig/office/fc/hwpf/model/SubdocumentType;)Lcom/intsig/office/fc/hwpf/usermodel/Range;

    move-result-object v0

    return-object v0
.end method

.method public getShapesTable()Lcom/intsig/office/fc/hwpf/model/ShapesTable;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_officeArts:Lcom/intsig/office/fc/hwpf/model/ShapesTable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTableStream()[B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_tableStream:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getText()Ljava/lang/StringBuilder;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_text:Ljava/lang/StringBuilder;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTextTable()Lcom/intsig/office/fc/hwpf/model/TextPieceTable;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->_cft:Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->getTextPieceTable()Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTextboxEnd(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->txbxBkd:Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;

    .line 2
    .line 3
    add-int/lit8 p1, p1, 0x1

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->getCharPosition(I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTextboxStart(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocument;->txbxBkd:Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/PlcfTxbxBkd;->getCharPosition(I)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public registerList(Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_lt:Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/model/ListTables;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_lt:Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->_lt:Lcom/intsig/office/fc/hwpf/model/ListTables;

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->getListData()Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->getOverride()Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/model/ListTables;->addList(Lcom/intsig/office/fc/hwpf/model/POIListData;Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;)I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    return p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public final Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;
.super Ljava/lang/Object;
.source "ParagraphHeight.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# instance fields
.field private clMac:Lcom/intsig/office/fc/util/BitField;

.field private dxaCol:I

.field private dymLineOrHeight:I

.field private fDiffLines:Lcom/intsig/office/fc/util/BitField;

.field private fSpare:Lcom/intsig/office/fc/util/BitField;

.field private fUnk:Lcom/intsig/office/fc/util/BitField;

.field private infoField:S

.field private reserved:S


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 11
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->fSpare:Lcom/intsig/office/fc/util/BitField;

    const/4 v0, 0x2

    .line 12
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->fUnk:Lcom/intsig/office/fc/util/BitField;

    const/4 v0, 0x4

    .line 13
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->fDiffLines:Lcom/intsig/office/fc/util/BitField;

    const v0, 0xff00

    .line 14
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->clMac:Lcom/intsig/office/fc/util/BitField;

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->fSpare:Lcom/intsig/office/fc/util/BitField;

    const/4 v0, 0x2

    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->fUnk:Lcom/intsig/office/fc/util/BitField;

    const/4 v1, 0x4

    .line 4
    invoke-static {v1}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->fDiffLines:Lcom/intsig/office/fc/util/BitField;

    const v2, 0xff00

    .line 5
    invoke-static {v2}, Lcom/intsig/office/fc/util/BitFieldFactory;->getInstance(I)Lcom/intsig/office/fc/util/BitField;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->clMac:Lcom/intsig/office/fc/util/BitField;

    .line 6
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v2

    iput-short v2, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->infoField:S

    add-int/2addr p2, v0

    .line 7
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getShort([BI)S

    move-result v2

    iput-short v2, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->reserved:S

    add-int/2addr p2, v0

    .line 8
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result v0

    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->dxaCol:I

    add-int/2addr p2, v1

    .line 9
    invoke-static {p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([BI)I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->dymLineOrHeight:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;

    .line 2
    .line 3
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->infoField:S

    .line 4
    .line 5
    iget-short v1, p1, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->infoField:S

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->reserved:S

    .line 10
    .line 11
    iget-short v1, p1, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->reserved:S

    .line 12
    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->dxaCol:I

    .line 16
    .line 17
    iget v1, p1, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->dxaCol:I

    .line 18
    .line 19
    if-ne v0, v1, :cond_0

    .line 20
    .line 21
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->dymLineOrHeight:I

    .line 22
    .line 23
    iget p1, p1, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->dymLineOrHeight:I

    .line 24
    .line 25
    if-ne v0, p1, :cond_0

    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 p1, 0x0

    .line 30
    :goto_0
    return p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected toByteArray()[B
    .locals 3

    .line 1
    const/16 v0, 0xc

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    iget-short v2, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->infoField:S

    .line 7
    .line 8
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 9
    .line 10
    .line 11
    const/4 v1, 0x2

    .line 12
    iget-short v2, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->reserved:S

    .line 13
    .line 14
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x4

    .line 18
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->dxaCol:I

    .line 19
    .line 20
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 21
    .line 22
    .line 23
    const/16 v1, 0x8

    .line 24
    .line 25
    iget v2, p0, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->dymLineOrHeight:I

    .line 26
    .line 27
    invoke-static {v0, v1, v2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 28
    .line 29
    .line 30
    return-object v0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/ParagraphHeight;->toByteArray()[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

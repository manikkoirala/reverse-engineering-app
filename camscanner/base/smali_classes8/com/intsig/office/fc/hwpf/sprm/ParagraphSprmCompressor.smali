.class public final Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmCompressor;
.super Ljava/lang/Object;
.source "ParagraphSprmCompressor.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static compressParagraphProperty(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;)[B
    .locals 7

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result v1

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eq v1, v2, :cond_0

    const/16 v1, 0x4600

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIstd()I

    move-result v2

    invoke-static {v1, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v1

    add-int/2addr v1, v4

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 4
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getJc()B

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getJc()B

    move-result v5

    if-eq v2, v5, :cond_1

    const/16 v2, 0x2403

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getJc()B

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 6
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFSideBySide()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFSideBySide()Z

    move-result v5

    if-eq v2, v5, :cond_2

    const/16 v2, 0x2404

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFSideBySide()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 8
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKeep()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKeep()Z

    move-result v5

    if-eq v2, v5, :cond_3

    const/16 v2, 0x2405

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKeep()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKeepFollow()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKeepFollow()Z

    move-result v5

    if-eq v2, v5, :cond_4

    const/16 v2, 0x2406

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKeepFollow()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 12
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFPageBreakBefore()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFPageBreakBefore()Z

    move-result v5

    if-eq v2, v5, :cond_5

    const/16 v2, 0x2407

    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFPageBreakBefore()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 14
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcl()B

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcl()B

    move-result v5

    if-eq v2, v5, :cond_6

    const/16 v2, 0x2408

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcl()B

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 16
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcp()B

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcp()B

    move-result v5

    if-eq v2, v5, :cond_7

    const/16 v2, 0x2409

    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcp()B

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 18
    :cond_7
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlvl()B

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlvl()B

    move-result v5

    if-eq v2, v5, :cond_8

    const/16 v2, 0x260a

    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlvl()B

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 20
    :cond_8
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlfo()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlfo()I

    move-result v5

    if-eq v2, v5, :cond_9

    const/16 v2, 0x460b

    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlfo()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 22
    :cond_9
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNoLnn()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNoLnn()Z

    move-result v5

    if-eq v2, v5, :cond_a

    const/16 v2, 0x240c

    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNoLnn()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 24
    :cond_a
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getItbdMac()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getItbdMac()I

    move-result v5

    if-ne v2, v5, :cond_b

    .line 25
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getRgdxaTab()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getRgdxaTab()[I

    move-result-object v5

    invoke-static {v2, v5}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getRgtbd()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getRgtbd()[B

    move-result-object v5

    invoke-static {v2, v5}, Ljava/util/Arrays;->equals([B[B)Z

    .line 27
    :cond_b
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft()I

    move-result v5

    if-eq v2, v5, :cond_c

    const/16 v2, -0x7bf1

    .line 28
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 29
    :cond_c
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft1()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft1()I

    move-result v5

    if-eq v2, v5, :cond_d

    const/16 v2, -0x7bef

    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft1()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 31
    :cond_d
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaRight()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaRight()I

    move-result v5

    if-eq v2, v5, :cond_e

    const/16 v2, -0x7bf2

    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaRight()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 33
    :cond_e
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcLeft()S

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcLeft()S

    move-result v5

    if-eq v2, v5, :cond_f

    const/16 v2, 0x4456

    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcLeft()S

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 35
    :cond_f
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcLeft1()S

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcLeft1()S

    move-result v5

    if-eq v2, v5, :cond_10

    const/16 v2, 0x4457

    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcLeft1()S

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 37
    :cond_10
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcRight()S

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcRight()S

    move-result v5

    if-eq v2, v5, :cond_11

    const/16 v2, 0x4455

    .line 38
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxcRight()S

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 39
    :cond_11
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLspd()Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLspd()Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    const/4 v2, 0x4

    new-array v2, v2, [B

    .line 40
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLspd()Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;

    move-result-object v5

    invoke-virtual {v5, v2, v4}, Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;->serialize([BI)V

    const/16 v5, 0x6412

    .line 41
    invoke-static {v2}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([B)I

    move-result v2

    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 42
    :cond_12
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaBefore()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaBefore()I

    move-result v5

    if-eq v2, v5, :cond_13

    const/16 v2, -0x5bed

    .line 43
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaBefore()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 44
    :cond_13
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaAfter()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaAfter()I

    move-result v5

    if-eq v2, v5, :cond_14

    const/16 v2, -0x5bec

    .line 45
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaAfter()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 46
    :cond_14
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFDyaBeforeAuto()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFDyaBeforeAuto()Z

    move-result v5

    if-eq v2, v5, :cond_15

    const/16 v2, 0x245b

    .line 47
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFDyaBeforeAuto()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 48
    :cond_15
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFDyaAfterAuto()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFDyaAfterAuto()Z

    move-result v5

    if-eq v2, v5, :cond_16

    const/16 v2, 0x245c

    .line 49
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFDyaAfterAuto()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 50
    :cond_16
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFInTable()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFInTable()Z

    move-result v5

    if-eq v2, v5, :cond_17

    const/16 v2, 0x2416

    .line 51
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFInTable()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 52
    :cond_17
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTtp()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTtp()Z

    move-result v5

    if-eq v2, v5, :cond_18

    const/16 v2, 0x2417

    .line 53
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTtp()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 54
    :cond_18
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaAbs()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaAbs()I

    move-result v5

    if-eq v2, v5, :cond_19

    const/16 v2, -0x7be8

    .line 55
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaAbs()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 56
    :cond_19
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaAbs()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaAbs()I

    move-result v5

    if-eq v2, v5, :cond_1a

    const/16 v2, -0x7be7

    .line 57
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaAbs()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 58
    :cond_1a
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaWidth()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaWidth()I

    move-result v5

    if-eq v2, v5, :cond_1b

    const/16 v2, -0x7be6

    .line 59
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaWidth()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 60
    :cond_1b
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getWr()B

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getWr()B

    move-result v5

    if-eq v2, v5, :cond_1c

    const/16 v2, 0x2423

    .line 61
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getWr()B

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 62
    :cond_1c
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcBar()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcBar()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 63
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcBar()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    const/16 v5, 0x6428

    .line 64
    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 65
    :cond_1d
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcBottom()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcBottom()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 66
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcBottom()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    const/16 v5, 0x6426

    .line 67
    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 68
    :cond_1e
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcLeft()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcLeft()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    .line 69
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcLeft()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    const/16 v5, 0x6425

    .line 70
    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 71
    :cond_1f
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcRight()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcRight()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    .line 72
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcRight()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    const/16 v5, 0x6427

    .line 73
    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 74
    :cond_20
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcTop()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcTop()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 75
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcTop()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    move-result v2

    const/16 v5, 0x6424

    .line 76
    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 77
    :cond_21
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNoAutoHyph()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNoAutoHyph()Z

    move-result v5

    if-eq v2, v5, :cond_22

    const/16 v2, 0x242a

    .line 78
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNoAutoHyph()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 79
    :cond_22
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaHeight()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaHeight()I

    move-result v5

    if-ne v2, v5, :cond_23

    .line 80
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFMinHeight()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFMinHeight()Z

    move-result v5

    if-eq v2, v5, :cond_25

    .line 81
    :cond_23
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaHeight()I

    move-result v2

    int-to-short v2, v2

    .line 82
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFMinHeight()Z

    move-result v5

    if-eqz v5, :cond_24

    const v5, 0x8000

    or-int/2addr v2, v5

    int-to-short v2, v2

    :cond_24
    const/16 v5, 0x442b

    .line 83
    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 84
    :cond_25
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDcs()Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;

    move-result-object v2

    if-eqz v2, :cond_26

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDcs()Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDcs()Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 85
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDcs()Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;->toShort()S

    move-result v2

    const/16 v5, 0x442c

    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 86
    :cond_26
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    if-eqz v2, :cond_27

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    .line 87
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;->toShort()S

    move-result v2

    const/16 v5, 0x442d

    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 88
    :cond_27
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaFromText()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaFromText()I

    move-result v5

    if-eq v2, v5, :cond_28

    const/16 v2, -0x7bd2

    .line 89
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaFromText()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 90
    :cond_28
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaFromText()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaFromText()I

    move-result v5

    if-eq v2, v5, :cond_29

    const/16 v2, -0x7bd1

    .line 91
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaFromText()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 92
    :cond_29
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFLocked()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFLocked()Z

    move-result v5

    if-eq v2, v5, :cond_2a

    const/16 v2, 0x2430

    .line 93
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFLocked()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 94
    :cond_2a
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFWidowControl()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFWidowControl()Z

    move-result v5

    if-eq v2, v5, :cond_2b

    const/16 v2, 0x2431

    .line 95
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFWidowControl()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 96
    :cond_2b
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKinsoku()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKinsoku()Z

    move-result v5

    if-eq v2, v5, :cond_2c

    const/16 v2, 0x2433

    .line 97
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaBefore()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 98
    :cond_2c
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFWordWrap()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFWordWrap()Z

    move-result v5

    if-eq v2, v5, :cond_2d

    const/16 v2, 0x2434

    .line 99
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFWordWrap()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 100
    :cond_2d
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFOverflowPunct()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFOverflowPunct()Z

    move-result v5

    if-eq v2, v5, :cond_2e

    const/16 v2, 0x2435

    .line 101
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFOverflowPunct()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 102
    :cond_2e
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTopLinePunct()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTopLinePunct()Z

    move-result v5

    if-eq v2, v5, :cond_2f

    const/16 v2, 0x2436

    .line 103
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTopLinePunct()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 104
    :cond_2f
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFAutoSpaceDE()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFAutoSpaceDE()Z

    move-result v5

    if-eq v2, v5, :cond_30

    const/16 v2, 0x2437

    .line 105
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFAutoSpaceDE()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 106
    :cond_30
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFAutoSpaceDN()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFAutoSpaceDN()Z

    move-result v5

    if-eq v2, v5, :cond_31

    const/16 v2, 0x2438

    .line 107
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFAutoSpaceDN()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 108
    :cond_31
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getWAlignFont()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getWAlignFont()I

    move-result v5

    if-eq v2, v5, :cond_32

    const/16 v2, 0x4439

    .line 109
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getWAlignFont()I

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 110
    :cond_32
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFBackward()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFBackward()Z

    move-result v5

    if-ne v2, v5, :cond_33

    .line 111
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFVertical()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFVertical()Z

    move-result v5

    if-ne v2, v5, :cond_33

    .line 112
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFRotateFont()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFRotateFont()Z

    move-result v5

    if-eq v2, v5, :cond_37

    .line 113
    :cond_33
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFBackward()Z

    move-result v2

    if-eqz v2, :cond_34

    const/4 v2, 0x2

    goto :goto_1

    :cond_34
    const/4 v2, 0x0

    .line 114
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFVertical()Z

    move-result v5

    if-eqz v5, :cond_35

    or-int/lit8 v2, v2, 0x1

    .line 115
    :cond_35
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFRotateFont()Z

    move-result v5

    if-eqz v5, :cond_36

    or-int/lit8 v2, v2, 0x4

    :cond_36
    const/16 v5, 0x443a

    .line 116
    invoke-static {v5, v2, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 117
    :cond_37
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getAnld()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getAnld()[B

    move-result-object v5

    invoke-static {v2, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_38

    const/16 v2, -0x39c2

    .line 118
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getAnld()[B

    move-result-object v5

    invoke-static {v2, v4, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 119
    :cond_38
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFPropRMark()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFPropRMark()Z

    move-result v5

    if-ne v2, v5, :cond_39

    .line 120
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIbstPropRMark()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIbstPropRMark()I

    move-result v5

    if-ne v2, v5, :cond_39

    .line 121
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDttmPropRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDttmPropRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3a

    :cond_39
    const/4 v2, 0x7

    new-array v2, v2, [B

    .line 122
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFPropRMark()Z

    move-result v5

    int-to-byte v5, v5

    aput-byte v5, v2, v4

    .line 123
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIbstPropRMark()I

    move-result v5

    int-to-short v5, v5

    const/4 v6, 0x1

    invoke-static {v2, v6, v5}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 124
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDttmPropRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v2, v6}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;->serialize([BI)V

    const/16 v5, -0x39c1

    .line 125
    invoke-static {v5, v4, v2, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 126
    :cond_3a
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLvl()B

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLvl()B

    move-result v5

    if-eq v2, v5, :cond_3b

    const/16 v2, 0x2640

    .line 127
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLvl()B

    move-result v5

    invoke-static {v2, v5, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 128
    :cond_3b
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFBiDi()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFBiDi()Z

    move-result v5

    if-eq v2, v5, :cond_3c

    const/16 v2, 0x2441

    .line 129
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFBiDi()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 130
    :cond_3c
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNumRMIns()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNumRMIns()Z

    move-result v5

    if-eq v2, v5, :cond_3d

    const/16 v2, 0x2443

    .line 131
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNumRMIns()Z

    move-result v5

    invoke-static {v2, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 132
    :cond_3d
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getNumrm()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getNumrm()[B

    move-result-object v5

    invoke-static {v2, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3e

    const/16 v2, -0x39bb

    .line 133
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getNumrm()[B

    move-result-object v5

    invoke-static {v2, v4, v5, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 134
    :cond_3e
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFInnerTableCell()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFInnerTableCell()Z

    move-result v4

    if-eq v2, v4, :cond_3f

    const/16 v2, 0x244b

    .line 135
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFInnerTableCell()Z

    move-result v4

    invoke-static {v2, v4, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 136
    :cond_3f
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTtpEmbedded()Z

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTtpEmbedded()Z

    move-result v4

    if-eq v2, v4, :cond_40

    const/16 v2, 0x244c

    .line 137
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTtpEmbedded()Z

    move-result v4

    invoke-static {v2, v4, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SZLjava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 138
    :cond_40
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getItap()I

    move-result v2

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getItap()I

    move-result p1

    if-eq v2, p1, :cond_41

    const/16 p1, 0x6649

    .line 139
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getItap()I

    move-result p0

    invoke-static {p1, p0, v3, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->addSprm(SI[BLjava/util/List;)I

    move-result p0

    add-int/2addr v1, p0

    .line 140
    :cond_41
    invoke-static {v0, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmUtils;->getGrpprl(Ljava/util/List;I)[B

    move-result-object p0

    return-object p0
.end method

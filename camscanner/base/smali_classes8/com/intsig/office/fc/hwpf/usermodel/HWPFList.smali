.class public final Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;
.super Ljava/lang/Object;
.source "HWPFList.java"


# instance fields
.field private _listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

.field private _override:Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

.field private _registered:Z

.field private _styleSheet:Lcom/intsig/office/fc/hwpf/model/StyleSheet;


# direct methods
.method public constructor <init>(ZLcom/intsig/office/fc/hwpf/model/StyleSheet;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 5
    .line 6
    invoke-static {}, Ljava/lang/Math;->random()D

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 11
    .line 12
    .line 13
    move-result-wide v3

    .line 14
    long-to-double v3, v3

    .line 15
    mul-double v1, v1, v3

    .line 16
    .line 17
    double-to-int v1, v1

    .line 18
    invoke-direct {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;-><init>(IZ)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 22
    .line 23
    new-instance p1, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLsid()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;-><init>(I)V

    .line 32
    .line 33
    .line 34
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_override:Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 35
    .line 36
    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_styleSheet:Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public getListData()Lcom/intsig/office/fc/hwpf/model/POIListData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getOverride()Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_override:Lcom/intsig/office/fc/hwpf/model/ListFormatOverride;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setLevelNumberProperties(ILcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLevel(I)Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLevelStyle(I)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_styleSheet:Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    .line 14
    .line 15
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hwpf/model/StyleSheet;->getCharacterStyle(I)Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-static {p2, p1}, Lcom/intsig/office/fc/hwpf/sprm/CharacterSprmCompressor;->compressCharacterProperty(Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;Lcom/intsig/office/fc/hwpf/usermodel/CharacterProperties;)[B

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->setNumberProperties([B)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setLevelParagraphProperties(ILcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLevel(I)Lcom/intsig/office/fc/hwpf/model/POIListLevel;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hwpf/model/POIListData;->getLevelStyle(I)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_styleSheet:Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    .line 14
    .line 15
    invoke-virtual {v1, p1}, Lcom/intsig/office/fc/hwpf/model/StyleSheet;->getParagraphStyle(I)Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-static {p2, p1}, Lcom/intsig/office/fc/hwpf/sprm/ParagraphSprmCompressor;->compressParagraphProperty(Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;)[B

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/POIListLevel;->setLevelProperties([B)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setLevelStyle(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/HWPFList;->_listData:Lcom/intsig/office/fc/hwpf/model/POIListData;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/hwpf/model/POIListData;->setLevelStyle(II)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

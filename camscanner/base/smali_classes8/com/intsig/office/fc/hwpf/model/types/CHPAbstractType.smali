.class public abstract Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;
.super Ljava/lang/Object;
.source "CHPAbstractType.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# static fields
.field protected static final ISS_NONE:B = 0x0t

.field protected static final ISS_SUBSCRIPTED:B = 0x2t

.field protected static final ISS_SUPERSCRIPTED:B = 0x1t

.field protected static final KCD_CIRCLE:B = 0x3t

.field protected static final KCD_COMMA:B = 0x2t

.field protected static final KCD_DOT:B = 0x1t

.field protected static final KCD_NON:B = 0x0t

.field protected static final KCD_UNDER_DOT:B = 0x4t

.field protected static final KUL_BY_WORD:B = 0x2t

.field protected static final KUL_DASH:B = 0x7t

.field protected static final KUL_DASHED_HEAVY:B = 0x17t

.field protected static final KUL_DASH_LONG:B = 0x27t

.field protected static final KUL_DASH_LONG_HEAVY:B = 0x37t

.field protected static final KUL_DOT:B = 0x8t

.field protected static final KUL_DOTTED:B = 0x4t

.field protected static final KUL_DOTTED_HEAVY:B = 0x14t

.field protected static final KUL_DOT_DASH:B = 0x9t

.field protected static final KUL_DOT_DASH_HEAVY:B = 0x19t

.field protected static final KUL_DOT_DOT_DASH:B = 0xat

.field protected static final KUL_DOT_DOT_DASH_HEAVY:B = 0x1at

.field protected static final KUL_DOUBLE:B = 0x3t

.field protected static final KUL_HIDDEN:B = 0x5t

.field protected static final KUL_NONE:B = 0x0t

.field protected static final KUL_SINGLE:B = 0x1t

.field protected static final KUL_THICK:B = 0x6t

.field protected static final KUL_WAVE:B = 0xbt

.field protected static final KUL_WAVE_DOUBLE:B = 0x2bt

.field protected static final KUL_WAVE_HEAVY:B = 0x1bt

.field protected static final LBRCRJ_BOTH:B = 0x3t

.field protected static final LBRCRJ_LEFT:B = 0x1t

.field protected static final LBRCRJ_NONE:B = 0x0t

.field protected static final LBRCRJ_RIGHT:B = 0x2t

.field protected static final SFXTTEXT_BACKGROUND_BLINK:B = 0x2t

.field protected static final SFXTTEXT_LAS_VEGAS_LIGHTS:B = 0x1t

.field protected static final SFXTTEXT_MARCHING_ANTS:B = 0x4t

.field protected static final SFXTTEXT_MARCHING_RED_ANTS:B = 0x5t

.field protected static final SFXTTEXT_NO:B = 0x0t

.field protected static final SFXTTEXT_SHIMMER:B = 0x6t

.field protected static final SFXTTEXT_SPARKLE_TEXT:B = 0x3t

.field private static fBiDi:Lcom/intsig/office/fc/util/BitField;

.field private static fBold:Lcom/intsig/office/fc/util/BitField;

.field private static fBoldBi:Lcom/intsig/office/fc/util/BitField;

.field private static fCaps:Lcom/intsig/office/fc/util/BitField;

.field private static fCellFitText:Lcom/intsig/office/fc/util/BitField;

.field private static fChsDiff:Lcom/intsig/office/fc/util/BitField;

.field private static fComplexScripts:Lcom/intsig/office/fc/util/BitField;

.field private static fDStrike:Lcom/intsig/office/fc/util/BitField;

.field private static fData:Lcom/intsig/office/fc/util/BitField;

.field private static fEmboss:Lcom/intsig/office/fc/util/BitField;

.field private static fFldVanish:Lcom/intsig/office/fc/util/BitField;

.field private static fHighlight:Lcom/intsig/office/fc/util/BitField;

.field private static fImprint:Lcom/intsig/office/fc/util/BitField;

.field private static fItalic:Lcom/intsig/office/fc/util/BitField;

.field private static fItalicBi:Lcom/intsig/office/fc/util/BitField;

.field private static fKumimoji:Lcom/intsig/office/fc/util/BitField;

.field private static fLSFitText:Lcom/intsig/office/fc/util/BitField;

.field private static fLowerCase:Lcom/intsig/office/fc/util/BitField;

.field private static fMacChs:Lcom/intsig/office/fc/util/BitField;

.field private static fObj:Lcom/intsig/office/fc/util/BitField;

.field private static fOle2:Lcom/intsig/office/fc/util/BitField;

.field private static fOutline:Lcom/intsig/office/fc/util/BitField;

.field private static fRMark:Lcom/intsig/office/fc/util/BitField;

.field private static fRMarkDel:Lcom/intsig/office/fc/util/BitField;

.field private static fRuby:Lcom/intsig/office/fc/util/BitField;

.field private static fShadow:Lcom/intsig/office/fc/util/BitField;

.field private static fSmallCaps:Lcom/intsig/office/fc/util/BitField;

.field private static fSpec:Lcom/intsig/office/fc/util/BitField;

.field private static fStrike:Lcom/intsig/office/fc/util/BitField;

.field private static fTNY:Lcom/intsig/office/fc/util/BitField;

.field private static fTNYCompress:Lcom/intsig/office/fc/util/BitField;

.field private static fTNYFetchTxm:Lcom/intsig/office/fc/util/BitField;

.field private static fUsePgsuSettings:Lcom/intsig/office/fc/util/BitField;

.field private static fVanish:Lcom/intsig/office/fc/util/BitField;

.field private static fWarichu:Lcom/intsig/office/fc/util/BitField;

.field private static fWarichuNoOpenBracket:Lcom/intsig/office/fc/util/BitField;

.field private static iWarichuBracket:Lcom/intsig/office/fc/util/BitField;

.field private static icoHighlight:Lcom/intsig/office/fc/util/BitField;

.field private static itypFELayout:Lcom/intsig/office/fc/util/BitField;

.field private static spare:Lcom/intsig/office/fc/util/BitField;

.field private static unused:Lcom/intsig/office/fc/util/BitField;


# instance fields
.field protected field_10_pctCharWidth:I

.field protected field_11_lidDefault:I

.field protected field_12_lidFE:I

.field protected field_13_kcd:B

.field protected field_14_fUndetermine:Z

.field protected field_15_iss:B

.field protected field_16_fSpecSymbol:Z

.field protected field_17_idct:B

.field protected field_18_idctHint:B

.field protected field_19_kul:B

.field protected field_1_grpfChp:I

.field protected field_20_hresi:Lcom/intsig/office/fc/hwpf/model/Hyphenation;

.field protected field_21_hpsKern:I

.field protected field_22_hpsPos:S

.field protected field_23_shd:Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

.field protected field_24_brc:Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

.field protected field_25_ibstRMark:I

.field protected field_26_sfxtText:B

.field protected field_27_fDblBdr:Z

.field protected field_28_fBorderWS:Z

.field protected field_29_ufel:S

.field protected field_2_hps:I

.field protected field_30_copt:B

.field protected field_31_hpsAsci:I

.field protected field_32_hpsFE:I

.field protected field_33_hpsBi:I

.field protected field_34_ftcSym:I

.field protected field_35_xchSym:I

.field protected field_36_fcPic:I

.field protected field_37_fcObj:I

.field protected field_38_lTagObj:I

.field protected field_39_fcData:I

.field protected field_3_ftcAscii:I

.field protected field_40_hresiOld:Lcom/intsig/office/fc/hwpf/model/Hyphenation;

.field protected field_41_ibstRMarkDel:I

.field protected field_42_dttmRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

.field protected field_43_dttmRMarkDel:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

.field protected field_44_istd:I

.field protected field_45_idslRMReason:I

.field protected field_46_idslReasonDel:I

.field protected field_47_cpg:I

.field protected field_48_Highlight:S

.field protected field_49_CharsetFlags:S

.field protected field_4_ftcFE:I

.field protected field_50_chse:S

.field protected field_51_fPropRMark:Z

.field protected field_52_ibstPropRMark:I

.field protected field_53_dttmPropRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

.field protected field_54_fConflictOrig:Z

.field protected field_55_fConflictOtherDel:Z

.field protected field_56_wConflict:I

.field protected field_57_IbstConflict:I

.field protected field_58_dttmConflict:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

.field protected field_59_fDispFldRMark:Z

.field protected field_5_ftcOther:I

.field protected field_60_ibstDispFldRMark:I

.field protected field_61_dttmDispFldRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

.field protected field_62_xstDispFldRMark:[B

.field protected field_63_fcObjp:I

.field protected field_64_lbrCRJ:B

.field protected field_65_fSpecVanish:Z

.field protected field_66_fHasOldProps:Z

.field protected field_67_fSdtVanish:Z

.field protected field_68_wCharScale:I

.field protected field_69_underlineColor:Lcom/intsig/office/fc/hwpf/model/Colorref;

.field protected field_6_ftcBi:I

.field protected field_7_dxaSpace:I

.field protected field_8_cv:Lcom/intsig/office/fc/hwpf/model/Colorref;

.field protected field_9_ico:B


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 1
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBold:Lcom/intsig/office/fc/util/BitField;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 10
    .line 11
    const/4 v2, 0x2

    .line 12
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 13
    .line 14
    .line 15
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fItalic:Lcom/intsig/office/fc/util/BitField;

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 18
    .line 19
    const/4 v2, 0x4

    .line 20
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 21
    .line 22
    .line 23
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRMarkDel:Lcom/intsig/office/fc/util/BitField;

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 26
    .line 27
    const/16 v2, 0x8

    .line 28
    .line 29
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 30
    .line 31
    .line 32
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fOutline:Lcom/intsig/office/fc/util/BitField;

    .line 33
    .line 34
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 35
    .line 36
    const/16 v3, 0x10

    .line 37
    .line 38
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fFldVanish:Lcom/intsig/office/fc/util/BitField;

    .line 42
    .line 43
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 44
    .line 45
    const/16 v4, 0x20

    .line 46
    .line 47
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 48
    .line 49
    .line 50
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fSmallCaps:Lcom/intsig/office/fc/util/BitField;

    .line 51
    .line 52
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 53
    .line 54
    const/16 v5, 0x40

    .line 55
    .line 56
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fCaps:Lcom/intsig/office/fc/util/BitField;

    .line 60
    .line 61
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 62
    .line 63
    const/16 v6, 0x80

    .line 64
    .line 65
    invoke-direct {v0, v6}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 66
    .line 67
    .line 68
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fVanish:Lcom/intsig/office/fc/util/BitField;

    .line 69
    .line 70
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 71
    .line 72
    const/16 v7, 0x100

    .line 73
    .line 74
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 75
    .line 76
    .line 77
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRMark:Lcom/intsig/office/fc/util/BitField;

    .line 78
    .line 79
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 80
    .line 81
    const/16 v8, 0x200

    .line 82
    .line 83
    invoke-direct {v0, v8}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 84
    .line 85
    .line 86
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fSpec:Lcom/intsig/office/fc/util/BitField;

    .line 87
    .line 88
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 89
    .line 90
    const/16 v9, 0x400

    .line 91
    .line 92
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 93
    .line 94
    .line 95
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fStrike:Lcom/intsig/office/fc/util/BitField;

    .line 96
    .line 97
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 98
    .line 99
    const/16 v10, 0x800

    .line 100
    .line 101
    invoke-direct {v0, v10}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 102
    .line 103
    .line 104
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fObj:Lcom/intsig/office/fc/util/BitField;

    .line 105
    .line 106
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 107
    .line 108
    const/16 v11, 0x1000

    .line 109
    .line 110
    invoke-direct {v0, v11}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 111
    .line 112
    .line 113
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fShadow:Lcom/intsig/office/fc/util/BitField;

    .line 114
    .line 115
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 116
    .line 117
    const/16 v12, 0x2000

    .line 118
    .line 119
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 120
    .line 121
    .line 122
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fLowerCase:Lcom/intsig/office/fc/util/BitField;

    .line 123
    .line 124
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 125
    .line 126
    const/16 v12, 0x4000

    .line 127
    .line 128
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 129
    .line 130
    .line 131
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fData:Lcom/intsig/office/fc/util/BitField;

    .line 132
    .line 133
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 134
    .line 135
    const v12, 0x8000

    .line 136
    .line 137
    .line 138
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 139
    .line 140
    .line 141
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fOle2:Lcom/intsig/office/fc/util/BitField;

    .line 142
    .line 143
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 144
    .line 145
    const/high16 v12, 0x10000

    .line 146
    .line 147
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 148
    .line 149
    .line 150
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fEmboss:Lcom/intsig/office/fc/util/BitField;

    .line 151
    .line 152
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 153
    .line 154
    const/high16 v12, 0x20000

    .line 155
    .line 156
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 157
    .line 158
    .line 159
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fImprint:Lcom/intsig/office/fc/util/BitField;

    .line 160
    .line 161
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 162
    .line 163
    const/high16 v12, 0x40000

    .line 164
    .line 165
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 166
    .line 167
    .line 168
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fDStrike:Lcom/intsig/office/fc/util/BitField;

    .line 169
    .line 170
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 171
    .line 172
    const/high16 v12, 0x80000

    .line 173
    .line 174
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 175
    .line 176
    .line 177
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fUsePgsuSettings:Lcom/intsig/office/fc/util/BitField;

    .line 178
    .line 179
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 180
    .line 181
    const/high16 v12, 0x100000

    .line 182
    .line 183
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 184
    .line 185
    .line 186
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBoldBi:Lcom/intsig/office/fc/util/BitField;

    .line 187
    .line 188
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 189
    .line 190
    const/high16 v12, 0x200000

    .line 191
    .line 192
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 193
    .line 194
    .line 195
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fComplexScripts:Lcom/intsig/office/fc/util/BitField;

    .line 196
    .line 197
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 198
    .line 199
    const/high16 v12, 0x400000

    .line 200
    .line 201
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 202
    .line 203
    .line 204
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fItalicBi:Lcom/intsig/office/fc/util/BitField;

    .line 205
    .line 206
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 207
    .line 208
    const/high16 v12, 0x800000

    .line 209
    .line 210
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 211
    .line 212
    .line 213
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBiDi:Lcom/intsig/office/fc/util/BitField;

    .line 214
    .line 215
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 216
    .line 217
    const/16 v12, 0xff

    .line 218
    .line 219
    invoke-direct {v0, v12}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 220
    .line 221
    .line 222
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->itypFELayout:Lcom/intsig/office/fc/util/BitField;

    .line 223
    .line 224
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 225
    .line 226
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 227
    .line 228
    .line 229
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNY:Lcom/intsig/office/fc/util/BitField;

    .line 230
    .line 231
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 232
    .line 233
    invoke-direct {v0, v8}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 234
    .line 235
    .line 236
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fWarichu:Lcom/intsig/office/fc/util/BitField;

    .line 237
    .line 238
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 239
    .line 240
    invoke-direct {v0, v9}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 241
    .line 242
    .line 243
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fKumimoji:Lcom/intsig/office/fc/util/BitField;

    .line 244
    .line 245
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 246
    .line 247
    invoke-direct {v0, v10}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 248
    .line 249
    .line 250
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRuby:Lcom/intsig/office/fc/util/BitField;

    .line 251
    .line 252
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 253
    .line 254
    invoke-direct {v0, v11}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 255
    .line 256
    .line 257
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fLSFitText:Lcom/intsig/office/fc/util/BitField;

    .line 258
    .line 259
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 260
    .line 261
    const v7, 0xe000

    .line 262
    .line 263
    .line 264
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 265
    .line 266
    .line 267
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->spare:Lcom/intsig/office/fc/util/BitField;

    .line 268
    .line 269
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 270
    .line 271
    const/4 v7, 0x7

    .line 272
    invoke-direct {v0, v7}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 273
    .line 274
    .line 275
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->iWarichuBracket:Lcom/intsig/office/fc/util/BitField;

    .line 276
    .line 277
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 278
    .line 279
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 280
    .line 281
    .line 282
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fWarichuNoOpenBracket:Lcom/intsig/office/fc/util/BitField;

    .line 283
    .line 284
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 285
    .line 286
    invoke-direct {v0, v3}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 287
    .line 288
    .line 289
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNYCompress:Lcom/intsig/office/fc/util/BitField;

    .line 290
    .line 291
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 292
    .line 293
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 294
    .line 295
    .line 296
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNYFetchTxm:Lcom/intsig/office/fc/util/BitField;

    .line 297
    .line 298
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 299
    .line 300
    invoke-direct {v0, v5}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 301
    .line 302
    .line 303
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fCellFitText:Lcom/intsig/office/fc/util/BitField;

    .line 304
    .line 305
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 306
    .line 307
    invoke-direct {v0, v6}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 308
    .line 309
    .line 310
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->unused:Lcom/intsig/office/fc/util/BitField;

    .line 311
    .line 312
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 313
    .line 314
    const/16 v2, 0x1f

    .line 315
    .line 316
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 317
    .line 318
    .line 319
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->icoHighlight:Lcom/intsig/office/fc/util/BitField;

    .line 320
    .line 321
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 322
    .line 323
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 324
    .line 325
    .line 326
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fHighlight:Lcom/intsig/office/fc/util/BitField;

    .line 327
    .line 328
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 329
    .line 330
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 331
    .line 332
    .line 333
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fChsDiff:Lcom/intsig/office/fc/util/BitField;

    .line 334
    .line 335
    new-instance v0, Lcom/intsig/office/fc/util/BitField;

    .line 336
    .line 337
    invoke-direct {v0, v4}, Lcom/intsig/office/fc/util/BitField;-><init>(I)V

    .line 338
    .line 339
    .line 340
    sput-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fMacChs:Lcom/intsig/office/fc/util/BitField;

    .line 341
    .line 342
    return-void
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x14

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/model/Colorref;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_8_cv:Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 14
    .line 15
    const/16 v0, 0x400

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    .line 18
    .line 19
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 22
    .line 23
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/model/Hyphenation;-><init>()V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 29
    .line 30
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;-><init>()V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_23_shd:Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 34
    .line 35
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 36
    .line 37
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;-><init>()V

    .line 38
    .line 39
    .line 40
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_24_brc:Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 41
    .line 42
    const/4 v0, -0x1

    .line 43
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    .line 44
    .line 45
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 46
    .line 47
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/model/Hyphenation;-><init>()V

    .line 48
    .line 49
    .line 50
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 51
    .line 52
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 53
    .line 54
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>()V

    .line 55
    .line 56
    .line 57
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 58
    .line 59
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 60
    .line 61
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>()V

    .line 62
    .line 63
    .line 64
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 65
    .line 66
    const/16 v0, 0xa

    .line 67
    .line 68
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    .line 69
    .line 70
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 71
    .line 72
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>()V

    .line 73
    .line 74
    .line 75
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 76
    .line 77
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 78
    .line 79
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>()V

    .line 80
    .line 81
    .line 82
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 83
    .line 84
    new-instance v0, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 85
    .line 86
    invoke-direct {v0}, Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;-><init>()V

    .line 87
    .line 88
    .line 89
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 90
    .line 91
    const/4 v0, 0x0

    .line 92
    new-array v0, v0, [B

    .line 93
    .line 94
    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    .line 95
    .line 96
    const/16 v0, 0x64

    .line 97
    .line 98
    iput v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method


# virtual methods
.method public getBrc()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_24_brc:Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCharsetFlags()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getChse()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_50_chse:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCopt()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCpg()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_47_cpg:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCv()Lcom/intsig/office/fc/hwpf/model/Colorref;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_8_cv:Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDttmConflict()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDttmDispFldRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDttmPropRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDttmRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDttmRMarkDel()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDxaSpace()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_7_dxaSpace:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFBorderWS()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_28_fBorderWS:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFConflictOrig()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_54_fConflictOrig:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFConflictOtherDel()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_55_fConflictOtherDel:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFDblBdr()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_27_fDblBdr:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFDispFldRMark()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_59_fDispFldRMark:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFHasOldProps()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_66_fHasOldProps:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFPropRMark()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_51_fPropRMark:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFSdtVanish()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_67_fSdtVanish:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFSpecSymbol()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_16_fSpecSymbol:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFSpecVanish()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_65_fSpecVanish:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFUndetermine()Z
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_14_fUndetermine:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFcData()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_39_fcData:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFcObj()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_37_fcObj:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFcObjp()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_63_fcObjp:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFcPic()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFtcAscii()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_3_ftcAscii:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFtcBi()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_6_ftcBi:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFtcFE()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_4_ftcFE:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFtcOther()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_5_ftcOther:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFtcSym()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_34_ftcSym:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getGrpfChp()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHighlight()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHps()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHpsAsci()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_31_hpsAsci:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHpsBi()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_33_hpsBi:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHpsFE()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_32_hpsFE:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHpsKern()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_21_hpsKern:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHpsPos()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_22_hpsPos:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHresi()Lcom/intsig/office/fc/hwpf/model/Hyphenation;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHresiOld()Lcom/intsig/office/fc/hwpf/model/Hyphenation;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIWarichuBracket()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->iWarichuBracket:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIbstConflict()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_57_IbstConflict:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIbstDispFldRMark()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_60_ibstDispFldRMark:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIbstPropRMark()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_52_ibstPropRMark:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIbstRMark()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_25_ibstRMark:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIbstRMarkDel()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_41_ibstRMarkDel:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIco()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_9_ico:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIcoHighlight()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->icoHighlight:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIdct()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_17_idct:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIdctHint()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_18_idctHint:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIdslRMReason()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_45_idslRMReason:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIdslReasonDel()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_46_idslReasonDel:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIss()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_15_iss:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIstd()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getItypFELayout()S
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->itypFELayout:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-short v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getKcd()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_13_kcd:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getKul()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_19_kul:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLTagObj()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_38_lTagObj:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLbrCRJ()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_64_lbrCRJ:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLidDefault()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLidFE()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPctCharWidth()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_10_pctCharWidth:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSfxtText()B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_26_sfxtText:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_23_shd:Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSpare()B
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->spare:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->getValue(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-byte v0, v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getUfel()S
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getUnderlineColor()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_69_underlineColor:Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/Colorref;->getValue()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0

    .line 10
    :cond_0
    const/4 v0, -0x1

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWCharScale()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWConflict()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_56_wConflict:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXchSym()I
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_35_xchSym:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getXstDispFldRMark()[B
    .locals 1
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFBiDi()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBiDi:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFBold()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBold:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFBoldBi()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBoldBi:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFCaps()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fCaps:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFCellFitText()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fCellFitText:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFChsDiff()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fChsDiff:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFComplexScripts()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fComplexScripts:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFDStrike()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fDStrike:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFData()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fData:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFEmboss()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fEmboss:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFFldVanish()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fFldVanish:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFHighlight()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fHighlight:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFImprint()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fImprint:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFItalic()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fItalic:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFItalicBi()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fItalicBi:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFKumimoji()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fKumimoji:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFLSFitText()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fLSFitText:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFLowerCase()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fLowerCase:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFMacChs()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fMacChs:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFObj()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fObj:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFOle2()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fOle2:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFOutline()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fOutline:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFRMark()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRMark:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFRMarkDel()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRMarkDel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFRuby()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRuby:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFShadow()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fShadow:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSmallCaps()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fSmallCaps:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFSpec()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fSpec:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFStrike()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fStrike:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFTNY()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNY:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFTNYCompress()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNYCompress:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFTNYFetchTxm()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNYFetchTxm:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFUsePgsuSettings()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fUsePgsuSettings:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFVanish()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fVanish:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFWarichu()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fWarichu:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isFWarichuNoOpenBracket()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fWarichuNoOpenBracket:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isUnused()Z
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->unused:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/util/BitField;->isSet(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setBrc(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_24_brc:Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCharsetFlags(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setChse(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_50_chse:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCopt(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCpg(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_47_cpg:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setCv(Lcom/intsig/office/fc/hwpf/model/Colorref;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_8_cv:Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDttmConflict(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_58_dttmConflict:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDttmDispFldRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_61_dttmDispFldRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDttmPropRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_53_dttmPropRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDttmRMark(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_42_dttmRMark:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDttmRMarkDel(Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_43_dttmRMarkDel:Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDxaSpace(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_7_dxaSpace:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFBiDi(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBiDi:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFBold(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBold:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFBoldBi(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fBoldBi:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFBorderWS(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_28_fBorderWS:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFCaps(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fCaps:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFCellFitText(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fCellFitText:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFChsDiff(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fChsDiff:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFComplexScripts(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fComplexScripts:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFConflictOrig(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_54_fConflictOrig:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFConflictOtherDel(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_55_fConflictOtherDel:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFDStrike(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fDStrike:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFData(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fData:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFDblBdr(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_27_fDblBdr:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFDispFldRMark(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_59_fDispFldRMark:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFEmboss(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fEmboss:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFFldVanish(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fFldVanish:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFHasOldProps(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_66_fHasOldProps:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFHighlight(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fHighlight:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFImprint(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fImprint:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFItalic(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fItalic:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFItalicBi(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fItalicBi:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFKumimoji(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fKumimoji:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFLSFitText(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fLSFitText:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFLowerCase(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fLowerCase:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFMacChs(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fMacChs:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_49_CharsetFlags:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFObj(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fObj:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFOle2(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fOle2:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFOutline(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fOutline:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFPropRMark(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_51_fPropRMark:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFRMark(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRMark:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFRMarkDel(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRMarkDel:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFRuby(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fRuby:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSdtVanish(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_67_fSdtVanish:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFShadow(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fShadow:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSmallCaps(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fSmallCaps:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSpec(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fSpec:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSpecSymbol(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_16_fSpecSymbol:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFSpecVanish(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_65_fSpecVanish:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFStrike(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fStrike:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFTNY(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNY:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFTNYCompress(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNYCompress:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFTNYFetchTxm(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fTNYFetchTxm:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFUndetermine(Z)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_14_fUndetermine:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFUsePgsuSettings(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fUsePgsuSettings:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFVanish(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fVanish:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFWarichu(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fWarichu:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFWarichuNoOpenBracket(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->fWarichuNoOpenBracket:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFcData(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_39_fcData:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFcObj(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_37_fcObj:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFcObjp(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_63_fcObjp:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFcPic(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_36_fcPic:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFtcAscii(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_3_ftcAscii:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFtcBi(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_6_ftcBi:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFtcFE(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_4_ftcFE:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFtcOther(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_5_ftcOther:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFtcSym(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_34_ftcSym:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setGrpfChp(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_1_grpfChp:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHighlight(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHps(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_2_hps:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHpsAsci(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_31_hpsAsci:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHpsBi(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_33_hpsBi:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHpsFE(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_32_hpsFE:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHpsKern(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_21_hpsKern:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHpsPos(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_22_hpsPos:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHresi(Lcom/intsig/office/fc/hwpf/model/Hyphenation;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_20_hresi:Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setHresiOld(Lcom/intsig/office/fc/hwpf/model/Hyphenation;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_40_hresiOld:Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIWarichuBracket(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->iWarichuBracket:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIbstConflict(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_57_IbstConflict:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIbstDispFldRMark(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_60_ibstDispFldRMark:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIbstPropRMark(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_52_ibstPropRMark:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIbstRMark(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_25_ibstRMark:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIbstRMarkDel(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_41_ibstRMarkDel:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIco(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_9_ico:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIcoHighlight(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->icoHighlight:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_48_Highlight:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIdct(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_17_idct:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIdctHint(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_18_idctHint:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIdslRMReason(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_45_idslRMReason:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIdslReasonDel(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_46_idslReasonDel:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIss(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_15_iss:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIstd(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_44_istd:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setItypFELayout(S)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->itypFELayout:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setKcd(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_13_kcd:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setKul(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_19_kul:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLTagObj(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_38_lTagObj:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLbrCRJ(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_64_lbrCRJ:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLidDefault(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_11_lidDefault:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLidFE(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_12_lidFE:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPctCharWidth(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_10_pctCharWidth:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSfxtText(B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_26_sfxtText:B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShd(Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_23_shd:Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSpare(B)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->spare:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-short v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setValue(II)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-short p1, p1

    .line 10
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUfel(S)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_29_ufel:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnderlineColor(Lcom/intsig/office/fc/hwpf/model/Colorref;)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_69_underlineColor:Lcom/intsig/office/fc/hwpf/model/Colorref;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setUnused(Z)V
    .locals 2
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->unused:Lcom/intsig/office/fc/util/BitField;

    .line 2
    .line 3
    iget-byte v1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 4
    .line 5
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/util/BitField;->setBoolean(IZ)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    int-to-byte p1, p1

    .line 10
    iput-byte p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_30_copt:B

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWCharScale(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_68_wCharScale:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWConflict(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_56_wConflict:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXchSym(I)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_35_xchSym:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setXstDispFldRMark([B)V
    .locals 0
    .annotation runtime Lcom/intsig/office/fc/util/Internal;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->field_62_xstDispFldRMark:[B

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[CHP]\n"

    .line 2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "    .grpfChp              = "

    .line 3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ("

    .line 4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getGrpfChp()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " )\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "         .fBold                    = "

    .line 5
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFBold()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fItalic                  = "

    .line 6
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFItalic()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fRMarkDel                = "

    .line 7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFRMarkDel()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fOutline                 = "

    .line 8
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFOutline()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fFldVanish               = "

    .line 9
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFFldVanish()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSmallCaps               = "

    .line 10
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFSmallCaps()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fCaps                    = "

    .line 11
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFCaps()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fVanish                  = "

    .line 12
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFVanish()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fRMark                   = "

    .line 13
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFRMark()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fSpec                    = "

    .line 14
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFSpec()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fStrike                  = "

    .line 15
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFStrike()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fObj                     = "

    .line 16
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFObj()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fShadow                  = "

    .line 17
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFShadow()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fLowerCase               = "

    .line 18
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFLowerCase()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fData                    = "

    .line 19
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFData()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fOle2                    = "

    .line 20
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFOle2()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fEmboss                  = "

    .line 21
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFEmboss()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fImprint                 = "

    .line 22
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFImprint()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fDStrike                 = "

    .line 23
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFDStrike()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fUsePgsuSettings         = "

    .line 24
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFUsePgsuSettings()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fBoldBi                  = "

    .line 25
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFBoldBi()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fComplexScripts          = "

    .line 26
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFComplexScripts()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fItalicBi                = "

    .line 27
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFItalicBi()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fBiDi                    = "

    .line 28
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFBiDi()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .hps                  = "

    .line 29
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHps()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ftcAscii             = "

    .line 31
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFtcAscii()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ftcFE                = "

    .line 33
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFtcFE()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ftcOther             = "

    .line 35
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFtcOther()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ftcBi                = "

    .line 37
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFtcBi()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .dxaSpace             = "

    .line 39
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDxaSpace()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cv                   = "

    .line 41
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getCv()Lcom/intsig/office/fc/hwpf/model/Colorref;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ico                  = "

    .line 43
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIco()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .pctCharWidth         = "

    .line 45
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getPctCharWidth()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .lidDefault           = "

    .line 47
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getLidDefault()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .lidFE                = "

    .line 49
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getLidFE()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .kcd                  = "

    .line 51
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getKcd()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .fUndetermine         = "

    .line 53
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFUndetermine()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .iss                  = "

    .line 55
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIss()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .fSpecSymbol          = "

    .line 57
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFSpecSymbol()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .idct                 = "

    .line 59
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIdct()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .idctHint             = "

    .line 61
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIdctHint()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .kul                  = "

    .line 63
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getKul()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .hresi                = "

    .line 65
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHresi()Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .hpsKern              = "

    .line 67
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsKern()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .hpsPos               = "

    .line 69
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsPos()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .shd                  = "

    .line 71
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .brc                  = "

    .line 73
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getBrc()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ibstRMark            = "

    .line 75
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIbstRMark()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .sfxtText             = "

    .line 77
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getSfxtText()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .fDblBdr              = "

    .line 79
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFDblBdr()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .fBorderWS            = "

    .line 81
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFBorderWS()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ufel                 = "

    .line 83
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getUfel()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .itypFELayout             = "

    .line 85
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getItypFELayout()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fTNY                     = "

    .line 86
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFTNY()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fWarichu                 = "

    .line 87
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFWarichu()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fKumimoji                = "

    .line 88
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFKumimoji()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fRuby                    = "

    .line 89
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFRuby()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fLSFitText               = "

    .line 90
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFLSFitText()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .spare                    = "

    .line 91
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getSpare()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .copt                 = "

    .line 92
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getCopt()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .iWarichuBracket          = "

    .line 94
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIWarichuBracket()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fWarichuNoOpenBracket     = "

    .line 95
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFWarichuNoOpenBracket()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fTNYCompress             = "

    .line 96
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFTNYCompress()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fTNYFetchTxm             = "

    .line 97
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFTNYFetchTxm()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fCellFitText             = "

    .line 98
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFCellFitText()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .unused                   = "

    .line 99
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isUnused()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .hpsAsci              = "

    .line 100
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsAsci()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .hpsFE                = "

    .line 102
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsFE()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .hpsBi                = "

    .line 104
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHpsBi()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ftcSym               = "

    .line 106
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFtcSym()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .xchSym               = "

    .line 108
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getXchSym()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .fcPic                = "

    .line 110
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFcPic()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .fcObj                = "

    .line 112
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFcObj()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .lTagObj              = "

    .line 114
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getLTagObj()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .fcData               = "

    .line 116
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFcData()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .hresiOld             = "

    .line 118
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHresiOld()Lcom/intsig/office/fc/hwpf/model/Hyphenation;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .ibstRMarkDel         = "

    .line 120
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIbstRMarkDel()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .dttmRMark            = "

    .line 122
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .dttmRMarkDel         = "

    .line 124
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmRMarkDel()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .istd                 = "

    .line 126
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIstd()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .idslRMReason         = "

    .line 128
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIdslRMReason()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .idslReasonDel        = "

    .line 130
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIdslReasonDel()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .cpg                  = "

    .line 132
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getCpg()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "    .Highlight            = "

    .line 134
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getHighlight()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .icoHighlight             = "

    .line 136
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIcoHighlight()B

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fHighlight               = "

    .line 137
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFHighlight()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "    .CharsetFlags         = "

    .line 138
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getCharsetFlags()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "         .fChsDiff                 = "

    .line 140
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFChsDiff()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "         .fMacChs                  = "

    .line 141
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->isFMacChs()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v3, "    .chse                 = "

    .line 142
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getChse()S

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .fPropRMark           = "

    .line 144
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFPropRMark()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .ibstPropRMark        = "

    .line 146
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIbstPropRMark()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .dttmPropRMark        = "

    .line 148
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmPropRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .fConflictOrig        = "

    .line 150
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFConflictOrig()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .fConflictOtherDel    = "

    .line 152
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFConflictOtherDel()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .wConflict            = "

    .line 154
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getWConflict()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .IbstConflict         = "

    .line 156
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIbstConflict()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .dttmConflict         = "

    .line 158
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmConflict()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .fDispFldRMark        = "

    .line 160
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFDispFldRMark()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .ibstDispFldRMark     = "

    .line 162
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getIbstDispFldRMark()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .dttmDispFldRMark     = "

    .line 164
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getDttmDispFldRMark()Lcom/intsig/office/fc/hwpf/usermodel/DateAndTime;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .xstDispFldRMark      = "

    .line 166
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getXstDispFldRMark()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .fcObjp               = "

    .line 168
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFcObjp()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .lbrCRJ               = "

    .line 170
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getLbrCRJ()B

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .fSpecVanish          = "

    .line 172
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFSpecVanish()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .fHasOldProps         = "

    .line 174
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFHasOldProps()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .fSdtVanish           = "

    .line 176
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getFSdtVanish()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "    .wCharScale           = "

    .line 178
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/model/types/CHPAbstractType;->getWCharScale()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "[/CHP]\n"

    .line 180
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

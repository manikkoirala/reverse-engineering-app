.class public final Lcom/intsig/office/fc/hwpf/model/SavedByTable;
.super Ljava/lang/Object;
.source "SavedByTable.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# instance fields
.field private entries:[Lcom/intsig/office/fc/hwpf/model/SavedByEntry;


# direct methods
.method public constructor <init>([BII)V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {p1, p2}, Lcom/intsig/office/fc/hwpf/model/SttbfUtils;->〇080([BI)[Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    array-length p2, p1

    .line 9
    div-int/lit8 p2, p2, 0x2

    .line 10
    .line 11
    new-array p3, p2, [Lcom/intsig/office/fc/hwpf/model/SavedByEntry;

    .line 12
    .line 13
    iput-object p3, p0, Lcom/intsig/office/fc/hwpf/model/SavedByTable;->entries:[Lcom/intsig/office/fc/hwpf/model/SavedByEntry;

    .line 14
    .line 15
    const/4 p3, 0x0

    .line 16
    :goto_0
    if-ge p3, p2, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/SavedByTable;->entries:[Lcom/intsig/office/fc/hwpf/model/SavedByEntry;

    .line 19
    .line 20
    new-instance v1, Lcom/intsig/office/fc/hwpf/model/SavedByEntry;

    .line 21
    .line 22
    mul-int/lit8 v2, p3, 0x2

    .line 23
    .line 24
    aget-object v3, p1, v2

    .line 25
    .line 26
    add-int/lit8 v2, v2, 0x1

    .line 27
    .line 28
    aget-object v2, p1, v2

    .line 29
    .line 30
    invoke-direct {v1, v3, v2}, Lcom/intsig/office/fc/hwpf/model/SavedByEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    aput-object v1, v0, p3

    .line 34
    .line 35
    add-int/lit8 p3, p3, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hwpf/model/SavedByEntry;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/SavedByTable;->entries:[Lcom/intsig/office/fc/hwpf/model/SavedByEntry;

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/SavedByTable;->entries:[Lcom/intsig/office/fc/hwpf/model/SavedByEntry;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    mul-int/lit8 v1, v1, 0x2

    .line 5
    .line 6
    new-array v1, v1, [Ljava/lang/String;

    .line 7
    .line 8
    array-length v2, v0

    .line 9
    const/4 v3, 0x0

    .line 10
    const/4 v4, 0x0

    .line 11
    :goto_0
    if-ge v3, v2, :cond_0

    .line 12
    .line 13
    aget-object v5, v0, v3

    .line 14
    .line 15
    add-int/lit8 v6, v4, 0x1

    .line 16
    .line 17
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/SavedByEntry;->getUserName()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v7

    .line 21
    aput-object v7, v1, v4

    .line 22
    .line 23
    add-int/lit8 v4, v6, 0x1

    .line 24
    .line 25
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/SavedByEntry;->getSaveLocation()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v5

    .line 29
    aput-object v5, v1, v6

    .line 30
    .line 31
    add-int/lit8 v3, v3, 0x1

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    invoke-static {p1, v1}, Lcom/intsig/office/fc/hwpf/model/SttbfUtils;->〇o00〇〇Oo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;[Ljava/lang/String;)I

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
.end method

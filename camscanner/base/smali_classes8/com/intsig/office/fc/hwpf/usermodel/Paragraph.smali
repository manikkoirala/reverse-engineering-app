.class public Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;
.super Lcom/intsig/office/fc/hwpf/usermodel/Range;
.source "Paragraph.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SPRM_ANLD:S = -0x39c2s

.field public static final SPRM_AUTOSPACEDE:S = 0x2437s

.field public static final SPRM_AUTOSPACEDN:S = 0x2438s

.field public static final SPRM_BRCBAR:S = 0x6629s

.field public static final SPRM_BRCBOTTOM:S = 0x6426s

.field public static final SPRM_BRCL:S = 0x2408s

.field public static final SPRM_BRCLEFT:S = 0x6425s

.field public static final SPRM_BRCP:S = 0x2409s

.field public static final SPRM_BRCRIGHT:S = 0x6427s

.field public static final SPRM_BRCTOP:S = 0x6424s

.field public static final SPRM_CHGTABS:S = -0x39ebs

.field public static final SPRM_CHGTABSPAPX:S = -0x39f3s

.field public static final SPRM_CRLF:S = 0x2444s

.field public static final SPRM_DCS:S = 0x442cs

.field public static final SPRM_DXAABS:S = -0x7be8s

.field public static final SPRM_DXAFROMTEXT:S = -0x7bd1s

.field public static final SPRM_DXALEFT:S = -0x7bf1s

.field public static final SPRM_DXALEFT1:S = -0x7befs

.field public static final SPRM_DXARIGHT:S = -0x7bf2s

.field public static final SPRM_DXAWIDTH:S = -0x7be6s

.field public static final SPRM_DYAABS:S = -0x7be7s

.field public static final SPRM_DYAAFTER:S = -0x5becs

.field public static final SPRM_DYABEFORE:S = -0x5beds

.field public static final SPRM_DYAFROMTEXT:S = -0x7bd2s

.field public static final SPRM_DYALINE:S = 0x6412s

.field public static final SPRM_FADJUSTRIGHT:S = 0x2448s

.field public static final SPRM_FBIDI:S = 0x2441s

.field public static final SPRM_FINTABLE:S = 0x2416s

.field public static final SPRM_FKEEP:S = 0x2405s

.field public static final SPRM_FKEEPFOLLOW:S = 0x2406s

.field public static final SPRM_FKINSOKU:S = 0x2433s

.field public static final SPRM_FLOCKED:S = 0x2430s

.field public static final SPRM_FNOAUTOHYPH:S = 0x242as

.field public static final SPRM_FNOLINENUMB:S = 0x240cs

.field public static final SPRM_FNUMRMLNS:S = 0x2443s

.field public static final SPRM_FOVERFLOWPUNCT:S = 0x2435s

.field public static final SPRM_FPAGEBREAKBEFORE:S = 0x2407s

.field public static final SPRM_FRAMETEXTFLOW:S = 0x443as

.field public static final SPRM_FSIDEBYSIDE:S = 0x2404s

.field public static final SPRM_FTOPLINEPUNCT:S = 0x2436s

.field public static final SPRM_FTTP:S = 0x2417s

.field public static final SPRM_FWIDOWCONTROL:S = 0x2431s

.field public static final SPRM_FWORDWRAP:S = 0x2434s

.field public static final SPRM_ILFO:S = 0x460bs

.field public static final SPRM_ILVL:S = 0x260as

.field public static final SPRM_JC:S = 0x2403s

.field public static final SPRM_NUMRM:S = -0x39bbs

.field public static final SPRM_OUTLVL:S = 0x2640s

.field public static final SPRM_PC:S = 0x261bs

.field public static final SPRM_PROPRMARK:S = -0x39c1s

.field public static final SPRM_RULER:S = -0x39ces

.field public static final SPRM_SHD:S = 0x442ds

.field public static final SPRM_USEPGSUSETTINGS:S = 0x2447s

.field public static final SPRM_WALIGNFONT:S = 0x4439s

.field public static final SPRM_WHEIGHTABS:S = 0x442bs

.field public static final SPRM_WR:S = 0x2423s


# instance fields
.field protected _istd:S

.field protected _papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

.field protected _props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;


# direct methods
.method protected constructor <init>(IILcom/intsig/office/fc/hwpf/usermodel/Table;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/fc/hwpf/usermodel/Range;-><init>(IILcom/intsig/office/fc/hwpf/usermodel/Range;)V

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->initAll()V

    .line 3
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_paragraphs:Ljava/util/List;

    iget p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_parEnd:I

    add-int/lit8 p2, p2, -0x1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/hwpf/model/PAPX;

    .line 4
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_doc:Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getStyleSheet()Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getParagraphProperties(Lcom/intsig/office/fc/hwpf/model/StyleSheet;)Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getSprmBuf()Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getIstd()S

    move-result p1

    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_istd:S

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/hwpf/model/PAPX;Lcom/intsig/office/fc/hwpf/usermodel/Range;)V
    .locals 3

    .line 7
    iget v0, p2, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_start:I

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p2, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {p0, v0, v1, p2}, Lcom/intsig/office/fc/hwpf/usermodel/Range;-><init>(IILcom/intsig/office/fc/hwpf/usermodel/Range;)V

    .line 8
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_doc:Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getStyleSheet()Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getParagraphProperties(Lcom/intsig/office/fc/hwpf/model/StyleSheet;)Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getSprmBuf()Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getIstd()S

    move-result p1

    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_istd:S

    return-void
.end method

.method protected constructor <init>(Lcom/intsig/office/fc/hwpf/model/PAPX;Lcom/intsig/office/fc/hwpf/usermodel/Range;I)V
    .locals 2

    .line 11
    iget v0, p2, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_start:I

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result p3

    iget v0, p2, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_end:I

    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, p3, v0, p2}, Lcom/intsig/office/fc/hwpf/usermodel/Range;-><init>(IILcom/intsig/office/fc/hwpf/usermodel/Range;)V

    .line 12
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Range;->_doc:Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;

    invoke-virtual {p2}, Lcom/intsig/office/fc/hwpf/HWPFDocumentCore;->getStyleSheet()Lcom/intsig/office/fc/hwpf/model/StyleSheet;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getParagraphProperties(Lcom/intsig/office/fc/hwpf/model/StyleSheet;)Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getSprmBuf()Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PAPX;->getIstd()S

    move-result p1

    iput-short p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_istd:S

    return-void
.end method

.method private getFrameTextFlow()S
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFVertical()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    int-to-short v0, v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFBackward()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    or-int/lit8 v0, v0, 0x2

    .line 22
    .line 23
    int-to-short v0, v0

    .line 24
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFRotateFont()Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_2

    .line 31
    .line 32
    or-int/lit8 v0, v0, 0x4

    .line 33
    .line 34
    int-to-short v0, v0

    .line 35
    :cond_2
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private setTableRowEnd(Z)V
    .locals 2

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFTtp(Z)V

    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    const/16 v1, 0x2417

    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;->clone()Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 14
    .line 15
    iput-object v1, v0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 16
    .line 17
    new-instance v1, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    invoke-direct {v1, v2}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 21
    .line 22
    .line 23
    iput-object v1, v0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public cloneProperties()Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    return-object v0

    .line 10
    :catch_0
    move-exception v0

    .line 11
    new-instance v1, Ljava/lang/RuntimeException;

    .line 12
    .line 13
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 14
    .line 15
    .line 16
    throw v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBarBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcBar()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBottomBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcBottom()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDropCap()Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDcs()Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFirstLineIndent()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft1()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFontAlignment()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getWAlignFont()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIlfo()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlfo()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIlvl()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getIlvl()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIndentFromLeft()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaLeft()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getIndentFromRight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDxaRight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getJustification()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getJc()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLeftBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcLeft()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLineSpacing()Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLspd()Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLvl()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getLvl()B

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRightBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcRight()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShading()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getShd()Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSpacingAfter()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaAfter()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSpacingBefore()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getDyaBefore()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getStyleIndex()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_istd:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTabClearPosition()S
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;->getTabClearPosition()S

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTableLevel()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getItap()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTopBorder()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getBrcTop()Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isAutoHyphenated()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNoAutoHyph()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    xor-int/lit8 v0, v0, 0x1

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isBackward()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFBackward()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isEmbeddedCellMark()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFInnerTableCell()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isInTable()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFInTable()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isKinsoku()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKinsoku()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isLineNotNumbered()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFNoLnn()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSideBySide()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFSideBySide()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isTableRowEnd()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTtp()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFTtpEmbedded()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 21
    :goto_1
    return v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isVertical()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->isFVertical()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isWidowControlled()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFWidowControl()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isWordWrapped()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFWordWrap()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public keepOnPage()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKeep()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public keepWithNext()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFKeepFollow()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public pageBreakBefore()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->getFPageBreakBefore()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setAutoHyphenated(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    xor-int/lit8 v1, p1, 0x1

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFNoAutoHyph(Z)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 9
    .line 10
    xor-int/lit8 p1, p1, 0x1

    .line 11
    .line 12
    const/16 v1, 0x242a

    .line 13
    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBackward(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFBackward(Z)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v0, 0x443a

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getFrameTextFlow()S

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBarBorder(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcBar(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x6629

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setBottomBorder(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcBottom(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x6426

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setDropCap(Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDcs(Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x442c

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/DropCapSpecifier;->toShort()S

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFirstLineIndent(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaLeft1(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, -0x7bef

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFontAlignment(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setWAlignFont(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x4439

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIndentFromLeft(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaLeft(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, -0x7bf1

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setIndentFromRight(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDxaRight(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, -0x7bf2

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setJustification(B)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setJc(B)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x2403

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SB)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setKeepOnPage(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFKeep(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x2405

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setKeepWithNext(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFKeepFollow(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x2406

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setKinsoku(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFKinsoku(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x2433

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLeftBorder(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcLeft(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x6425

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLineNotNumbered(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFNoLnn(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x240c

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setLineSpacing(Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setLspd(Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x6412

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/LineSpacingDescriptor;->toInt()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setPageBreakBefore(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFPageBreakBefore(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x2407

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRightBorder(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcRight(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x6427

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShading(Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setShd(Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x442d

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/ShadingDescriptor;->toShort()S

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSideBySide(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFSideBySide(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x2404

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSpacingAfter(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDyaAfter(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, -0x5bec

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setSpacingBefore(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setDyaBefore(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, -0x5bed

    .line 9
    .line 10
    int-to-short p1, p1

    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method setTableRowEnd(Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->setTableRowEnd(Z)V

    .line 2
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/TableSprmCompressor;->compressTableProperty(Lcom/intsig/office/fc/hwpf/usermodel/TableProperties;)[B

    move-result-object p1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->append([B)V

    return-void
.end method

.method public setTopBorder(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setBrcTop(Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x6424

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/usermodel/BorderCode;->toInt()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SI)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setVertical(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFVertical(Z)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v0, 0x443a

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->getFrameTextFlow()S

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {p1, v0, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SS)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWidowControl(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFWidowControl(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x2431

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setWordWrapped(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_props:Lcom/intsig/office/fc/hwpf/usermodel/ParagraphProperties;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/hwpf/model/types/PAPAbstractType;->setFWordWrap(Z)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/usermodel/Paragraph;->_papx:Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 7
    .line 8
    const/16 v1, 0x2434

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->updateSprm(SZ)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Paragraph ["

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getStartOffset()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, "; "

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/usermodel/Range;->getEndOffset()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v1, ")"

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public type()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

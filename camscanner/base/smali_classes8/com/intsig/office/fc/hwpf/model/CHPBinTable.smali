.class public Lcom/intsig/office/fc/hwpf/model/CHPBinTable;
.super Ljava/lang/Object;
.source "CHPBinTable.java"


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# instance fields
.field protected _textRuns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/fc/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>([B[BIIILcom/intsig/office/fc/hwpf/model/TextPieceTable;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p6

    .line 3
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;-><init>([B[BIILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    return-void
.end method

.method public constructor <init>([B[BIILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V
    .locals 6

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 6
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    const/4 v1, 0x4

    invoke-direct {v0, p2, p3, p4, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>([BIII)V

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->length()I

    move-result p2

    const/4 p3, 0x0

    const/4 p4, 0x0

    :goto_0
    if-ge p4, p2, :cond_2

    .line 8
    invoke-virtual {v0, p4}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->getProperty(I)Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    move-result-object v1

    .line 9
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/intsig/office/fc/util/LittleEndian;->getInt([B)I

    move-result v1

    mul-int/lit16 v1, v1, 0x200

    .line 10
    new-instance v2, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;

    invoke-direct {v2, p1, v1, p5}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;-><init>([BILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    .line 11
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/FormattedDiskPage;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_1

    .line 12
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->getCHPX(I)Lcom/intsig/office/fc/hwpf/model/CHPX;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 13
    iget-object v5, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static binarySearch(Ljava/util/List;I)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hwpf/model/CHPX;",
            ">;I)I"
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, -0x1

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-gt v1, v0, :cond_2

    .line 9
    .line 10
    add-int v2, v1, v0

    .line 11
    .line 12
    ushr-int/lit8 v2, v2, 0x1

    .line 13
    .line 14
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    check-cast v3, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 19
    .line 20
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    if-ge v3, p1, :cond_0

    .line 25
    .line 26
    add-int/lit8 v2, v2, 0x1

    .line 27
    .line 28
    move v1, v2

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    if-le v3, p1, :cond_1

    .line 31
    .line 32
    add-int/lit8 v2, v2, -0x1

    .line 33
    .line 34
    move v0, v2

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    return v2

    .line 37
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 38
    .line 39
    neg-int p0, v1

    .line 40
    return p0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public adjustForDelete(III)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int v1, p2, p3

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 16
    .line 17
    move v3, p1

    .line 18
    :goto_0
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-ge v2, v1, :cond_0

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 25
    .line 26
    add-int/lit8 v3, v3, 0x1

    .line 27
    .line 28
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    if-ne p1, v3, :cond_1

    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 38
    .line 39
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    sub-int/2addr v2, v1

    .line 50
    add-int/2addr v2, p2

    .line 51
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 52
    .line 53
    .line 54
    goto :goto_2

    .line 55
    :cond_1
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 56
    .line 57
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 62
    .line 63
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 64
    .line 65
    .line 66
    :goto_1
    add-int/lit8 p1, p1, 0x1

    .line 67
    .line 68
    if-ge p1, v3, :cond_2

    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 71
    .line 72
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    check-cast v2, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 77
    .line 78
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 82
    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 86
    .line 87
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    sub-int/2addr v2, v1

    .line 98
    add-int/2addr v2, p2

    .line 99
    invoke-virtual {p1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 100
    .line 101
    .line 102
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 103
    .line 104
    if-ge v3, v0, :cond_3

    .line 105
    .line 106
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 107
    .line 108
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    check-cast p1, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 113
    .line 114
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 115
    .line 116
    .line 117
    move-result p2

    .line 118
    sub-int/2addr p2, p3

    .line 119
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 123
    .line 124
    .line 125
    move-result p2

    .line 126
    sub-int/2addr p2, p3

    .line 127
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 128
    .line 129
    .line 130
    goto :goto_2

    .line 131
    :cond_3
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public adjustForInsert(II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    add-int/2addr v2, p2

    .line 20
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 21
    .line 22
    .line 23
    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 24
    .line 25
    if-ge p1, v0, :cond_0

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    add-int/2addr v2, p2

    .line 40
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    add-int/2addr v2, p2

    .line 48
    invoke-virtual {v1, v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getTextRuns()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/hwpf/model/CHPX;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public insert(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1, v1, p3}, Lcom/intsig/office/fc/hwpf/model/CHPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 11
    .line 12
    .line 13
    iget-object p3, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    .line 16
    .line 17
    .line 18
    move-result p3

    .line 19
    if-ne p1, p3, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    iget-object p3, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object p3

    .line 33
    check-cast p3, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 34
    .line 35
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    if-ge v2, p2, :cond_1

    .line 40
    .line 41
    new-instance v2, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 42
    .line 43
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/CHPX;->getSprmBuf()Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    invoke-direct {v2, v1, v1, v3}, Lcom/intsig/office/fc/hwpf/model/CHPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setStart(I)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    invoke-virtual {v2, v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p3, p2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 61
    .line 62
    .line 63
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 64
    .line 65
    add-int/lit8 p3, p1, 0x1

    .line 66
    .line 67
    invoke-virtual {p2, p3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 71
    .line 72
    add-int/lit8 p1, p1, 0x2

    .line 73
    .line 74
    invoke-virtual {p2, p1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    iget-object p2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 79
    .line 80
    invoke-virtual {p2, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 81
    .line 82
    .line 83
    :goto_0
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public rebuild(Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;)V
    .locals 12

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz p1, :cond_5

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->getGrpprls()[Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/ComplexFileTable;->getTextPieceTable()Lcom/intsig/office/fc/hwpf/model/TextPieceTable;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/TextPieceTable;->getTextPieces()Ljava/util/List;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    if-eqz v3, :cond_5

    .line 29
    .line 30
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    check-cast v3, Lcom/intsig/office/fc/hwpf/model/TextPiece;

    .line 35
    .line 36
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/TextPiece;->getPieceDescriptor()Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PieceDescriptor;->getPrm()Lcom/intsig/office/fc/hwpf/model/PropertyModifier;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PropertyModifier;->isComplex()Z

    .line 45
    .line 46
    .line 47
    move-result v5

    .line 48
    if-nez v5, :cond_1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PropertyModifier;->getIgrpprl()S

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    if-ltz v4, :cond_0

    .line 56
    .line 57
    array-length v5, v2

    .line 58
    if-lt v4, v5, :cond_2

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    aget-object v4, v2, v4

    .line 62
    .line 63
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->iterator()Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    .line 64
    .line 65
    .line 66
    move-result-object v5

    .line 67
    :cond_3
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    .line 68
    .line 69
    .line 70
    move-result v6

    .line 71
    if-eqz v6, :cond_4

    .line 72
    .line 73
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 74
    .line 75
    .line 76
    move-result-object v6

    .line 77
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    .line 78
    .line 79
    .line 80
    move-result v6

    .line 81
    const/4 v7, 0x2

    .line 82
    if-ne v6, v7, :cond_3

    .line 83
    .line 84
    const/4 v5, 0x1

    .line 85
    goto :goto_1

    .line 86
    :cond_4
    const/4 v5, 0x0

    .line 87
    :goto_1
    if-eqz v5, :cond_0

    .line 88
    .line 89
    :try_start_0
    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->clone()Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    move-result-object v4

    .line 93
    check-cast v4, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .line 95
    new-instance v5, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 96
    .line 97
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 98
    .line 99
    .line 100
    move-result v6

    .line 101
    invoke-virtual {v3}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 102
    .line 103
    .line 104
    move-result v3

    .line 105
    invoke-direct {v5, v6, v3, v4}, Lcom/intsig/office/fc/hwpf/model/CHPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 106
    .line 107
    .line 108
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 109
    .line 110
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    .line 112
    .line 113
    goto :goto_0

    .line 114
    :catch_0
    move-exception p1

    .line 115
    new-instance v0, Ljava/lang/Error;

    .line 116
    .line 117
    invoke-direct {v0, p1}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    .line 118
    .line 119
    .line 120
    throw v0

    .line 121
    :cond_5
    new-instance p1, Ljava/util/ArrayList;

    .line 122
    .line 123
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 124
    .line 125
    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 126
    .line 127
    .line 128
    sget-object v2, Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;->instance:Lcom/intsig/office/fc/hwpf/model/PropertyNode$StartComparator;

    .line 129
    .line 130
    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 131
    .line 132
    .line 133
    new-instance v2, Ljava/util/IdentityHashMap;

    .line 134
    .line 135
    invoke-direct {v2}, Ljava/util/IdentityHashMap;-><init>()V

    .line 136
    .line 137
    .line 138
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 139
    .line 140
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 141
    .line 142
    .line 143
    move-result-object v3

    .line 144
    const/4 v4, 0x0

    .line 145
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 146
    .line 147
    .line 148
    move-result v5

    .line 149
    if-eqz v5, :cond_6

    .line 150
    .line 151
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 152
    .line 153
    .line 154
    move-result-object v5

    .line 155
    check-cast v5, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 156
    .line 157
    add-int/lit8 v6, v4, 0x1

    .line 158
    .line 159
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 160
    .line 161
    .line 162
    move-result-object v4

    .line 163
    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    .line 165
    .line 166
    move v4, v6

    .line 167
    goto :goto_2

    .line 168
    :cond_6
    new-instance v3, Lcom/intsig/office/fc/hwpf/model/CHPBinTable$1;

    .line 169
    .line 170
    invoke-direct {v3, p0, v2}, Lcom/intsig/office/fc/hwpf/model/CHPBinTable$1;-><init>(Lcom/intsig/office/fc/hwpf/model/CHPBinTable;Ljava/util/Map;)V

    .line 171
    .line 172
    .line 173
    new-instance v2, Ljava/util/HashSet;

    .line 174
    .line 175
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 176
    .line 177
    .line 178
    iget-object v4, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 179
    .line 180
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 181
    .line 182
    .line 183
    move-result-object v4

    .line 184
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 185
    .line 186
    .line 187
    move-result v5

    .line 188
    if-eqz v5, :cond_7

    .line 189
    .line 190
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 191
    .line 192
    .line 193
    move-result-object v5

    .line 194
    check-cast v5, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 195
    .line 196
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 197
    .line 198
    .line 199
    move-result v6

    .line 200
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 201
    .line 202
    .line 203
    move-result-object v6

    .line 204
    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    .line 206
    .line 207
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 208
    .line 209
    .line 210
    move-result v5

    .line 211
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 212
    .line 213
    .line 214
    move-result-object v5

    .line 215
    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216
    .line 217
    .line 218
    goto :goto_3

    .line 219
    :cond_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 220
    .line 221
    .line 222
    move-result-object v4

    .line 223
    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 224
    .line 225
    .line 226
    new-instance v4, Ljava/util/ArrayList;

    .line 227
    .line 228
    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 229
    .line 230
    .line 231
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 232
    .line 233
    .line 234
    new-instance v2, Ljava/util/LinkedList;

    .line 235
    .line 236
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 237
    .line 238
    .line 239
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 240
    .line 241
    .line 242
    move-result-object v4

    .line 243
    const/4 v5, 0x0

    .line 244
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 245
    .line 246
    .line 247
    move-result v6

    .line 248
    if-eqz v6, :cond_10

    .line 249
    .line 250
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 251
    .line 252
    .line 253
    move-result-object v6

    .line 254
    check-cast v6, Ljava/lang/Integer;

    .line 255
    .line 256
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    .line 257
    .line 258
    .line 259
    move-result v6

    .line 260
    invoke-static {p1, v6}, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->binarySearch(Ljava/util/List;I)I

    .line 261
    .line 262
    .line 263
    move-result v7

    .line 264
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    .line 265
    .line 266
    .line 267
    move-result v7

    .line 268
    :goto_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 269
    .line 270
    .line 271
    move-result v8

    .line 272
    if-lt v7, v8, :cond_8

    .line 273
    .line 274
    add-int/lit8 v7, v7, -0x1

    .line 275
    .line 276
    goto :goto_5

    .line 277
    :cond_8
    :goto_6
    if-lez v7, :cond_9

    .line 278
    .line 279
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 280
    .line 281
    .line 282
    move-result-object v8

    .line 283
    check-cast v8, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 284
    .line 285
    invoke-virtual {v8}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 286
    .line 287
    .line 288
    move-result v8

    .line 289
    if-lt v8, v6, :cond_9

    .line 290
    .line 291
    add-int/lit8 v7, v7, -0x1

    .line 292
    .line 293
    goto :goto_6

    .line 294
    :cond_9
    new-instance v8, Ljava/util/LinkedList;

    .line 295
    .line 296
    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 297
    .line 298
    .line 299
    :goto_7
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 300
    .line 301
    .line 302
    move-result v9

    .line 303
    if-ge v7, v9, :cond_c

    .line 304
    .line 305
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 306
    .line 307
    .line 308
    move-result-object v9

    .line 309
    check-cast v9, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 310
    .line 311
    invoke-virtual {v9}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 312
    .line 313
    .line 314
    move-result v10

    .line 315
    if-ge v6, v10, :cond_a

    .line 316
    .line 317
    goto :goto_8

    .line 318
    :cond_a
    invoke-virtual {v9}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 319
    .line 320
    .line 321
    move-result v10

    .line 322
    invoke-static {v5, v10}, Ljava/lang/Math;->max(II)I

    .line 323
    .line 324
    .line 325
    move-result v10

    .line 326
    invoke-virtual {v9}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 327
    .line 328
    .line 329
    move-result v11

    .line 330
    invoke-static {v6, v11}, Ljava/lang/Math;->min(II)I

    .line 331
    .line 332
    .line 333
    move-result v11

    .line 334
    if-ge v10, v11, :cond_b

    .line 335
    .line 336
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    .line 338
    .line 339
    :cond_b
    add-int/lit8 v7, v7, 0x1

    .line 340
    .line 341
    goto :goto_7

    .line 342
    :cond_c
    :goto_8
    invoke-interface {v8}, Ljava/util/List;->size()I

    .line 343
    .line 344
    .line 345
    move-result v7

    .line 346
    if-nez v7, :cond_d

    .line 347
    .line 348
    new-instance v7, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 349
    .line 350
    new-instance v8, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 351
    .line 352
    invoke-direct {v8, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 353
    .line 354
    .line 355
    invoke-direct {v7, v5, v6, v8}, Lcom/intsig/office/fc/hwpf/model/CHPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 356
    .line 357
    .line 358
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    .line 360
    .line 361
    goto :goto_a

    .line 362
    :cond_d
    invoke-interface {v8}, Ljava/util/List;->size()I

    .line 363
    .line 364
    .line 365
    move-result v7

    .line 366
    if-ne v7, v0, :cond_e

    .line 367
    .line 368
    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 369
    .line 370
    .line 371
    move-result-object v7

    .line 372
    check-cast v7, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 373
    .line 374
    invoke-virtual {v7}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 375
    .line 376
    .line 377
    move-result v9

    .line 378
    if-ne v9, v5, :cond_e

    .line 379
    .line 380
    invoke-virtual {v7}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 381
    .line 382
    .line 383
    move-result v9

    .line 384
    if-ne v9, v6, :cond_e

    .line 385
    .line 386
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    .line 388
    .line 389
    goto :goto_a

    .line 390
    :cond_e
    invoke-static {v8, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 391
    .line 392
    .line 393
    new-instance v7, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 394
    .line 395
    invoke-direct {v7, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>(I)V

    .line 396
    .line 397
    .line 398
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 399
    .line 400
    .line 401
    move-result-object v8

    .line 402
    :goto_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    .line 403
    .line 404
    .line 405
    move-result v9

    .line 406
    if-eqz v9, :cond_f

    .line 407
    .line 408
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 409
    .line 410
    .line 411
    move-result-object v9

    .line 412
    check-cast v9, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 413
    .line 414
    invoke-virtual {v9}, Lcom/intsig/office/fc/hwpf/model/CHPX;->getGrpprl()[B

    .line 415
    .line 416
    .line 417
    move-result-object v9

    .line 418
    invoke-virtual {v7, v9, v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->append([BI)V

    .line 419
    .line 420
    .line 421
    goto :goto_9

    .line 422
    :cond_f
    new-instance v8, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 423
    .line 424
    invoke-direct {v8, v5, v6, v7}, Lcom/intsig/office/fc/hwpf/model/CHPX;-><init>(IILcom/intsig/office/fc/hwpf/sprm/SprmBuffer;)V

    .line 425
    .line 426
    .line 427
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 428
    .line 429
    .line 430
    :goto_a
    move v5, v6

    .line 431
    goto/16 :goto_4

    .line 432
    .line 433
    :cond_10
    new-instance p1, Ljava/util/ArrayList;

    .line 434
    .line 435
    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 436
    .line 437
    .line 438
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    .line 439
    .line 440
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 441
    .line 442
    .line 443
    move-result-object p1

    .line 444
    const/4 v0, 0x0

    .line 445
    :goto_b
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 446
    .line 447
    .line 448
    move-result v1

    .line 449
    if-eqz v1, :cond_13

    .line 450
    .line 451
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 452
    .line 453
    .line 454
    move-result-object v1

    .line 455
    check-cast v1, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 456
    .line 457
    if-nez v0, :cond_11

    .line 458
    .line 459
    goto :goto_c

    .line 460
    :cond_11
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 461
    .line 462
    .line 463
    move-result v2

    .line 464
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    .line 465
    .line 466
    .line 467
    move-result v3

    .line 468
    if-ne v2, v3, :cond_12

    .line 469
    .line 470
    invoke-virtual {v0}, Lcom/intsig/office/fc/hwpf/model/CHPX;->getGrpprl()[B

    .line 471
    .line 472
    .line 473
    move-result-object v2

    .line 474
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/CHPX;->getGrpprl()[B

    .line 475
    .line 476
    .line 477
    move-result-object v3

    .line 478
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    .line 479
    .line 480
    .line 481
    move-result v2

    .line 482
    if-eqz v2, :cond_12

    .line 483
    .line 484
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    .line 485
    .line 486
    .line 487
    move-result v1

    .line 488
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->setEnd(I)V

    .line 489
    .line 490
    .line 491
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    .line 492
    .line 493
    .line 494
    goto :goto_b

    .line 495
    :cond_12
    :goto_c
    move-object v0, v1

    .line 496
    goto :goto_b

    .line 497
    :cond_13
    return-void
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;ILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "WordDocument"

    .line 1
    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;

    move-result-object v0

    const-string v1, "1Table"

    .line 2
    invoke-virtual {p1, v1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFFileSystem;->getStream(Ljava/lang/String;)Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;

    move-result-object p1

    .line 3
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;ILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V

    return-void
.end method

.method public writeTo(Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;ILcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    new-instance p3, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;

    const/4 v0, 0x4

    invoke-direct {p3, v0}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;-><init>(I)V

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v1

    .line 6
    rem-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_0

    rsub-int v1, v1, 0x200

    .line 7
    new-array v1, v1, [B

    .line 8
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 9
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/model/io/HWPFOutputStream;->getOffset()I

    move-result v1

    .line 10
    div-int/lit16 v1, v1, 0x200

    .line 11
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/hwpf/model/CHPX;

    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getEnd()I

    move-result v2

    invoke-interface {p4, v2}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v2

    .line 12
    iget-object v3, p0, Lcom/intsig/office/fc/hwpf/model/CHPBinTable;->_textRuns:Ljava/util/ArrayList;

    :goto_0
    const/4 v4, 0x0

    .line 13
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/intsig/office/fc/hwpf/model/CHPX;

    .line 14
    invoke-virtual {v5}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v5

    invoke-interface {p4, v5}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v5

    .line 15
    new-instance v6, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;

    invoke-direct {v6}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;-><init>()V

    .line 16
    invoke-virtual {v6, v3}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->fill(Ljava/util/List;)V

    .line 17
    invoke-virtual {v6, p4}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->toByteArray(Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;)[B

    move-result-object v3

    .line 18
    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    .line 19
    invoke-virtual {v6}, Lcom/intsig/office/fc/hwpf/model/CHPFormattedDiskPage;->getOverflow()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 20
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/hwpf/model/CHPX;

    invoke-virtual {v4}, Lcom/intsig/office/fc/hwpf/model/PropertyNode;->getStart()I

    move-result v4

    invoke-interface {p4, v4}, Lcom/intsig/office/fc/hwpf/model/CharIndexTranslator;->getByteIndex(I)I

    move-result v4

    goto :goto_1

    :cond_1
    move v4, v2

    :goto_1
    new-array v6, v0, [B

    add-int/lit8 v7, v1, 0x1

    .line 21
    invoke-static {v6, v1}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BI)V

    .line 22
    new-instance v1, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;

    invoke-direct {v1, v5, v4, v6}, Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;-><init>(II[B)V

    invoke-virtual {p3, v1}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->addProperty(Lcom/intsig/office/fc/hwpf/model/GenericPropertyNode;)V

    if-nez v3, :cond_2

    .line 23
    invoke-virtual {p3}, Lcom/intsig/office/fc/hwpf/model/PlexOfCps;->toByteArray()[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void

    :cond_2
    move v1, v7

    goto :goto_0
.end method

.class public final Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;
.super Ljava/lang/Object;
.source "SprmBuffer.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Lcom/intsig/office/fc/util/Internal;
.end annotation


# instance fields
.field _buf:[B

.field _istd:Z

.field _offset:I

.field private final _sprmsStartOffset:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    add-int/lit8 v0, p1, 0x4

    .line 11
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 12
    iput p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    .line 13
    iput p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_sprmsStartOffset:I

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BI)V

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1

    const/4 v0, 0x0

    .line 9
    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BZI)V

    return-void
.end method

.method public constructor <init>([BZ)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    .line 3
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;-><init>([BZI)V

    return-void
.end method

.method public constructor <init>([BZI)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    array-length v0, p1

    iput v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    .line 6
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 7
    iput-boolean p2, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_istd:Z

    .line 8
    iput p3, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_sprmsStartOffset:I

    return-void
.end method

.method private ensureCapacity(I)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    .line 2
    .line 3
    add-int v1, v0, p1

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 6
    .line 7
    array-length v3, v2

    .line 8
    if-lt v1, v3, :cond_0

    .line 9
    .line 10
    add-int/2addr v0, p1

    .line 11
    new-array p1, v0, [B

    .line 12
    .line 13
    array-length v0, v2

    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-static {v2, v1, p1, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 16
    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private findSprmOffset(S)I
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->findSprm(S)Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    const/4 p1, -0x1

    .line 8
    return p1

    .line 9
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getGrpprlOffset()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public addSprm(SB)V
    .locals 2

    const/4 v0, 0x3

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 3
    iget p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 p1, p1, 0x2

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    add-int/lit8 v1, p1, 0x1

    iput v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    aput-byte p2, v0, p1

    return-void
.end method

.method public addSprm(SI)V
    .locals 2

    const/4 v0, 0x6

    .line 11
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 12
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 13
    iget p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    .line 14
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {v0, p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    .line 15
    iget p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 p1, p1, 0x4

    iput p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    return-void
.end method

.method public addSprm(SS)V
    .locals 2

    const/4 v0, 0x4

    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 17
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 18
    iget p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    .line 19
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {v0, p1, p2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 20
    iget p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    return-void
.end method

.method public addSprm(S[B)V
    .locals 3

    .line 5
    array-length v0, p2

    add-int/lit8 v0, v0, 0x3

    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 7
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    invoke-static {v0, v1, p1}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    .line 8
    iget p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    add-int/lit8 p1, p1, 0x2

    .line 9
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    add-int/lit8 v1, p1, 0x1

    iput v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v2, p2

    int-to-byte v2, v2

    aput-byte v2, v0, p1

    const/4 p1, 0x0

    .line 10
    array-length v2, p2

    invoke-static {p2, p1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public append([B)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->append([BI)V

    return-void
.end method

.method public append([BI)V
    .locals 3

    .line 2
    array-length v0, p1

    sub-int/2addr v0, p2

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->ensureCapacity(I)V

    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    iget v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    array-length v2, p1

    sub-int/2addr v2, p2

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4
    iget v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    array-length p1, p1

    sub-int/2addr p1, p2

    add-int/2addr v0, p1

    iput v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_offset:I

    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 8
    .line 9
    array-length v1, v1

    .line 10
    new-array v1, v1, [B

    .line 11
    .line 12
    iput-object v1, v0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 15
    .line 16
    const/4 v3, 0x0

    .line 17
    array-length v4, v2

    .line 18
    invoke-static {v2, v3, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 19
    .line 20
    .line 21
    return-object v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    check-cast p1, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 4
    .line 5
    iget-object p1, p1, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 6
    .line 7
    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public findSprm(S)Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;
    .locals 4

    .line 1
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperationFromOpcode(S)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getTypeFromOpcode(S)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    new-instance v1, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 12
    .line 13
    const/4 v3, 0x2

    .line 14
    invoke-direct {v1, v2, v3}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 15
    .line 16
    .line 17
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getOperation()I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    if-ne v3, v0, :cond_0

    .line 32
    .line 33
    invoke-virtual {v2}, Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;->getType()I

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    if-ne v3, p1, :cond_0

    .line 38
    .line 39
    return-object v2

    .line 40
    :cond_1
    const/4 p1, 0x0

    .line 41
    return-object p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public iterator()Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_sprmsStartOffset:I

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;-><init>([BI)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toByteArray()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Sprms ("

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    .line 12
    .line 13
    array-length v1, v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, " byte(s)): "

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->iterator()Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_0

    .line 31
    .line 32
    :try_start_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hwpf/sprm/SprmIterator;->next()Lcom/intsig/office/fc/hwpf/sprm/SprmOperation;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    .line 38
    .line 39
    goto :goto_1

    .line 40
    :catch_0
    const-string v2, "error"

    .line 41
    .line 42
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    :goto_1
    const-string v2, "; "

    .line 46
    .line 47
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public updateSprm(SB)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->findSprmOffset(S)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    aput-byte p2, p1, v0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->addSprm(SB)V

    return-void
.end method

.method public updateSprm(SI)V
    .locals 2

    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->findSprmOffset(S)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 8
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {p1, v0, p2}, Lcom/intsig/office/fc/util/LittleEndian;->putInt([BII)V

    return-void

    .line 9
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->addSprm(SI)V

    return-void
.end method

.method public updateSprm(SS)V
    .locals 2

    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->findSprmOffset(S)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 11
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    invoke-static {p1, v0, p2}, Lcom/intsig/office/fc/util/LittleEndian;->putShort([BIS)V

    return-void

    .line 12
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->addSprm(SS)V

    return-void
.end method

.method public updateSprm(SZ)V
    .locals 2

    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->findSprmOffset(S)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 5
    iget-object p1, p0, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->_buf:[B

    int-to-byte p2, p2

    aput-byte p2, p1, v0

    return-void

    .line 6
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/fc/hwpf/sprm/SprmBuffer;->addSprm(SI)V

    return-void
.end method

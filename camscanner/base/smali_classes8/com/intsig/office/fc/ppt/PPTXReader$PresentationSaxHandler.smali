.class Lcom/intsig/office/fc/ppt/PPTXReader$PresentationSaxHandler;
.super Ljava/lang/Object;
.source "PPTXReader.java"

# interfaces
.implements Lcom/intsig/office/fc/dom4j/ElementHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/ppt/PPTXReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PresentationSaxHandler"
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/office/fc/ppt/PPTXReader;


# direct methods
.method constructor <init>(Lcom/intsig/office/fc/ppt/PPTXReader;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/ppt/PPTXReader$PresentationSaxHandler;->〇080:Lcom/intsig/office/fc/ppt/PPTXReader;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public onEnd(Lcom/intsig/office/fc/dom4j/ElementPath;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTXReader$PresentationSaxHandler;->〇080:Lcom/intsig/office/fc/ppt/PPTXReader;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/office/fc/ppt/PPTXReader;->access$000(Lcom/intsig/office/fc/ppt/PPTXReader;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_4

    .line 8
    .line 9
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/ElementPath;->getCurrent()Lcom/intsig/office/fc/dom4j/Element;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :try_start_0
    const-string v1, "sldMasterIdLst"

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTXReader$PresentationSaxHandler;->〇080:Lcom/intsig/office/fc/ppt/PPTXReader;

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ppt/PPTXReader;->processMasterPart(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const-string v1, "defaultTextStyle"

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-eqz v1, :cond_1

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTXReader$PresentationSaxHandler;->〇080:Lcom/intsig/office/fc/ppt/PPTXReader;

    .line 40
    .line 41
    invoke-static {v0, p1}, Lcom/intsig/office/fc/ppt/PPTXReader;->〇〇808〇(Lcom/intsig/office/fc/ppt/PPTXReader;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    const-string v1, "sldSz"

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    if-eqz v1, :cond_2

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTXReader$PresentationSaxHandler;->〇080:Lcom/intsig/office/fc/ppt/PPTXReader;

    .line 54
    .line 55
    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ppt/PPTXReader;->setPageSize(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_2
    const-string v1, "sldId"

    .line 60
    .line 61
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-eqz v0, :cond_3

    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTXReader$PresentationSaxHandler;->〇080:Lcom/intsig/office/fc/ppt/PPTXReader;

    .line 68
    .line 69
    invoke-static {v0, p1}, Lcom/intsig/office/fc/ppt/PPTXReader;->OO0o〇〇(Lcom/intsig/office/fc/ppt/PPTXReader;Lcom/intsig/office/fc/dom4j/Element;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 75
    .line 76
    .line 77
    :cond_3
    :goto_0
    invoke-interface {p1}, Lcom/intsig/office/fc/dom4j/Node;->detach()Lcom/intsig/office/fc/dom4j/Node;

    .line 78
    .line 79
    .line 80
    return-void

    .line 81
    :cond_4
    new-instance p1, Lcom/intsig/office/system/AbortReaderError;

    .line 82
    .line 83
    const-string v0, "abort Reader"

    .line 84
    .line 85
    invoke-direct {p1, v0}, Lcom/intsig/office/system/AbortReaderError;-><init>(Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    throw p1
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public onStart(Lcom/intsig/office/fc/dom4j/ElementPath;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

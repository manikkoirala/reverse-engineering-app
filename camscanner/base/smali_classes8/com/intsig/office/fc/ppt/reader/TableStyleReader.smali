.class public Lcom/intsig/office/fc/ppt/reader/TableStyleReader;
.super Ljava/lang/Object;
.source "TableStyleReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/ppt/reader/TableStyleReader$TableStyleSaxHandler;
    }
.end annotation


# static fields
.field private static tableStyleReader:Lcom/intsig/office/fc/ppt/reader/TableStyleReader;


# instance fields
.field private defaultFontSize:I

.field private pgModel:Lcom/intsig/office/pg/model/PGModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->tableStyleReader:Lcom/intsig/office/fc/ppt/reader/TableStyleReader;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->pgModel:Lcom/intsig/office/pg/model/PGModel;

    .line 6
    .line 7
    const/16 v0, 0xc

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->defaultFontSize:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getTableCellBorders(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "left"

    .line 7
    .line 8
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const-string v2, "ln"

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;->setLeftBorder(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    const-string v1, "right"

    .line 24
    .line 25
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;->setRightBorder(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 36
    .line 37
    .line 38
    :cond_1
    const-string v1, "top"

    .line 39
    .line 40
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    if-eqz v1, :cond_2

    .line 45
    .line 46
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;->setTopBorder(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 51
    .line 52
    .line 53
    :cond_2
    const-string v1, "bottom"

    .line 54
    .line 55
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    if-eqz p1, :cond_3

    .line 60
    .line 61
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;->setBottomBorder(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 66
    .line 67
    .line 68
    :cond_3
    return-object v0
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static instance()Lcom/intsig/office/fc/ppt/reader/TableStyleReader;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->tableStyleReader:Lcom/intsig/office/fc/ppt/reader/TableStyleReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "tcTxStyle"

    .line 7
    .line 8
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-eqz v1, :cond_2

    .line 13
    .line 14
    new-instance v2, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 15
    .line 16
    invoke-direct {v2}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v3, "b"

    .line 20
    .line 21
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    const-string v4, "on"

    .line 26
    .line 27
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    const/4 v5, 0x1

    .line 32
    if-eqz v3, :cond_0

    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    invoke-virtual {v3, v2, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontBold(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 39
    .line 40
    .line 41
    :cond_0
    const-string v3, "i"

    .line 42
    .line 43
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    if-eqz v1, :cond_1

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-virtual {v1, v2, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontItalic(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 58
    .line 59
    .line 60
    :cond_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    iget v3, p0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->defaultFontSize:I

    .line 65
    .line 66
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->setFontAttributeSet(Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 70
    .line 71
    .line 72
    :cond_2
    const-string v1, "tcStyle"

    .line 73
    .line 74
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    const-string v1, "tcBdr"

    .line 79
    .line 80
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    if-eqz v1, :cond_3

    .line 85
    .line 86
    invoke-direct {p0, v1}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->getTableCellBorders(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->setTableCellBorders(Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;)V

    .line 91
    .line 92
    .line 93
    :cond_3
    const-string v1, "fill"

    .line 94
    .line 95
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->setTableCellBgFill(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 100
    .line 101
    .line 102
    return-object v0
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private processTableStyle(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/pg/model/tableStyle/TableStyle;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "styleId"

    .line 7
    .line 8
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const-string v2, "wholeTbl"

    .line 13
    .line 14
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    if-eqz v2, :cond_0

    .line 19
    .line 20
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setWholeTable(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    const-string v2, "band1H"

    .line 28
    .line 29
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    if-eqz v2, :cond_1

    .line 34
    .line 35
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setBand1H(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 40
    .line 41
    .line 42
    :cond_1
    const-string v2, "band2H"

    .line 43
    .line 44
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    if-eqz v2, :cond_2

    .line 49
    .line 50
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setBand2H(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 55
    .line 56
    .line 57
    :cond_2
    const-string v2, "band1V"

    .line 58
    .line 59
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    if-eqz v2, :cond_3

    .line 64
    .line 65
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setBand1V(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 70
    .line 71
    .line 72
    :cond_3
    const-string v2, "band2V"

    .line 73
    .line 74
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    if-eqz v2, :cond_4

    .line 79
    .line 80
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setBand2V(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 85
    .line 86
    .line 87
    :cond_4
    const-string v2, "lastCol"

    .line 88
    .line 89
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    if-eqz v2, :cond_5

    .line 94
    .line 95
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setLastCol(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 100
    .line 101
    .line 102
    :cond_5
    const-string v2, "firstCol"

    .line 103
    .line 104
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 105
    .line 106
    .line 107
    move-result-object v2

    .line 108
    if-eqz v2, :cond_6

    .line 109
    .line 110
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setFirstCol(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 115
    .line 116
    .line 117
    :cond_6
    const-string v2, "lastRow"

    .line 118
    .line 119
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    if-eqz v2, :cond_7

    .line 124
    .line 125
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 126
    .line 127
    .line 128
    move-result-object v2

    .line 129
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setLastRow(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 130
    .line 131
    .line 132
    :cond_7
    const-string v2, "firstRow"

    .line 133
    .line 134
    invoke-interface {p1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    if-eqz p1, :cond_8

    .line 139
    .line 140
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableCellStyle(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;

    .line 141
    .line 142
    .line 143
    move-result-object p1

    .line 144
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/model/tableStyle/TableStyle;->setFirstRow(Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;)V

    .line 145
    .line 146
    .line 147
    :cond_8
    iget-object p1, p0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->pgModel:Lcom/intsig/office/pg/model/PGModel;

    .line 148
    .line 149
    invoke-virtual {p1, v1, v0}, Lcom/intsig/office/pg/model/PGModel;->putTableStyle(Ljava/lang/String;Lcom/intsig/office/pg/model/tableStyle/TableStyle;)V

    .line 150
    .line 151
    .line 152
    return-void
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/fc/ppt/reader/TableStyleReader;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->processTableStyle(Lcom/intsig/office/fc/dom4j/Element;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public read(Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->pgModel:Lcom/intsig/office/pg/model/PGModel;

    .line 2
    .line 3
    iput p3, p0, Lcom/intsig/office/fc/ppt/reader/TableStyleReader;->defaultFontSize:I

    .line 4
    .line 5
    new-instance p1, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 6
    .line 7
    invoke-direct {p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 8
    .line 9
    .line 10
    :try_start_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 11
    .line 12
    .line 13
    move-result-object p2

    .line 14
    new-instance p3, Lcom/intsig/office/fc/ppt/reader/TableStyleReader$TableStyleSaxHandler;

    .line 15
    .line 16
    invoke-direct {p3, p0}, Lcom/intsig/office/fc/ppt/reader/TableStyleReader$TableStyleSaxHandler;-><init>(Lcom/intsig/office/fc/ppt/reader/TableStyleReader;)V

    .line 17
    .line 18
    .line 19
    const-string v0, "/tblStyleLst/tblStyle"

    .line 20
    .line 21
    invoke-virtual {p1, v0, p3}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->addHandler(Ljava/lang/String;Lcom/intsig/office/fc/dom4j/ElementHandler;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, p2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :catchall_0
    move-exception p2

    .line 35
    goto :goto_0

    .line 36
    :catch_0
    move-exception p2

    .line 37
    :try_start_1
    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->resetHandlers()V

    .line 39
    .line 40
    .line 41
    throw p2
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.class public Lcom/intsig/office/fc/ppt/reader/SmartArtReader;
.super Ljava/lang/Object;
.source "SmartArtReader.java"


# static fields
.field private static reader:Lcom/intsig/office/fc/ppt/reader/SmartArtReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;->reader:Lcom/intsig/office/fc/ppt/reader/SmartArtReader;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getBackgrouond(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;I)Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    const-string p4, "useBgFill"

    .line 2
    .line 3
    invoke-interface {p8, p4}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-interface {p8, p4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object p4

    .line 13
    if-eqz p4, :cond_1

    .line 14
    .line 15
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-lez v0, :cond_1

    .line 20
    .line 21
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 22
    .line 23
    .line 24
    move-result p4

    .line 25
    if-lez p4, :cond_1

    .line 26
    .line 27
    invoke-virtual {p7}, Lcom/intsig/office/pg/model/PGSlide;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 28
    .line 29
    .line 30
    move-result-object p4

    .line 31
    if-nez p4, :cond_2

    .line 32
    .line 33
    if-eqz p6, :cond_0

    .line 34
    .line 35
    invoke-virtual {p6}, Lcom/intsig/office/pg/model/PGLayout;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 36
    .line 37
    .line 38
    move-result-object p4

    .line 39
    :cond_0
    if-nez p4, :cond_2

    .line 40
    .line 41
    if-eqz p5, :cond_2

    .line 42
    .line 43
    invoke-virtual {p5}, Lcom/intsig/office/pg/model/PGMaster;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 44
    .line 45
    .line 46
    move-result-object p4

    .line 47
    goto :goto_0

    .line 48
    :cond_1
    const/4 p4, 0x0

    .line 49
    :cond_2
    :goto_0
    const-string p6, "spPr"

    .line 50
    .line 51
    invoke-interface {p8, p6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 52
    .line 53
    .line 54
    move-result-object v5

    .line 55
    invoke-interface {p8}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p6

    .line 59
    if-nez p4, :cond_3

    .line 60
    .line 61
    const-string p7, "noFill"

    .line 62
    .line 63
    invoke-interface {v5, p7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 64
    .line 65
    .line 66
    move-result-object p7

    .line 67
    if-nez p7, :cond_3

    .line 68
    .line 69
    const-string p7, "cxnSp"

    .line 70
    .line 71
    invoke-virtual {p6, p7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    move-result p6

    .line 75
    if-nez p6, :cond_3

    .line 76
    .line 77
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->instance()Lcom/intsig/office/fc/ppt/reader/BackgroundReader;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    move-object v1, p1

    .line 82
    move-object v2, p2

    .line 83
    move-object v3, p3

    .line 84
    move-object v4, p5

    .line 85
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 86
    .line 87
    .line 88
    move-result-object p4

    .line 89
    if-nez p4, :cond_3

    .line 90
    .line 91
    const/16 p6, 0x13

    .line 92
    .line 93
    if-eq p9, p6, :cond_3

    .line 94
    .line 95
    const/16 p6, 0xb9

    .line 96
    .line 97
    if-eq p9, p6, :cond_3

    .line 98
    .line 99
    const/16 p6, 0x55

    .line 100
    .line 101
    if-eq p9, p6, :cond_3

    .line 102
    .line 103
    const/16 p6, 0x56

    .line 104
    .line 105
    if-eq p9, p6, :cond_3

    .line 106
    .line 107
    const/16 p6, 0xba

    .line 108
    .line 109
    if-eq p9, p6, :cond_3

    .line 110
    .line 111
    const/16 p6, 0x57

    .line 112
    .line 113
    if-eq p9, p6, :cond_3

    .line 114
    .line 115
    const/16 p6, 0x58

    .line 116
    .line 117
    if-eq p9, p6, :cond_3

    .line 118
    .line 119
    const/16 p6, 0xe9

    .line 120
    .line 121
    if-eq p9, p6, :cond_3

    .line 122
    .line 123
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->instance()Lcom/intsig/office/fc/ppt/reader/BackgroundReader;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    const-string p4, "style"

    .line 128
    .line 129
    invoke-interface {p8, p4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 130
    .line 131
    .line 132
    move-result-object v5

    .line 133
    move-object v1, p1

    .line 134
    move-object v2, p2

    .line 135
    move-object v3, p3

    .line 136
    move-object v4, p5

    .line 137
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 138
    .line 139
    .line 140
    move-result-object p4

    .line 141
    :cond_3
    return-object p4
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method private getTextBoxData(Lcom/intsig/office/system/IControl;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/shape/IShape;
    .locals 23

    .line 1
    move-object/from16 v2, p2

    .line 2
    .line 3
    move-object/from16 v3, p3

    .line 4
    .line 5
    move-object/from16 v10, p4

    .line 6
    .line 7
    const-string v11, "txXfrm"

    .line 8
    .line 9
    invoke-interface {v10, v11}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v12, 0x0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const/high16 v4, 0x3f800000    # 1.0f

    .line 21
    .line 22
    invoke-virtual {v1, v0, v4, v4}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    move-object v0, v12

    .line 28
    :goto_0
    const-string v1, "txBody"

    .line 29
    .line 30
    invoke-interface {v10, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 31
    .line 32
    .line 33
    move-result-object v13

    .line 34
    if-eqz v13, :cond_7

    .line 35
    .line 36
    new-instance v14, Lcom/intsig/office/common/shape/TextBox;

    .line 37
    .line 38
    invoke-direct {v14}, Lcom/intsig/office/common/shape/TextBox;-><init>()V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v14, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 42
    .line 43
    .line 44
    new-instance v15, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 45
    .line 46
    invoke-direct {v15}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 47
    .line 48
    .line 49
    const-wide/16 v4, 0x0

    .line 50
    .line 51
    invoke-virtual {v15, v4, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v14, v15}, Lcom/intsig/office/common/shape/TextBox;->setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v15}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    iget v5, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 66
    .line 67
    int-to-float v5, v5

    .line 68
    const/high16 v6, 0x41700000    # 15.0f

    .line 69
    .line 70
    mul-float v5, v5, v6

    .line 71
    .line 72
    float-to-int v5, v5

    .line 73
    invoke-virtual {v4, v1, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 74
    .line 75
    .line 76
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 77
    .line 78
    .line 79
    move-result-object v4

    .line 80
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 81
    .line 82
    int-to-float v0, v0

    .line 83
    mul-float v0, v0, v6

    .line 84
    .line 85
    float-to-int v0, v0

    .line 86
    invoke-virtual {v4, v1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 87
    .line 88
    .line 89
    const-string v8, "dgm"

    .line 90
    .line 91
    const/4 v9, 0x0

    .line 92
    if-eqz v3, :cond_1

    .line 93
    .line 94
    invoke-virtual {v3, v12, v9}, Lcom/intsig/office/pg/model/PGLayout;->getSectionAttr(Ljava/lang/String;I)Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    move-object/from16 v19, v0

    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_1
    move-object/from16 v19, v12

    .line 102
    .line 103
    :goto_1
    if-eqz v2, :cond_2

    .line 104
    .line 105
    invoke-virtual {v2, v12, v9}, Lcom/intsig/office/pg/model/PGMaster;->getSectionAttr(Ljava/lang/String;I)Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    move-object/from16 v20, v0

    .line 110
    .line 111
    goto :goto_2

    .line 112
    :cond_2
    move-object/from16 v20, v12

    .line 113
    .line 114
    :goto_2
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/SectionAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/SectionAttr;

    .line 115
    .line 116
    .line 117
    move-result-object v16

    .line 118
    const-string v7, "bodyPr"

    .line 119
    .line 120
    invoke-interface {v13, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 121
    .line 122
    .line 123
    move-result-object v17

    .line 124
    const/16 v21, 0x0

    .line 125
    .line 126
    move-object/from16 v18, v1

    .line 127
    .line 128
    invoke-virtual/range {v16 .. v21}, Lcom/intsig/office/fc/ppt/attribute/SectionAttr;->setSectionAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 129
    .line 130
    .line 131
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    const/4 v4, 0x0

    .line 136
    const-string v1, "style"

    .line 137
    .line 138
    invoke-interface {v10, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 139
    .line 140
    .line 141
    move-result-object v6

    .line 142
    const/16 v16, 0x0

    .line 143
    .line 144
    move-object/from16 v1, p1

    .line 145
    .line 146
    move-object/from16 v2, p2

    .line 147
    .line 148
    move-object/from16 v3, p3

    .line 149
    .line 150
    move-object v5, v15

    .line 151
    move-object/from16 v22, v7

    .line 152
    .line 153
    move-object v7, v13

    .line 154
    const/16 v17, 0x0

    .line 155
    .line 156
    move/from16 v9, v16

    .line 157
    .line 158
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->processParagraph(Lcom/intsig/office/system/IControl;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGStyle;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;I)I

    .line 159
    .line 160
    .line 161
    move-result v0

    .line 162
    int-to-long v0, v0

    .line 163
    invoke-virtual {v15, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v14}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 167
    .line 168
    .line 169
    move-result-object v0

    .line 170
    if-eqz v0, :cond_3

    .line 171
    .line 172
    invoke-virtual {v14}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 173
    .line 174
    .line 175
    move-result-object v0

    .line 176
    invoke-virtual {v0, v12}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    if-eqz v0, :cond_3

    .line 181
    .line 182
    invoke-virtual {v14}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 183
    .line 184
    .line 185
    move-result-object v0

    .line 186
    invoke-virtual {v0, v12}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 187
    .line 188
    .line 189
    move-result-object v0

    .line 190
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 191
    .line 192
    .line 193
    move-result v0

    .line 194
    if-lez v0, :cond_3

    .line 195
    .line 196
    invoke-virtual {v14}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    invoke-virtual {v0, v12}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v0

    .line 204
    const-string v1, "\n"

    .line 205
    .line 206
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 207
    .line 208
    .line 209
    move-result v0

    .line 210
    if-nez v0, :cond_3

    .line 211
    .line 212
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 213
    .line 214
    .line 215
    move-result-object v0

    .line 216
    invoke-interface {v10, v11}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 217
    .line 218
    .line 219
    move-result-object v1

    .line 220
    invoke-virtual {v0, v14, v1}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 221
    .line 222
    .line 223
    :cond_3
    move-object/from16 v0, v22

    .line 224
    .line 225
    invoke-interface {v13, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 226
    .line 227
    .line 228
    move-result-object v0

    .line 229
    if-eqz v0, :cond_6

    .line 230
    .line 231
    const-string v1, "wrap"

    .line 232
    .line 233
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    if-eqz v0, :cond_5

    .line 238
    .line 239
    const-string v1, "square"

    .line 240
    .line 241
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 242
    .line 243
    .line 244
    move-result v0

    .line 245
    if-eqz v0, :cond_4

    .line 246
    .line 247
    goto :goto_3

    .line 248
    :cond_4
    const/4 v9, 0x0

    .line 249
    goto :goto_4

    .line 250
    :cond_5
    :goto_3
    const/4 v9, 0x1

    .line 251
    :goto_4
    invoke-virtual {v14, v9}, Lcom/intsig/office/common/shape/TextBox;->setWrapLine(Z)V

    .line 252
    .line 253
    .line 254
    :cond_6
    return-object v14

    .line 255
    :cond_7
    return-object v12
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static instance()Lcom/intsig/office/fc/ppt/reader/SmartArtReader;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;->reader:Lcom/intsig/office/fc/ppt/reader/SmartArtReader;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private processAutoShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/shape/IShape;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object/from16 v10, p0

    .line 2
    .line 3
    move-object/from16 v11, p8

    .line 4
    .line 5
    const-string v0, "spPr"

    .line 6
    .line 7
    invoke-interface {v11, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 8
    .line 9
    .line 10
    move-result-object v12

    .line 11
    const/4 v13, 0x0

    .line 12
    if-nez v12, :cond_0

    .line 13
    .line 14
    return-object v13

    .line 15
    :cond_0
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "xfrm"

    .line 20
    .line 21
    invoke-interface {v12, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const/high16 v2, 0x3f800000    # 1.0f

    .line 26
    .line 27
    invoke-virtual {v0, v1, v2, v2}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 28
    .line 29
    .line 30
    move-result-object v14

    .line 31
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {v0, v11}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getPlaceholderName(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface/range {p8 .. p8}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    const-string v2, "cxnSp"

    .line 44
    .line 45
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    const/16 v15, 0x14

    .line 50
    .line 51
    const/16 v16, 0x1

    .line 52
    .line 53
    const/16 v17, 0x0

    .line 54
    .line 55
    if-eqz v1, :cond_1

    .line 56
    .line 57
    const/16 v0, 0x14

    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_1
    const-string v1, "Text Box"

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-nez v1, :cond_3

    .line 67
    .line 68
    const-string v1, "TextBox"

    .line 69
    .line 70
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_2
    const/4 v0, 0x0

    .line 78
    goto :goto_1

    .line 79
    :cond_3
    :goto_0
    const/4 v0, 0x1

    .line 80
    :goto_1
    const-string v1, "prstGeom"

    .line 81
    .line 82
    invoke-interface {v12, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 83
    .line 84
    .line 85
    move-result-object v1

    .line 86
    const/16 v9, 0xe9

    .line 87
    .line 88
    if-eqz v1, :cond_7

    .line 89
    .line 90
    const-string v2, "prst"

    .line 91
    .line 92
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 93
    .line 94
    .line 95
    move-result-object v3

    .line 96
    if-eqz v3, :cond_4

    .line 97
    .line 98
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    if-eqz v2, :cond_4

    .line 103
    .line 104
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    if-lez v3, :cond_4

    .line 109
    .line 110
    invoke-static {}, Lcom/intsig/office/common/autoshape/AutoShapeTypes;->instance()Lcom/intsig/office/common/autoshape/AutoShapeTypes;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/autoshape/AutoShapeTypes;->getAutoShapeType(Ljava/lang/String;)I

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    :cond_4
    const-string v2, "avLst"

    .line 119
    .line 120
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    if-eqz v1, :cond_5

    .line 125
    .line 126
    const-string v2, "gd"

    .line 127
    .line 128
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    if-eqz v1, :cond_5

    .line 133
    .line 134
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 135
    .line 136
    .line 137
    move-result v2

    .line 138
    if-lez v2, :cond_5

    .line 139
    .line 140
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 141
    .line 142
    .line 143
    move-result v2

    .line 144
    new-array v2, v2, [Ljava/lang/Float;

    .line 145
    .line 146
    const/4 v3, 0x0

    .line 147
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 148
    .line 149
    .line 150
    move-result v4

    .line 151
    if-ge v3, v4, :cond_6

    .line 152
    .line 153
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    .line 158
    .line 159
    const-string v5, "fmla"

    .line 160
    .line 161
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v4

    .line 165
    const/4 v5, 0x4

    .line 166
    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v4

    .line 170
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 171
    .line 172
    .line 173
    move-result v4

    .line 174
    const v5, 0x47c35000    # 100000.0f

    .line 175
    .line 176
    .line 177
    div-float/2addr v4, v5

    .line 178
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 179
    .line 180
    .line 181
    move-result-object v4

    .line 182
    aput-object v4, v2, v3

    .line 183
    .line 184
    add-int/lit8 v3, v3, 0x1

    .line 185
    .line 186
    goto :goto_2

    .line 187
    :cond_5
    move-object v2, v13

    .line 188
    :cond_6
    move v8, v0

    .line 189
    move-object v7, v2

    .line 190
    goto :goto_3

    .line 191
    :cond_7
    const-string v1, "custGeom"

    .line 192
    .line 193
    invoke-interface {v12, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 194
    .line 195
    .line 196
    move-result-object v1

    .line 197
    if-eqz v1, :cond_8

    .line 198
    .line 199
    move-object v7, v13

    .line 200
    const/16 v8, 0xe9

    .line 201
    .line 202
    goto :goto_3

    .line 203
    :cond_8
    move v8, v0

    .line 204
    move-object v7, v13

    .line 205
    :goto_3
    move-object/from16 v0, p0

    .line 206
    .line 207
    move-object/from16 v1, p1

    .line 208
    .line 209
    move-object/from16 v2, p2

    .line 210
    .line 211
    move-object/from16 v3, p3

    .line 212
    .line 213
    move-object/from16 v4, p4

    .line 214
    .line 215
    move-object/from16 v5, p5

    .line 216
    .line 217
    move-object/from16 v6, p6

    .line 218
    .line 219
    move-object/from16 v18, v7

    .line 220
    .line 221
    move-object/from16 v7, p7

    .line 222
    .line 223
    move/from16 v19, v8

    .line 224
    .line 225
    move-object/from16 v8, p8

    .line 226
    .line 227
    const/16 v13, 0xe9

    .line 228
    .line 229
    move/from16 v9, v19

    .line 230
    .line 231
    invoke-direct/range {v0 .. v9}, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;->getBackgrouond(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;I)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 232
    .line 233
    .line 234
    move-result-object v0

    .line 235
    move-object/from16 v4, p5

    .line 236
    .line 237
    invoke-static {v1, v2, v3, v4, v11}, Lcom/intsig/office/fc/LineKit;->createShapeLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;

    .line 238
    .line 239
    .line 240
    move-result-object v1

    .line 241
    const-string v2, "ln"

    .line 242
    .line 243
    invoke-interface {v12, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 244
    .line 245
    .line 246
    move-result-object v2

    .line 247
    const-string v3, "style"

    .line 248
    .line 249
    invoke-interface {v11, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 250
    .line 251
    .line 252
    move-result-object v3

    .line 253
    if-eqz v2, :cond_a

    .line 254
    .line 255
    const-string v3, "noFill"

    .line 256
    .line 257
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 258
    .line 259
    .line 260
    move-result-object v3

    .line 261
    if-eqz v3, :cond_b

    .line 262
    .line 263
    :cond_9
    :goto_4
    move/from16 v9, v19

    .line 264
    .line 265
    goto :goto_5

    .line 266
    :cond_a
    if-eqz v3, :cond_9

    .line 267
    .line 268
    const-string v4, "lnRef"

    .line 269
    .line 270
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 271
    .line 272
    .line 273
    move-result-object v3

    .line 274
    if-nez v3, :cond_b

    .line 275
    .line 276
    goto :goto_4

    .line 277
    :cond_b
    move/from16 v9, v19

    .line 278
    .line 279
    const/16 v17, 0x1

    .line 280
    .line 281
    :goto_5
    if-eq v9, v15, :cond_13

    .line 282
    .line 283
    const/16 v3, 0x20

    .line 284
    .line 285
    if-eq v9, v3, :cond_13

    .line 286
    .line 287
    const/16 v3, 0x22

    .line 288
    .line 289
    if-eq v9, v3, :cond_13

    .line 290
    .line 291
    const/16 v3, 0x26

    .line 292
    .line 293
    if-ne v9, v3, :cond_c

    .line 294
    .line 295
    goto :goto_8

    .line 296
    :cond_c
    if-ne v9, v13, :cond_e

    .line 297
    .line 298
    new-instance v3, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;

    .line 299
    .line 300
    invoke-direct {v3}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;-><init>()V

    .line 301
    .line 302
    .line 303
    if-eqz v1, :cond_d

    .line 304
    .line 305
    invoke-virtual {v1}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 306
    .line 307
    .line 308
    move-result-object v4

    .line 309
    move-object/from16 v20, v4

    .line 310
    .line 311
    goto :goto_6

    .line 312
    :cond_d
    const/16 v20, 0x0

    .line 313
    .line 314
    :goto_6
    move-object/from16 p1, v3

    .line 315
    .line 316
    move-object/from16 p2, p8

    .line 317
    .line 318
    move-object/from16 p3, v0

    .line 319
    .line 320
    move/from16 p4, v17

    .line 321
    .line 322
    move-object/from16 p5, v20

    .line 323
    .line 324
    move-object/from16 p6, v2

    .line 325
    .line 326
    move-object/from16 p7, v14

    .line 327
    .line 328
    invoke-static/range {p1 .. p7}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->processArbitraryPolygonShape(Lcom/intsig/office/common/shape/ArbitraryPolygonShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/bg/BackgroundAndFill;ZLcom/intsig/office/common/bg/BackgroundAndFill;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 329
    .line 330
    .line 331
    invoke-virtual {v3, v9}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 332
    .line 333
    .line 334
    invoke-direct {v10, v3, v12}, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 335
    .line 336
    .line 337
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 338
    .line 339
    .line 340
    return-object v3

    .line 341
    :cond_e
    if-nez v0, :cond_10

    .line 342
    .line 343
    if-eqz v1, :cond_f

    .line 344
    .line 345
    goto :goto_7

    .line 346
    :cond_f
    const/4 v2, 0x0

    .line 347
    return-object v2

    .line 348
    :cond_10
    :goto_7
    new-instance v2, Lcom/intsig/office/common/shape/AutoShape;

    .line 349
    .line 350
    invoke-direct {v2, v9}, Lcom/intsig/office/common/shape/AutoShape;-><init>(I)V

    .line 351
    .line 352
    .line 353
    invoke-virtual {v2, v14}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 354
    .line 355
    .line 356
    if-eqz v0, :cond_11

    .line 357
    .line 358
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 359
    .line 360
    .line 361
    :cond_11
    if-eqz v1, :cond_12

    .line 362
    .line 363
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 364
    .line 365
    .line 366
    :cond_12
    move-object/from16 v13, v18

    .line 367
    .line 368
    invoke-virtual {v2, v13}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 369
    .line 370
    .line 371
    invoke-direct {v10, v2, v12}, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 372
    .line 373
    .line 374
    return-object v2

    .line 375
    :cond_13
    :goto_8
    move-object/from16 v13, v18

    .line 376
    .line 377
    new-instance v0, Lcom/intsig/office/common/shape/LineShape;

    .line 378
    .line 379
    invoke-direct {v0}, Lcom/intsig/office/common/shape/LineShape;-><init>()V

    .line 380
    .line 381
    .line 382
    invoke-virtual {v0, v9}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 383
    .line 384
    .line 385
    invoke-virtual {v0, v14}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 386
    .line 387
    .line 388
    invoke-virtual {v0, v13}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 389
    .line 390
    .line 391
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 392
    .line 393
    .line 394
    if-eqz v2, :cond_15

    .line 395
    .line 396
    const-string v1, "headEnd"

    .line 397
    .line 398
    invoke-interface {v2, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 399
    .line 400
    .line 401
    move-result-object v1

    .line 402
    const-string v3, "len"

    .line 403
    .line 404
    const-string v4, "w"

    .line 405
    .line 406
    const-string v5, "type"

    .line 407
    .line 408
    if-eqz v1, :cond_14

    .line 409
    .line 410
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 411
    .line 412
    .line 413
    move-result-object v6

    .line 414
    if-eqz v6, :cond_14

    .line 415
    .line 416
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 417
    .line 418
    .line 419
    move-result-object v6

    .line 420
    invoke-static {v6}, Lcom/intsig/office/common/shape/Arrow;->getArrowType(Ljava/lang/String;)B

    .line 421
    .line 422
    .line 423
    move-result v6

    .line 424
    if-eqz v6, :cond_14

    .line 425
    .line 426
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 427
    .line 428
    .line 429
    move-result-object v7

    .line 430
    invoke-static {v7}, Lcom/intsig/office/common/shape/Arrow;->getArrowSize(Ljava/lang/String;)I

    .line 431
    .line 432
    .line 433
    move-result v7

    .line 434
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 435
    .line 436
    .line 437
    move-result-object v1

    .line 438
    invoke-static {v1}, Lcom/intsig/office/common/shape/Arrow;->getArrowSize(Ljava/lang/String;)I

    .line 439
    .line 440
    .line 441
    move-result v1

    .line 442
    invoke-virtual {v0, v6, v7, v1}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    .line 443
    .line 444
    .line 445
    :cond_14
    const-string v1, "tailEnd"

    .line 446
    .line 447
    invoke-interface {v2, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 448
    .line 449
    .line 450
    move-result-object v1

    .line 451
    if-eqz v1, :cond_15

    .line 452
    .line 453
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 454
    .line 455
    .line 456
    move-result-object v2

    .line 457
    if-eqz v2, :cond_15

    .line 458
    .line 459
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 460
    .line 461
    .line 462
    move-result-object v2

    .line 463
    invoke-static {v2}, Lcom/intsig/office/common/shape/Arrow;->getArrowType(Ljava/lang/String;)B

    .line 464
    .line 465
    .line 466
    move-result v2

    .line 467
    if-eqz v2, :cond_15

    .line 468
    .line 469
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 470
    .line 471
    .line 472
    move-result-object v4

    .line 473
    invoke-static {v4}, Lcom/intsig/office/common/shape/Arrow;->getArrowSize(Ljava/lang/String;)I

    .line 474
    .line 475
    .line 476
    move-result v4

    .line 477
    invoke-interface {v1, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 478
    .line 479
    .line 480
    move-result-object v1

    .line 481
    invoke-static {v1}, Lcom/intsig/office/common/shape/Arrow;->getArrowSize(Ljava/lang/String;)I

    .line 482
    .line 483
    .line 484
    move-result v1

    .line 485
    invoke-virtual {v0, v2, v4, v1}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    .line 486
    .line 487
    .line 488
    :cond_15
    invoke-direct {v10, v0, v12}, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 489
    .line 490
    .line 491
    return-object v0
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
.end method

.method private processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/IShape;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public read(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)Lcom/intsig/office/common/shape/SmartArt;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object/from16 v9, p1

    .line 2
    .line 3
    move-object/from16 v10, p2

    .line 4
    .line 5
    move-object/from16 v11, p4

    .line 6
    .line 7
    new-instance v6, Lcom/intsig/office/fc/dom4j/io/SAXReader;

    .line 8
    .line 9
    invoke-direct {v6}, Lcom/intsig/office/fc/dom4j/io/SAXReader;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual/range {p8 .. p8}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v6, v0}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 21
    .line 22
    .line 23
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 24
    .line 25
    .line 26
    move-result-object v7

    .line 27
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->instance()Lcom/intsig/office/fc/ppt/reader/BackgroundReader;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const-string v1, "bg"

    .line 32
    .line 33
    invoke-interface {v7, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    move-object/from16 v1, p1

    .line 38
    .line 39
    move-object/from16 v2, p2

    .line 40
    .line 41
    move-object/from16 v3, p8

    .line 42
    .line 43
    move-object/from16 v4, p4

    .line 44
    .line 45
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    const-string v1, "whole"

    .line 50
    .line 51
    invoke-interface {v7, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    const-string v2, "ln"

    .line 56
    .line 57
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    move-object/from16 v2, p8

    .line 62
    .line 63
    invoke-static {v9, v10, v2, v11, v1}, Lcom/intsig/office/fc/LineKit;->createLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const-string v2, "extLst"

    .line 68
    .line 69
    invoke-interface {v7, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    const/4 v3, 0x0

    .line 74
    if-eqz v2, :cond_0

    .line 75
    .line 76
    const-string v4, "ext"

    .line 77
    .line 78
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    if-eqz v2, :cond_0

    .line 83
    .line 84
    const-string v4, "dataModelExt"

    .line 85
    .line 86
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    if-eqz v2, :cond_0

    .line 91
    .line 92
    const-string v4, "relId"

    .line 93
    .line 94
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    if-eqz v2, :cond_0

    .line 99
    .line 100
    move-object/from16 v4, p7

    .line 101
    .line 102
    invoke-virtual {v4, v2}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    if-eqz v2, :cond_0

    .line 107
    .line 108
    invoke-virtual {v2}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    invoke-virtual {v10, v2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 113
    .line 114
    .line 115
    move-result-object v2

    .line 116
    move-object v12, v2

    .line 117
    goto :goto_0

    .line 118
    :cond_0
    move-object v12, v3

    .line 119
    :goto_0
    if-nez v12, :cond_1

    .line 120
    .line 121
    return-object v3

    .line 122
    :cond_1
    invoke-virtual {v12}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    .line 123
    .line 124
    .line 125
    move-result-object v2

    .line 126
    invoke-virtual {v6, v2}, Lcom/intsig/office/fc/dom4j/io/SAXReader;->read(Ljava/io/InputStream;)Lcom/intsig/office/fc/dom4j/Document;

    .line 127
    .line 128
    .line 129
    move-result-object v3

    .line 130
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 131
    .line 132
    .line 133
    new-instance v13, Lcom/intsig/office/common/shape/SmartArt;

    .line 134
    .line 135
    invoke-direct {v13}, Lcom/intsig/office/common/shape/SmartArt;-><init>()V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v13, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 142
    .line 143
    .line 144
    invoke-interface {v3}, Lcom/intsig/office/fc/dom4j/Document;->getRootElement()Lcom/intsig/office/fc/dom4j/Element;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    const-string v1, "spTree"

    .line 149
    .line 150
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    if-eqz v0, :cond_4

    .line 155
    .line 156
    const-string v1, "sp"

    .line 157
    .line 158
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator(Ljava/lang/String;)Ljava/util/Iterator;

    .line 159
    .line 160
    .line 161
    move-result-object v14

    .line 162
    :cond_2
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    .line 163
    .line 164
    .line 165
    move-result v0

    .line 166
    if-eqz v0, :cond_4

    .line 167
    .line 168
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 169
    .line 170
    .line 171
    move-result-object v0

    .line 172
    move-object v15, v0

    .line 173
    check-cast v15, Lcom/intsig/office/fc/dom4j/Element;

    .line 174
    .line 175
    move-object/from16 v0, p0

    .line 176
    .line 177
    move-object/from16 v1, p1

    .line 178
    .line 179
    move-object/from16 v2, p2

    .line 180
    .line 181
    move-object v3, v12

    .line 182
    move-object/from16 v4, p3

    .line 183
    .line 184
    move-object/from16 v5, p4

    .line 185
    .line 186
    move-object/from16 v6, p5

    .line 187
    .line 188
    move-object/from16 v7, p6

    .line 189
    .line 190
    move-object v8, v15

    .line 191
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;->processAutoShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/shape/IShape;

    .line 192
    .line 193
    .line 194
    move-result-object v0

    .line 195
    if-eqz v0, :cond_3

    .line 196
    .line 197
    invoke-interface {v0, v13}, Lcom/intsig/office/common/shape/IShape;->setParent(Lcom/intsig/office/common/shape/IShape;)V

    .line 198
    .line 199
    .line 200
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/shape/SmartArt;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 201
    .line 202
    .line 203
    :cond_3
    move-object/from16 v0, p0

    .line 204
    .line 205
    move-object/from16 v1, p5

    .line 206
    .line 207
    invoke-direct {v0, v9, v11, v1, v15}, Lcom/intsig/office/fc/ppt/reader/SmartArtReader;->getTextBoxData(Lcom/intsig/office/system/IControl;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/shape/IShape;

    .line 208
    .line 209
    .line 210
    move-result-object v2

    .line 211
    if-eqz v2, :cond_2

    .line 212
    .line 213
    invoke-virtual {v13, v2}, Lcom/intsig/office/common/shape/SmartArt;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 214
    .line 215
    .line 216
    goto :goto_1

    .line 217
    :cond_4
    move-object/from16 v0, p0

    .line 218
    .line 219
    return-object v13
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
.end method

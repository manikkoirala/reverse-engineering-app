.class public Lcom/intsig/office/fc/ppt/attribute/ParaAttr;
.super Ljava/lang/Object;
.source "ParaAttr.java"


# static fields
.field public static final POINT_PER_LINE_PER_FONTSIZE:F = 1.2f

.field private static kit:Lcom/intsig/office/fc/ppt/attribute/ParaAttr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->kit:Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->kit:Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public processParaWithPct(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 9

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->getMaxFontSize()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz p1, :cond_1

    .line 10
    .line 11
    const-string v1, "spcBef"

    .line 12
    .line 13
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const/high16 v2, 0x41a00000    # 20.0f

    .line 18
    .line 19
    const v3, 0x3f99999a    # 1.2f

    .line 20
    .line 21
    .line 22
    const v4, 0x47c35000    # 100000.0f

    .line 23
    .line 24
    .line 25
    const-string v5, "spcPct"

    .line 26
    .line 27
    const-string v6, "val"

    .line 28
    .line 29
    if-eqz v1, :cond_0

    .line 30
    .line 31
    invoke-interface {v1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    if-eqz v1, :cond_0

    .line 36
    .line 37
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 38
    .line 39
    .line 40
    move-result-object v7

    .line 41
    if-eqz v7, :cond_0

    .line 42
    .line 43
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    if-eqz v1, :cond_0

    .line 48
    .line 49
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 50
    .line 51
    .line 52
    move-result v7

    .line 53
    if-lez v7, :cond_0

    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 56
    .line 57
    .line 58
    move-result-object v7

    .line 59
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    int-to-float v1, v1

    .line 64
    div-float/2addr v1, v4

    .line 65
    int-to-float v8, v0

    .line 66
    mul-float v1, v1, v8

    .line 67
    .line 68
    mul-float v1, v1, v3

    .line 69
    .line 70
    mul-float v1, v1, v2

    .line 71
    .line 72
    float-to-int v1, v1

    .line 73
    invoke-virtual {v7, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 74
    .line 75
    .line 76
    :cond_0
    const-string v1, "spcAft"

    .line 77
    .line 78
    invoke-interface {p1, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    if-eqz p1, :cond_1

    .line 83
    .line 84
    invoke-interface {p1, v5}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    if-eqz p1, :cond_1

    .line 89
    .line 90
    invoke-interface {p1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    if-eqz v1, :cond_1

    .line 95
    .line 96
    invoke-interface {p1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    if-eqz p1, :cond_1

    .line 101
    .line 102
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 103
    .line 104
    .line 105
    move-result v1

    .line 106
    if-lez v1, :cond_1

    .line 107
    .line 108
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 113
    .line 114
    .line 115
    move-result p1

    .line 116
    int-to-float p1, p1

    .line 117
    div-float/2addr p1, v4

    .line 118
    int-to-float v0, v0

    .line 119
    mul-float p1, p1, v0

    .line 120
    .line 121
    mul-float p1, p1, v3

    .line 122
    .line 123
    mul-float p1, p1, v2

    .line 124
    .line 125
    float-to-int p1, p1

    .line 126
    invoke-virtual {v1, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 127
    .line 128
    .line 129
    :cond_1
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public processParagraph(Lcom/intsig/office/system/IControl;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGStyle;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;I)I
    .locals 31

    .line 1
    move-object/from16 v10, p0

    .line 2
    .line 3
    move-object/from16 v11, p2

    .line 4
    .line 5
    move-object/from16 v12, p3

    .line 6
    .line 7
    move-object/from16 v13, p4

    .line 8
    .line 9
    move-object/from16 v14, p6

    .line 10
    .line 11
    move-object/from16 v0, p7

    .line 12
    .line 13
    move-object/from16 v15, p8

    .line 14
    .line 15
    move/from16 v9, p9

    .line 16
    .line 17
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 18
    .line 19
    const-string v2, "bodyPr"

    .line 20
    .line 21
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    const/16 v3, 0x64

    .line 26
    .line 27
    if-eqz v2, :cond_2

    .line 28
    .line 29
    const-string v4, "normAutofit"

    .line 30
    .line 31
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    if-eqz v2, :cond_2

    .line 36
    .line 37
    const-string v4, "fontScale"

    .line 38
    .line 39
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 40
    .line 41
    .line 42
    move-result-object v5

    .line 43
    if-eqz v5, :cond_0

    .line 44
    .line 45
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v4

    .line 49
    if-eqz v4, :cond_0

    .line 50
    .line 51
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    if-lez v5, :cond_0

    .line 56
    .line 57
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    div-int/lit16 v3, v3, 0x3e8

    .line 62
    .line 63
    :cond_0
    const-string v4, "lnSpcReduction"

    .line 64
    .line 65
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 66
    .line 67
    .line 68
    move-result-object v5

    .line 69
    if-eqz v5, :cond_1

    .line 70
    .line 71
    invoke-interface {v2, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    if-eqz v2, :cond_1

    .line 76
    .line 77
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 78
    .line 79
    .line 80
    move-result v4

    .line 81
    if-lez v4, :cond_1

    .line 82
    .line 83
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    move/from16 v16, v2

    .line 88
    .line 89
    move/from16 v17, v3

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_1
    move/from16 v17, v3

    .line 93
    .line 94
    const/16 v16, 0x0

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_2
    const/16 v16, 0x0

    .line 98
    .line 99
    const/16 v17, 0x64

    .line 100
    .line 101
    :goto_0
    const-string v2, "subTitle"

    .line 102
    .line 103
    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 104
    .line 105
    .line 106
    move-result v18

    .line 107
    const-string v2, "lstStyle"

    .line 108
    .line 109
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 110
    .line 111
    .line 112
    move-result-object v7

    .line 113
    const-string v2, "p"

    .line 114
    .line 115
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 120
    .line 121
    .line 122
    move-result-object v19

    .line 123
    const/4 v6, 0x0

    .line 124
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    if-eqz v0, :cond_e

    .line 129
    .line 130
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    move-object v5, v0

    .line 135
    check-cast v5, Lcom/intsig/office/fc/dom4j/Element;

    .line 136
    .line 137
    const-string v0, "pPr"

    .line 138
    .line 139
    invoke-interface {v5, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 140
    .line 141
    .line 142
    move-result-object v4

    .line 143
    const/4 v0, 0x1

    .line 144
    if-eqz v4, :cond_3

    .line 145
    .line 146
    const-string v2, "lvl"

    .line 147
    .line 148
    invoke-interface {v4, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 149
    .line 150
    .line 151
    move-result-object v3

    .line 152
    if-eqz v3, :cond_3

    .line 153
    .line 154
    invoke-interface {v4, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v2

    .line 158
    if-eqz v2, :cond_3

    .line 159
    .line 160
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 161
    .line 162
    .line 163
    move-result v3

    .line 164
    if-lez v3, :cond_3

    .line 165
    .line 166
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 167
    .line 168
    .line 169
    move-result v2

    .line 170
    add-int/2addr v0, v2

    .line 171
    move v3, v0

    .line 172
    goto :goto_2

    .line 173
    :cond_3
    const/4 v3, 0x1

    .line 174
    :goto_2
    const/4 v0, -0x1

    .line 175
    if-eqz v12, :cond_4

    .line 176
    .line 177
    invoke-virtual {v12, v15, v9, v3}, Lcom/intsig/office/pg/model/PGLayout;->getStyleID(Ljava/lang/String;II)I

    .line 178
    .line 179
    .line 180
    move-result v2

    .line 181
    goto :goto_3

    .line 182
    :cond_4
    const/4 v2, -0x1

    .line 183
    :goto_3
    if-eqz v11, :cond_5

    .line 184
    .line 185
    invoke-virtual {v11, v15, v9, v3}, Lcom/intsig/office/pg/model/PGMaster;->getTextStyle(Ljava/lang/String;II)I

    .line 186
    .line 187
    .line 188
    move-result v0

    .line 189
    :cond_5
    if-gez v0, :cond_6

    .line 190
    .line 191
    if-eqz v13, :cond_6

    .line 192
    .line 193
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 194
    .line 195
    invoke-virtual {v13, v3}, Lcom/intsig/office/pg/model/PGStyle;->getStyle(I)I

    .line 196
    .line 197
    .line 198
    move-result v1

    .line 199
    move-object/from16 v20, v0

    .line 200
    .line 201
    move/from16 v21, v1

    .line 202
    .line 203
    goto :goto_4

    .line 204
    :cond_6
    move/from16 v21, v0

    .line 205
    .line 206
    move-object/from16 v20, v1

    .line 207
    .line 208
    :goto_4
    new-instance v1, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 209
    .line 210
    invoke-direct {v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 211
    .line 212
    .line 213
    int-to-long v8, v6

    .line 214
    invoke-virtual {v1, v8, v9}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 215
    .line 216
    .line 217
    const/4 v0, 0x0

    .line 218
    if-eqz v7, :cond_9

    .line 219
    .line 220
    const-string v8, "defPPr"

    .line 221
    .line 222
    if-gtz v3, :cond_8

    .line 223
    .line 224
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 225
    .line 226
    .line 227
    move-result-object v9

    .line 228
    if-nez v9, :cond_7

    .line 229
    .line 230
    goto :goto_5

    .line 231
    :cond_7
    move v9, v3

    .line 232
    goto :goto_6

    .line 233
    :cond_8
    :goto_5
    add-int/lit8 v9, v3, 0x1

    .line 234
    .line 235
    :goto_6
    packed-switch v9, :pswitch_data_0

    .line 236
    .line 237
    .line 238
    move-object v9, v0

    .line 239
    goto :goto_8

    .line 240
    :pswitch_0
    const-string v8, "lvl9pPr"

    .line 241
    .line 242
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 243
    .line 244
    .line 245
    move-result-object v8

    .line 246
    goto :goto_7

    .line 247
    :pswitch_1
    const-string v8, "lvl8pPr"

    .line 248
    .line 249
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 250
    .line 251
    .line 252
    move-result-object v8

    .line 253
    goto :goto_7

    .line 254
    :pswitch_2
    const-string v8, "lvl7pPr"

    .line 255
    .line 256
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 257
    .line 258
    .line 259
    move-result-object v8

    .line 260
    goto :goto_7

    .line 261
    :pswitch_3
    const-string v8, "lvl6pPr"

    .line 262
    .line 263
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 264
    .line 265
    .line 266
    move-result-object v8

    .line 267
    goto :goto_7

    .line 268
    :pswitch_4
    const-string v8, "lvl5pPr"

    .line 269
    .line 270
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 271
    .line 272
    .line 273
    move-result-object v8

    .line 274
    goto :goto_7

    .line 275
    :pswitch_5
    const-string v8, "lvl4pPr"

    .line 276
    .line 277
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 278
    .line 279
    .line 280
    move-result-object v8

    .line 281
    goto :goto_7

    .line 282
    :pswitch_6
    const-string v8, "lvl3pPr"

    .line 283
    .line 284
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 285
    .line 286
    .line 287
    move-result-object v8

    .line 288
    goto :goto_7

    .line 289
    :pswitch_7
    const-string v8, "lvl2pPr"

    .line 290
    .line 291
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 292
    .line 293
    .line 294
    move-result-object v8

    .line 295
    goto :goto_7

    .line 296
    :pswitch_8
    const-string v8, "lvl1pPr"

    .line 297
    .line 298
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 299
    .line 300
    .line 301
    move-result-object v8

    .line 302
    goto :goto_7

    .line 303
    :pswitch_9
    invoke-interface {v7, v8}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 304
    .line 305
    .line 306
    move-result-object v8

    .line 307
    :goto_7
    move-object v9, v8

    .line 308
    :goto_8
    if-eqz v9, :cond_9

    .line 309
    .line 310
    new-instance v8, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 311
    .line 312
    invoke-direct {v8}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 313
    .line 314
    .line 315
    const/16 v23, 0x0

    .line 316
    .line 317
    const/16 v24, -0x1

    .line 318
    .line 319
    const/16 v25, -0x1

    .line 320
    .line 321
    const/16 v26, 0x0

    .line 322
    .line 323
    const/16 v27, 0x1

    .line 324
    .line 325
    move-object/from16 v0, p0

    .line 326
    .line 327
    move-object/from16 p7, v1

    .line 328
    .line 329
    move-object/from16 v1, p1

    .line 330
    .line 331
    move/from16 v28, v2

    .line 332
    .line 333
    move-object v2, v9

    .line 334
    move/from16 v29, v3

    .line 335
    .line 336
    move-object v3, v8

    .line 337
    move-object/from16 v30, v4

    .line 338
    .line 339
    move-object/from16 v4, v23

    .line 340
    .line 341
    move-object/from16 v23, v5

    .line 342
    .line 343
    move/from16 v5, v24

    .line 344
    .line 345
    move/from16 v24, v6

    .line 346
    .line 347
    move/from16 v6, v25

    .line 348
    .line 349
    move-object/from16 v25, v7

    .line 350
    .line 351
    move/from16 v7, v26

    .line 352
    .line 353
    move-object/from16 v26, v8

    .line 354
    .line 355
    move/from16 v8, v27

    .line 356
    .line 357
    move-object v12, v9

    .line 358
    move/from16 v9, v18

    .line 359
    .line 360
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIIZZ)V

    .line 361
    .line 362
    .line 363
    const-string v0, "defRPr"

    .line 364
    .line 365
    invoke-interface {v12, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 366
    .line 367
    .line 368
    move-result-object v2

    .line 369
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 370
    .line 371
    .line 372
    move-result-object v0

    .line 373
    const/4 v4, 0x0

    .line 374
    const/16 v5, 0x64

    .line 375
    .line 376
    const/4 v6, -0x1

    .line 377
    const/4 v7, 0x0

    .line 378
    move-object/from16 v1, p2

    .line 379
    .line 380
    move-object/from16 v3, v26

    .line 381
    .line 382
    invoke-virtual/range {v0 .. v7}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setRunAttribute(Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIZ)V

    .line 383
    .line 384
    .line 385
    move-object/from16 v0, v26

    .line 386
    .line 387
    invoke-virtual {v10, v12, v0}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->processParaWithPct(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 388
    .line 389
    .line 390
    goto :goto_9

    .line 391
    :cond_9
    move-object/from16 p7, v1

    .line 392
    .line 393
    move/from16 v28, v2

    .line 394
    .line 395
    move/from16 v29, v3

    .line 396
    .line 397
    move-object/from16 v30, v4

    .line 398
    .line 399
    move-object/from16 v23, v5

    .line 400
    .line 401
    move/from16 v24, v6

    .line 402
    .line 403
    move-object/from16 v25, v7

    .line 404
    .line 405
    :goto_9
    move/from16 v8, v28

    .line 406
    .line 407
    if-nez v0, :cond_a

    .line 408
    .line 409
    if-lez v8, :cond_a

    .line 410
    .line 411
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    .line 412
    .line 413
    .line 414
    move-result-object v1

    .line 415
    invoke-virtual {v1, v8}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    .line 416
    .line 417
    .line 418
    move-result-object v1

    .line 419
    if-eqz v1, :cond_c

    .line 420
    .line 421
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 422
    .line 423
    .line 424
    move-result-object v0

    .line 425
    goto :goto_a

    .line 426
    :cond_a
    if-nez v0, :cond_b

    .line 427
    .line 428
    if-eqz v14, :cond_b

    .line 429
    .line 430
    const-string v1, "fontRef"

    .line 431
    .line 432
    invoke-interface {v14, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 433
    .line 434
    .line 435
    move-result-object v1

    .line 436
    invoke-interface {v1}, Lcom/intsig/office/fc/dom4j/Element;->elements()Ljava/util/List;

    .line 437
    .line 438
    .line 439
    move-result-object v2

    .line 440
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 441
    .line 442
    .line 443
    move-result v2

    .line 444
    if-lez v2, :cond_c

    .line 445
    .line 446
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 447
    .line 448
    .line 449
    move-result-object v0

    .line 450
    invoke-virtual {v0, v11, v1}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getColor(Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)I

    .line 451
    .line 452
    .line 453
    move-result v0

    .line 454
    new-instance v1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 455
    .line 456
    invoke-direct {v1}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 457
    .line 458
    .line 459
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 460
    .line 461
    .line 462
    move-result-object v2

    .line 463
    invoke-virtual {v2, v1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 464
    .line 465
    .line 466
    move-object v0, v1

    .line 467
    goto :goto_a

    .line 468
    :cond_b
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    .line 469
    .line 470
    .line 471
    move-result v1

    .line 472
    if-eqz v1, :cond_c

    .line 473
    .line 474
    if-nez v0, :cond_c

    .line 475
    .line 476
    if-eqz v13, :cond_c

    .line 477
    .line 478
    move/from16 v1, v29

    .line 479
    .line 480
    invoke-virtual {v13, v1}, Lcom/intsig/office/pg/model/PGStyle;->getDefaultFontColor(I)Ljava/lang/String;

    .line 481
    .line 482
    .line 483
    move-result-object v1

    .line 484
    if-eqz v1, :cond_c

    .line 485
    .line 486
    new-instance v0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 487
    .line 488
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 489
    .line 490
    .line 491
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 492
    .line 493
    .line 494
    move-result-object v2

    .line 495
    invoke-virtual {v11, v1}, Lcom/intsig/office/pg/model/PGMaster;->getColor(Ljava/lang/String;)I

    .line 496
    .line 497
    .line 498
    move-result v1

    .line 499
    invoke-virtual {v2, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 500
    .line 501
    .line 502
    :cond_c
    :goto_a
    move-object v9, v0

    .line 503
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 504
    .line 505
    .line 506
    move-result-object v0

    .line 507
    move-object/from16 v1, p2

    .line 508
    .line 509
    move-object/from16 v2, p7

    .line 510
    .line 511
    move-object/from16 v3, v23

    .line 512
    .line 513
    move-object v4, v9

    .line 514
    move/from16 v5, v24

    .line 515
    .line 516
    move/from16 v6, v17

    .line 517
    .line 518
    move/from16 v7, v21

    .line 519
    .line 520
    invoke-virtual/range {v0 .. v7}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->processRun(Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/simpletext/model/ParagraphElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;III)I

    .line 521
    .line 522
    .line 523
    move-result v12

    .line 524
    const-string v0, "r"

    .line 525
    .line 526
    move-object/from16 v1, v23

    .line 527
    .line 528
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 529
    .line 530
    .line 531
    move-result-object v0

    .line 532
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 533
    .line 534
    .line 535
    move-result v0

    .line 536
    if-nez v0, :cond_d

    .line 537
    .line 538
    const-string v0, "fld"

    .line 539
    .line 540
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    .line 541
    .line 542
    .line 543
    move-result-object v0

    .line 544
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 545
    .line 546
    .line 547
    move-result v0

    .line 548
    if-nez v0, :cond_d

    .line 549
    .line 550
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 551
    .line 552
    .line 553
    move-result-object v3

    .line 554
    const/16 v22, 0x0

    .line 555
    .line 556
    move-object/from16 v0, p0

    .line 557
    .line 558
    move-object/from16 v1, p1

    .line 559
    .line 560
    move-object/from16 v2, v30

    .line 561
    .line 562
    move-object v4, v9

    .line 563
    move v5, v8

    .line 564
    move/from16 v6, v21

    .line 565
    .line 566
    move/from16 v7, v16

    .line 567
    .line 568
    move/from16 v8, v22

    .line 569
    .line 570
    move/from16 v9, v18

    .line 571
    .line 572
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIIZZ)V

    .line 573
    .line 574
    .line 575
    goto :goto_b

    .line 576
    :cond_d
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 577
    .line 578
    .line 579
    move-result-object v3

    .line 580
    const/16 v22, 0x1

    .line 581
    .line 582
    move-object/from16 v0, p0

    .line 583
    .line 584
    move-object/from16 v1, p1

    .line 585
    .line 586
    move-object/from16 v2, v30

    .line 587
    .line 588
    move-object v4, v9

    .line 589
    move v5, v8

    .line 590
    move/from16 v6, v21

    .line 591
    .line 592
    move/from16 v7, v16

    .line 593
    .line 594
    move/from16 v8, v22

    .line 595
    .line 596
    move/from16 v9, v18

    .line 597
    .line 598
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAttribute(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIIZZ)V

    .line 599
    .line 600
    .line 601
    :goto_b
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 602
    .line 603
    .line 604
    move-result-object v0

    .line 605
    move-object/from16 v1, v30

    .line 606
    .line 607
    invoke-virtual {v10, v1, v0}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->processParaWithPct(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 608
    .line 609
    .line 610
    int-to-long v0, v12

    .line 611
    move-object/from16 v2, p7

    .line 612
    .line 613
    invoke-virtual {v2, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 614
    .line 615
    .line 616
    const-wide/16 v0, 0x0

    .line 617
    .line 618
    move-object/from16 v3, p5

    .line 619
    .line 620
    invoke-virtual {v3, v2, v0, v1}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 621
    .line 622
    .line 623
    move/from16 v9, p9

    .line 624
    .line 625
    move v6, v12

    .line 626
    move-object/from16 v1, v20

    .line 627
    .line 628
    move-object/from16 v7, v25

    .line 629
    .line 630
    move-object/from16 v12, p3

    .line 631
    .line 632
    goto/16 :goto_1

    .line 633
    .line 634
    :cond_e
    move/from16 v24, v6

    .line 635
    .line 636
    invoke-static {}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->instance()Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;

    .line 637
    .line 638
    .line 639
    move-result-object v0

    .line 640
    invoke-virtual {v0}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->clearData()V

    .line 641
    .line 642
    .line 643
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    .line 644
    .line 645
    .line 646
    move-result-object v0

    .line 647
    const/4 v1, 0x0

    .line 648
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setMaxFontSize(I)V

    .line 649
    .line 650
    .line 651
    return v24

    .line 652
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x1005

    .line 8
    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;Ljava/lang/String;)V
    .locals 1

    .line 1
    const-string v0, "l"

    .line 2
    .line 3
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p2, p1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const-string v0, "ctr"

    .line 19
    .line 20
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    const/4 v0, 0x1

    .line 31
    invoke-virtual {p2, p1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const-string v0, "r"

    .line 36
    .line 37
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result p2

    .line 41
    if-eqz p2, :cond_2

    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    const/4 v0, 0x2

    .line 48
    invoke-virtual {p2, p1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 49
    .line 50
    .line 51
    :cond_2
    :goto_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaAttribute(Lcom/intsig/office/ss/model/style/CellStyle;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 5

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 89
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/style/CellStyle;->getIndent()S

    move-result v0

    mul-int/lit8 v0, v0, 0x22

    .line 90
    invoke-virtual {p1}, Lcom/intsig/office/ss/model/style/CellStyle;->getHorizontalAlign()S

    move-result p1

    const/16 v1, 0x1003

    const/high16 v2, 0x41700000    # 15.0f

    const/16 v3, 0x1001

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 91
    :pswitch_0
    invoke-interface {p3, v3, v4}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    int-to-float p1, v0

    mul-float p1, p1, v2

    .line 92
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-interface {p3, v1, p1}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 93
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_0

    .line 94
    :pswitch_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_0

    :pswitch_2
    int-to-float p1, v0

    mul-float p1, p1, v2

    .line 95
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-interface {p3, v3, p1}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 96
    invoke-interface {p3, v1, v4}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 97
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object p1

    invoke-virtual {p1, p2, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 98
    :goto_0
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 99
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 100
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 101
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 102
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 103
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    goto :goto_1

    :cond_0
    if-eqz p3, :cond_1

    .line 104
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 105
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 106
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 107
    invoke-virtual {p0, p3, p2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    :cond_1
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public setParaAttribute(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;IIIZZ)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    const v6, 0x186a0

    const v7, 0x495f3e00    # 914400.0f

    const/high16 v8, 0x42900000    # 72.0f

    const v9, 0x47c35000    # 100000.0f

    const/4 v10, 0x5

    const/high16 v11, 0x41a00000    # 20.0f

    if-eqz v1, :cond_a

    const-string v12, "algn"

    .line 1
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 2
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 3
    invoke-virtual {v0, v2, v12}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;Ljava/lang/String;)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    :goto_0
    const-string v12, "spcBef"

    .line 5
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v12

    const-string v13, "spcPts"

    const-string v14, "val"

    if-eqz v12, :cond_1

    .line 6
    invoke-interface {v12, v13}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 7
    invoke-interface {v12, v14}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v15

    if-eqz v15, :cond_2

    .line 8
    invoke-interface {v12, v14}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 9
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_2

    .line 10
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v15

    .line 11
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    div-int/lit8 v12, v12, 0x64

    int-to-float v12, v12

    mul-float v12, v12, v11

    float-to-int v12, v12

    .line 12
    invoke-virtual {v15, v2, v12}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_1

    .line 13
    :cond_1
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    :cond_2
    :goto_1
    const-string v12, "spcAft"

    .line 14
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v12

    if-eqz v12, :cond_3

    .line 15
    invoke-interface {v12, v13}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v12

    if-eqz v12, :cond_4

    .line 16
    invoke-interface {v12, v14}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v15

    if-eqz v15, :cond_4

    .line 17
    invoke-interface {v12, v14}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_4

    .line 18
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_4

    .line 19
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v15

    .line 20
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    div-int/lit8 v12, v12, 0x64

    int-to-float v12, v12

    mul-float v12, v12, v11

    float-to-int v12, v12

    .line 21
    invoke-virtual {v15, v2, v12}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_2

    .line 22
    :cond_3
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    :cond_4
    :goto_2
    const-string v12, "lnSpc"

    .line 23
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v12

    if-eqz v12, :cond_6

    .line 24
    invoke-interface {v12, v13}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 25
    invoke-interface {v6, v14}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v13

    if-eqz v13, :cond_5

    .line 26
    invoke-interface {v6, v14}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 27
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_5

    .line 28
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v13

    const/4 v15, 0x4

    invoke-virtual {v13, v2, v15}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 29
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v13

    .line 30
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    div-int/lit8 v6, v6, 0x64

    int-to-float v6, v6

    mul-float v6, v6, v11

    float-to-int v6, v6

    int-to-float v6, v6

    .line 31
    invoke-virtual {v13, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    :cond_5
    const-string v6, "spcPct"

    .line 32
    invoke-interface {v12, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 33
    invoke-interface {v6, v14}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v12

    if-eqz v12, :cond_8

    .line 34
    invoke-interface {v6, v14}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 35
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_8

    .line 36
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v12

    invoke-virtual {v12, v2, v10}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 37
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    sub-int v6, v6, p7

    int-to-float v6, v6

    div-float/2addr v6, v9

    invoke-virtual {v10, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    goto :goto_3

    :cond_6
    if-lez p7, :cond_7

    .line 38
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v12

    invoke-virtual {v12, v2, v10}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 39
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    sub-int v6, v6, p7

    int-to-float v6, v6

    div-float/2addr v6, v9

    invoke-virtual {v10, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    goto :goto_3

    .line 40
    :cond_7
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    :cond_8
    :goto_3
    const-string v6, "marR"

    .line 41
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v9

    if-eqz v9, :cond_9

    .line 42
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 43
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_c

    .line 44
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    .line 45
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v8

    div-float/2addr v6, v7

    mul-float v6, v6, v11

    float-to-int v6, v6

    .line 46
    invoke-virtual {v9, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_5

    .line 47
    :cond_9
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    goto :goto_5

    .line 48
    :cond_a
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 49
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 50
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    if-lez p7, :cond_b

    .line 51
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v12

    invoke-virtual {v12, v2, v10}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 52
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    sub-int v6, v6, p7

    int-to-float v6, v6

    div-float/2addr v6, v9

    invoke-virtual {v10, v2, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    goto :goto_4

    .line 53
    :cond_b
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 54
    :goto_4
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 55
    invoke-virtual {v0, v3, v2}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 56
    :cond_c
    :goto_5
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    move-result-object v6

    if-eqz v1, :cond_d

    const-string v9, "marL"

    .line 57
    invoke-interface {v1, v9}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v10

    if-eqz v10, :cond_d

    .line 58
    invoke-interface {v1, v9}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_f

    .line 59
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_f

    .line 60
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    int-to-float v9, v9

    mul-float v9, v9, v8

    div-float/2addr v9, v7

    mul-float v9, v9, v11

    float-to-int v9, v9

    .line 61
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    invoke-virtual {v10, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentInitLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 62
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    invoke-virtual {v10, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_6

    :cond_d
    const/16 v9, 0x1001

    if-eqz v3, :cond_e

    .line 63
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    invoke-virtual {v10, v3, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 64
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentInitLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v9

    .line 65
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    invoke-virtual {v10, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_6

    :cond_e
    if-eqz v6, :cond_f

    .line 66
    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v10

    if-eqz v10, :cond_f

    .line 67
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v12

    invoke-virtual {v10, v12, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 68
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v9

    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentInitLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v9

    .line 69
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v10

    invoke-virtual {v10, v2, v9}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    goto :goto_6

    :cond_f
    const/4 v9, 0x0

    :goto_6
    const/4 v10, 0x1

    if-eqz v1, :cond_10

    const-string v12, "indent"

    .line 70
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v13

    if-eqz v13, :cond_10

    .line 71
    invoke-interface {v1, v12}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_12

    .line 72
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_12

    .line 73
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v8

    div-float/2addr v6, v7

    mul-float v6, v6, v11

    float-to-int v6, v6

    .line 74
    invoke-virtual {v0, v2, v9, v6, v10}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;IIZ)V

    goto :goto_7

    :cond_10
    const/16 v7, 0x1008

    if-eqz v3, :cond_11

    .line 75
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v6

    invoke-virtual {v6, v3, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 76
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v6

    .line 77
    invoke-virtual {v0, v2, v9, v6, v10}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;IIZ)V

    goto :goto_7

    :cond_11
    if-eqz v6, :cond_12

    .line 78
    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v8

    if-eqz v8, :cond_12

    .line 79
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v8

    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v11

    invoke-virtual {v8, v11, v7}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 80
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v7

    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v6

    .line 81
    invoke-virtual {v0, v2, v9, v6, v10}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->setSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;IIZ)V

    :cond_12
    :goto_7
    if-eqz p8, :cond_17

    if-eqz v1, :cond_13

    const-string v6, "buNone"

    .line 82
    invoke-interface {v1, v6}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    if-nez v6, :cond_17

    .line 83
    :cond_13
    invoke-static {}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->instance()Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;

    move-result-object v6

    const/4 v7, -0x1

    move-object/from16 v8, p1

    invoke-virtual {v6, v8, v7, v1}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->addBulletNumber(Lcom/intsig/office/system/IControl;ILcom/intsig/office/fc/dom4j/Element;)I

    move-result v1

    if-ne v1, v7, :cond_14

    if-eqz v3, :cond_14

    .line 84
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->getPGParaBulletID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    move-result v1

    :cond_14
    if-ne v1, v7, :cond_15

    if-ltz v4, :cond_15

    .line 85
    invoke-static {}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->instance()Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->getBulletID(I)I

    move-result v1

    :cond_15
    if-ne v1, v7, :cond_16

    if-lez v5, :cond_16

    if-nez p9, :cond_16

    .line 86
    invoke-static {}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->instance()Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->getBulletID(I)I

    move-result v1

    :cond_16
    if-ltz v1, :cond_17

    .line 87
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPGParaBulletID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_17
    if-lez v5, :cond_18

    .line 88
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v1

    invoke-virtual {v1, v2, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    :cond_18
    return-void
.end method

.method public setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x1004

    .line 8
    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x1006

    .line 8
    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x1001

    .line 8
    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x1003

    .line 8
    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x100a

    .line 8
    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-virtual {v0, p2, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    .line 29
    .line 30
    :cond_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const/16 v1, 0x1009

    .line 35
    .line 36
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;)F

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    .line 55
    .line 56
    .line 57
    :cond_1
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x1008

    .line 8
    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;IIZ)V
    .locals 1

    .line 1
    if-gez p3, :cond_0

    .line 2
    .line 3
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-le v0, p2, :cond_0

    .line 8
    .line 9
    neg-int p3, p2

    .line 10
    :cond_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0, p1, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 15
    .line 16
    .line 17
    if-eqz p4, :cond_1

    .line 18
    .line 19
    if-gez p3, :cond_1

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 22
    .line 23
    .line 24
    move-result-object p4

    .line 25
    add-int/2addr p2, p3

    .line 26
    invoke-virtual {p4, p1, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 27
    .line 28
    .line 29
    :cond_1
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

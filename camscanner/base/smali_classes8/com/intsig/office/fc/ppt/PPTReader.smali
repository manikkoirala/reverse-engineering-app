.class public Lcom/intsig/office/fc/ppt/PPTReader;
.super Lcom/intsig/office/system/AbstractReader;
.source "PPTReader.java"


# static fields
.field public static final DEFAULT_CELL_HEIGHT:I = 0x28

.field public static final DEFAULT_CELL_WIDTH:I = 0x64

.field public static final FIRST_READ_SLIDE_NUM:I = 0x2

.field public static final POINT_PER_LINE_PER_FONTSIZE:F = 1.2f


# instance fields
.field private currentReaderIndex:I

.field private filePath:Ljava/lang/String;

.field private hasProcessedMasterDateTime:Z

.field private hasProcessedMasterFooter:Z

.field private hasProcessedMasterSlideNumber:Z

.field private isGetThumbnail:Z

.field private maxFontSize:I

.field private model:Lcom/intsig/office/pg/model/PGModel;

.field private number:I

.field private offset:I

.field private poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

.field private poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

.field private slideMasterIndexs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private tableShape:Z

.field private titleMasterIndexs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/intsig/office/fc/ppt/PPTReader;-><init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;ZLandroid/net/Uri;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;ZLandroid/net/Uri;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/system/AbstractReader;-><init>()V

    const/4 v0, 0x1

    .line 3
    iput v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->number:I

    .line 4
    iput-object p2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->filePath:Ljava/lang/String;

    .line 5
    iput-object p1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 6
    iput-boolean p3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->isGetThumbnail:Z

    .line 7
    iput-object p4, p0, Lcom/intsig/office/fc/ppt/PPTReader;->uri:Landroid/net/Uri;

    return-void
.end method

.method private converFill(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Fill;)Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz p2, :cond_12

    .line 5
    .line 6
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getFillType()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    const/16 v3, 0x9

    .line 11
    .line 12
    if-ne v2, v3, :cond_0

    .line 13
    .line 14
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/pg/model/PGSlide;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    goto/16 :goto_6

    .line 19
    .line 20
    :cond_0
    const/4 v3, 0x0

    .line 21
    if-nez v2, :cond_1

    .line 22
    .line 23
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    if-eqz v2, :cond_12

    .line 28
    .line 29
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 30
    .line 31
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 35
    .line 36
    .line 37
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->converterColor(Lcom/intsig/office/java/awt/Color;)I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 46
    .line 47
    .line 48
    goto/16 :goto_6

    .line 49
    .line 50
    :cond_1
    const/4 v4, 0x1

    .line 51
    const/4 v5, 0x6

    .line 52
    const/4 v6, 0x5

    .line 53
    const/4 v7, 0x4

    .line 54
    const/4 v8, 0x7

    .line 55
    const/4 v9, 0x2

    .line 56
    if-eq v2, v8, :cond_5

    .line 57
    .line 58
    if-eq v2, v7, :cond_5

    .line 59
    .line 60
    if-eq v2, v6, :cond_5

    .line 61
    .line 62
    if-ne v2, v5, :cond_2

    .line 63
    .line 64
    goto/16 :goto_0

    .line 65
    .line 66
    :cond_2
    if-ne v2, v9, :cond_3

    .line 67
    .line 68
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 69
    .line 70
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v9}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 74
    .line 75
    .line 76
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getPictureData()Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    if-eqz v2, :cond_12

    .line 81
    .line 82
    iget-object v4, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 83
    .line 84
    invoke-interface {v4}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 85
    .line 86
    .line 87
    move-result-object v4

    .line 88
    invoke-virtual {v4}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 89
    .line 90
    .line 91
    move-result-object v4

    .line 92
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/hslf/usermodel/PictureData;)I

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    new-instance v4, Lcom/intsig/office/common/bg/TileShader;

    .line 97
    .line 98
    iget-object v5, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 99
    .line 100
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 101
    .line 102
    .line 103
    move-result-object v5

    .line 104
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 105
    .line 106
    .line 107
    move-result-object v5

    .line 108
    invoke-virtual {v5, v2}, Lcom/intsig/office/common/picture/PictureManage;->getPicture(I)Lcom/intsig/office/common/picture/Picture;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    const/high16 v5, 0x3f800000    # 1.0f

    .line 113
    .line 114
    invoke-direct {v4, v2, v3, v5, v5}, Lcom/intsig/office/common/bg/TileShader;-><init>(Lcom/intsig/office/common/picture/Picture;IFF)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 118
    .line 119
    .line 120
    goto/16 :goto_6

    .line 121
    .line 122
    :cond_3
    const/4 v5, 0x3

    .line 123
    if-ne v2, v5, :cond_4

    .line 124
    .line 125
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getPictureData()Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 126
    .line 127
    .line 128
    move-result-object v2

    .line 129
    if-eqz v2, :cond_12

    .line 130
    .line 131
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 132
    .line 133
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 137
    .line 138
    .line 139
    iget-object v3, v0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 140
    .line 141
    invoke-interface {v3}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 142
    .line 143
    .line 144
    move-result-object v3

    .line 145
    invoke-virtual {v3}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 146
    .line 147
    .line 148
    move-result-object v3

    .line 149
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/hslf/usermodel/PictureData;)I

    .line 150
    .line 151
    .line 152
    move-result v2

    .line 153
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setPictureIndex(I)V

    .line 154
    .line 155
    .line 156
    goto/16 :goto_6

    .line 157
    .line 158
    :cond_4
    if-ne v2, v4, :cond_12

    .line 159
    .line 160
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getFillbackColor()Lcom/intsig/office/java/awt/Color;

    .line 161
    .line 162
    .line 163
    move-result-object v2

    .line 164
    if-eqz v2, :cond_12

    .line 165
    .line 166
    new-instance v1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 167
    .line 168
    invoke-direct {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 169
    .line 170
    .line 171
    invoke-virtual {v1, v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 172
    .line 173
    .line 174
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getFillbackColor()Lcom/intsig/office/java/awt/Color;

    .line 175
    .line 176
    .line 177
    move-result-object v2

    .line 178
    invoke-direct {v0, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->converterColor(Lcom/intsig/office/java/awt/Color;)I

    .line 179
    .line 180
    .line 181
    move-result v2

    .line 182
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 183
    .line 184
    .line 185
    goto/16 :goto_6

    .line 186
    .line 187
    :cond_5
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getFillAngle()I

    .line 188
    .line 189
    .line 190
    move-result v10

    .line 191
    const/16 v11, -0x87

    .line 192
    .line 193
    if-eq v10, v11, :cond_8

    .line 194
    .line 195
    const/16 v11, -0x5a

    .line 196
    .line 197
    if-eq v10, v11, :cond_7

    .line 198
    .line 199
    const/16 v11, -0x2d

    .line 200
    .line 201
    if-eq v10, v11, :cond_6

    .line 202
    .line 203
    if-eqz v10, :cond_7

    .line 204
    .line 205
    goto :goto_1

    .line 206
    :cond_6
    const/16 v10, 0x87

    .line 207
    .line 208
    goto :goto_1

    .line 209
    :cond_7
    add-int/lit8 v10, v10, 0x5a

    .line 210
    .line 211
    goto :goto_1

    .line 212
    :cond_8
    const/16 v10, 0x2d

    .line 213
    .line 214
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getFillFocus()I

    .line 215
    .line 216
    .line 217
    move-result v11

    .line 218
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 219
    .line 220
    .line 221
    move-result-object v12

    .line 222
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getFillbackColor()Lcom/intsig/office/java/awt/Color;

    .line 223
    .line 224
    .line 225
    move-result-object v13

    .line 226
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->isShaderPreset()Z

    .line 227
    .line 228
    .line 229
    move-result v14

    .line 230
    if-eqz v14, :cond_9

    .line 231
    .line 232
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getShaderColors()[I

    .line 233
    .line 234
    .line 235
    move-result-object v14

    .line 236
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getShaderPositions()[F

    .line 237
    .line 238
    .line 239
    move-result-object v15

    .line 240
    goto :goto_2

    .line 241
    :cond_9
    move-object v14, v1

    .line 242
    move-object v15, v14

    .line 243
    :goto_2
    if-nez v14, :cond_c

    .line 244
    .line 245
    new-array v14, v9, [I

    .line 246
    .line 247
    const/16 v16, -0x1

    .line 248
    .line 249
    if-nez v12, :cond_a

    .line 250
    .line 251
    const/4 v12, -0x1

    .line 252
    goto :goto_3

    .line 253
    :cond_a
    invoke-virtual {v12}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 254
    .line 255
    .line 256
    move-result v12

    .line 257
    :goto_3
    aput v12, v14, v3

    .line 258
    .line 259
    if-nez v13, :cond_b

    .line 260
    .line 261
    goto :goto_4

    .line 262
    :cond_b
    invoke-virtual {v13}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 263
    .line 264
    .line 265
    move-result v16

    .line 266
    :goto_4
    aput v16, v14, v4

    .line 267
    .line 268
    :cond_c
    if-nez v15, :cond_d

    .line 269
    .line 270
    new-array v15, v9, [F

    .line 271
    .line 272
    fill-array-data v15, :array_0

    .line 273
    .line 274
    .line 275
    :cond_d
    if-ne v2, v8, :cond_e

    .line 276
    .line 277
    new-instance v1, Lcom/intsig/office/common/bg/LinearGradientShader;

    .line 278
    .line 279
    int-to-float v3, v10

    .line 280
    invoke-direct {v1, v3, v14, v15}, Lcom/intsig/office/common/bg/LinearGradientShader;-><init>(F[I[F)V

    .line 281
    .line 282
    .line 283
    goto :goto_5

    .line 284
    :cond_e
    if-eq v2, v7, :cond_f

    .line 285
    .line 286
    if-eq v2, v6, :cond_f

    .line 287
    .line 288
    if-ne v2, v5, :cond_10

    .line 289
    .line 290
    :cond_f
    new-instance v1, Lcom/intsig/office/common/bg/RadialGradientShader;

    .line 291
    .line 292
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Fill;->getRadialGradientPositionType()I

    .line 293
    .line 294
    .line 295
    move-result v3

    .line 296
    invoke-direct {v1, v3, v14, v15}, Lcom/intsig/office/common/bg/RadialGradientShader;-><init>(I[I[F)V

    .line 297
    .line 298
    .line 299
    :cond_10
    :goto_5
    if-eqz v1, :cond_11

    .line 300
    .line 301
    invoke-virtual {v1, v11}, Lcom/intsig/office/common/bg/Gradient;->setFocus(I)V

    .line 302
    .line 303
    .line 304
    :cond_11
    new-instance v3, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 305
    .line 306
    invoke-direct {v3}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 307
    .line 308
    .line 309
    int-to-byte v2, v2

    .line 310
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 311
    .line 312
    .line 313
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setShader(Lcom/intsig/office/common/bg/AShader;)V

    .line 314
    .line 315
    .line 316
    move-object v1, v3

    .line 317
    :cond_12
    :goto_6
    return-object v1

    .line 318
    nop

    .line 319
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private converterColor(Lcom/intsig/office/java/awt/Color;)I
    .locals 0

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getBorderColor(Lcom/intsig/office/fc/hslf/model/Line;)I
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ppt/PPTReader;->converterColor(Lcom/intsig/office/java/awt/Color;)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1

    .line 14
    :cond_0
    const/high16 p1, -0x1000000

    .line 15
    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getGroupShapeID(ILjava/util/Map;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;)I"
        }
    .end annotation

    .line 1
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Ljava/lang/Integer;

    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Ljava/util/List;

    .line 34
    .line 35
    if-eqz v2, :cond_0

    .line 36
    .line 37
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    if-eqz v2, :cond_0

    .line 46
    .line 47
    return v1

    .line 48
    :cond_1
    const/4 p1, -0x1

    .line 49
    return p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private getParaIndex(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;)I
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGSlide;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    array-length v0, p1

    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    :goto_0
    if-ge v2, v0, :cond_3

    .line 9
    .line 10
    aget-object v3, p1, v2

    .line 11
    .line 12
    instance-of v4, v3, Lcom/intsig/office/common/shape/TextBox;

    .line 13
    .line 14
    if-eqz v4, :cond_2

    .line 15
    .line 16
    invoke-interface {v3}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->getTargetElementID()I

    .line 21
    .line 22
    .line 23
    move-result v4

    .line 24
    if-ne v3, v4, :cond_2

    .line 25
    .line 26
    aget-object p1, p1, v2

    .line 27
    .line 28
    check-cast p1, Lcom/intsig/office/common/shape/TextBox;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const-wide/16 v2, 0x0

    .line 35
    .line 36
    invoke-virtual {p1, v2, v3}, Lcom/intsig/office/simpletext/model/SectionElement;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    check-cast v0, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 41
    .line 42
    :goto_1
    if-eqz v0, :cond_3

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 45
    .line 46
    .line 47
    move-result-wide v2

    .line 48
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 49
    .line 50
    .line 51
    move-result-wide v4

    .line 52
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->getData1()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    int-to-long v6, v0

    .line 57
    cmp-long v0, v4, v6

    .line 58
    .line 59
    if-nez v0, :cond_1

    .line 60
    .line 61
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->getData2()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    int-to-long v4, v0

    .line 66
    cmp-long v0, v2, v4

    .line 67
    .line 68
    if-eqz v0, :cond_0

    .line 69
    .line 70
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->getData2()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    add-int/lit8 v0, v0, -0x1

    .line 75
    .line 76
    int-to-long v4, v0

    .line 77
    cmp-long v0, v2, v4

    .line 78
    .line 79
    if-nez v0, :cond_1

    .line 80
    .line 81
    :cond_0
    return v1

    .line 82
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 83
    .line 84
    invoke-virtual {p1, v2, v3}, Lcom/intsig/office/simpletext/model/SectionElement;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    check-cast v0, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 89
    .line 90
    goto :goto_1

    .line 91
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_3
    const/4 p1, -0x2

    .line 95
    return p1
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;)Lcom/intsig/office/common/borders/Line;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/ppt/PPTReader;->getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;Z)Lcom/intsig/office/common/borders/Line;

    move-result-object p1

    return-object p1
.end method

.method private getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;Z)Lcom/intsig/office/common/borders/Line;
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->hasLine()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getLineWidth()D

    move-result-wide v1

    const-wide v3, 0x3ff5555560000000L    # 1.3333333730697632

    mul-double v1, v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int p2, v1

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getLineDashing()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 5
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 6
    new-instance v0, Lcom/intsig/office/common/borders/Line;

    invoke-direct {v0}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 7
    new-instance v2, Lcom/intsig/office/common/bg/BackgroundAndFill;

    invoke-direct {v2}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ppt/PPTReader;->converterColor(Lcom/intsig/office/java/awt/Color;)I

    move-result p1

    invoke-virtual {v2, p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 9
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/borders/Line;->setDash(Z)V

    .line 11
    invoke-virtual {v0, p2}, Lcom/intsig/office/common/borders/Border;->setLineWidth(I)V

    goto :goto_1

    :cond_1
    if-eqz p2, :cond_2

    .line 12
    new-instance v0, Lcom/intsig/office/common/borders/Line;

    invoke-direct {v0}, Lcom/intsig/office/common/borders/Line;-><init>()V

    .line 13
    new-instance p1, Lcom/intsig/office/common/bg/BackgroundAndFill;

    invoke-direct {p1}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    const/high16 p2, -0x1000000

    .line 14
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 15
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/borders/Line;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    :cond_2
    :goto_1
    return-object v0
.end method

.method private isTitleSlide(Lcom/intsig/office/fc/hslf/model/Slide;)Z
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getSSlideLayoutAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom$SSlideLayoutAtom;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getSSlideLayoutAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom$SSlideLayoutAtom;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/SlideAtom$SSlideLayoutAtom;->getGeometryType()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 v0, 0x0

    .line 28
    :goto_0
    const/4 v2, 0x1

    .line 29
    if-nez v0, :cond_1

    .line 30
    .line 31
    return v2

    .line 32
    :cond_1
    const/16 v3, 0x10

    .line 33
    .line 34
    if-ne v0, v3, :cond_5

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    array-length v0, p1

    .line 41
    const/4 v4, 0x0

    .line 42
    :goto_1
    if-ge v4, v0, :cond_4

    .line 43
    .line 44
    aget-object v5, p1, v4

    .line 45
    .line 46
    instance-of v6, v5, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 47
    .line 48
    if-nez v6, :cond_2

    .line 49
    .line 50
    return v1

    .line 51
    :cond_2
    check-cast v5, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 52
    .line 53
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/model/TextShape;->getPlaceholderAtom()Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;

    .line 54
    .line 55
    .line 56
    move-result-object v5

    .line 57
    if-eqz v5, :cond_3

    .line 58
    .line 59
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;->getPlaceholderId()I

    .line 60
    .line 61
    .line 62
    move-result v5

    .line 63
    const/16 v6, 0xf

    .line 64
    .line 65
    if-eq v5, v6, :cond_3

    .line 66
    .line 67
    if-eq v5, v3, :cond_3

    .line 68
    .line 69
    const/4 v6, -0x1

    .line 70
    if-eq v5, v6, :cond_3

    .line 71
    .line 72
    return v1

    .line 73
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_4
    return v2

    .line 77
    :cond_5
    return v1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private processAnimation(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/office/pg/model/PGSlide;",
            "Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/office/pg/animate/ShapeAnimation;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    .line 3
    .line 4
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    if-nez p2, :cond_0

    .line 12
    .line 13
    return-object v0

    .line 14
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .line 18
    .line 19
    const/4 v3, 0x0

    .line 20
    const/4 v4, 0x0

    .line 21
    :goto_0
    array-length v5, p2

    .line 22
    if-ge v4, v5, :cond_2

    .line 23
    .line 24
    aget-object v5, p2, v4

    .line 25
    .line 26
    instance-of v6, v5, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 27
    .line 28
    if-eqz v6, :cond_1

    .line 29
    .line 30
    check-cast v5, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 31
    .line 32
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    const/4 v4, 0x1

    .line 43
    if-le p2, v4, :cond_4

    .line 44
    .line 45
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    :cond_3
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    if-eqz v2, :cond_9

    .line 54
    .line 55
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    check-cast v2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 60
    .line 61
    sget-wide v3, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;->RECORD_ID:J

    .line 62
    .line 63
    invoke-virtual {v2, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    if-eqz v2, :cond_3

    .line 68
    .line 69
    check-cast v2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 70
    .line 71
    invoke-direct {p0, p1, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->processSingleAnimation(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;)Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    if-eqz v2, :cond_3

    .line 76
    .line 77
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    goto :goto_1

    .line 81
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 82
    .line 83
    .line 84
    move-result p2

    .line 85
    if-ne p2, v4, :cond_9

    .line 86
    .line 87
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 88
    .line 89
    .line 90
    move-result-object p2

    .line 91
    check-cast p2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 92
    .line 93
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 94
    .line 95
    .line 96
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 97
    .line 98
    .line 99
    move-result-object p2

    .line 100
    const/4 v5, 0x0

    .line 101
    :goto_2
    array-length v6, p2

    .line 102
    if-ge v5, v6, :cond_6

    .line 103
    .line 104
    aget-object v6, p2, v5

    .line 105
    .line 106
    instance-of v7, v6, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 107
    .line 108
    if-eqz v7, :cond_5

    .line 109
    .line 110
    check-cast v6, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 111
    .line 112
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    :cond_5
    add-int/lit8 v5, v5, 0x1

    .line 116
    .line 117
    goto :goto_2

    .line 118
    :cond_6
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 119
    .line 120
    .line 121
    move-result p2

    .line 122
    if-ne p2, v4, :cond_7

    .line 123
    .line 124
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 125
    .line 126
    .line 127
    move-result-object p2

    .line 128
    check-cast p2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 129
    .line 130
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/ppt/PPTReader;->processSingleAnimation(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;)Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 131
    .line 132
    .line 133
    move-result-object p1

    .line 134
    if-eqz p1, :cond_9

    .line 135
    .line 136
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    .line 138
    .line 139
    goto :goto_4

    .line 140
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 141
    .line 142
    .line 143
    move-result p2

    .line 144
    if-le p2, v4, :cond_9

    .line 145
    .line 146
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 147
    .line 148
    .line 149
    move-result-object p2

    .line 150
    :cond_8
    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    if-eqz v2, :cond_9

    .line 155
    .line 156
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 157
    .line 158
    .line 159
    move-result-object v2

    .line 160
    check-cast v2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 161
    .line 162
    invoke-direct {p0, p1, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->processSingleAnimation(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;)Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 163
    .line 164
    .line 165
    move-result-object v2

    .line 166
    if-eqz v2, :cond_8

    .line 167
    .line 168
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    .line 170
    .line 171
    goto :goto_3

    .line 172
    :cond_9
    :goto_4
    return-object v1

    .line 173
    :catch_0
    return-object v0
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processCurrentSlideHeadersFooters(Lcom/intsig/office/common/shape/TextBox;Ljava/lang/String;)Lcom/intsig/office/common/shape/TextBox;
    .locals 9

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_1

    .line 3
    .line 4
    if-eqz p2, :cond_1

    .line 5
    .line 6
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-lez v1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 23
    .line 24
    .line 25
    move-result-wide v1

    .line 26
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 31
    .line 32
    .line 33
    move-result-wide v3

    .line 34
    sub-long/2addr v1, v3

    .line 35
    const-wide/16 v3, 0x0

    .line 36
    .line 37
    cmp-long v5, v1, v3

    .line 38
    .line 39
    if-lez v5, :cond_1

    .line 40
    .line 41
    new-instance v1, Lcom/intsig/office/common/shape/TextBox;

    .line 42
    .line 43
    invoke-direct {v1}, Lcom/intsig/office/common/shape/TextBox;-><init>()V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/TextBox;->isWrapLine()Z

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/shape/TextBox;->setWrapLine(Z)V

    .line 58
    .line 59
    .line 60
    new-instance v2, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 61
    .line 62
    invoke-direct {v2}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 69
    .line 70
    .line 71
    move-result v5

    .line 72
    int-to-long v5, v5

    .line 73
    invoke-virtual {v2, v5, v6}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 77
    .line 78
    .line 79
    move-result-object v5

    .line 80
    invoke-virtual {v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 81
    .line 82
    .line 83
    move-result-object v5

    .line 84
    invoke-interface {v5}, Lcom/intsig/office/simpletext/model/IAttributeSet;->clone()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 85
    .line 86
    .line 87
    move-result-object v5

    .line 88
    invoke-virtual {v2, v5}, Lcom/intsig/office/simpletext/model/AbstractElement;->setAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1, v2}, Lcom/intsig/office/common/shape/TextBox;->setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/SectionElement;->getParaCollection()Lcom/intsig/office/simpletext/model/IElementCollection;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    const/4 v5, 0x0

    .line 103
    invoke-interface {p1, v5}, Lcom/intsig/office/simpletext/model/IElementCollection;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    check-cast p1, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 108
    .line 109
    new-instance v6, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 110
    .line 111
    invoke-direct {v6}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v6, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 118
    .line 119
    .line 120
    move-result v7

    .line 121
    int-to-long v7, v7

    .line 122
    invoke-virtual {v6, v7, v8}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 126
    .line 127
    .line 128
    move-result-object v7

    .line 129
    invoke-interface {v7}, Lcom/intsig/office/simpletext/model/IAttributeSet;->clone()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 130
    .line 131
    .line 132
    move-result-object v7

    .line 133
    invoke-virtual {v6, v7}, Lcom/intsig/office/simpletext/model/AbstractElement;->setAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v2, v6, v3, v4}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 137
    .line 138
    .line 139
    invoke-virtual {p1, v5}, Lcom/intsig/office/simpletext/model/ParagraphElement;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    check-cast p1, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 144
    .line 145
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/model/LeafElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    .line 151
    const-string v2, "*"

    .line 152
    .line 153
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 154
    .line 155
    .line 156
    move-result v5

    .line 157
    if-eqz v5, :cond_0

    .line 158
    .line 159
    invoke-virtual {v0, v2, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object p2

    .line 163
    :cond_0
    new-instance v0, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 164
    .line 165
    invoke-direct {v0, p2}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    invoke-virtual {v0, v3, v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 169
    .line 170
    .line 171
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 172
    .line 173
    .line 174
    move-result p2

    .line 175
    int-to-long v2, p2

    .line 176
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 177
    .line 178
    .line 179
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 180
    .line 181
    .line 182
    move-result-object p1

    .line 183
    invoke-interface {p1}, Lcom/intsig/office/simpletext/model/IAttributeSet;->clone()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;)V

    .line 188
    .line 189
    .line 190
    invoke-virtual {v6, v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 191
    .line 192
    .line 193
    return-object v1

    .line 194
    :cond_1
    return-object v0
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processGroupShape(Lcom/intsig/office/pg/model/PGSlide;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGSlide;->getGroupShape()Ljava/util/Map;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_0
    if-ge v2, v1, :cond_1

    .line 14
    .line 15
    invoke-virtual {p1, v2}, Lcom/intsig/office/pg/model/PGSlide;->getShape(I)Lcom/intsig/office/common/shape/IShape;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    invoke-interface {v3}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    invoke-direct {p0, v4, v0}, Lcom/intsig/office/fc/ppt/PPTReader;->getGroupShapeID(ILjava/util/Map;)I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    invoke-interface {v3, v4}, Lcom/intsig/office/common/shape/IShape;->setGroupShapeID(I)V

    .line 28
    .line 29
    .line 30
    add-int/lit8 v2, v2, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private processNormalTextShape(Lcom/intsig/office/common/shape/TextBox;Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/java/awt/Rectangle;II)V
    .locals 15

    .line 1
    move-object v8, p0

    .line 2
    move-object/from16 v0, p3

    .line 3
    .line 4
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getText()Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v9

    .line 8
    if-eqz v9, :cond_6

    .line 9
    .line 10
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-lez v1, :cond_6

    .line 19
    .line 20
    new-instance v10, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 21
    .line 22
    invoke-direct {v10}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 23
    .line 24
    .line 25
    move-object/from16 v1, p1

    .line 26
    .line 27
    invoke-virtual {v1, v10}, Lcom/intsig/office/common/shape/TextBox;->setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v10}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    iget v3, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 39
    .line 40
    int-to-float v3, v3

    .line 41
    const/high16 v4, 0x41700000    # 15.0f

    .line 42
    .line 43
    mul-float v3, v3, v4

    .line 44
    .line 45
    float-to-int v3, v3

    .line 46
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 47
    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 54
    .line 55
    int-to-float v0, v0

    .line 56
    mul-float v0, v0, v4

    .line 57
    .line 58
    float-to-int v0, v0

    .line 59
    invoke-virtual {v2, v1, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 60
    .line 61
    .line 62
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginLeft()F

    .line 67
    .line 68
    .line 69
    move-result v2

    .line 70
    const/high16 v3, 0x41a00000    # 20.0f

    .line 71
    .line 72
    mul-float v2, v2, v3

    .line 73
    .line 74
    float-to-int v2, v2

    .line 75
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 76
    .line 77
    .line 78
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginRight()F

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    mul-float v2, v2, v3

    .line 87
    .line 88
    float-to-int v2, v2

    .line 89
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 90
    .line 91
    .line 92
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginTop()F

    .line 97
    .line 98
    .line 99
    move-result v2

    .line 100
    mul-float v2, v2, v3

    .line 101
    .line 102
    float-to-int v2, v2

    .line 103
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 104
    .line 105
    .line 106
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginBottom()F

    .line 111
    .line 112
    .line 113
    move-result v2

    .line 114
    mul-float v2, v2, v3

    .line 115
    .line 116
    float-to-int v2, v2

    .line 117
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 118
    .line 119
    .line 120
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getVerticalAlignment()I

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    const/4 v2, 0x1

    .line 125
    const/4 v3, 0x0

    .line 126
    packed-switch v0, :pswitch_data_0

    .line 127
    .line 128
    .line 129
    :pswitch_0
    const/4 v4, 0x0

    .line 130
    goto :goto_0

    .line 131
    :pswitch_1
    const/4 v4, 0x2

    .line 132
    goto :goto_0

    .line 133
    :pswitch_2
    const/4 v4, 0x1

    .line 134
    :goto_0
    const/4 v5, 0x3

    .line 135
    if-eq v0, v5, :cond_0

    .line 136
    .line 137
    const/16 v5, 0x8

    .line 138
    .line 139
    if-eq v0, v5, :cond_0

    .line 140
    .line 141
    const/4 v5, 0x4

    .line 142
    if-eq v0, v5, :cond_0

    .line 143
    .line 144
    const/4 v5, 0x5

    .line 145
    if-eq v0, v5, :cond_0

    .line 146
    .line 147
    const/16 v5, 0x9

    .line 148
    .line 149
    if-ne v0, v5, :cond_1

    .line 150
    .line 151
    :cond_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 156
    .line 157
    .line 158
    :cond_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    invoke-virtual {v0, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 163
    .line 164
    .line 165
    iput v3, v8, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 166
    .line 167
    int-to-long v0, v3

    .line 168
    invoke-virtual {v10, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 169
    .line 170
    .line 171
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    .line 172
    .line 173
    .line 174
    move-result v11

    .line 175
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getHyperlinks()[Lcom/intsig/office/fc/hslf/model/Hyperlink;

    .line 180
    .line 181
    .line 182
    move-result-object v12

    .line 183
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 184
    .line 185
    .line 186
    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    .line 188
    .line 189
    .line 190
    move-result v0

    .line 191
    const/4 v5, 0x0

    .line 192
    if-eqz v0, :cond_5

    .line 193
    .line 194
    const/4 v13, 0x0

    .line 195
    :goto_1
    if-ge v13, v11, :cond_5

    .line 196
    .line 197
    iget-boolean v0, v8, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 198
    .line 199
    if-eqz v0, :cond_2

    .line 200
    .line 201
    goto :goto_2

    .line 202
    :cond_2
    invoke-virtual {v9, v13}, Ljava/lang/String;->charAt(I)C

    .line 203
    .line 204
    .line 205
    move-result v0

    .line 206
    const/16 v1, 0xa

    .line 207
    .line 208
    if-ne v0, v1, :cond_4

    .line 209
    .line 210
    add-int/lit8 v14, v13, 0x1

    .line 211
    .line 212
    if-lt v14, v11, :cond_3

    .line 213
    .line 214
    goto :goto_2

    .line 215
    :cond_3
    move-object v0, p0

    .line 216
    move-object v1, v10

    .line 217
    move-object/from16 v2, p2

    .line 218
    .line 219
    move-object v3, v9

    .line 220
    move-object v4, v12

    .line 221
    move v6, v14

    .line 222
    move/from16 v7, p5

    .line 223
    .line 224
    invoke-direct/range {v0 .. v7}, Lcom/intsig/office/fc/ppt/PPTReader;->processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hslf/model/TextShape;Ljava/lang/String;[Lcom/intsig/office/fc/hslf/model/Hyperlink;III)V

    .line 225
    .line 226
    .line 227
    move v5, v14

    .line 228
    :cond_4
    add-int/lit8 v13, v13, 0x1

    .line 229
    .line 230
    goto :goto_1

    .line 231
    :cond_5
    :goto_2
    move-object v0, p0

    .line 232
    move-object v1, v10

    .line 233
    move-object/from16 v2, p2

    .line 234
    .line 235
    move-object v3, v9

    .line 236
    move-object v4, v12

    .line 237
    move v6, v11

    .line 238
    move/from16 v7, p5

    .line 239
    .line 240
    invoke-direct/range {v0 .. v7}, Lcom/intsig/office/fc/ppt/PPTReader;->processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hslf/model/TextShape;Ljava/lang/String;[Lcom/intsig/office/fc/hslf/model/Hyperlink;III)V

    .line 241
    .line 242
    .line 243
    iget v0, v8, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 244
    .line 245
    int-to-long v0, v0

    .line 246
    invoke-virtual {v10, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 247
    .line 248
    .line 249
    invoke-static {}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->instance()Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;

    .line 250
    .line 251
    .line 252
    move-result-object v0

    .line 253
    invoke-virtual {v0}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->clearData()V

    .line 254
    .line 255
    .line 256
    :cond_6
    return-void

    .line 257
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private processNotes(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Notes;)V
    .locals 6

    .line 1
    if-eqz p2, :cond_4

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    array-length v0, p2

    .line 8
    const-string v1, ""

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    :goto_0
    if-ge v2, v0, :cond_3

    .line 12
    .line 13
    aget-object v3, p2, v2

    .line 14
    .line 15
    iget-boolean v4, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 16
    .line 17
    if-eqz v4, :cond_0

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    instance-of v4, v3, Lcom/intsig/office/fc/hslf/model/AutoShape;

    .line 21
    .line 22
    if-nez v4, :cond_1

    .line 23
    .line 24
    instance-of v4, v3, Lcom/intsig/office/fc/hslf/model/TextBox;

    .line 25
    .line 26
    if-eqz v4, :cond_2

    .line 27
    .line 28
    :cond_1
    check-cast v3, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 29
    .line 30
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TextShape;->getPlaceholderAtom()Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    if-eqz v4, :cond_2

    .line 35
    .line 36
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;->getPlaceholderId()I

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    const/16 v5, 0xc

    .line 41
    .line 42
    if-ne v4, v5, :cond_2

    .line 43
    .line 44
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TextShape;->getText()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    if-eqz v3, :cond_2

    .line 49
    .line 50
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    if-lez v4, :cond_2

    .line 55
    .line 56
    new-instance v4, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    .line 72
    .line 73
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const/16 v1, 0xa

    .line 80
    .line 81
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_3
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object p2

    .line 95
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 96
    .line 97
    .line 98
    move-result p2

    .line 99
    if-lez p2, :cond_4

    .line 100
    .line 101
    new-instance p2, Lcom/intsig/office/pg/model/PGNotes;

    .line 102
    .line 103
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    invoke-direct {p2, v0}, Lcom/intsig/office/pg/model/PGNotes;-><init>(Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/model/PGSlide;->setNotes(Lcom/intsig/office/pg/model/PGNotes;)V

    .line 111
    .line 112
    .line 113
    :cond_4
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/hslf/model/TextShape;Ljava/lang/String;[Lcom/intsig/office/fc/hslf/model/Hyperlink;III)V
    .locals 26

    .line 1
    move-object/from16 v9, p0

    .line 2
    .line 3
    move-object/from16 v10, p3

    .line 4
    .line 5
    move-object/from16 v11, p4

    .line 6
    .line 7
    move/from16 v0, p5

    .line 8
    .line 9
    move/from16 v12, p6

    .line 10
    .line 11
    move/from16 v13, p7

    .line 12
    .line 13
    new-instance v14, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 14
    .line 15
    invoke-direct {v14}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 16
    .line 17
    .line 18
    iget v1, v9, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 19
    .line 20
    int-to-long v1, v1

    .line 21
    invoke-virtual {v14, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v14}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 25
    .line 26
    .line 27
    move-result-object v15

    .line 28
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRichTextRunAt(I)Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;

    .line 33
    .line 34
    .line 35
    move-result-object v16

    .line 36
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getAlignment()I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    invoke-virtual {v1, v15, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 45
    .line 46
    .line 47
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getLineSpacing()I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    const/high16 v17, 0x42c80000    # 100.0f

    .line 52
    .line 53
    const/high16 v18, 0x41a00000    # 20.0f

    .line 54
    .line 55
    if-ltz v1, :cond_1

    .line 56
    .line 57
    if-nez v1, :cond_0

    .line 58
    .line 59
    const/16 v1, 0x64

    .line 60
    .line 61
    :cond_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    const/4 v3, 0x5

    .line 66
    invoke-virtual {v2, v15, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 67
    .line 68
    .line 69
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    int-to-float v1, v1

    .line 74
    div-float v1, v1, v17

    .line 75
    .line 76
    invoke-virtual {v2, v15, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_1
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    const/4 v3, 0x4

    .line 85
    invoke-virtual {v2, v15, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 86
    .line 87
    .line 88
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    neg-int v1, v1

    .line 93
    div-int/lit8 v1, v1, 0x8

    .line 94
    .line 95
    int-to-float v1, v1

    .line 96
    mul-float v1, v1, v18

    .line 97
    .line 98
    float-to-int v1, v1

    .line 99
    int-to-float v1, v1

    .line 100
    invoke-virtual {v2, v15, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V

    .line 101
    .line 102
    .line 103
    :goto_0
    iget-boolean v1, v9, Lcom/intsig/office/fc/ppt/PPTReader;->tableShape:Z

    .line 104
    .line 105
    const/4 v8, 0x0

    .line 106
    if-eqz v1, :cond_3

    .line 107
    .line 108
    if-nez v0, :cond_2

    .line 109
    .line 110
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    invoke-virtual {v14}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 115
    .line 116
    .line 117
    move-result-object v2

    .line 118
    invoke-virtual {v1, v2, v8}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 119
    .line 120
    .line 121
    :cond_2
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    .line 122
    .line 123
    .line 124
    move-result v1

    .line 125
    if-ne v12, v1, :cond_3

    .line 126
    .line 127
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-virtual {v14}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 132
    .line 133
    .line 134
    move-result-object v2

    .line 135
    invoke-virtual {v1, v2, v8}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 136
    .line 137
    .line 138
    :cond_3
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getTextOffset()I

    .line 139
    .line 140
    .line 141
    move-result v1

    .line 142
    int-to-float v1, v1

    .line 143
    mul-float v1, v1, v18

    .line 144
    .line 145
    float-to-int v1, v1

    .line 146
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getBulletOffset()I

    .line 147
    .line 148
    .line 149
    move-result v2

    .line 150
    int-to-float v2, v2

    .line 151
    mul-float v2, v2, v18

    .line 152
    .line 153
    float-to-int v2, v2

    .line 154
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getIndentLevel()I

    .line 155
    .line 156
    .line 157
    move-result v21

    .line 158
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 159
    .line 160
    .line 161
    move-result-object v3

    .line 162
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/TextRun;->getTextRuler()Lcom/intsig/office/fc/hslf/record/TextRulerAtom;

    .line 163
    .line 164
    .line 165
    move-result-object v3

    .line 166
    if-eqz v3, :cond_5

    .line 167
    .line 168
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/record/TextRulerAtom;->getBulletOffsets()[I

    .line 169
    .line 170
    .line 171
    move-result-object v4

    .line 172
    aget v4, v4, v21

    .line 173
    .line 174
    const/high16 v5, 0x44100000    # 576.0f

    .line 175
    .line 176
    const/high16 v6, 0x42900000    # 72.0f

    .line 177
    .line 178
    if-ltz v4, :cond_4

    .line 179
    .line 180
    int-to-float v1, v4

    .line 181
    mul-float v1, v1, v6

    .line 182
    .line 183
    div-float/2addr v1, v5

    .line 184
    mul-float v1, v1, v18

    .line 185
    .line 186
    float-to-int v1, v1

    .line 187
    :cond_4
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/record/TextRulerAtom;->getTextOffsets()[I

    .line 188
    .line 189
    .line 190
    move-result-object v3

    .line 191
    aget v3, v3, v21

    .line 192
    .line 193
    if-ltz v3, :cond_5

    .line 194
    .line 195
    int-to-float v2, v3

    .line 196
    mul-float v2, v2, v6

    .line 197
    .line 198
    div-float/2addr v2, v5

    .line 199
    mul-float v2, v2, v18

    .line 200
    .line 201
    float-to-int v2, v2

    .line 202
    :cond_5
    sub-int v3, v2, v1

    .line 203
    .line 204
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 205
    .line 206
    .line 207
    move-result-object v4

    .line 208
    invoke-virtual {v4, v15, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 209
    .line 210
    .line 211
    if-gez v3, :cond_6

    .line 212
    .line 213
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 214
    .line 215
    .line 216
    move-result-object v1

    .line 217
    invoke-virtual {v1, v15, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 218
    .line 219
    .line 220
    goto :goto_1

    .line 221
    :cond_6
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 222
    .line 223
    .line 224
    move-result-object v2

    .line 225
    invoke-virtual {v2, v15, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 226
    .line 227
    .line 228
    :goto_1
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isBullet()Z

    .line 229
    .line 230
    .line 231
    move-result v1

    .line 232
    if-eqz v1, :cond_7

    .line 233
    .line 234
    const-string v1, "\n"

    .line 235
    .line 236
    invoke-virtual {v10, v0, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 237
    .line 238
    .line 239
    move-result-object v2

    .line 240
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 241
    .line 242
    .line 243
    move-result v1

    .line 244
    if-nez v1, :cond_7

    .line 245
    .line 246
    invoke-static {}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->instance()Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;

    .line 247
    .line 248
    .line 249
    move-result-object v19

    .line 250
    iget-object v1, v9, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 251
    .line 252
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 253
    .line 254
    .line 255
    move-result-object v2

    .line 256
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getNumberingType(I)I

    .line 257
    .line 258
    .line 259
    move-result v22

    .line 260
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 261
    .line 262
    .line 263
    move-result-object v2

    .line 264
    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/hslf/model/TextRun;->getNumberingStart(I)I

    .line 265
    .line 266
    .line 267
    move-result v23

    .line 268
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getBulletChar()C

    .line 269
    .line 270
    .line 271
    move-result v24

    .line 272
    move-object/from16 v20, v1

    .line 273
    .line 274
    invoke-virtual/range {v19 .. v24}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->addBulletNumber(Lcom/intsig/office/system/IControl;IIIC)I

    .line 275
    .line 276
    .line 277
    move-result v1

    .line 278
    if-ltz v1, :cond_7

    .line 279
    .line 280
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 281
    .line 282
    .line 283
    move-result-object v2

    .line 284
    invoke-virtual {v2, v15, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPGParaBulletID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 285
    .line 286
    .line 287
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 288
    .line 289
    .line 290
    move-result-object v1

    .line 291
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRunType()I

    .line 292
    .line 293
    .line 294
    move-result v1

    .line 295
    const/4 v7, 0x1

    .line 296
    move v6, v0

    .line 297
    if-nez v1, :cond_8

    .line 298
    .line 299
    const/16 v19, 0x1

    .line 300
    .line 301
    goto :goto_2

    .line 302
    :cond_8
    const/16 v19, 0x0

    .line 303
    .line 304
    :goto_2
    if-ge v6, v12, :cond_15

    .line 305
    .line 306
    iget-boolean v0, v9, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 307
    .line 308
    if-eqz v0, :cond_9

    .line 309
    .line 310
    goto/16 :goto_a

    .line 311
    .line 312
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getTextRun()Lcom/intsig/office/fc/hslf/model/TextRun;

    .line 313
    .line 314
    .line 315
    move-result-object v0

    .line 316
    invoke-virtual {v0, v6}, Lcom/intsig/office/fc/hslf/model/TextRun;->getRichTextRunAt(I)Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;

    .line 317
    .line 318
    .line 319
    move-result-object v20

    .line 320
    if-nez v20, :cond_a

    .line 321
    .line 322
    goto/16 :goto_a

    .line 323
    .line 324
    :cond_a
    invoke-virtual/range {v20 .. v20}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getEndIndex()I

    .line 325
    .line 326
    .line 327
    move-result v0

    .line 328
    if-le v0, v12, :cond_b

    .line 329
    .line 330
    move v5, v12

    .line 331
    goto :goto_3

    .line 332
    :cond_b
    move v5, v0

    .line 333
    :goto_3
    if-eqz v11, :cond_11

    .line 334
    .line 335
    const/4 v0, 0x0

    .line 336
    :goto_4
    array-length v1, v11

    .line 337
    if-ge v0, v1, :cond_10

    .line 338
    .line 339
    aget-object v1, v11, v0

    .line 340
    .line 341
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->getStartIndex()I

    .line 342
    .line 343
    .line 344
    move-result v4

    .line 345
    aget-object v1, v11, v0

    .line 346
    .line 347
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->getEndIndex()I

    .line 348
    .line 349
    .line 350
    move-result v3

    .line 351
    if-lt v4, v6, :cond_d

    .line 352
    .line 353
    if-gt v4, v5, :cond_d

    .line 354
    .line 355
    iget-object v1, v9, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 356
    .line 357
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 358
    .line 359
    .line 360
    move-result-object v1

    .line 361
    invoke-virtual {v1}, Lcom/intsig/office/system/SysKit;->getHyperlinkManage()Lcom/intsig/office/common/hyperlink/HyperlinkManage;

    .line 362
    .line 363
    .line 364
    move-result-object v1

    .line 365
    aget-object v0, v11, v0

    .line 366
    .line 367
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->getAddress()Ljava/lang/String;

    .line 368
    .line 369
    .line 370
    move-result-object v0

    .line 371
    invoke-virtual {v1, v0, v7}, Lcom/intsig/office/common/hyperlink/HyperlinkManage;->addHyperlink(Ljava/lang/String;I)I

    .line 372
    .line 373
    .line 374
    move-result v21

    .line 375
    invoke-virtual {v10, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 376
    .line 377
    .line 378
    move-result-object v22

    .line 379
    const/16 v23, -0x1

    .line 380
    .line 381
    move-object/from16 v0, p0

    .line 382
    .line 383
    move-object/from16 v1, p2

    .line 384
    .line 385
    move-object/from16 v2, v20

    .line 386
    .line 387
    move/from16 v25, v3

    .line 388
    .line 389
    move-object v3, v14

    .line 390
    move/from16 p5, v4

    .line 391
    .line 392
    move-object/from16 v4, v22

    .line 393
    .line 394
    move v12, v5

    .line 395
    move/from16 v5, v23

    .line 396
    .line 397
    move/from16 v7, p5

    .line 398
    .line 399
    const/16 v22, 0x0

    .line 400
    .line 401
    move/from16 v8, v19

    .line 402
    .line 403
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 404
    .line 405
    .line 406
    move/from16 v8, v25

    .line 407
    .line 408
    if-gt v8, v12, :cond_c

    .line 409
    .line 410
    move/from16 v7, p5

    .line 411
    .line 412
    invoke-virtual {v10, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 413
    .line 414
    .line 415
    move-result-object v4

    .line 416
    move-object/from16 v0, p0

    .line 417
    .line 418
    move-object/from16 v1, p2

    .line 419
    .line 420
    move-object/from16 v2, v20

    .line 421
    .line 422
    move-object v3, v14

    .line 423
    move/from16 v5, v21

    .line 424
    .line 425
    move v6, v7

    .line 426
    move v7, v8

    .line 427
    move-object/from16 v23, v15

    .line 428
    .line 429
    move v15, v8

    .line 430
    move/from16 v8, v19

    .line 431
    .line 432
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 433
    .line 434
    .line 435
    move v5, v15

    .line 436
    goto :goto_5

    .line 437
    :cond_c
    move/from16 v7, p5

    .line 438
    .line 439
    move-object/from16 v23, v15

    .line 440
    .line 441
    invoke-virtual {v10, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 442
    .line 443
    .line 444
    move-result-object v4

    .line 445
    move-object/from16 v0, p0

    .line 446
    .line 447
    move-object/from16 v1, p2

    .line 448
    .line 449
    move-object/from16 v2, v20

    .line 450
    .line 451
    move-object v3, v14

    .line 452
    move/from16 v5, v21

    .line 453
    .line 454
    move v6, v7

    .line 455
    move v7, v12

    .line 456
    move/from16 v8, v19

    .line 457
    .line 458
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 459
    .line 460
    .line 461
    move v5, v12

    .line 462
    :goto_5
    move v6, v5

    .line 463
    const/4 v8, 0x1

    .line 464
    const/16 v21, 0x1

    .line 465
    .line 466
    goto/16 :goto_7

    .line 467
    .line 468
    :cond_d
    move v7, v4

    .line 469
    move v12, v5

    .line 470
    move-object/from16 v23, v15

    .line 471
    .line 472
    const/16 v22, 0x0

    .line 473
    .line 474
    move v15, v3

    .line 475
    if-le v6, v7, :cond_f

    .line 476
    .line 477
    if-le v15, v6, :cond_f

    .line 478
    .line 479
    iget-object v1, v9, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 480
    .line 481
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 482
    .line 483
    .line 484
    move-result-object v1

    .line 485
    invoke-virtual {v1}, Lcom/intsig/office/system/SysKit;->getHyperlinkManage()Lcom/intsig/office/common/hyperlink/HyperlinkManage;

    .line 486
    .line 487
    .line 488
    move-result-object v1

    .line 489
    aget-object v0, v11, v0

    .line 490
    .line 491
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Hyperlink;->getAddress()Ljava/lang/String;

    .line 492
    .line 493
    .line 494
    move-result-object v0

    .line 495
    const/4 v8, 0x1

    .line 496
    invoke-virtual {v1, v0, v8}, Lcom/intsig/office/common/hyperlink/HyperlinkManage;->addHyperlink(Ljava/lang/String;I)I

    .line 497
    .line 498
    .line 499
    move-result v5

    .line 500
    if-gt v12, v15, :cond_e

    .line 501
    .line 502
    invoke-virtual {v10, v6, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 503
    .line 504
    .line 505
    move-result-object v4

    .line 506
    move-object/from16 v0, p0

    .line 507
    .line 508
    move-object/from16 v1, p2

    .line 509
    .line 510
    move-object/from16 v2, v20

    .line 511
    .line 512
    move-object v3, v14

    .line 513
    move v7, v12

    .line 514
    const/16 v21, 0x1

    .line 515
    .line 516
    move/from16 v8, v19

    .line 517
    .line 518
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 519
    .line 520
    .line 521
    move v5, v12

    .line 522
    goto :goto_6

    .line 523
    :cond_e
    const/16 v21, 0x1

    .line 524
    .line 525
    invoke-virtual {v10, v6, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 526
    .line 527
    .line 528
    move-result-object v4

    .line 529
    move-object/from16 v0, p0

    .line 530
    .line 531
    move-object/from16 v1, p2

    .line 532
    .line 533
    move-object/from16 v2, v20

    .line 534
    .line 535
    move-object v3, v14

    .line 536
    move v7, v15

    .line 537
    move/from16 v8, v19

    .line 538
    .line 539
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 540
    .line 541
    .line 542
    move v5, v15

    .line 543
    :goto_6
    move v6, v5

    .line 544
    const/4 v8, 0x1

    .line 545
    goto :goto_7

    .line 546
    :cond_f
    const/16 v21, 0x1

    .line 547
    .line 548
    add-int/lit8 v0, v0, 0x1

    .line 549
    .line 550
    move v5, v12

    .line 551
    move-object/from16 v15, v23

    .line 552
    .line 553
    const/4 v7, 0x1

    .line 554
    const/4 v8, 0x0

    .line 555
    move/from16 v12, p6

    .line 556
    .line 557
    goto/16 :goto_4

    .line 558
    .line 559
    :cond_10
    move v12, v5

    .line 560
    move-object/from16 v23, v15

    .line 561
    .line 562
    const/16 v21, 0x1

    .line 563
    .line 564
    const/16 v22, 0x0

    .line 565
    .line 566
    const/4 v8, 0x0

    .line 567
    :goto_7
    if-eqz v8, :cond_12

    .line 568
    .line 569
    goto :goto_9

    .line 570
    :cond_11
    move v12, v5

    .line 571
    move-object/from16 v23, v15

    .line 572
    .line 573
    const/16 v21, 0x1

    .line 574
    .line 575
    const/16 v22, 0x0

    .line 576
    .line 577
    :cond_12
    const/4 v0, 0x7

    .line 578
    if-eq v13, v0, :cond_14

    .line 579
    .line 580
    const/16 v0, 0x9

    .line 581
    .line 582
    if-ne v13, v0, :cond_13

    .line 583
    .line 584
    goto :goto_8

    .line 585
    :cond_13
    invoke-virtual {v10, v6, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 586
    .line 587
    .line 588
    move-result-object v4

    .line 589
    const/4 v5, -0x1

    .line 590
    move-object/from16 v0, p0

    .line 591
    .line 592
    move-object/from16 v1, p2

    .line 593
    .line 594
    move-object/from16 v2, v20

    .line 595
    .line 596
    move-object v3, v14

    .line 597
    move v7, v12

    .line 598
    move/from16 v8, v19

    .line 599
    .line 600
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 601
    .line 602
    .line 603
    move v6, v12

    .line 604
    goto :goto_9

    .line 605
    :cond_14
    :goto_8
    const/4 v5, -0x1

    .line 606
    move-object/from16 v0, p0

    .line 607
    .line 608
    move-object/from16 v1, p2

    .line 609
    .line 610
    move-object/from16 v2, v20

    .line 611
    .line 612
    move-object v3, v14

    .line 613
    move-object/from16 v4, p3

    .line 614
    .line 615
    move v7, v12

    .line 616
    move/from16 v8, v19

    .line 617
    .line 618
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 619
    .line 620
    .line 621
    move/from16 v6, p6

    .line 622
    .line 623
    :goto_9
    move/from16 v12, p6

    .line 624
    .line 625
    move-object/from16 v15, v23

    .line 626
    .line 627
    const/4 v7, 0x1

    .line 628
    const/4 v8, 0x0

    .line 629
    goto/16 :goto_2

    .line 630
    .line 631
    :cond_15
    :goto_a
    move-object/from16 v23, v15

    .line 632
    .line 633
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getSpaceBefore()I

    .line 634
    .line 635
    .line 636
    move-result v0

    .line 637
    const v1, 0x3f99999a    # 1.2f

    .line 638
    .line 639
    .line 640
    if-lez v0, :cond_16

    .line 641
    .line 642
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 643
    .line 644
    .line 645
    move-result-object v2

    .line 646
    int-to-float v0, v0

    .line 647
    div-float v0, v0, v17

    .line 648
    .line 649
    iget v3, v9, Lcom/intsig/office/fc/ppt/PPTReader;->maxFontSize:I

    .line 650
    .line 651
    int-to-float v3, v3

    .line 652
    mul-float v0, v0, v3

    .line 653
    .line 654
    mul-float v0, v0, v1

    .line 655
    .line 656
    mul-float v0, v0, v18

    .line 657
    .line 658
    float-to-int v0, v0

    .line 659
    move-object/from16 v3, v23

    .line 660
    .line 661
    invoke-virtual {v2, v3, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 662
    .line 663
    .line 664
    goto :goto_b

    .line 665
    :cond_16
    move-object/from16 v3, v23

    .line 666
    .line 667
    if-gez v0, :cond_17

    .line 668
    .line 669
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 670
    .line 671
    .line 672
    move-result-object v2

    .line 673
    neg-int v0, v0

    .line 674
    div-int/lit8 v0, v0, 0x8

    .line 675
    .line 676
    int-to-float v0, v0

    .line 677
    mul-float v0, v0, v18

    .line 678
    .line 679
    float-to-int v0, v0

    .line 680
    invoke-virtual {v2, v3, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 681
    .line 682
    .line 683
    :cond_17
    :goto_b
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getSpaceAfter()I

    .line 684
    .line 685
    .line 686
    move-result v0

    .line 687
    if-ltz v0, :cond_18

    .line 688
    .line 689
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 690
    .line 691
    .line 692
    move-result-object v2

    .line 693
    int-to-float v0, v0

    .line 694
    div-float v0, v0, v17

    .line 695
    .line 696
    iget v4, v9, Lcom/intsig/office/fc/ppt/PPTReader;->maxFontSize:I

    .line 697
    .line 698
    int-to-float v4, v4

    .line 699
    mul-float v0, v0, v4

    .line 700
    .line 701
    mul-float v0, v0, v1

    .line 702
    .line 703
    mul-float v0, v0, v18

    .line 704
    .line 705
    float-to-int v0, v0

    .line 706
    invoke-virtual {v2, v3, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 707
    .line 708
    .line 709
    goto :goto_c

    .line 710
    :cond_18
    if-gez v0, :cond_19

    .line 711
    .line 712
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 713
    .line 714
    .line 715
    move-result-object v1

    .line 716
    neg-int v0, v0

    .line 717
    div-int/lit8 v0, v0, 0x8

    .line 718
    .line 719
    int-to-float v0, v0

    .line 720
    mul-float v0, v0, v18

    .line 721
    .line 722
    float-to-int v0, v0

    .line 723
    invoke-virtual {v1, v3, v0}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 724
    .line 725
    .line 726
    :cond_19
    :goto_c
    iget v0, v9, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 727
    .line 728
    int-to-long v0, v0

    .line 729
    invoke-virtual {v14, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 730
    .line 731
    .line 732
    const-wide/16 v0, 0x0

    .line 733
    .line 734
    move-object/from16 v2, p1

    .line 735
    .line 736
    invoke-virtual {v2, v14, v0, v1}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 737
    .line 738
    .line 739
    return-void
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
.end method

.method private processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V
    .locals 19

    .line 1
    move-object/from16 v9, p0

    .line 2
    .line 3
    move/from16 v10, p5

    .line 4
    .line 5
    move/from16 v11, p7

    .line 6
    .line 7
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->getSheet()Lcom/intsig/office/fc/hslf/model/Sheet;

    .line 8
    .line 9
    .line 10
    move-result-object v12

    .line 11
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMetaCharactersType()B

    .line 12
    .line 13
    .line 14
    move-result v13

    .line 15
    const/16 v0, 0xa0

    .line 16
    .line 17
    const/16 v1, 0x20

    .line 18
    .line 19
    move-object/from16 v2, p4

    .line 20
    .line 21
    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v14

    .line 25
    const/4 v15, 0x0

    .line 26
    const/4 v0, 0x0

    .line 27
    if-eqz p8, :cond_2

    .line 28
    .line 29
    const/4 v8, 0x0

    .line 30
    :goto_0
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-ge v8, v1, :cond_1

    .line 35
    .line 36
    invoke-virtual {v14, v8}, Ljava/lang/String;->charAt(I)C

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    const/16 v2, 0xa

    .line 41
    .line 42
    if-ne v1, v2, :cond_0

    .line 43
    .line 44
    invoke-virtual {v14, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    add-int v6, p6, v0

    .line 49
    .line 50
    add-int v16, p6, v8

    .line 51
    .line 52
    const/16 v17, 0x0

    .line 53
    .line 54
    move-object/from16 v0, p0

    .line 55
    .line 56
    move-object/from16 v1, p1

    .line 57
    .line 58
    move-object/from16 v2, p2

    .line 59
    .line 60
    move-object/from16 v3, p3

    .line 61
    .line 62
    move/from16 v5, p5

    .line 63
    .line 64
    move/from16 v7, v16

    .line 65
    .line 66
    move/from16 v18, v8

    .line 67
    .line 68
    move/from16 v8, v17

    .line 69
    .line 70
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 71
    .line 72
    .line 73
    const/16 v0, 0xb

    .line 74
    .line 75
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v4

    .line 79
    add-int/lit8 v7, v16, 0x1

    .line 80
    .line 81
    const/4 v8, 0x0

    .line 82
    move-object/from16 v0, p0

    .line 83
    .line 84
    move/from16 v6, v16

    .line 85
    .line 86
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 87
    .line 88
    .line 89
    add-int/lit8 v0, v18, 0x1

    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_0
    move/from16 v18, v8

    .line 93
    .line 94
    :goto_1
    add-int/lit8 v8, v18, 0x1

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    .line 98
    .line 99
    .line 100
    move-result v1

    .line 101
    if-ge v0, v1, :cond_2

    .line 102
    .line 103
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v4

    .line 111
    add-int v6, p6, v0

    .line 112
    .line 113
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    .line 114
    .line 115
    .line 116
    move-result v0

    .line 117
    add-int v7, p6, v0

    .line 118
    .line 119
    const/4 v8, 0x0

    .line 120
    move-object/from16 v0, p0

    .line 121
    .line 122
    move-object/from16 v1, p1

    .line 123
    .line 124
    move-object/from16 v2, p2

    .line 125
    .line 126
    move-object/from16 v3, p3

    .line 127
    .line 128
    move/from16 v5, p5

    .line 129
    .line 130
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processRun(Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;Lcom/intsig/office/simpletext/model/ParagraphElement;Ljava/lang/String;IIIZ)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    :cond_2
    add-int v0, p6, v0

    .line 138
    .line 139
    iput v15, v9, Lcom/intsig/office/fc/ppt/PPTReader;->maxFontSize:I

    .line 140
    .line 141
    if-gt v11, v0, :cond_3

    .line 142
    .line 143
    return-void

    .line 144
    :cond_3
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    .line 145
    .line 146
    .line 147
    move-result v1

    .line 148
    if-le v1, v11, :cond_4

    .line 149
    .line 150
    invoke-virtual {v14, v0, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v14

    .line 154
    :cond_4
    const-string v0, "*"

    .line 155
    .line 156
    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 157
    .line 158
    .line 159
    move-result v1

    .line 160
    const/4 v2, 0x2

    .line 161
    if-eqz v1, :cond_7

    .line 162
    .line 163
    if-eq v13, v2, :cond_6

    .line 164
    .line 165
    const/4 v1, 0x3

    .line 166
    if-eq v13, v1, :cond_6

    .line 167
    .line 168
    const/4 v1, 0x5

    .line 169
    if-ne v13, v1, :cond_5

    .line 170
    .line 171
    goto :goto_2

    .line 172
    :cond_5
    const/4 v0, 0x4

    .line 173
    if-ne v13, v0, :cond_7

    .line 174
    .line 175
    iget-object v0, v9, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 176
    .line 177
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    if-eqz v0, :cond_7

    .line 182
    .line 183
    iget-object v0, v9, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 184
    .line 185
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v14

    .line 189
    goto :goto_3

    .line 190
    :cond_6
    :goto_2
    invoke-static {}, Lcom/intsig/office/ss/util/format/NumericFormatter;->instance()Lcom/intsig/office/ss/util/format/NumericFormatter;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    new-instance v3, Ljava/util/Date;

    .line 195
    .line 196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 197
    .line 198
    .line 199
    move-result-wide v4

    .line 200
    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 201
    .line 202
    .line 203
    const-string v4, "yyyy/m/d"

    .line 204
    .line 205
    invoke-virtual {v1, v4, v3}, Lcom/intsig/office/ss/util/format/NumericFormatter;->getFormatContents(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v1

    .line 209
    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object v14

    .line 213
    :cond_7
    :goto_3
    new-instance v0, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 214
    .line 215
    invoke-direct {v0, v14}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 219
    .line 220
    .line 221
    move-result-object v1

    .line 222
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontSize()I

    .line 223
    .line 224
    .line 225
    move-result v3

    .line 226
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 227
    .line 228
    .line 229
    move-result-object v4

    .line 230
    if-lez v3, :cond_8

    .line 231
    .line 232
    goto :goto_4

    .line 233
    :cond_8
    const/16 v3, 0x12

    .line 234
    .line 235
    :goto_4
    invoke-virtual {v4, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 236
    .line 237
    .line 238
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontSize()I

    .line 239
    .line 240
    .line 241
    move-result v3

    .line 242
    invoke-virtual {v9, v3}, Lcom/intsig/office/fc/ppt/PPTReader;->setMaxFontSize(I)V

    .line 243
    .line 244
    .line 245
    const-string v3, "\n"

    .line 246
    .line 247
    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 248
    .line 249
    .line 250
    move-result v3

    .line 251
    if-nez v3, :cond_e

    .line 252
    .line 253
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontName()Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object v3

    .line 257
    if-eqz v3, :cond_9

    .line 258
    .line 259
    invoke-static {}, Lcom/intsig/office/simpletext/font/FontTypefaceManage;->instance()Lcom/intsig/office/simpletext/font/FontTypefaceManage;

    .line 260
    .line 261
    .line 262
    move-result-object v3

    .line 263
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontName()Ljava/lang/String;

    .line 264
    .line 265
    .line 266
    move-result-object v4

    .line 267
    invoke-virtual {v3, v4}, Lcom/intsig/office/simpletext/font/FontTypefaceManage;->addFontName(Ljava/lang/String;)I

    .line 268
    .line 269
    .line 270
    move-result v3

    .line 271
    if-ltz v3, :cond_9

    .line 272
    .line 273
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 274
    .line 275
    .line 276
    move-result-object v4

    .line 277
    invoke-virtual {v4, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontName(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 278
    .line 279
    .line 280
    :cond_9
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 281
    .line 282
    .line 283
    move-result-object v3

    .line 284
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getFontColor()Lcom/intsig/office/java/awt/Color;

    .line 285
    .line 286
    .line 287
    move-result-object v4

    .line 288
    invoke-direct {v9, v4}, Lcom/intsig/office/fc/ppt/PPTReader;->converterColor(Lcom/intsig/office/java/awt/Color;)I

    .line 289
    .line 290
    .line 291
    move-result v4

    .line 292
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 293
    .line 294
    .line 295
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 296
    .line 297
    .line 298
    move-result-object v3

    .line 299
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isBold()Z

    .line 300
    .line 301
    .line 302
    move-result v4

    .line 303
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontBold(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 304
    .line 305
    .line 306
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 307
    .line 308
    .line 309
    move-result-object v3

    .line 310
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isItalic()Z

    .line 311
    .line 312
    .line 313
    move-result v4

    .line 314
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontItalic(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 315
    .line 316
    .line 317
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 318
    .line 319
    .line 320
    move-result-object v3

    .line 321
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isUnderlined()Z

    .line 322
    .line 323
    .line 324
    move-result v4

    .line 325
    const/4 v5, 0x1

    .line 326
    if-ne v4, v5, :cond_a

    .line 327
    .line 328
    const/4 v15, 0x1

    .line 329
    :cond_a
    invoke-virtual {v3, v1, v15}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 330
    .line 331
    .line 332
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 333
    .line 334
    .line 335
    move-result-object v3

    .line 336
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->isStrikethrough()Z

    .line 337
    .line 338
    .line 339
    move-result v4

    .line 340
    invoke-virtual {v3, v1, v4}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 341
    .line 342
    .line 343
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/usermodel/RichTextRun;->getSuperscript()I

    .line 344
    .line 345
    .line 346
    move-result v3

    .line 347
    if-eqz v3, :cond_c

    .line 348
    .line 349
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 350
    .line 351
    .line 352
    move-result-object v4

    .line 353
    if-lez v3, :cond_b

    .line 354
    .line 355
    const/4 v2, 0x1

    .line 356
    :cond_b
    invoke-virtual {v4, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontScript(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 357
    .line 358
    .line 359
    :cond_c
    if-ltz v10, :cond_e

    .line 360
    .line 361
    if-eqz v12, :cond_d

    .line 362
    .line 363
    invoke-virtual {v12}, Lcom/intsig/office/fc/hslf/model/Sheet;->getColorScheme()Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;

    .line 364
    .line 365
    .line 366
    move-result-object v2

    .line 367
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/ColorSchemeAtom;->getAccentAndHyperlinkColourRGB()I

    .line 368
    .line 369
    .line 370
    move-result v2

    .line 371
    invoke-static {v2}, Lcom/intsig/office/fc/FCKit;->BGRtoRGB(I)I

    .line 372
    .line 373
    .line 374
    move-result v2

    .line 375
    goto :goto_5

    .line 376
    :cond_d
    const v2, -0xffff01

    .line 377
    .line 378
    .line 379
    :goto_5
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 380
    .line 381
    .line 382
    move-result-object v3

    .line 383
    invoke-virtual {v3, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 384
    .line 385
    .line 386
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 387
    .line 388
    .line 389
    move-result-object v3

    .line 390
    invoke-virtual {v3, v1, v5}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 391
    .line 392
    .line 393
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 394
    .line 395
    .line 396
    move-result-object v3

    .line 397
    invoke-virtual {v3, v1, v2}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontUnderlineColr(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 398
    .line 399
    .line 400
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 401
    .line 402
    .line 403
    move-result-object v2

    .line 404
    invoke-virtual {v2, v1, v10}, Lcom/intsig/office/simpletext/model/AttrManage;->setHyperlinkID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 405
    .line 406
    .line 407
    :cond_e
    iget v1, v9, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 408
    .line 409
    int-to-long v1, v1

    .line 410
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 411
    .line 412
    .line 413
    iget v1, v9, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 414
    .line 415
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    .line 416
    .line 417
    .line 418
    move-result v2

    .line 419
    add-int/2addr v1, v2

    .line 420
    iput v1, v9, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 421
    .line 422
    int-to-long v1, v1

    .line 423
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 424
    .line 425
    .line 426
    move-object/from16 v1, p3

    .line 427
    .line 428
    invoke-virtual {v1, v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 429
    .line 430
    .line 431
    return-void
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
.end method

.method private processShape(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hslf/model/Shape;I)V
    .locals 17

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move-object/from16 v7, p1

    .line 4
    .line 5
    move-object/from16 v8, p2

    .line 6
    .line 7
    move-object/from16 v9, p3

    .line 8
    .line 9
    move/from16 v10, p4

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-boolean v0, v6, Lcom/intsig/office/fc/ppt/PPTReader;->tableShape:Z

    .line 13
    .line 14
    iget-boolean v1, v6, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 15
    .line 16
    if-nez v1, :cond_38

    .line 17
    .line 18
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->isHidden()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_0

    .line 23
    .line 24
    goto/16 :goto_18

    .line 25
    .line 26
    :cond_0
    instance-of v1, v9, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 27
    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    move-object v2, v9

    .line 31
    check-cast v2, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 32
    .line 33
    invoke-virtual {v2, v9}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getClientAnchor2D(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    goto :goto_0

    .line 38
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    :goto_0
    if-nez v2, :cond_2

    .line 43
    .line 44
    return-void

    .line 45
    :cond_2
    new-instance v11, Lcom/intsig/office/java/awt/Rectangle;

    .line 46
    .line 47
    invoke-direct {v11}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 51
    .line 52
    .line 53
    move-result-wide v3

    .line 54
    const-wide v12, 0x3ff5555560000000L    # 1.3333333730697632

    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    mul-double v3, v3, v12

    .line 60
    .line 61
    double-to-int v3, v3

    .line 62
    iput v3, v11, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 63
    .line 64
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 65
    .line 66
    .line 67
    move-result-wide v3

    .line 68
    mul-double v3, v3, v12

    .line 69
    .line 70
    double-to-int v3, v3

    .line 71
    iput v3, v11, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 72
    .line 73
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 74
    .line 75
    .line 76
    move-result-wide v3

    .line 77
    mul-double v3, v3, v12

    .line 78
    .line 79
    double-to-int v3, v3

    .line 80
    iput v3, v11, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 81
    .line 82
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 83
    .line 84
    .line 85
    move-result-wide v2

    .line 86
    mul-double v2, v2, v12

    .line 87
    .line 88
    double-to-int v2, v2

    .line 89
    iput v2, v11, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 90
    .line 91
    instance-of v2, v9, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 92
    .line 93
    const/4 v12, 0x2

    .line 94
    if-eqz v2, :cond_6

    .line 95
    .line 96
    if-ne v10, v12, :cond_4

    .line 97
    .line 98
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getMasterShapeID()I

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/pg/model/PGSlide;->getMasterIndexs()[I

    .line 103
    .line 104
    .line 105
    move-result-object v3

    .line 106
    iget-object v4, v6, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 107
    .line 108
    aget v3, v3, v0

    .line 109
    .line 110
    invoke-virtual {v4, v3}, Lcom/intsig/office/pg/model/PGModel;->getSlideMaster(I)Lcom/intsig/office/pg/model/PGSlide;

    .line 111
    .line 112
    .line 113
    move-result-object v3

    .line 114
    if-eqz v3, :cond_4

    .line 115
    .line 116
    invoke-virtual {v3}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    .line 117
    .line 118
    .line 119
    move-result v4

    .line 120
    const/4 v5, 0x0

    .line 121
    :goto_1
    if-ge v5, v4, :cond_4

    .line 122
    .line 123
    invoke-virtual {v3, v5}, Lcom/intsig/office/pg/model/PGSlide;->getShape(I)Lcom/intsig/office/common/shape/IShape;

    .line 124
    .line 125
    .line 126
    move-result-object v14

    .line 127
    invoke-interface {v14}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    .line 128
    .line 129
    .line 130
    move-result v15

    .line 131
    if-ne v15, v2, :cond_3

    .line 132
    .line 133
    goto :goto_2

    .line 134
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 135
    .line 136
    goto :goto_1

    .line 137
    :cond_4
    const/4 v14, 0x0

    .line 138
    :goto_2
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getFill()Lcom/intsig/office/fc/hslf/model/Fill;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    invoke-direct {v6, v7, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->converFill(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Fill;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 143
    .line 144
    .line 145
    move-result-object v2

    .line 146
    if-nez v2, :cond_5

    .line 147
    .line 148
    if-eqz v14, :cond_5

    .line 149
    .line 150
    instance-of v3, v14, Lcom/intsig/office/common/shape/AbstractShape;

    .line 151
    .line 152
    if-eqz v3, :cond_5

    .line 153
    .line 154
    move-object v2, v14

    .line 155
    check-cast v2, Lcom/intsig/office/common/shape/AbstractShape;

    .line 156
    .line 157
    invoke-virtual {v2}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    :cond_5
    move-object v3, v9

    .line 162
    check-cast v3, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 163
    .line 164
    invoke-direct {v6, v3}, Lcom/intsig/office/fc/ppt/PPTReader;->getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;)Lcom/intsig/office/common/borders/Line;

    .line 165
    .line 166
    .line 167
    move-result-object v3

    .line 168
    if-nez v3, :cond_7

    .line 169
    .line 170
    if-eqz v14, :cond_7

    .line 171
    .line 172
    instance-of v4, v14, Lcom/intsig/office/common/shape/AbstractShape;

    .line 173
    .line 174
    if-eqz v4, :cond_7

    .line 175
    .line 176
    check-cast v14, Lcom/intsig/office/common/shape/AbstractShape;

    .line 177
    .line 178
    invoke-virtual {v14}, Lcom/intsig/office/common/shape/AbstractShape;->getLine()Lcom/intsig/office/common/borders/Line;

    .line 179
    .line 180
    .line 181
    move-result-object v3

    .line 182
    goto :goto_3

    .line 183
    :cond_6
    const/4 v2, 0x0

    .line 184
    const/4 v3, 0x0

    .line 185
    :cond_7
    :goto_3
    instance-of v4, v9, Lcom/intsig/office/fc/hslf/model/Line;

    .line 186
    .line 187
    if-nez v4, :cond_c

    .line 188
    .line 189
    instance-of v5, v9, Lcom/intsig/office/fc/hslf/model/Freeform;

    .line 190
    .line 191
    if-nez v5, :cond_c

    .line 192
    .line 193
    instance-of v5, v9, Lcom/intsig/office/fc/hslf/model/AutoShape;

    .line 194
    .line 195
    if-nez v5, :cond_c

    .line 196
    .line 197
    instance-of v5, v9, Lcom/intsig/office/fc/hslf/model/TextBox;

    .line 198
    .line 199
    if-nez v5, :cond_c

    .line 200
    .line 201
    instance-of v5, v9, Lcom/intsig/office/fc/hslf/model/Picture;

    .line 202
    .line 203
    if-eqz v5, :cond_8

    .line 204
    .line 205
    goto :goto_6

    .line 206
    :cond_8
    instance-of v2, v9, Lcom/intsig/office/fc/hslf/model/Table;

    .line 207
    .line 208
    if-eqz v2, :cond_9

    .line 209
    .line 210
    move-object v0, v9

    .line 211
    check-cast v0, Lcom/intsig/office/fc/hslf/model/Table;

    .line 212
    .line 213
    invoke-direct {v6, v7, v0, v8, v10}, Lcom/intsig/office/fc/ppt/PPTReader;->processTable(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Table;Lcom/intsig/office/common/shape/GroupShape;I)V

    .line 214
    .line 215
    .line 216
    goto/16 :goto_18

    .line 217
    .line 218
    :cond_9
    if-eqz v1, :cond_38

    .line 219
    .line 220
    move-object v1, v9

    .line 221
    check-cast v1, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 222
    .line 223
    new-instance v2, Lcom/intsig/office/common/shape/GroupShape;

    .line 224
    .line 225
    invoke-direct {v2}, Lcom/intsig/office/common/shape/GroupShape;-><init>()V

    .line 226
    .line 227
    .line 228
    invoke-virtual {v2, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 229
    .line 230
    .line 231
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 232
    .line 233
    .line 234
    move-result v3

    .line 235
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 236
    .line 237
    .line 238
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getFlipHorizontal()Z

    .line 239
    .line 240
    .line 241
    move-result v3

    .line 242
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipHorizontal(Z)V

    .line 243
    .line 244
    .line 245
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getFlipVertical()Z

    .line 246
    .line 247
    .line 248
    move-result v3

    .line 249
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setFlipVertical(Z)V

    .line 250
    .line 251
    .line 252
    invoke-virtual {v2, v8}, Lcom/intsig/office/common/shape/AbstractShape;->setParent(Lcom/intsig/office/common/shape/IShape;)V

    .line 253
    .line 254
    .line 255
    invoke-virtual {v6, v9, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V

    .line 256
    .line 257
    .line 258
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 259
    .line 260
    .line 261
    move-result-object v1

    .line 262
    new-instance v3, Ljava/util/ArrayList;

    .line 263
    .line 264
    array-length v4, v1

    .line 265
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 266
    .line 267
    .line 268
    :goto_4
    array-length v4, v1

    .line 269
    if-ge v0, v4, :cond_a

    .line 270
    .line 271
    aget-object v4, v1, v0

    .line 272
    .line 273
    invoke-direct {v6, v7, v2, v4, v10}, Lcom/intsig/office/fc/ppt/PPTReader;->processShape(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hslf/model/Shape;I)V

    .line 274
    .line 275
    .line 276
    aget-object v4, v1, v0

    .line 277
    .line 278
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 279
    .line 280
    .line 281
    move-result v4

    .line 282
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 283
    .line 284
    .line 285
    move-result-object v4

    .line 286
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    .line 288
    .line 289
    add-int/lit8 v0, v0, 0x1

    .line 290
    .line 291
    goto :goto_4

    .line 292
    :cond_a
    if-nez v8, :cond_b

    .line 293
    .line 294
    invoke-virtual {v7, v2}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 295
    .line 296
    .line 297
    goto :goto_5

    .line 298
    :cond_b
    invoke-virtual {v8, v2}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 299
    .line 300
    .line 301
    :goto_5
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 302
    .line 303
    .line 304
    move-result v0

    .line 305
    invoke-virtual {v7, v0, v3}, Lcom/intsig/office/pg/model/PGSlide;->addGroupShape(ILjava/util/List;)V

    .line 306
    .line 307
    .line 308
    goto/16 :goto_18

    .line 309
    .line 310
    :cond_c
    :goto_6
    const/high16 v1, 0x3f800000    # 1.0f

    .line 311
    .line 312
    const/16 v5, 0x21

    .line 313
    .line 314
    const/4 v14, 0x1

    .line 315
    if-eqz v4, :cond_11

    .line 316
    .line 317
    if-eqz v3, :cond_38

    .line 318
    .line 319
    new-instance v4, Lcom/intsig/office/common/shape/LineShape;

    .line 320
    .line 321
    invoke-direct {v4}, Lcom/intsig/office/common/shape/LineShape;-><init>()V

    .line 322
    .line 323
    .line 324
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 325
    .line 326
    .line 327
    move-result v10

    .line 328
    invoke-virtual {v4, v10}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 329
    .line 330
    .line 331
    invoke-virtual {v4, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 332
    .line 333
    .line 334
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 335
    .line 336
    .line 337
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 338
    .line 339
    .line 340
    move-object v2, v9

    .line 341
    check-cast v2, Lcom/intsig/office/fc/hslf/model/Line;

    .line 342
    .line 343
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Line;->getAdjustmentValue()[Ljava/lang/Float;

    .line 344
    .line 345
    .line 346
    move-result-object v3

    .line 347
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 348
    .line 349
    .line 350
    move-result v10

    .line 351
    if-ne v10, v5, :cond_d

    .line 352
    .line 353
    if-nez v3, :cond_d

    .line 354
    .line 355
    new-array v3, v14, [Ljava/lang/Float;

    .line 356
    .line 357
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 358
    .line 359
    .line 360
    move-result-object v1

    .line 361
    aput-object v1, v3, v0

    .line 362
    .line 363
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 364
    .line 365
    .line 366
    goto :goto_7

    .line 367
    :cond_d
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 368
    .line 369
    .line 370
    :goto_7
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getStartArrowType()I

    .line 371
    .line 372
    .line 373
    move-result v0

    .line 374
    if-lez v0, :cond_e

    .line 375
    .line 376
    int-to-byte v0, v0

    .line 377
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getStartArrowWidth()I

    .line 378
    .line 379
    .line 380
    move-result v1

    .line 381
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getStartArrowLength()I

    .line 382
    .line 383
    .line 384
    move-result v3

    .line 385
    invoke-virtual {v4, v0, v1, v3}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    .line 386
    .line 387
    .line 388
    :cond_e
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getEndArrowType()I

    .line 389
    .line 390
    .line 391
    move-result v0

    .line 392
    if-lez v0, :cond_f

    .line 393
    .line 394
    int-to-byte v0, v0

    .line 395
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getEndArrowWidth()I

    .line 396
    .line 397
    .line 398
    move-result v1

    .line 399
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getEndArrowLength()I

    .line 400
    .line 401
    .line 402
    move-result v2

    .line 403
    invoke-virtual {v4, v0, v1, v2}, Lcom/intsig/office/common/shape/LineShape;->createEndArrow(BII)V

    .line 404
    .line 405
    .line 406
    :cond_f
    move-object v0, v9

    .line 407
    check-cast v0, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 408
    .line 409
    invoke-virtual {v6, v0, v4}, Lcom/intsig/office/fc/ppt/PPTReader;->processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V

    .line 410
    .line 411
    .line 412
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 413
    .line 414
    .line 415
    move-result v0

    .line 416
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 417
    .line 418
    .line 419
    if-nez v8, :cond_10

    .line 420
    .line 421
    invoke-virtual {v7, v4}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 422
    .line 423
    .line 424
    goto/16 :goto_18

    .line 425
    .line 426
    :cond_10
    invoke-virtual {v8, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 427
    .line 428
    .line 429
    goto/16 :goto_18

    .line 430
    .line 431
    :cond_11
    instance-of v4, v9, Lcom/intsig/office/fc/hslf/model/Freeform;

    .line 432
    .line 433
    const/4 v15, 0x5

    .line 434
    if-eqz v4, :cond_21

    .line 435
    .line 436
    if-nez v2, :cond_12

    .line 437
    .line 438
    if-eqz v3, :cond_38

    .line 439
    .line 440
    :cond_12
    new-instance v1, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;

    .line 441
    .line 442
    invoke-direct {v1}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;-><init>()V

    .line 443
    .line 444
    .line 445
    const/16 v4, 0xe9

    .line 446
    .line 447
    invoke-virtual {v1, v4}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 448
    .line 449
    .line 450
    invoke-virtual {v1, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 451
    .line 452
    .line 453
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getStartArrowType()I

    .line 454
    .line 455
    .line 456
    move-result v4

    .line 457
    if-lez v4, :cond_17

    .line 458
    .line 459
    move-object v5, v9

    .line 460
    check-cast v5, Lcom/intsig/office/fc/hslf/model/Freeform;

    .line 461
    .line 462
    invoke-virtual {v5, v11}, Lcom/intsig/office/fc/hslf/model/Freeform;->getStartArrowPathAndTail(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 463
    .line 464
    .line 465
    move-result-object v5

    .line 466
    if-eqz v5, :cond_17

    .line 467
    .line 468
    invoke-virtual {v5}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 469
    .line 470
    .line 471
    move-result-object v10

    .line 472
    if-eqz v10, :cond_17

    .line 473
    .line 474
    invoke-virtual {v5}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 475
    .line 476
    .line 477
    move-result-object v10

    .line 478
    new-instance v12, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 479
    .line 480
    invoke-direct {v12}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 481
    .line 482
    .line 483
    invoke-virtual {v5}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 484
    .line 485
    .line 486
    move-result-object v5

    .line 487
    invoke-virtual {v12, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 488
    .line 489
    .line 490
    invoke-virtual {v12, v14}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 491
    .line 492
    .line 493
    if-eq v4, v15, :cond_15

    .line 494
    .line 495
    if-eqz v3, :cond_14

    .line 496
    .line 497
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 498
    .line 499
    .line 500
    move-result-object v5

    .line 501
    if-nez v5, :cond_13

    .line 502
    .line 503
    goto :goto_8

    .line 504
    :cond_13
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 505
    .line 506
    .line 507
    move-result-object v5

    .line 508
    invoke-virtual {v12, v5}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 509
    .line 510
    .line 511
    goto :goto_9

    .line 512
    :cond_14
    :goto_8
    move-object v5, v9

    .line 513
    check-cast v5, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 514
    .line 515
    invoke-virtual {v5}, Lcom/intsig/office/fc/hslf/model/Shape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    .line 516
    .line 517
    .line 518
    move-result-object v5

    .line 519
    if-eqz v5, :cond_16

    .line 520
    .line 521
    new-instance v13, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 522
    .line 523
    invoke-direct {v13}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 524
    .line 525
    .line 526
    invoke-virtual {v13, v0}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 527
    .line 528
    .line 529
    invoke-direct {v6, v5}, Lcom/intsig/office/fc/ppt/PPTReader;->converterColor(Lcom/intsig/office/java/awt/Color;)I

    .line 530
    .line 531
    .line 532
    move-result v5

    .line 533
    invoke-virtual {v13, v5}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 534
    .line 535
    .line 536
    invoke-virtual {v12, v13}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 537
    .line 538
    .line 539
    goto :goto_9

    .line 540
    :cond_15
    invoke-virtual {v12, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 541
    .line 542
    .line 543
    :cond_16
    :goto_9
    invoke-virtual {v1, v12}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 544
    .line 545
    .line 546
    move-object v12, v10

    .line 547
    goto :goto_a

    .line 548
    :cond_17
    const/4 v12, 0x0

    .line 549
    :goto_a
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getEndArrowType()I

    .line 550
    .line 551
    .line 552
    move-result v5

    .line 553
    if-lez v5, :cond_1c

    .line 554
    .line 555
    move-object v10, v9

    .line 556
    check-cast v10, Lcom/intsig/office/fc/hslf/model/Freeform;

    .line 557
    .line 558
    invoke-virtual {v10, v11}, Lcom/intsig/office/fc/hslf/model/Freeform;->getEndArrowPathAndTail(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;

    .line 559
    .line 560
    .line 561
    move-result-object v10

    .line 562
    if-eqz v10, :cond_1c

    .line 563
    .line 564
    invoke-virtual {v10}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 565
    .line 566
    .line 567
    move-result-object v13

    .line 568
    if-eqz v13, :cond_1c

    .line 569
    .line 570
    invoke-virtual {v10}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowTailCenter()Landroid/graphics/PointF;

    .line 571
    .line 572
    .line 573
    move-result-object v13

    .line 574
    new-instance v0, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 575
    .line 576
    invoke-direct {v0}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 577
    .line 578
    .line 579
    invoke-virtual {v10}, Lcom/intsig/office/common/autoshape/pathbuilder/ArrowPathAndTail;->getArrowPath()Landroid/graphics/Path;

    .line 580
    .line 581
    .line 582
    move-result-object v10

    .line 583
    invoke-virtual {v0, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 584
    .line 585
    .line 586
    invoke-virtual {v0, v14}, Lcom/intsig/office/common/autoshape/ExtendPath;->setArrowFlag(Z)V

    .line 587
    .line 588
    .line 589
    if-eq v5, v15, :cond_1a

    .line 590
    .line 591
    if-eqz v3, :cond_19

    .line 592
    .line 593
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 594
    .line 595
    .line 596
    move-result-object v10

    .line 597
    if-nez v10, :cond_18

    .line 598
    .line 599
    goto :goto_b

    .line 600
    :cond_18
    invoke-virtual {v3}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 601
    .line 602
    .line 603
    move-result-object v10

    .line 604
    invoke-virtual {v0, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 605
    .line 606
    .line 607
    goto :goto_c

    .line 608
    :cond_19
    :goto_b
    move-object v10, v9

    .line 609
    check-cast v10, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 610
    .line 611
    invoke-virtual {v10}, Lcom/intsig/office/fc/hslf/model/Shape;->getLineColor()Lcom/intsig/office/java/awt/Color;

    .line 612
    .line 613
    .line 614
    move-result-object v10

    .line 615
    if-eqz v10, :cond_1b

    .line 616
    .line 617
    new-instance v14, Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 618
    .line 619
    invoke-direct {v14}, Lcom/intsig/office/common/bg/BackgroundAndFill;-><init>()V

    .line 620
    .line 621
    .line 622
    const/4 v15, 0x0

    .line 623
    invoke-virtual {v14, v15}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setFillType(B)V

    .line 624
    .line 625
    .line 626
    invoke-direct {v6, v10}, Lcom/intsig/office/fc/ppt/PPTReader;->converterColor(Lcom/intsig/office/java/awt/Color;)I

    .line 627
    .line 628
    .line 629
    move-result v10

    .line 630
    invoke-virtual {v14, v10}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setForegroundColor(I)V

    .line 631
    .line 632
    .line 633
    invoke-virtual {v0, v14}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 634
    .line 635
    .line 636
    goto :goto_c

    .line 637
    :cond_1a
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 638
    .line 639
    .line 640
    :cond_1b
    :goto_c
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 641
    .line 642
    .line 643
    move-object v14, v13

    .line 644
    goto :goto_d

    .line 645
    :cond_1c
    const/4 v14, 0x0

    .line 646
    :goto_d
    move-object v10, v9

    .line 647
    check-cast v10, Lcom/intsig/office/fc/hslf/model/Freeform;

    .line 648
    .line 649
    int-to-byte v13, v4

    .line 650
    int-to-byte v15, v5

    .line 651
    invoke-virtual/range {v10 .. v15}, Lcom/intsig/office/fc/hslf/model/Freeform;->getFreeformPath(Lcom/intsig/office/java/awt/Rectangle;Landroid/graphics/PointF;BLandroid/graphics/PointF;B)[Landroid/graphics/Path;

    .line 652
    .line 653
    .line 654
    move-result-object v0

    .line 655
    const/4 v4, 0x0

    .line 656
    :goto_e
    if-eqz v0, :cond_1f

    .line 657
    .line 658
    array-length v5, v0

    .line 659
    if-ge v4, v5, :cond_1f

    .line 660
    .line 661
    new-instance v5, Lcom/intsig/office/common/autoshape/ExtendPath;

    .line 662
    .line 663
    invoke-direct {v5}, Lcom/intsig/office/common/autoshape/ExtendPath;-><init>()V

    .line 664
    .line 665
    .line 666
    aget-object v10, v0, v4

    .line 667
    .line 668
    invoke-virtual {v5, v10}, Lcom/intsig/office/common/autoshape/ExtendPath;->setPath(Landroid/graphics/Path;)V

    .line 669
    .line 670
    .line 671
    if-eqz v3, :cond_1d

    .line 672
    .line 673
    invoke-virtual {v5, v3}, Lcom/intsig/office/common/autoshape/ExtendPath;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 674
    .line 675
    .line 676
    :cond_1d
    if-eqz v2, :cond_1e

    .line 677
    .line 678
    invoke-virtual {v5, v2}, Lcom/intsig/office/common/autoshape/ExtendPath;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 679
    .line 680
    .line 681
    :cond_1e
    invoke-virtual {v1, v5}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;->appendPath(Lcom/intsig/office/common/autoshape/ExtendPath;)V

    .line 682
    .line 683
    .line 684
    add-int/lit8 v4, v4, 0x1

    .line 685
    .line 686
    goto :goto_e

    .line 687
    :cond_1f
    move-object v0, v9

    .line 688
    check-cast v0, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 689
    .line 690
    invoke-virtual {v6, v0, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V

    .line 691
    .line 692
    .line 693
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 694
    .line 695
    .line 696
    move-result v0

    .line 697
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 698
    .line 699
    .line 700
    if-nez v8, :cond_20

    .line 701
    .line 702
    invoke-virtual {v7, v1}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 703
    .line 704
    .line 705
    goto/16 :goto_18

    .line 706
    .line 707
    :cond_20
    invoke-virtual {v8, v1}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 708
    .line 709
    .line 710
    goto/16 :goto_18

    .line 711
    .line 712
    :cond_21
    instance-of v0, v9, Lcom/intsig/office/fc/hslf/model/AutoShape;

    .line 713
    .line 714
    if-nez v0, :cond_27

    .line 715
    .line 716
    instance-of v0, v9, Lcom/intsig/office/fc/hslf/model/TextBox;

    .line 717
    .line 718
    if-eqz v0, :cond_22

    .line 719
    .line 720
    goto :goto_f

    .line 721
    :cond_22
    instance-of v0, v9, Lcom/intsig/office/fc/hslf/model/Picture;

    .line 722
    .line 723
    if-eqz v0, :cond_38

    .line 724
    .line 725
    move-object v0, v9

    .line 726
    check-cast v0, Lcom/intsig/office/fc/hslf/model/Picture;

    .line 727
    .line 728
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Picture;->getPictureData()Lcom/intsig/office/fc/hslf/usermodel/PictureData;

    .line 729
    .line 730
    .line 731
    move-result-object v1

    .line 732
    if-eqz v1, :cond_24

    .line 733
    .line 734
    new-instance v4, Lcom/intsig/office/common/shape/PictureShape;

    .line 735
    .line 736
    invoke-direct {v4}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 737
    .line 738
    .line 739
    iget-object v5, v6, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 740
    .line 741
    invoke-interface {v5}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 742
    .line 743
    .line 744
    move-result-object v5

    .line 745
    invoke-virtual {v5}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 746
    .line 747
    .line 748
    move-result-object v5

    .line 749
    invoke-virtual {v5, v1}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/hslf/usermodel/PictureData;)I

    .line 750
    .line 751
    .line 752
    move-result v1

    .line 753
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 754
    .line 755
    .line 756
    invoke-virtual {v4, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 757
    .line 758
    .line 759
    move-object v1, v9

    .line 760
    check-cast v1, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 761
    .line 762
    invoke-virtual {v6, v1, v4}, Lcom/intsig/office/fc/ppt/PPTReader;->processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V

    .line 763
    .line 764
    .line 765
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 766
    .line 767
    .line 768
    move-result v1

    .line 769
    invoke-virtual {v4, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 770
    .line 771
    .line 772
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Picture;->getEscherOptRecord()Lcom/intsig/office/fc/ddf/EscherOptRecord;

    .line 773
    .line 774
    .line 775
    move-result-object v0

    .line 776
    invoke-static {v0}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getPictureEffectInfor(Lcom/intsig/office/fc/ddf/EscherOptRecord;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 777
    .line 778
    .line 779
    move-result-object v0

    .line 780
    invoke-virtual {v4, v0}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 781
    .line 782
    .line 783
    invoke-virtual {v4, v2}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 784
    .line 785
    .line 786
    invoke-virtual {v4, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 787
    .line 788
    .line 789
    if-nez v8, :cond_23

    .line 790
    .line 791
    invoke-virtual {v7, v4}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 792
    .line 793
    .line 794
    goto/16 :goto_18

    .line 795
    .line 796
    :cond_23
    invoke-virtual {v8, v4}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 797
    .line 798
    .line 799
    goto/16 :goto_18

    .line 800
    .line 801
    :cond_24
    if-nez v2, :cond_25

    .line 802
    .line 803
    if-eqz v3, :cond_38

    .line 804
    .line 805
    :cond_25
    new-instance v0, Lcom/intsig/office/common/shape/AutoShape;

    .line 806
    .line 807
    invoke-direct {v0, v14}, Lcom/intsig/office/common/shape/AutoShape;-><init>(I)V

    .line 808
    .line 809
    .line 810
    const/4 v1, 0x0

    .line 811
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/AutoShape;->setAuotShape07(Z)V

    .line 812
    .line 813
    .line 814
    invoke-virtual {v0, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 815
    .line 816
    .line 817
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 818
    .line 819
    .line 820
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 821
    .line 822
    .line 823
    if-nez v8, :cond_26

    .line 824
    .line 825
    invoke-virtual {v7, v0}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 826
    .line 827
    .line 828
    goto/16 :goto_18

    .line 829
    .line 830
    :cond_26
    invoke-virtual {v8, v0}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 831
    .line 832
    .line 833
    goto/16 :goto_18

    .line 834
    .line 835
    :cond_27
    :goto_f
    move-object v4, v9

    .line 836
    check-cast v4, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 837
    .line 838
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/model/TextShape;->getPlaceholderId()I

    .line 839
    .line 840
    .line 841
    move-result v13

    .line 842
    if-nez v2, :cond_29

    .line 843
    .line 844
    if-eqz v3, :cond_28

    .line 845
    .line 846
    goto :goto_10

    .line 847
    :cond_28
    const/4 v15, 0x0

    .line 848
    goto/16 :goto_15

    .line 849
    .line 850
    :cond_29
    :goto_10
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 851
    .line 852
    .line 853
    move-result v0

    .line 854
    const/16 v15, 0x14

    .line 855
    .line 856
    if-eq v0, v15, :cond_2c

    .line 857
    .line 858
    const/16 v15, 0x20

    .line 859
    .line 860
    if-eq v0, v15, :cond_2c

    .line 861
    .line 862
    if-eq v0, v5, :cond_2c

    .line 863
    .line 864
    const/16 v15, 0x22

    .line 865
    .line 866
    if-eq v0, v15, :cond_2c

    .line 867
    .line 868
    const/16 v15, 0x23

    .line 869
    .line 870
    if-eq v0, v15, :cond_2c

    .line 871
    .line 872
    const/16 v15, 0x24

    .line 873
    .line 874
    if-eq v0, v15, :cond_2c

    .line 875
    .line 876
    const/16 v15, 0x25

    .line 877
    .line 878
    if-eq v0, v15, :cond_2c

    .line 879
    .line 880
    const/16 v15, 0x26

    .line 881
    .line 882
    if-eq v0, v15, :cond_2c

    .line 883
    .line 884
    const/16 v15, 0x27

    .line 885
    .line 886
    if-eq v0, v15, :cond_2c

    .line 887
    .line 888
    const/16 v15, 0x28

    .line 889
    .line 890
    if-ne v0, v15, :cond_2a

    .line 891
    .line 892
    goto :goto_11

    .line 893
    :cond_2a
    new-instance v0, Lcom/intsig/office/common/shape/AutoShape;

    .line 894
    .line 895
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 896
    .line 897
    .line 898
    move-result v1

    .line 899
    invoke-direct {v0, v1}, Lcom/intsig/office/common/shape/AutoShape;-><init>(I)V

    .line 900
    .line 901
    .line 902
    const/4 v1, 0x0

    .line 903
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/AutoShape;->setAuotShape07(Z)V

    .line 904
    .line 905
    .line 906
    invoke-virtual {v0, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 907
    .line 908
    .line 909
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 910
    .line 911
    .line 912
    if-eqz v3, :cond_2b

    .line 913
    .line 914
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 915
    .line 916
    .line 917
    :cond_2b
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 918
    .line 919
    .line 920
    move-result v1

    .line 921
    const/16 v2, 0xca

    .line 922
    .line 923
    if-eq v1, v2, :cond_2f

    .line 924
    .line 925
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getAdjustmentValue()[Ljava/lang/Float;

    .line 926
    .line 927
    .line 928
    move-result-object v1

    .line 929
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 930
    .line 931
    .line 932
    goto :goto_13

    .line 933
    :cond_2c
    :goto_11
    new-instance v0, Lcom/intsig/office/common/shape/LineShape;

    .line 934
    .line 935
    invoke-direct {v0}, Lcom/intsig/office/common/shape/LineShape;-><init>()V

    .line 936
    .line 937
    .line 938
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 939
    .line 940
    .line 941
    move-result v2

    .line 942
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 943
    .line 944
    .line 945
    invoke-virtual {v0, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 946
    .line 947
    .line 948
    invoke-virtual {v0, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 949
    .line 950
    .line 951
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getAdjustmentValue()[Ljava/lang/Float;

    .line 952
    .line 953
    .line 954
    move-result-object v2

    .line 955
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 956
    .line 957
    .line 958
    move-result v3

    .line 959
    if-ne v3, v5, :cond_2d

    .line 960
    .line 961
    if-nez v2, :cond_2d

    .line 962
    .line 963
    new-array v2, v14, [Ljava/lang/Float;

    .line 964
    .line 965
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 966
    .line 967
    .line 968
    move-result-object v1

    .line 969
    const/4 v3, 0x0

    .line 970
    aput-object v1, v2, v3

    .line 971
    .line 972
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 973
    .line 974
    .line 975
    goto :goto_12

    .line 976
    :cond_2d
    invoke-virtual {v0, v2}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 977
    .line 978
    .line 979
    :goto_12
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getStartArrowType()I

    .line 980
    .line 981
    .line 982
    move-result v1

    .line 983
    if-lez v1, :cond_2e

    .line 984
    .line 985
    int-to-byte v1, v1

    .line 986
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getStartArrowWidth()I

    .line 987
    .line 988
    .line 989
    move-result v2

    .line 990
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getStartArrowLength()I

    .line 991
    .line 992
    .line 993
    move-result v3

    .line 994
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    .line 995
    .line 996
    .line 997
    :cond_2e
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getEndArrowType()I

    .line 998
    .line 999
    .line 1000
    move-result v1

    .line 1001
    if-lez v1, :cond_2f

    .line 1002
    .line 1003
    int-to-byte v1, v1

    .line 1004
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getEndArrowWidth()I

    .line 1005
    .line 1006
    .line 1007
    move-result v2

    .line 1008
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getEndArrowLength()I

    .line 1009
    .line 1010
    .line 1011
    move-result v3

    .line 1012
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/office/common/shape/LineShape;->createEndArrow(BII)V

    .line 1013
    .line 1014
    .line 1015
    :cond_2f
    :goto_13
    move-object v1, v9

    .line 1016
    check-cast v1, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 1017
    .line 1018
    invoke-virtual {v6, v1, v0}, Lcom/intsig/office/fc/ppt/PPTReader;->processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V

    .line 1019
    .line 1020
    .line 1021
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 1022
    .line 1023
    .line 1024
    move-result v1

    .line 1025
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 1026
    .line 1027
    .line 1028
    invoke-virtual {v0, v13}, Lcom/intsig/office/common/shape/AbstractShape;->setPlaceHolderID(I)V

    .line 1029
    .line 1030
    .line 1031
    if-nez v8, :cond_30

    .line 1032
    .line 1033
    invoke-virtual {v7, v0}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 1034
    .line 1035
    .line 1036
    goto :goto_14

    .line 1037
    :cond_30
    invoke-virtual {v8, v0}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 1038
    .line 1039
    .line 1040
    :goto_14
    move-object v15, v0

    .line 1041
    :goto_15
    new-instance v5, Lcom/intsig/office/common/shape/TextBox;

    .line 1042
    .line 1043
    invoke-direct {v5}, Lcom/intsig/office/common/shape/TextBox;-><init>()V

    .line 1044
    .line 1045
    .line 1046
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMetaCharactersType()B

    .line 1047
    .line 1048
    .line 1049
    move-result v3

    .line 1050
    invoke-virtual {v5, v3}, Lcom/intsig/office/common/shape/TextBox;->setMCType(B)V

    .line 1051
    .line 1052
    .line 1053
    move-object/from16 v0, p0

    .line 1054
    .line 1055
    move-object v1, v5

    .line 1056
    move-object v2, v4

    .line 1057
    move v4, v3

    .line 1058
    move-object v3, v11

    .line 1059
    move v11, v4

    .line 1060
    move/from16 v4, p4

    .line 1061
    .line 1062
    move-object/from16 v16, v5

    .line 1063
    .line 1064
    move v5, v13

    .line 1065
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/ppt/PPTReader;->processTextShape(Lcom/intsig/office/common/shape/TextBox;Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/java/awt/Rectangle;II)V

    .line 1066
    .line 1067
    .line 1068
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 1069
    .line 1070
    .line 1071
    move-result-object v0

    .line 1072
    if-eqz v0, :cond_38

    .line 1073
    .line 1074
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/common/shape/TextBox;->isWordArt()Z

    .line 1075
    .line 1076
    .line 1077
    move-result v0

    .line 1078
    if-eqz v0, :cond_31

    .line 1079
    .line 1080
    if-eqz v15, :cond_31

    .line 1081
    .line 1082
    const/4 v0, 0x0

    .line 1083
    invoke-virtual {v15, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 1084
    .line 1085
    .line 1086
    :cond_31
    move-object v0, v9

    .line 1087
    check-cast v0, Lcom/intsig/office/fc/hslf/model/SimpleShape;

    .line 1088
    .line 1089
    move-object/from16 v1, v16

    .line 1090
    .line 1091
    invoke-virtual {v6, v0, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V

    .line 1092
    .line 1093
    .line 1094
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 1095
    .line 1096
    .line 1097
    move-result v0

    .line 1098
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 1099
    .line 1100
    .line 1101
    invoke-virtual {v1, v13}, Lcom/intsig/office/common/shape/AbstractShape;->setPlaceHolderID(I)V

    .line 1102
    .line 1103
    .line 1104
    if-ne v10, v12, :cond_35

    .line 1105
    .line 1106
    const/16 v0, 0x9

    .line 1107
    .line 1108
    if-ne v13, v0, :cond_32

    .line 1109
    .line 1110
    iput-boolean v14, v6, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterFooter:Z

    .line 1111
    .line 1112
    goto :goto_16

    .line 1113
    :cond_32
    const/4 v0, 0x7

    .line 1114
    if-ne v13, v0, :cond_34

    .line 1115
    .line 1116
    if-eq v11, v12, :cond_33

    .line 1117
    .line 1118
    const/4 v0, 0x3

    .line 1119
    if-eq v11, v0, :cond_33

    .line 1120
    .line 1121
    const/4 v0, 0x5

    .line 1122
    if-ne v11, v0, :cond_34

    .line 1123
    .line 1124
    :cond_33
    iput-boolean v14, v6, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterDateTime:Z

    .line 1125
    .line 1126
    goto :goto_16

    .line 1127
    :cond_34
    const/16 v0, 0x8

    .line 1128
    .line 1129
    if-ne v13, v0, :cond_35

    .line 1130
    .line 1131
    if-ne v11, v14, :cond_35

    .line 1132
    .line 1133
    iput-boolean v14, v6, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterSlideNumber:Z

    .line 1134
    .line 1135
    :cond_35
    :goto_16
    if-eqz v8, :cond_37

    .line 1136
    .line 1137
    if-nez v10, :cond_36

    .line 1138
    .line 1139
    invoke-static/range {p3 .. p3}, Lcom/intsig/office/fc/hslf/model/MasterSheet;->isPlaceholder(Lcom/intsig/office/fc/hslf/model/Shape;)Z

    .line 1140
    .line 1141
    .line 1142
    move-result v0

    .line 1143
    if-eqz v0, :cond_36

    .line 1144
    .line 1145
    goto :goto_17

    .line 1146
    :cond_36
    invoke-virtual {v8, v1}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 1147
    .line 1148
    .line 1149
    goto :goto_18

    .line 1150
    :cond_37
    :goto_17
    invoke-virtual {v7, v1}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 1151
    .line 1152
    .line 1153
    :cond_38
    :goto_18
    return-void
.end method

.method private processSingleAnimation(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;)Lcom/intsig/office/pg/animate/ShapeAnimation;
    .locals 12

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    sget-wide v1, Lcom/intsig/office/fc/hslf/record/TimeNodeAttributeContainer;->RECORD_ID:J

    .line 3
    .line 4
    invoke-virtual {p2, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    check-cast v1, Lcom/intsig/office/fc/hslf/record/TimeNodeAttributeContainer;

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    array-length v2, v1

    .line 15
    const/4 v3, 0x0

    .line 16
    const/4 v4, 0x0

    .line 17
    :goto_0
    const/4 v5, 0x2

    .line 18
    const/4 v6, -0x1

    .line 19
    if-ge v4, v2, :cond_3

    .line 20
    .line 21
    aget-object v7, v1, v4

    .line 22
    .line 23
    instance-of v8, v7, Lcom/intsig/office/fc/hslf/record/TimeVariant;

    .line 24
    .line 25
    if-eqz v8, :cond_2

    .line 26
    .line 27
    move-object v8, v7

    .line 28
    check-cast v8, Lcom/intsig/office/fc/hslf/record/TimeVariant;

    .line 29
    .line 30
    invoke-virtual {v8}, Lcom/intsig/office/fc/hslf/record/TimeVariant;->getAttributeType()I

    .line 31
    .line 32
    .line 33
    move-result v8

    .line 34
    const/16 v9, 0xb

    .line 35
    .line 36
    if-ne v8, v9, :cond_2

    .line 37
    .line 38
    check-cast v7, Lcom/intsig/office/fc/hslf/record/TimeVariant;

    .line 39
    .line 40
    invoke-virtual {v7}, Lcom/intsig/office/fc/hslf/record/TimeVariant;->getValue()Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    check-cast v1, Ljava/lang/Integer;

    .line 45
    .line 46
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    const/4 v2, 0x1

    .line 51
    if-eq v1, v2, :cond_1

    .line 52
    .line 53
    if-eq v1, v5, :cond_0

    .line 54
    .line 55
    const/4 v4, 0x3

    .line 56
    if-eq v1, v4, :cond_4

    .line 57
    .line 58
    return-object v0

    .line 59
    :cond_0
    const/4 v2, 0x2

    .line 60
    goto :goto_1

    .line 61
    :cond_1
    const/4 v2, 0x0

    .line 62
    goto :goto_1

    .line 63
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_3
    const/4 v2, -0x1

    .line 67
    :cond_4
    :goto_1
    sget-wide v7, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;->RECORD_ID:J

    .line 68
    .line 69
    invoke-virtual {p2, v7, v8}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    check-cast p2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 74
    .line 75
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 76
    .line 77
    .line 78
    move-result-object p2

    .line 79
    array-length v1, p2

    .line 80
    :goto_2
    if-ge v3, v1, :cond_a

    .line 81
    .line 82
    aget-object v4, p2, v3

    .line 83
    .line 84
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 85
    .line 86
    .line 87
    move-result-wide v7

    .line 88
    sget-wide v9, Lcom/intsig/office/fc/hslf/record/TimeAnimateBehaviorContainer;->RECORD_ID:J

    .line 89
    .line 90
    cmp-long v11, v7, v9

    .line 91
    .line 92
    if-eqz v11, :cond_6

    .line 93
    .line 94
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 95
    .line 96
    .line 97
    move-result-wide v7

    .line 98
    sget-wide v9, Lcom/intsig/office/fc/hslf/record/TimeColorBehaviorContainer;->RECORD_ID:J

    .line 99
    .line 100
    cmp-long v11, v7, v9

    .line 101
    .line 102
    if-eqz v11, :cond_6

    .line 103
    .line 104
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 105
    .line 106
    .line 107
    move-result-wide v7

    .line 108
    sget-wide v9, Lcom/intsig/office/fc/hslf/record/TimeEffectBehaviorContainer;->RECORD_ID:J

    .line 109
    .line 110
    cmp-long v11, v7, v9

    .line 111
    .line 112
    if-eqz v11, :cond_6

    .line 113
    .line 114
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 115
    .line 116
    .line 117
    move-result-wide v7

    .line 118
    sget-wide v9, Lcom/intsig/office/fc/hslf/record/TimeMotionBehaviorContainer;->RECORD_ID:J

    .line 119
    .line 120
    cmp-long v11, v7, v9

    .line 121
    .line 122
    if-eqz v11, :cond_6

    .line 123
    .line 124
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 125
    .line 126
    .line 127
    move-result-wide v7

    .line 128
    sget-wide v9, Lcom/intsig/office/fc/hslf/record/TimeRotationBehaviorContainer;->RECORD_ID:J

    .line 129
    .line 130
    cmp-long v11, v7, v9

    .line 131
    .line 132
    if-eqz v11, :cond_6

    .line 133
    .line 134
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 135
    .line 136
    .line 137
    move-result-wide v7

    .line 138
    sget-wide v9, Lcom/intsig/office/fc/hslf/record/TimeScaleBehaviorContainer;->RECORD_ID:J

    .line 139
    .line 140
    cmp-long v11, v7, v9

    .line 141
    .line 142
    if-eqz v11, :cond_6

    .line 143
    .line 144
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 145
    .line 146
    .line 147
    move-result-wide v7

    .line 148
    sget-wide v9, Lcom/intsig/office/fc/hslf/record/TimeSetBehaviorContainer;->RECORD_ID:J

    .line 149
    .line 150
    cmp-long v11, v7, v9

    .line 151
    .line 152
    if-eqz v11, :cond_6

    .line 153
    .line 154
    invoke-virtual {v4}, Lcom/intsig/office/fc/hslf/record/Record;->getRecordType()J

    .line 155
    .line 156
    .line 157
    move-result-wide v7

    .line 158
    sget-wide v9, Lcom/intsig/office/fc/hslf/record/TimeCommandBehaviorContainer;->RECORD_ID:J

    .line 159
    .line 160
    cmp-long v11, v7, v9

    .line 161
    .line 162
    if-nez v11, :cond_5

    .line 163
    .line 164
    goto :goto_3

    .line 165
    :cond_5
    add-int/lit8 v3, v3, 0x1

    .line 166
    .line 167
    goto :goto_2

    .line 168
    :cond_6
    :goto_3
    check-cast v4, Lcom/intsig/office/fc/hslf/record/PositionDependentRecordContainer;

    .line 169
    .line 170
    sget-wide v7, Lcom/intsig/office/fc/hslf/record/TimeBehaviorContainer;->RECORD_ID:J

    .line 171
    .line 172
    invoke-virtual {v4, v7, v8}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 173
    .line 174
    .line 175
    move-result-object p2

    .line 176
    check-cast p2, Lcom/intsig/office/fc/hslf/record/TimeBehaviorContainer;

    .line 177
    .line 178
    sget-wide v3, Lcom/intsig/office/fc/hslf/record/ClientVisualElementContainer;->RECORD_ID:J

    .line 179
    .line 180
    invoke-virtual {p2, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 181
    .line 182
    .line 183
    move-result-object p2

    .line 184
    check-cast p2, Lcom/intsig/office/fc/hslf/record/ClientVisualElementContainer;

    .line 185
    .line 186
    sget-wide v3, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->RECORD_ID:J

    .line 187
    .line 188
    invoke-virtual {p2, v3, v4}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 189
    .line 190
    .line 191
    move-result-object p2

    .line 192
    check-cast p2, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;

    .line 193
    .line 194
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->getTargetElementType()I

    .line 195
    .line 196
    .line 197
    move-result v1

    .line 198
    if-eqz v1, :cond_9

    .line 199
    .line 200
    if-eq v1, v5, :cond_8

    .line 201
    .line 202
    const/4 p1, 0x6

    .line 203
    if-eq v1, p1, :cond_7

    .line 204
    .line 205
    goto :goto_4

    .line 206
    :cond_7
    new-instance p1, Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 207
    .line 208
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->getTargetElementID()I

    .line 209
    .line 210
    .line 211
    move-result p2

    .line 212
    invoke-direct {p1, p2, v2, v6, v6}, Lcom/intsig/office/pg/animate/ShapeAnimation;-><init>(IBII)V

    .line 213
    .line 214
    .line 215
    return-object p1

    .line 216
    :cond_8
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/ppt/PPTReader;->getParaIndex(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;)I

    .line 217
    .line 218
    .line 219
    move-result p1

    .line 220
    new-instance v1, Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 221
    .line 222
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->getTargetElementID()I

    .line 223
    .line 224
    .line 225
    move-result p2

    .line 226
    invoke-direct {v1, p2, v2, p1, p1}, Lcom/intsig/office/pg/animate/ShapeAnimation;-><init>(IBII)V

    .line 227
    .line 228
    .line 229
    return-object v1

    .line 230
    :cond_9
    new-instance p1, Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 231
    .line 232
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/VisualShapeAtom;->getTargetElementID()I

    .line 233
    .line 234
    .line 235
    move-result p2

    .line 236
    const/4 v1, -0x2

    .line 237
    invoke-direct {p1, p2, v2, v1, v1}, Lcom/intsig/office/pg/animate/ShapeAnimation;-><init>(IBII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    .line 239
    .line 240
    return-object p1

    .line 241
    :catch_0
    :cond_a
    :goto_4
    return-object v0
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processSlide(Lcom/intsig/office/fc/hslf/model/Slide;)V
    .locals 10

    .line 1
    new-instance v0, Lcom/intsig/office/pg/model/PGSlide;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/pg/model/PGSlide;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/PGSlide;->setSlideType(I)V

    .line 8
    .line 9
    .line 10
    iget v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->number:I

    .line 11
    .line 12
    add-int/lit8 v3, v2, 0x1

    .line 13
    .line 14
    iput v3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->number:I

    .line 15
    .line 16
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/PGSlide;->setSlideNo(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Slide;->getBackground()Lcom/intsig/office/fc/hslf/model/Background;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Slide;->getBackground()Lcom/intsig/office/fc/hslf/model/Background;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getFill()Lcom/intsig/office/fc/hslf/model/Fill;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-direct {p0, v0, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->converFill(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Fill;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/PGSlide;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ppt/PPTReader;->processMaster(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Slide;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    if-eqz v2, :cond_1

    .line 52
    .line 53
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getSSlideLayoutAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom$SSlideLayoutAtom;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    if-eqz v3, :cond_1

    .line 58
    .line 59
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getSSlideLayoutAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom$SSlideLayoutAtom;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/record/SlideAtom$SSlideLayoutAtom;->getGeometryType()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/PGSlide;->setGeometryType(I)V

    .line 68
    .line 69
    .line 70
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/fc/ppt/PPTReader;->resetFlag()V

    .line 71
    .line 72
    .line 73
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    array-length v3, v2

    .line 78
    const/4 v4, 0x0

    .line 79
    const/4 v5, 0x0

    .line 80
    :goto_0
    const/4 v6, 0x0

    .line 81
    if-ge v5, v3, :cond_2

    .line 82
    .line 83
    aget-object v7, v2, v5

    .line 84
    .line 85
    invoke-direct {p0, v0, v6, v7, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->processShape(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hslf/model/Shape;I)V

    .line 86
    .line 87
    .line 88
    add-int/lit8 v5, v5, 0x1

    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 92
    .line 93
    invoke-virtual {v1}, Lcom/intsig/office/pg/model/PGModel;->isOmitTitleSlide()Z

    .line 94
    .line 95
    .line 96
    move-result v1

    .line 97
    if-eqz v1, :cond_3

    .line 98
    .line 99
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ppt/PPTReader;->isTitleSlide(Lcom/intsig/office/fc/hslf/model/Slide;)Z

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    if-nez v1, :cond_c

    .line 104
    .line 105
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 106
    .line 107
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getMasterIndexs()[I

    .line 108
    .line 109
    .line 110
    move-result-object v2

    .line 111
    aget v2, v2, v4

    .line 112
    .line 113
    invoke-virtual {v1, v2}, Lcom/intsig/office/pg/model/PGModel;->getSlideMaster(I)Lcom/intsig/office/pg/model/PGSlide;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    if-eqz v1, :cond_c

    .line 118
    .line 119
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideHeadersFooters()Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    const/16 v3, 0x9

    .line 124
    .line 125
    const/16 v5, 0x8

    .line 126
    .line 127
    const/4 v7, 0x7

    .line 128
    if-eqz v2, :cond_7

    .line 129
    .line 130
    invoke-virtual {v0, v4}, Lcom/intsig/office/pg/model/PGSlide;->setShowMasterHeadersFooters(Z)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->isSlideNumberVisible()Z

    .line 134
    .line 135
    .line 136
    move-result v8

    .line 137
    if-eqz v8, :cond_4

    .line 138
    .line 139
    iget-boolean v8, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterSlideNumber:Z

    .line 140
    .line 141
    if-nez v8, :cond_4

    .line 142
    .line 143
    invoke-virtual {v1, v5}, Lcom/intsig/office/pg/model/PGSlide;->getTextboxByPlaceHolderID(I)Lcom/intsig/office/common/shape/IShape;

    .line 144
    .line 145
    .line 146
    move-result-object v5

    .line 147
    check-cast v5, Lcom/intsig/office/common/shape/TextBox;

    .line 148
    .line 149
    if-eqz v5, :cond_4

    .line 150
    .line 151
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getSlideNo()I

    .line 152
    .line 153
    .line 154
    move-result v8

    .line 155
    iget-object v9, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 156
    .line 157
    invoke-virtual {v9}, Lcom/intsig/office/pg/model/PGModel;->getSlideNumberOffset()I

    .line 158
    .line 159
    .line 160
    move-result v9

    .line 161
    add-int/2addr v8, v9

    .line 162
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v8

    .line 166
    invoke-direct {p0, v5, v8}, Lcom/intsig/office/fc/ppt/PPTReader;->processCurrentSlideHeadersFooters(Lcom/intsig/office/common/shape/TextBox;Ljava/lang/String;)Lcom/intsig/office/common/shape/TextBox;

    .line 167
    .line 168
    .line 169
    move-result-object v5

    .line 170
    invoke-virtual {v0, v5}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 171
    .line 172
    .line 173
    :cond_4
    iget-boolean v5, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterFooter:Z

    .line 174
    .line 175
    if-nez v5, :cond_5

    .line 176
    .line 177
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->isFooterVisible()Z

    .line 178
    .line 179
    .line 180
    move-result v5

    .line 181
    if-eqz v5, :cond_5

    .line 182
    .line 183
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 184
    .line 185
    .line 186
    move-result-object v5

    .line 187
    if-eqz v5, :cond_5

    .line 188
    .line 189
    invoke-virtual {v1, v3}, Lcom/intsig/office/pg/model/PGSlide;->getTextboxByPlaceHolderID(I)Lcom/intsig/office/common/shape/IShape;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    check-cast v3, Lcom/intsig/office/common/shape/TextBox;

    .line 194
    .line 195
    if-eqz v3, :cond_5

    .line 196
    .line 197
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v5

    .line 201
    invoke-direct {p0, v3, v5}, Lcom/intsig/office/fc/ppt/PPTReader;->processCurrentSlideHeadersFooters(Lcom/intsig/office/common/shape/TextBox;Ljava/lang/String;)Lcom/intsig/office/common/shape/TextBox;

    .line 202
    .line 203
    .line 204
    move-result-object v3

    .line 205
    invoke-virtual {v0, v3}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 206
    .line 207
    .line 208
    :cond_5
    iget-boolean v3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterDateTime:Z

    .line 209
    .line 210
    if-nez v3, :cond_6

    .line 211
    .line 212
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->isUserDateVisible()Z

    .line 213
    .line 214
    .line 215
    move-result v3

    .line 216
    if-eqz v3, :cond_6

    .line 217
    .line 218
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getDateTimeText()Ljava/lang/String;

    .line 219
    .line 220
    .line 221
    move-result-object v3

    .line 222
    if-eqz v3, :cond_6

    .line 223
    .line 224
    invoke-virtual {v1, v7}, Lcom/intsig/office/pg/model/PGSlide;->getTextboxByPlaceHolderID(I)Lcom/intsig/office/common/shape/IShape;

    .line 225
    .line 226
    .line 227
    move-result-object v1

    .line 228
    check-cast v1, Lcom/intsig/office/common/shape/TextBox;

    .line 229
    .line 230
    if-eqz v1, :cond_c

    .line 231
    .line 232
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getDateTimeText()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object v2

    .line 236
    invoke-direct {p0, v1, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->processCurrentSlideHeadersFooters(Lcom/intsig/office/common/shape/TextBox;Ljava/lang/String;)Lcom/intsig/office/common/shape/TextBox;

    .line 237
    .line 238
    .line 239
    move-result-object v1

    .line 240
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 241
    .line 242
    .line 243
    goto/16 :goto_1

    .line 244
    .line 245
    :cond_6
    iget-boolean v3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterDateTime:Z

    .line 246
    .line 247
    if-nez v3, :cond_c

    .line 248
    .line 249
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->isDateTimeVisible()Z

    .line 250
    .line 251
    .line 252
    move-result v2

    .line 253
    if-eqz v2, :cond_c

    .line 254
    .line 255
    invoke-static {}, Lcom/intsig/office/ss/util/format/NumericFormatter;->instance()Lcom/intsig/office/ss/util/format/NumericFormatter;

    .line 256
    .line 257
    .line 258
    move-result-object v2

    .line 259
    new-instance v3, Ljava/util/Date;

    .line 260
    .line 261
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 262
    .line 263
    .line 264
    move-result-wide v8

    .line 265
    invoke-direct {v3, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 266
    .line 267
    .line 268
    const-string v5, "yyyy/m/d"

    .line 269
    .line 270
    invoke-virtual {v2, v5, v3}, Lcom/intsig/office/ss/util/format/NumericFormatter;->getFormatContents(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    .line 271
    .line 272
    .line 273
    move-result-object v2

    .line 274
    invoke-virtual {v1, v7}, Lcom/intsig/office/pg/model/PGSlide;->getTextboxByPlaceHolderID(I)Lcom/intsig/office/common/shape/IShape;

    .line 275
    .line 276
    .line 277
    move-result-object v1

    .line 278
    check-cast v1, Lcom/intsig/office/common/shape/TextBox;

    .line 279
    .line 280
    if-eqz v1, :cond_c

    .line 281
    .line 282
    invoke-virtual {v1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 283
    .line 284
    .line 285
    move-result-object v3

    .line 286
    if-eqz v3, :cond_c

    .line 287
    .line 288
    invoke-direct {p0, v1, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->processCurrentSlideHeadersFooters(Lcom/intsig/office/common/shape/TextBox;Ljava/lang/String;)Lcom/intsig/office/common/shape/TextBox;

    .line 289
    .line 290
    .line 291
    move-result-object v1

    .line 292
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 293
    .line 294
    .line 295
    goto :goto_1

    .line 296
    :cond_7
    iget-boolean v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterSlideNumber:Z

    .line 297
    .line 298
    if-nez v2, :cond_8

    .line 299
    .line 300
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 301
    .line 302
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->isSlideNumberVisible()Z

    .line 303
    .line 304
    .line 305
    move-result v2

    .line 306
    if-eqz v2, :cond_8

    .line 307
    .line 308
    invoke-virtual {v1, v5}, Lcom/intsig/office/pg/model/PGSlide;->getTextboxByPlaceHolderID(I)Lcom/intsig/office/common/shape/IShape;

    .line 309
    .line 310
    .line 311
    move-result-object v2

    .line 312
    check-cast v2, Lcom/intsig/office/common/shape/TextBox;

    .line 313
    .line 314
    if-eqz v2, :cond_8

    .line 315
    .line 316
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getSlideNo()I

    .line 317
    .line 318
    .line 319
    move-result v5

    .line 320
    iget-object v8, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 321
    .line 322
    invoke-virtual {v8}, Lcom/intsig/office/pg/model/PGModel;->getSlideNumberOffset()I

    .line 323
    .line 324
    .line 325
    move-result v8

    .line 326
    add-int/2addr v5, v8

    .line 327
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 328
    .line 329
    .line 330
    move-result-object v5

    .line 331
    invoke-direct {p0, v2, v5}, Lcom/intsig/office/fc/ppt/PPTReader;->processCurrentSlideHeadersFooters(Lcom/intsig/office/common/shape/TextBox;Ljava/lang/String;)Lcom/intsig/office/common/shape/TextBox;

    .line 332
    .line 333
    .line 334
    move-result-object v2

    .line 335
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 336
    .line 337
    .line 338
    :cond_8
    iget-boolean v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterFooter:Z

    .line 339
    .line 340
    if-nez v2, :cond_9

    .line 341
    .line 342
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 343
    .line 344
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->isFooterVisible()Z

    .line 345
    .line 346
    .line 347
    move-result v2

    .line 348
    if-eqz v2, :cond_9

    .line 349
    .line 350
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 351
    .line 352
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 353
    .line 354
    .line 355
    move-result-object v2

    .line 356
    if-eqz v2, :cond_9

    .line 357
    .line 358
    invoke-virtual {v1, v3}, Lcom/intsig/office/pg/model/PGSlide;->getTextboxByPlaceHolderID(I)Lcom/intsig/office/common/shape/IShape;

    .line 359
    .line 360
    .line 361
    move-result-object v2

    .line 362
    check-cast v2, Lcom/intsig/office/common/shape/TextBox;

    .line 363
    .line 364
    if-eqz v2, :cond_9

    .line 365
    .line 366
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 367
    .line 368
    .line 369
    :cond_9
    iget-boolean v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterDateTime:Z

    .line 370
    .line 371
    if-nez v2, :cond_c

    .line 372
    .line 373
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 374
    .line 375
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getDateTimeText()Ljava/lang/String;

    .line 376
    .line 377
    .line 378
    move-result-object v2

    .line 379
    if-eqz v2, :cond_a

    .line 380
    .line 381
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 382
    .line 383
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->isUserDateVisible()Z

    .line 384
    .line 385
    .line 386
    move-result v2

    .line 387
    if-nez v2, :cond_b

    .line 388
    .line 389
    :cond_a
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 390
    .line 391
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->isDateTimeVisible()Z

    .line 392
    .line 393
    .line 394
    move-result v2

    .line 395
    if-eqz v2, :cond_c

    .line 396
    .line 397
    :cond_b
    invoke-virtual {v1, v7}, Lcom/intsig/office/pg/model/PGSlide;->getTextboxByPlaceHolderID(I)Lcom/intsig/office/common/shape/IShape;

    .line 398
    .line 399
    .line 400
    move-result-object v1

    .line 401
    check-cast v1, Lcom/intsig/office/common/shape/TextBox;

    .line 402
    .line 403
    if-eqz v1, :cond_c

    .line 404
    .line 405
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 406
    .line 407
    .line 408
    :cond_c
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Slide;->getNotesSheet()Lcom/intsig/office/fc/hslf/model/Notes;

    .line 409
    .line 410
    .line 411
    move-result-object v1

    .line 412
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->processNotes(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Notes;)V

    .line 413
    .line 414
    .line 415
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/ppt/PPTReader;->processGroupShape(Lcom/intsig/office/pg/model/PGSlide;)V

    .line 416
    .line 417
    .line 418
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideShowSlideInfoAtom()Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;

    .line 419
    .line 420
    .line 421
    move-result-object v1

    .line 422
    if-eqz v1, :cond_d

    .line 423
    .line 424
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/record/SlideShowSlideInfoAtom;->isValidateTransition()Z

    .line 425
    .line 426
    .line 427
    move-result v1

    .line 428
    if-eqz v1, :cond_d

    .line 429
    .line 430
    const/4 v4, 0x1

    .line 431
    :cond_d
    invoke-virtual {v0, v4}, Lcom/intsig/office/pg/model/PGSlide;->setTransition(Z)V

    .line 432
    .line 433
    .line 434
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideProgTagsContainer()Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;

    .line 435
    .line 436
    .line 437
    move-result-object p1

    .line 438
    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/ppt/PPTReader;->processSlideshow(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;)V

    .line 439
    .line 440
    .line 441
    iget-object p1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 442
    .line 443
    invoke-virtual {p1, v0}, Lcom/intsig/office/pg/model/PGModel;->appendSlide(Lcom/intsig/office/pg/model/PGSlide;)V

    .line 444
    .line 445
    .line 446
    iget-boolean p1, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 447
    .line 448
    if-nez p1, :cond_e

    .line 449
    .line 450
    iget-object p1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 451
    .line 452
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGModel;->getSlideCount()I

    .line 453
    .line 454
    .line 455
    move-result p1

    .line 456
    if-eqz p1, :cond_e

    .line 457
    .line 458
    iget p1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->currentReaderIndex:I

    .line 459
    .line 460
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 461
    .line 462
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlideCount()I

    .line 463
    .line 464
    .line 465
    move-result v0

    .line 466
    if-lt p1, v0, :cond_f

    .line 467
    .line 468
    :cond_e
    iget-object p1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->slideMasterIndexs:Ljava/util/Map;

    .line 469
    .line 470
    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 471
    .line 472
    .line 473
    iput-object v6, p0, Lcom/intsig/office/fc/ppt/PPTReader;->slideMasterIndexs:Ljava/util/Map;

    .line 474
    .line 475
    iget-object p1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->titleMasterIndexs:Ljava/util/Map;

    .line 476
    .line 477
    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 478
    .line 479
    .line 480
    iput-object v6, p0, Lcom/intsig/office/fc/ppt/PPTReader;->titleMasterIndexs:Ljava/util/Map;

    .line 481
    .line 482
    :cond_f
    return-void
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method private processSlideshow(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/SlideProgTagsContainer;)V
    .locals 4

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 5
    .line 6
    .line 7
    move-result-object p2

    .line 8
    if-eqz p2, :cond_6

    .line 9
    .line 10
    array-length v0, p2

    .line 11
    const/4 v1, 0x1

    .line 12
    if-lt v0, v1, :cond_6

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    aget-object p2, p2, v0

    .line 16
    .line 17
    instance-of v1, p2, Lcom/intsig/office/fc/hslf/record/SlideProgBinaryTagContainer;

    .line 18
    .line 19
    if-nez v1, :cond_1

    .line 20
    .line 21
    goto :goto_2

    .line 22
    :cond_1
    check-cast p2, Lcom/intsig/office/fc/hslf/record/SlideProgBinaryTagContainer;

    .line 23
    .line 24
    sget-wide v1, Lcom/intsig/office/fc/hslf/record/BinaryTagDataBlob;->RECORD_ID:J

    .line 25
    .line 26
    invoke-virtual {p2, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    if-nez p2, :cond_2

    .line 31
    .line 32
    return-void

    .line 33
    :cond_2
    check-cast p2, Lcom/intsig/office/fc/hslf/record/BinaryTagDataBlob;

    .line 34
    .line 35
    sget-wide v1, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;->RECORD_ID:J

    .line 36
    .line 37
    invoke-virtual {p2, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    if-nez p2, :cond_3

    .line 42
    .line 43
    return-void

    .line 44
    :cond_3
    check-cast p2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 45
    .line 46
    sget-wide v1, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;->RECORD_ID:J

    .line 47
    .line 48
    invoke-virtual {p2, v1, v2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->findFirstOfType(J)Lcom/intsig/office/fc/hslf/record/Record;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    if-nez p2, :cond_4

    .line 53
    .line 54
    return-void

    .line 55
    :cond_4
    check-cast p2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 56
    .line 57
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/RecordContainer;->getChildRecords()[Lcom/intsig/office/fc/hslf/record/Record;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    if-eqz p2, :cond_7

    .line 62
    .line 63
    array-length v1, p2

    .line 64
    :goto_0
    if-ge v0, v1, :cond_7

    .line 65
    .line 66
    aget-object v2, p2, v0

    .line 67
    .line 68
    instance-of v3, v2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 69
    .line 70
    if-eqz v3, :cond_5

    .line 71
    .line 72
    check-cast v2, Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;

    .line 73
    .line 74
    invoke-direct {p0, p1, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->processAnimation(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/record/TimeNodeContainer;)Ljava/util/List;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    if-eqz v2, :cond_5

    .line 79
    .line 80
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 85
    .line 86
    .line 87
    move-result v3

    .line 88
    if-eqz v3, :cond_5

    .line 89
    .line 90
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    move-result-object v3

    .line 94
    check-cast v3, Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 95
    .line 96
    invoke-virtual {p1, v3}, Lcom/intsig/office/pg/model/PGSlide;->addShapeAnimation(Lcom/intsig/office/pg/animate/ShapeAnimation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .line 98
    .line 99
    goto :goto_1

    .line 100
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_6
    :goto_2
    return-void

    .line 104
    :catch_0
    move-exception p1

    .line 105
    iget-object p2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 106
    .line 107
    invoke-interface {p2}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 108
    .line 109
    .line 110
    move-result-object p2

    .line 111
    invoke-virtual {p2}, Lcom/intsig/office/system/SysKit;->getErrorKit()Lcom/intsig/office/system/ErrorUtil;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    invoke-virtual {p2, p1}, Lcom/intsig/office/system/ErrorUtil;->writerLog(Ljava/lang/Throwable;)V

    .line 116
    .line 117
    .line 118
    :cond_7
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processTable(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Table;Lcom/intsig/office/common/shape/GroupShape;I)V
    .locals 30

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move-object/from16 v7, p1

    .line 4
    .line 5
    move-object/from16 v8, p2

    .line 6
    .line 7
    move-object/from16 v9, p3

    .line 8
    .line 9
    invoke-virtual {v8, v8}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getClientAnchor2D(Lcom/intsig/office/fc/hslf/model/Shape;)Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 10
    .line 11
    .line 12
    move-result-object v10

    .line 13
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getCoordinates()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 14
    .line 15
    .line 16
    move-result-object v11

    .line 17
    const/4 v12, 0x1

    .line 18
    iput-boolean v12, v6, Lcom/intsig/office/fc/ppt/PPTReader;->tableShape:Z

    .line 19
    .line 20
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Table;->getNumberOfRows()I

    .line 21
    .line 22
    .line 23
    move-result v13

    .line 24
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Table;->getNumberOfColumns()I

    .line 25
    .line 26
    .line 27
    move-result v14

    .line 28
    new-instance v15, Lcom/intsig/office/common/shape/TableShape;

    .line 29
    .line 30
    invoke-direct {v15, v13, v14}, Lcom/intsig/office/common/shape/TableShape;-><init>(II)V

    .line 31
    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    :goto_0
    const-wide v16, 0x3ff5555560000000L    # 1.3333333730697632

    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    if-ge v4, v13, :cond_5

    .line 40
    .line 41
    const/4 v3, 0x0

    .line 42
    :goto_1
    if-ge v3, v14, :cond_4

    .line 43
    .line 44
    iget-boolean v0, v6, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 45
    .line 46
    if-eqz v0, :cond_0

    .line 47
    .line 48
    return-void

    .line 49
    :cond_0
    invoke-virtual {v8, v4, v3}, Lcom/intsig/office/fc/hslf/model/Table;->getCell(II)Lcom/intsig/office/fc/hslf/model/TableCell;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    if-eqz v2, :cond_3

    .line 54
    .line 55
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    if-eqz v0, :cond_3

    .line 60
    .line 61
    invoke-virtual {v11}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 62
    .line 63
    .line 64
    move-result-wide v18

    .line 65
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 66
    .line 67
    .line 68
    move-result-wide v20

    .line 69
    div-double v18, v18, v20

    .line 70
    .line 71
    invoke-virtual {v11}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 72
    .line 73
    .line 74
    move-result-wide v20

    .line 75
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 76
    .line 77
    .line 78
    move-result-wide v22

    .line 79
    div-double v20, v20, v22

    .line 80
    .line 81
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 82
    .line 83
    .line 84
    move-result-wide v22

    .line 85
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 86
    .line 87
    .line 88
    move-result-wide v24

    .line 89
    invoke-virtual {v11}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 90
    .line 91
    .line 92
    move-result-wide v26

    .line 93
    sub-double v24, v24, v26

    .line 94
    .line 95
    div-double v24, v24, v18

    .line 96
    .line 97
    add-double v22, v22, v24

    .line 98
    .line 99
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 100
    .line 101
    .line 102
    move-result-wide v24

    .line 103
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 104
    .line 105
    .line 106
    move-result-wide v26

    .line 107
    invoke-virtual {v11}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 108
    .line 109
    .line 110
    move-result-wide v28

    .line 111
    sub-double v26, v26, v28

    .line 112
    .line 113
    div-double v26, v26, v20

    .line 114
    .line 115
    add-double v24, v24, v26

    .line 116
    .line 117
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 118
    .line 119
    .line 120
    move-result-wide v26

    .line 121
    div-double v26, v26, v18

    .line 122
    .line 123
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 124
    .line 125
    .line 126
    move-result-wide v0

    .line 127
    div-double v0, v0, v20

    .line 128
    .line 129
    new-instance v5, Lcom/intsig/office/java/awt/Rectanglef;

    .line 130
    .line 131
    invoke-direct {v5}, Lcom/intsig/office/java/awt/Rectanglef;-><init>()V

    .line 132
    .line 133
    .line 134
    move/from16 v20, v13

    .line 135
    .line 136
    mul-double v12, v22, v16

    .line 137
    .line 138
    double-to-float v12, v12

    .line 139
    invoke-virtual {v5, v12}, Lcom/intsig/office/java/awt/Rectanglef;->setX(F)V

    .line 140
    .line 141
    .line 142
    mul-double v12, v24, v16

    .line 143
    .line 144
    double-to-float v12, v12

    .line 145
    invoke-virtual {v5, v12}, Lcom/intsig/office/java/awt/Rectanglef;->setY(F)V

    .line 146
    .line 147
    .line 148
    mul-double v12, v26, v16

    .line 149
    .line 150
    double-to-float v12, v12

    .line 151
    invoke-virtual {v5, v12}, Lcom/intsig/office/java/awt/Rectanglef;->setWidth(F)V

    .line 152
    .line 153
    .line 154
    mul-double v0, v0, v16

    .line 155
    .line 156
    double-to-float v0, v0

    .line 157
    invoke-virtual {v5, v0}, Lcom/intsig/office/java/awt/Rectanglef;->setHeight(F)V

    .line 158
    .line 159
    .line 160
    new-instance v12, Lcom/intsig/office/common/shape/TableCell;

    .line 161
    .line 162
    invoke-direct {v12}, Lcom/intsig/office/common/shape/TableCell;-><init>()V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v12, v5}, Lcom/intsig/office/common/shape/TableCell;->setBounds(Lcom/intsig/office/java/awt/Rectanglef;)V

    .line 166
    .line 167
    .line 168
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TableCell;->getBorderLeft()Lcom/intsig/office/fc/hslf/model/Line;

    .line 169
    .line 170
    .line 171
    move-result-object v0

    .line 172
    const/4 v1, 0x1

    .line 173
    invoke-direct {v6, v0, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;Z)Lcom/intsig/office/common/borders/Line;

    .line 174
    .line 175
    .line 176
    move-result-object v0

    .line 177
    invoke-virtual {v12, v0}, Lcom/intsig/office/common/shape/TableCell;->setLeftLine(Lcom/intsig/office/common/borders/Line;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TableCell;->getBorderRight()Lcom/intsig/office/fc/hslf/model/Line;

    .line 181
    .line 182
    .line 183
    move-result-object v0

    .line 184
    invoke-direct {v6, v0, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;Z)Lcom/intsig/office/common/borders/Line;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    invoke-virtual {v12, v0}, Lcom/intsig/office/common/shape/TableCell;->setRightLine(Lcom/intsig/office/common/borders/Line;)V

    .line 189
    .line 190
    .line 191
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TableCell;->getBorderTop()Lcom/intsig/office/fc/hslf/model/Line;

    .line 192
    .line 193
    .line 194
    move-result-object v0

    .line 195
    invoke-direct {v6, v0, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;Z)Lcom/intsig/office/common/borders/Line;

    .line 196
    .line 197
    .line 198
    move-result-object v0

    .line 199
    invoke-virtual {v12, v0}, Lcom/intsig/office/common/shape/TableCell;->setTopLine(Lcom/intsig/office/common/borders/Line;)V

    .line 200
    .line 201
    .line 202
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TableCell;->getBorderBottom()Lcom/intsig/office/fc/hslf/model/Line;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    invoke-direct {v6, v0, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;Z)Lcom/intsig/office/common/borders/Line;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    invoke-virtual {v12, v0}, Lcom/intsig/office/common/shape/TableCell;->setBottomLine(Lcom/intsig/office/common/borders/Line;)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getFill()Lcom/intsig/office/fc/hslf/model/Fill;

    .line 214
    .line 215
    .line 216
    move-result-object v0

    .line 217
    invoke-direct {v6, v7, v0}, Lcom/intsig/office/fc/ppt/PPTReader;->converFill(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Fill;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 218
    .line 219
    .line 220
    move-result-object v0

    .line 221
    invoke-virtual {v12, v0}, Lcom/intsig/office/common/shape/TableCell;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getText()Ljava/lang/String;

    .line 225
    .line 226
    .line 227
    move-result-object v0

    .line 228
    if-eqz v0, :cond_1

    .line 229
    .line 230
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 231
    .line 232
    .line 233
    move-result-object v0

    .line 234
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 235
    .line 236
    .line 237
    move-result v0

    .line 238
    if-lez v0, :cond_1

    .line 239
    .line 240
    new-instance v13, Lcom/intsig/office/common/shape/TextBox;

    .line 241
    .line 242
    invoke-direct {v13}, Lcom/intsig/office/common/shape/TextBox;-><init>()V

    .line 243
    .line 244
    .line 245
    new-instance v1, Lcom/intsig/office/java/awt/Rectangle;

    .line 246
    .line 247
    invoke-virtual {v5}, Lcom/intsig/office/java/awt/Rectanglef;->getX()F

    .line 248
    .line 249
    .line 250
    move-result v0

    .line 251
    float-to-int v0, v0

    .line 252
    move-object/from16 v21, v2

    .line 253
    .line 254
    invoke-virtual {v5}, Lcom/intsig/office/java/awt/Rectanglef;->getY()F

    .line 255
    .line 256
    .line 257
    move-result v2

    .line 258
    float-to-int v2, v2

    .line 259
    move/from16 v22, v3

    .line 260
    .line 261
    invoke-virtual {v5}, Lcom/intsig/office/java/awt/Rectanglef;->getWidth()F

    .line 262
    .line 263
    .line 264
    move-result v3

    .line 265
    float-to-int v3, v3

    .line 266
    invoke-virtual {v5}, Lcom/intsig/office/java/awt/Rectanglef;->getHeight()F

    .line 267
    .line 268
    .line 269
    move-result v5

    .line 270
    float-to-int v5, v5

    .line 271
    invoke-direct {v1, v0, v2, v3, v5}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 272
    .line 273
    .line 274
    const/4 v5, -0x1

    .line 275
    move-object/from16 v0, p0

    .line 276
    .line 277
    move-object v3, v1

    .line 278
    move-object v1, v13

    .line 279
    move-object/from16 v23, v21

    .line 280
    .line 281
    move-object/from16 v2, v23

    .line 282
    .line 283
    move/from16 v21, v22

    .line 284
    .line 285
    move/from16 v22, v4

    .line 286
    .line 287
    move/from16 v4, p4

    .line 288
    .line 289
    const/4 v8, 0x0

    .line 290
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/ppt/PPTReader;->processTextShape(Lcom/intsig/office/common/shape/TextBox;Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/java/awt/Rectangle;II)V

    .line 291
    .line 292
    .line 293
    invoke-virtual {v13}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 294
    .line 295
    .line 296
    move-result-object v0

    .line 297
    if-eqz v0, :cond_2

    .line 298
    .line 299
    move-object/from16 v0, v23

    .line 300
    .line 301
    invoke-virtual {v6, v0, v13}, Lcom/intsig/office/fc/ppt/PPTReader;->processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V

    .line 302
    .line 303
    .line 304
    invoke-virtual {v12, v13}, Lcom/intsig/office/common/shape/TableCell;->setText(Lcom/intsig/office/common/shape/TextBox;)V

    .line 305
    .line 306
    .line 307
    goto :goto_2

    .line 308
    :cond_1
    move/from16 v21, v3

    .line 309
    .line 310
    move/from16 v22, v4

    .line 311
    .line 312
    const/4 v8, 0x0

    .line 313
    :cond_2
    :goto_2
    mul-int v4, v22, v14

    .line 314
    .line 315
    add-int v4, v4, v21

    .line 316
    .line 317
    invoke-virtual {v15, v4, v12}, Lcom/intsig/office/common/shape/TableShape;->addCell(ILcom/intsig/office/common/shape/TableCell;)V

    .line 318
    .line 319
    .line 320
    goto :goto_3

    .line 321
    :cond_3
    move/from16 v21, v3

    .line 322
    .line 323
    move/from16 v22, v4

    .line 324
    .line 325
    move/from16 v20, v13

    .line 326
    .line 327
    const/4 v8, 0x0

    .line 328
    :goto_3
    add-int/lit8 v3, v21, 0x1

    .line 329
    .line 330
    move-object/from16 v8, p2

    .line 331
    .line 332
    move/from16 v13, v20

    .line 333
    .line 334
    move/from16 v4, v22

    .line 335
    .line 336
    const/4 v12, 0x1

    .line 337
    goto/16 :goto_1

    .line 338
    .line 339
    :cond_4
    move/from16 v22, v4

    .line 340
    .line 341
    move/from16 v20, v13

    .line 342
    .line 343
    const/4 v8, 0x0

    .line 344
    add-int/lit8 v4, v22, 0x1

    .line 345
    .line 346
    move-object/from16 v8, p2

    .line 347
    .line 348
    const/4 v12, 0x1

    .line 349
    goto/16 :goto_0

    .line 350
    .line 351
    :cond_5
    const/4 v8, 0x0

    .line 352
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/Table;->getTableBorders()[Lcom/intsig/office/fc/hslf/model/Line;

    .line 353
    .line 354
    .line 355
    move-result-object v0

    .line 356
    array-length v1, v0

    .line 357
    const/4 v5, 0x0

    .line 358
    :goto_4
    if-ge v5, v1, :cond_9

    .line 359
    .line 360
    aget-object v2, v0, v5

    .line 361
    .line 362
    const/4 v3, 0x1

    .line 363
    invoke-direct {v6, v2, v3}, Lcom/intsig/office/fc/ppt/PPTReader;->getShapeLine(Lcom/intsig/office/fc/hslf/model/SimpleShape;Z)Lcom/intsig/office/common/borders/Line;

    .line 364
    .line 365
    .line 366
    move-result-object v4

    .line 367
    if-eqz v4, :cond_8

    .line 368
    .line 369
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 370
    .line 371
    .line 372
    move-result-object v3

    .line 373
    if-nez v3, :cond_6

    .line 374
    .line 375
    return-void

    .line 376
    :cond_6
    new-instance v11, Lcom/intsig/office/java/awt/Rectangle;

    .line 377
    .line 378
    invoke-direct {v11}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 379
    .line 380
    .line 381
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 382
    .line 383
    .line 384
    move-result-wide v12

    .line 385
    mul-double v12, v12, v16

    .line 386
    .line 387
    double-to-int v12, v12

    .line 388
    iput v12, v11, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 389
    .line 390
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 391
    .line 392
    .line 393
    move-result-wide v12

    .line 394
    mul-double v12, v12, v16

    .line 395
    .line 396
    double-to-int v12, v12

    .line 397
    iput v12, v11, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 398
    .line 399
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 400
    .line 401
    .line 402
    move-result-wide v12

    .line 403
    mul-double v12, v12, v16

    .line 404
    .line 405
    double-to-int v12, v12

    .line 406
    iput v12, v11, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 407
    .line 408
    invoke-virtual {v3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 409
    .line 410
    .line 411
    move-result-wide v12

    .line 412
    mul-double v12, v12, v16

    .line 413
    .line 414
    double-to-int v3, v12

    .line 415
    iput v3, v11, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 416
    .line 417
    new-instance v3, Lcom/intsig/office/common/shape/LineShape;

    .line 418
    .line 419
    invoke-direct {v3}, Lcom/intsig/office/common/shape/LineShape;-><init>()V

    .line 420
    .line 421
    .line 422
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 423
    .line 424
    .line 425
    move-result v12

    .line 426
    invoke-virtual {v3, v12}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 427
    .line 428
    .line 429
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 430
    .line 431
    .line 432
    invoke-virtual {v3, v4}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 433
    .line 434
    .line 435
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Line;->getAdjustmentValue()[Ljava/lang/Float;

    .line 436
    .line 437
    .line 438
    move-result-object v4

    .line 439
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/AutoShape;->getShapeType()I

    .line 440
    .line 441
    .line 442
    move-result v11

    .line 443
    const/16 v12, 0x21

    .line 444
    .line 445
    if-ne v11, v12, :cond_7

    .line 446
    .line 447
    if-nez v4, :cond_7

    .line 448
    .line 449
    const/4 v4, 0x1

    .line 450
    new-array v11, v4, [Ljava/lang/Float;

    .line 451
    .line 452
    const/high16 v12, 0x3f800000    # 1.0f

    .line 453
    .line 454
    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 455
    .line 456
    .line 457
    move-result-object v12

    .line 458
    aput-object v12, v11, v8

    .line 459
    .line 460
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 461
    .line 462
    .line 463
    goto :goto_5

    .line 464
    :cond_7
    const/4 v4, 0x1

    .line 465
    const/4 v11, 0x0

    .line 466
    invoke-virtual {v3, v11}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 467
    .line 468
    .line 469
    :goto_5
    invoke-virtual {v6, v2, v3}, Lcom/intsig/office/fc/ppt/PPTReader;->processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V

    .line 470
    .line 471
    .line 472
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeId()I

    .line 473
    .line 474
    .line 475
    move-result v2

    .line 476
    invoke-virtual {v3, v2}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 477
    .line 478
    .line 479
    invoke-virtual {v7, v3}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 480
    .line 481
    .line 482
    goto :goto_6

    .line 483
    :cond_8
    const/4 v4, 0x1

    .line 484
    :goto_6
    add-int/lit8 v5, v5, 0x1

    .line 485
    .line 486
    goto/16 :goto_4

    .line 487
    .line 488
    :cond_9
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 489
    .line 490
    invoke-direct {v0}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 491
    .line 492
    .line 493
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 494
    .line 495
    .line 496
    move-result-wide v1

    .line 497
    mul-double v1, v1, v16

    .line 498
    .line 499
    double-to-int v1, v1

    .line 500
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 501
    .line 502
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 503
    .line 504
    .line 505
    move-result-wide v1

    .line 506
    mul-double v1, v1, v16

    .line 507
    .line 508
    double-to-int v1, v1

    .line 509
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 510
    .line 511
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 512
    .line 513
    .line 514
    move-result-wide v1

    .line 515
    mul-double v1, v1, v16

    .line 516
    .line 517
    double-to-int v1, v1

    .line 518
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 519
    .line 520
    invoke-virtual {v10}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 521
    .line 522
    .line 523
    move-result-wide v1

    .line 524
    mul-double v1, v1, v16

    .line 525
    .line 526
    double-to-int v1, v1

    .line 527
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 528
    .line 529
    invoke-virtual {v15, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 530
    .line 531
    .line 532
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getShapeId()I

    .line 533
    .line 534
    .line 535
    move-result v0

    .line 536
    invoke-virtual {v15, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 537
    .line 538
    .line 539
    invoke-virtual {v15, v8}, Lcom/intsig/office/common/shape/TableShape;->setTable07(Z)V

    .line 540
    .line 541
    .line 542
    if-nez v9, :cond_a

    .line 543
    .line 544
    invoke-virtual {v7, v15}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 545
    .line 546
    .line 547
    goto :goto_7

    .line 548
    :cond_a
    invoke-virtual {v9, v15}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 549
    .line 550
    .line 551
    :goto_7
    iput-boolean v8, v6, Lcom/intsig/office/fc/ppt/PPTReader;->tableShape:Z

    .line 552
    .line 553
    return-void
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
.end method

.method private processTextShape(Lcom/intsig/office/common/shape/TextBox;Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/java/awt/Rectangle;II)V
    .locals 8

    .line 1
    if-nez p3, :cond_1

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/SimpleShape;->getLogicalAnchor2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 4
    .line 5
    .line 6
    move-result-object p3

    .line 7
    if-nez p3, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 11
    .line 12
    invoke-direct {v0}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 16
    .line 17
    .line 18
    move-result-wide v1

    .line 19
    const-wide v3, 0x3ff5555560000000L    # 1.3333333730697632

    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    mul-double v1, v1, v3

    .line 25
    .line 26
    double-to-int v1, v1

    .line 27
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 28
    .line 29
    invoke-virtual {p3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 30
    .line 31
    .line 32
    move-result-wide v1

    .line 33
    mul-double v1, v1, v3

    .line 34
    .line 35
    double-to-int v1, v1

    .line 36
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 37
    .line 38
    invoke-virtual {p3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 39
    .line 40
    .line 41
    move-result-wide v1

    .line 42
    mul-double v1, v1, v3

    .line 43
    .line 44
    double-to-int v1, v1

    .line 45
    iput v1, v0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 46
    .line 47
    invoke-virtual {p3}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 48
    .line 49
    .line 50
    move-result-wide v1

    .line 51
    mul-double v1, v1, v3

    .line 52
    .line 53
    double-to-int p3, v1

    .line 54
    iput p3, v0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 55
    .line 56
    move-object v5, v0

    .line 57
    goto :goto_0

    .line 58
    :cond_1
    move-object v5, p3

    .line 59
    :goto_0
    invoke-virtual {p1, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getWordWrap()I

    .line 63
    .line 64
    .line 65
    move-result p3

    .line 66
    const/4 v0, 0x1

    .line 67
    if-nez p3, :cond_2

    .line 68
    .line 69
    const/4 p3, 0x1

    .line 70
    goto :goto_1

    .line 71
    :cond_2
    const/4 p3, 0x0

    .line 72
    :goto_1
    invoke-virtual {p1, p3}, Lcom/intsig/office/common/shape/TextBox;->setWrapLine(Z)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getText()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object p3

    .line 79
    if-eqz p3, :cond_3

    .line 80
    .line 81
    move-object v1, p0

    .line 82
    move-object v2, p1

    .line 83
    move-object v3, p2

    .line 84
    move-object v4, v5

    .line 85
    move v5, p4

    .line 86
    move v6, p5

    .line 87
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/fc/ppt/PPTReader;->processNormalTextShape(Lcom/intsig/office/common/shape/TextBox;Lcom/intsig/office/fc/hslf/model/TextShape;Lcom/intsig/office/java/awt/Rectangle;II)V

    .line 88
    .line 89
    .line 90
    goto :goto_2

    .line 91
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getUnicodeGeoText()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    if-eqz v4, :cond_4

    .line 96
    .line 97
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 98
    .line 99
    .line 100
    move-result p3

    .line 101
    if-lez p3, :cond_4

    .line 102
    .line 103
    invoke-virtual {p1, v0}, Lcom/intsig/office/common/shape/TextBox;->setWordArt(Z)V

    .line 104
    .line 105
    .line 106
    move-object v1, p0

    .line 107
    move-object v2, p1

    .line 108
    move-object v3, p2

    .line 109
    move v6, p4

    .line 110
    move v7, p5

    .line 111
    invoke-direct/range {v1 .. v7}, Lcom/intsig/office/fc/ppt/PPTReader;->processWordArtTextShape(Lcom/intsig/office/common/shape/TextBox;Lcom/intsig/office/fc/hslf/model/TextShape;Ljava/lang/String;Lcom/intsig/office/java/awt/Rectangle;II)V

    .line 112
    .line 113
    .line 114
    :cond_4
    :goto_2
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private processWordArtParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Ljava/lang/String;III)V
    .locals 8

    .line 1
    new-instance v0, Lcom/intsig/office/simpletext/model/ParagraphElement;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/ParagraphElement;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 7
    .line 8
    int-to-long v1, v1

    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    const/4 v3, 0x1

    .line 21
    invoke-virtual {v2, v1, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 22
    .line 23
    .line 24
    new-instance v1, Lcom/intsig/office/simpletext/model/LeafElement;

    .line 25
    .line 26
    invoke-direct {v1, p2}, Lcom/intsig/office/simpletext/model/LeafElement;-><init>(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-static {}, Lcom/intsig/office/common/PaintKit;->instance()Lcom/intsig/office/common/PaintKit;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    invoke-virtual {v4}, Lcom/intsig/office/common/PaintKit;->getPaint()Landroid/graphics/Paint;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    const/16 v5, 0xc

    .line 42
    .line 43
    int-to-float v6, v5

    .line 44
    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 48
    .line 49
    .line 50
    move-result-object v6

    .line 51
    :goto_0
    invoke-virtual {v4, p2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 52
    .line 53
    .line 54
    move-result v7

    .line 55
    float-to-int v7, v7

    .line 56
    if-ge v7, p3, :cond_0

    .line 57
    .line 58
    iget v7, v6, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 59
    .line 60
    iget v6, v6, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 61
    .line 62
    sub-float/2addr v7, v6

    .line 63
    float-to-double v6, v7

    .line 64
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    .line 65
    .line 66
    .line 67
    move-result-wide v6

    .line 68
    double-to-int v6, v6

    .line 69
    if-ge v6, p4, :cond_0

    .line 70
    .line 71
    add-int/lit8 v5, v5, 0x1

    .line 72
    .line 73
    int-to-float v6, v5

    .line 74
    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v4}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 78
    .line 79
    .line 80
    move-result-object v6

    .line 81
    goto :goto_0

    .line 82
    :cond_0
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 83
    .line 84
    .line 85
    move-result-object p3

    .line 86
    invoke-virtual {v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 87
    .line 88
    .line 89
    move-result-object p4

    .line 90
    sub-int/2addr v5, v3

    .line 91
    int-to-float v3, v5

    .line 92
    const/high16 v4, 0x3f400000    # 0.75f

    .line 93
    .line 94
    mul-float v3, v3, v4

    .line 95
    .line 96
    float-to-int v3, v3

    .line 97
    invoke-virtual {p3, p4, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 98
    .line 99
    .line 100
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 101
    .line 102
    .line 103
    move-result-object p3

    .line 104
    invoke-virtual {p3, v2, p5}, Lcom/intsig/office/simpletext/model/AttrManage;->setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 105
    .line 106
    .line 107
    const/16 p3, 0x12

    .line 108
    .line 109
    invoke-virtual {p0, p3}, Lcom/intsig/office/fc/ppt/PPTReader;->setMaxFontSize(I)V

    .line 110
    .line 111
    .line 112
    iget p3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 113
    .line 114
    int-to-long p3, p3

    .line 115
    invoke-virtual {v1, p3, p4}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 116
    .line 117
    .line 118
    iget p3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 119
    .line 120
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 121
    .line 122
    .line 123
    move-result p2

    .line 124
    add-int/2addr p3, p2

    .line 125
    iput p3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 126
    .line 127
    int-to-long p2, p3

    .line 128
    invoke-virtual {v1, p2, p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 129
    .line 130
    .line 131
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/ParagraphElement;->appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V

    .line 132
    .line 133
    .line 134
    iget p2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 135
    .line 136
    int-to-long p2, p2

    .line 137
    invoke-virtual {v0, p2, p3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 138
    .line 139
    .line 140
    const-wide/16 p2, 0x0

    .line 141
    .line 142
    invoke-virtual {p1, v0, p2, p3}, Lcom/intsig/office/simpletext/model/SectionElement;->appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V

    .line 143
    .line 144
    .line 145
    return-void
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private processWordArtTextShape(Lcom/intsig/office/common/shape/TextBox;Lcom/intsig/office/fc/hslf/model/TextShape;Ljava/lang/String;Lcom/intsig/office/java/awt/Rectangle;II)V
    .locals 10

    .line 1
    move-object v0, p0

    .line 2
    move-object v1, p3

    .line 3
    move-object v2, p4

    .line 4
    move v3, p5

    .line 5
    move/from16 v4, p6

    .line 6
    .line 7
    const/16 v5, 0x9

    .line 8
    .line 9
    const/4 v6, 0x0

    .line 10
    const/4 v7, 0x2

    .line 11
    const-string v8, "*"

    .line 12
    .line 13
    const/4 v9, 0x7

    .line 14
    if-ne v4, v5, :cond_2

    .line 15
    .line 16
    invoke-virtual {p3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v5

    .line 20
    if-eqz v5, :cond_2

    .line 21
    .line 22
    if-nez v3, :cond_0

    .line 23
    .line 24
    iget-object v3, v0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 25
    .line 26
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    if-eqz v3, :cond_4

    .line 31
    .line 32
    iget-object v1, v0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    if-ne v3, v7, :cond_4

    .line 40
    .line 41
    iget-object v1, v0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    if-eqz v1, :cond_1

    .line 48
    .line 49
    iget-object v1, v0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getFooterText()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    goto :goto_0

    .line 56
    :cond_1
    move-object v1, v6

    .line 57
    goto :goto_0

    .line 58
    :cond_2
    if-ne v4, v9, :cond_4

    .line 59
    .line 60
    invoke-virtual {p3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 61
    .line 62
    .line 63
    move-result v4

    .line 64
    if-eqz v4, :cond_4

    .line 65
    .line 66
    if-nez v3, :cond_3

    .line 67
    .line 68
    iget-object v3, v0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 69
    .line 70
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getDateTimeText()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    if-eqz v3, :cond_4

    .line 75
    .line 76
    iget-object v1, v0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 77
    .line 78
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getDateTimeText()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    goto :goto_0

    .line 83
    :cond_3
    if-ne v3, v7, :cond_4

    .line 84
    .line 85
    iget-object v1, v0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 86
    .line 87
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getDateTimeText()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    if-eqz v1, :cond_1

    .line 92
    .line 93
    iget-object v1, v0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 94
    .line 95
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/model/HeadersFooters;->getDateTimeText()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    :cond_4
    :goto_0
    new-instance v3, Lcom/intsig/office/simpletext/model/SectionElement;

    .line 100
    .line 101
    invoke-direct {v3}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    .line 102
    .line 103
    .line 104
    move-object v4, p1

    .line 105
    invoke-virtual {p1, v3}, Lcom/intsig/office/common/shape/TextBox;->setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 109
    .line 110
    .line 111
    move-result-object v4

    .line 112
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 113
    .line 114
    .line 115
    move-result-object v5

    .line 116
    iget v6, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 117
    .line 118
    int-to-float v6, v6

    .line 119
    const/high16 v7, 0x41700000    # 15.0f

    .line 120
    .line 121
    mul-float v6, v6, v7

    .line 122
    .line 123
    float-to-int v6, v6

    .line 124
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 125
    .line 126
    .line 127
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 128
    .line 129
    .line 130
    move-result-object v5

    .line 131
    iget v6, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 132
    .line 133
    int-to-float v6, v6

    .line 134
    mul-float v6, v6, v7

    .line 135
    .line 136
    float-to-int v6, v6

    .line 137
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 138
    .line 139
    .line 140
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 141
    .line 142
    .line 143
    move-result-object v5

    .line 144
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginLeft()F

    .line 145
    .line 146
    .line 147
    move-result v6

    .line 148
    const/high16 v7, 0x41a00000    # 20.0f

    .line 149
    .line 150
    mul-float v6, v6, v7

    .line 151
    .line 152
    float-to-int v6, v6

    .line 153
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 154
    .line 155
    .line 156
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 157
    .line 158
    .line 159
    move-result-object v5

    .line 160
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginRight()F

    .line 161
    .line 162
    .line 163
    move-result v6

    .line 164
    mul-float v6, v6, v7

    .line 165
    .line 166
    float-to-int v6, v6

    .line 167
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 168
    .line 169
    .line 170
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 171
    .line 172
    .line 173
    move-result-object v5

    .line 174
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginTop()F

    .line 175
    .line 176
    .line 177
    move-result v6

    .line 178
    mul-float v6, v6, v7

    .line 179
    .line 180
    float-to-int v6, v6

    .line 181
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 182
    .line 183
    .line 184
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 185
    .line 186
    .line 187
    move-result-object v5

    .line 188
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginBottom()F

    .line 189
    .line 190
    .line 191
    move-result v6

    .line 192
    mul-float v6, v6, v7

    .line 193
    .line 194
    float-to-int v6, v6

    .line 195
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 196
    .line 197
    .line 198
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 199
    .line 200
    .line 201
    move-result-object v5

    .line 202
    const/4 v6, 0x1

    .line 203
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 204
    .line 205
    .line 206
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    .line 207
    .line 208
    .line 209
    move-result-object v5

    .line 210
    invoke-virtual {v5, v4, v6}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V

    .line 211
    .line 212
    .line 213
    iget v4, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 214
    .line 215
    int-to-float v4, v4

    .line 216
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginLeft()F

    .line 217
    .line 218
    .line 219
    move-result v5

    .line 220
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginRight()F

    .line 221
    .line 222
    .line 223
    move-result v6

    .line 224
    add-float/2addr v5, v6

    .line 225
    const v6, 0x3faaaaab

    .line 226
    .line 227
    .line 228
    mul-float v5, v5, v6

    .line 229
    .line 230
    sub-float/2addr v4, v5

    .line 231
    float-to-int v4, v4

    .line 232
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 233
    .line 234
    int-to-float v2, v2

    .line 235
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginTop()F

    .line 236
    .line 237
    .line 238
    move-result v5

    .line 239
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/TextShape;->getMarginBottom()F

    .line 240
    .line 241
    .line 242
    move-result v7

    .line 243
    add-float/2addr v5, v7

    .line 244
    mul-float v5, v5, v6

    .line 245
    .line 246
    sub-float/2addr v2, v5

    .line 247
    float-to-int v2, v2

    .line 248
    const/4 v5, 0x0

    .line 249
    iput v5, v0, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 250
    .line 251
    int-to-long v6, v5

    .line 252
    invoke-virtual {v3, v6, v7}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 253
    .line 254
    .line 255
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/Shape;->getFill()Lcom/intsig/office/fc/hslf/model/Fill;

    .line 256
    .line 257
    .line 258
    move-result-object v6

    .line 259
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/Fill;->getFillType()I

    .line 260
    .line 261
    .line 262
    move-result v7

    .line 263
    if-nez v7, :cond_5

    .line 264
    .line 265
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/Fill;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 266
    .line 267
    .line 268
    move-result-object v5

    .line 269
    if-eqz v5, :cond_8

    .line 270
    .line 271
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/Fill;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 272
    .line 273
    .line 274
    move-result-object v5

    .line 275
    invoke-direct {p0, v5}, Lcom/intsig/office/fc/ppt/PPTReader;->converterColor(Lcom/intsig/office/java/awt/Color;)I

    .line 276
    .line 277
    .line 278
    move-result v5

    .line 279
    goto :goto_1

    .line 280
    :cond_5
    if-eq v7, v9, :cond_6

    .line 281
    .line 282
    const/4 v8, 0x4

    .line 283
    if-eq v7, v8, :cond_6

    .line 284
    .line 285
    const/4 v8, 0x5

    .line 286
    if-eq v7, v8, :cond_6

    .line 287
    .line 288
    const/4 v8, 0x6

    .line 289
    if-ne v7, v8, :cond_8

    .line 290
    .line 291
    :cond_6
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/Fill;->getForegroundColor()Lcom/intsig/office/java/awt/Color;

    .line 292
    .line 293
    .line 294
    move-result-object v7

    .line 295
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/Fill;->isShaderPreset()Z

    .line 296
    .line 297
    .line 298
    move-result v8

    .line 299
    if-eqz v8, :cond_8

    .line 300
    .line 301
    invoke-virtual {v6}, Lcom/intsig/office/fc/hslf/model/Fill;->getShaderColors()[I

    .line 302
    .line 303
    .line 304
    move-result-object v6

    .line 305
    if-eqz v6, :cond_7

    .line 306
    .line 307
    aget v5, v6, v5

    .line 308
    .line 309
    goto :goto_1

    .line 310
    :cond_7
    if-eqz v7, :cond_8

    .line 311
    .line 312
    invoke-virtual {v7}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 313
    .line 314
    .line 315
    move-result v5

    .line 316
    goto :goto_1

    .line 317
    :cond_8
    const/high16 v5, -0x1000000

    .line 318
    .line 319
    :goto_1
    move-object p1, p0

    .line 320
    move-object p2, v3

    .line 321
    move-object p3, v1

    .line 322
    move p4, v4

    .line 323
    move p5, v2

    .line 324
    move/from16 p6, v5

    .line 325
    .line 326
    invoke-direct/range {p1 .. p6}, Lcom/intsig/office/fc/ppt/PPTReader;->processWordArtParagraph(Lcom/intsig/office/simpletext/model/SectionElement;Ljava/lang/String;III)V

    .line 327
    .line 328
    .line 329
    iget v1, v0, Lcom/intsig/office/fc/ppt/PPTReader;->offset:I

    .line 330
    .line 331
    int-to-long v1, v1

    .line 332
    invoke-virtual {v3, v1, v2}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 333
    .line 334
    .line 335
    invoke-static {}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->instance()Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;

    .line 336
    .line 337
    .line 338
    move-result-object v1

    .line 339
    invoke-virtual {v1}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->clearData()V

    .line 340
    .line 341
    .line 342
    return-void
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method private resetFlag()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterDateTime:Z

    .line 3
    .line 4
    iput-boolean v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterFooter:Z

    .line 5
    .line 6
    iput-boolean v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->hasProcessedMasterSlideNumber:Z

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public backReader()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->currentReaderIndex:I

    .line 4
    .line 5
    add-int/lit8 v2, v1, 0x1

    .line 6
    .line 7
    iput v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->currentReaderIndex:I

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlide(I)Lcom/intsig/office/fc/hslf/model/Slide;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/ppt/PPTReader;->processSlide(Lcom/intsig/office/fc/hslf/model/Slide;)V

    .line 14
    .line 15
    .line 16
    iget-boolean v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->isGetThumbnail:Z

    .line 17
    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 21
    .line 22
    const v1, 0x2000000f

    .line 23
    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
.end method

.method public dispose()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/fc/ppt/PPTReader;->isReaderFinish()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    invoke-super {p0}, Lcom/intsig/office/system/AbstractReader;->dispose()V

    .line 8
    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGModel;->getSlideCount()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    const/4 v1, 0x2

    .line 23
    if-ge v0, v1, :cond_0

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlideCount()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-lez v0, :cond_0

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGModel;->dispose()V

    .line 36
    .line 37
    .line 38
    :cond_0
    const/4 v0, 0x0

    .line 39
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->filePath:Ljava/lang/String;

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 44
    .line 45
    if-eqz v1, :cond_1

    .line 46
    .line 47
    :try_start_0
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->dispose()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .line 49
    .line 50
    :catch_0
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 51
    .line 52
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->slideMasterIndexs:Ljava/util/Map;

    .line 53
    .line 54
    if-eqz v1, :cond_2

    .line 55
    .line 56
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 57
    .line 58
    .line 59
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->slideMasterIndexs:Ljava/util/Map;

    .line 60
    .line 61
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->titleMasterIndexs:Ljava/util/Map;

    .line 62
    .line 63
    if-eqz v1, :cond_3

    .line 64
    .line 65
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 66
    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->titleMasterIndexs:Ljava/util/Map;

    .line 69
    .line 70
    :cond_3
    invoke-static {}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->instance()Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/intsig/office/fc/ppt/bulletnumber/BulletNumberManage;->dispose()V

    .line 75
    .line 76
    .line 77
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 78
    .line 79
    .line 80
    :cond_4
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getModel()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 7
    .line 8
    new-instance v1, Lcom/intsig/office/fc/hslf/HSLFSlideShow;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 11
    .line 12
    iget-object v3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->filePath:Ljava/lang/String;

    .line 13
    .line 14
    iget-object v4, p0, Lcom/intsig/office/fc/ppt/PPTReader;->uri:Landroid/net/Uri;

    .line 15
    .line 16
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;-><init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;Landroid/net/Uri;)V

    .line 17
    .line 18
    .line 19
    iget-boolean v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->isGetThumbnail:Z

    .line 20
    .line 21
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;-><init>(Lcom/intsig/office/fc/hslf/HSLFSlideShow;Z)V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 25
    .line 26
    new-instance v0, Lcom/intsig/office/pg/model/PGModel;

    .line 27
    .line 28
    invoke-direct {v0}, Lcom/intsig/office/pg/model/PGModel;-><init>()V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getPageSize()Lcom/intsig/office/java/awt/Dimension;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    iget v1, v0, Lcom/intsig/office/java/awt/Dimension;->width:I

    .line 40
    .line 41
    int-to-float v1, v1

    .line 42
    const v2, 0x3faaaaab

    .line 43
    .line 44
    .line 45
    mul-float v1, v1, v2

    .line 46
    .line 47
    float-to-int v1, v1

    .line 48
    iput v1, v0, Lcom/intsig/office/java/awt/Dimension;->width:I

    .line 49
    .line 50
    iget v1, v0, Lcom/intsig/office/java/awt/Dimension;->height:I

    .line 51
    .line 52
    int-to-float v1, v1

    .line 53
    mul-float v1, v1, v2

    .line 54
    .line 55
    float-to-int v1, v1

    .line 56
    iput v1, v0, Lcom/intsig/office/java/awt/Dimension;->height:I

    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 59
    .line 60
    invoke-virtual {v1, v0}, Lcom/intsig/office/pg/model/PGModel;->setPageSize(Lcom/intsig/office/java/awt/Dimension;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getDocumentRecord()Lcom/intsig/office/fc/hslf/record/Document;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/Document;->getDocumentAtom()Lcom/intsig/office/fc/hslf/record/DocumentAtom;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    if-eqz v0, :cond_1

    .line 74
    .line 75
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/DocumentAtom;->getFirstSlideNum()I

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    add-int/lit8 v2, v2, -0x1

    .line 82
    .line 83
    invoke-virtual {v1, v2}, Lcom/intsig/office/pg/model/PGModel;->setSlideNumberOffset(I)V

    .line 84
    .line 85
    .line 86
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/record/DocumentAtom;->getOmitTitlePlace()Z

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    invoke-virtual {v1, v0}, Lcom/intsig/office/pg/model/PGModel;->setOmitTitleSlide(Z)V

    .line 93
    .line 94
    .line 95
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 96
    .line 97
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlideCount()I

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 102
    .line 103
    invoke-virtual {v1, v0}, Lcom/intsig/office/pg/model/PGModel;->setSlideCount(I)V

    .line 104
    .line 105
    .line 106
    if-eqz v0, :cond_4

    .line 107
    .line 108
    iget-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 109
    .line 110
    invoke-virtual {v1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlideHeadersFooters()Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    iput-object v1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiHeadersFooters:Lcom/intsig/office/fc/hslf/model/HeadersFooters;

    .line 115
    .line 116
    const/4 v1, 0x2

    .line 117
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 118
    .line 119
    .line 120
    move-result v0

    .line 121
    const/4 v1, 0x0

    .line 122
    :goto_0
    if-ge v1, v0, :cond_2

    .line 123
    .line 124
    iget-boolean v2, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 125
    .line 126
    if-nez v2, :cond_2

    .line 127
    .line 128
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 129
    .line 130
    iget v3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->currentReaderIndex:I

    .line 131
    .line 132
    add-int/lit8 v4, v3, 0x1

    .line 133
    .line 134
    iput v4, p0, Lcom/intsig/office/fc/ppt/PPTReader;->currentReaderIndex:I

    .line 135
    .line 136
    invoke-virtual {v2, v3}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlide(I)Lcom/intsig/office/fc/hslf/model/Slide;

    .line 137
    .line 138
    .line 139
    move-result-object v2

    .line 140
    invoke-direct {p0, v2}, Lcom/intsig/office/fc/ppt/PPTReader;->processSlide(Lcom/intsig/office/fc/hslf/model/Slide;)V

    .line 141
    .line 142
    .line 143
    add-int/lit8 v1, v1, 0x1

    .line 144
    .line 145
    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/fc/ppt/PPTReader;->isReaderFinish()Z

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    if-nez v0, :cond_3

    .line 151
    .line 152
    iget-boolean v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->isGetThumbnail:Z

    .line 153
    .line 154
    if-nez v0, :cond_3

    .line 155
    .line 156
    new-instance v0, Lcom/intsig/office/system/BackReaderThread;

    .line 157
    .line 158
    iget-object v1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 159
    .line 160
    invoke-direct {v0, p0, v1}, Lcom/intsig/office/system/BackReaderThread;-><init>(Lcom/intsig/office/system/IReader;Lcom/intsig/office/system/IControl;)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 164
    .line 165
    .line 166
    :cond_3
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 167
    .line 168
    return-object v0

    .line 169
    :cond_4
    new-instance v0, Ljava/lang/Exception;

    .line 170
    .line 171
    const-string v1, "Format error"

    .line 172
    .line 173
    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 174
    .line 175
    .line 176
    throw v0
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method public isReaderFinish()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 7
    .line 8
    if-eqz v2, :cond_1

    .line 9
    .line 10
    iget-boolean v2, p0, Lcom/intsig/office/system/AbstractReader;->abortReader:Z

    .line 11
    .line 12
    if-nez v2, :cond_1

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGModel;->getSlideCount()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    iget v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->currentReaderIndex:I

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlideCount()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-lt v0, v2, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 v1, 0x0

    .line 32
    :cond_1
    :goto_0
    return v1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public isRectangle(Lcom/intsig/office/fc/hslf/model/TextShape;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getShapeType()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 v0, 0x1

    .line 6
    if-eq p1, v0, :cond_1

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    if-eq p1, v1, :cond_1

    .line 10
    .line 11
    const/16 v1, 0xca

    .line 12
    .line 13
    if-ne p1, v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    return p1

    .line 18
    :cond_1
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public processGrpRotation(Lcom/intsig/office/fc/hslf/model/Shape;Lcom/intsig/office/common/shape/IShape;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getRotation()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getFlipHorizontal()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    const/4 v2, 0x1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {p2, v2}, Lcom/intsig/office/common/shape/IShape;->setFlipHorizontal(Z)V

    .line 14
    .line 15
    .line 16
    neg-float v0, v0

    .line 17
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/Shape;->getFlipVertical()Z

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-eqz p1, :cond_1

    .line 22
    .line 23
    invoke-interface {p2, v2}, Lcom/intsig/office/common/shape/IShape;->setFlipVertical(Z)V

    .line 24
    .line 25
    .line 26
    neg-float v0, v0

    .line 27
    :cond_1
    instance-of p1, p2, Lcom/intsig/office/common/shape/LineShape;

    .line 28
    .line 29
    if-eqz p1, :cond_3

    .line 30
    .line 31
    const/high16 p1, 0x42340000    # 45.0f

    .line 32
    .line 33
    cmpl-float p1, v0, p1

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    const/high16 p1, 0x43070000    # 135.0f

    .line 38
    .line 39
    cmpl-float p1, v0, p1

    .line 40
    .line 41
    if-eqz p1, :cond_2

    .line 42
    .line 43
    const/high16 p1, 0x43610000    # 225.0f

    .line 44
    .line 45
    cmpl-float p1, v0, p1

    .line 46
    .line 47
    if-nez p1, :cond_3

    .line 48
    .line 49
    :cond_2
    invoke-interface {p2}, Lcom/intsig/office/common/shape/IShape;->getFlipHorizontal()Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-nez p1, :cond_3

    .line 54
    .line 55
    invoke-interface {p2}, Lcom/intsig/office/common/shape/IShape;->getFlipVertical()Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    if-nez p1, :cond_3

    .line 60
    .line 61
    const/high16 p1, 0x42b40000    # 90.0f

    .line 62
    .line 63
    sub-float/2addr v0, p1

    .line 64
    :cond_3
    invoke-interface {p2, v0}, Lcom/intsig/office/common/shape/IShape;->setRotation(F)V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public processMaster(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/hslf/model/Slide;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->slideMasterIndexs:Ljava/util/Map;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/HashMap;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->slideMasterIndexs:Ljava/util/Map;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->titleMasterIndexs:Ljava/util/Map;

    .line 13
    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    new-instance v0, Ljava/util/HashMap;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->titleMasterIndexs:Ljava/util/Map;

    .line 22
    .line 23
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/model/Slide;->getSlideRecord()Lcom/intsig/office/fc/hslf/record/Slide;

    .line 24
    .line 25
    .line 26
    move-result-object p2

    .line 27
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/Slide;->getSlideAtom()Lcom/intsig/office/fc/hslf/record/SlideAtom;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getFollowMasterObjects()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_2

    .line 36
    .line 37
    return-void

    .line 38
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/office/fc/hslf/record/SlideAtom;->getMasterID()I

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlidesMasters()[Lcom/intsig/office/fc/hslf/model/SlideMaster;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    const/4 v1, 0x0

    .line 49
    const/4 v2, 0x0

    .line 50
    :goto_0
    array-length v3, v0

    .line 51
    const/4 v4, 0x0

    .line 52
    if-ge v2, v3, :cond_6

    .line 53
    .line 54
    aget-object v3, v0, v2

    .line 55
    .line 56
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/Sheet;->_getSheetNumber()I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    if-ne p2, v3, :cond_5

    .line 61
    .line 62
    iget-object v3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->slideMasterIndexs:Ljava/util/Map;

    .line 63
    .line 64
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 65
    .line 66
    .line 67
    move-result-object v5

    .line 68
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    check-cast v3, Ljava/lang/Integer;

    .line 73
    .line 74
    if-eqz v3, :cond_3

    .line 75
    .line 76
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 77
    .line 78
    .line 79
    move-result p2

    .line 80
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/model/PGSlide;->setMasterSlideIndex(I)V

    .line 81
    .line 82
    .line 83
    return-void

    .line 84
    :cond_3
    new-instance v3, Lcom/intsig/office/pg/model/PGSlide;

    .line 85
    .line 86
    invoke-direct {v3}, Lcom/intsig/office/pg/model/PGSlide;-><init>()V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v3, v1}, Lcom/intsig/office/pg/model/PGSlide;->setSlideType(I)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGSlide;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 93
    .line 94
    .line 95
    move-result-object v5

    .line 96
    invoke-virtual {v3, v5}, Lcom/intsig/office/pg/model/PGSlide;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 97
    .line 98
    .line 99
    aget-object v0, v0, v2

    .line 100
    .line 101
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    const/4 v2, 0x0

    .line 106
    :goto_1
    array-length v5, v0

    .line 107
    if-ge v2, v5, :cond_4

    .line 108
    .line 109
    aget-object v5, v0, v2

    .line 110
    .line 111
    invoke-direct {p0, v3, v4, v5, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->processShape(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hslf/model/Shape;I)V

    .line 112
    .line 113
    .line 114
    add-int/lit8 v2, v2, 0x1

    .line 115
    .line 116
    goto :goto_1

    .line 117
    :cond_4
    invoke-virtual {v3}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    .line 118
    .line 119
    .line 120
    move-result v0

    .line 121
    if-lez v0, :cond_6

    .line 122
    .line 123
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 124
    .line 125
    invoke-virtual {v0, v3}, Lcom/intsig/office/pg/model/PGModel;->appendSlideMaster(Lcom/intsig/office/pg/model/PGSlide;)I

    .line 126
    .line 127
    .line 128
    move-result v0

    .line 129
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 134
    .line 135
    .line 136
    move-result v2

    .line 137
    invoke-virtual {p1, v2}, Lcom/intsig/office/pg/model/PGSlide;->setMasterSlideIndex(I)V

    .line 138
    .line 139
    .line 140
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->slideMasterIndexs:Ljava/util/Map;

    .line 141
    .line 142
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 143
    .line 144
    .line 145
    move-result-object v3

    .line 146
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    goto :goto_2

    .line 150
    :cond_5
    add-int/lit8 v2, v2, 0x1

    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->poiSlideShow:Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 154
    .line 155
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getTitleMasters()[Lcom/intsig/office/fc/hslf/model/TitleMaster;

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    if-eqz v0, :cond_a

    .line 160
    .line 161
    const/4 v2, 0x0

    .line 162
    :goto_3
    array-length v3, v0

    .line 163
    if-ge v2, v3, :cond_a

    .line 164
    .line 165
    aget-object v3, v0, v2

    .line 166
    .line 167
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/Sheet;->_getSheetNumber()I

    .line 168
    .line 169
    .line 170
    move-result v3

    .line 171
    if-ne p2, v3, :cond_9

    .line 172
    .line 173
    iget-object v3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->titleMasterIndexs:Ljava/util/Map;

    .line 174
    .line 175
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 176
    .line 177
    .line 178
    move-result-object v5

    .line 179
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    .line 181
    .line 182
    move-result-object v3

    .line 183
    check-cast v3, Ljava/lang/Integer;

    .line 184
    .line 185
    if-eqz v3, :cond_7

    .line 186
    .line 187
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 188
    .line 189
    .line 190
    move-result p2

    .line 191
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/model/PGSlide;->setLayoutSlideIndex(I)V

    .line 192
    .line 193
    .line 194
    goto :goto_5

    .line 195
    :cond_7
    new-instance v3, Lcom/intsig/office/pg/model/PGSlide;

    .line 196
    .line 197
    invoke-direct {v3}, Lcom/intsig/office/pg/model/PGSlide;-><init>()V

    .line 198
    .line 199
    .line 200
    invoke-virtual {v3, v1}, Lcom/intsig/office/pg/model/PGSlide;->setSlideType(I)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGSlide;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 204
    .line 205
    .line 206
    move-result-object v5

    .line 207
    invoke-virtual {v3, v5}, Lcom/intsig/office/pg/model/PGSlide;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 208
    .line 209
    .line 210
    aget-object v0, v0, v2

    .line 211
    .line 212
    invoke-virtual {v0}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 213
    .line 214
    .line 215
    move-result-object v0

    .line 216
    const/4 v2, 0x0

    .line 217
    :goto_4
    array-length v5, v0

    .line 218
    if-ge v2, v5, :cond_8

    .line 219
    .line 220
    aget-object v5, v0, v2

    .line 221
    .line 222
    invoke-direct {p0, v3, v4, v5, v1}, Lcom/intsig/office/fc/ppt/PPTReader;->processShape(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/fc/hslf/model/Shape;I)V

    .line 223
    .line 224
    .line 225
    add-int/lit8 v2, v2, 0x1

    .line 226
    .line 227
    goto :goto_4

    .line 228
    :cond_8
    invoke-virtual {v3}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    .line 229
    .line 230
    .line 231
    move-result v0

    .line 232
    if-lez v0, :cond_a

    .line 233
    .line 234
    iget-object v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->model:Lcom/intsig/office/pg/model/PGModel;

    .line 235
    .line 236
    invoke-virtual {v0, v3}, Lcom/intsig/office/pg/model/PGModel;->appendSlideMaster(Lcom/intsig/office/pg/model/PGSlide;)I

    .line 237
    .line 238
    .line 239
    move-result v0

    .line 240
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 241
    .line 242
    .line 243
    move-result-object v0

    .line 244
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 245
    .line 246
    .line 247
    move-result v1

    .line 248
    invoke-virtual {p1, v1}, Lcom/intsig/office/pg/model/PGSlide;->setLayoutSlideIndex(I)V

    .line 249
    .line 250
    .line 251
    iget-object p1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->titleMasterIndexs:Ljava/util/Map;

    .line 252
    .line 253
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 254
    .line 255
    .line 256
    move-result-object p2

    .line 257
    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    .line 259
    .line 260
    goto :goto_5

    .line 261
    :cond_9
    add-int/lit8 v2, v2, 0x1

    .line 262
    .line 263
    goto :goto_3

    .line 264
    :cond_a
    :goto_5
    return-void
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public searchContent(Ljava/io/File;Ljava/lang/String;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance p1, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/office/fc/hslf/HSLFSlideShow;

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/system/AbstractReader;->control:Lcom/intsig/office/system/IControl;

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/office/fc/ppt/PPTReader;->filePath:Ljava/lang/String;

    .line 8
    .line 9
    iget-object v3, p0, Lcom/intsig/office/fc/ppt/PPTReader;->uri:Landroid/net/Uri;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/hslf/HSLFSlideShow;-><init>(Lcom/intsig/office/system/IControl;Ljava/lang/String;Landroid/net/Uri;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p1, v0}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;-><init>(Lcom/intsig/office/fc/hslf/HSLFSlideShow;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/usermodel/SlideShow;->getSlides()[Lcom/intsig/office/fc/hslf/model/Slide;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    array-length v0, p1

    .line 22
    const/4 v1, 0x0

    .line 23
    const/4 v2, 0x0

    .line 24
    :goto_0
    if-ge v2, v0, :cond_5

    .line 25
    .line 26
    aget-object v3, p1, v2

    .line 27
    .line 28
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 29
    .line 30
    .line 31
    move-result-object v4

    .line 32
    array-length v5, v4

    .line 33
    const/4 v6, 0x0

    .line 34
    :goto_1
    const/4 v7, 0x1

    .line 35
    if-ge v6, v5, :cond_1

    .line 36
    .line 37
    aget-object v8, v4, v6

    .line 38
    .line 39
    invoke-virtual {p0, v8, p2}, Lcom/intsig/office/fc/ppt/PPTReader;->searchShape(Lcom/intsig/office/fc/hslf/model/Shape;Ljava/lang/String;)Z

    .line 40
    .line 41
    .line 42
    move-result v8

    .line 43
    if-eqz v8, :cond_0

    .line 44
    .line 45
    return v7

    .line 46
    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_1
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/Slide;->getNotesSheet()Lcom/intsig/office/fc/hslf/model/Notes;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    if-eqz v3, :cond_4

    .line 54
    .line 55
    invoke-virtual {v3}, Lcom/intsig/office/fc/hslf/model/Sheet;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    array-length v4, v3

    .line 60
    const/4 v5, 0x0

    .line 61
    :goto_2
    if-ge v5, v4, :cond_4

    .line 62
    .line 63
    aget-object v6, v3, v5

    .line 64
    .line 65
    instance-of v8, v6, Lcom/intsig/office/fc/hslf/model/AutoShape;

    .line 66
    .line 67
    if-nez v8, :cond_2

    .line 68
    .line 69
    instance-of v8, v6, Lcom/intsig/office/fc/hslf/model/TextBox;

    .line 70
    .line 71
    if-eqz v8, :cond_3

    .line 72
    .line 73
    :cond_2
    move-object v8, v6

    .line 74
    check-cast v8, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 75
    .line 76
    invoke-virtual {v8}, Lcom/intsig/office/fc/hslf/model/TextShape;->getPlaceholderAtom()Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;

    .line 77
    .line 78
    .line 79
    move-result-object v8

    .line 80
    if-eqz v8, :cond_3

    .line 81
    .line 82
    invoke-virtual {v8}, Lcom/intsig/office/fc/hslf/record/OEPlaceholderAtom;->getPlaceholderId()I

    .line 83
    .line 84
    .line 85
    move-result v8

    .line 86
    const/16 v9, 0xc

    .line 87
    .line 88
    if-ne v8, v9, :cond_3

    .line 89
    .line 90
    invoke-virtual {p0, v6, p2}, Lcom/intsig/office/fc/ppt/PPTReader;->searchShape(Lcom/intsig/office/fc/hslf/model/Shape;Ljava/lang/String;)Z

    .line 91
    .line 92
    .line 93
    move-result v6

    .line 94
    if-eqz v6, :cond_3

    .line 95
    .line 96
    return v7

    .line 97
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 98
    .line 99
    goto :goto_2

    .line 100
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_5
    return v1
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public searchShape(Lcom/intsig/office/fc/hslf/model/Shape;Ljava/lang/String;)Z
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    instance-of v1, p1, Lcom/intsig/office/fc/hslf/model/AutoShape;

    .line 7
    .line 8
    const/4 v2, 0x1

    .line 9
    const/4 v3, 0x0

    .line 10
    if-nez v1, :cond_2

    .line 11
    .line 12
    instance-of v1, p1, Lcom/intsig/office/fc/hslf/model/TextBox;

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_0
    instance-of v0, p1, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 18
    .line 19
    if-eqz v0, :cond_4

    .line 20
    .line 21
    check-cast p1, Lcom/intsig/office/fc/hslf/model/ShapeGroup;

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/ShapeGroup;->getShapes()[Lcom/intsig/office/fc/hslf/model/Shape;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    const/4 v0, 0x0

    .line 28
    :goto_0
    array-length v1, p1

    .line 29
    if-ge v0, v1, :cond_4

    .line 30
    .line 31
    aget-object v1, p1, v0

    .line 32
    .line 33
    invoke-virtual {p0, v1, p2}, Lcom/intsig/office/fc/ppt/PPTReader;->searchShape(Lcom/intsig/office/fc/hslf/model/Shape;Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-eqz v1, :cond_1

    .line 38
    .line 39
    return v2

    .line 40
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    :goto_1
    check-cast p1, Lcom/intsig/office/fc/hslf/model/TextShape;

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/office/fc/hslf/model/TextShape;->getText()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    if-ltz p1, :cond_3

    .line 57
    .line 58
    return v2

    .line 59
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    invoke-virtual {v0, v3, p1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    :cond_4
    return v3
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setMaxFontSize(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ppt/PPTReader;->maxFontSize:I

    .line 2
    .line 3
    if-le p1, v0, :cond_0

    .line 4
    .line 5
    iput p1, p0, Lcom/intsig/office/fc/ppt/PPTReader;->maxFontSize:I

    .line 6
    .line 7
    :cond_0
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

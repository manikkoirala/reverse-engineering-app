.class public Lcom/intsig/office/fc/ppt/ShapeManage;
.super Ljava/lang/Object;
.source "ShapeManage.java"


# static fields
.field private static kit:Lcom/intsig/office/fc/ppt/ShapeManage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ppt/ShapeManage;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/fc/ppt/ShapeManage;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/fc/ppt/ShapeManage;->kit:Lcom/intsig/office/fc/ppt/ShapeManage;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getBackgrouond(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;IBLjava/lang/String;I)Lcom/intsig/office/common/bg/BackgroundAndFill;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v0, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p12

    const-string v1, "useBgFill"

    .line 1
    invoke-interface {v9, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2
    invoke-interface {v9, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/office/pg/model/PGSlide;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz v8, :cond_0

    .line 5
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/office/pg/model/PGLayout;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    if-eqz v7, :cond_1

    .line 6
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/pg/model/PGMaster;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->setSlideBackgroundFill(Z)V

    :cond_2
    return-object v0

    :cond_3
    const-string v1, "spPr"

    .line 8
    invoke-interface {v9, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    .line 9
    invoke-interface/range {p8 .. p8}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "noFill"

    .line 10
    invoke-interface {v6, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v2

    const/4 v13, 0x0

    if-nez v2, :cond_5

    const-string v2, "cxnSp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 11
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->instance()Lcom/intsig/office/fc/ppt/reader/BackgroundReader;

    move-result-object v1

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v1

    if-nez v1, :cond_4

    const/16 v2, 0x13

    if-eq v12, v2, :cond_4

    const/16 v2, 0xb9

    if-eq v12, v2, :cond_4

    const/16 v2, 0x55

    if-eq v12, v2, :cond_4

    const/16 v2, 0x56

    if-eq v12, v2, :cond_4

    const/16 v2, 0xba

    if-eq v12, v2, :cond_4

    const/16 v2, 0x57

    if-eq v12, v2, :cond_4

    const/16 v2, 0x58

    if-eq v12, v2, :cond_4

    const/16 v2, 0xe9

    if-eq v12, v2, :cond_4

    .line 12
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->instance()Lcom/intsig/office/fc/ppt/reader/BackgroundReader;

    move-result-object v1

    const-string v2, "style"

    invoke-interface {v9, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 13
    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getFillType()B

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Lcom/intsig/office/common/bg/BackgroundAndFill;->getForegroundColor()I

    move-result v2

    const v3, 0xffffff

    and-int/2addr v2, v3

    if-nez v2, :cond_4

    goto :goto_0

    :cond_4
    move-object v13, v1

    :cond_5
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    if-nez v13, :cond_7

    if-ne v11, v2, :cond_7

    .line 14
    invoke-static {}, Lcom/intsig/office/pg/model/PGPlaceholderUtil;->instance()Lcom/intsig/office/pg/model/PGPlaceholderUtil;

    move-result-object v3

    move-object/from16 v4, p11

    invoke-virtual {v3, v4}, Lcom/intsig/office/pg/model/PGPlaceholderUtil;->isTitleOrBody(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v8, :cond_7

    .line 15
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/office/pg/model/PGLayout;->getSlideMasterIndex()I

    move-result v3

    if-ltz v3, :cond_7

    if-ltz v10, :cond_7

    .line 16
    invoke-virtual/range {p6 .. p6}, Lcom/intsig/office/pg/model/PGLayout;->getSlideMasterIndex()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/intsig/office/pg/model/PGModel;->getSlideMaster(I)Lcom/intsig/office/pg/model/PGSlide;

    move-result-object v3

    .line 17
    invoke-virtual {v8, v10}, Lcom/intsig/office/pg/model/PGLayout;->getTitleBodyID(I)Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 18
    invoke-virtual {v3}, Lcom/intsig/office/pg/model/PGSlide;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    move-result-object v3

    const/4 v5, 0x0

    .line 19
    :goto_1
    array-length v6, v3

    if-ge v5, v6, :cond_7

    .line 20
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget-object v8, v3, v5

    invoke-interface {v8}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    move-result v8

    if-ne v6, v8, :cond_6

    aget-object v6, v3, v5

    instance-of v8, v6, Lcom/intsig/office/common/shape/AutoShape;

    if-eqz v8, :cond_6

    .line 21
    check-cast v6, Lcom/intsig/office/common/shape/AutoShape;

    invoke-virtual {v6}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v13

    goto :goto_2

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_7
    :goto_2
    if-nez v13, :cond_9

    if-ne v11, v2, :cond_9

    if-eqz v7, :cond_9

    .line 22
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/pg/model/PGMaster;->getSlideMasterIndex()I

    move-result v2

    if-ltz v2, :cond_9

    if-ltz v10, :cond_9

    .line 23
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/pg/model/PGMaster;->getSlideMasterIndex()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/PGModel;->getSlideMaster(I)Lcom/intsig/office/pg/model/PGSlide;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    move-result-object v0

    .line 25
    invoke-virtual {v7, v10}, Lcom/intsig/office/pg/model/PGMaster;->getTitleBodyID(I)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 26
    invoke-virtual {v7, v10}, Lcom/intsig/office/pg/model/PGMaster;->getTitleBodyID(I)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 27
    :goto_3
    array-length v3, v0

    if-ge v1, v3, :cond_9

    .line 28
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v4, v0, v1

    invoke-interface {v4}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    move-result v4

    if-ne v3, v4, :cond_8

    aget-object v3, v0, v1

    instance-of v4, v3, Lcom/intsig/office/common/shape/AutoShape;

    if-eqz v4, :cond_8

    .line 29
    check-cast v3, Lcom/intsig/office/common/shape/AutoShape;

    invoke-virtual {v3}, Lcom/intsig/office/common/shape/AbstractShape;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v13

    goto :goto_4

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_9
    :goto_4
    return-object v13
.end method

.method public static instance()Lcom/intsig/office/fc/ppt/ShapeManage;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ppt/ShapeManage;->kit:Lcom/intsig/office/fc/ppt/ShapeManage;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private isRect(Ljava/lang/String;I)Z
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p1, :cond_1

    .line 3
    .line 4
    const-string v1, "Title"

    .line 5
    .line 6
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    const-string v1, "title"

    .line 13
    .line 14
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-nez v1, :cond_0

    .line 19
    .line 20
    const-string v1, "ctrTitle"

    .line 21
    .line 22
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-nez v1, :cond_0

    .line 27
    .line 28
    const-string v1, "subTitle"

    .line 29
    .line 30
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-nez v1, :cond_0

    .line 35
    .line 36
    const-string v1, "body"

    .line 37
    .line 38
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    if-nez v2, :cond_0

    .line 43
    .line 44
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    if-nez v1, :cond_0

    .line 49
    .line 50
    const-string v1, "half"

    .line 51
    .line 52
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    if-nez v1, :cond_0

    .line 57
    .line 58
    const-string v1, "dt"

    .line 59
    .line 60
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    if-nez v1, :cond_0

    .line 65
    .line 66
    const-string v1, "ftr"

    .line 67
    .line 68
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    if-nez v1, :cond_0

    .line 73
    .line 74
    const-string v1, "sldNum"

    .line 75
    .line 76
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 77
    .line 78
    .line 79
    move-result p1

    .line 80
    if-eqz p1, :cond_1

    .line 81
    .line 82
    :cond_0
    return v0

    .line 83
    :cond_1
    if-lez p2, :cond_2

    .line 84
    .line 85
    return v0

    .line 86
    :cond_2
    const/4 p1, 0x0

    .line 87
    return p1
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1, p3, p2}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->processRotation(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/IShape;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getOffX()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    add-int/2addr v0, v1

    .line 10
    iput v0, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 11
    .line 12
    iget v0, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getOffY()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    add-int/2addr v0, p1

    .line 19
    iput v0, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 20
    .line 21
    :cond_0
    return-object p2
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private processSmartArt(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 4

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    :try_start_0
    const-string v0, "relIds"

    .line 4
    .line 5
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    const-string v0, "dm"

    .line 10
    .line 11
    invoke-interface {p2, v0}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    const/4 v0, 0x3

    .line 16
    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/model/PGSlide;->getSmartArt(Ljava/lang/String;)Lcom/intsig/office/common/shape/SmartArt;

    .line 25
    .line 26
    .line 27
    move-result-object p2

    .line 28
    if-eqz p2, :cond_1

    .line 29
    .line 30
    invoke-virtual {p2, p3}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/SmartArt;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    .line 34
    .line 35
    .line 36
    move-result-object p3

    .line 37
    array-length v1, p3

    .line 38
    const/4 v2, 0x0

    .line 39
    :goto_0
    if-ge v2, v1, :cond_0

    .line 40
    .line 41
    aget-object v3, p3, v2

    .line 42
    .line 43
    invoke-interface {v3, v0}, Lcom/intsig/office/common/shape/IShape;->setShapeID(I)V

    .line 44
    .line 45
    .line 46
    add-int/lit8 v2, v2, 0x1

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .line 51
    .line 52
    :catch_0
    :cond_1
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method


# virtual methods
.method public addPicture(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGSlide;ILcom/intsig/office/java/awt/Rectangle;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)Lcom/intsig/office/common/shape/PictureShape;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/office/common/shape/PictureShape;

    .line 4
    .line 5
    invoke-direct {v0}, Lcom/intsig/office/common/shape/PictureShape;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getPictureManage()Lcom/intsig/office/common/picture/PictureManage;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {p1, p2}, Lcom/intsig/office/common/picture/PictureManage;->addPicture(Lcom/intsig/office/fc/openxml4j/opc/PackagePart;)I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    invoke-virtual {v0, p1}, Lcom/intsig/office/common/shape/PictureShape;->setPictureIndex(I)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, p5}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0, p7, v0, p6}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, p4}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, p8}, Lcom/intsig/office/common/shape/PictureShape;->setPictureEffectInfor(Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)V

    .line 33
    .line 34
    .line 35
    if-nez p7, :cond_0

    .line 36
    .line 37
    invoke-virtual {p3, v0}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {p7, v0}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    const/4 v0, 0x0

    .line 46
    :goto_0
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
.end method

.method public processAutoShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;IILcom/intsig/office/java/awt/Rectangle;ZLcom/intsig/office/common/shape/GroupShape;BLjava/lang/String;Z)Lcom/intsig/office/common/shape/AbstractShape;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v13, p8

    move/from16 v14, p9

    move-object/from16 v15, p11

    move/from16 v12, p16

    const-string v0, "spPr"

    .line 1
    invoke-interface {v13, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v11

    const/16 v16, 0x0

    if-eqz v11, :cond_16

    .line 2
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v0

    invoke-virtual {v0, v13}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getPlaceholderName(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    move-result-object v0

    .line 3
    invoke-interface/range {p8 .. p8}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cxnSp"

    .line 4
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/16 v10, 0x20

    const/16 v17, 0x1

    const/16 v18, 0x0

    if-eqz v1, :cond_0

    const/16 v0, 0x20

    goto :goto_1

    :cond_0
    if-nez p12, :cond_2

    const-string v1, "Text Box"

    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "TextBox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    const-string v1, "prstGeom"

    .line 6
    invoke-interface {v11, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    const/16 v9, 0xe9

    if-eqz v1, :cond_6

    const-string v2, "prst"

    .line 7
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 8
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 9
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 10
    invoke-static {}, Lcom/intsig/office/common/autoshape/AutoShapeTypes;->instance()Lcom/intsig/office/common/autoshape/AutoShapeTypes;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/intsig/office/common/autoshape/AutoShapeTypes;->getAutoShapeType(Ljava/lang/String;)I

    move-result v0

    :cond_3
    const-string v2, "avLst"

    .line 11
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v2, "gd"

    .line 12
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->elements(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 13
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 14
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/Float;

    const/4 v3, 0x0

    .line 15
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_5

    .line 16
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/intsig/office/fc/dom4j/Element;

    const-string v5, "fmla"

    .line 17
    invoke-interface {v4, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    .line 18
    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 19
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    const v5, 0x47c35000    # 100000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    move-object/from16 v2, v16

    :cond_5
    move v8, v0

    move-object v7, v2

    goto :goto_3

    :cond_6
    const-string v1, "custGeom"

    .line 20
    invoke-interface {v11, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_7

    move-object/from16 v7, v16

    const/16 v8, 0xe9

    goto :goto_3

    :cond_7
    if-eqz p12, :cond_8

    move-object/from16 v7, v16

    const/4 v8, 0x1

    goto :goto_3

    :cond_8
    move v8, v0

    move-object/from16 v7, v16

    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v19, v7

    move-object/from16 v7, p7

    move/from16 p12, v8

    move-object/from16 v8, p8

    const/16 v15, 0xe9

    move/from16 v9, p10

    const/16 v15, 0x20

    move/from16 v10, p14

    move-object v15, v11

    move-object/from16 v11, p15

    move/from16 v12, p12

    .line 21
    invoke-direct/range {v0 .. v12}, Lcom/intsig/office/fc/ppt/ShapeManage;->getBackgrouond(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;IBLjava/lang/String;I)Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v0

    move-object/from16 v4, p5

    .line 22
    invoke-static {v1, v2, v3, v4, v13}, Lcom/intsig/office/fc/LineKit;->createShapeLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;

    move-result-object v1

    const-string v2, "ln"

    .line 23
    invoke-interface {v15, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v2

    const-string v3, "style"

    .line 24
    invoke-interface {v13, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    if-eqz v2, :cond_9

    const-string v3, "noFill"

    .line 25
    invoke-interface {v2, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    if-eqz v3, :cond_a

    goto :goto_4

    :cond_9
    if-eqz v3, :cond_b

    const-string v4, "lnRef"

    .line 26
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    if-nez v3, :cond_a

    goto :goto_4

    :cond_a
    const/16 v18, 0x1

    :cond_b
    :goto_4
    const/16 v3, 0x14

    move/from16 v9, p12

    if-eq v9, v3, :cond_12

    const/16 v3, 0x20

    if-eq v9, v3, :cond_12

    const/16 v3, 0x21

    if-eq v9, v3, :cond_12

    const/16 v3, 0x22

    if-eq v9, v3, :cond_12

    const/16 v3, 0x25

    if-eq v9, v3, :cond_12

    const/16 v3, 0x26

    if-eq v9, v3, :cond_12

    const/16 v3, 0x27

    if-eq v9, v3, :cond_12

    const/16 v3, 0x28

    if-ne v9, v3, :cond_c

    goto :goto_5

    :cond_c
    const/16 v3, 0xe9

    if-ne v9, v3, :cond_e

    .line 27
    new-instance v3, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;

    invoke-direct {v3}, Lcom/intsig/office/common/shape/ArbitraryPolygonShape;-><init>()V

    if-eqz v1, :cond_d

    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/common/borders/Line;->getBackgroundAndFill()Lcom/intsig/office/common/bg/BackgroundAndFill;

    move-result-object v4

    move-object/from16 v16, v4

    :cond_d
    move-object/from16 p1, v3

    move-object/from16 p2, p8

    move-object/from16 p3, v0

    move/from16 p4, v18

    move-object/from16 p5, v16

    move-object/from16 p6, v2

    move-object/from16 p7, p11

    .line 29
    invoke-static/range {p1 .. p7}, Lcom/intsig/office/common/autoshape/ArbitraryPolygonShapePath;->processArbitraryPolygonShape(Lcom/intsig/office/common/shape/ArbitraryPolygonShape;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/bg/BackgroundAndFill;ZLcom/intsig/office/common/bg/BackgroundAndFill;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V

    .line 30
    invoke-virtual {v3, v9}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 31
    invoke-virtual {v3, v14}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    move-object/from16 v4, p0

    move-object/from16 v0, p13

    .line 32
    invoke-direct {v4, v0, v3, v15}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    move/from16 v5, p16

    .line 33
    invoke-virtual {v3, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setHidden(Z)V

    .line 34
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    return-object v3

    :cond_e
    move-object/from16 v4, p0

    move/from16 v5, p16

    if-nez v0, :cond_f

    if-eqz v1, :cond_17

    .line 35
    :cond_f
    new-instance v2, Lcom/intsig/office/common/shape/AutoShape;

    invoke-direct {v2, v9}, Lcom/intsig/office/common/shape/AutoShape;-><init>(I)V

    move-object/from16 v3, p11

    .line 36
    invoke-virtual {v2, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 37
    invoke-virtual {v2, v14}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 38
    invoke-virtual {v2, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setHidden(Z)V

    if-eqz v0, :cond_10

    .line 39
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    :cond_10
    if-eqz v1, :cond_11

    .line 40
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    :cond_11
    move-object/from16 v0, v19

    .line 41
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    return-object v2

    :cond_12
    :goto_5
    move-object/from16 v4, p0

    move-object/from16 v3, p11

    move/from16 v5, p16

    move-object/from16 v0, v19

    if-nez v18, :cond_13

    return-object v16

    .line 42
    :cond_13
    new-instance v6, Lcom/intsig/office/common/shape/LineShape;

    invoke-direct {v6}, Lcom/intsig/office/common/shape/LineShape;-><init>()V

    .line 43
    invoke-virtual {v6, v9}, Lcom/intsig/office/common/shape/AutoShape;->setShapeType(I)V

    .line 44
    invoke-virtual {v6, v3}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 45
    invoke-virtual {v6, v14}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 46
    invoke-virtual {v6, v5}, Lcom/intsig/office/common/shape/AbstractShape;->setHidden(Z)V

    .line 47
    invoke-virtual {v6, v0}, Lcom/intsig/office/common/shape/AutoShape;->setAdjustData([Ljava/lang/Float;)V

    .line 48
    invoke-virtual {v6, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    if-eqz v2, :cond_15

    const-string v0, "headEnd"

    .line 49
    invoke-interface {v2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    const-string v1, "len"

    const-string v3, "w"

    const-string v5, "type"

    if-eqz v0, :cond_14

    .line 50
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v7

    if-eqz v7, :cond_14

    .line 51
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/intsig/office/common/shape/Arrow;->getArrowType(Ljava/lang/String;)B

    move-result v7

    if-eqz v7, :cond_14

    .line 52
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/intsig/office/common/shape/Arrow;->getArrowSize(Ljava/lang/String;)I

    move-result v8

    .line 53
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/intsig/office/common/shape/Arrow;->getArrowSize(Ljava/lang/String;)I

    move-result v0

    .line 54
    invoke-virtual {v6, v7, v8, v0}, Lcom/intsig/office/common/shape/LineShape;->createStartArrow(BII)V

    :cond_14
    const-string v0, "tailEnd"

    .line 55
    invoke-interface {v2, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 56
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v2

    if-eqz v2, :cond_15

    .line 57
    invoke-interface {v0, v5}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/intsig/office/common/shape/Arrow;->getArrowType(Ljava/lang/String;)B

    move-result v2

    if-eqz v2, :cond_15

    .line 58
    invoke-interface {v0, v3}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/intsig/office/common/shape/Arrow;->getArrowSize(Ljava/lang/String;)I

    move-result v3

    .line 59
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/intsig/office/common/shape/Arrow;->getArrowSize(Ljava/lang/String;)I

    move-result v0

    .line 60
    invoke-virtual {v6, v2, v3, v0}, Lcom/intsig/office/common/shape/LineShape;->createEndArrow(BII)V

    :cond_15
    return-object v6

    :cond_16
    move-object/from16 v4, p0

    :cond_17
    return-object v16
.end method

.method public processAutoShapeAndTextShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGStyle;Lcom/intsig/office/pg/model/PGSlide;BLcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FFZ)I
    .locals 29
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v14, p5

    move-object/from16 v13, p6

    move-object/from16 v12, p8

    move/from16 v11, p9

    move-object/from16 v10, p10

    move-object/from16 v9, p11

    const-string v0, "nvSpPr"

    .line 1
    invoke-interface {v10, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "nvCxnSpPr"

    .line 2
    invoke-interface {v10, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    :cond_0
    const-string v1, "cNvPr"

    .line 3
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    const-string v1, "id"

    .line 4
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 5
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getPlaceholderType(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    move-result-object v0

    .line 6
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getPlaceholderIdx(Lcom/intsig/office/fc/dom4j/Element;)I

    move-result v7

    const/4 v6, 0x1

    if-ne v11, v6, :cond_1

    .line 7
    invoke-virtual {v13, v7, v0}, Lcom/intsig/office/pg/model/PGLayout;->addShapeType(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    if-eqz v14, :cond_2

    if-ltz v7, :cond_2

    .line 8
    invoke-virtual {v13, v7}, Lcom/intsig/office/pg/model/PGLayout;->getShapeType(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_0
    move-object v5, v0

    .line 9
    invoke-static {}, Lcom/intsig/office/pg/model/PGPlaceholderUtil;->instance()Lcom/intsig/office/pg/model/PGPlaceholderUtil;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/intsig/office/pg/model/PGPlaceholderUtil;->isTitleOrBody(Ljava/lang/String;)Z

    move-result v0

    const/16 v17, 0x0

    if-eqz v0, :cond_4

    if-nez v11, :cond_3

    .line 10
    invoke-virtual {v14, v7, v7}, Lcom/intsig/office/pg/model/PGMaster;->addTitleBodyID(II)V

    goto :goto_1

    :cond_3
    if-ne v11, v6, :cond_6

    .line 11
    invoke-virtual {v13, v7, v8}, Lcom/intsig/office/pg/model/PGLayout;->addTitleBodyID(II)V

    goto :goto_1

    :cond_4
    if-eqz v11, :cond_5

    if-ne v11, v6, :cond_6

    .line 12
    :cond_5
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->isUserDrawn(Lcom/intsig/office/fc/dom4j/Element;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v4, 0x0

    goto :goto_2

    :cond_6
    :goto_1
    const/4 v0, -0x1

    const/4 v4, -0x1

    :goto_2
    const-string v3, "spPr"

    .line 13
    invoke-interface {v10, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 14
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v1

    const-string v2, "xfrm"

    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    move/from16 v2, p12

    move/from16 v6, p13

    invoke-virtual {v1, v0, v2, v6}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v0

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    :goto_3
    if-nez v0, :cond_8

    if-eqz v13, :cond_8

    .line 15
    invoke-virtual {v13, v5, v7}, Lcom/intsig/office/pg/model/PGLayout;->getAnchor(Ljava/lang/String;I)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v0

    if-nez v0, :cond_8

    if-eqz v14, :cond_8

    .line 16
    invoke-virtual {v14, v5, v7}, Lcom/intsig/office/pg/model/PGMaster;->getAnchor(Ljava/lang/String;I)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v0

    :cond_8
    if-eqz v0, :cond_16

    .line 17
    invoke-direct {v15, v9, v0}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v6

    if-nez p14, :cond_a

    if-nez p14, :cond_9

    .line 18
    invoke-static {}, Lcom/intsig/office/pg/model/PGPlaceholderUtil;->instance()Lcom/intsig/office/pg/model/PGPlaceholderUtil;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/intsig/office/pg/model/PGPlaceholderUtil;->isHeaderFooter(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    goto :goto_4

    :cond_9
    move-object/from16 v21, v3

    move/from16 v22, v4

    move-object/from16 p2, v5

    move-object/from16 p3, v6

    move/from16 p4, v7

    move/from16 p12, v8

    const/4 v10, 0x0

    const/16 v18, 0x1

    goto :goto_6

    .line 19
    :cond_a
    :goto_4
    invoke-direct {v15, v5, v7}, Lcom/intsig/office/fc/ppt/ShapeManage;->isRect(Ljava/lang/String;I)Z

    move-result v19

    if-nez p14, :cond_b

    invoke-static {}, Lcom/intsig/office/pg/model/PGPlaceholderUtil;->instance()Lcom/intsig/office/pg/model/PGPlaceholderUtil;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/intsig/office/pg/model/PGPlaceholderUtil;->isTitleOrBody(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v20, 0x1

    goto :goto_5

    :cond_b
    const/16 v20, 0x0

    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v21, v3

    move-object/from16 v3, p3

    move/from16 v22, v4

    move-object/from16 v4, p4

    move-object/from16 p2, v5

    move-object/from16 v5, p5

    move-object/from16 p3, v6

    const/16 v18, 0x1

    move-object/from16 v6, p6

    move/from16 p4, v7

    move-object/from16 v7, p8

    move/from16 p12, v8

    move-object/from16 v8, p10

    move/from16 v9, p12

    move/from16 v10, p4

    move-object/from16 v11, p3

    move/from16 v12, v19

    move-object/from16 v13, p11

    move/from16 v14, p9

    move-object/from16 v15, p2

    move/from16 v16, v20

    .line 20
    invoke-virtual/range {v0 .. v16}, Lcom/intsig/office/fc/ppt/ShapeManage;->processAutoShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;IILcom/intsig/office/java/awt/Rectangle;ZLcom/intsig/office/common/shape/GroupShape;BLjava/lang/String;Z)Lcom/intsig/office/common/shape/AbstractShape;

    move-result-object v2

    move-object v10, v2

    :goto_6
    if-eqz v10, :cond_d

    move-object/from16 v11, p11

    if-nez v11, :cond_c

    move-object/from16 v12, p8

    .line 21
    invoke-virtual {v12, v10}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    goto :goto_7

    :cond_c
    move-object/from16 v12, p8

    .line 22
    invoke-virtual {v11, v10}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    :goto_7
    move/from16 v0, v22

    .line 23
    invoke-virtual {v10, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setPlaceHolderID(I)V

    move-object/from16 v13, p10

    move-object/from16 v14, v21

    .line 24
    invoke-interface {v13, v14}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    move-object/from16 v15, p0

    invoke-direct {v15, v11, v10, v1}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    goto :goto_8

    :cond_d
    move-object/from16 v15, p0

    move-object/from16 v12, p8

    move-object/from16 v13, p10

    move-object/from16 v11, p11

    move-object/from16 v14, v21

    move/from16 v0, v22

    :goto_8
    const-string v1, "txBody"

    .line 25
    invoke-interface {v13, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v9

    if-eqz v9, :cond_15

    if-eqz p14, :cond_15

    .line 26
    new-instance v8, Lcom/intsig/office/common/shape/TextBox;

    invoke-direct {v8}, Lcom/intsig/office/common/shape/TextBox;-><init>()V

    move-object/from16 v1, p3

    .line 27
    invoke-virtual {v8, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 28
    invoke-virtual {v8, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setPlaceHolderID(I)V

    move/from16 v7, p12

    .line 29
    invoke-virtual {v8, v7}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 30
    new-instance v6, Lcom/intsig/office/simpletext/model/SectionElement;

    invoke-direct {v6}, Lcom/intsig/office/simpletext/model/SectionElement;-><init>()V

    const-wide/16 v2, 0x0

    .line 31
    invoke-virtual {v6, v2, v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->setStartOffset(J)V

    .line 32
    invoke-virtual {v8, v6}, Lcom/intsig/office/common/shape/TextBox;->setElement(Lcom/intsig/office/simpletext/model/SectionElement;)V

    .line 33
    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/AbstractElement;->getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v0

    .line 34
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    iget v3, v1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    int-to-float v3, v3

    const/high16 v4, 0x41700000    # 15.0f

    mul-float v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v0, v3}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    .line 35
    invoke-static {}, Lcom/intsig/office/simpletext/model/AttrManage;->instance()Lcom/intsig/office/simpletext/model/AttrManage;

    move-result-object v2

    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    int-to-float v1, v1

    mul-float v1, v1, v4

    float-to-int v1, v1

    invoke-virtual {v2, v0, v1}, Lcom/intsig/office/simpletext/model/AttrManage;->setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V

    move-object/from16 v3, p6

    move-object/from16 v4, p2

    move/from16 v5, p4

    if-eqz v3, :cond_e

    .line 36
    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/pg/model/PGLayout;->getSectionAttr(Ljava/lang/String;I)Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v2

    move-object/from16 v24, v2

    move-object/from16 v2, p5

    goto :goto_9

    :cond_e
    move-object/from16 v2, p5

    const/16 v24, 0x0

    :goto_9
    if-eqz v2, :cond_f

    .line 37
    invoke-virtual {v2, v4, v5}, Lcom/intsig/office/pg/model/PGMaster;->getSectionAttr(Ljava/lang/String;I)Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v1

    move-object/from16 v25, v1

    goto :goto_a

    :cond_f
    const/16 v25, 0x0

    .line 38
    :goto_a
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/SectionAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/SectionAttr;

    move-result-object v21

    const-string v1, "bodyPr"

    invoke-interface {v9, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v22

    const/16 v26, 0x0

    move-object/from16 v23, v0

    invoke-virtual/range {v21 .. v26}, Lcom/intsig/office/fc/ppt/attribute/SectionAttr;->setSectionAttribute(Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V

    .line 39
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/ParaAttr;

    move-result-object v0

    move-object/from16 p2, v1

    const-string v1, "style"

    .line 40
    invoke-interface {v13, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v16

    move-object/from16 v27, p2

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    move-object/from16 v19, v4

    move-object/from16 v4, p7

    move/from16 v20, v5

    move-object v5, v6

    move-object/from16 p2, v10

    move-object v10, v6

    move-object/from16 v6, v16

    move/from16 v16, v7

    move-object v7, v9

    move-object/from16 p1, v8

    move-object/from16 v8, v19

    move-object/from16 v28, v9

    move/from16 v9, v20

    .line 41
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/office/fc/ppt/attribute/ParaAttr;->processParagraph(Lcom/intsig/office/system/IControl;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGStyle;Lcom/intsig/office/simpletext/model/SectionElement;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/fc/dom4j/Element;Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    .line 42
    invoke-virtual {v10, v0, v1}, Lcom/intsig/office/simpletext/model/AbstractElement;->setEndOffset(J)V

    .line 43
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 44
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_11

    .line 45
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 46
    invoke-interface {v13, v14}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    move-object/from16 v1, p1

    invoke-direct {v15, v11, v1, v0}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    if-nez v11, :cond_10

    .line 47
    invoke-virtual {v12, v1}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    goto :goto_b

    .line 48
    :cond_10
    invoke-virtual {v11, v1}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    goto :goto_b

    :cond_11
    move-object/from16 v1, p1

    if-eqz p2, :cond_12

    .line 49
    invoke-interface {v13, v14}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    move-object/from16 v2, p2

    invoke-direct {v15, v11, v2, v0}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    :cond_12
    :goto_b
    move-object/from16 v2, v27

    move-object/from16 v0, v28

    .line 50
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_17

    const-string v2, "wrap"

    .line 51
    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    const-string v2, "square"

    .line 52
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    goto :goto_c

    :cond_13
    const/4 v6, 0x0

    goto :goto_d

    :cond_14
    :goto_c
    const/4 v6, 0x1

    :goto_d
    invoke-virtual {v1, v6}, Lcom/intsig/office/common/shape/TextBox;->setWrapLine(Z)V

    goto :goto_e

    :cond_15
    move/from16 v16, p12

    goto :goto_e

    :cond_16
    move/from16 v16, v8

    :cond_17
    :goto_e
    return v16
.end method

.method public processGraphicFrame(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FF)I
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v11, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v8, p7

    move-object/from16 v0, p8

    const-string v1, "nvGraphicFramePr"

    .line 1
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    const-string v4, "id"

    if-eqz v1, :cond_0

    const-string v7, "cNvPr"

    .line 2
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v12, v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v12, 0x0

    :goto_0
    const-string v1, "xfrm"

    .line 4
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    .line 5
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v7

    move/from16 v9, p10

    move/from16 v10, p11

    invoke-virtual {v7, v1, v9, v10}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v1

    if-nez v1, :cond_2

    if-eqz v6, :cond_2

    .line 6
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getPlaceholderType(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    move-result-object v1

    .line 7
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getPlaceholderIdx(Lcom/intsig/office/fc/dom4j/Element;)I

    move-result v7

    .line 8
    invoke-virtual {v6, v1, v7}, Lcom/intsig/office/pg/model/PGLayout;->getAnchor(Ljava/lang/String;I)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v13

    if-nez v13, :cond_1

    if-eqz v5, :cond_1

    .line 9
    invoke-virtual {v5, v1, v7}, Lcom/intsig/office/pg/model/PGMaster;->getAnchor(Ljava/lang/String;I)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v13

    :cond_2
    :goto_1
    if-eqz v1, :cond_7

    move-object/from16 v13, p9

    .line 10
    invoke-direct {v11, v13, v1}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v14

    const-string v1, "graphic"

    .line 11
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_7

    const-string v7, "graphicData"

    .line 12
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v1

    if-eqz v1, :cond_7

    const-string v7, "uri"

    .line 13
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v15

    if-eqz v15, :cond_7

    .line 14
    invoke-interface {v1, v7}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v15, "http://schemas.openxmlformats.org/presentationml/2006/ole"

    .line 15
    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    const-string v4, "oleObj"

    .line 16
    invoke-interface {v1, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v7

    if-nez v7, :cond_3

    const-string v0, "AlternateContent"

    .line 17
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v1, "Fallback"

    .line 18
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 19
    invoke-interface {v0, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v1, "pic"

    .line 20
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v7

    if-eqz v7, :cond_7

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    .line 21
    invoke-virtual/range {v0 .. v10}, Lcom/intsig/office/fc/ppt/ShapeManage;->processPicture(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FF)I

    goto/16 :goto_2

    :cond_3
    const-string v1, "spid"

    .line 22
    invoke-interface {v7, v1}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 23
    invoke-interface {v7, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 24
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/PictureReader;->instance()Lcom/intsig/office/fc/ppt/reader/PictureReader;

    move-result-object v4

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v4, v2, v3, v1, v5}, Lcom/intsig/office/fc/ppt/reader/PictureReader;->getOLEPart(Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    move-result-object v2

    const-string v1, "spPr"

    .line 25
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v3, p7

    move v4, v12

    move-object v5, v14

    move-object/from16 v7, p9

    move-object v8, v9

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/fc/ppt/ShapeManage;->addPicture(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGSlide;ILcom/intsig/office/java/awt/Rectangle;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)Lcom/intsig/office/common/shape/PictureShape;

    goto/16 :goto_2

    :cond_4
    const-string v0, "http://schemas.openxmlformats.org/drawingml/2006/chart"

    .line 26
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "chart"

    .line 27
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 28
    invoke-interface {v0, v4}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 29
    invoke-interface {v0, v4}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 30
    invoke-virtual {v3, v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    move-result-object v3

    .line 32
    invoke-static {}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->instance()Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;

    move-result-object v0

    invoke-virtual/range {p5 .. p5}, Lcom/intsig/office/pg/model/PGMaster;->getSchemeColor()Ljava/util/Map;

    move-result-object v4

    const/4 v5, 0x2

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/xls/Reader/drawing/ChartReader;->read(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Ljava/util/Map;B)Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 33
    new-instance v1, Lcom/intsig/office/common/shape/AChart;

    invoke-direct {v1}, Lcom/intsig/office/common/shape/AChart;-><init>()V

    .line 34
    invoke-virtual {v1, v0}, Lcom/intsig/office/common/shape/AChart;->setAChart(Lcom/intsig/office/thirdpart/achartengine/chart/AbstractChart;)V

    .line 35
    invoke-virtual {v1, v14}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 36
    invoke-virtual {v1, v12}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 37
    invoke-virtual {v8, v1}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    goto :goto_2

    :cond_5
    const-string v0, "http://schemas.openxmlformats.org/drawingml/2006/table"

    .line 38
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "tbl"

    .line 39
    invoke-interface {v1, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v6

    if-eqz v6, :cond_7

    const-string v0, "tblPr"

    .line 40
    invoke-interface {v6, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 41
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/TableReader;->instance()Lcom/intsig/office/fc/ppt/reader/TableReader;

    move-result-object v0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object v7, v14

    invoke-virtual/range {v0 .. v7}, Lcom/intsig/office/fc/ppt/reader/TableReader;->getTable(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/common/shape/TableShape;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 42
    invoke-virtual {v0, v14}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 43
    invoke-virtual {v0, v12}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 44
    invoke-virtual {v8, v0}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    goto :goto_2

    :cond_6
    const-string v0, "http://schemas.openxmlformats.org/drawingml/2006/diagram"

    .line 45
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 46
    invoke-direct {v11, v8, v1, v14}, Lcom/intsig/office/fc/ppt/ShapeManage;->processSmartArt(Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/java/awt/Rectangle;)V

    :cond_7
    :goto_2
    return v12
.end method

.method public processPicture(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FF)I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object/from16 v6, p2

    .line 2
    .line 3
    move-object/from16 v7, p3

    .line 4
    .line 5
    move-object/from16 v8, p4

    .line 6
    .line 7
    move-object/from16 v0, p5

    .line 8
    .line 9
    move-object/from16 v9, p7

    .line 10
    .line 11
    const-string v1, "nvPicPr"

    .line 12
    .line 13
    invoke-interface {v9, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    const-string v2, "cNvPr"

    .line 20
    .line 21
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    const-string v2, "id"

    .line 28
    .line 29
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    move v11, v1

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 v1, 0x0

    .line 40
    const/4 v11, 0x0

    .line 41
    :goto_0
    const-string v1, "blipFill"

    .line 42
    .line 43
    invoke-interface {v9, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    if-nez v2, :cond_1

    .line 48
    .line 49
    const-string v3, "AlternateContent"

    .line 50
    .line 51
    invoke-interface {v9, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    if-eqz v3, :cond_1

    .line 56
    .line 57
    const-string v4, "Fallback"

    .line 58
    .line 59
    invoke-interface {v3, v4}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 60
    .line 61
    .line 62
    move-result-object v3

    .line 63
    if-eqz v3, :cond_1

    .line 64
    .line 65
    invoke-interface {v3, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    :cond_1
    move-object v10, v2

    .line 70
    if-eqz v10, :cond_4

    .line 71
    .line 72
    const-string v1, "blip"

    .line 73
    .line 74
    invoke-interface {v10, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    if-eqz v1, :cond_4

    .line 79
    .line 80
    const-string v2, "embed"

    .line 81
    .line 82
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attribute(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Attribute;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    if-eqz v3, :cond_4

    .line 87
    .line 88
    invoke-interface {v1, v2}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    if-eqz v1, :cond_4

    .line 93
    .line 94
    const-string v12, "spPr"

    .line 95
    .line 96
    invoke-interface {v9, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 97
    .line 98
    .line 99
    move-result-object v5

    .line 100
    if-eqz v5, :cond_4

    .line 101
    .line 102
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    const-string v3, "xfrm"

    .line 107
    .line 108
    invoke-interface {v5, v3}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 109
    .line 110
    .line 111
    move-result-object v3

    .line 112
    move/from16 v4, p9

    .line 113
    .line 114
    move/from16 v13, p10

    .line 115
    .line 116
    invoke-virtual {v2, v3, v4, v13}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    if-nez v2, :cond_3

    .line 121
    .line 122
    if-eqz v0, :cond_3

    .line 123
    .line 124
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 125
    .line 126
    .line 127
    move-result-object v2

    .line 128
    invoke-virtual {v2, v9}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getPlaceholderType(Lcom/intsig/office/fc/dom4j/Element;)Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    .line 133
    .line 134
    .line 135
    move-result-object v3

    .line 136
    invoke-virtual {v3, v9}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getPlaceholderIdx(Lcom/intsig/office/fc/dom4j/Element;)I

    .line 137
    .line 138
    .line 139
    move-result v3

    .line 140
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/pg/model/PGLayout;->getAnchor(Ljava/lang/String;I)Lcom/intsig/office/java/awt/Rectangle;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    if-nez v0, :cond_2

    .line 145
    .line 146
    if-eqz v8, :cond_2

    .line 147
    .line 148
    invoke-virtual {v8, v2, v3}, Lcom/intsig/office/pg/model/PGMaster;->getAnchor(Ljava/lang/String;I)Lcom/intsig/office/java/awt/Rectangle;

    .line 149
    .line 150
    .line 151
    move-result-object v2

    .line 152
    goto :goto_1

    .line 153
    :cond_2
    move-object v2, v0

    .line 154
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 155
    .line 156
    move-object/from16 v13, p0

    .line 157
    .line 158
    move-object/from16 v14, p8

    .line 159
    .line 160
    invoke-direct {v13, v14, v2}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 161
    .line 162
    .line 163
    move-result-object v15

    .line 164
    invoke-virtual {v7, v1}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getRelationship(Ljava/lang/String;)Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;

    .line 165
    .line 166
    .line 167
    move-result-object v16

    .line 168
    if-eqz v16, :cond_5

    .line 169
    .line 170
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->instance()Lcom/intsig/office/fc/ppt/reader/BackgroundReader;

    .line 171
    .line 172
    .line 173
    move-result-object v0

    .line 174
    move-object/from16 v1, p1

    .line 175
    .line 176
    move-object/from16 v2, p2

    .line 177
    .line 178
    move-object/from16 v3, p3

    .line 179
    .line 180
    move-object/from16 v4, p4

    .line 181
    .line 182
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/ppt/reader/BackgroundReader;->processBackground(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/bg/BackgroundAndFill;

    .line 183
    .line 184
    .line 185
    move-result-object v0

    .line 186
    invoke-static {v1, v6, v7, v8, v9}, Lcom/intsig/office/fc/LineKit;->createShapeLine(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/borders/Line;

    .line 187
    .line 188
    .line 189
    move-result-object v8

    .line 190
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/fc/openxml4j/opc/PackageRelationship;->getTargetURI()Ljava/net/URI;

    .line 191
    .line 192
    .line 193
    move-result-object v2

    .line 194
    invoke-virtual {v6, v2}, Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;->getPart(Ljava/net/URI;)Lcom/intsig/office/fc/openxml4j/opc/PackagePart;

    .line 195
    .line 196
    .line 197
    move-result-object v4

    .line 198
    invoke-interface {v9, v12}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    .line 199
    .line 200
    .line 201
    move-result-object v9

    .line 202
    invoke-static {v10}, Lcom/intsig/office/common/pictureefftect/PictureEffectInfoFactory;->getPictureEffectInfor(Lcom/intsig/office/fc/dom4j/Element;)Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;

    .line 203
    .line 204
    .line 205
    move-result-object v10

    .line 206
    move-object/from16 v2, p0

    .line 207
    .line 208
    move-object/from16 v3, p1

    .line 209
    .line 210
    move-object/from16 v5, p6

    .line 211
    .line 212
    move v6, v11

    .line 213
    move-object v7, v15

    .line 214
    move-object v1, v8

    .line 215
    move-object v8, v9

    .line 216
    move-object/from16 v9, p8

    .line 217
    .line 218
    invoke-virtual/range {v2 .. v10}, Lcom/intsig/office/fc/ppt/ShapeManage;->addPicture(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGSlide;ILcom/intsig/office/java/awt/Rectangle;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/common/pictureefftect/PictureEffectInfo;)Lcom/intsig/office/common/shape/PictureShape;

    .line 219
    .line 220
    .line 221
    move-result-object v2

    .line 222
    if-eqz v2, :cond_5

    .line 223
    .line 224
    invoke-virtual {v2, v0}, Lcom/intsig/office/common/shape/AbstractShape;->setBackgroundAndFill(Lcom/intsig/office/common/bg/BackgroundAndFill;)V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v2, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setLine(Lcom/intsig/office/common/borders/Line;)V

    .line 228
    .line 229
    .line 230
    goto :goto_2

    .line 231
    :cond_4
    move-object/from16 v13, p0

    .line 232
    .line 233
    :cond_5
    :goto_2
    return v11
.end method

.method public processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGStyle;Lcom/intsig/office/pg/model/PGSlide;BLcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FF)Ljava/lang/Integer;
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v14, p8

    move-object/from16 v10, p10

    move-object/from16 v13, p11

    move/from16 v12, p12

    move/from16 v11, p13

    .line 1
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->isHidden(Lcom/intsig/office/fc/dom4j/Element;)Z

    move-result v0

    const/16 v16, 0x0

    if-eqz v0, :cond_0

    return-object v16

    .line 2
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/fc/openxml4j/opc/PackagePart;->getPartName()Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/office/fc/openxml4j/opc/PackagePartName;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/ppt/slides/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const/16 v17, 0x1

    const/4 v9, 0x0

    if-nez v0, :cond_2

    if-nez v0, :cond_1

    .line 3
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->isUserDrawn(Lcom/intsig/office/fc/dom4j/Element;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v8, 0x1

    .line 4
    :goto_1
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setSlide(Z)V

    .line 5
    invoke-interface/range {p10 .. p10}, Lcom/intsig/office/fc/dom4j/Node;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sp"

    .line 6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "cxnSp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto/16 :goto_7

    :cond_3
    const-string v1, "pic"

    .line 7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v8, :cond_c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p8

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move/from16 v9, p12

    move/from16 v10, p13

    .line 8
    invoke-virtual/range {v0 .. v10}, Lcom/intsig/office/fc/ppt/ShapeManage;->processPicture(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FF)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_4
    const-string v1, "graphicFrame"

    .line 9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v8, :cond_c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    move/from16 v10, p12

    move/from16 v11, p13

    .line 10
    invoke-virtual/range {v0 .. v11}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGraphicFrame(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGSlide;Lcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FF)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_5
    const-string v1, "grpSp"

    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v0, "nvGrpSpPr"

    .line 12
    invoke-interface {v10, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v1, "cNvPr"

    .line 13
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v1, "id"

    .line 14
    invoke-interface {v0, v1}, Lcom/intsig/office/fc/dom4j/Element;->attributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v8, v0

    goto :goto_2

    :cond_6
    const/4 v8, 0x0

    :goto_2
    const-string v0, "grpSpPr"

    .line 15
    invoke-interface {v10, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 16
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v1

    const-string v2, "xfrm"

    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v3

    invoke-virtual {v1, v3, v12, v11}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v1

    .line 17
    invoke-direct {v15, v13, v1}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpSpRect(Lcom/intsig/office/common/shape/GroupShape;Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v1

    .line 18
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v3

    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getAnchorFitZoom(Lcom/intsig/office/fc/dom4j/Element;)[F

    move-result-object v16

    .line 19
    invoke-static {}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->instance()Lcom/intsig/office/fc/ppt/reader/ReaderKit;

    move-result-object v3

    invoke-interface {v0, v2}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v2

    aget v4, v16, v9

    mul-float v4, v4, v12

    aget v5, v16, v17

    mul-float v5, v5, v11

    invoke-virtual {v3, v2, v4, v5}, Lcom/intsig/office/fc/ppt/reader/ReaderKit;->getChildShapeAnchor(Lcom/intsig/office/fc/dom4j/Element;FF)Lcom/intsig/office/java/awt/Rectangle;

    move-result-object v2

    .line 20
    new-instance v3, Lcom/intsig/office/common/shape/GroupShape;

    invoke-direct {v3}, Lcom/intsig/office/common/shape/GroupShape;-><init>()V

    .line 21
    iget v4, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    iget v5, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    sub-int/2addr v4, v5

    iget v5, v1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    sub-int/2addr v5, v2

    invoke-virtual {v3, v4, v5}, Lcom/intsig/office/common/shape/GroupShape;->setOffPostion(II)V

    .line 22
    invoke-virtual {v3, v8}, Lcom/intsig/office/common/shape/AbstractShape;->setShapeID(I)V

    .line 23
    invoke-virtual {v3, v1}, Lcom/intsig/office/common/shape/AbstractShape;->setBounds(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 24
    invoke-virtual {v3, v13}, Lcom/intsig/office/common/shape/AbstractShape;->setParent(Lcom/intsig/office/common/shape/IShape;)V

    .line 25
    invoke-direct {v15, v13, v3, v0}, Lcom/intsig/office/fc/ppt/ShapeManage;->processGrpRotation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/fc/dom4j/Element;)V

    move-object v7, v3

    goto :goto_3

    :cond_7
    move-object/from16 v7, v16

    .line 26
    :goto_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 27
    invoke-interface/range {p10 .. p10}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 28
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/intsig/office/fc/dom4j/Element;

    aget v0, v16, v9

    mul-float v19, v0, v12

    aget v0, v16, v17

    mul-float v20, v0, v11

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object v15, v6

    move-object/from16 v6, p6

    move-object/from16 p10, v7

    move-object/from16 v7, p7

    move/from16 v21, v8

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v11, p10

    move/from16 v12, v19

    move-object v14, v13

    move/from16 v13, v20

    .line 29
    invoke-virtual/range {v0 .. v13}, Lcom/intsig/office/fc/ppt/ShapeManage;->processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGStyle;Lcom/intsig/office/pg/model/PGSlide;BLcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FF)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 30
    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    move-object/from16 v7, p10

    move/from16 v12, p12

    move/from16 v11, p13

    move-object v13, v14

    move-object v6, v15

    move/from16 v8, v21

    const/4 v9, 0x0

    move-object/from16 v15, p0

    move-object/from16 v14, p8

    goto :goto_4

    :cond_9
    move-object v15, v6

    move-object/from16 p10, v7

    move/from16 v21, v8

    move-object v14, v13

    if-nez v14, :cond_a

    move-object/from16 v14, p8

    move-object/from16 v3, p10

    .line 31
    invoke-virtual {v14, v3}, Lcom/intsig/office/pg/model/PGSlide;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    goto :goto_5

    :cond_a
    move-object/from16 v3, p10

    move-object v13, v14

    move-object/from16 v14, p8

    .line 32
    invoke-virtual {v13, v3}, Lcom/intsig/office/common/shape/GroupShape;->appendShapes(Lcom/intsig/office/common/shape/IShape;)V

    :goto_5
    move/from16 v9, v21

    .line 33
    invoke-virtual {v14, v9, v15}, Lcom/intsig/office/pg/model/PGSlide;->addGroupShape(ILjava/util/List;)V

    .line 34
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_b
    const-string v1, "AlternateContent"

    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "Fallback"

    .line 36
    invoke-interface {v10, v0}, Lcom/intsig/office/fc/dom4j/Element;->element(Ljava/lang/String;)Lcom/intsig/office/fc/dom4j/Element;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 37
    invoke-interface {v0}, Lcom/intsig/office/fc/dom4j/Element;->elementIterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 38
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/intsig/office/fc/dom4j/Element;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    .line 39
    invoke-virtual/range {v0 .. v13}, Lcom/intsig/office/fc/ppt/ShapeManage;->processShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGStyle;Lcom/intsig/office/pg/model/PGSlide;BLcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FF)Ljava/lang/Integer;

    move-object/from16 v13, p11

    goto :goto_6

    .line 40
    :cond_c
    invoke-static {}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->instance()Lcom/intsig/office/fc/ppt/attribute/RunAttr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/intsig/office/fc/ppt/attribute/RunAttr;->setSlide(Z)V

    return-object v16

    :cond_d
    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v17, v8

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, v17

    .line 41
    invoke-virtual/range {v0 .. v14}, Lcom/intsig/office/fc/ppt/ShapeManage;->processAutoShapeAndTextShape(Lcom/intsig/office/system/IControl;Lcom/intsig/office/fc/openxml4j/opc/ZipPackage;Lcom/intsig/office/fc/openxml4j/opc/PackagePart;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/model/PGMaster;Lcom/intsig/office/pg/model/PGLayout;Lcom/intsig/office/pg/model/PGStyle;Lcom/intsig/office/pg/model/PGSlide;BLcom/intsig/office/fc/dom4j/Element;Lcom/intsig/office/common/shape/GroupShape;FFZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

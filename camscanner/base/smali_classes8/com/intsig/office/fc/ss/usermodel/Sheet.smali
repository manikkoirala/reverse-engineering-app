.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/Sheet;
.super Ljava/lang/Object;
.source "Sheet.java"


# static fields
.field public static final BottomMargin:S = 0x3s

.field public static final FooterMargin:S = 0x5s

.field public static final HeaderMargin:S = 0x4s

.field public static final LeftMargin:S = 0x0s

.field public static final PANE_LOWER_LEFT:B = 0x2t

.field public static final PANE_LOWER_RIGHT:B = 0x0t

.field public static final PANE_UPPER_LEFT:B = 0x3t

.field public static final PANE_UPPER_RIGHT:B = 0x1t

.field public static final RightMargin:S = 0x1s

.field public static final TopMargin:S = 0x2s

.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;
.super Ljava/lang/Object;
.source "FormulaEvaluator.java"


# virtual methods
.method public abstract clearAllCachedResultValues()V
.end method

.method public abstract evaluate(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/CellValue;
.end method

.method public abstract evaluateAll()V
.end method

.method public abstract evaluateFormulaCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)I
.end method

.method public abstract evaluateInCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/usermodel/ICell;
.end method

.method public abstract notifyDeleteCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)V
.end method

.method public abstract notifySetFormula(Lcom/intsig/office/fc/ss/usermodel/ICell;)V
.end method

.method public abstract notifyUpdateCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)V
.end method

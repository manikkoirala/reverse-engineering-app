.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/DataValidationHelper;
.super Ljava/lang/Object;
.source "DataValidationHelper.java"


# virtual methods
.method public abstract createCustomConstraint(Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createDateConstraint(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createDecimalConstraint(ILjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createExplicitListConstraint([Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createFormulaListConstraint(Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createIntegerConstraint(ILjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createNumericConstraint(IILjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createTextLengthConstraint(ILjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createTimeConstraint(ILjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;
.end method

.method public abstract createValidation(Lcom/intsig/office/fc/ss/usermodel/DataValidationConstraint;Lcom/intsig/office/fc/ss/util/CellRangeAddressList;)Lcom/intsig/office/fc/ss/usermodel/DataValidation;
.end method

.class public Lcom/intsig/office/fc/ss/usermodel/DataFormatter;
.super Ljava/lang/Object;
.source "DataFormatter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/ss/usermodel/DataFormatter$ConstantStringFormat;,
        Lcom/intsig/office/fc/ss/usermodel/DataFormatter$PhoneFormat;,
        Lcom/intsig/office/fc/ss/usermodel/DataFormatter$ZipPlusFourFormat;,
        Lcom/intsig/office/fc/ss/usermodel/DataFormatter$SSNFormat;
    }
.end annotation


# static fields
.field private static final amPmPattern:Ljava/util/regex/Pattern;

.field private static final colorPattern:Ljava/util/regex/Pattern;

.field private static final daysAsText:Ljava/util/regex/Pattern;

.field private static final invalidDateTimeString:Ljava/lang/String;

.field private static final numPattern:Ljava/util/regex/Pattern;

.field private static final specialPatternGroup:Ljava/util/regex/Pattern;


# instance fields
.field private final dateSymbols:Ljava/text/DateFormatSymbols;

.field private final decimalSymbols:Ljava/text/DecimalFormatSymbols;

.field private defaultNumFormat:Ljava/text/Format;

.field private emulateCsv:Z

.field private final formats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/text/Format;",
            ">;"
        }
    .end annotation
.end field

.field private final generalDecimalNumFormat:Ljava/text/Format;

.field private final generalWholeNumFormat:Ljava/text/Format;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    const-string v0, "[0#]+"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->numPattern:Ljava/util/regex/Pattern;

    .line 8
    .line 9
    const-string v0, "([d]{3,})"

    .line 10
    .line 11
    const/4 v1, 0x2

    .line 12
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->daysAsText:Ljava/util/regex/Pattern;

    .line 17
    .line 18
    const-string v0, "((A|P)[M/P]*)"

    .line 19
    .line 20
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->amPmPattern:Ljava/util/regex/Pattern;

    .line 25
    .line 26
    const-string v0, "(\\[\\$[^-\\]]*-[0-9A-Z]+\\])"

    .line 27
    .line 28
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->specialPatternGroup:Ljava/util/regex/Pattern;

    .line 33
    .line 34
    const-string v0, "(\\[BLACK\\])|(\\[BLUE\\])|(\\[CYAN\\])|(\\[GREEN\\])|(\\[MAGENTA\\])|(\\[RED\\])|(\\[WHITE\\])|(\\[YELLOW\\])|(\\[COLOR\\s*\\d\\])|(\\[COLOR\\s*[0-5]\\d\\])"

    .line 35
    .line 36
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->colorPattern:Ljava/util/regex/Pattern;

    .line 41
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    const/4 v1, 0x0

    .line 48
    :goto_0
    const/16 v2, 0xff

    .line 49
    .line 50
    if-ge v1, v2, :cond_0

    .line 51
    .line 52
    const/16 v2, 0x23

    .line 53
    .line 54
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    add-int/lit8 v1, v1, 0x1

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->invalidDateTimeString:Ljava/lang/String;

    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 2

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->emulateCsv:Z

    .line 8
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0, p1}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->dateSymbols:Ljava/text/DateFormatSymbols;

    .line 9
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0, p1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->decimalSymbols:Ljava/text/DecimalFormatSymbols;

    .line 10
    new-instance p1, Ljava/text/DecimalFormat;

    const-string v1, "#"

    invoke-direct {p1, v1, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->generalWholeNumFormat:Ljava/text/Format;

    .line 11
    new-instance p1, Ljava/text/DecimalFormat;

    const-string v1, "#.##########"

    invoke-direct {p1, v1, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->generalDecimalNumFormat:Ljava/text/Format;

    .line 12
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->formats:Ljava/util/Map;

    .line 13
    sget-object p1, Lcom/intsig/office/fc/ss/usermodel/DataFormatter$ZipPlusFourFormat;->o0:Ljava/text/Format;

    const-string v0, "00000\\-0000"

    .line 14
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->addFormat(Ljava/lang/String;Ljava/text/Format;)V

    const-string v0, "00000-0000"

    .line 15
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->addFormat(Ljava/lang/String;Ljava/text/Format;)V

    .line 16
    sget-object p1, Lcom/intsig/office/fc/ss/usermodel/DataFormatter$PhoneFormat;->o0:Ljava/text/Format;

    const-string v0, "[<=9999999]###\\-####;\\(###\\)\\ ###\\-####"

    .line 17
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->addFormat(Ljava/lang/String;Ljava/text/Format;)V

    const-string v0, "[<=9999999]###-####;(###) ###-####"

    .line 18
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->addFormat(Ljava/lang/String;Ljava/text/Format;)V

    const-string v0, "###\\-####;\\(###\\)\\ ###\\-####"

    .line 19
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->addFormat(Ljava/lang/String;Ljava/text/Format;)V

    const-string v0, "###-####;(###) ###-####"

    .line 20
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->addFormat(Ljava/lang/String;Ljava/text/Format;)V

    .line 21
    sget-object p1, Lcom/intsig/office/fc/ss/usermodel/DataFormatter$SSNFormat;->o0:Ljava/text/Format;

    const-string v0, "000\\-00\\-0000"

    .line 22
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->addFormat(Ljava/lang/String;Ljava/text/Format;)V

    const-string v0, "000-00-0000"

    .line 23
    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->addFormat(Ljava/lang/String;Ljava/text/Format;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;Z)V
    .locals 0

    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;-><init>(Ljava/util/Locale;)V

    .line 5
    iput-boolean p2, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->emulateCsv:Z

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;-><init>(Ljava/util/Locale;)V

    .line 3
    iput-boolean p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->emulateCsv:Z

    return-void
.end method

.method private cleanFormatForNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->emulateCsv:Z

    .line 7
    .line 8
    const/16 v1, 0x2a

    .line 9
    .line 10
    const/16 v2, 0x5c

    .line 11
    .line 12
    const/16 v3, 0x5f

    .line 13
    .line 14
    const/4 v4, 0x0

    .line 15
    if-eqz p1, :cond_5

    .line 16
    .line 17
    const/4 p1, 0x0

    .line 18
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    .line 19
    .line 20
    .line 21
    move-result v5

    .line 22
    if-ge p1, v5, :cond_a

    .line 23
    .line 24
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 25
    .line 26
    .line 27
    move-result v5

    .line 28
    const/16 v6, 0x3f

    .line 29
    .line 30
    if-eq v5, v3, :cond_0

    .line 31
    .line 32
    if-eq v5, v1, :cond_0

    .line 33
    .line 34
    if-ne v5, v6, :cond_4

    .line 35
    .line 36
    :cond_0
    if-lez p1, :cond_1

    .line 37
    .line 38
    add-int/lit8 v7, p1, -0x1

    .line 39
    .line 40
    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 41
    .line 42
    .line 43
    move-result v7

    .line 44
    if-ne v7, v2, :cond_1

    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_1
    const/16 v7, 0x20

    .line 48
    .line 49
    if-ne v5, v6, :cond_2

    .line 50
    .line 51
    invoke-virtual {v0, p1, v7}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 52
    .line 53
    .line 54
    goto :goto_2

    .line 55
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    .line 56
    .line 57
    .line 58
    move-result v6

    .line 59
    add-int/lit8 v6, v6, -0x1

    .line 60
    .line 61
    if-ge p1, v6, :cond_4

    .line 62
    .line 63
    if-ne v5, v3, :cond_3

    .line 64
    .line 65
    add-int/lit8 v5, p1, 0x1

    .line 66
    .line 67
    invoke-virtual {v0, v5, v7}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 68
    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_3
    add-int/lit8 v5, p1, 0x1

    .line 72
    .line 73
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 74
    .line 75
    .line 76
    :goto_1
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 77
    .line 78
    .line 79
    :cond_4
    :goto_2
    add-int/lit8 p1, p1, 0x1

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_5
    const/4 p1, 0x0

    .line 83
    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    .line 84
    .line 85
    .line 86
    move-result v5

    .line 87
    if-ge p1, v5, :cond_a

    .line 88
    .line 89
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 90
    .line 91
    .line 92
    move-result v5

    .line 93
    if-eq v5, v3, :cond_6

    .line 94
    .line 95
    if-ne v5, v1, :cond_9

    .line 96
    .line 97
    :cond_6
    if-lez p1, :cond_7

    .line 98
    .line 99
    add-int/lit8 v5, p1, -0x1

    .line 100
    .line 101
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 102
    .line 103
    .line 104
    move-result v5

    .line 105
    if-ne v5, v2, :cond_7

    .line 106
    .line 107
    goto :goto_4

    .line 108
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    .line 109
    .line 110
    .line 111
    move-result v5

    .line 112
    add-int/lit8 v5, v5, -0x1

    .line 113
    .line 114
    if-ge p1, v5, :cond_8

    .line 115
    .line 116
    add-int/lit8 v5, p1, 0x1

    .line 117
    .line 118
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 119
    .line 120
    .line 121
    :cond_8
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 122
    .line 123
    .line 124
    :cond_9
    :goto_4
    add-int/lit8 p1, p1, 0x1

    .line 125
    .line 126
    goto :goto_3

    .line 127
    :cond_a
    :goto_5
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    .line 128
    .line 129
    .line 130
    move-result p1

    .line 131
    if-ge v4, p1, :cond_e

    .line 132
    .line 133
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 134
    .line 135
    .line 136
    move-result p1

    .line 137
    if-eq p1, v2, :cond_c

    .line 138
    .line 139
    const/16 v1, 0x22

    .line 140
    .line 141
    if-ne p1, v1, :cond_b

    .line 142
    .line 143
    goto :goto_6

    .line 144
    :cond_b
    const/16 v1, 0x2b

    .line 145
    .line 146
    if-ne p1, v1, :cond_d

    .line 147
    .line 148
    if-lez v4, :cond_d

    .line 149
    .line 150
    add-int/lit8 p1, v4, -0x1

    .line 151
    .line 152
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 153
    .line 154
    .line 155
    move-result p1

    .line 156
    const/16 v1, 0x45

    .line 157
    .line 158
    if-ne p1, v1, :cond_d

    .line 159
    .line 160
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 161
    .line 162
    .line 163
    goto :goto_7

    .line 164
    :cond_c
    :goto_6
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 165
    .line 166
    .line 167
    :goto_7
    add-int/lit8 v4, v4, -0x1

    .line 168
    .line 169
    :cond_d
    add-int/lit8 v4, v4, 0x1

    .line 170
    .line 171
    goto :goto_5

    .line 172
    :cond_e
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object p1

    .line 176
    return-object p1
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private createDateFormat(Ljava/lang/String;D)Ljava/text/Format;
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const-string v1, "\\\\-"

    .line 4
    .line 5
    const-string v2, "-"

    .line 6
    .line 7
    move-object/from16 v3, p1

    .line 8
    .line 9
    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const-string v2, "\\\\,"

    .line 14
    .line 15
    const-string v3, ","

    .line 16
    .line 17
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const-string v2, "\\\\\\."

    .line 22
    .line 23
    const-string v3, "."

    .line 24
    .line 25
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    const-string v2, "\\\\ "

    .line 30
    .line 31
    const-string v3, " "

    .line 32
    .line 33
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    const-string v2, "\\\\/"

    .line 38
    .line 39
    const-string v3, "/"

    .line 40
    .line 41
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    const-string v2, ";@"

    .line 46
    .line 47
    const-string v4, ""

    .line 48
    .line 49
    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    const-string v2, "\"/\""

    .line 54
    .line 55
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    sget-object v2, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->amPmPattern:Ljava/util/regex/Pattern;

    .line 60
    .line 61
    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    const/4 v3, 0x0

    .line 66
    const/4 v4, 0x0

    .line 67
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    .line 68
    .line 69
    .line 70
    move-result v5

    .line 71
    const-string v6, "@"

    .line 72
    .line 73
    const/4 v7, 0x1

    .line 74
    if-eqz v5, :cond_0

    .line 75
    .line 76
    invoke-virtual {v2, v6}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    sget-object v2, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->amPmPattern:Ljava/util/regex/Pattern;

    .line 81
    .line 82
    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    const/4 v4, 0x1

    .line 87
    goto :goto_0

    .line 88
    :cond_0
    const-string v2, "a"

    .line 89
    .line 90
    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    sget-object v2, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->daysAsText:Ljava/util/regex/Pattern;

    .line 95
    .line 96
    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    if-eqz v5, :cond_1

    .line 105
    .line 106
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v1

    .line 110
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    const-string v5, "D"

    .line 115
    .line 116
    const-string v6, "E"

    .line 117
    .line 118
    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    .line 127
    .line 128
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 129
    .line 130
    .line 131
    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    new-instance v5, Ljava/util/ArrayList;

    .line 136
    .line 137
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .line 139
    .line 140
    const/4 v6, 0x0

    .line 141
    const/4 v8, 0x0

    .line 142
    const/4 v9, 0x1

    .line 143
    :goto_1
    array-length v10, v1

    .line 144
    if-ge v6, v10, :cond_1a

    .line 145
    .line 146
    aget-char v10, v1, v6

    .line 147
    .line 148
    const/16 v11, 0x5b

    .line 149
    .line 150
    if-ne v10, v11, :cond_2

    .line 151
    .line 152
    if-nez v8, :cond_2

    .line 153
    .line 154
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 155
    .line 156
    .line 157
    const/4 v8, 0x1

    .line 158
    :goto_2
    const/4 v9, 0x0

    .line 159
    goto/16 :goto_d

    .line 160
    .line 161
    :cond_2
    const/16 v11, 0x5d

    .line 162
    .line 163
    if-ne v10, v11, :cond_3

    .line 164
    .line 165
    if-eqz v8, :cond_3

    .line 166
    .line 167
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 168
    .line 169
    .line 170
    const/4 v8, 0x0

    .line 171
    goto/16 :goto_d

    .line 172
    .line 173
    :cond_3
    const/16 v11, 0x53

    .line 174
    .line 175
    const/16 v12, 0x68

    .line 176
    .line 177
    const/16 v13, 0x73

    .line 178
    .line 179
    const/16 v14, 0x4d

    .line 180
    .line 181
    const/16 v15, 0x6d

    .line 182
    .line 183
    const/16 v3, 0x48

    .line 184
    .line 185
    if-eqz v8, :cond_a

    .line 186
    .line 187
    if-eq v10, v12, :cond_9

    .line 188
    .line 189
    if-ne v10, v3, :cond_4

    .line 190
    .line 191
    goto :goto_5

    .line 192
    :cond_4
    if-eq v10, v15, :cond_8

    .line 193
    .line 194
    if-ne v10, v14, :cond_5

    .line 195
    .line 196
    goto :goto_4

    .line 197
    :cond_5
    if-eq v10, v13, :cond_7

    .line 198
    .line 199
    if-ne v10, v11, :cond_6

    .line 200
    .line 201
    goto :goto_3

    .line 202
    :cond_6
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 203
    .line 204
    .line 205
    goto/16 :goto_d

    .line 206
    .line 207
    :cond_7
    :goto_3
    invoke-virtual {v2, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 208
    .line 209
    .line 210
    goto/16 :goto_d

    .line 211
    .line 212
    :cond_8
    :goto_4
    invoke-virtual {v2, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 213
    .line 214
    .line 215
    goto/16 :goto_d

    .line 216
    .line 217
    :cond_9
    :goto_5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 218
    .line 219
    .line 220
    goto/16 :goto_d

    .line 221
    .line 222
    :cond_a
    if-eq v10, v12, :cond_18

    .line 223
    .line 224
    if-ne v10, v3, :cond_b

    .line 225
    .line 226
    goto/16 :goto_c

    .line 227
    .line 228
    :cond_b
    if-eq v10, v15, :cond_16

    .line 229
    .line 230
    if-ne v10, v14, :cond_c

    .line 231
    .line 232
    goto :goto_b

    .line 233
    :cond_c
    if-eq v10, v13, :cond_13

    .line 234
    .line 235
    if-ne v10, v11, :cond_d

    .line 236
    .line 237
    goto :goto_8

    .line 238
    :cond_d
    invoke-static {v10}, Ljava/lang/Character;->isLetter(C)Z

    .line 239
    .line 240
    .line 241
    move-result v3

    .line 242
    if-eqz v3, :cond_12

    .line 243
    .line 244
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 245
    .line 246
    .line 247
    const/16 v3, 0x79

    .line 248
    .line 249
    if-eq v10, v3, :cond_11

    .line 250
    .line 251
    const/16 v9, 0x59

    .line 252
    .line 253
    if-ne v10, v9, :cond_e

    .line 254
    .line 255
    goto :goto_7

    .line 256
    :cond_e
    const/16 v3, 0x64

    .line 257
    .line 258
    if-eq v10, v3, :cond_10

    .line 259
    .line 260
    const/16 v9, 0x44

    .line 261
    .line 262
    if-ne v10, v9, :cond_f

    .line 263
    .line 264
    goto :goto_6

    .line 265
    :cond_f
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 266
    .line 267
    .line 268
    goto :goto_a

    .line 269
    :cond_10
    :goto_6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 270
    .line 271
    .line 272
    goto :goto_a

    .line 273
    :cond_11
    :goto_7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 274
    .line 275
    .line 276
    goto :goto_a

    .line 277
    :cond_12
    invoke-virtual {v2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 278
    .line 279
    .line 280
    goto :goto_d

    .line 281
    :cond_13
    :goto_8
    invoke-virtual {v2, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 282
    .line 283
    .line 284
    const/4 v3, 0x0

    .line 285
    :goto_9
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 286
    .line 287
    .line 288
    move-result v9

    .line 289
    if-ge v3, v9, :cond_15

    .line 290
    .line 291
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 292
    .line 293
    .line 294
    move-result-object v9

    .line 295
    check-cast v9, Ljava/lang/Integer;

    .line 296
    .line 297
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    .line 298
    .line 299
    .line 300
    move-result v9

    .line 301
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 302
    .line 303
    .line 304
    move-result v10

    .line 305
    if-ne v10, v14, :cond_14

    .line 306
    .line 307
    add-int/lit8 v10, v9, 0x1

    .line 308
    .line 309
    const-string v11, "m"

    .line 310
    .line 311
    invoke-virtual {v2, v9, v10, v11}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 312
    .line 313
    .line 314
    :cond_14
    add-int/lit8 v3, v3, 0x1

    .line 315
    .line 316
    goto :goto_9

    .line 317
    :cond_15
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 318
    .line 319
    .line 320
    :goto_a
    const/4 v9, 0x1

    .line 321
    goto :goto_d

    .line 322
    :cond_16
    :goto_b
    if-eqz v9, :cond_17

    .line 323
    .line 324
    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 325
    .line 326
    .line 327
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    .line 328
    .line 329
    .line 330
    move-result v3

    .line 331
    sub-int/2addr v3, v7

    .line 332
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 333
    .line 334
    .line 335
    move-result-object v3

    .line 336
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    .line 338
    .line 339
    goto :goto_d

    .line 340
    :cond_17
    invoke-virtual {v2, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 341
    .line 342
    .line 343
    goto :goto_d

    .line 344
    :cond_18
    :goto_c
    if-eqz v4, :cond_19

    .line 345
    .line 346
    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 347
    .line 348
    .line 349
    goto/16 :goto_2

    .line 350
    .line 351
    :cond_19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 352
    .line 353
    .line 354
    goto/16 :goto_2

    .line 355
    .line 356
    :goto_d
    add-int/lit8 v6, v6, 0x1

    .line 357
    .line 358
    const/4 v3, 0x0

    .line 359
    goto/16 :goto_1

    .line 360
    .line 361
    :cond_1a
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 362
    .line 363
    .line 364
    move-result-object v1

    .line 365
    :try_start_0
    new-instance v2, Lcom/intsig/office/fc/ss/usermodel/ExcelStyleDateFormatter;

    .line 366
    .line 367
    iget-object v3, v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->dateSymbols:Ljava/text/DateFormatSymbols;

    .line 368
    .line 369
    invoke-direct {v2, v1, v3}, Lcom/intsig/office/fc/ss/usermodel/ExcelStyleDateFormatter;-><init>(Ljava/lang/String;Ljava/text/DateFormatSymbols;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    .line 371
    .line 372
    return-object v2

    .line 373
    :catch_0
    move-wide/from16 v1, p2

    .line 374
    .line 375
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getDefaultFormat(D)Ljava/text/Format;

    .line 376
    .line 377
    .line 378
    move-result-object v1

    .line 379
    return-object v1
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private createFormat(DILjava/lang/String;)Ljava/text/Format;
    .locals 7

    .line 4
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->colorPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 5
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-eqz v1, :cond_2

    .line 6
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 7
    invoke-virtual {p4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_0

    goto :goto_1

    .line 8
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v1, v0

    invoke-virtual {p4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 10
    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 11
    :cond_1
    sget-object p4, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->colorPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p4, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p4

    move-object v6, v0

    move-object v0, p4

    move-object p4, v6

    goto :goto_0

    .line 12
    :cond_2
    :goto_1
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->specialPatternGroup:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 13
    :goto_2
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 14
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object p4

    const/16 v1, 0x24

    .line 15
    invoke-virtual {p4, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/16 v5, 0x2d

    invoke-virtual {p4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {p4, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p4

    .line 16
    invoke-virtual {p4, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-le v4, v3, :cond_3

    .line 17
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 18
    invoke-virtual {p4, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {p4, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v5, 0x5c

    .line 19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 20
    invoke-virtual {p4, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p4, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 21
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p4

    .line 22
    :cond_3
    invoke-virtual {v0, p4}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 23
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->specialPatternGroup:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    goto :goto_2

    :cond_4
    if-eqz p4, :cond_9

    .line 24
    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    goto :goto_3

    .line 25
    :cond_5
    invoke-static {p3, p4}, Lcom/intsig/office/ss/util/DateUtil;->isADateFormat(ILjava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_6

    invoke-static {p1, p2}, Lcom/intsig/office/ss/util/DateUtil;->isValidExcelDate(D)Z

    move-result p3

    if-eqz p3, :cond_6

    .line 26
    invoke-direct {p0, p4, p1, p2}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->createDateFormat(Ljava/lang/String;D)Ljava/text/Format;

    move-result-object p1

    return-object p1

    .line 27
    :cond_6
    sget-object p3, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->numPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p3, p4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/regex/Matcher;->find()Z

    move-result p3

    if-eqz p3, :cond_7

    .line 28
    invoke-direct {p0, p4, p1, p2}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->createNumberFormat(Ljava/lang/String;D)Ljava/text/Format;

    move-result-object p1

    return-object p1

    .line 29
    :cond_7
    iget-boolean p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->emulateCsv:Z

    if-eqz p1, :cond_8

    .line 30
    new-instance p1, Lcom/intsig/office/fc/ss/usermodel/DataFormatter$ConstantStringFormat;

    invoke-direct {p0, p4}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->cleanFormatForNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter$ConstantStringFormat;-><init>(Ljava/lang/String;)V

    return-object p1

    :cond_8
    const/4 p1, 0x0

    return-object p1

    .line 31
    :cond_9
    :goto_3
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getDefaultFormat(D)Ljava/text/Format;

    move-result-object p1

    return-object p1
.end method

.method static createIntegerOnlyFormat(Ljava/lang/String;)Ljava/text/DecimalFormat;
    .locals 1

    .line 1
    new-instance v0, Ljava/text/DecimalFormat;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p0, 0x1

    .line 7
    invoke-virtual {v0, p0}, Ljava/text/DecimalFormat;->setParseIntegerOnly(Z)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private createNumberFormat(Ljava/lang/String;D)Ljava/text/Format;
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->cleanFormatForNumber(Ljava/lang/String;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :try_start_0
    new-instance v0, Ljava/text/DecimalFormat;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->decimalSymbols:Ljava/text/DecimalFormatSymbols;

    .line 8
    .line 9
    invoke-direct {v0, p1, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 10
    .line 11
    .line 12
    invoke-static {v0}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->setExcelStyleRoundingMode(Ljava/text/DecimalFormat;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    .line 14
    .line 15
    return-object v0

    .line 16
    :catch_0
    invoke-direct {p0, p2, p3}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getDefaultFormat(D)Ljava/text/Format;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private getDefaultFormat(D)Ljava/text/Format;
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->defaultNumFormat:Ljava/text/Format;

    if-eqz v0, :cond_0

    return-object v0

    .line 3
    :cond_0
    invoke-static {p1, p2}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->isWholeNumber(D)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 4
    iget-object p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->generalWholeNumFormat:Ljava/text/Format;

    return-object p1

    .line 5
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->generalDecimalNumFormat:Ljava/text/Format;

    return-object p1
.end method

.method private getFormat(DILjava/lang/String;)Ljava/text/Format;
    .locals 6

    const/16 v0, 0x3b

    .line 6
    invoke-virtual {p4, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 7
    invoke-virtual {p4, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    const/4 v3, -0x1

    const-wide/16 v4, 0x0

    if-eq v1, v3, :cond_3

    if-eq v1, v2, :cond_3

    add-int/lit8 v1, v1, 0x1

    .line 8
    invoke-virtual {p4, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    const/4 v1, 0x0

    if-ne v0, v2, :cond_1

    cmpl-double v0, p1, v4

    if-nez v0, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 9
    invoke-virtual {p4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p4, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    :cond_1
    cmpl-double v3, p1, v4

    if-nez v3, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 11
    invoke-virtual {p4, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    .line 12
    :cond_2
    invoke-virtual {p4, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p4

    .line 13
    :cond_3
    :goto_0
    iget-boolean v0, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->emulateCsv:Z

    if-eqz v0, :cond_4

    cmpl-double v0, p1, v4

    if-nez v0, :cond_4

    const-string v0, "#"

    invoke-virtual {p4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "0"

    invoke-virtual {p4, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ""

    .line 14
    invoke-virtual {p4, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 15
    :cond_4
    iget-object v0, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->formats:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/Format;

    if-eqz v0, :cond_5

    return-object v0

    :cond_5
    const-string v0, "General"

    .line 16
    invoke-virtual {v0, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "@"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_1

    .line 17
    :cond_6
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->createFormat(DILjava/lang/String;)Ljava/text/Format;

    move-result-object p1

    .line 18
    iget-object p2, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->formats:Ljava/util/Map;

    invoke-interface {p2, p4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1

    .line 19
    :cond_7
    :goto_1
    invoke-static {p1, p2}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->isWholeNumber(D)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 20
    iget-object p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->generalWholeNumFormat:Ljava/text/Format;

    return-object p1

    .line 21
    :cond_8
    iget-object p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->generalDecimalNumFormat:Ljava/text/Format;

    return-object p1
.end method

.method private getFormat(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/text/Format;
    .locals 5

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellStyle()Lcom/intsig/office/fc/ss/usermodel/ICellStyle;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 2
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellStyle()Lcom/intsig/office/fc/ss/usermodel/ICellStyle;

    move-result-object v0

    invoke-interface {v0}, Lcom/intsig/office/fc/ss/usermodel/ICellStyle;->getDataFormat()S

    move-result v0

    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellStyle()Lcom/intsig/office/fc/ss/usermodel/ICellStyle;

    move-result-object v2

    invoke-interface {v2}, Lcom/intsig/office/fc/ss/usermodel/ICellStyle;->getDataFormatString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 4
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 5
    :cond_1
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getNumericCellValue()D

    move-result-wide v3

    invoke-direct {p0, v3, v4, v0, v2}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getFormat(DILjava/lang/String;)Ljava/text/Format;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    return-object v1
.end method

.method private getFormattedDateString(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/lang/String;
    .locals 4

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getFormat(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/text/Format;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, Lcom/intsig/office/fc/ss/usermodel/ExcelStyleDateFormatter;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    move-object v1, v0

    .line 10
    check-cast v1, Lcom/intsig/office/fc/ss/usermodel/ExcelStyleDateFormatter;

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getNumericCellValue()D

    .line 13
    .line 14
    .line 15
    move-result-wide v2

    .line 16
    invoke-virtual {v1, v2, v3}, Lcom/intsig/office/fc/ss/usermodel/ExcelStyleDateFormatter;->setDateToBeFormatted(D)V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getDateCellValue()Ljava/util/Date;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->performDateFormatting(Ljava/util/Date;Ljava/text/Format;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    return-object p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private getFormattedNumberString(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getFormat(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/text/Format;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getNumericCellValue()D

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1

    .line 16
    :cond_0
    new-instance p1, Ljava/lang/Double;

    .line 17
    .line 18
    invoke-direct {p1, v1, v2}, Ljava/lang/Double;-><init>(D)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p1}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    return-object p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static isWholeNumber(D)Z
    .locals 3

    .line 1
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    cmpl-double v2, p0, v0

    .line 6
    .line 7
    if-nez v2, :cond_0

    .line 8
    .line 9
    const/4 p0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p0, 0x0

    .line 12
    :goto_0
    return p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private performDateFormatting(Ljava/util/Date;Ljava/text/Format;)Ljava/lang/String;
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    invoke-virtual {p2, p1}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1

    .line 8
    :cond_0
    invoke-virtual {p1}, Ljava/util/Date;->toString()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static setExcelStyleRoundingMode(Ljava/text/DecimalFormat;)V
    .locals 1

    .line 1
    sget-object v0, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-static {p0, v0}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->setExcelStyleRoundingMode(Ljava/text/DecimalFormat;Ljava/math/RoundingMode;)V

    return-void
.end method

.method public static setExcelStyleRoundingMode(Ljava/text/DecimalFormat;Ljava/math/RoundingMode;)V
    .locals 7

    const-string v0, "Unable to set rounding mode"

    .line 2
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "setRoundingMode"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Ljava/math/RoundingMode;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v6

    .line 3
    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception p0

    .line 4
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    move-exception p0

    .line 5
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_2
    :goto_0
    return-void
.end method


# virtual methods
.method public addFormat(Ljava/lang/String;Ljava/text/Format;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->formats:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public createFormat(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/text/Format;
    .locals 4

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellStyle()Lcom/intsig/office/fc/ss/usermodel/ICellStyle;

    move-result-object v0

    invoke-interface {v0}, Lcom/intsig/office/fc/ss/usermodel/ICellStyle;->getDataFormat()S

    move-result v0

    .line 2
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellStyle()Lcom/intsig/office/fc/ss/usermodel/ICellStyle;

    move-result-object v1

    invoke-interface {v1}, Lcom/intsig/office/fc/ss/usermodel/ICellStyle;->getDataFormatString()Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getNumericCellValue()D

    move-result-wide v2

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->createFormat(DILjava/lang/String;)Ljava/text/Format;

    move-result-object p1

    return-object p1
.end method

.method public formatCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->formatCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formatCellValue(Lcom/intsig/office/fc/ss/usermodel/ICell;Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;)Ljava/lang/String;
    .locals 3

    const-string v0, ""

    if-nez p1, :cond_0

    return-object v0

    .line 2
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    if-nez p2, :cond_1

    .line 3
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellFormula()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 4
    :cond_1
    invoke-interface {p2, p1}, Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;->evaluateFormulaCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)I

    move-result v1

    :cond_2
    if-eqz v1, :cond_6

    const/4 p2, 0x1

    if-eq v1, p2, :cond_5

    const/4 p2, 0x3

    if-eq v1, p2, :cond_4

    const/4 p2, 0x4

    if-ne v1, p2, :cond_3

    .line 5
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getBooleanCellValue()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 6
    :cond_3
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected celltype ("

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    return-object v0

    .line 7
    :cond_5
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getRichStringCellValue()Lcom/intsig/office/fc/ss/usermodel/RichTextString;

    move-result-object p1

    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/RichTextString;->getString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 8
    :cond_6
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getFormattedNumberString(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formatRawCellContents(DILjava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move-object v4, p4

    .line 1
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->formatRawCellContents(DILjava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formatRawCellContents(DILjava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .line 2
    invoke-static {p3, p4}, Lcom/intsig/office/ss/util/DateUtil;->isADateFormat(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3
    invoke-static {p1, p2}, Lcom/intsig/office/ss/util/DateUtil;->isValidExcelDate(D)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getFormat(DILjava/lang/String;)Ljava/text/Format;

    move-result-object p3

    .line 5
    instance-of p4, p3, Lcom/intsig/office/fc/ss/usermodel/ExcelStyleDateFormatter;

    if-eqz p4, :cond_0

    .line 6
    move-object p4, p3

    check-cast p4, Lcom/intsig/office/fc/ss/usermodel/ExcelStyleDateFormatter;

    invoke-virtual {p4, p1, p2}, Lcom/intsig/office/fc/ss/usermodel/ExcelStyleDateFormatter;->setDateToBeFormatted(D)V

    .line 7
    :cond_0
    invoke-static {p1, p2, p5}, Lcom/intsig/office/ss/util/DateUtil;->getJavaDate(DZ)Ljava/util/Date;

    move-result-object p1

    .line 8
    invoke-direct {p0, p1, p3}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->performDateFormatting(Ljava/util/Date;Ljava/text/Format;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 9
    :cond_1
    iget-boolean p5, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->emulateCsv:Z

    if-eqz p5, :cond_2

    .line 10
    sget-object p1, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->invalidDateTimeString:Ljava/lang/String;

    return-object p1

    .line 11
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getFormat(DILjava/lang/String;)Ljava/text/Format;

    move-result-object p3

    if-nez p3, :cond_3

    .line 12
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 13
    :cond_3
    new-instance p4, Ljava/lang/Double;

    invoke-direct {p4, p1, p2}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {p3, p4}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "E"

    .line 14
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_4

    const-string p3, "E-"

    invoke-virtual {p1, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_4

    const-string p3, "E+"

    .line 15
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_4
    return-object p1
.end method

.method public getDefaultFormat(Lcom/intsig/office/fc/ss/usermodel/ICell;)Ljava/text/Format;
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getNumericCellValue()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->getDefaultFormat(D)Ljava/text/Format;

    move-result-object p1

    return-object p1
.end method

.method public setDefaultNumberFormat(Ljava/text/Format;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->formats:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_2

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Ljava/util/Map$Entry;

    .line 22
    .line 23
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    iget-object v3, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->generalDecimalNumFormat:Ljava/text/Format;

    .line 28
    .line 29
    if-eq v2, v3, :cond_1

    .line 30
    .line 31
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    iget-object v3, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->generalWholeNumFormat:Ljava/text/Format;

    .line 36
    .line 37
    if-ne v2, v3, :cond_0

    .line 38
    .line 39
    :cond_1
    invoke-interface {v1, p1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    iput-object p1, p0, Lcom/intsig/office/fc/ss/usermodel/DataFormatter;->defaultNumFormat:Ljava/text/Format;

    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

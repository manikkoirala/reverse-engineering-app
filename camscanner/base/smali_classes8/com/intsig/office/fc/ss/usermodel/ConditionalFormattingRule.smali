.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
.super Ljava/lang/Object;
.source "ConditionalFormattingRule.java"


# static fields
.field public static final CONDITION_TYPE_CELL_VALUE_IS:B = 0x1t

.field public static final CONDITION_TYPE_FORMULA:B = 0x2t


# virtual methods
.method public abstract createBorderFormatting()Lcom/intsig/office/fc/ss/usermodel/BorderFormatting;
.end method

.method public abstract createFontFormatting()Lcom/intsig/office/fc/ss/usermodel/FontFormatting;
.end method

.method public abstract createPatternFormatting()Lcom/intsig/office/fc/ss/usermodel/PatternFormatting;
.end method

.method public abstract getBorderFormatting()Lcom/intsig/office/fc/ss/usermodel/BorderFormatting;
.end method

.method public abstract getComparisonOperation()B
.end method

.method public abstract getConditionType()B
.end method

.method public abstract getFontFormatting()Lcom/intsig/office/fc/ss/usermodel/FontFormatting;
.end method

.method public abstract getFormula1()Ljava/lang/String;
.end method

.method public abstract getFormula2()Ljava/lang/String;
.end method

.method public abstract getPatternFormatting()Lcom/intsig/office/fc/ss/usermodel/PatternFormatting;
.end method

.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/IHyperlink;
.super Ljava/lang/Object;
.source "IHyperlink.java"

# interfaces
.implements Lcom/intsig/office/fc/usermodel/Hyperlink;


# virtual methods
.method public abstract getFirstColumn()I
.end method

.method public abstract getFirstRow()I
.end method

.method public abstract getLastColumn()I
.end method

.method public abstract getLastRow()I
.end method

.method public abstract setFirstColumn(I)V
.end method

.method public abstract setFirstRow(I)V
.end method

.method public abstract setLastColumn(I)V
.end method

.method public abstract setLastRow(I)V
.end method

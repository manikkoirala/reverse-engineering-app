.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/IRow;
.super Ljava/lang/Object;
.source "IRow.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/intsig/office/fc/ss/usermodel/ICell;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATE_NULL_AS_BLANK:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

.field public static final RETURN_BLANK_AS_NULL:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

.field public static final RETURN_NULL_AND_BLANK:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;-><init>(Lcom/intsig/office/fc/ss/usermodel/〇080;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/IRow;->RETURN_NULL_AND_BLANK:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 10
    .line 11
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;-><init>(Lcom/intsig/office/fc/ss/usermodel/〇080;)V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/IRow;->RETURN_BLANK_AS_NULL:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 15
    .line 16
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 17
    .line 18
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;-><init>(Lcom/intsig/office/fc/ss/usermodel/〇080;)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/IRow;->CREATE_NULL_AS_BLANK:Lcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public abstract cellIterator()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/intsig/office/fc/ss/usermodel/ICell;",
            ">;"
        }
    .end annotation
.end method

.method public abstract createCell(I)Lcom/intsig/office/fc/ss/usermodel/ICell;
.end method

.method public abstract createCell(II)Lcom/intsig/office/fc/ss/usermodel/ICell;
.end method

.method public abstract getCell(I)Lcom/intsig/office/fc/ss/usermodel/ICell;
.end method

.method public abstract getCell(ILcom/intsig/office/fc/ss/usermodel/IRow$MissingCellPolicy;)Lcom/intsig/office/fc/ss/usermodel/ICell;
.end method

.method public abstract getFirstCellNum()S
.end method

.method public abstract getHeight()S
.end method

.method public abstract getHeightInPoints()F
.end method

.method public abstract getLastCellNum()S
.end method

.method public abstract getPhysicalNumberOfCells()I
.end method

.method public abstract getRowNum()I
.end method

.method public abstract getRowStyle()Lcom/intsig/office/fc/ss/usermodel/ICellStyle;
.end method

.method public abstract getSheet()Lcom/intsig/office/fc/ss/usermodel/Sheet;
.end method

.method public abstract getZeroHeight()Z
.end method

.method public abstract isFormatted()Z
.end method

.method public abstract removeCell(Lcom/intsig/office/fc/ss/usermodel/ICell;)V
.end method

.method public abstract setHeight(S)V
.end method

.method public abstract setHeightInPoints(F)V
.end method

.method public abstract setRowNum(I)V
.end method

.method public abstract setRowStyle(Lcom/intsig/office/fc/ss/usermodel/ICellStyle;)V
.end method

.method public abstract setZeroHeight(Z)V
.end method

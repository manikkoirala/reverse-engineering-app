.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/charts/ScatterChartData;
.super Ljava/lang/Object;
.source "ScatterChartData.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/charts/ChartData;


# virtual methods
.method public abstract addSerie(Lcom/intsig/office/fc/ss/util/DataMarker;Lcom/intsig/office/fc/ss/util/DataMarker;)Lcom/intsig/office/fc/ss/usermodel/charts/ScatterChartSerie;
.end method

.method public abstract getSeries()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/office/fc/ss/usermodel/charts/ScatterChartSerie;",
            ">;"
        }
    .end annotation
.end method

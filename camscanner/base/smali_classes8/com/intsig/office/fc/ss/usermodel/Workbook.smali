.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/Workbook;
.super Ljava/lang/Object;
.source "Workbook.java"


# static fields
.field public static final PICTURE_TYPE_DIB:I = 0x7

.field public static final PICTURE_TYPE_EMF:I = 0x2

.field public static final PICTURE_TYPE_JPEG:I = 0x5

.field public static final PICTURE_TYPE_PICT:I = 0x4

.field public static final PICTURE_TYPE_PNG:I = 0x6

.field public static final PICTURE_TYPE_WMF:I = 0x3

.field public static final SHEET_STATE_HIDDEN:I = 0x1

.field public static final SHEET_STATE_VERY_HIDDEN:I = 0x2

.field public static final SHEET_STATE_VISIBLE:I


# virtual methods
.method public abstract getNumberOfSheets()I
.end method

.method public abstract getSheetAt(I)Lcom/intsig/office/fc/ss/usermodel/Sheet;
.end method

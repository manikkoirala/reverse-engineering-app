.class final Lcom/intsig/office/fc/ss/util/ExpandedDouble;
.super Ljava/lang/Object;
.source "ExpandedDouble.java"


# static fields
.field private static final O8:Ljava/math/BigInteger;

.field private static final 〇o〇:Ljava/math/BigInteger;


# instance fields
.field private final 〇080:Ljava/math/BigInteger;

.field private final 〇o00〇〇Oo:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const-wide v0, 0xfffffffffffffL

    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    sput-object v0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o〇:Ljava/math/BigInteger;

    .line 11
    .line 12
    const-wide/high16 v0, 0x10000000000000L

    .line 13
    .line 14
    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    sput-object v0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->O8:Ljava/math/BigInteger;

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>(J)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x34

    shr-long v0, p1, v0

    long-to-int v1, v0

    if-nez v1, :cond_0

    .line 2
    invoke-static {p1, p2}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object p1

    sget-object p2, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o〇:Ljava/math/BigInteger;

    invoke-virtual {p1, p2}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    .line 3
    invoke-virtual {p1}, Ljava/math/BigInteger;->bitLength()I

    move-result p2

    rsub-int/lit8 p2, p2, 0x40

    .line 4
    invoke-virtual {p1, p2}, Ljava/math/BigInteger;->shiftLeft(I)Ljava/math/BigInteger;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇080:Ljava/math/BigInteger;

    and-int/lit16 p1, v1, 0x7ff

    add-int/lit16 p1, p1, -0x3ff

    sub-int/2addr p1, p2

    .line 5
    iput p1, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o00〇〇Oo:I

    goto :goto_0

    .line 6
    :cond_0
    invoke-static {p1, p2}, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o〇(J)Ljava/math/BigInteger;

    move-result-object p1

    .line 7
    iput-object p1, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇080:Ljava/math/BigInteger;

    and-int/lit16 p1, v1, 0x7ff

    add-int/lit16 p1, p1, -0x3ff

    .line 8
    iput p1, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o00〇〇Oo:I

    :goto_0
    return-void
.end method

.method constructor <init>(Ljava/math/BigInteger;I)V
    .locals 2

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    invoke-virtual {p1}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    .line 11
    iput-object p1, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇080:Ljava/math/BigInteger;

    .line 12
    iput p2, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o00〇〇Oo:I

    return-void

    .line 13
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "bad bit length"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static 〇080(JI)Lcom/intsig/office/fc/ss/util/ExpandedDouble;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;

    .line 2
    .line 3
    invoke-static {p0, p1}, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o〇(J)Ljava/math/BigInteger;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-direct {v0, p0, p2}, Lcom/intsig/office/fc/ss/util/ExpandedDouble;-><init>(Ljava/math/BigInteger;I)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static 〇o〇(J)Ljava/math/BigInteger;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    sget-object p1, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o〇:Ljava/math/BigInteger;

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Ljava/math/BigInteger;->and(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    sget-object p1, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->O8:Ljava/math/BigInteger;

    .line 12
    .line 13
    invoke-virtual {p0, p1}, Ljava/math/BigInteger;->or(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    const/16 p1, 0xb

    .line 18
    .line 19
    invoke-virtual {p0, p1}, Ljava/math/BigInteger;->shiftLeft(I)Ljava/math/BigInteger;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    return-object p0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public O8()Lcom/intsig/office/fc/ss/util/NormalisedDecimal;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇080:Ljava/math/BigInteger;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o00〇〇Oo:I

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/office/fc/ss/util/NormalisedDecimal;->〇o00〇〇Oo(Ljava/math/BigInteger;I)Lcom/intsig/office/fc/ss/util/NormalisedDecimal;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public 〇o00〇〇Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ss/util/ExpandedDouble;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

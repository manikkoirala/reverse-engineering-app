.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/charts/ManualLayout;
.super Ljava/lang/Object;
.source "ManualLayout.java"


# virtual methods
.method public abstract getHeightMode()Lcom/intsig/office/fc/ss/usermodel/charts/LayoutMode;
.end method

.method public abstract getHeightRatio()D
.end method

.method public abstract getTarget()Lcom/intsig/office/fc/ss/usermodel/charts/LayoutTarget;
.end method

.method public abstract getWidthMode()Lcom/intsig/office/fc/ss/usermodel/charts/LayoutMode;
.end method

.method public abstract getWidthRatio()D
.end method

.method public abstract getX()D
.end method

.method public abstract getXMode()Lcom/intsig/office/fc/ss/usermodel/charts/LayoutMode;
.end method

.method public abstract getY()D
.end method

.method public abstract getYMode()Lcom/intsig/office/fc/ss/usermodel/charts/LayoutMode;
.end method

.method public abstract setHeightMode(Lcom/intsig/office/fc/ss/usermodel/charts/LayoutMode;)V
.end method

.method public abstract setHeightRatio(D)V
.end method

.method public abstract setTarget(Lcom/intsig/office/fc/ss/usermodel/charts/LayoutTarget;)V
.end method

.method public abstract setWidthMode(Lcom/intsig/office/fc/ss/usermodel/charts/LayoutMode;)V
.end method

.method public abstract setWidthRatio(D)V
.end method

.method public abstract setX(D)V
.end method

.method public abstract setXMode(Lcom/intsig/office/fc/ss/usermodel/charts/LayoutMode;)V
.end method

.method public abstract setY(D)V
.end method

.method public abstract setYMode(Lcom/intsig/office/fc/ss/usermodel/charts/LayoutMode;)V
.end method

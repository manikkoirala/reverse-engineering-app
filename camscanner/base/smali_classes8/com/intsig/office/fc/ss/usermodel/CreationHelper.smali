.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/CreationHelper;
.super Ljava/lang/Object;
.source "CreationHelper.java"


# virtual methods
.method public abstract createClientAnchor()Lcom/intsig/office/fc/hssf/usermodel/IClientAnchor;
.end method

.method public abstract createDataFormat()Lcom/intsig/office/fc/ss/usermodel/DataFormat;
.end method

.method public abstract createFormulaEvaluator()Lcom/intsig/office/fc/ss/usermodel/FormulaEvaluator;
.end method

.method public abstract createHyperlink(I)Lcom/intsig/office/fc/ss/usermodel/IHyperlink;
.end method

.method public abstract createRichTextString(Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/RichTextString;
.end method

.class Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;
.super Ljava/lang/Object;
.source "CellDateFormatter.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/format/CellFormatPart$PartHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/fc/ss/format/CellDateFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatePartHandler"
.end annotation


# instance fields
.field private O8:I

.field final synthetic Oo08:Lcom/intsig/office/fc/ss/format/CellDateFormatter;

.field private 〇080:I

.field private 〇o00〇〇Oo:I

.field private 〇o〇:I


# direct methods
.method private constructor <init>(Lcom/intsig/office/fc/ss/format/CellDateFormatter;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->Oo08:Lcom/intsig/office/fc/ss/format/CellDateFormatter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, -0x1

    .line 3
    iput p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 4
    iput p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇o〇:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/office/fc/ss/format/CellDateFormatter;Lcom/intsig/office/fc/ss/format/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;-><init>(Lcom/intsig/office/fc/ss/format/CellDateFormatter;)V

    return-void
.end method


# virtual methods
.method public 〇080(Ljava/util/regex/Matcher;Ljava/lang/String;Lcom/intsig/office/fc/ss/format/CellFormatType;Ljava/lang/StringBuffer;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 p3, 0x0

    .line 6
    invoke-virtual {p2, p3}, Ljava/lang/String;->charAt(I)C

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/16 v1, 0x6d

    .line 11
    .line 12
    const/4 v2, -0x1

    .line 13
    sparse-switch v0, :sswitch_data_0

    .line 14
    .line 15
    .line 16
    goto/16 :goto_2

    .line 17
    .line 18
    :sswitch_0
    iput v2, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 19
    .line 20
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    const/4 p3, 0x3

    .line 25
    if-ne p1, p3, :cond_0

    .line 26
    .line 27
    const-string p2, "yyyy"

    .line 28
    .line 29
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    return-object p1

    .line 34
    :sswitch_1
    iget p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 35
    .line 36
    if-ltz p1, :cond_2

    .line 37
    .line 38
    :goto_0
    iget p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇o00〇〇Oo:I

    .line 39
    .line 40
    if-ge p3, p1, :cond_1

    .line 41
    .line 42
    iget p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 43
    .line 44
    add-int/2addr p1, p3

    .line 45
    invoke-virtual {p4, p1, v1}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 46
    .line 47
    .line 48
    add-int/lit8 p3, p3, 0x1

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    iput v2, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 52
    .line 53
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    return-object p1

    .line 58
    :sswitch_2
    iput p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 59
    .line 60
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 61
    .line 62
    .line 63
    move-result p1

    .line 64
    iput p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇o00〇〇Oo:I

    .line 65
    .line 66
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    return-object p1

    .line 71
    :sswitch_3
    iput v2, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 72
    .line 73
    iput p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇o〇:I

    .line 74
    .line 75
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    iput p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->O8:I

    .line 80
    .line 81
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    return-object p1

    .line 86
    :sswitch_4
    iput v2, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 87
    .line 88
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 89
    .line 90
    .line 91
    move-result p1

    .line 92
    const/4 p3, 0x2

    .line 93
    if-gt p1, p3, :cond_3

    .line 94
    .line 95
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    return-object p1

    .line 100
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    const/16 p2, 0x64

    .line 105
    .line 106
    const/16 p3, 0x45

    .line 107
    .line 108
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    return-object p1

    .line 113
    :sswitch_5
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 114
    .line 115
    .line 116
    move-result p1

    .line 117
    const/4 p4, 0x1

    .line 118
    if-le p1, p4, :cond_7

    .line 119
    .line 120
    iput v2, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 121
    .line 122
    iget-object p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->Oo08:Lcom/intsig/office/fc/ss/format/CellDateFormatter;

    .line 123
    .line 124
    invoke-static {p1, p4}, Lcom/intsig/office/fc/ss/format/CellDateFormatter;->Oo08(Lcom/intsig/office/fc/ss/format/CellDateFormatter;Z)V

    .line 125
    .line 126
    .line 127
    iget-object p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->Oo08:Lcom/intsig/office/fc/ss/format/CellDateFormatter;

    .line 128
    .line 129
    invoke-virtual {p2, p4}, Ljava/lang/String;->charAt(I)C

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    if-ne v0, v1, :cond_4

    .line 138
    .line 139
    const/4 v0, 0x1

    .line 140
    goto :goto_1

    .line 141
    :cond_4
    const/4 v0, 0x0

    .line 142
    :goto_1
    invoke-static {p1, v0}, Lcom/intsig/office/fc/ss/format/CellDateFormatter;->o〇0(Lcom/intsig/office/fc/ss/format/CellDateFormatter;Z)V

    .line 143
    .line 144
    .line 145
    iget-object p1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->Oo08:Lcom/intsig/office/fc/ss/format/CellDateFormatter;

    .line 146
    .line 147
    invoke-static {p1}, Lcom/intsig/office/fc/ss/format/CellDateFormatter;->〇o00〇〇Oo(Lcom/intsig/office/fc/ss/format/CellDateFormatter;)Z

    .line 148
    .line 149
    .line 150
    move-result v0

    .line 151
    if-nez v0, :cond_5

    .line 152
    .line 153
    invoke-virtual {p2, p3}, Ljava/lang/String;->charAt(I)C

    .line 154
    .line 155
    .line 156
    move-result p2

    .line 157
    invoke-static {p2}, Ljava/lang/Character;->isUpperCase(C)Z

    .line 158
    .line 159
    .line 160
    move-result p2

    .line 161
    if-eqz p2, :cond_6

    .line 162
    .line 163
    :cond_5
    const/4 p3, 0x1

    .line 164
    :cond_6
    invoke-static {p1, p3}, Lcom/intsig/office/fc/ss/format/CellDateFormatter;->〇o〇(Lcom/intsig/office/fc/ss/format/CellDateFormatter;Z)V

    .line 165
    .line 166
    .line 167
    const-string p1, "a"

    .line 168
    .line 169
    return-object p1

    .line 170
    :sswitch_6
    iput v2, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇080:I

    .line 171
    .line 172
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 173
    .line 174
    .line 175
    move-result p1

    .line 176
    iget-object p3, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->Oo08:Lcom/intsig/office/fc/ss/format/CellDateFormatter;

    .line 177
    .line 178
    new-instance p4, Ljava/lang/StringBuilder;

    .line 179
    .line 180
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    .line 182
    .line 183
    const-string v0, "%0"

    .line 184
    .line 185
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    .line 187
    .line 188
    add-int/lit8 v0, p1, 0x2

    .line 189
    .line 190
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    const-string v0, "."

    .line 194
    .line 195
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    const-string p1, "f"

    .line 202
    .line 203
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object p1

    .line 210
    invoke-static {p3, p1}, Lcom/intsig/office/fc/ss/format/CellDateFormatter;->O8(Lcom/intsig/office/fc/ss/format/CellDateFormatter;Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    const/16 p1, 0x30

    .line 214
    .line 215
    const/16 p3, 0x53

    .line 216
    .line 217
    invoke-virtual {p2, p1, p3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object p1

    .line 221
    return-object p1

    .line 222
    :cond_7
    :goto_2
    const/4 p1, 0x0

    .line 223
    return-object p1

    .line 224
    nop

    .line 225
    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_6
        0x41 -> :sswitch_5
        0x44 -> :sswitch_4
        0x48 -> :sswitch_3
        0x4d -> :sswitch_2
        0x50 -> :sswitch_5
        0x53 -> :sswitch_1
        0x59 -> :sswitch_0
        0x61 -> :sswitch_5
        0x64 -> :sswitch_4
        0x68 -> :sswitch_3
        0x6d -> :sswitch_2
        0x70 -> :sswitch_5
        0x73 -> :sswitch_1
        0x79 -> :sswitch_0
    .end sparse-switch
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public 〇o00〇〇Oo(Ljava/lang/StringBuffer;)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇o〇:I

    .line 2
    .line 3
    if-ltz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->Oo08:Lcom/intsig/office/fc/ss/format/CellDateFormatter;

    .line 6
    .line 7
    invoke-static {v0}, Lcom/intsig/office/fc/ss/format/CellDateFormatter;->〇080(Lcom/intsig/office/fc/ss/format/CellDateFormatter;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    :goto_0
    iget v1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->O8:I

    .line 15
    .line 16
    if-ge v0, v1, :cond_0

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/office/fc/ss/format/CellDateFormatter$DatePartHandler;->〇o〇:I

    .line 19
    .line 20
    add-int/2addr v1, v0

    .line 21
    const/16 v2, 0x48

    .line 22
    .line 23
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 24
    .line 25
    .line 26
    add-int/lit8 v0, v0, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

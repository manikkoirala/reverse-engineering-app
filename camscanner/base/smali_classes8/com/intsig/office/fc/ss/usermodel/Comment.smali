.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/Comment;
.super Ljava/lang/Object;
.source "Comment.java"


# virtual methods
.method public abstract getAuthor()Ljava/lang/String;
.end method

.method public abstract getColumn()I
.end method

.method public abstract getRow()I
.end method

.method public abstract getString()Lcom/intsig/office/fc/ss/usermodel/RichTextString;
.end method

.method public abstract isVisible()Z
.end method

.method public abstract setAuthor(Ljava/lang/String;)V
.end method

.method public abstract setColumn(I)V
.end method

.method public abstract setRow(I)V
.end method

.method public abstract setString(Lcom/intsig/office/fc/ss/usermodel/RichTextString;)V
.end method

.method public abstract setVisible(Z)V
.end method

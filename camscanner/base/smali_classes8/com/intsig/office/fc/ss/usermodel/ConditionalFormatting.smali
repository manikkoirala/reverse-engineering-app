.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/ConditionalFormatting;
.super Ljava/lang/Object;
.source "ConditionalFormatting.java"


# virtual methods
.method public abstract addRule(Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)V
.end method

.method public abstract getFormattingRanges()[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
.end method

.method public abstract getNumberOfRules()I
.end method

.method public abstract getRule(I)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
.end method

.method public abstract setRule(ILcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)V
.end method

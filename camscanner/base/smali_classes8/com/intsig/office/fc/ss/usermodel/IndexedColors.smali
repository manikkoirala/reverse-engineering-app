.class public final enum Lcom/intsig/office/fc/ss/usermodel/IndexedColors;
.super Ljava/lang/Enum;
.source "IndexedColors.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/office/fc/ss/usermodel/IndexedColors;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum AQUA:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum AUTOMATIC:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum BLACK:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum BLUE_GREY:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum BRIGHT_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum BROWN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum CORAL:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum CORNFLOWER_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum DARK_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum DARK_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum DARK_RED:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum DARK_TEAL:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum DARK_YELLOW:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum GOLD:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum GREY_25_PERCENT:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum GREY_40_PERCENT:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum GREY_50_PERCENT:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum GREY_80_PERCENT:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum INDIGO:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LAVENDER:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LEMON_CHIFFON:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_CORNFLOWER_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_ORANGE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_TURQUOISE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_YELLOW:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum LIME:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum MAROON:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum OLIVE_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum ORANGE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum ORCHID:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum PALE_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum PINK:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum PLUM:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum RED:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum ROSE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum ROYAL_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum SEA_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum SKY_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum TAN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum TEAL:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum TURQUOISE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum VIOLET:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum WHITE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

.field public static final enum YELLOW:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;


# instance fields
.field public final index:S


# direct methods
.method static constructor <clinit>()V
    .locals 51

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 2
    .line 3
    const-string v1, "BLACK"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/16 v3, 0x8

    .line 7
    .line 8
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 9
    .line 10
    .line 11
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->BLACK:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 12
    .line 13
    new-instance v1, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 14
    .line 15
    const-string v2, "WHITE"

    .line 16
    .line 17
    const/4 v4, 0x1

    .line 18
    const/16 v5, 0x9

    .line 19
    .line 20
    invoke-direct {v1, v2, v4, v5}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 21
    .line 22
    .line 23
    sput-object v1, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->WHITE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 26
    .line 27
    const-string v4, "RED"

    .line 28
    .line 29
    const/4 v6, 0x2

    .line 30
    const/16 v7, 0xa

    .line 31
    .line 32
    invoke-direct {v2, v4, v6, v7}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 33
    .line 34
    .line 35
    sput-object v2, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->RED:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 36
    .line 37
    new-instance v4, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 38
    .line 39
    const-string v6, "BRIGHT_GREEN"

    .line 40
    .line 41
    const/4 v8, 0x3

    .line 42
    const/16 v9, 0xb

    .line 43
    .line 44
    invoke-direct {v4, v6, v8, v9}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 45
    .line 46
    .line 47
    sput-object v4, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->BRIGHT_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 48
    .line 49
    new-instance v6, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 50
    .line 51
    const-string v8, "BLUE"

    .line 52
    .line 53
    const/4 v10, 0x4

    .line 54
    const/16 v11, 0xc

    .line 55
    .line 56
    invoke-direct {v6, v8, v10, v11}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 57
    .line 58
    .line 59
    sput-object v6, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 60
    .line 61
    new-instance v8, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 62
    .line 63
    const-string v10, "YELLOW"

    .line 64
    .line 65
    const/4 v12, 0x5

    .line 66
    const/16 v13, 0xd

    .line 67
    .line 68
    invoke-direct {v8, v10, v12, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 69
    .line 70
    .line 71
    sput-object v8, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->YELLOW:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 72
    .line 73
    new-instance v10, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 74
    .line 75
    const-string v12, "PINK"

    .line 76
    .line 77
    const/4 v14, 0x6

    .line 78
    const/16 v15, 0xe

    .line 79
    .line 80
    invoke-direct {v10, v12, v14, v15}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 81
    .line 82
    .line 83
    sput-object v10, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->PINK:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 84
    .line 85
    new-instance v12, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 86
    .line 87
    const-string v14, "TURQUOISE"

    .line 88
    .line 89
    const/4 v15, 0x7

    .line 90
    const/16 v13, 0xf

    .line 91
    .line 92
    invoke-direct {v12, v14, v15, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 93
    .line 94
    .line 95
    sput-object v12, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->TURQUOISE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 96
    .line 97
    new-instance v14, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 98
    .line 99
    const-string v15, "DARK_RED"

    .line 100
    .line 101
    const/16 v13, 0x10

    .line 102
    .line 103
    invoke-direct {v14, v15, v3, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 104
    .line 105
    .line 106
    sput-object v14, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->DARK_RED:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 107
    .line 108
    new-instance v15, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 109
    .line 110
    const-string v3, "GREEN"

    .line 111
    .line 112
    const/16 v13, 0x11

    .line 113
    .line 114
    invoke-direct {v15, v3, v5, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 115
    .line 116
    .line 117
    sput-object v15, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 118
    .line 119
    new-instance v3, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 120
    .line 121
    const-string v5, "DARK_BLUE"

    .line 122
    .line 123
    const/16 v13, 0x12

    .line 124
    .line 125
    invoke-direct {v3, v5, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 126
    .line 127
    .line 128
    sput-object v3, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->DARK_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 129
    .line 130
    new-instance v5, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 131
    .line 132
    const-string v7, "DARK_YELLOW"

    .line 133
    .line 134
    const/16 v13, 0x13

    .line 135
    .line 136
    invoke-direct {v5, v7, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 137
    .line 138
    .line 139
    sput-object v5, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->DARK_YELLOW:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 140
    .line 141
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 142
    .line 143
    const-string v9, "VIOLET"

    .line 144
    .line 145
    const/16 v13, 0x14

    .line 146
    .line 147
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 148
    .line 149
    .line 150
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->VIOLET:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 151
    .line 152
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 153
    .line 154
    const-string v11, "TEAL"

    .line 155
    .line 156
    const/16 v13, 0x15

    .line 157
    .line 158
    move-object/from16 v16, v7

    .line 159
    .line 160
    const/16 v7, 0xd

    .line 161
    .line 162
    invoke-direct {v9, v11, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 163
    .line 164
    .line 165
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->TEAL:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 166
    .line 167
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 168
    .line 169
    const-string v11, "GREY_25_PERCENT"

    .line 170
    .line 171
    const/16 v13, 0x16

    .line 172
    .line 173
    move-object/from16 v17, v9

    .line 174
    .line 175
    const/16 v9, 0xe

    .line 176
    .line 177
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 178
    .line 179
    .line 180
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->GREY_25_PERCENT:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 181
    .line 182
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 183
    .line 184
    const-string v11, "GREY_50_PERCENT"

    .line 185
    .line 186
    const/16 v13, 0x17

    .line 187
    .line 188
    move-object/from16 v18, v7

    .line 189
    .line 190
    const/16 v7, 0xf

    .line 191
    .line 192
    invoke-direct {v9, v11, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 193
    .line 194
    .line 195
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->GREY_50_PERCENT:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 196
    .line 197
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 198
    .line 199
    const-string v11, "CORNFLOWER_BLUE"

    .line 200
    .line 201
    const/16 v13, 0x18

    .line 202
    .line 203
    move-object/from16 v19, v9

    .line 204
    .line 205
    const/16 v9, 0x10

    .line 206
    .line 207
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 208
    .line 209
    .line 210
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->CORNFLOWER_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 211
    .line 212
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 213
    .line 214
    const-string v11, "MAROON"

    .line 215
    .line 216
    const/16 v13, 0x19

    .line 217
    .line 218
    move-object/from16 v20, v7

    .line 219
    .line 220
    const/16 v7, 0x11

    .line 221
    .line 222
    invoke-direct {v9, v11, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 223
    .line 224
    .line 225
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->MAROON:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 226
    .line 227
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 228
    .line 229
    const-string v11, "LEMON_CHIFFON"

    .line 230
    .line 231
    const/16 v13, 0x1a

    .line 232
    .line 233
    move-object/from16 v21, v9

    .line 234
    .line 235
    const/16 v9, 0x12

    .line 236
    .line 237
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 238
    .line 239
    .line 240
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LEMON_CHIFFON:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 241
    .line 242
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 243
    .line 244
    const-string v11, "ORCHID"

    .line 245
    .line 246
    const/16 v13, 0x1c

    .line 247
    .line 248
    move-object/from16 v22, v7

    .line 249
    .line 250
    const/16 v7, 0x13

    .line 251
    .line 252
    invoke-direct {v9, v11, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 253
    .line 254
    .line 255
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->ORCHID:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 256
    .line 257
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 258
    .line 259
    const-string v11, "CORAL"

    .line 260
    .line 261
    const/16 v13, 0x1d

    .line 262
    .line 263
    move-object/from16 v23, v9

    .line 264
    .line 265
    const/16 v9, 0x14

    .line 266
    .line 267
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 268
    .line 269
    .line 270
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->CORAL:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 271
    .line 272
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 273
    .line 274
    const-string v11, "ROYAL_BLUE"

    .line 275
    .line 276
    const/16 v13, 0x1e

    .line 277
    .line 278
    move-object/from16 v24, v7

    .line 279
    .line 280
    const/16 v7, 0x15

    .line 281
    .line 282
    invoke-direct {v9, v11, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 283
    .line 284
    .line 285
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->ROYAL_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 286
    .line 287
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 288
    .line 289
    const-string v11, "LIGHT_CORNFLOWER_BLUE"

    .line 290
    .line 291
    const/16 v13, 0x1f

    .line 292
    .line 293
    move-object/from16 v25, v9

    .line 294
    .line 295
    const/16 v9, 0x16

    .line 296
    .line 297
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 298
    .line 299
    .line 300
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LIGHT_CORNFLOWER_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 301
    .line 302
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 303
    .line 304
    const-string v11, "SKY_BLUE"

    .line 305
    .line 306
    const/16 v13, 0x28

    .line 307
    .line 308
    move-object/from16 v26, v7

    .line 309
    .line 310
    const/16 v7, 0x17

    .line 311
    .line 312
    invoke-direct {v9, v11, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 313
    .line 314
    .line 315
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->SKY_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 316
    .line 317
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 318
    .line 319
    const-string v11, "LIGHT_TURQUOISE"

    .line 320
    .line 321
    const/16 v13, 0x29

    .line 322
    .line 323
    move-object/from16 v27, v9

    .line 324
    .line 325
    const/16 v9, 0x18

    .line 326
    .line 327
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 328
    .line 329
    .line 330
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LIGHT_TURQUOISE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 331
    .line 332
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 333
    .line 334
    const-string v11, "LIGHT_GREEN"

    .line 335
    .line 336
    const/16 v13, 0x2a

    .line 337
    .line 338
    move-object/from16 v28, v7

    .line 339
    .line 340
    const/16 v7, 0x19

    .line 341
    .line 342
    invoke-direct {v9, v11, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 343
    .line 344
    .line 345
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LIGHT_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 346
    .line 347
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 348
    .line 349
    const-string v11, "LIGHT_YELLOW"

    .line 350
    .line 351
    const/16 v13, 0x2b

    .line 352
    .line 353
    move-object/from16 v29, v9

    .line 354
    .line 355
    const/16 v9, 0x1a

    .line 356
    .line 357
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 358
    .line 359
    .line 360
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LIGHT_YELLOW:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 361
    .line 362
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 363
    .line 364
    const/16 v11, 0x1b

    .line 365
    .line 366
    const/16 v13, 0x2c

    .line 367
    .line 368
    move-object/from16 v30, v7

    .line 369
    .line 370
    const-string v7, "PALE_BLUE"

    .line 371
    .line 372
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 373
    .line 374
    .line 375
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->PALE_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 376
    .line 377
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 378
    .line 379
    const-string v11, "ROSE"

    .line 380
    .line 381
    const/16 v13, 0x2d

    .line 382
    .line 383
    move-object/from16 v31, v9

    .line 384
    .line 385
    const/16 v9, 0x1c

    .line 386
    .line 387
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 388
    .line 389
    .line 390
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->ROSE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 391
    .line 392
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 393
    .line 394
    const-string v11, "LAVENDER"

    .line 395
    .line 396
    const/16 v13, 0x2e

    .line 397
    .line 398
    move-object/from16 v32, v7

    .line 399
    .line 400
    const/16 v7, 0x1d

    .line 401
    .line 402
    invoke-direct {v9, v11, v7, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 403
    .line 404
    .line 405
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LAVENDER:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 406
    .line 407
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 408
    .line 409
    const-string v11, "TAN"

    .line 410
    .line 411
    const/16 v13, 0x2f

    .line 412
    .line 413
    move-object/from16 v33, v9

    .line 414
    .line 415
    const/16 v9, 0x1e

    .line 416
    .line 417
    invoke-direct {v7, v11, v9, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 418
    .line 419
    .line 420
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->TAN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 421
    .line 422
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 423
    .line 424
    const/16 v11, 0x1f

    .line 425
    .line 426
    const/16 v13, 0x30

    .line 427
    .line 428
    move-object/from16 v34, v7

    .line 429
    .line 430
    const-string v7, "LIGHT_BLUE"

    .line 431
    .line 432
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 433
    .line 434
    .line 435
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LIGHT_BLUE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 436
    .line 437
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 438
    .line 439
    const/16 v11, 0x20

    .line 440
    .line 441
    const/16 v13, 0x31

    .line 442
    .line 443
    move-object/from16 v35, v9

    .line 444
    .line 445
    const-string v9, "AQUA"

    .line 446
    .line 447
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 448
    .line 449
    .line 450
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->AQUA:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 451
    .line 452
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 453
    .line 454
    const/16 v11, 0x21

    .line 455
    .line 456
    const/16 v13, 0x32

    .line 457
    .line 458
    move-object/from16 v36, v7

    .line 459
    .line 460
    const-string v7, "LIME"

    .line 461
    .line 462
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 463
    .line 464
    .line 465
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LIME:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 466
    .line 467
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 468
    .line 469
    const/16 v11, 0x22

    .line 470
    .line 471
    const/16 v13, 0x33

    .line 472
    .line 473
    move-object/from16 v37, v9

    .line 474
    .line 475
    const-string v9, "GOLD"

    .line 476
    .line 477
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 478
    .line 479
    .line 480
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->GOLD:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 481
    .line 482
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 483
    .line 484
    const/16 v11, 0x23

    .line 485
    .line 486
    const/16 v13, 0x34

    .line 487
    .line 488
    move-object/from16 v38, v7

    .line 489
    .line 490
    const-string v7, "LIGHT_ORANGE"

    .line 491
    .line 492
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 493
    .line 494
    .line 495
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->LIGHT_ORANGE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 496
    .line 497
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 498
    .line 499
    const/16 v11, 0x24

    .line 500
    .line 501
    const/16 v13, 0x35

    .line 502
    .line 503
    move-object/from16 v39, v9

    .line 504
    .line 505
    const-string v9, "ORANGE"

    .line 506
    .line 507
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 508
    .line 509
    .line 510
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->ORANGE:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 511
    .line 512
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 513
    .line 514
    const/16 v11, 0x25

    .line 515
    .line 516
    const/16 v13, 0x36

    .line 517
    .line 518
    move-object/from16 v40, v7

    .line 519
    .line 520
    const-string v7, "BLUE_GREY"

    .line 521
    .line 522
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 523
    .line 524
    .line 525
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->BLUE_GREY:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 526
    .line 527
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 528
    .line 529
    const/16 v11, 0x26

    .line 530
    .line 531
    const/16 v13, 0x37

    .line 532
    .line 533
    move-object/from16 v41, v9

    .line 534
    .line 535
    const-string v9, "GREY_40_PERCENT"

    .line 536
    .line 537
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 538
    .line 539
    .line 540
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->GREY_40_PERCENT:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 541
    .line 542
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 543
    .line 544
    const/16 v11, 0x27

    .line 545
    .line 546
    const/16 v13, 0x38

    .line 547
    .line 548
    move-object/from16 v42, v7

    .line 549
    .line 550
    const-string v7, "DARK_TEAL"

    .line 551
    .line 552
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 553
    .line 554
    .line 555
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->DARK_TEAL:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 556
    .line 557
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 558
    .line 559
    const/16 v11, 0x28

    .line 560
    .line 561
    const/16 v13, 0x39

    .line 562
    .line 563
    move-object/from16 v43, v9

    .line 564
    .line 565
    const-string v9, "SEA_GREEN"

    .line 566
    .line 567
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 568
    .line 569
    .line 570
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->SEA_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 571
    .line 572
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 573
    .line 574
    const/16 v11, 0x29

    .line 575
    .line 576
    const/16 v13, 0x3a

    .line 577
    .line 578
    move-object/from16 v44, v7

    .line 579
    .line 580
    const-string v7, "DARK_GREEN"

    .line 581
    .line 582
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 583
    .line 584
    .line 585
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->DARK_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 586
    .line 587
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 588
    .line 589
    const/16 v11, 0x2a

    .line 590
    .line 591
    const/16 v13, 0x3b

    .line 592
    .line 593
    move-object/from16 v45, v9

    .line 594
    .line 595
    const-string v9, "OLIVE_GREEN"

    .line 596
    .line 597
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 598
    .line 599
    .line 600
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->OLIVE_GREEN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 601
    .line 602
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 603
    .line 604
    const/16 v11, 0x2b

    .line 605
    .line 606
    const/16 v13, 0x3c

    .line 607
    .line 608
    move-object/from16 v46, v7

    .line 609
    .line 610
    const-string v7, "BROWN"

    .line 611
    .line 612
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 613
    .line 614
    .line 615
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->BROWN:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 616
    .line 617
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 618
    .line 619
    const/16 v11, 0x2c

    .line 620
    .line 621
    const/16 v13, 0x3d

    .line 622
    .line 623
    move-object/from16 v47, v9

    .line 624
    .line 625
    const-string v9, "PLUM"

    .line 626
    .line 627
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 628
    .line 629
    .line 630
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->PLUM:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 631
    .line 632
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 633
    .line 634
    const/16 v11, 0x2d

    .line 635
    .line 636
    const/16 v13, 0x3e

    .line 637
    .line 638
    move-object/from16 v48, v7

    .line 639
    .line 640
    const-string v7, "INDIGO"

    .line 641
    .line 642
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 643
    .line 644
    .line 645
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->INDIGO:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 646
    .line 647
    new-instance v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 648
    .line 649
    const/16 v11, 0x2e

    .line 650
    .line 651
    const/16 v13, 0x3f

    .line 652
    .line 653
    move-object/from16 v49, v9

    .line 654
    .line 655
    const-string v9, "GREY_80_PERCENT"

    .line 656
    .line 657
    invoke-direct {v7, v9, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 658
    .line 659
    .line 660
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->GREY_80_PERCENT:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 661
    .line 662
    new-instance v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 663
    .line 664
    const/16 v11, 0x2f

    .line 665
    .line 666
    const/16 v13, 0x40

    .line 667
    .line 668
    move-object/from16 v50, v7

    .line 669
    .line 670
    const-string v7, "AUTOMATIC"

    .line 671
    .line 672
    invoke-direct {v9, v7, v11, v13}, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    .line 673
    .line 674
    .line 675
    sput-object v9, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->AUTOMATIC:Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 676
    .line 677
    const/16 v7, 0x30

    .line 678
    .line 679
    new-array v7, v7, [Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 680
    .line 681
    const/4 v11, 0x0

    .line 682
    aput-object v0, v7, v11

    .line 683
    .line 684
    const/4 v0, 0x1

    .line 685
    aput-object v1, v7, v0

    .line 686
    .line 687
    const/4 v0, 0x2

    .line 688
    aput-object v2, v7, v0

    .line 689
    .line 690
    const/4 v0, 0x3

    .line 691
    aput-object v4, v7, v0

    .line 692
    .line 693
    const/4 v0, 0x4

    .line 694
    aput-object v6, v7, v0

    .line 695
    .line 696
    const/4 v0, 0x5

    .line 697
    aput-object v8, v7, v0

    .line 698
    .line 699
    const/4 v0, 0x6

    .line 700
    aput-object v10, v7, v0

    .line 701
    .line 702
    const/4 v0, 0x7

    .line 703
    aput-object v12, v7, v0

    .line 704
    .line 705
    const/16 v0, 0x8

    .line 706
    .line 707
    aput-object v14, v7, v0

    .line 708
    .line 709
    const/16 v0, 0x9

    .line 710
    .line 711
    aput-object v15, v7, v0

    .line 712
    .line 713
    const/16 v0, 0xa

    .line 714
    .line 715
    aput-object v3, v7, v0

    .line 716
    .line 717
    const/16 v0, 0xb

    .line 718
    .line 719
    aput-object v5, v7, v0

    .line 720
    .line 721
    const/16 v0, 0xc

    .line 722
    .line 723
    aput-object v16, v7, v0

    .line 724
    .line 725
    const/16 v0, 0xd

    .line 726
    .line 727
    aput-object v17, v7, v0

    .line 728
    .line 729
    const/16 v0, 0xe

    .line 730
    .line 731
    aput-object v18, v7, v0

    .line 732
    .line 733
    const/16 v0, 0xf

    .line 734
    .line 735
    aput-object v19, v7, v0

    .line 736
    .line 737
    const/16 v0, 0x10

    .line 738
    .line 739
    aput-object v20, v7, v0

    .line 740
    .line 741
    const/16 v0, 0x11

    .line 742
    .line 743
    aput-object v21, v7, v0

    .line 744
    .line 745
    const/16 v0, 0x12

    .line 746
    .line 747
    aput-object v22, v7, v0

    .line 748
    .line 749
    const/16 v0, 0x13

    .line 750
    .line 751
    aput-object v23, v7, v0

    .line 752
    .line 753
    const/16 v0, 0x14

    .line 754
    .line 755
    aput-object v24, v7, v0

    .line 756
    .line 757
    const/16 v0, 0x15

    .line 758
    .line 759
    aput-object v25, v7, v0

    .line 760
    .line 761
    const/16 v0, 0x16

    .line 762
    .line 763
    aput-object v26, v7, v0

    .line 764
    .line 765
    const/16 v0, 0x17

    .line 766
    .line 767
    aput-object v27, v7, v0

    .line 768
    .line 769
    const/16 v0, 0x18

    .line 770
    .line 771
    aput-object v28, v7, v0

    .line 772
    .line 773
    const/16 v0, 0x19

    .line 774
    .line 775
    aput-object v29, v7, v0

    .line 776
    .line 777
    const/16 v0, 0x1a

    .line 778
    .line 779
    aput-object v30, v7, v0

    .line 780
    .line 781
    const/16 v0, 0x1b

    .line 782
    .line 783
    aput-object v31, v7, v0

    .line 784
    .line 785
    const/16 v0, 0x1c

    .line 786
    .line 787
    aput-object v32, v7, v0

    .line 788
    .line 789
    const/16 v0, 0x1d

    .line 790
    .line 791
    aput-object v33, v7, v0

    .line 792
    .line 793
    const/16 v0, 0x1e

    .line 794
    .line 795
    aput-object v34, v7, v0

    .line 796
    .line 797
    const/16 v0, 0x1f

    .line 798
    .line 799
    aput-object v35, v7, v0

    .line 800
    .line 801
    const/16 v0, 0x20

    .line 802
    .line 803
    aput-object v36, v7, v0

    .line 804
    .line 805
    const/16 v0, 0x21

    .line 806
    .line 807
    aput-object v37, v7, v0

    .line 808
    .line 809
    const/16 v0, 0x22

    .line 810
    .line 811
    aput-object v38, v7, v0

    .line 812
    .line 813
    const/16 v0, 0x23

    .line 814
    .line 815
    aput-object v39, v7, v0

    .line 816
    .line 817
    const/16 v0, 0x24

    .line 818
    .line 819
    aput-object v40, v7, v0

    .line 820
    .line 821
    const/16 v0, 0x25

    .line 822
    .line 823
    aput-object v41, v7, v0

    .line 824
    .line 825
    const/16 v0, 0x26

    .line 826
    .line 827
    aput-object v42, v7, v0

    .line 828
    .line 829
    const/16 v0, 0x27

    .line 830
    .line 831
    aput-object v43, v7, v0

    .line 832
    .line 833
    const/16 v0, 0x28

    .line 834
    .line 835
    aput-object v44, v7, v0

    .line 836
    .line 837
    const/16 v0, 0x29

    .line 838
    .line 839
    aput-object v45, v7, v0

    .line 840
    .line 841
    const/16 v0, 0x2a

    .line 842
    .line 843
    aput-object v46, v7, v0

    .line 844
    .line 845
    const/16 v0, 0x2b

    .line 846
    .line 847
    aput-object v47, v7, v0

    .line 848
    .line 849
    const/16 v0, 0x2c

    .line 850
    .line 851
    aput-object v48, v7, v0

    .line 852
    .line 853
    const/16 v0, 0x2d

    .line 854
    .line 855
    aput-object v49, v7, v0

    .line 856
    .line 857
    const/16 v0, 0x2e

    .line 858
    .line 859
    aput-object v50, v7, v0

    .line 860
    .line 861
    const/16 v0, 0x2f

    .line 862
    .line 863
    aput-object v9, v7, v0

    .line 864
    .line 865
    sput-object v7, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->$VALUES:[Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 866
    .line 867
    return-void
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    int-to-short p1, p3

    .line 5
    iput-short p1, p0, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->index:S

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/IndexedColors;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static values()[Lcom/intsig/office/fc/ss/usermodel/IndexedColors;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->$VALUES:[Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/office/fc/ss/usermodel/IndexedColors;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getIndex()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/ss/usermodel/IndexedColors;->index:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

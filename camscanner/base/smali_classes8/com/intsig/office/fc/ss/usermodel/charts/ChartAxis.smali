.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/charts/ChartAxis;
.super Ljava/lang/Object;
.source "ChartAxis.java"


# virtual methods
.method public abstract crossAxis(Lcom/intsig/office/fc/ss/usermodel/charts/ChartAxis;)V
.end method

.method public abstract getCrosses()Lcom/intsig/office/fc/ss/usermodel/charts/AxisCrosses;
.end method

.method public abstract getId()J
.end method

.method public abstract getLogBase()D
.end method

.method public abstract getMaximum()D
.end method

.method public abstract getMinimum()D
.end method

.method public abstract getNumberFormat()Ljava/lang/String;
.end method

.method public abstract getOrientation()Lcom/intsig/office/fc/ss/usermodel/charts/AxisOrientation;
.end method

.method public abstract getPosition()Lcom/intsig/office/fc/ss/usermodel/charts/AxisPosition;
.end method

.method public abstract isSetLogBase()Z
.end method

.method public abstract isSetMaximum()Z
.end method

.method public abstract isSetMinimum()Z
.end method

.method public abstract setCrosses(Lcom/intsig/office/fc/ss/usermodel/charts/AxisCrosses;)V
.end method

.method public abstract setLogBase(D)V
.end method

.method public abstract setMaximum(D)V
.end method

.method public abstract setMinimum(D)V
.end method

.method public abstract setNumberFormat(Ljava/lang/String;)V
.end method

.method public abstract setOrientation(Lcom/intsig/office/fc/ss/usermodel/charts/AxisOrientation;)V
.end method

.method public abstract setPosition(Lcom/intsig/office/fc/ss/usermodel/charts/AxisPosition;)V
.end method

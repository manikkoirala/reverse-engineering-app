.class public abstract enum Lcom/intsig/office/fc/ss/format/CellFormatType;
.super Ljava/lang/Enum;
.source "CellFormatType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/office/fc/ss/format/CellFormatType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/office/fc/ss/format/CellFormatType;

.field public static final enum DATE:Lcom/intsig/office/fc/ss/format/CellFormatType;

.field public static final enum ELAPSED:Lcom/intsig/office/fc/ss/format/CellFormatType;

.field public static final enum GENERAL:Lcom/intsig/office/fc/ss/format/CellFormatType;

.field public static final enum NUMBER:Lcom/intsig/office/fc/ss/format/CellFormatType;

.field public static final enum TEXT:Lcom/intsig/office/fc/ss/format/CellFormatType;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/format/CellFormatType$1;

    .line 2
    .line 3
    const-string v1, "GENERAL"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x0

    .line 7
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/ss/format/CellFormatType$1;-><init>(Ljava/lang/String;ILcom/intsig/office/fc/ss/format/O8;)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/fc/ss/format/CellFormatType;->GENERAL:Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/office/fc/ss/format/CellFormatType$2;

    .line 13
    .line 14
    const-string v4, "NUMBER"

    .line 15
    .line 16
    const/4 v5, 0x1

    .line 17
    invoke-direct {v1, v4, v5, v3}, Lcom/intsig/office/fc/ss/format/CellFormatType$2;-><init>(Ljava/lang/String;ILcom/intsig/office/fc/ss/format/Oo08;)V

    .line 18
    .line 19
    .line 20
    sput-object v1, Lcom/intsig/office/fc/ss/format/CellFormatType;->NUMBER:Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 21
    .line 22
    new-instance v4, Lcom/intsig/office/fc/ss/format/CellFormatType$3;

    .line 23
    .line 24
    const-string v6, "DATE"

    .line 25
    .line 26
    const/4 v7, 0x2

    .line 27
    invoke-direct {v4, v6, v7, v3}, Lcom/intsig/office/fc/ss/format/CellFormatType$3;-><init>(Ljava/lang/String;ILcom/intsig/office/fc/ss/format/o〇0;)V

    .line 28
    .line 29
    .line 30
    sput-object v4, Lcom/intsig/office/fc/ss/format/CellFormatType;->DATE:Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 31
    .line 32
    new-instance v6, Lcom/intsig/office/fc/ss/format/CellFormatType$4;

    .line 33
    .line 34
    const-string v8, "ELAPSED"

    .line 35
    .line 36
    const/4 v9, 0x3

    .line 37
    invoke-direct {v6, v8, v9, v3}, Lcom/intsig/office/fc/ss/format/CellFormatType$4;-><init>(Ljava/lang/String;ILcom/intsig/office/fc/ss/format/〇〇888;)V

    .line 38
    .line 39
    .line 40
    sput-object v6, Lcom/intsig/office/fc/ss/format/CellFormatType;->ELAPSED:Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 41
    .line 42
    new-instance v8, Lcom/intsig/office/fc/ss/format/CellFormatType$5;

    .line 43
    .line 44
    const-string v10, "TEXT"

    .line 45
    .line 46
    const/4 v11, 0x4

    .line 47
    invoke-direct {v8, v10, v11, v3}, Lcom/intsig/office/fc/ss/format/CellFormatType$5;-><init>(Ljava/lang/String;ILcom/intsig/office/fc/ss/format/oO80;)V

    .line 48
    .line 49
    .line 50
    sput-object v8, Lcom/intsig/office/fc/ss/format/CellFormatType;->TEXT:Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 51
    .line 52
    const/4 v3, 0x5

    .line 53
    new-array v3, v3, [Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 54
    .line 55
    aput-object v0, v3, v2

    .line 56
    .line 57
    aput-object v1, v3, v5

    .line 58
    .line 59
    aput-object v4, v3, v7

    .line 60
    .line 61
    aput-object v6, v3, v9

    .line 62
    .line 63
    aput-object v8, v3, v11

    .line 64
    .line 65
    sput-object v3, Lcom/intsig/office/fc/ss/format/CellFormatType;->$VALUES:[Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/intsig/office/fc/ss/format/〇80〇808〇O;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/fc/ss/format/CellFormatType;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/office/fc/ss/format/CellFormatType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static values()[Lcom/intsig/office/fc/ss/format/CellFormatType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/format/CellFormatType;->$VALUES:[Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/office/fc/ss/format/CellFormatType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method abstract formatter(Ljava/lang/String;)Lcom/intsig/office/fc/ss/format/CellFormatter;
.end method

.method abstract isSpecial(C)Z
.end method

.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/SheetConditionalFormatting;
.super Ljava/lang/Object;
.source "SheetConditionalFormatting.java"


# virtual methods
.method public abstract addConditionalFormatting(Lcom/intsig/office/fc/ss/usermodel/ConditionalFormatting;)I
.end method

.method public abstract addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)I
.end method

.method public abstract addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)I
.end method

.method public abstract addConditionalFormatting([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;[Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;)I
.end method

.method public abstract createConditionalFormattingRule(BLjava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
.end method

.method public abstract createConditionalFormattingRule(BLjava/lang/String;Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
.end method

.method public abstract createConditionalFormattingRule(Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormattingRule;
.end method

.method public abstract getConditionalFormattingAt(I)Lcom/intsig/office/fc/ss/usermodel/ConditionalFormatting;
.end method

.method public abstract getNumConditionalFormattings()I
.end method

.method public abstract removeConditionalFormatting(I)V
.end method

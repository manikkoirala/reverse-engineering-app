.class public Lcom/intsig/office/fc/ss/format/CellNumberFormatter;
.super Lcom/intsig/office/fc/ss/format/CellFormatter;
.source "CellNumberFormatter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Fraction;,
        Lcom/intsig/office/fc/ss/format/CellNumberFormatter$NumPartHandler;,
        Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;,
        Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    }
.end annotation


# static fields
.field private static final SIMPLE_FLOAT:Lcom/intsig/office/fc/ss/format/CellFormatter;

.field private static final SIMPLE_INT:Lcom/intsig/office/fc/ss/format/CellFormatter;

.field static final SIMPLE_NUMBER:Lcom/intsig/office/fc/ss/format/CellFormatter;


# instance fields
.field private afterFractional:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

.field private afterInteger:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

.field private decimalFmt:Ljava/text/DecimalFormat;

.field private decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

.field private denominatorFmt:Ljava/lang/String;

.field private denominatorSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private final desc:Ljava/lang/String;

.field private exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

.field private exponentDigitSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private exponentSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private fractionalSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private improperFraction:Z

.field private integerCommas:Z

.field private integerSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private maxDenominator:I

.field private numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

.field private numeratorFmt:Ljava/lang/String;

.field private numeratorSpecials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field

.field private printfFmt:Ljava/lang/String;

.field private scale:D

.field private slash:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

.field private final specials:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$1;

    .line 2
    .line 3
    const-string v1, "General"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$1;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->SIMPLE_NUMBER:Lcom/intsig/office/fc/ss/format/CellFormatter;

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;

    .line 11
    .line 12
    const-string v1, "#"

    .line 13
    .line 14
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;-><init>(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    sput-object v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->SIMPLE_INT:Lcom/intsig/office/fc/ss/format/CellFormatter;

    .line 18
    .line 19
    new-instance v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;

    .line 20
    .line 21
    const-string v1, "#.#"

    .line 22
    .line 23
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;-><init>(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    sput-object v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->SIMPLE_FLOAT:Lcom/intsig/office/fc/ss/format/CellFormatter;

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 10

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/format/CellFormatter;-><init>(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 5
    .line 6
    iput-wide v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 7
    .line 8
    new-instance v2, Ljava/util/LinkedList;

    .line 9
    .line 10
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 14
    .line 15
    new-instance v3, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$NumPartHandler;

    .line 16
    .line 17
    const/4 v4, 0x0

    .line 18
    invoke-direct {v3, p0, v4}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$NumPartHandler;-><init>(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;Lcom/intsig/office/fc/ss/format/OO0o〇〇〇〇0;)V

    .line 19
    .line 20
    .line 21
    sget-object v5, Lcom/intsig/office/fc/ss/format/CellFormatType;->NUMBER:Lcom/intsig/office/fc/ss/format/CellFormatType;

    .line 22
    .line 23
    invoke-static {p1, v5, v3}, Lcom/intsig/office/fc/ss/format/CellFormatPart;->parseFormat(Ljava/lang/String;Lcom/intsig/office/fc/ss/format/CellFormatType;Lcom/intsig/office/fc/ss/format/CellFormatPart$PartHandler;)Ljava/lang/StringBuffer;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 28
    .line 29
    if-nez v3, :cond_0

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 32
    .line 33
    if-eqz v3, :cond_1

    .line 34
    .line 35
    :cond_0
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->slash:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 36
    .line 37
    if-eqz v3, :cond_1

    .line 38
    .line 39
    iput-object v4, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->slash:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 40
    .line 41
    iput-object v4, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 42
    .line 43
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->interpretCommas(Ljava/lang/StringBuffer;)V

    .line 44
    .line 45
    .line 46
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 47
    .line 48
    const/4 v5, 0x0

    .line 49
    if-nez v3, :cond_2

    .line 50
    .line 51
    const/4 v3, 0x0

    .line 52
    const/4 v6, 0x0

    .line 53
    goto :goto_0

    .line 54
    :cond_2
    invoke-direct {p0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->interpretPrecision()I

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    add-int/lit8 v6, v3, 0x1

    .line 59
    .line 60
    if-nez v3, :cond_3

    .line 61
    .line 62
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 63
    .line 64
    invoke-interface {v2, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 65
    .line 66
    .line 67
    iput-object v4, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 68
    .line 69
    :cond_3
    :goto_0
    const/4 v4, 0x1

    .line 70
    if-nez v3, :cond_4

    .line 71
    .line 72
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 73
    .line 74
    .line 75
    move-result-object v7

    .line 76
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_4
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 80
    .line 81
    invoke-interface {v2, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 82
    .line 83
    .line 84
    move-result v7

    .line 85
    add-int/2addr v7, v4

    .line 86
    invoke-direct {p0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->fractionalEnd()I

    .line 87
    .line 88
    .line 89
    move-result v8

    .line 90
    invoke-interface {v2, v7, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    .line 91
    .line 92
    .line 93
    move-result-object v7

    .line 94
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    .line 95
    .line 96
    :goto_1
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 97
    .line 98
    const/4 v8, 0x2

    .line 99
    if-nez v7, :cond_5

    .line 100
    .line 101
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 102
    .line 103
    .line 104
    move-result-object v7

    .line 105
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    .line 106
    .line 107
    goto :goto_2

    .line 108
    :cond_5
    invoke-interface {v2, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 109
    .line 110
    .line 111
    move-result v7

    .line 112
    invoke-direct {p0, v7, v8}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specialsFor(II)Ljava/util/List;

    .line 113
    .line 114
    .line 115
    move-result-object v9

    .line 116
    iput-object v9, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    .line 117
    .line 118
    add-int/2addr v7, v8

    .line 119
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specialsFor(I)Ljava/util/List;

    .line 120
    .line 121
    .line 122
    move-result-object v7

    .line 123
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponentDigitSpecials:Ljava/util/List;

    .line 124
    .line 125
    :goto_2
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->slash:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 126
    .line 127
    if-nez v7, :cond_6

    .line 128
    .line 129
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 130
    .line 131
    .line 132
    move-result-object v7

    .line 133
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 134
    .line 135
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 136
    .line 137
    .line 138
    move-result-object v7

    .line 139
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 140
    .line 141
    goto :goto_4

    .line 142
    :cond_6
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 143
    .line 144
    if-nez v7, :cond_7

    .line 145
    .line 146
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 147
    .line 148
    .line 149
    move-result-object v7

    .line 150
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 151
    .line 152
    goto :goto_3

    .line 153
    :cond_7
    invoke-interface {v2, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 154
    .line 155
    .line 156
    move-result v7

    .line 157
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specialsFor(I)Ljava/util/List;

    .line 158
    .line 159
    .line 160
    move-result-object v7

    .line 161
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 162
    .line 163
    :goto_3
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->slash:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 164
    .line 165
    invoke-interface {v2, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 166
    .line 167
    .line 168
    move-result v7

    .line 169
    add-int/2addr v7, v4

    .line 170
    invoke-direct {p0, v7}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specialsFor(I)Ljava/util/List;

    .line 171
    .line 172
    .line 173
    move-result-object v7

    .line 174
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 175
    .line 176
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    .line 177
    .line 178
    .line 179
    move-result v7

    .line 180
    if-eqz v7, :cond_8

    .line 181
    .line 182
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 183
    .line 184
    .line 185
    move-result-object v7

    .line 186
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 187
    .line 188
    goto :goto_4

    .line 189
    :cond_8
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 190
    .line 191
    invoke-static {v7}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->maxValue(Ljava/util/List;)I

    .line 192
    .line 193
    .line 194
    move-result v7

    .line 195
    iput v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->maxDenominator:I

    .line 196
    .line 197
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 198
    .line 199
    invoke-static {v7}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->singleNumberFormat(Ljava/util/List;)Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object v7

    .line 203
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorFmt:Ljava/lang/String;

    .line 204
    .line 205
    iget-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 206
    .line 207
    invoke-static {v7}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->singleNumberFormat(Ljava/util/List;)Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object v7

    .line 211
    iput-object v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorFmt:Ljava/lang/String;

    .line 212
    .line 213
    :goto_4
    invoke-direct {p0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerEnd()I

    .line 214
    .line 215
    .line 216
    move-result v7

    .line 217
    invoke-interface {v2, v5, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    .line 218
    .line 219
    .line 220
    move-result-object v2

    .line 221
    iput-object v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 222
    .line 223
    iget-object v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 224
    .line 225
    const/16 v7, 0x2e

    .line 226
    .line 227
    const/16 v9, 0x30

    .line 228
    .line 229
    if-nez v2, :cond_9

    .line 230
    .line 231
    new-instance v2, Ljava/lang/StringBuffer;

    .line 232
    .line 233
    const-string v4, "%"

    .line 234
    .line 235
    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    invoke-direct {p0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->calculateIntegerPartWidth()I

    .line 239
    .line 240
    .line 241
    move-result v4

    .line 242
    add-int/2addr v4, v6

    .line 243
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 244
    .line 245
    .line 246
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 247
    .line 248
    .line 249
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 250
    .line 251
    .line 252
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 253
    .line 254
    .line 255
    const-string v3, "f"

    .line 256
    .line 257
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 258
    .line 259
    .line 260
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object v2

    .line 264
    iput-object v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->printfFmt:Ljava/lang/String;

    .line 265
    .line 266
    goto/16 :goto_9

    .line 267
    .line 268
    :cond_9
    new-instance v2, Ljava/lang/StringBuffer;

    .line 269
    .line 270
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 271
    .line 272
    .line 273
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 274
    .line 275
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 276
    .line 277
    .line 278
    move-result v6

    .line 279
    if-ne v6, v4, :cond_a

    .line 280
    .line 281
    const-string v3, "0"

    .line 282
    .line 283
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 284
    .line 285
    .line 286
    const/4 v4, 0x0

    .line 287
    goto :goto_7

    .line 288
    :cond_a
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 289
    .line 290
    .line 291
    move-result-object v3

    .line 292
    :cond_b
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 293
    .line 294
    .line 295
    move-result v6

    .line 296
    if-eqz v6, :cond_d

    .line 297
    .line 298
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 299
    .line 300
    .line 301
    move-result-object v6

    .line 302
    check-cast v6, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 303
    .line 304
    invoke-static {v6}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    .line 305
    .line 306
    .line 307
    move-result v6

    .line 308
    if-eqz v6, :cond_b

    .line 309
    .line 310
    if-eqz v4, :cond_c

    .line 311
    .line 312
    const/16 v4, 0x23

    .line 313
    .line 314
    goto :goto_6

    .line 315
    :cond_c
    const/16 v4, 0x30

    .line 316
    .line 317
    :goto_6
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 318
    .line 319
    .line 320
    const/4 v4, 0x0

    .line 321
    goto :goto_5

    .line 322
    :cond_d
    :goto_7
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    .line 323
    .line 324
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 325
    .line 326
    .line 327
    move-result v3

    .line 328
    if-lez v3, :cond_10

    .line 329
    .line 330
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 331
    .line 332
    .line 333
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    .line 334
    .line 335
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 336
    .line 337
    .line 338
    move-result-object v3

    .line 339
    :cond_e
    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 340
    .line 341
    .line 342
    move-result v6

    .line 343
    if-eqz v6, :cond_10

    .line 344
    .line 345
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 346
    .line 347
    .line 348
    move-result-object v6

    .line 349
    check-cast v6, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 350
    .line 351
    invoke-static {v6}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    .line 352
    .line 353
    .line 354
    move-result v6

    .line 355
    if-eqz v6, :cond_e

    .line 356
    .line 357
    if-nez v4, :cond_f

    .line 358
    .line 359
    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 360
    .line 361
    .line 362
    :cond_f
    const/4 v4, 0x0

    .line 363
    goto :goto_8

    .line 364
    :cond_10
    const/16 v3, 0x45

    .line 365
    .line 366
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 367
    .line 368
    .line 369
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    .line 370
    .line 371
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 372
    .line 373
    .line 374
    move-result v4

    .line 375
    invoke-interface {v3, v8, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    .line 376
    .line 377
    .line 378
    move-result-object v3

    .line 379
    invoke-static {v2, v3}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->placeZeros(Ljava/lang/StringBuffer;Ljava/util/List;)V

    .line 380
    .line 381
    .line 382
    new-instance v3, Ljava/text/DecimalFormat;

    .line 383
    .line 384
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 385
    .line 386
    .line 387
    move-result-object v2

    .line 388
    invoke-direct {v3, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 389
    .line 390
    .line 391
    iput-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalFmt:Ljava/text/DecimalFormat;

    .line 392
    .line 393
    :goto_9
    iget-object v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 394
    .line 395
    if-eqz v2, :cond_11

    .line 396
    .line 397
    iput-wide v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 398
    .line 399
    :cond_11
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 400
    .line 401
    .line 402
    move-result-object p1

    .line 403
    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->desc:Ljava/lang/String;

    .line 404
    .line 405
    return-void
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method static bridge synthetic O8(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;)D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->previousNumber()Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic Oo08(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->slash:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic Oooo8o0〇()Lcom/intsig/office/fc/ss/format/CellFormatter;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->SIMPLE_FLOAT:Lcom/intsig/office/fc/ss/format/CellFormatter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private calculateIntegerPartWidth()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_2

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterInteger:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 21
    .line 22
    if-ne v2, v3, :cond_1

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    invoke-static {v2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-eqz v2, :cond_0

    .line 30
    .line 31
    add-int/lit8 v1, v1, 0x1

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    :goto_1
    return v1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method static deleteMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Z)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;-><init>(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Z)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private static firstDigit(Ljava/util/List;)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;)",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;"
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    return-object v0

    .line 24
    :cond_1
    const/4 p0, 0x0

    .line 25
    return-object p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private fractionalEnd()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterFractional:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterInteger:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterFractional:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 17
    .line 18
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterFractional:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 19
    .line 20
    if-nez v0, :cond_2

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    goto :goto_1

    .line 29
    :cond_2
    iget-object v1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    :goto_1
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private static varargs hasChar(C[Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C[",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;)Z"
        }
    .end annotation

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    if-ge v2, v0, :cond_2

    .line 5
    .line 6
    aget-object v3, p1, v2

    .line 7
    .line 8
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v3

    .line 12
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v4

    .line 16
    if-eqz v4, :cond_1

    .line 17
    .line 18
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    check-cast v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 23
    .line 24
    iget-char v4, v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 25
    .line 26
    if-ne v4, p0, :cond_0

    .line 27
    .line 28
    const/4 p0, 0x1

    .line 29
    return p0

    .line 30
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_2
    return v1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static varargs hasOnly(C[Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C[",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;)Z"
        }
    .end annotation

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    if-ge v2, v0, :cond_2

    .line 5
    .line 6
    aget-object v3, p1, v2

    .line 7
    .line 8
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v3

    .line 12
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v4

    .line 16
    if-eqz v4, :cond_1

    .line 17
    .line 18
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    check-cast v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 23
    .line 24
    iget-char v4, v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 25
    .line 26
    if-eq v4, p0, :cond_0

    .line 27
    .line 28
    return v1

    .line 29
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_2
    const/4 p0, 0x1

    .line 33
    return p0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static insertMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;I)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, p1, p2, v1}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;-><init>(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;ILcom/intsig/office/fc/ss/format/〇8o8o〇;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private integerEnd()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iput-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterInteger:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterInteger:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 16
    .line 17
    if-eqz v0, :cond_2

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterInteger:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_2
    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterInteger:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 24
    .line 25
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->afterInteger:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 26
    .line 27
    if-nez v0, :cond_3

    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    goto :goto_1

    .line 36
    :cond_3
    iget-object v1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 37
    .line 38
    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    :goto_1
    return v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private interpretCommas(Ljava/lang/StringBuffer;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerEnd()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-interface {v0, v1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    iput-boolean v1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerCommas:Z

    .line 13
    .line 14
    const/4 v2, 0x1

    .line 15
    const/4 v3, 0x1

    .line 16
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    .line 17
    .line 18
    .line 19
    move-result v4

    .line 20
    const-wide v5, 0x408f400000000000L    # 1000.0

    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    const/16 v7, 0x2c

    .line 26
    .line 27
    if-eqz v4, :cond_2

    .line 28
    .line 29
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    check-cast v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 34
    .line 35
    iget-char v4, v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 36
    .line 37
    if-eq v4, v7, :cond_0

    .line 38
    .line 39
    const/4 v3, 0x0

    .line 40
    goto :goto_0

    .line 41
    :cond_0
    if-eqz v3, :cond_1

    .line 42
    .line 43
    iget-wide v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 44
    .line 45
    div-double/2addr v7, v5

    .line 46
    iput-wide v7, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    iput-boolean v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerCommas:Z

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 53
    .line 54
    if-eqz v0, :cond_4

    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->fractionalEnd()I

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    invoke-interface {v0, v2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    :goto_1
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    .line 67
    .line 68
    .line 69
    move-result v2

    .line 70
    if-eqz v2, :cond_4

    .line 71
    .line 72
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 77
    .line 78
    iget-char v2, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 79
    .line 80
    if-eq v2, v7, :cond_3

    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_3
    iget-wide v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 84
    .line 85
    div-double/2addr v2, v5

    .line 86
    iput-wide v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 90
    .line 91
    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    :cond_5
    :goto_3
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    if-eqz v2, :cond_6

    .line 100
    .line 101
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 106
    .line 107
    iget v3, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 108
    .line 109
    sub-int/2addr v3, v1

    .line 110
    iput v3, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 111
    .line 112
    iget-char v3, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 113
    .line 114
    if-ne v3, v7, :cond_5

    .line 115
    .line 116
    add-int/lit8 v1, v1, 0x1

    .line 117
    .line 118
    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    .line 119
    .line 120
    .line 121
    iget v2, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 122
    .line 123
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 124
    .line 125
    .line 126
    goto :goto_3

    .line 127
    :cond_6
    return-void
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private interpretPrecision()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, -0x1

    .line 6
    return v0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 8
    .line 9
    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-interface {v1, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    :cond_1
    const/4 v1, 0x0

    .line 27
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_2

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 38
    .line 39
    invoke-static {v2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_2

    .line 44
    .line 45
    add-int/lit8 v1, v1, 0x1

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_2
    return v1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private static isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z
    .locals 1

    .line 1
    iget-char p0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 2
    .line 3
    const/16 v0, 0x30

    .line 4
    .line 5
    if-eq p0, v0, :cond_1

    .line 6
    .line 7
    const/16 v0, 0x3f

    .line 8
    .line 9
    if-eq p0, v0, :cond_1

    .line 10
    .line 11
    const/16 v0, 0x23

    .line 12
    .line 13
    if-ne p0, v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 19
    :goto_1
    return p0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static maxValue(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;)I"
        }
    .end annotation

    .line 1
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    int-to-double v0, p0

    .line 6
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    .line 7
    .line 8
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    .line 9
    .line 10
    .line 11
    move-result-wide v0

    .line 12
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 13
    .line 14
    sub-double/2addr v0, v2

    .line 15
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    long-to-int p0, v0

    .line 20
    return p0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic oO80(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic o〇0(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static placeZeros(Ljava/lang/StringBuffer;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const/16 v0, 0x30

    .line 24
    .line 25
    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private previousNumber()Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-interface {v0, v1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_3

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 22
    .line 23
    invoke-static {v1}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    if-eqz v2, :cond_2

    .line 34
    .line 35
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 40
    .line 41
    iget v3, v1, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 42
    .line 43
    iget v4, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 44
    .line 45
    sub-int/2addr v3, v4

    .line 46
    const/4 v4, 0x1

    .line 47
    if-le v3, v4, :cond_1

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_1
    invoke-static {v2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    if-eqz v3, :cond_2

    .line 55
    .line 56
    move-object v1, v2

    .line 57
    goto :goto_0

    .line 58
    :cond_2
    :goto_1
    return-object v1

    .line 59
    :cond_3
    const/4 v0, 0x0

    .line 60
    return-object v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method static replaceMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZC)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;
    .locals 7

    .line 1
    new-instance v6, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 2
    .line 3
    move-object v0, v6

    .line 4
    move-object v1, p0

    .line 5
    move v2, p1

    .line 6
    move-object v3, p2

    .line 7
    move v4, p3

    .line 8
    move v5, p4

    .line 9
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;-><init>(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZC)V

    .line 10
    .line 11
    .line 12
    return-object v6
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private static singleNumberFormat(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "%0"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result p0

    .line 15
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string p0, "d"

    .line 19
    .line 20
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p0

    .line 27
    return-object p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private specialsFor(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 9
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specialsFor(II)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private specialsFor(II)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    add-int/2addr p2, p1

    invoke-interface {v0, p2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 4
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 5
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    .line 6
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 7
    invoke-static {v2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->isDigitFmt(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget v4, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    iget v1, v1, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    sub-int/2addr v4, v1

    if-le v4, v3, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 p2, p2, 0x1

    move-object v1, v2

    goto :goto_0

    .line 8
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    add-int/2addr p2, v3

    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private writeFraction(DLjava/lang/StringBuffer;DLjava/lang/StringBuffer;Ljava/util/Set;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/lang/StringBuffer;",
            "D",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/Set<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;",
            ">;)V"
        }
    .end annotation

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-wide/from16 v8, p4

    .line 4
    .line 5
    move-object/from16 v0, p7

    .line 6
    .line 7
    iget-boolean v1, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->improperFraction:Z

    .line 8
    .line 9
    const-wide/16 v10, 0x0

    .line 10
    .line 11
    const/4 v12, 0x1

    .line 12
    if-nez v1, :cond_b

    .line 13
    .line 14
    const/16 v13, 0x20

    .line 15
    .line 16
    const/4 v14, 0x2

    .line 17
    const/16 v15, 0x3f

    .line 18
    .line 19
    const/16 v1, 0x30

    .line 20
    .line 21
    const/4 v6, 0x0

    .line 22
    cmpl-double v2, v8, v10

    .line 23
    .line 24
    if-nez v2, :cond_1

    .line 25
    .line 26
    new-array v3, v12, [Ljava/util/List;

    .line 27
    .line 28
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 29
    .line 30
    aput-object v4, v3, v6

    .line 31
    .line 32
    invoke-static {v1, v3}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-nez v3, :cond_1

    .line 37
    .line 38
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 39
    .line 40
    const/4 v8, 0x0

    .line 41
    move-object/from16 v1, p0

    .line 42
    .line 43
    move-object/from16 v2, p3

    .line 44
    .line 45
    move-object/from16 v3, p6

    .line 46
    .line 47
    move-object/from16 v5, p7

    .line 48
    .line 49
    const/4 v9, 0x0

    .line 50
    move v6, v8

    .line 51
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 52
    .line 53
    .line 54
    iget-object v1, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 55
    .line 56
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    sub-int/2addr v2, v12

    .line 61
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    check-cast v1, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 66
    .line 67
    iget-object v2, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 68
    .line 69
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    sub-int/2addr v3, v12

    .line 74
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 79
    .line 80
    const/4 v3, 0x3

    .line 81
    new-array v3, v3, [Ljava/util/List;

    .line 82
    .line 83
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 84
    .line 85
    aput-object v4, v3, v9

    .line 86
    .line 87
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 88
    .line 89
    aput-object v4, v3, v12

    .line 90
    .line 91
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 92
    .line 93
    aput-object v4, v3, v14

    .line 94
    .line 95
    invoke-static {v15, v3}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    .line 96
    .line 97
    .line 98
    move-result v3

    .line 99
    if-eqz v3, :cond_0

    .line 100
    .line 101
    invoke-static {v1, v9, v2, v12, v13}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->replaceMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZC)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_0
    invoke-static {v1, v9, v2, v12}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->deleteMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Z)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 110
    .line 111
    .line 112
    move-result-object v1

    .line 113
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    :goto_0
    return-void

    .line 117
    :cond_1
    const/4 v3, 0x0

    .line 118
    cmpl-double v4, p1, v10

    .line 119
    .line 120
    if-nez v4, :cond_2

    .line 121
    .line 122
    if-nez v2, :cond_2

    .line 123
    .line 124
    const/4 v6, 0x1

    .line 125
    goto :goto_1

    .line 126
    :cond_2
    const/4 v6, 0x0

    .line 127
    :goto_1
    if-nez v2, :cond_4

    .line 128
    .line 129
    new-array v2, v12, [Ljava/util/List;

    .line 130
    .line 131
    iget-object v5, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 132
    .line 133
    aput-object v5, v2, v3

    .line 134
    .line 135
    invoke-static {v1, v2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    .line 136
    .line 137
    .line 138
    move-result v2

    .line 139
    if-eqz v2, :cond_3

    .line 140
    .line 141
    goto :goto_2

    .line 142
    :cond_3
    const/4 v2, 0x0

    .line 143
    goto :goto_3

    .line 144
    :cond_4
    :goto_2
    const/4 v2, 0x1

    .line 145
    :goto_3
    if-eqz v6, :cond_6

    .line 146
    .line 147
    new-array v5, v12, [Ljava/util/List;

    .line 148
    .line 149
    iget-object v10, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 150
    .line 151
    aput-object v10, v5, v3

    .line 152
    .line 153
    const/16 v10, 0x23

    .line 154
    .line 155
    invoke-static {v10, v5}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->hasOnly(C[Ljava/util/List;)Z

    .line 156
    .line 157
    .line 158
    move-result v5

    .line 159
    if-nez v5, :cond_5

    .line 160
    .line 161
    new-array v5, v12, [Ljava/util/List;

    .line 162
    .line 163
    iget-object v10, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 164
    .line 165
    aput-object v10, v5, v3

    .line 166
    .line 167
    invoke-static {v1, v5}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    .line 168
    .line 169
    .line 170
    move-result v5

    .line 171
    if-nez v5, :cond_6

    .line 172
    .line 173
    :cond_5
    const/4 v5, 0x1

    .line 174
    goto :goto_4

    .line 175
    :cond_6
    const/4 v5, 0x0

    .line 176
    :goto_4
    if-nez v6, :cond_7

    .line 177
    .line 178
    if-nez v4, :cond_7

    .line 179
    .line 180
    if-eqz v2, :cond_7

    .line 181
    .line 182
    new-array v2, v12, [Ljava/util/List;

    .line 183
    .line 184
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 185
    .line 186
    aput-object v4, v2, v3

    .line 187
    .line 188
    invoke-static {v1, v2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    .line 189
    .line 190
    .line 191
    move-result v1

    .line 192
    if-nez v1, :cond_7

    .line 193
    .line 194
    const/4 v6, 0x1

    .line 195
    goto :goto_5

    .line 196
    :cond_7
    const/4 v6, 0x0

    .line 197
    :goto_5
    if-nez v5, :cond_9

    .line 198
    .line 199
    if-eqz v6, :cond_8

    .line 200
    .line 201
    goto :goto_7

    .line 202
    :cond_8
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 203
    .line 204
    const/4 v6, 0x0

    .line 205
    move-object/from16 v1, p0

    .line 206
    .line 207
    move-object/from16 v2, p3

    .line 208
    .line 209
    move-object/from16 v3, p6

    .line 210
    .line 211
    move-object/from16 v5, p7

    .line 212
    .line 213
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 214
    .line 215
    .line 216
    :goto_6
    const-wide/16 v1, 0x0

    .line 217
    .line 218
    goto :goto_8

    .line 219
    :cond_9
    :goto_7
    iget-object v1, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 220
    .line 221
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 222
    .line 223
    .line 224
    move-result v2

    .line 225
    sub-int/2addr v2, v12

    .line 226
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 227
    .line 228
    .line 229
    move-result-object v1

    .line 230
    check-cast v1, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 231
    .line 232
    new-array v2, v14, [Ljava/util/List;

    .line 233
    .line 234
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 235
    .line 236
    aput-object v4, v2, v3

    .line 237
    .line 238
    iget-object v4, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 239
    .line 240
    aput-object v4, v2, v12

    .line 241
    .line 242
    invoke-static {v15, v2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->hasChar(C[Ljava/util/List;)Z

    .line 243
    .line 244
    .line 245
    move-result v2

    .line 246
    if-eqz v2, :cond_a

    .line 247
    .line 248
    iget-object v2, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 249
    .line 250
    invoke-static {v1, v12, v2, v3, v13}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->replaceMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZC)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 251
    .line 252
    .line 253
    move-result-object v1

    .line 254
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 255
    .line 256
    .line 257
    goto :goto_6

    .line 258
    :cond_a
    iget-object v2, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 259
    .line 260
    invoke-static {v1, v12, v2, v3}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->deleteMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Z)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 261
    .line 262
    .line 263
    move-result-object v1

    .line 264
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 265
    .line 266
    .line 267
    goto :goto_6

    .line 268
    :cond_b
    move-wide v1, v10

    .line 269
    :goto_8
    cmpl-double v3, v8, v1

    .line 270
    .line 271
    if-eqz v3, :cond_d

    .line 272
    .line 273
    :try_start_0
    iget-boolean v3, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->improperFraction:Z

    .line 274
    .line 275
    if-eqz v3, :cond_c

    .line 276
    .line 277
    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    .line 278
    .line 279
    rem-double v3, v8, v3

    .line 280
    .line 281
    cmpl-double v5, v3, v1

    .line 282
    .line 283
    if-nez v5, :cond_c

    .line 284
    .line 285
    goto :goto_9

    .line 286
    :cond_c
    new-instance v1, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Fraction;

    .line 287
    .line 288
    iget v2, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->maxDenominator:I

    .line 289
    .line 290
    invoke-direct {v1, v8, v9, v2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Fraction;-><init>(DI)V

    .line 291
    .line 292
    .line 293
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Fraction;->〇o00〇〇Oo()I

    .line 294
    .line 295
    .line 296
    move-result v2

    .line 297
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Fraction;->〇080()I

    .line 298
    .line 299
    .line 300
    move-result v1

    .line 301
    move v12, v1

    .line 302
    goto :goto_a

    .line 303
    :cond_d
    :goto_9
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->round(D)J

    .line 304
    .line 305
    .line 306
    move-result-wide v1

    .line 307
    long-to-int v2, v1

    .line 308
    :goto_a
    iget-boolean v1, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->improperFraction:Z

    .line 309
    .line 310
    if-eqz v1, :cond_e

    .line 311
    .line 312
    int-to-long v1, v2

    .line 313
    int-to-double v3, v12

    .line 314
    mul-double v3, v3, p1

    .line 315
    .line 316
    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    .line 317
    .line 318
    .line 319
    move-result-wide v3

    .line 320
    add-long/2addr v1, v3

    .line 321
    long-to-int v2, v1

    .line 322
    :cond_e
    move v3, v2

    .line 323
    iget-object v2, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorFmt:Ljava/lang/String;

    .line 324
    .line 325
    iget-object v5, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numeratorSpecials:Ljava/util/List;

    .line 326
    .line 327
    move-object/from16 v1, p0

    .line 328
    .line 329
    move-object/from16 v4, p6

    .line 330
    .line 331
    move-object/from16 v6, p7

    .line 332
    .line 333
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeSingleInteger(Ljava/lang/String;ILjava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;)V

    .line 334
    .line 335
    .line 336
    iget-object v2, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorFmt:Ljava/lang/String;

    .line 337
    .line 338
    iget-object v5, v7, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->denominatorSpecials:Ljava/util/List;

    .line 339
    .line 340
    move-object/from16 v1, p0

    .line 341
    .line 342
    move v3, v12

    .line 343
    move-object/from16 v4, p6

    .line 344
    .line 345
    move-object/from16 v6, p7

    .line 346
    .line 347
    invoke-direct/range {v1 .. v6}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeSingleInteger(Ljava/lang/String;ILjava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    .line 349
    .line 350
    goto :goto_b

    .line 351
    :catch_0
    move-exception v0

    .line 352
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 353
    .line 354
    .line 355
    :goto_b
    return-void
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private writeFractional(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_5

    .line 8
    .line 9
    const-string v0, "."

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    add-int/lit8 v0, v0, 0x1

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 18
    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    const-string v1, "e"

    .line 22
    .line 23
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    :goto_0
    add-int/lit8 v1, v1, -0x1

    .line 33
    .line 34
    :goto_1
    const/16 v2, 0x30

    .line 35
    .line 36
    if-le v1, v0, :cond_1

    .line 37
    .line 38
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    if-ne v3, v2, :cond_1

    .line 43
    .line 44
    add-int/lit8 v1, v1, -0x1

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->fractionalSpecials:Ljava/util/List;

    .line 48
    .line 49
    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    :goto_2
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    if-eqz v4, :cond_5

    .line 58
    .line 59
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    check-cast v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 64
    .line 65
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    if-ne v5, v2, :cond_3

    .line 70
    .line 71
    iget-char v6, v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 72
    .line 73
    if-eq v6, v2, :cond_3

    .line 74
    .line 75
    if-ge v0, v1, :cond_2

    .line 76
    .line 77
    goto :goto_3

    .line 78
    :cond_2
    const/16 v5, 0x3f

    .line 79
    .line 80
    if-ne v6, v5, :cond_4

    .line 81
    .line 82
    iget v4, v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 83
    .line 84
    const/16 v5, 0x20

    .line 85
    .line 86
    invoke-virtual {p2, v4, v5}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 87
    .line 88
    .line 89
    goto :goto_4

    .line 90
    :cond_3
    :goto_3
    iget v4, v4, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 91
    .line 92
    invoke-virtual {p2, v4, v5}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 93
    .line 94
    .line 95
    :cond_4
    :goto_4
    add-int/lit8 v0, v0, 0x1

    .line 96
    .line 97
    goto :goto_2

    .line 98
    :cond_5
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    move-object/from16 v3, p4

    .line 8
    .line 9
    const-string v4, "."

    .line 10
    .line 11
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    .line 12
    .line 13
    .line 14
    move-result v4

    .line 15
    const/4 v5, 0x1

    .line 16
    sub-int/2addr v4, v5

    .line 17
    if-gez v4, :cond_1

    .line 18
    .line 19
    iget-object v4, v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 20
    .line 21
    if-eqz v4, :cond_0

    .line 22
    .line 23
    iget-object v4, v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 24
    .line 25
    if-ne v2, v4, :cond_0

    .line 26
    .line 27
    const-string v4, "E"

    .line 28
    .line 29
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    :goto_0
    sub-int/2addr v4, v5

    .line 39
    :cond_1
    const/4 v6, 0x0

    .line 40
    const/4 v7, 0x0

    .line 41
    :goto_1
    const/16 v8, 0x2c

    .line 42
    .line 43
    const/16 v9, 0x30

    .line 44
    .line 45
    if-ge v7, v4, :cond_3

    .line 46
    .line 47
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 48
    .line 49
    .line 50
    move-result v10

    .line 51
    if-eq v10, v9, :cond_2

    .line 52
    .line 53
    if-eq v10, v8, :cond_2

    .line 54
    .line 55
    goto :goto_2

    .line 56
    :cond_2
    add-int/lit8 v7, v7, 0x1

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_3
    :goto_2
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    .line 60
    .line 61
    .line 62
    move-result v10

    .line 63
    invoke-interface {v2, v10}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    const/4 v10, 0x0

    .line 68
    const/4 v11, 0x0

    .line 69
    :goto_3
    invoke-interface {v2}, Ljava/util/ListIterator;->hasPrevious()Z

    .line 70
    .line 71
    .line 72
    move-result v12

    .line 73
    if-eqz v12, :cond_c

    .line 74
    .line 75
    if-ltz v4, :cond_4

    .line 76
    .line 77
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 78
    .line 79
    .line 80
    move-result v12

    .line 81
    goto :goto_4

    .line 82
    :cond_4
    const/16 v12, 0x30

    .line 83
    .line 84
    :goto_4
    invoke-interface {v2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 85
    .line 86
    .line 87
    move-result-object v13

    .line 88
    check-cast v13, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 89
    .line 90
    if-eqz p5, :cond_5

    .line 91
    .line 92
    if-lez v11, :cond_5

    .line 93
    .line 94
    rem-int/lit8 v14, v11, 0x3

    .line 95
    .line 96
    if-nez v14, :cond_5

    .line 97
    .line 98
    const/4 v14, 0x1

    .line 99
    goto :goto_5

    .line 100
    :cond_5
    const/4 v14, 0x0

    .line 101
    :goto_5
    const/16 v15, 0x3f

    .line 102
    .line 103
    if-ne v12, v9, :cond_7

    .line 104
    .line 105
    iget-char v8, v13, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 106
    .line 107
    if-eq v8, v9, :cond_7

    .line 108
    .line 109
    if-eq v8, v15, :cond_7

    .line 110
    .line 111
    if-lt v4, v7, :cond_6

    .line 112
    .line 113
    goto :goto_6

    .line 114
    :cond_6
    move-object/from16 v15, p2

    .line 115
    .line 116
    const/4 v8, 0x0

    .line 117
    goto :goto_8

    .line 118
    :cond_7
    :goto_6
    iget-char v8, v13, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 119
    .line 120
    if-ne v8, v15, :cond_8

    .line 121
    .line 122
    if-ge v4, v7, :cond_8

    .line 123
    .line 124
    const/4 v8, 0x1

    .line 125
    goto :goto_7

    .line 126
    :cond_8
    const/4 v8, 0x0

    .line 127
    :goto_7
    iget v10, v13, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 128
    .line 129
    if-eqz v8, :cond_9

    .line 130
    .line 131
    const/16 v12, 0x20

    .line 132
    .line 133
    :cond_9
    move-object/from16 v15, p2

    .line 134
    .line 135
    invoke-virtual {v15, v10, v12}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 136
    .line 137
    .line 138
    move-object v10, v13

    .line 139
    :goto_8
    if-eqz v14, :cond_b

    .line 140
    .line 141
    if-eqz v8, :cond_a

    .line 142
    .line 143
    const-string v8, " "

    .line 144
    .line 145
    goto :goto_9

    .line 146
    :cond_a
    const-string v8, ","

    .line 147
    .line 148
    :goto_9
    const/4 v12, 0x2

    .line 149
    invoke-static {v13, v8, v12}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->insertMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;I)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 150
    .line 151
    .line 152
    move-result-object v8

    .line 153
    invoke-interface {v3, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 154
    .line 155
    .line 156
    :cond_b
    add-int/lit8 v11, v11, 0x1

    .line 157
    .line 158
    add-int/lit8 v4, v4, -0x1

    .line 159
    .line 160
    const/16 v8, 0x2c

    .line 161
    .line 162
    goto :goto_3

    .line 163
    :cond_c
    if-ltz v4, :cond_f

    .line 164
    .line 165
    add-int/2addr v4, v5

    .line 166
    new-instance v2, Ljava/lang/StringBuffer;

    .line 167
    .line 168
    invoke-virtual {v1, v6, v4}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v1

    .line 172
    invoke-direct {v2, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 173
    .line 174
    .line 175
    if-eqz p5, :cond_e

    .line 176
    .line 177
    :goto_a
    if-lez v4, :cond_e

    .line 178
    .line 179
    if-lez v11, :cond_d

    .line 180
    .line 181
    rem-int/lit8 v1, v11, 0x3

    .line 182
    .line 183
    if-nez v1, :cond_d

    .line 184
    .line 185
    const/16 v1, 0x2c

    .line 186
    .line 187
    invoke-virtual {v2, v4, v1}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 188
    .line 189
    .line 190
    goto :goto_b

    .line 191
    :cond_d
    const/16 v1, 0x2c

    .line 192
    .line 193
    :goto_b
    add-int/lit8 v11, v11, 0x1

    .line 194
    .line 195
    add-int/lit8 v4, v4, -0x1

    .line 196
    .line 197
    goto :goto_a

    .line 198
    :cond_e
    invoke-static {v10, v2, v5}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->insertMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Ljava/lang/CharSequence;I)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 199
    .line 200
    .line 201
    move-result-object v1

    .line 202
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 203
    .line 204
    .line 205
    :cond_f
    return-void
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method private writeScientific(DLjava/lang/StringBuffer;Ljava/util/Set;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/Set<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v6, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v7, Ljava/text/FieldPosition;

    .line 7
    .line 8
    const/4 v8, 0x1

    .line 9
    invoke-direct {v7, v8}, Ljava/text/FieldPosition;-><init>(I)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalFmt:Ljava/text/DecimalFormat;

    .line 13
    .line 14
    invoke-virtual {v0, p1, p2, v6, v7}, Ljava/text/DecimalFormat;->format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 15
    .line 16
    .line 17
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 18
    .line 19
    iget-boolean v5, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerCommas:Z

    .line 20
    .line 21
    move-object v0, p0

    .line 22
    move-object v1, v6

    .line 23
    move-object v2, p3

    .line 24
    move-object v4, p4

    .line 25
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0, v6, p3}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeFractional(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v7}, Ljava/text/FieldPosition;->getEndIndex()I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    add-int/2addr p1, v8

    .line 36
    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 37
    .line 38
    .line 39
    move-result p2

    .line 40
    const/16 v0, 0x2b

    .line 41
    .line 42
    const/16 v1, 0x2d

    .line 43
    .line 44
    if-eq p2, v1, :cond_0

    .line 45
    .line 46
    invoke-virtual {v6, p1, v0}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 47
    .line 48
    .line 49
    const/16 p2, 0x2b

    .line 50
    .line 51
    :cond_0
    iget-object v2, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponentSpecials:Ljava/util/List;

    .line 52
    .line 53
    invoke-interface {v2, v8}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 62
    .line 63
    iget-char v3, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇080:C

    .line 64
    .line 65
    if-eq p2, v1, :cond_2

    .line 66
    .line 67
    if-ne v3, v0, :cond_1

    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_1
    invoke-static {v2, v8, v2, v8}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->deleteMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;Z)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 71
    .line 72
    .line 73
    move-result-object p2

    .line 74
    invoke-interface {p4, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_2
    :goto_0
    invoke-static {v2, v8, v2, v8, p2}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->replaceMod(Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZLcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;ZC)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 79
    .line 80
    .line 81
    move-result-object p2

    .line 82
    invoke-interface {p4, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    :goto_1
    new-instance v1, Ljava/lang/StringBuffer;

    .line 86
    .line 87
    add-int/2addr p1, v8

    .line 88
    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    invoke-direct {v1, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    iget-object v3, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponentDigitSpecials:Ljava/util/List;

    .line 96
    .line 97
    const/4 v5, 0x0

    .line 98
    move-object v0, p0

    .line 99
    move-object v2, p3

    .line 100
    move-object v4, p4

    .line 101
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private writeSingleInteger(Ljava/lang/String;ILjava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/StringBuffer;",
            "Ljava/util/List<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v1, Ljava/lang/StringBuffer;

    .line 2
    .line 3
    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/util/Formatter;

    .line 7
    .line 8
    invoke-direct {v0, v1}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    .line 9
    .line 10
    .line 11
    sget-object v2, Lcom/intsig/office/fc/ss/format/CellFormatter;->LOCALE:Ljava/util/Locale;

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    new-array v3, v3, [Ljava/lang/Object;

    .line 15
    .line 16
    const/4 v4, 0x0

    .line 17
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    aput-object p2, v3, v4

    .line 22
    .line 23
    invoke-virtual {v0, v2, p1, v3}, Ljava/util/Formatter;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 24
    .line 25
    .line 26
    const/4 v5, 0x0

    .line 27
    move-object v0, p0

    .line 28
    move-object v2, p3

    .line 29
    move-object v3, p4

    .line 30
    move-object v4, p5

    .line 31
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->improperFraction:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->slash:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇O〇(Ljava/util/List;)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->firstDigit(Ljava/util/List;)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;)Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇〇808〇()Lcom/intsig/office/fc/ss/format/CellFormatter;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->SIMPLE_INT:Lcom/intsig/office/fc/ss/format/CellFormatter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/office/fc/ss/format/CellNumberFormatter;Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->decimalPoint:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public formatValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 17

    .line 1
    move-object/from16 v8, p0

    .line 2
    .line 3
    move-object/from16 v9, p1

    .line 4
    .line 5
    move-object/from16 v0, p2

    .line 6
    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    iget-wide v2, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->scale:D

    .line 14
    .line 15
    mul-double v0, v0, v2

    .line 16
    .line 17
    const/4 v10, 0x0

    .line 18
    const/4 v11, 0x1

    .line 19
    const-wide/16 v2, 0x0

    .line 20
    .line 21
    cmpg-double v4, v0, v2

    .line 22
    .line 23
    if-gez v4, :cond_0

    .line 24
    .line 25
    const/4 v12, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 v12, 0x0

    .line 28
    :goto_0
    if-eqz v12, :cond_1

    .line 29
    .line 30
    neg-double v0, v0

    .line 31
    :cond_1
    iget-object v4, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->slash:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 32
    .line 33
    if-eqz v4, :cond_3

    .line 34
    .line 35
    iget-boolean v4, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->improperFraction:Z

    .line 36
    .line 37
    if-eqz v4, :cond_2

    .line 38
    .line 39
    move-wide v4, v0

    .line 40
    move-wide v1, v2

    .line 41
    goto :goto_1

    .line 42
    :cond_2
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 43
    .line 44
    rem-double v2, v0, v2

    .line 45
    .line 46
    double-to-long v0, v0

    .line 47
    long-to-double v0, v0

    .line 48
    :cond_3
    move-wide v4, v2

    .line 49
    move-wide v1, v0

    .line 50
    :goto_1
    new-instance v13, Ljava/util/TreeSet;

    .line 51
    .line 52
    invoke-direct {v13}, Ljava/util/TreeSet;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v14, Ljava/lang/StringBuffer;

    .line 56
    .line 57
    iget-object v0, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->desc:Ljava/lang/String;

    .line 58
    .line 59
    invoke-direct {v14, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    iget-object v0, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->exponent:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 63
    .line 64
    if-eqz v0, :cond_4

    .line 65
    .line 66
    invoke-direct {v8, v1, v2, v14, v13}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeScientific(DLjava/lang/StringBuffer;Ljava/util/Set;)V

    .line 67
    .line 68
    .line 69
    goto :goto_2

    .line 70
    :cond_4
    iget-boolean v0, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->improperFraction:Z

    .line 71
    .line 72
    if-eqz v0, :cond_5

    .line 73
    .line 74
    const/4 v3, 0x0

    .line 75
    move-object/from16 v0, p0

    .line 76
    .line 77
    move-object v6, v14

    .line 78
    move-object v7, v13

    .line 79
    invoke-direct/range {v0 .. v7}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeFraction(DLjava/lang/StringBuffer;DLjava/lang/StringBuffer;Ljava/util/Set;)V

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_5
    new-instance v3, Ljava/lang/StringBuffer;

    .line 84
    .line 85
    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 86
    .line 87
    .line 88
    new-instance v0, Ljava/util/Formatter;

    .line 89
    .line 90
    invoke-direct {v0, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    .line 91
    .line 92
    .line 93
    sget-object v6, Lcom/intsig/office/fc/ss/format/CellFormatter;->LOCALE:Ljava/util/Locale;

    .line 94
    .line 95
    iget-object v7, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->printfFmt:Ljava/lang/String;

    .line 96
    .line 97
    new-array v15, v11, [Ljava/lang/Object;

    .line 98
    .line 99
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 100
    .line 101
    .line 102
    move-result-object v16

    .line 103
    aput-object v16, v15, v10

    .line 104
    .line 105
    invoke-virtual {v0, v6, v7, v15}, Ljava/util/Formatter;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 106
    .line 107
    .line 108
    iget-object v0, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->numerator:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 109
    .line 110
    if-nez v0, :cond_6

    .line 111
    .line 112
    invoke-direct {v8, v3, v14}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeFractional(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    .line 113
    .line 114
    .line 115
    iget-object v4, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerSpecials:Ljava/util/List;

    .line 116
    .line 117
    iget-boolean v5, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->integerCommas:Z

    .line 118
    .line 119
    move-object/from16 v0, p0

    .line 120
    .line 121
    move-object v1, v3

    .line 122
    move-object v2, v14

    .line 123
    move-object v3, v4

    .line 124
    move-object v4, v13

    .line 125
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeInteger(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/util/List;Ljava/util/Set;Z)V

    .line 126
    .line 127
    .line 128
    goto :goto_2

    .line 129
    :cond_6
    move-object/from16 v0, p0

    .line 130
    .line 131
    move-object v6, v14

    .line 132
    move-object v7, v13

    .line 133
    invoke-direct/range {v0 .. v7}, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->writeFraction(DLjava/lang/StringBuffer;DLjava/lang/StringBuffer;Ljava/util/Set;)V

    .line 134
    .line 135
    .line 136
    :goto_2
    iget-object v0, v8, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->specials:Ljava/util/List;

    .line 137
    .line 138
    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 147
    .line 148
    .line 149
    move-result v2

    .line 150
    if-eqz v2, :cond_7

    .line 151
    .line 152
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 153
    .line 154
    .line 155
    move-result-object v2

    .line 156
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 157
    .line 158
    goto :goto_3

    .line 159
    :cond_7
    const/4 v2, 0x0

    .line 160
    :goto_3
    new-instance v4, Ljava/util/BitSet;

    .line 161
    .line 162
    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    .line 163
    .line 164
    .line 165
    const/4 v5, 0x0

    .line 166
    :goto_4
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    .line 167
    .line 168
    .line 169
    move-result v6

    .line 170
    if-eqz v6, :cond_15

    .line 171
    .line 172
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 173
    .line 174
    .line 175
    move-result-object v6

    .line 176
    check-cast v6, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 177
    .line 178
    iget v7, v6, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 179
    .line 180
    add-int v13, v7, v5

    .line 181
    .line 182
    invoke-virtual {v4, v7}, Ljava/util/BitSet;->get(I)Z

    .line 183
    .line 184
    .line 185
    move-result v7

    .line 186
    if-nez v7, :cond_8

    .line 187
    .line 188
    invoke-virtual {v14, v13}, Ljava/lang/StringBuffer;->charAt(I)C

    .line 189
    .line 190
    .line 191
    move-result v7

    .line 192
    const/16 v15, 0x23

    .line 193
    .line 194
    if-ne v7, v15, :cond_8

    .line 195
    .line 196
    invoke-virtual {v14, v13}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 197
    .line 198
    .line 199
    add-int/lit8 v5, v5, -0x1

    .line 200
    .line 201
    iget v7, v6, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 202
    .line 203
    invoke-virtual {v4, v7}, Ljava/util/BitSet;->set(I)V

    .line 204
    .line 205
    .line 206
    :cond_8
    :goto_5
    if-eqz v2, :cond_14

    .line 207
    .line 208
    iget-object v7, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->o0:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 209
    .line 210
    if-ne v6, v7, :cond_14

    .line 211
    .line 212
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    .line 213
    .line 214
    .line 215
    move-result v7

    .line 216
    iget v13, v6, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 217
    .line 218
    add-int v15, v13, v5

    .line 219
    .line 220
    iget v3, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->〇OOo8〇0:I

    .line 221
    .line 222
    if-eq v3, v11, :cond_11

    .line 223
    .line 224
    const/4 v11, 0x2

    .line 225
    if-eq v3, v11, :cond_f

    .line 226
    .line 227
    const/4 v11, 0x3

    .line 228
    if-ne v3, v11, :cond_e

    .line 229
    .line 230
    iget-boolean v3, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->o〇00O:Z

    .line 231
    .line 232
    if-nez v3, :cond_9

    .line 233
    .line 234
    :goto_6
    add-int/lit8 v13, v13, 0x1

    .line 235
    .line 236
    add-int/lit8 v15, v15, 0x1

    .line 237
    .line 238
    :cond_9
    invoke-virtual {v4, v13}, Ljava/util/BitSet;->get(I)Z

    .line 239
    .line 240
    .line 241
    move-result v3

    .line 242
    if-eqz v3, :cond_a

    .line 243
    .line 244
    goto :goto_6

    .line 245
    :cond_a
    iget-object v3, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->〇08O〇00〇o:Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;

    .line 246
    .line 247
    iget v3, v3, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 248
    .line 249
    iget-boolean v11, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->O8o08O8O:Z

    .line 250
    .line 251
    if-eqz v11, :cond_b

    .line 252
    .line 253
    add-int/lit8 v3, v3, 0x1

    .line 254
    .line 255
    :cond_b
    add-int v11, v3, v5

    .line 256
    .line 257
    if-ge v15, v11, :cond_12

    .line 258
    .line 259
    iget-object v2, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->OO:Ljava/lang/CharSequence;

    .line 260
    .line 261
    const-string v10, ""

    .line 262
    .line 263
    if-ne v2, v10, :cond_c

    .line 264
    .line 265
    invoke-virtual {v14, v15, v11}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 266
    .line 267
    .line 268
    const/4 v10, 0x0

    .line 269
    goto :goto_8

    .line 270
    :cond_c
    const/4 v10, 0x0

    .line 271
    invoke-interface {v2, v10}, Ljava/lang/CharSequence;->charAt(I)C

    .line 272
    .line 273
    .line 274
    move-result v2

    .line 275
    :goto_7
    if-ge v15, v11, :cond_d

    .line 276
    .line 277
    invoke-virtual {v14, v15, v2}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 278
    .line 279
    .line 280
    add-int/lit8 v15, v15, 0x1

    .line 281
    .line 282
    goto :goto_7

    .line 283
    :cond_d
    :goto_8
    invoke-virtual {v4, v13, v3}, Ljava/util/BitSet;->set(II)V

    .line 284
    .line 285
    .line 286
    goto :goto_a

    .line 287
    :cond_e
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 288
    .line 289
    new-instance v1, Ljava/lang/StringBuilder;

    .line 290
    .line 291
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 292
    .line 293
    .line 294
    const-string v3, "Unknown op: "

    .line 295
    .line 296
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    .line 298
    .line 299
    iget v2, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->〇OOo8〇0:I

    .line 300
    .line 301
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 302
    .line 303
    .line 304
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 305
    .line 306
    .line 307
    move-result-object v1

    .line 308
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 309
    .line 310
    .line 311
    throw v0

    .line 312
    :cond_f
    iget-object v3, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->OO:Ljava/lang/CharSequence;

    .line 313
    .line 314
    const-string v11, ","

    .line 315
    .line 316
    invoke-virtual {v3, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 317
    .line 318
    .line 319
    move-result v3

    .line 320
    if-eqz v3, :cond_10

    .line 321
    .line 322
    iget v3, v6, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$Special;->〇o00〇〇Oo:I

    .line 323
    .line 324
    invoke-virtual {v4, v3}, Ljava/util/BitSet;->get(I)Z

    .line 325
    .line 326
    .line 327
    move-result v3

    .line 328
    if-eqz v3, :cond_10

    .line 329
    .line 330
    goto :goto_a

    .line 331
    :cond_10
    const/4 v3, 0x1

    .line 332
    goto :goto_9

    .line 333
    :cond_11
    const/4 v3, 0x0

    .line 334
    :goto_9
    add-int/2addr v15, v3

    .line 335
    iget-object v2, v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;->OO:Ljava/lang/CharSequence;

    .line 336
    .line 337
    invoke-virtual {v14, v15, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/CharSequence;)Ljava/lang/StringBuffer;

    .line 338
    .line 339
    .line 340
    :cond_12
    :goto_a
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    .line 341
    .line 342
    .line 343
    move-result v2

    .line 344
    sub-int/2addr v2, v7

    .line 345
    add-int/2addr v5, v2

    .line 346
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 347
    .line 348
    .line 349
    move-result v2

    .line 350
    if-eqz v2, :cond_13

    .line 351
    .line 352
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 353
    .line 354
    .line 355
    move-result-object v2

    .line 356
    check-cast v2, Lcom/intsig/office/fc/ss/format/CellNumberFormatter$StringMod;

    .line 357
    .line 358
    goto :goto_b

    .line 359
    :cond_13
    const/4 v2, 0x0

    .line 360
    :goto_b
    const/4 v11, 0x1

    .line 361
    goto/16 :goto_5

    .line 362
    .line 363
    :cond_14
    const/4 v11, 0x1

    .line 364
    goto/16 :goto_4

    .line 365
    .line 366
    :cond_15
    if-eqz v12, :cond_16

    .line 367
    .line 368
    const/16 v0, 0x2d

    .line 369
    .line 370
    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 371
    .line 372
    .line 373
    :cond_16
    invoke-virtual {v9, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 374
    .line 375
    .line 376
    return-void
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public simpleValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/format/CellNumberFormatter;->SIMPLE_NUMBER:Lcom/intsig/office/fc/ss/format/CellFormatter;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/fc/ss/format/CellFormatter;->formatValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

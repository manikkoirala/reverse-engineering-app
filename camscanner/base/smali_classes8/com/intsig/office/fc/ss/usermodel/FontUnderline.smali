.class public final enum Lcom/intsig/office/fc/ss/usermodel/FontUnderline;
.super Ljava/lang/Enum;
.source "FontUnderline.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/office/fc/ss/usermodel/FontUnderline;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

.field public static final enum DOUBLE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

.field public static final enum DOUBLE_ACCOUNTING:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

.field public static final enum NONE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

.field public static final enum SINGLE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

.field public static final enum SINGLE_ACCOUNTING:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

.field private static _table:[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 2
    .line 3
    const-string v1, "SINGLE"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x1

    .line 7
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->SINGLE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 13
    .line 14
    const-string v4, "DOUBLE"

    .line 15
    .line 16
    const/4 v5, 0x2

    .line 17
    invoke-direct {v1, v4, v3, v5}, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    .line 18
    .line 19
    .line 20
    sput-object v1, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->DOUBLE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 21
    .line 22
    new-instance v4, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 23
    .line 24
    const-string v6, "SINGLE_ACCOUNTING"

    .line 25
    .line 26
    const/4 v7, 0x3

    .line 27
    invoke-direct {v4, v6, v5, v7}, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    .line 28
    .line 29
    .line 30
    sput-object v4, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->SINGLE_ACCOUNTING:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 31
    .line 32
    new-instance v6, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 33
    .line 34
    const-string v8, "DOUBLE_ACCOUNTING"

    .line 35
    .line 36
    const/4 v9, 0x4

    .line 37
    invoke-direct {v6, v8, v7, v9}, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    .line 38
    .line 39
    .line 40
    sput-object v6, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->DOUBLE_ACCOUNTING:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 41
    .line 42
    new-instance v8, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 43
    .line 44
    const-string v10, "NONE"

    .line 45
    .line 46
    const/4 v11, 0x5

    .line 47
    invoke-direct {v8, v10, v9, v11}, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;-><init>(Ljava/lang/String;II)V

    .line 48
    .line 49
    .line 50
    sput-object v8, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->NONE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 51
    .line 52
    new-array v10, v11, [Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 53
    .line 54
    aput-object v0, v10, v2

    .line 55
    .line 56
    aput-object v1, v10, v3

    .line 57
    .line 58
    aput-object v4, v10, v5

    .line 59
    .line 60
    aput-object v6, v10, v7

    .line 61
    .line 62
    aput-object v8, v10, v9

    .line 63
    .line 64
    sput-object v10, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->$VALUES:[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 65
    .line 66
    const/4 v0, 0x6

    .line 67
    new-array v0, v0, [Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 68
    .line 69
    sput-object v0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->_table:[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 70
    .line 71
    invoke-static {}, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->values()[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    array-length v1, v0

    .line 76
    :goto_0
    if-ge v2, v1, :cond_0

    .line 77
    .line 78
    aget-object v3, v0, v2

    .line 79
    .line 80
    sget-object v4, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->_table:[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 81
    .line 82
    invoke-virtual {v3}, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->getValue()I

    .line 83
    .line 84
    .line 85
    move-result v5

    .line 86
    aput-object v3, v4, v5

    .line 87
    .line 88
    add-int/lit8 v2, v2, 0x1

    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_0
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->value:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static valueOf(B)Lcom/intsig/office/fc/ss/usermodel/FontUnderline;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/16 v0, 0x21

    if-eq p0, v0, :cond_1

    const/16 v0, 0x22

    if-eq p0, v0, :cond_0

    .line 3
    sget-object p0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->NONE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    goto :goto_0

    .line 4
    :cond_0
    sget-object p0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->DOUBLE_ACCOUNTING:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    goto :goto_0

    .line 5
    :cond_1
    sget-object p0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->SINGLE_ACCOUNTING:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    goto :goto_0

    .line 6
    :cond_2
    sget-object p0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->DOUBLE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    goto :goto_0

    .line 7
    :cond_3
    sget-object p0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->SINGLE:Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    :goto_0
    return-object p0
.end method

.method public static valueOf(I)Lcom/intsig/office/fc/ss/usermodel/FontUnderline;
    .locals 1

    .line 2
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->_table:[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    aget-object p0, v0, p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/office/fc/ss/usermodel/FontUnderline;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    return-object p0
.end method

.method public static values()[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->$VALUES:[Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/office/fc/ss/usermodel/FontUnderline;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public getByteValue()B
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline$1;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    aget v0, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x2

    .line 10
    const/4 v2, 0x1

    .line 11
    if-eq v0, v2, :cond_3

    .line 12
    .line 13
    if-eq v0, v1, :cond_2

    .line 14
    .line 15
    const/4 v1, 0x3

    .line 16
    if-eq v0, v1, :cond_1

    .line 17
    .line 18
    const/4 v1, 0x4

    .line 19
    if-eq v0, v1, :cond_0

    .line 20
    .line 21
    return v2

    .line 22
    :cond_0
    const/4 v0, 0x0

    .line 23
    return v0

    .line 24
    :cond_1
    const/16 v0, 0x21

    .line 25
    .line 26
    return v0

    .line 27
    :cond_2
    const/16 v0, 0x22

    .line 28
    .line 29
    return v0

    .line 30
    :cond_3
    return v1
.end method

.method public getValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ss/usermodel/FontUnderline;->value:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

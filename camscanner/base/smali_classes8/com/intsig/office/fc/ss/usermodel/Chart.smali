.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/Chart;
.super Ljava/lang/Object;
.source "Chart.java"

# interfaces
.implements Lcom/intsig/office/fc/ss/usermodel/charts/ManuallyPositionable;


# virtual methods
.method public abstract deleteLegend()V
.end method

.method public abstract getAxis()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/office/fc/ss/usermodel/charts/ChartAxis;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getChartAxisFactory()Lcom/intsig/office/fc/ss/usermodel/charts/ChartAxisFactory;
.end method

.method public abstract getChartDataFactory()Lcom/intsig/office/fc/ss/usermodel/charts/ChartDataFactory;
.end method

.method public abstract getOrCreateLegend()Lcom/intsig/office/fc/ss/usermodel/charts/ChartLegend;
.end method

.method public varargs abstract plot(Lcom/intsig/office/fc/ss/usermodel/charts/ChartData;[Lcom/intsig/office/fc/ss/usermodel/charts/ChartAxis;)V
.end method

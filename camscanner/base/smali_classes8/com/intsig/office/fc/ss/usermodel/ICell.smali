.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/ICell;
.super Ljava/lang/Object;
.source "ICell.java"


# static fields
.field public static final CELL_TYPE_BLANK:I = 0x3

.field public static final CELL_TYPE_BOOLEAN:I = 0x4

.field public static final CELL_TYPE_ERROR:I = 0x5

.field public static final CELL_TYPE_FORMULA:I = 0x2

.field public static final CELL_TYPE_NUMERIC:I = 0x0

.field public static final CELL_TYPE_STRING:I = 0x1


# virtual methods
.method public abstract getArrayFormulaRange()Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
.end method

.method public abstract getBooleanCellValue()Z
.end method

.method public abstract getCachedFormulaResultType()I
.end method

.method public abstract getCellComment()Lcom/intsig/office/fc/ss/usermodel/Comment;
.end method

.method public abstract getCellFormula()Ljava/lang/String;
.end method

.method public abstract getCellStyle()Lcom/intsig/office/fc/ss/usermodel/ICellStyle;
.end method

.method public abstract getCellType()I
.end method

.method public abstract getColumnIndex()I
.end method

.method public abstract getDateCellValue()Ljava/util/Date;
.end method

.method public abstract getErrorCellValue()B
.end method

.method public abstract getHyperlink()Lcom/intsig/office/fc/ss/usermodel/IHyperlink;
.end method

.method public abstract getNumericCellValue()D
.end method

.method public abstract getRichStringCellValue()Lcom/intsig/office/fc/ss/usermodel/RichTextString;
.end method

.method public abstract getRow()Lcom/intsig/office/fc/ss/usermodel/IRow;
.end method

.method public abstract getRowIndex()I
.end method

.method public abstract getSheet()Lcom/intsig/office/fc/ss/usermodel/Sheet;
.end method

.method public abstract getStringCellValue()Ljava/lang/String;
.end method

.method public abstract isPartOfArrayFormulaGroup()Z
.end method

.method public abstract removeCellComment()V
.end method

.method public abstract setAsActiveCell()V
.end method

.method public abstract setCellComment(Lcom/intsig/office/fc/ss/usermodel/Comment;)V
.end method

.method public abstract setCellErrorValue(B)V
.end method

.method public abstract setCellFormula(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/fc/hssf/formula/FormulaParseException;
        }
    .end annotation
.end method

.method public abstract setCellStyle(Lcom/intsig/office/fc/ss/usermodel/ICellStyle;)V
.end method

.method public abstract setCellType(I)V
.end method

.method public abstract setCellValue(D)V
.end method

.method public abstract setCellValue(Lcom/intsig/office/fc/ss/usermodel/RichTextString;)V
.end method

.method public abstract setCellValue(Ljava/lang/String;)V
.end method

.method public abstract setCellValue(Ljava/util/Calendar;)V
.end method

.method public abstract setCellValue(Ljava/util/Date;)V
.end method

.method public abstract setCellValue(Z)V
.end method

.method public abstract setHyperlink(Lcom/intsig/office/fc/ss/usermodel/IHyperlink;)V
.end method

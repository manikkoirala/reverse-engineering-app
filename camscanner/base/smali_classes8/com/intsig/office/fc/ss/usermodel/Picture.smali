.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/Picture;
.super Ljava/lang/Object;
.source "Picture.java"


# virtual methods
.method public abstract getPictureData()Lcom/intsig/office/fc/ss/usermodel/PictureData;
.end method

.method public abstract getPreferredSize()Lcom/intsig/office/fc/hssf/usermodel/IClientAnchor;
.end method

.method public abstract resize()V
.end method

.method public abstract resize(D)V
.end method

.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/CellRange;
.super Ljava/lang/Object;
.source "CellRange.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Lcom/intsig/office/fc/ss/usermodel/ICell;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "TC;>;"
    }
.end annotation


# virtual methods
.method public abstract getCell(II)Lcom/intsig/office/fc/ss/usermodel/ICell;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)TC;"
        }
    .end annotation
.end method

.method public abstract getCells()[[Lcom/intsig/office/fc/ss/usermodel/ICell;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[[TC;"
        }
    .end annotation
.end method

.method public abstract getFlattenedCells()[Lcom/intsig/office/fc/ss/usermodel/ICell;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[TC;"
        }
    .end annotation
.end method

.method public abstract getHeight()I
.end method

.method public abstract getReferenceText()Ljava/lang/String;
.end method

.method public abstract getTopLeftCell()Lcom/intsig/office/fc/ss/usermodel/ICell;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TC;"
        }
    .end annotation
.end method

.method public abstract getWidth()I
.end method

.method public abstract iterator()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TC;>;"
        }
    .end annotation
.end method

.method public abstract size()I
.end method

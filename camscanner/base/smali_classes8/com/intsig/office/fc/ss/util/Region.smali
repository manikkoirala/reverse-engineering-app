.class public Lcom/intsig/office/fc/ss/util/Region;
.super Ljava/lang/Object;
.source "Region.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/intsig/office/fc/ss/util/Region;",
        ">;"
    }
.end annotation


# instance fields
.field private _colFrom:S

.field private _colTo:S

.field private _rowFrom:I

.field private _rowTo:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ISIS)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowFrom:I

    .line 4
    iput p3, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowTo:I

    .line 5
    iput-short p2, p0, Lcom/intsig/office/fc/ss/util/Region;->_colFrom:S

    .line 6
    iput-short p4, p0, Lcom/intsig/office/fc/ss/util/Region;->_colTo:S

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Lcom/intsig/office/fc/ss/util/CellReference;

    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/intsig/office/fc/ss/util/CellReference;-><init>(Ljava/lang/String;)V

    .line 9
    new-instance v2, Lcom/intsig/office/fc/ss/util/CellReference;

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Lcom/intsig/office/fc/ss/util/CellReference;-><init>(Ljava/lang/String;)V

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->getRow()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowFrom:I

    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->getCol()S

    move-result p1

    iput-short p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_colFrom:S

    .line 12
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellReference;->getRow()I

    move-result p1

    iput p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowTo:I

    .line 13
    invoke-virtual {v2}, Lcom/intsig/office/fc/ss/util/CellReference;->getCol()S

    move-result p1

    iput-short p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_colTo:S

    return-void
.end method

.method public static convertCellRangesToRegions([Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)[Lcom/intsig/office/fc/ss/util/Region;
    .locals 4

    .line 1
    array-length v0, p0

    .line 2
    const/4 v1, 0x1

    .line 3
    const/4 v2, 0x0

    .line 4
    if-ge v0, v1, :cond_0

    .line 5
    .line 6
    new-array p0, v2, [Lcom/intsig/office/fc/ss/util/Region;

    .line 7
    .line 8
    return-object p0

    .line 9
    :cond_0
    new-array v1, v0, [Lcom/intsig/office/fc/ss/util/Region;

    .line 10
    .line 11
    :goto_0
    if-eq v2, v0, :cond_1

    .line 12
    .line 13
    aget-object v3, p0, v2

    .line 14
    .line 15
    invoke-static {v3}, Lcom/intsig/office/fc/ss/util/Region;->convertToRegion(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)Lcom/intsig/office/fc/ss/util/Region;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    aput-object v3, v1, v2

    .line 20
    .line 21
    add-int/lit8 v2, v2, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    return-object v1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static convertRegionsToCellRanges([Lcom/intsig/office/fc/ss/util/Region;)[Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
    .locals 4

    .line 1
    array-length v0, p0

    .line 2
    const/4 v1, 0x1

    .line 3
    const/4 v2, 0x0

    .line 4
    if-ge v0, v1, :cond_0

    .line 5
    .line 6
    new-array p0, v2, [Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 7
    .line 8
    return-object p0

    .line 9
    :cond_0
    new-array v1, v0, [Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 10
    .line 11
    :goto_0
    if-eq v2, v0, :cond_1

    .line 12
    .line 13
    aget-object v3, p0, v2

    .line 14
    .line 15
    invoke-static {v3}, Lcom/intsig/office/fc/ss/util/Region;->convertToCellRangeAddress(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    aput-object v3, v1, v2

    .line 20
    .line 21
    add-int/lit8 v2, v2, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    return-object v1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static convertToCellRangeAddress(Lcom/intsig/office/fc/ss/util/Region;)Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getRowFrom()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getRowTo()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getColumnFrom()S

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getColumnTo()S

    .line 16
    .line 17
    .line 18
    move-result p0

    .line 19
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;-><init>(IIII)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private static convertToRegion(Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)Lcom/intsig/office/fc/ss/util/Region;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/util/Region;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    int-to-short v2, v2

    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 17
    .line 18
    .line 19
    move-result p0

    .line 20
    int-to-short p0, p0

    .line 21
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/intsig/office/fc/ss/util/Region;-><init>(ISIS)V

    .line 22
    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public compareTo(Lcom/intsig/office/fc/ss/util/Region;)I
    .locals 2

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getRowFrom()I

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getRowFrom()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getColumnFrom()S

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getColumnFrom()S

    move-result v1

    if-ne v0, v1, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getRowTo()I

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getRowTo()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getColumnTo()S

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getColumnTo()S

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getRowFrom()I

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getRowFrom()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getColumnFrom()S

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getColumnFrom()S

    move-result v1

    if-lt v0, v1, :cond_2

    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getRowTo()I

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getRowTo()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/fc/ss/util/Region;->getColumnTo()S

    move-result v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/Region;->getColumnTo()S

    move-result p1

    if-ge v0, p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/office/fc/ss/util/Region;

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/ss/util/Region;->compareTo(Lcom/intsig/office/fc/ss/util/Region;)I

    move-result p1

    return p1
.end method

.method public contains(IS)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowFrom:I

    .line 2
    .line 3
    if-gt v0, p1, :cond_0

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowTo:I

    .line 6
    .line 7
    if-lt v0, p1, :cond_0

    .line 8
    .line 9
    iget-short p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_colFrom:S

    .line 10
    .line 11
    if-gt p1, p2, :cond_0

    .line 12
    .line 13
    iget-short p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_colTo:S

    .line 14
    .line 15
    if-lt p1, p2, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    return p1

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public equals(Lcom/intsig/office/fc/ss/util/Region;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/ss/util/Region;->compareTo(Lcom/intsig/office/fc/ss/util/Region;)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 p1, 0x0

    .line 10
    :goto_0
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getArea()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowTo:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowFrom:I

    .line 4
    .line 5
    sub-int/2addr v0, v1

    .line 6
    add-int/lit8 v0, v0, 0x1

    .line 7
    .line 8
    iget-short v1, p0, Lcom/intsig/office/fc/ss/util/Region;->_colTo:S

    .line 9
    .line 10
    iget-short v2, p0, Lcom/intsig/office/fc/ss/util/Region;->_colFrom:S

    .line 11
    .line 12
    sub-int/2addr v1, v2

    .line 13
    add-int/lit8 v1, v1, 0x1

    .line 14
    .line 15
    mul-int v0, v0, v1

    .line 16
    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColumnFrom()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/ss/util/Region;->_colFrom:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColumnTo()S
    .locals 1

    .line 1
    iget-short v0, p0, Lcom/intsig/office/fc/ss/util/Region;->_colTo:S

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRegionRef()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/fc/ss/util/CellReference;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowFrom:I

    .line 4
    .line 5
    iget-short v2, p0, Lcom/intsig/office/fc/ss/util/Region;->_colFrom:S

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/fc/ss/util/CellReference;-><init>(IS)V

    .line 8
    .line 9
    .line 10
    new-instance v1, Lcom/intsig/office/fc/ss/util/CellReference;

    .line 11
    .line 12
    iget v2, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowTo:I

    .line 13
    .line 14
    iget-short v3, p0, Lcom/intsig/office/fc/ss/util/Region;->_colTo:S

    .line 15
    .line 16
    invoke-direct {v1, v2, v3}, Lcom/intsig/office/fc/ss/util/CellReference;-><init>(IS)V

    .line 17
    .line 18
    .line 19
    new-instance v2, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/office/fc/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v0, ":"

    .line 32
    .line 33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/office/fc/ss/util/CellReference;->formatAsString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getRowFrom()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowFrom:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRowTo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowTo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setColumnFrom(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_colFrom:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setColumnTo(S)V
    .locals 0

    .line 1
    iput-short p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_colTo:S

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRowFrom(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowFrom:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setRowTo(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/fc/ss/util/Region;->_rowTo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

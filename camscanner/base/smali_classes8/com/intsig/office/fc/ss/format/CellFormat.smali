.class public Lcom/intsig/office/fc/ss/format/CellFormat;
.super Ljava/lang/Object;
.source "CellFormat.java"


# static fields
.field private static final DEFAULT_TEXT_FORMAT:Lcom/intsig/office/fc/ss/format/CellFormatPart;

.field public static final GENERAL_FORMAT:Lcom/intsig/office/fc/ss/format/CellFormat;

.field private static final ONE_PART:Ljava/util/regex/Pattern;

.field private static final formatCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/office/fc/ss/format/CellFormat;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final format:Ljava/lang/String;

.field private final negNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

.field private final posNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

.field private final textFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

.field private final zeroNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/office/fc/ss/format/CellFormatPart;->FORMAT_PAT:Ljava/util/regex/Pattern;

    .line 7
    .line 8
    invoke-virtual {v1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    const-string v1, "(;|$)"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const/4 v1, 0x6

    .line 25
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sput-object v0, Lcom/intsig/office/fc/ss/format/CellFormat;->ONE_PART:Ljava/util/regex/Pattern;

    .line 30
    .line 31
    new-instance v0, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 32
    .line 33
    const-string v1, "@"

    .line 34
    .line 35
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/ss/format/CellFormatPart;-><init>(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    sput-object v0, Lcom/intsig/office/fc/ss/format/CellFormat;->DEFAULT_TEXT_FORMAT:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 39
    .line 40
    new-instance v0, Lcom/intsig/office/fc/ss/format/CellFormat$1;

    .line 41
    .line 42
    const-string v1, "General"

    .line 43
    .line 44
    invoke-direct {v0, v1}, Lcom/intsig/office/fc/ss/format/CellFormat$1;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    sput-object v0, Lcom/intsig/office/fc/ss/format/CellFormat;->GENERAL_FORMAT:Lcom/intsig/office/fc/ss/format/CellFormat;

    .line 48
    .line 49
    new-instance v0, Ljava/util/WeakHashMap;

    .line 50
    .line 51
    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 52
    .line 53
    .line 54
    sput-object v0, Lcom/intsig/office/fc/ss/format/CellFormat;->formatCache:Ljava/util/Map;

    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 6

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->format:Ljava/lang/String;

    .line 4
    sget-object v0, Lcom/intsig/office/fc/ss/format/CellFormat;->ONE_PART:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6
    :goto_0
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    .line 7
    :try_start_0
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    const-string v4, ";"

    .line 8
    invoke-virtual {v1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 9
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 10
    :cond_0
    new-instance v2, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    invoke-direct {v2, v1}, Lcom/intsig/office/fc/ss/format/CellFormatPart;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 11
    sget-object v2, Lcom/intsig/office/fc/ss/format/CellFormatter;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid format: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/intsig/office/fc/ss/format/CellFormatter;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 13
    invoke-virtual {v2, v3, v4, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    .line 14
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 15
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    if-eq p1, v3, :cond_4

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    const/4 v4, 0x3

    if-eq p1, v4, :cond_2

    .line 16
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->posNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 17
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->zeroNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 18
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->negNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 19
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->textFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    goto :goto_1

    .line 20
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->posNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 21
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->zeroNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->negNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 23
    sget-object p1, Lcom/intsig/office/fc/ss/format/CellFormat;->DEFAULT_TEXT_FORMAT:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->textFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    goto :goto_1

    .line 24
    :cond_3
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->zeroNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->posNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 25
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->negNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 26
    sget-object p1, Lcom/intsig/office/fc/ss/format/CellFormat;->DEFAULT_TEXT_FORMAT:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->textFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    goto :goto_1

    .line 27
    :cond_4
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->negNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->zeroNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->posNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    .line 28
    sget-object p1, Lcom/intsig/office/fc/ss/format/CellFormat;->DEFAULT_TEXT_FORMAT:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    iput-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->textFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    :goto_1
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/intsig/office/fc/ss/format/〇o〇;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/fc/ss/format/CellFormat;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/intsig/office/fc/ss/format/CellFormat;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/office/fc/ss/format/CellFormat;->formatCache:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    check-cast v1, Lcom/intsig/office/fc/ss/format/CellFormat;

    .line 8
    .line 9
    if-nez v1, :cond_1

    .line 10
    .line 11
    const-string v1, "General"

    .line 12
    .line 13
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    sget-object v1, Lcom/intsig/office/fc/ss/format/CellFormat;->GENERAL_FORMAT:Lcom/intsig/office/fc/ss/format/CellFormat;

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    new-instance v1, Lcom/intsig/office/fc/ss/format/CellFormat;

    .line 23
    .line 24
    invoke-direct {v1, p0}, Lcom/intsig/office/fc/ss/format/CellFormat;-><init>(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :goto_0
    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    :cond_1
    return-object v1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static ultimateType(Lcom/intsig/office/fc/ss/usermodel/ICell;)I
    .locals 2

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellType()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    invoke-interface {p0}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCachedFormulaResultType()I

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    return p0

    .line 13
    :cond_0
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public apply(Lcom/intsig/office/fc/ss/usermodel/ICell;)Lcom/intsig/office/fc/ss/format/CellFormatResult;
    .locals 2

    .line 8
    invoke-static {p1}, Lcom/intsig/office/fc/ss/format/CellFormat;->ultimateType(Lcom/intsig/office/fc/ss/usermodel/ICell;)I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const-string p1, "?"

    .line 9
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1

    .line 10
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getBooleanCellValue()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1

    :cond_1
    const-string p1, ""

    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1

    .line 12
    :cond_2
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getStringCellValue()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1

    .line 13
    :cond_3
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getNumericCellValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/intsig/office/fc/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;
    .locals 5

    .line 1
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_2

    .line 2
    move-object v0, p1

    check-cast v0, Ljava/lang/Number;

    .line 3
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    .line 4
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->posNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ss/format/CellFormatPart;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1

    :cond_0
    cmpg-double v4, v0, v2

    if-gez v4, :cond_1

    .line 5
    iget-object p1, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->negNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    neg-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/intsig/office/fc/ss/format/CellFormatPart;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->zeroNumFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ss/format/CellFormatPart;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1

    .line 7
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->textFmt:Lcom/intsig/office/fc/ss/format/CellFormatPart;

    invoke-virtual {v0, p1}, Lcom/intsig/office/fc/ss/format/CellFormatPart;->apply(Ljava/lang/Object;)Lcom/intsig/office/fc/ss/format/CellFormatResult;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    if-ne p0, p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    return p1

    .line 5
    :cond_0
    instance-of v0, p1, Lcom/intsig/office/fc/ss/format/CellFormat;

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    check-cast p1, Lcom/intsig/office/fc/ss/format/CellFormat;

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->format:Ljava/lang/String;

    .line 12
    .line 13
    iget-object p1, p1, Lcom/intsig/office/fc/ss/format/CellFormat;->format:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    return p1

    .line 20
    :cond_1
    const/4 p1, 0x0

    .line 21
    return p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/fc/ss/format/CellFormat;->format:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

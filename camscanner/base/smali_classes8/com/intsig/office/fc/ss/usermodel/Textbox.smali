.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/Textbox;
.super Ljava/lang/Object;
.source "Textbox.java"


# static fields
.field public static final OBJECT_TYPE_TEXT:S = 0x6s


# virtual methods
.method public abstract getMarginBottom()I
.end method

.method public abstract getMarginLeft()I
.end method

.method public abstract getMarginRight()I
.end method

.method public abstract getMarginTop()I
.end method

.method public abstract getString()Lcom/intsig/office/fc/ss/usermodel/RichTextString;
.end method

.method public abstract setMarginBottom(I)V
.end method

.method public abstract setMarginLeft(I)V
.end method

.method public abstract setMarginRight(I)V
.end method

.method public abstract setMarginTop(I)V
.end method

.method public abstract setString(Lcom/intsig/office/fc/ss/usermodel/RichTextString;)V
.end method

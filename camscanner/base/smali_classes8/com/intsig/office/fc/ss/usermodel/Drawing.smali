.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/Drawing;
.super Ljava/lang/Object;
.source "Drawing.java"


# virtual methods
.method public abstract createAnchor(IIIIIIII)Lcom/intsig/office/fc/hssf/usermodel/IClientAnchor;
.end method

.method public abstract createCellComment(Lcom/intsig/office/fc/hssf/usermodel/IClientAnchor;)Lcom/intsig/office/fc/ss/usermodel/Comment;
.end method

.method public abstract createChart(Lcom/intsig/office/fc/hssf/usermodel/IClientAnchor;)Lcom/intsig/office/fc/ss/usermodel/Chart;
.end method

.method public abstract createPicture(Lcom/intsig/office/fc/hssf/usermodel/IClientAnchor;I)Lcom/intsig/office/fc/ss/usermodel/Picture;
.end method

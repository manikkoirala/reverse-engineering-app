.class public Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;
.super Ljava/lang/Object;
.source "CellWalk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;
    }
.end annotation


# instance fields
.field private range:Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

.field private sheet:Lcom/intsig/office/fc/ss/usermodel/Sheet;

.field private traverseEmptyCells:Z


# direct methods
.method public constructor <init>(Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->sheet:Lcom/intsig/office/fc/ss/usermodel/Sheet;

    .line 4
    iput-object p2, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->range:Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    const/4 p1, 0x0

    .line 5
    iput-boolean p1, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->traverseEmptyCells:Z

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/fc/ss/util/DataMarker;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/DataMarker;->getSheet()Lcom/intsig/office/fc/ss/usermodel/Sheet;

    move-result-object v0

    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/DataMarker;->getRange()Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;-><init>(Lcom/intsig/office/fc/ss/usermodel/Sheet;Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;)V

    return-void
.end method

.method private isEmpty(Lcom/intsig/office/fc/ss/usermodel/ICell;)Z
    .locals 1

    .line 1
    invoke-interface {p1}, Lcom/intsig/office/fc/ss/usermodel/ICell;->getCellType()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 v0, 0x3

    .line 6
    if-ne p1, v0, :cond_0

    .line 7
    .line 8
    const/4 p1, 0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    :goto_0
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public isTraverseEmptyCells()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->traverseEmptyCells:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setTraverseEmptyCells(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->traverseEmptyCells:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public traverse(Lcom/intsig/office/fc/ss/util/cellwalk/CellHandler;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->range:Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstRow()I

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->range:Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastRow()I

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->range:Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getFirstColumn()I

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;->range:Lcom/intsig/office/fc/ss/util/HSSFCellRangeAddress;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/fc/ss/util/CellRangeAddressBase;->getLastColumn()I

    .line 19
    .line 20
    .line 21
    new-instance p1, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    invoke-direct {p1, p0, v0}, Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk$SimpleCellWalkContext;-><init>(Lcom/intsig/office/fc/ss/util/cellwalk/CellWalk;LO8oO0/〇080;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

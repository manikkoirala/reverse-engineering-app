.class public interface abstract Lcom/intsig/office/fc/ss/usermodel/RichTextString;
.super Ljava/lang/Object;
.source "RichTextString.java"


# virtual methods
.method public abstract applyFont(IILcom/intsig/office/fc/ss/usermodel/IFont;)V
.end method

.method public abstract applyFont(IIS)V
.end method

.method public abstract applyFont(Lcom/intsig/office/fc/ss/usermodel/IFont;)V
.end method

.method public abstract applyFont(S)V
.end method

.method public abstract clearFormatting()V
.end method

.method public abstract getIndexOfFormattingRun(I)I
.end method

.method public abstract getString()Ljava/lang/String;
.end method

.method public abstract length()I
.end method

.method public abstract numFormattingRuns()I
.end method

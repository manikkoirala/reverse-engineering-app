.class public interface abstract Lcom/intsig/office/fc/sl/usermodel/Slide;
.super Ljava/lang/Object;
.source "Slide.java"

# interfaces
.implements Lcom/intsig/office/fc/sl/usermodel/Sheet;


# virtual methods
.method public abstract getFollowMasterBackground()Z
.end method

.method public abstract getFollowMasterColourScheme()Z
.end method

.method public abstract getFollowMasterObjects()Z
.end method

.method public abstract getNotes()Lcom/intsig/office/fc/sl/usermodel/Notes;
.end method

.method public abstract setFollowMasterBackground(Z)V
.end method

.method public abstract setFollowMasterColourScheme(Z)V
.end method

.method public abstract setFollowMasterObjects(Z)V
.end method

.method public abstract setNotes(Lcom/intsig/office/fc/sl/usermodel/Notes;)V
.end method

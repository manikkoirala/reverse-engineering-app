.class public interface abstract Lcom/intsig/office/fc/sl/usermodel/Shape;
.super Ljava/lang/Object;
.source "Shape.java"


# virtual methods
.method public abstract getAnchor()Lcom/intsig/office/java/awt/geom/Rectangle2D;
.end method

.method public abstract getParent()Lcom/intsig/office/fc/sl/usermodel/Shape;
.end method

.method public abstract getShapeType()I
.end method

.method public abstract moveTo(FF)V
.end method

.method public abstract setAnchor(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V
.end method

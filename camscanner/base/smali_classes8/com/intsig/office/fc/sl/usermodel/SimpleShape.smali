.class public interface abstract Lcom/intsig/office/fc/sl/usermodel/SimpleShape;
.super Ljava/lang/Object;
.source "SimpleShape.java"

# interfaces
.implements Lcom/intsig/office/fc/sl/usermodel/Shape;


# virtual methods
.method public abstract getFill()Lcom/intsig/office/fc/sl/usermodel/Fill;
.end method

.method public abstract getHyperlink()Lcom/intsig/office/fc/sl/usermodel/Hyperlink;
.end method

.method public abstract getLineStyle()Lcom/intsig/office/fc/sl/usermodel/LineStyle;
.end method

.method public abstract setHyperlink(Lcom/intsig/office/fc/sl/usermodel/Hyperlink;)V
.end method

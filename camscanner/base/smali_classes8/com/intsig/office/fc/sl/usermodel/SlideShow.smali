.class public interface abstract Lcom/intsig/office/fc/sl/usermodel/SlideShow;
.super Ljava/lang/Object;
.source "SlideShow.java"


# virtual methods
.method public abstract createMasterSheet()Lcom/intsig/office/fc/sl/usermodel/MasterSheet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract createSlide()Lcom/intsig/office/fc/sl/usermodel/Slide;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getMasterSheet()[Lcom/intsig/office/fc/sl/usermodel/MasterSheet;
.end method

.method public abstract getResources()Lcom/intsig/office/fc/sl/usermodel/Resources;
.end method

.method public abstract getSlides()[Lcom/intsig/office/fc/sl/usermodel/Slide;
.end method

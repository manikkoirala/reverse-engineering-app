.class public interface abstract Lcom/intsig/office/simpletext/model/IElementCollection;
.super Ljava/lang/Object;
.source "IElementCollection.java"


# virtual methods
.method public abstract dispose()V
.end method

.method public abstract getElement(J)Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract size()I
.end method

.class public Lcom/intsig/office/simpletext/model/ElementCollectionImpl;
.super Ljava/lang/Object;
.source "ElementCollectionImpl.java"

# interfaces
.implements Lcom/intsig/office/simpletext/model/IElementCollection;


# static fields
.field public static final CAPACITY:I = 0x5


# instance fields
.field protected elems:[Lcom/intsig/office/simpletext/model/IElement;

.field private size:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-array p1, p1, [Lcom/intsig/office/simpletext/model/IElement;

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private ensureCapacity()V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x5

    .line 4
    .line 5
    new-array v1, v1, [Lcom/intsig/office/simpletext/model/IElement;

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11
    .line 12
    .line 13
    iput-object v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public addElement(Lcom/intsig/office/simpletext/model/IElement;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 4
    .line 5
    array-length v1, v1

    .line 6
    if-lt v0, v1, :cond_0

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->ensureCapacity()V

    .line 9
    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 14
    .line 15
    aput-object p1, v0, v1

    .line 16
    .line 17
    add-int/lit8 v1, v1, 0x1

    .line 18
    .line 19
    iput v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dispose()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    :goto_0
    iget v2, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    if-ge v0, v2, :cond_0

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 13
    .line 14
    aget-object v2, v2, v0

    .line 15
    .line 16
    invoke-interface {v2}, Lcom/intsig/office/simpletext/model/IElement;->dispose()V

    .line 17
    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 20
    .line 21
    aput-object v3, v2, v0

    .line 22
    .line 23
    add-int/lit8 v0, v0, 0x1

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iput-object v3, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 27
    .line 28
    :cond_1
    iput v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 29
    .line 30
    return-void
.end method

.method public getElement(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getIndex(J)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;
    .locals 1

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    iget v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 4
    .line 5
    if-ge p1, v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 8
    .line 9
    aget-object p1, v0, p1

    .line 10
    .line 11
    return-object p1

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method protected getIndex(J)I
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 2
    .line 3
    if-eqz v0, :cond_4

    .line 4
    .line 5
    const-wide/16 v1, 0x0

    .line 6
    .line 7
    cmp-long v3, p1, v1

    .line 8
    .line 9
    if-ltz v3, :cond_4

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 12
    .line 13
    add-int/lit8 v0, v0, -0x1

    .line 14
    .line 15
    aget-object v0, v1, v0

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 18
    .line 19
    .line 20
    move-result-wide v0

    .line 21
    cmp-long v2, p1, v0

    .line 22
    .line 23
    if-ltz v2, :cond_0

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_0
    iget v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    :cond_1
    :goto_0
    add-int v2, v0, v1

    .line 30
    .line 31
    div-int/lit8 v2, v2, 0x2

    .line 32
    .line 33
    iget-object v3, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 34
    .line 35
    aget-object v3, v3, v2

    .line 36
    .line 37
    invoke-interface {v3}, Lcom/intsig/office/simpletext/model/IElement;->getStartOffset()J

    .line 38
    .line 39
    .line 40
    move-result-wide v4

    .line 41
    invoke-interface {v3}, Lcom/intsig/office/simpletext/model/IElement;->getEndOffset()J

    .line 42
    .line 43
    .line 44
    move-result-wide v6

    .line 45
    cmp-long v3, p1, v4

    .line 46
    .line 47
    if-ltz v3, :cond_2

    .line 48
    .line 49
    cmp-long v3, p1, v6

    .line 50
    .line 51
    if-gez v3, :cond_2

    .line 52
    .line 53
    return v2

    .line 54
    :cond_2
    cmp-long v3, v4, p1

    .line 55
    .line 56
    if-lez v3, :cond_3

    .line 57
    .line 58
    add-int/lit8 v0, v2, -0x1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_3
    cmp-long v3, v6, p1

    .line 62
    .line 63
    if-gtz v3, :cond_1

    .line 64
    .line 65
    add-int/lit8 v1, v2, 0x1

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_4
    :goto_1
    const/4 p1, -0x1

    .line 69
    return p1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public insertElementForIndex(Lcom/intsig/office/simpletext/model/IElement;I)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 6
    .line 7
    array-length v1, v1

    .line 8
    if-lt v0, v1, :cond_0

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->ensureCapacity()V

    .line 11
    .line 12
    .line 13
    :cond_0
    iget v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 14
    .line 15
    :goto_0
    if-lt v0, p2, :cond_1

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 18
    .line 19
    add-int/lit8 v2, v0, -0x1

    .line 20
    .line 21
    aget-object v2, v1, v2

    .line 22
    .line 23
    aput-object v2, v1, v0

    .line 24
    .line 25
    add-int/lit8 v0, v0, -0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 29
    .line 30
    aput-object p1, v0, p2

    .line 31
    .line 32
    iget p1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 33
    .line 34
    add-int/lit8 p1, p1, 0x1

    .line 35
    .line 36
    iput p1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public removeElement(J)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getIndex(J)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-gez p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    int-to-long p1, p1

    .line 9
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->removeElement(J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public removeElementForIndex(I)V
    .locals 4

    .line 1
    if-gez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 5
    .line 6
    aget-object v0, v0, p1

    .line 7
    .line 8
    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 11
    .line 12
    if-ge p1, v1, :cond_1

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 15
    .line 16
    add-int/lit8 v2, p1, -0x1

    .line 17
    .line 18
    aget-object v3, v1, p1

    .line 19
    .line 20
    aput-object v3, v1, v2

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->elems:[Lcom/intsig/office/simpletext/model/IElement;

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    aput-object v2, p1, v1

    .line 27
    .line 28
    add-int/lit8 v1, v1, -0x1

    .line 29
    .line 30
    iput v1, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 31
    .line 32
    invoke-interface {v0}, Lcom/intsig/office/simpletext/model/IElement;->dispose()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
.end method

.method public size()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

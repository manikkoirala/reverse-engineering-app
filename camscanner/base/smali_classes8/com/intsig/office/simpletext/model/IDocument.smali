.class public interface abstract Lcom/intsig/office/simpletext/model/IDocument;
.super Ljava/lang/Object;
.source "IDocument.java"


# virtual methods
.method public abstract appendElement(Lcom/intsig/office/simpletext/model/IElement;J)V
.end method

.method public abstract appendParagraph(Lcom/intsig/office/simpletext/model/IElement;J)V
.end method

.method public abstract appendSection(Lcom/intsig/office/simpletext/model/IElement;)V
.end method

.method public abstract dispose()V
.end method

.method public abstract getArea(J)J
.end method

.method public abstract getAreaEnd(J)J
.end method

.method public abstract getAreaStart(J)J
.end method

.method public abstract getFEElement(J)Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract getHFElement(JB)Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract getLeaf(J)Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract getLength(J)J
.end method

.method public abstract getParaCount(J)I
.end method

.method public abstract getParagraph(J)Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract getParagraphForIndex(IJ)Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract getSection(J)Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract getText(JJ)Ljava/lang/String;
.end method

.method public abstract insertString(Ljava/lang/String;Lcom/intsig/office/simpletext/model/IAttributeSet;J)V
.end method

.method public abstract setLeafAttr(JILcom/intsig/office/simpletext/model/IAttributeSet;)V
.end method

.method public abstract setParagraphAttr(JILcom/intsig/office/simpletext/model/IAttributeSet;)V
.end method

.method public abstract setSectionAttr(JILcom/intsig/office/simpletext/model/IAttributeSet;)V
.end method

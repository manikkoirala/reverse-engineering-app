.class public interface abstract Lcom/intsig/office/simpletext/model/IAttributeSet;
.super Ljava/lang/Object;
.source "IAttributeSet.java"


# virtual methods
.method public abstract clone()Lcom/intsig/office/simpletext/model/IAttributeSet;
.end method

.method public abstract dispose()V
.end method

.method public abstract getAttribute(S)I
.end method

.method public abstract getID()I
.end method

.method public abstract mergeAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;)V
.end method

.method public abstract removeAttribute(S)V
.end method

.method public abstract setAttribute(SI)V
.end method

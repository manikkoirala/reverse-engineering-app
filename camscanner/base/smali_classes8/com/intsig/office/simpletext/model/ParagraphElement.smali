.class public Lcom/intsig/office/simpletext/model/ParagraphElement;
.super Lcom/intsig/office/simpletext/model/AbstractElement;
.source "ParagraphElement.java"


# instance fields
.field private leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/AbstractElement;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 5
    .line 6
    const/16 v1, 0xa

    .line 7
    .line 8
    invoke-direct {v0, v1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;-><init>(I)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/simpletext/model/ParagraphElement;->leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public appendLeaf(Lcom/intsig/office/simpletext/model/LeafElement;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ParagraphElement;->leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->addElement(Lcom/intsig/office/simpletext/model/IElement;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dispose()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/office/simpletext/model/AbstractElement;->dispose()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ParagraphElement;->leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->dispose()V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/intsig/office/simpletext/model/ParagraphElement;->leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ParagraphElement;->leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getLeaf(J)Lcom/intsig/office/simpletext/model/IElement;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/ParagraphElement;->leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElement(J)Lcom/intsig/office/simpletext/model/IElement;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;
    .locals 4

    .line 1
    iget-object p1, p0, Lcom/intsig/office/simpletext/model/ParagraphElement;->leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->size()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    :goto_0
    if-ge v1, p1, :cond_0

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/office/simpletext/model/ParagraphElement;->leaf:Lcom/intsig/office/simpletext/model/ElementCollectionImpl;

    .line 16
    .line 17
    invoke-virtual {v2, v1}, Lcom/intsig/office/simpletext/model/ElementCollectionImpl;->getElementForIndex(I)Lcom/intsig/office/simpletext/model/IElement;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    const/4 v3, 0x0

    .line 22
    invoke-interface {v2, v3}, Lcom/intsig/office/simpletext/model/IElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    add-int/lit8 v1, v1, 0x1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    return-object p1
    .line 37
    .line 38
.end method

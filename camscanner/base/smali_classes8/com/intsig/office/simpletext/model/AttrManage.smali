.class public Lcom/intsig/office/simpletext/model/AttrManage;
.super Ljava/lang/Object;
.source "AttrManage.java"


# static fields
.field public static am:Lcom/intsig/office/simpletext/model/AttrManage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/simpletext/model/AttrManage;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/AttrManage;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/simpletext/model/AttrManage;->am:Lcom/intsig/office/simpletext/model/AttrManage;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static instance()Lcom/intsig/office/simpletext/model/AttrManage;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/office/simpletext/model/AttrManage;->am:Lcom/intsig/office/simpletext/model/AttrManage;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public fillCharAttr(Lcom/intsig/office/simpletext/view/CharAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/CharAttr;->reset()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontName(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    iput v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->fontIndex:I

    .line 9
    .line 10
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    iput v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->fontSize:I

    .line 15
    .line 16
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontScale(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    iput v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->fontScale:I

    .line 21
    .line 22
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    iput v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->fontColor:I

    .line 27
    .line 28
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontBold(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    iput-boolean v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isBold:Z

    .line 33
    .line 34
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontItalic(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    iput-boolean v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isItalic:Z

    .line 39
    .line 40
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    iput-boolean v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isStrikeThrough:Z

    .line 45
    .line 46
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontDoubleStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    iput-boolean v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->isDoubleStrikeThrough:Z

    .line 51
    .line 52
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    iput v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->underlineType:I

    .line 57
    .line 58
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontUnderlineColor(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    iput v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->underlineColor:I

    .line 63
    .line 64
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontScript(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    int-to-short v0, v0

    .line 69
    iput-short v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->subSuperScriptType:S

    .line 70
    .line 71
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontHighLight(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    iput v0, p1, Lcom/intsig/office/simpletext/view/CharAttr;->highlightedColor:I

    .line 76
    .line 77
    invoke-virtual {p0, p2, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontEncloseChanacterType(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 78
    .line 79
    .line 80
    move-result p2

    .line 81
    int-to-byte p2, p2

    .line 82
    iput-byte p2, p1, Lcom/intsig/office/simpletext/view/CharAttr;->encloseType:B

    .line 83
    .line 84
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontPageNumberType(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 85
    .line 86
    .line 87
    move-result p2

    .line 88
    int-to-byte p2, p2

    .line 89
    iput-byte p2, p1, Lcom/intsig/office/simpletext/view/CharAttr;->pageNumberType:B

    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public fillPageAttr(Lcom/intsig/office/simpletext/view/PageAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/view/PageAttr;->reset()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)B

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    iput-byte v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->verticalAlign:B

    .line 9
    .line 10
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)B

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    iput-byte v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->horizontalAlign:B

    .line 15
    .line 16
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    int-to-float v0, v0

    .line 21
    const v1, 0x3d888889

    .line 22
    .line 23
    .line 24
    mul-float v0, v0, v1

    .line 25
    .line 26
    float-to-int v0, v0

    .line 27
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 28
    .line 29
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    int-to-float v0, v0

    .line 34
    mul-float v0, v0, v1

    .line 35
    .line 36
    float-to-int v0, v0

    .line 37
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 38
    .line 39
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    int-to-float v0, v0

    .line 44
    mul-float v0, v0, v1

    .line 45
    .line 46
    float-to-int v0, v0

    .line 47
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 48
    .line 49
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    int-to-float v0, v0

    .line 54
    mul-float v0, v0, v1

    .line 55
    .line 56
    float-to-int v0, v0

    .line 57
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 58
    .line 59
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    int-to-float v0, v0

    .line 64
    mul-float v0, v0, v1

    .line 65
    .line 66
    float-to-int v0, v0

    .line 67
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 68
    .line 69
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    int-to-float v0, v0

    .line 74
    mul-float v0, v0, v1

    .line 75
    .line 76
    float-to-int v0, v0

    .line 77
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 78
    .line 79
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageHeaderMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    int-to-float v0, v0

    .line 84
    mul-float v0, v0, v1

    .line 85
    .line 86
    float-to-int v0, v0

    .line 87
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->headerMargin:I

    .line 88
    .line 89
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageFooterMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    int-to-float v0, v0

    .line 94
    mul-float v0, v0, v1

    .line 95
    .line 96
    float-to-int v0, v0

    .line 97
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->footerMargin:I

    .line 98
    .line 99
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageBackgroundColor(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->pageBRColor:I

    .line 104
    .line 105
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    iput v0, p1, Lcom/intsig/office/simpletext/view/PageAttr;->pageBorder:I

    .line 110
    .line 111
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getPageLinePitch(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 112
    .line 113
    .line 114
    move-result p2

    .line 115
    int-to-float p2, p2

    .line 116
    mul-float p2, p2, v1

    .line 117
    .line 118
    iput p2, p1, Lcom/intsig/office/simpletext/view/PageAttr;->pageLinePitch:F

    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public fillParaAttr(Lcom/intsig/office/system/IControl;Lcom/intsig/office/simpletext/view/ParaAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/office/simpletext/view/ParaAttr;->reset()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaTabsClearPostion(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    int-to-float v0, v0

    .line 9
    const v1, 0x3d888889

    .line 10
    .line 11
    .line 12
    mul-float v0, v0, v1

    .line 13
    .line 14
    float-to-int v0, v0

    .line 15
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->tabClearPosition:I

    .line 16
    .line 17
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    int-to-float v0, v0

    .line 22
    mul-float v0, v0, v1

    .line 23
    .line 24
    float-to-int v0, v0

    .line 25
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->leftIndent:I

    .line 26
    .line 27
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    int-to-float v0, v0

    .line 32
    mul-float v0, v0, v1

    .line 33
    .line 34
    float-to-int v0, v0

    .line 35
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->rightIndent:I

    .line 36
    .line 37
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    int-to-float v0, v0

    .line 42
    mul-float v0, v0, v1

    .line 43
    .line 44
    float-to-int v0, v0

    .line 45
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->beforeSpace:I

    .line 46
    .line 47
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    int-to-float v0, v0

    .line 52
    mul-float v0, v0, v1

    .line 53
    .line 54
    float-to-int v0, v0

    .line 55
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->afterSpace:I

    .line 56
    .line 57
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-byte v0, v0

    .line 62
    iput-byte v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->lineSpaceType:B

    .line 63
    .line 64
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;)F

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->lineSpaceValue:F

    .line 69
    .line 70
    iget-byte v2, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->lineSpaceType:B

    .line 71
    .line 72
    const/4 v3, 0x3

    .line 73
    if-eq v2, v3, :cond_0

    .line 74
    .line 75
    const/4 v3, 0x4

    .line 76
    if-ne v2, v3, :cond_1

    .line 77
    .line 78
    :cond_0
    mul-float v0, v0, v1

    .line 79
    .line 80
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->lineSpaceValue:F

    .line 81
    .line 82
    :cond_1
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    int-to-byte v0, v0

    .line 87
    iput-byte v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->horizontalAlignment:B

    .line 88
    .line 89
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    int-to-float v0, v0

    .line 94
    mul-float v0, v0, v1

    .line 95
    .line 96
    float-to-int v0, v0

    .line 97
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->specialIndentValue:I

    .line 98
    .line 99
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    iput v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listID:I

    .line 104
    .line 105
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getParaListLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    int-to-byte v0, v0

    .line 110
    iput-byte v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    .line 111
    .line 112
    invoke-virtual {p0, p3}, Lcom/intsig/office/simpletext/model/AttrManage;->getPGParaBulletID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 113
    .line 114
    .line 115
    move-result p3

    .line 116
    iput p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->pgBulletID:I

    .line 117
    .line 118
    iget p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listID:I

    .line 119
    .line 120
    if-ltz p3, :cond_8

    .line 121
    .line 122
    iget-byte p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    .line 123
    .line 124
    if-ltz p3, :cond_8

    .line 125
    .line 126
    if-eqz p1, :cond_8

    .line 127
    .line 128
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    invoke-virtual {p1}, Lcom/intsig/office/system/SysKit;->getListManage()Lcom/intsig/office/common/bulletnumber/ListManage;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    iget p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listID:I

    .line 137
    .line 138
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 139
    .line 140
    .line 141
    move-result-object p3

    .line 142
    invoke-virtual {p1, p3}, Lcom/intsig/office/common/bulletnumber/ListManage;->getListData(Ljava/lang/Integer;)Lcom/intsig/office/common/bulletnumber/ListData;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    if-eqz p1, :cond_8

    .line 147
    .line 148
    iget-byte p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listLevel:B

    .line 149
    .line 150
    invoke-virtual {p1, p3}, Lcom/intsig/office/common/bulletnumber/ListData;->getLevel(I)Lcom/intsig/office/common/bulletnumber/ListLevel;

    .line 151
    .line 152
    .line 153
    move-result-object p1

    .line 154
    if-eqz p1, :cond_8

    .line 155
    .line 156
    invoke-virtual {p1}, Lcom/intsig/office/common/bulletnumber/ListLevel;->getTextIndent()I

    .line 157
    .line 158
    .line 159
    move-result p3

    .line 160
    int-to-float p3, p3

    .line 161
    mul-float p3, p3, v1

    .line 162
    .line 163
    float-to-int p3, p3

    .line 164
    iput p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listTextIndent:I

    .line 165
    .line 166
    invoke-virtual {p1}, Lcom/intsig/office/common/bulletnumber/ListLevel;->getSpecialIndent()I

    .line 167
    .line 168
    .line 169
    move-result p1

    .line 170
    int-to-float p1, p1

    .line 171
    mul-float p1, p1, v1

    .line 172
    .line 173
    float-to-int p1, p1

    .line 174
    add-int/2addr p3, p1

    .line 175
    iput p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listAlignIndent:I

    .line 176
    .line 177
    iget p1, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->leftIndent:I

    .line 178
    .line 179
    iget v0, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listTextIndent:I

    .line 180
    .line 181
    sub-int v1, p1, v0

    .line 182
    .line 183
    const/4 v2, 0x0

    .line 184
    if-eqz v1, :cond_5

    .line 185
    .line 186
    if-nez p1, :cond_2

    .line 187
    .line 188
    goto :goto_1

    .line 189
    :cond_2
    add-int/2addr p1, p3

    .line 190
    if-ne p1, v0, :cond_3

    .line 191
    .line 192
    iput p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->leftIndent:I

    .line 193
    .line 194
    :cond_3
    iget p1, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->specialIndentValue:I

    .line 195
    .line 196
    if-ltz p1, :cond_4

    .line 197
    .line 198
    iput p1, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listAlignIndent:I

    .line 199
    .line 200
    goto :goto_0

    .line 201
    :cond_4
    iput v2, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listAlignIndent:I

    .line 202
    .line 203
    :goto_0
    if-nez p1, :cond_8

    .line 204
    .line 205
    iget p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->leftIndent:I

    .line 206
    .line 207
    sub-int v1, v0, p3

    .line 208
    .line 209
    if-lez v1, :cond_8

    .line 210
    .line 211
    sub-int/2addr v0, p3

    .line 212
    sub-int/2addr p1, v0

    .line 213
    iput p1, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->specialIndentValue:I

    .line 214
    .line 215
    goto :goto_2

    .line 216
    :cond_5
    :goto_1
    iget p1, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->specialIndentValue:I

    .line 217
    .line 218
    if-nez p1, :cond_6

    .line 219
    .line 220
    sub-int/2addr v0, p3

    .line 221
    neg-int p1, v0

    .line 222
    iput p1, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->specialIndentValue:I

    .line 223
    .line 224
    :cond_6
    iget p1, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->specialIndentValue:I

    .line 225
    .line 226
    if-gez p1, :cond_7

    .line 227
    .line 228
    iput p3, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->leftIndent:I

    .line 229
    .line 230
    iput v2, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->listAlignIndent:I

    .line 231
    .line 232
    goto :goto_2

    .line 233
    :cond_7
    if-le p3, p1, :cond_8

    .line 234
    .line 235
    add-int/2addr p1, p3

    .line 236
    iput p1, p2, Lcom/intsig/office/simpletext/view/ParaAttr;->specialIndentValue:I

    .line 237
    .line 238
    :cond_8
    :goto_2
    return-void
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method public fillTableAttr(Lcom/intsig/office/simpletext/view/TableAttr;Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p1, Lcom/intsig/office/simpletext/view/TableAttr;->topMargin:I

    .line 3
    .line 4
    const/4 v1, 0x7

    .line 5
    iput v1, p1, Lcom/intsig/office/simpletext/view/TableAttr;->leftMargin:I

    .line 6
    .line 7
    iput v1, p1, Lcom/intsig/office/simpletext/view/TableAttr;->rightMargin:I

    .line 8
    .line 9
    iput v0, p1, Lcom/intsig/office/simpletext/view/TableAttr;->bottomMargin:I

    .line 10
    .line 11
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getTableCellWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    int-to-float v0, v0

    .line 16
    const v1, 0x3d888889

    .line 17
    .line 18
    .line 19
    mul-float v0, v0, v1

    .line 20
    .line 21
    float-to-int v0, v0

    .line 22
    iput v0, p1, Lcom/intsig/office/simpletext/view/TableAttr;->cellWidth:I

    .line 23
    .line 24
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getTableCellVerAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    int-to-byte v0, v0

    .line 29
    iput-byte v0, p1, Lcom/intsig/office/simpletext/view/TableAttr;->cellVerticalAlign:B

    .line 30
    .line 31
    invoke-virtual {p0, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getTableCellTableBackground(Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 32
    .line 33
    .line 34
    move-result p2

    .line 35
    iput p2, p1, Lcom/intsig/office/simpletext/view/TableAttr;->cellBackground:I

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontBold(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 3

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 3
    .line 4
    .line 5
    move-result p2

    .line 6
    const/4 v1, 0x0

    .line 7
    const/high16 v2, -0x80000000

    .line 8
    .line 9
    if-ne p2, v2, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-ne p2, v2, :cond_0

    .line 16
    .line 17
    return v1

    .line 18
    :cond_0
    const/4 p1, 0x1

    .line 19
    if-ne p2, p1, :cond_1

    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    :cond_1
    return v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 2

    .line 1
    const/4 v0, 0x3

    .line 2
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 3
    .line 4
    .line 5
    move-result p2

    .line 6
    const/high16 v1, -0x80000000

    .line 7
    .line 8
    if-ne p2, v1, :cond_0

    .line 9
    .line 10
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    if-ne p2, v1, :cond_0

    .line 15
    .line 16
    const/high16 p1, -0x1000000

    .line 17
    .line 18
    return p1

    .line 19
    :cond_0
    return p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontDoubleStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 3

    .line 1
    const/4 v0, 0x7

    .line 2
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 3
    .line 4
    .line 5
    move-result p2

    .line 6
    const/4 v1, 0x0

    .line 7
    const/high16 v2, -0x80000000

    .line 8
    .line 9
    if-ne p2, v2, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-ne p2, v2, :cond_0

    .line 16
    .line 17
    return v1

    .line 18
    :cond_0
    const/4 p1, 0x1

    .line 19
    if-ne p2, p1, :cond_1

    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    :cond_1
    return v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontEncloseChanacterType(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 0

    .line 1
    const/16 p1, 0x10

    .line 2
    .line 3
    invoke-interface {p2, p1}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 p2, -0x80000000

    .line 8
    .line 9
    if-ne p1, p2, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontHighLight(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 2

    .line 1
    const/16 v0, 0xb

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const/high16 v1, -0x80000000

    .line 8
    .line 9
    if-ne p2, v1, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-ne p2, v1, :cond_0

    .line 16
    .line 17
    const/4 p1, -0x1

    .line 18
    return p1

    .line 19
    :cond_0
    return p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontItalic(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 3

    .line 1
    const/4 v0, 0x5

    .line 2
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 3
    .line 4
    .line 5
    move-result p2

    .line 6
    const/4 v1, 0x0

    .line 7
    const/high16 v2, -0x80000000

    .line 8
    .line 9
    if-ne p2, v2, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-ne p2, v2, :cond_0

    .line 16
    .line 17
    return v1

    .line 18
    :cond_0
    const/4 p1, 0x1

    .line 19
    if-ne p2, p1, :cond_1

    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    :cond_1
    return v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontName(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 2

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 3
    .line 4
    .line 5
    move-result p2

    .line 6
    const/high16 v1, -0x80000000

    .line 7
    .line 8
    if-ne p2, v1, :cond_0

    .line 9
    .line 10
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    if-ne p2, v1, :cond_0

    .line 15
    .line 16
    const/4 p1, -0x1

    .line 17
    return p1

    .line 18
    :cond_0
    return p2
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontPageNumberType(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0xf

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getFontScale(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 2

    .line 1
    const/16 v0, 0xe

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const/high16 v1, -0x80000000

    .line 8
    .line 9
    if-ne p2, v1, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-ne p2, v1, :cond_0

    .line 16
    .line 17
    const/16 p1, 0x64

    .line 18
    .line 19
    return p1

    .line 20
    :cond_0
    return p2
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontScript(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 2

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const/high16 v1, -0x80000000

    .line 8
    .line 9
    if-ne p2, v1, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-ne p2, v1, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x0

    .line 18
    return p1

    .line 19
    :cond_0
    return p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 3
    .line 4
    .line 5
    move-result p2

    .line 6
    const/high16 v1, -0x80000000

    .line 7
    .line 8
    if-ne p2, v1, :cond_0

    .line 9
    .line 10
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    if-ne p2, v1, :cond_0

    .line 15
    .line 16
    const/16 p1, 0xc

    .line 17
    .line 18
    return p1

    .line 19
    :cond_0
    return p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 3

    .line 1
    const/4 v0, 0x6

    .line 2
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 3
    .line 4
    .line 5
    move-result p2

    .line 6
    const/4 v1, 0x0

    .line 7
    const/high16 v2, -0x80000000

    .line 8
    .line 9
    if-ne p2, v2, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-ne p2, v2, :cond_0

    .line 16
    .line 17
    return v1

    .line 18
    :cond_0
    const/4 p1, 0x1

    .line 19
    if-ne p2, p1, :cond_1

    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    :cond_1
    return v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 3
    .line 4
    .line 5
    move-result p1

    .line 6
    const/high16 v0, -0x80000000

    .line 7
    .line 8
    if-ne p1, v0, :cond_0

    .line 9
    .line 10
    const/4 p1, -0x1

    .line 11
    :cond_0
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 2

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const/high16 v1, -0x80000000

    .line 8
    .line 9
    if-ne p2, v1, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    if-ne p2, v1, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x0

    .line 18
    return p1

    .line 19
    :cond_0
    return p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFontUnderlineColor(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 3

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    invoke-interface {p2, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/high16 v2, -0x80000000

    .line 8
    .line 9
    if-ne v1, v2, :cond_0

    .line 10
    .line 11
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-ne v1, v2, :cond_0

    .line 16
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/simpletext/model/AttrManage;->getFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;Lcom/intsig/office/simpletext/model/IAttributeSet;)I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    return p1

    .line 22
    :cond_0
    return v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getHperlinkID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0xc

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPGParaBulletID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x100e

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageBackgroundColor(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x200a

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x200b

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageFooterMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x2008

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/16 p1, 0x352

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageHeaderMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x2007

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/16 p1, 0x352

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x2001

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/16 p1, 0x4b0

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)B
    .locals 1

    .line 1
    const/16 v0, 0x2009

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    return p1

    .line 13
    :cond_0
    int-to-byte p1, p1

    .line 14
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageLinePitch(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x200c

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x2005

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/16 p1, 0x5a0

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x2002

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/16 p1, 0x708

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x2003

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/16 p1, 0x708

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x2004

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/16 p1, 0x5a0

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)B
    .locals 1

    .line 1
    const/16 v0, 0x2006

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    return p1

    .line 13
    :cond_0
    int-to-byte p1, p1

    .line 14
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x2000

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/16 p1, 0x3e8

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1005

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1004

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1006

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaIndentInitLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1002

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1001

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1003

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x100b

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;)F
    .locals 1

    .line 1
    const/16 v0, 0x1009

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/high16 p1, 0x3f800000    # 1.0f

    .line 12
    .line 13
    return p1

    .line 14
    :cond_0
    int-to-float p1, p1

    .line 15
    const/high16 v0, 0x42c80000    # 100.0f

    .line 16
    .line 17
    div-float/2addr p1, v0

    .line 18
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x100a

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x100d

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaListLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x100c

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1008

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1000

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaTabsClearPostion(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x100f

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getParaVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x1007

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0xd

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableBottomBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3002

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableBottomBorderColor(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3003

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/high16 p1, -0x1000000

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableBottomMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3012

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableCellTableBackground(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x200a

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableCellVerAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3010

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableCellWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3009

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableLeftBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3004

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableLeftBorderColor(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3005

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/high16 p1, -0x1000000

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableLeftMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3013

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableRightBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3006

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableRightBorderColor(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3007

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/high16 p1, -0x1000000

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableRightMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3014

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableRowHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3008

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableTopBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3000

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableTopBorderColor(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3001

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/high16 p1, -0x1000000

    .line 12
    .line 13
    :cond_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getTableTopMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;)I
    .locals 1

    .line 1
    const/16 v0, 0x3011

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    :cond_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public hasAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;S)Z
    .locals 0

    .line 1
    invoke-interface {p1, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/high16 p2, -0x80000000

    .line 6
    .line 7
    if-eq p1, p2, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public isTableHeaderRow(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 2

    .line 1
    const/16 v0, 0x300a

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    if-ne p1, v1, :cond_1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    const/4 v1, 0x0

    .line 17
    :goto_0
    return v1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public isTableHorFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 2

    .line 1
    const/16 v0, 0x300c

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    if-ne p1, v0, :cond_1

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    :cond_1
    return v1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public isTableHorMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 2

    .line 1
    const/16 v0, 0x300d

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    if-ne p1, v0, :cond_1

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    :cond_1
    return v1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public isTableRowSplit(Lcom/intsig/office/simpletext/model/IAttributeSet;I)Z
    .locals 1

    .line 1
    const/16 p2, 0x300b

    .line 2
    .line 3
    invoke-interface {p1, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 p2, -0x80000000

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    if-ne p1, p2, :cond_0

    .line 11
    .line 12
    return v0

    .line 13
    :cond_0
    if-ne p1, v0, :cond_1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    const/4 v0, 0x0

    .line 17
    :goto_0
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public isTableVerFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 2

    .line 1
    const/16 v0, 0x300e

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    if-ne p1, v0, :cond_1

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    :cond_1
    return v1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public isTableVerMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;)Z
    .locals 2

    .line 1
    const/16 v0, 0x300f

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/office/simpletext/model/IAttributeSet;->getAttribute(S)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/high16 v0, -0x80000000

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    if-ne p1, v0, :cond_1

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    :cond_1
    return v1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setEncloseChanacterType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x10

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontBold(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/4 v0, 0x3

    .line 2
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontDoubleStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/4 v0, 0x7

    .line 2
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontHighLight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0xb

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontItalic(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontName(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontPageNumberType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0xf

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontScale(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0xe

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontScript(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontSize(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontStrike(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/4 v0, 0x6

    .line 2
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontUnderline(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setFontUnderlineColr(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setHyperlinkID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0xc

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPGParaBulletID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x100e

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageBackgroundColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x200a

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x200b

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageFooterMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x2008

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageHeaderMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x2007

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x2001

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V
    .locals 1

    .line 1
    const/16 v0, 0x2009

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageLinePitch(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x200c

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageMarginBottom(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x2005

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageMarginLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x2002

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageMarginRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x2003

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageMarginTop(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x2004

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;B)V
    .locals 1

    .line 1
    const/16 v0, 0x2006

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setPageWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x2000

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaAfter(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1005

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaBefore(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1004

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaHorizontalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1006

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaIndentInitLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1002

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaIndentLeft(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1001

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaIndentRight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1003

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x100b

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaLineSpace(Lcom/intsig/office/simpletext/model/IAttributeSet;F)V
    .locals 1

    .line 1
    const/high16 v0, 0x42c80000    # 100.0f

    .line 2
    .line 3
    mul-float p2, p2, v0

    .line 4
    .line 5
    float-to-int p2, p2

    .line 6
    const/16 v0, 0x1009

    .line 7
    .line 8
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaLineSpaceType(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x100a

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaListID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x100d

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaListLevel(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x100c

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaSpecialIndent(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1008

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaStyleID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1000

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaTabsClearPostion(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x100f

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setParaVerticalAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x1007

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setShapeID(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0xd

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableBottomBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3002

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableBottomBorderColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3003

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableBottomMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3012

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableCellBackground(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x200a

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableCellVerAlign(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3010

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableCellWidth(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3009

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableHeaderRow(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/16 v0, 0x300a

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableHorFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/16 v0, 0x300c

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableHorMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/16 v0, 0x300d

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableLeftBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3004

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableLeftBorderColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3005

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableLeftMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3013

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableRightBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3006

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableRightBorderColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3007

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableRightMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3014

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableRowHeight(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3008

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableRowSplit(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/16 v0, 0x300b

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableTopBorder(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3000

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableTopBorderColor(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3001

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableTopMargin(Lcom/intsig/office/simpletext/model/IAttributeSet;I)V
    .locals 1

    .line 1
    const/16 v0, 0x3011

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableVerFirstMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/16 v0, 0x300e

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTableVerMerged(Lcom/intsig/office/simpletext/model/IAttributeSet;Z)V
    .locals 1

    .line 1
    const/16 v0, 0x300f

    .line 2
    .line 3
    invoke-interface {p1, v0, p2}, Lcom/intsig/office/simpletext/model/IAttributeSet;->setAttribute(SI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

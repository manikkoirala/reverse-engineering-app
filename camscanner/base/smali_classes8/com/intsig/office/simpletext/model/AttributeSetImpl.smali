.class public Lcom/intsig/office/simpletext/model/AttributeSetImpl;
.super Ljava/lang/Object;
.source "AttributeSetImpl.java"

# interfaces
.implements Lcom/intsig/office/simpletext/model/IAttributeSet;


# static fields
.field public static final CAPACITY:I = 0x5


# instance fields
.field private ID:I

.field private arrayID:[S

.field private arrayValue:[I

.field private size:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 6
    .line 7
    const/16 v0, 0xa

    .line 8
    .line 9
    new-array v1, v0, [S

    .line 10
    .line 11
    iput-object v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 12
    .line 13
    new-array v0, v0, [I

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private ensureCapacity()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, 0x5

    .line 4
    .line 5
    new-array v2, v1, [S

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-static {v3, v4, v2, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11
    .line 12
    .line 13
    iput-object v2, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 14
    .line 15
    new-array v0, v1, [I

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 18
    .line 19
    iget v2, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 20
    .line 21
    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private getAttribute(SZ)I
    .locals 3

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getIDIndex(I)I

    move-result v0

    if-ltz v0, :cond_0

    .line 3
    iget-object p1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    aget p1, p1, v0

    return p1

    :cond_0
    const/high16 v0, -0x80000000

    if-nez p2, :cond_1

    return v0

    :cond_1
    const/16 p2, 0xfff

    if-ge p1, p2, :cond_2

    const/4 p2, 0x0

    .line 4
    invoke-direct {p0, p2}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getIDIndex(I)I

    move-result p2

    if-ltz p2, :cond_2

    .line 5
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    move-result-object v1

    iget-object v2, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    aget p2, v2, p2

    invoke-virtual {v1, p2}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    move-result-object p2

    .line 6
    invoke-direct {p0, p2, p1}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getAttributeForStyle(Lcom/intsig/office/simpletext/model/Style;S)I

    move-result p2

    goto :goto_0

    :cond_2
    const/high16 p2, -0x80000000

    :goto_0
    if-eq p2, v0, :cond_3

    return p2

    :cond_3
    const/16 v0, 0x1000

    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getIDIndex(I)I

    move-result v0

    if-ltz v0, :cond_4

    .line 8
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    move-result-object p2

    iget-object v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    aget v0, v1, v0

    invoke-virtual {p2, v0}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    move-result-object p2

    .line 9
    invoke-direct {p0, p2, p1}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getAttributeForStyle(Lcom/intsig/office/simpletext/model/Style;S)I

    move-result p2

    :cond_4
    return p2
.end method

.method private getAttributeForStyle(Lcom/intsig/office/simpletext/model/Style;S)I
    .locals 3

    .line 1
    const/high16 v0, -0x80000000

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    return v0

    .line 6
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/Style;->getAttrbuteSet()Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    check-cast v1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-direct {v1, p2, v2}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getAttribute(SZ)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eq v1, v0, :cond_1

    .line 18
    .line 19
    return v1

    .line 20
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/Style;->getBaseID()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-ltz v1, :cond_2

    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/office/simpletext/model/StyleManage;->instance()Lcom/intsig/office/simpletext/model/StyleManage;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/Style;->getBaseID()I

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    invoke-virtual {v0, p1}, Lcom/intsig/office/simpletext/model/StyleManage;->getStyle(I)Lcom/intsig/office/simpletext/model/Style;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getAttributeForStyle(Lcom/intsig/office/simpletext/model/Style;S)I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    return p1

    .line 43
    :cond_2
    return v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private getIDIndex(I)I
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    iget v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 3
    .line 4
    if-ge v0, v1, :cond_1

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 7
    .line 8
    aget-short v1, v1, v0

    .line 9
    .line 10
    if-ne v1, p1, :cond_0

    .line 11
    .line 12
    return v0

    .line 13
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    const/4 p1, -0x1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public clone()Lcom/intsig/office/simpletext/model/IAttributeSet;
    .locals 5

    .line 2
    new-instance v0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    invoke-direct {v0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;-><init>()V

    .line 3
    iget v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    iput v1, v0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 4
    iget v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    new-array v2, v1, [S

    .line 5
    iget-object v3, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    const/4 v4, 0x0

    invoke-static {v3, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6
    iput-object v2, v0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 7
    iget v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    new-array v2, v1, [I

    .line 8
    iget-object v3, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    invoke-static {v3, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 9
    iput-object v2, v0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->clone()Lcom/intsig/office/simpletext/model/IAttributeSet;

    move-result-object v0

    return-object v0
.end method

.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getAttribute(S)I
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getAttribute(SZ)I

    move-result p1

    return p1
.end method

.method public getID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->ID:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public mergeAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 5

    .line 1
    instance-of v0, p1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;

    .line 7
    .line 8
    iget-object v0, p1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 9
    .line 10
    array-length v0, v0

    .line 11
    const/4 v1, 0x0

    .line 12
    :goto_0
    if-ge v1, v0, :cond_3

    .line 13
    .line 14
    iget-object v2, p1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 15
    .line 16
    aget-short v2, v2, v1

    .line 17
    .line 18
    invoke-direct {p0, v2}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getIDIndex(I)I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-lez v2, :cond_1

    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 25
    .line 26
    iget-object v4, p1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 27
    .line 28
    aget v4, v4, v1

    .line 29
    .line 30
    aput v4, v3, v2

    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_1
    iget v2, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 34
    .line 35
    iget-object v3, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 36
    .line 37
    array-length v3, v3

    .line 38
    if-lt v2, v3, :cond_2

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->ensureCapacity()V

    .line 41
    .line 42
    .line 43
    :cond_2
    iget-object v2, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 44
    .line 45
    iget v3, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 46
    .line 47
    iget-object v4, p1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 48
    .line 49
    aget-short v4, v4, v1

    .line 50
    .line 51
    aput-short v4, v2, v3

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 54
    .line 55
    iget-object v4, p1, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 56
    .line 57
    aget v4, v4, v1

    .line 58
    .line 59
    aput v4, v2, v3

    .line 60
    .line 61
    add-int/lit8 v3, v3, 0x1

    .line 62
    .line 63
    iput v3, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 64
    .line 65
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_3
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public removeAttribute(S)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getIDIndex(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-ltz p1, :cond_1

    .line 6
    .line 7
    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 10
    .line 11
    if-ge p1, v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 14
    .line 15
    add-int/lit8 v1, p1, -0x1

    .line 16
    .line 17
    aget-short v2, v0, p1

    .line 18
    .line 19
    aput-short v2, v0, v1

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 22
    .line 23
    aget v2, v0, p1

    .line 24
    .line 25
    aput v2, v0, v1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 29
    .line 30
    iput v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 31
    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setAttribute(SI)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 4
    .line 5
    array-length v1, v1

    .line 6
    if-lt v0, v1, :cond_0

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->ensureCapacity()V

    .line 9
    .line 10
    .line 11
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->getIDIndex(I)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-ltz v0, :cond_1

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 18
    .line 19
    aput p2, p1, v0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayID:[S

    .line 23
    .line 24
    iget v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 25
    .line 26
    aput-short p1, v0, v1

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->arrayValue:[I

    .line 29
    .line 30
    aput p2, p1, v1

    .line 31
    .line 32
    add-int/lit8 v1, v1, 0x1

    .line 33
    .line 34
    iput v1, p0, Lcom/intsig/office/simpletext/model/AttributeSetImpl;->size:I

    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

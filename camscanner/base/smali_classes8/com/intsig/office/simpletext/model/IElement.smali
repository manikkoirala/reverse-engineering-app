.class public interface abstract Lcom/intsig/office/simpletext/model/IElement;
.super Ljava/lang/Object;
.source "IElement.java"


# virtual methods
.method public abstract dispose()V
.end method

.method public abstract getAttribute()Lcom/intsig/office/simpletext/model/IAttributeSet;
.end method

.method public abstract getEndOffset()J
.end method

.method public abstract getStartOffset()J
.end method

.method public abstract getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;
.end method

.method public abstract getType()S
.end method

.method public abstract setAttribute(Lcom/intsig/office/simpletext/model/IAttributeSet;)V
.end method

.method public abstract setEndOffset(J)V
.end method

.method public abstract setStartOffset(J)V
.end method

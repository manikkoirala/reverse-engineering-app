.class public interface abstract Lcom/intsig/office/simpletext/view/IView;
.super Ljava/lang/Object;
.source "IView.java"


# virtual methods
.method public abstract appendChlidView(Lcom/intsig/office/simpletext/view/IView;)V
.end method

.method public abstract contains(IIZ)Z
.end method

.method public abstract contains(JZ)Z
.end method

.method public abstract deleteView(Lcom/intsig/office/simpletext/view/IView;Z)V
.end method

.method public abstract dispose()V
.end method

.method public abstract doLayout(IIIIJI)I
.end method

.method public abstract draw(Landroid/graphics/Canvas;IIF)V
.end method

.method public abstract free()V
.end method

.method public abstract getBottomIndent()I
.end method

.method public abstract getChildCount()I
.end method

.method public abstract getChildView()Lcom/intsig/office/simpletext/view/IView;
.end method

.method public abstract getContainer()Lcom/intsig/office/simpletext/control/IWord;
.end method

.method public abstract getControl()Lcom/intsig/office/system/IControl;
.end method

.method public abstract getDocument()Lcom/intsig/office/simpletext/model/IDocument;
.end method

.method public abstract getElemEnd(Lcom/intsig/office/simpletext/model/IDocument;)J
.end method

.method public abstract getElemStart(Lcom/intsig/office/simpletext/model/IDocument;)J
.end method

.method public abstract getElement()Lcom/intsig/office/simpletext/model/IElement;
.end method

.method public abstract getEndOffset(Lcom/intsig/office/simpletext/model/IDocument;)J
.end method

.method public abstract getHeight()I
.end method

.method public abstract getLastView()Lcom/intsig/office/simpletext/view/IView;
.end method

.method public abstract getLayoutSpan(B)I
.end method

.method public abstract getLeftIndent()I
.end method

.method public abstract getNextForCoordinate(JIII)J
.end method

.method public abstract getNextForOffset(JIII)J
.end method

.method public abstract getNextView()Lcom/intsig/office/simpletext/view/IView;
.end method

.method public abstract getParentView()Lcom/intsig/office/simpletext/view/IView;
.end method

.method public abstract getPreView()Lcom/intsig/office/simpletext/view/IView;
.end method

.method public abstract getRightIndent()I
.end method

.method public abstract getStartOffset(Lcom/intsig/office/simpletext/model/IDocument;)J
.end method

.method public abstract getTopIndent()I
.end method

.method public abstract getType()S
.end method

.method public abstract getView(IIIZ)Lcom/intsig/office/simpletext/view/IView;
.end method

.method public abstract getView(JIZ)Lcom/intsig/office/simpletext/view/IView;
.end method

.method public abstract getViewRect(IIF)Landroid/graphics/Rect;
.end method

.method public abstract getWidth()I
.end method

.method public abstract getX()I
.end method

.method public abstract getY()I
.end method

.method public abstract insertView(Lcom/intsig/office/simpletext/view/IView;Lcom/intsig/office/simpletext/view/IView;)V
.end method

.method public abstract intersection(Landroid/graphics/Rect;IIF)Z
.end method

.method public abstract modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;
.end method

.method public abstract setBottomIndent(I)V
.end method

.method public abstract setBound(IIII)V
.end method

.method public abstract setChildView(Lcom/intsig/office/simpletext/view/IView;)V
.end method

.method public abstract setElement(Lcom/intsig/office/simpletext/model/IElement;)V
.end method

.method public abstract setEndOffset(J)V
.end method

.method public abstract setHeight(I)V
.end method

.method public abstract setIndent(IIII)V
.end method

.method public abstract setLeftIndent(I)V
.end method

.method public abstract setLocation(II)V
.end method

.method public abstract setNextView(Lcom/intsig/office/simpletext/view/IView;)V
.end method

.method public abstract setParentView(Lcom/intsig/office/simpletext/view/IView;)V
.end method

.method public abstract setPreView(Lcom/intsig/office/simpletext/view/IView;)V
.end method

.method public abstract setRightIndent(I)V
.end method

.method public abstract setSize(II)V
.end method

.method public abstract setStartOffset(J)V
.end method

.method public abstract setTopIndent(I)V
.end method

.method public abstract setWidth(I)V
.end method

.method public abstract setX(I)V
.end method

.method public abstract setY(I)V
.end method

.method public abstract viewToModel(IIZ)J
.end method

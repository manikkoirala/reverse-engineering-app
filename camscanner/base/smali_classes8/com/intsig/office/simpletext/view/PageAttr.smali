.class public Lcom/intsig/office/simpletext/view/PageAttr;
.super Ljava/lang/Object;
.source "PageAttr.java"


# static fields
.field public static final GRIDTYPE_CHAR:B = 0x3t

.field public static final GRIDTYPE_LINE:B = 0x2t

.field public static final GRIDTYPE_LINE_AND_CHAR:B = 0x1t

.field public static final GRIDTYPE_NONE:B


# instance fields
.field public bottomMargin:I

.field public footerMargin:I

.field public headerMargin:I

.field public horizontalAlign:B

.field public leftMargin:I

.field public pageBRColor:I

.field public pageBorder:I

.field public pageHeight:I

.field public pageLinePitch:F

.field public pageWidth:I

.field public rightMargin:I

.field public topMargin:I

.field public verticalAlign:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public reset()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-byte v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->verticalAlign:B

    .line 3
    .line 4
    iput-byte v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->horizontalAlign:B

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->pageWidth:I

    .line 7
    .line 8
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->pageHeight:I

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->topMargin:I

    .line 11
    .line 12
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->bottomMargin:I

    .line 13
    .line 14
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->leftMargin:I

    .line 15
    .line 16
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->rightMargin:I

    .line 17
    .line 18
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->headerMargin:I

    .line 19
    .line 20
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->footerMargin:I

    .line 21
    .line 22
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->pageBorder:I

    .line 23
    .line 24
    const/4 v0, -0x1

    .line 25
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->pageBRColor:I

    .line 26
    .line 27
    const/4 v0, 0x0

    .line 28
    iput v0, p0, Lcom/intsig/office/simpletext/view/PageAttr;->pageLinePitch:F

    .line 29
    .line 30
    return-void
.end method

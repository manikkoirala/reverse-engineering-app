.class public Lcom/intsig/office/simpletext/view/CharAttr;
.super Ljava/lang/Object;
.source "CharAttr.java"


# instance fields
.field public encloseType:B

.field public fontColor:I

.field public fontIndex:I

.field public fontScale:I

.field public fontSize:I

.field public highlightedColor:I

.field public isBold:Z

.field public isDoubleStrikeThrough:Z

.field public isItalic:Z

.field public isStrikeThrough:Z

.field public pageNumberType:B

.field public subSuperScriptType:S

.field public underlineColor:I

.field public underlineType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public reset()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/office/simpletext/view/CharAttr;->underlineType:I

    .line 3
    .line 4
    const/4 v1, -0x1

    .line 5
    iput v1, p0, Lcom/intsig/office/simpletext/view/CharAttr;->fontColor:I

    .line 6
    .line 7
    iput v1, p0, Lcom/intsig/office/simpletext/view/CharAttr;->underlineColor:I

    .line 8
    .line 9
    iput-boolean v0, p0, Lcom/intsig/office/simpletext/view/CharAttr;->isStrikeThrough:Z

    .line 10
    .line 11
    iput-boolean v0, p0, Lcom/intsig/office/simpletext/view/CharAttr;->isDoubleStrikeThrough:Z

    .line 12
    .line 13
    iput-short v0, p0, Lcom/intsig/office/simpletext/view/CharAttr;->subSuperScriptType:S

    .line 14
    .line 15
    iput v1, p0, Lcom/intsig/office/simpletext/view/CharAttr;->fontIndex:I

    .line 16
    .line 17
    iput-byte v1, p0, Lcom/intsig/office/simpletext/view/CharAttr;->encloseType:B

    .line 18
    .line 19
    iput-byte v1, p0, Lcom/intsig/office/simpletext/view/CharAttr;->pageNumberType:B

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

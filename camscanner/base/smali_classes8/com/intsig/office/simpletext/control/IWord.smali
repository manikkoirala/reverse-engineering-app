.class public interface abstract Lcom/intsig/office/simpletext/control/IWord;
.super Ljava/lang/Object;
.source "IWord.java"


# virtual methods
.method public abstract dispose()V
.end method

.method public abstract getControl()Lcom/intsig/office/system/IControl;
.end method

.method public abstract getDocument()Lcom/intsig/office/simpletext/model/IDocument;
.end method

.method public abstract getEditType()B
.end method

.method public abstract getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;
.end method

.method public abstract getParagraphAnimation(I)Lcom/intsig/office/pg/animate/IAnimation;
.end method

.method public abstract getText(JJ)Ljava/lang/String;
.end method

.method public abstract getTextBox()Lcom/intsig/office/common/shape/IShape;
.end method

.method public abstract modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;
.end method

.method public abstract viewToModel(IIZ)J
.end method

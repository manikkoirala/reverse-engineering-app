.class public interface abstract Lcom/intsig/office/simpletext/control/IHighlight;
.super Ljava/lang/Object;
.source "IHighlight.java"


# virtual methods
.method public abstract addHighlight(JJ)V
.end method

.method public abstract addHighlights(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/simpletext/model/HighLightWord;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract dispose()V
.end method

.method public abstract draw(Landroid/graphics/Canvas;Lcom/intsig/office/simpletext/view/IView;IIJJF)V
.end method

.method public abstract drawAllHighLight(Landroid/graphics/Canvas;Lcom/intsig/office/simpletext/view/IView;IIJJF)V
.end method

.method public abstract getSelectEnd()J
.end method

.method public abstract getSelectStart()J
.end method

.method public abstract getSelectText()Ljava/lang/String;
.end method

.method public abstract isSelectText()Z
.end method

.method public abstract removeHighlight()V
.end method

.method public abstract setPaintHighlight(Z)V
.end method

.method public abstract setSelectEnd(J)V
.end method

.method public abstract setSelectStart(J)V
.end method

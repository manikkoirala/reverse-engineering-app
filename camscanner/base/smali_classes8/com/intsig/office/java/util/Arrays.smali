.class public Lcom/intsig/office/java/util/Arrays;
.super Ljava/lang/Object;
.source "Arrays.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/java/util/Arrays$ArrayList;
    }
.end annotation


# static fields
.field private static final INSERTIONSORT_THRESHOLD:I = 0x7


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static varargs asList([Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/office/java/util/Arrays$ArrayList;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/java/util/Arrays$ArrayList;-><init>([Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static binarySearch([BB)I
    .locals 2

    .line 13
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([BIIB)I

    move-result p0

    return p0
.end method

.method public static binarySearch([BIIB)I
    .locals 1

    .line 14
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 15
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([BIIB)I

    move-result p0

    return p0
.end method

.method public static binarySearch([CC)I
    .locals 2

    .line 10
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([CIIC)I

    move-result p0

    return p0
.end method

.method public static binarySearch([CIIC)I
    .locals 1

    .line 11
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 12
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([CIIC)I

    move-result p0

    return p0
.end method

.method public static binarySearch([DD)I
    .locals 2

    .line 16
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([DIID)I

    move-result p0

    return p0
.end method

.method public static binarySearch([DIID)I
    .locals 1

    .line 17
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 18
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([DIID)I

    move-result p0

    return p0
.end method

.method public static binarySearch([FF)I
    .locals 2

    .line 19
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([FIIF)I

    move-result p0

    return p0
.end method

.method public static binarySearch([FIIF)I
    .locals 1

    .line 20
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 21
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([FIIF)I

    move-result p0

    return p0
.end method

.method public static binarySearch([II)I
    .locals 2

    .line 4
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([IIII)I

    move-result p0

    return p0
.end method

.method public static binarySearch([IIII)I
    .locals 1

    .line 5
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 6
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([IIII)I

    move-result p0

    return p0
.end method

.method public static binarySearch([JIIJ)I
    .locals 1

    .line 2
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 3
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([JIIJ)I

    move-result p0

    return p0
.end method

.method public static binarySearch([JJ)I
    .locals 2

    .line 1
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([JIIJ)I

    move-result p0

    return p0
.end method

.method public static binarySearch([Ljava/lang/Object;IILjava/lang/Object;)I
    .locals 1

    .line 23
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 24
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([Ljava/lang/Object;IILjava/lang/Object;)I

    move-result p0

    return p0
.end method

.method public static binarySearch([Ljava/lang/Object;IILjava/lang/Object;Ljava/util/Comparator;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;IITT;",
            "Ljava/util/Comparator<",
            "-TT;>;)I"
        }
    .end annotation

    .line 26
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 27
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([Ljava/lang/Object;IILjava/lang/Object;Ljava/util/Comparator;)I

    move-result p0

    return p0
.end method

.method public static binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .line 22
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([Ljava/lang/Object;IILjava/lang/Object;)I

    move-result p0

    return p0
.end method

.method public static binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;",
            "Ljava/util/Comparator<",
            "-TT;>;)I"
        }
    .end annotation

    .line 25
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([Ljava/lang/Object;IILjava/lang/Object;Ljava/util/Comparator;)I

    move-result p0

    return p0
.end method

.method public static binarySearch([SIIS)I
    .locals 1

    .line 8
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 9
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([SIIS)I

    move-result p0

    return p0
.end method

.method public static binarySearch([SS)I
    .locals 2

    .line 7
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([SIIS)I

    move-result p0

    return p0
.end method

.method private static binarySearch0([BIIB)I
    .locals 2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-gt p1, p2, :cond_2

    add-int v0, p1, p2

    ushr-int/lit8 v0, v0, 0x1

    .line 5
    aget-byte v1, p0, v0

    if-ge v1, p3, :cond_0

    add-int/lit8 v0, v0, 0x1

    move p1, v0

    goto :goto_0

    :cond_0
    if-le v1, p3, :cond_1

    add-int/lit8 v0, v0, -0x1

    move p2, v0

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    add-int/lit8 p1, p1, 0x1

    neg-int p0, p1

    return p0
.end method

.method private static binarySearch0([CIIC)I
    .locals 2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-gt p1, p2, :cond_2

    add-int v0, p1, p2

    ushr-int/lit8 v0, v0, 0x1

    .line 4
    aget-char v1, p0, v0

    if-ge v1, p3, :cond_0

    add-int/lit8 v0, v0, 0x1

    move p1, v0

    goto :goto_0

    :cond_0
    if-le v1, p3, :cond_1

    add-int/lit8 v0, v0, -0x1

    move p2, v0

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    add-int/lit8 p1, p1, 0x1

    neg-int p0, p1

    return p0
.end method

.method private static binarySearch0([DIID)I
    .locals 8

    const/4 v0, 0x1

    sub-int/2addr p2, v0

    :goto_0
    if-gt p1, p2, :cond_6

    add-int v1, p1, p2

    ushr-int/2addr v1, v0

    .line 6
    aget-wide v2, p0, v1

    const/4 v4, -0x1

    cmpg-double v5, v2, p3

    if-gez v5, :cond_0

    goto :goto_1

    :cond_0
    cmpl-double v5, v2, p3

    if-lez v5, :cond_2

    :cond_1
    const/4 v4, 0x1

    goto :goto_1

    .line 7
    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 8
    invoke-static {p3, p4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v5

    cmp-long v7, v2, v5

    if-nez v7, :cond_3

    const/4 v2, 0x0

    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    if-gez v7, :cond_1

    :goto_1
    if-gez v4, :cond_4

    add-int/lit8 v1, v1, 0x1

    move p1, v1

    goto :goto_0

    :cond_4
    if-lez v4, :cond_5

    add-int/lit8 v1, v1, -0x1

    move p2, v1

    goto :goto_0

    :cond_5
    return v1

    :cond_6
    add-int/2addr p1, v0

    neg-int p0, p1

    return p0
.end method

.method private static binarySearch0([FIIF)I
    .locals 5

    const/4 v0, 0x1

    sub-int/2addr p2, v0

    :goto_0
    if-gt p1, p2, :cond_6

    add-int v1, p1, p2

    ushr-int/2addr v1, v0

    .line 9
    aget v2, p0, v1

    const/4 v3, -0x1

    cmpg-float v4, v2, p3

    if-gez v4, :cond_0

    goto :goto_1

    :cond_0
    cmpl-float v4, v2, p3

    if-lez v4, :cond_2

    :cond_1
    const/4 v3, 0x1

    goto :goto_1

    .line 10
    :cond_2
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    .line 11
    invoke-static {p3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-ne v2, v4, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    if-ge v2, v4, :cond_1

    :goto_1
    if-gez v3, :cond_4

    add-int/lit8 v1, v1, 0x1

    move p1, v1

    goto :goto_0

    :cond_4
    if-lez v3, :cond_5

    add-int/lit8 v1, v1, -0x1

    move p2, v1

    goto :goto_0

    :cond_5
    return v1

    :cond_6
    add-int/2addr p1, v0

    neg-int p0, p1

    return p0
.end method

.method private static binarySearch0([IIII)I
    .locals 2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-gt p1, p2, :cond_2

    add-int v0, p1, p2

    ushr-int/lit8 v0, v0, 0x1

    .line 2
    aget v1, p0, v0

    if-ge v1, p3, :cond_0

    add-int/lit8 v0, v0, 0x1

    move p1, v0

    goto :goto_0

    :cond_0
    if-le v1, p3, :cond_1

    add-int/lit8 v0, v0, -0x1

    move p2, v0

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    add-int/lit8 p1, p1, 0x1

    neg-int p0, p1

    return p0
.end method

.method private static binarySearch0([JIIJ)I
    .locals 4

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-gt p1, p2, :cond_2

    add-int v0, p1, p2

    ushr-int/lit8 v0, v0, 0x1

    .line 1
    aget-wide v1, p0, v0

    cmp-long v3, v1, p3

    if-gez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    move p1, v0

    goto :goto_0

    :cond_0
    if-lez v3, :cond_1

    add-int/lit8 v0, v0, -0x1

    move p2, v0

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    add-int/lit8 p1, p1, 0x1

    neg-int p0, p1

    return p0
.end method

.method private static binarySearch0([Ljava/lang/Object;IILjava/lang/Object;)I
    .locals 2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-gt p1, p2, :cond_2

    add-int v0, p1, p2

    ushr-int/lit8 v0, v0, 0x1

    .line 12
    aget-object v1, p0, v0

    check-cast v1, Ljava/lang/Comparable;

    .line 13
    invoke-interface {v1, p3}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    move p1, v0

    goto :goto_0

    :cond_0
    if-lez v1, :cond_1

    add-int/lit8 v0, v0, -0x1

    move p2, v0

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    add-int/lit8 p1, p1, 0x1

    neg-int p0, p1

    return p0
.end method

.method private static binarySearch0([Ljava/lang/Object;IILjava/lang/Object;Ljava/util/Comparator;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;IITT;",
            "Ljava/util/Comparator<",
            "-TT;>;)I"
        }
    .end annotation

    if-nez p4, :cond_0

    .line 14
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([Ljava/lang/Object;IILjava/lang/Object;)I

    move-result p0

    return p0

    :cond_0
    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-gt p1, p2, :cond_3

    add-int v0, p1, p2

    ushr-int/lit8 v0, v0, 0x1

    .line 15
    aget-object v1, p0, v0

    .line 16
    invoke-interface {p4, v1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    move p1, v0

    goto :goto_0

    :cond_1
    if-lez v1, :cond_2

    add-int/lit8 v0, v0, -0x1

    move p2, v0

    goto :goto_0

    :cond_2
    return v0

    :cond_3
    add-int/lit8 p1, p1, 0x1

    neg-int p0, p1

    return p0
.end method

.method private static binarySearch0([SIIS)I
    .locals 2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-gt p1, p2, :cond_2

    add-int v0, p1, p2

    ushr-int/lit8 v0, v0, 0x1

    .line 3
    aget-short v1, p0, v0

    if-ge v1, p3, :cond_0

    add-int/lit8 v0, v0, 0x1

    move p1, v0

    goto :goto_0

    :cond_0
    if-le v1, p3, :cond_1

    add-int/lit8 v0, v0, -0x1

    move p2, v0

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    add-int/lit8 p1, p1, 0x1

    neg-int p0, p1

    return p0
.end method

.method public static copyOf([BI)[B
    .locals 2

    .line 5
    new-array v0, p1, [B

    .line 6
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static copyOf([CI)[C
    .locals 2

    .line 13
    new-array v0, p1, [C

    .line 14
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static copyOf([DI)[D
    .locals 2

    .line 17
    new-array v0, p1, [D

    .line 18
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static copyOf([FI)[F
    .locals 2

    .line 15
    new-array v0, p1, [F

    .line 16
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static copyOf([II)[I
    .locals 2

    .line 9
    new-array v0, p1, [I

    .line 10
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static copyOf([JI)[J
    .locals 2

    .line 11
    new-array v0, p1, [J

    .line 12
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;I)[TT;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/intsig/office/java/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">([TU;I",
            "Ljava/lang/Class<",
            "+[TT;>;)[TT;"
        }
    .end annotation

    .line 2
    const-class v0, [Ljava/lang/Object;

    if-ne p2, v0, :cond_0

    new-array p2, p1, [Ljava/lang/Object;

    goto :goto_0

    .line 3
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object p2

    invoke-static {p2, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/Object;

    .line 4
    :goto_0
    array-length v0, p0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v0, 0x0

    invoke-static {p0, v0, p2, v0, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2
.end method

.method public static copyOf([SI)[S
    .locals 2

    .line 7
    new-array v0, p1, [S

    .line 8
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static copyOf([ZI)[Z
    .locals 2

    .line 19
    new-array v0, p1, [Z

    .line 20
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static copyOfRange([BII)[B
    .locals 2

    sub-int v0, p2, p1

    if-ltz v0, :cond_0

    .line 6
    new-array p2, v0, [B

    .line 7
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 8
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static copyOfRange([CII)[C
    .locals 2

    sub-int v0, p2, p1

    if-ltz v0, :cond_0

    .line 18
    new-array p2, v0, [C

    .line 19
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 20
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static copyOfRange([DII)[D
    .locals 2

    sub-int v0, p2, p1

    if-ltz v0, :cond_0

    .line 24
    new-array p2, v0, [D

    .line 25
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 26
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static copyOfRange([FII)[F
    .locals 2

    sub-int v0, p2, p1

    if-ltz v0, :cond_0

    .line 21
    new-array p2, v0, [F

    .line 22
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 23
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static copyOfRange([III)[I
    .locals 2

    sub-int v0, p2, p1

    if-ltz v0, :cond_0

    .line 12
    new-array p2, v0, [I

    .line 13
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 14
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static copyOfRange([JII)[J
    .locals 2

    sub-int v0, p2, p1

    if-ltz v0, :cond_0

    .line 15
    new-array p2, v0, [J

    .line 16
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 17
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II)[TT;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lcom/intsig/office/java/util/Arrays;->copyOfRange([Ljava/lang/Object;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static copyOfRange([Ljava/lang/Object;IILjava/lang/Class;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">([TU;II",
            "Ljava/lang/Class<",
            "+[TT;>;)[TT;"
        }
    .end annotation

    sub-int v0, p2, p1

    if-ltz v0, :cond_1

    .line 2
    const-class p2, [Ljava/lang/Object;

    if-ne p3, p2, :cond_0

    new-array p2, v0, [Ljava/lang/Object;

    goto :goto_0

    .line 3
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object p2

    invoke-static {p2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/Object;

    .line 4
    :goto_0
    array-length p3, p0

    sub-int/2addr p3, p1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result p3

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 5
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static copyOfRange([SII)[S
    .locals 2

    sub-int v0, p2, p1

    if-ltz v0, :cond_0

    .line 9
    new-array p2, v0, [S

    .line 10
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 11
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static copyOfRange([ZII)[Z
    .locals 2

    sub-int v0, p2, p1

    if-ltz v0, :cond_0

    .line 27
    new-array p2, v0, [Z

    .line 28
    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2

    .line 29
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " > "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    if-eqz p0, :cond_10

    .line 7
    .line 8
    if-nez p1, :cond_1

    .line 9
    .line 10
    goto/16 :goto_3

    .line 11
    .line 12
    :cond_1
    array-length v2, p0

    .line 13
    array-length v3, p1

    .line 14
    if-eq v3, v2, :cond_2

    .line 15
    .line 16
    return v1

    .line 17
    :cond_2
    const/4 v3, 0x0

    .line 18
    :goto_0
    if-ge v3, v2, :cond_f

    .line 19
    .line 20
    aget-object v4, p0, v3

    .line 21
    .line 22
    aget-object v5, p1, v3

    .line 23
    .line 24
    if-ne v4, v5, :cond_3

    .line 25
    .line 26
    goto/16 :goto_2

    .line 27
    .line 28
    :cond_3
    if-nez v4, :cond_4

    .line 29
    .line 30
    return v1

    .line 31
    :cond_4
    instance-of v6, v4, [Ljava/lang/Object;

    .line 32
    .line 33
    if-eqz v6, :cond_5

    .line 34
    .line 35
    instance-of v6, v5, [Ljava/lang/Object;

    .line 36
    .line 37
    if-eqz v6, :cond_5

    .line 38
    .line 39
    check-cast v4, [Ljava/lang/Object;

    .line 40
    .line 41
    check-cast v5, [Ljava/lang/Object;

    .line 42
    .line 43
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    goto/16 :goto_1

    .line 48
    .line 49
    :cond_5
    instance-of v6, v4, [B

    .line 50
    .line 51
    if-eqz v6, :cond_6

    .line 52
    .line 53
    instance-of v6, v5, [B

    .line 54
    .line 55
    if-eqz v6, :cond_6

    .line 56
    .line 57
    check-cast v4, [B

    .line 58
    .line 59
    check-cast v5, [B

    .line 60
    .line 61
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->equals([B[B)Z

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    goto/16 :goto_1

    .line 66
    .line 67
    :cond_6
    instance-of v6, v4, [S

    .line 68
    .line 69
    if-eqz v6, :cond_7

    .line 70
    .line 71
    instance-of v6, v5, [S

    .line 72
    .line 73
    if-eqz v6, :cond_7

    .line 74
    .line 75
    check-cast v4, [S

    .line 76
    .line 77
    check-cast v5, [S

    .line 78
    .line 79
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->equals([S[S)Z

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    goto/16 :goto_1

    .line 84
    .line 85
    :cond_7
    instance-of v6, v4, [I

    .line 86
    .line 87
    if-eqz v6, :cond_8

    .line 88
    .line 89
    instance-of v6, v5, [I

    .line 90
    .line 91
    if-eqz v6, :cond_8

    .line 92
    .line 93
    check-cast v4, [I

    .line 94
    .line 95
    check-cast v5, [I

    .line 96
    .line 97
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->equals([I[I)Z

    .line 98
    .line 99
    .line 100
    move-result v4

    .line 101
    goto :goto_1

    .line 102
    :cond_8
    instance-of v6, v4, [J

    .line 103
    .line 104
    if-eqz v6, :cond_9

    .line 105
    .line 106
    instance-of v6, v5, [J

    .line 107
    .line 108
    if-eqz v6, :cond_9

    .line 109
    .line 110
    check-cast v4, [J

    .line 111
    .line 112
    check-cast v5, [J

    .line 113
    .line 114
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->equals([J[J)Z

    .line 115
    .line 116
    .line 117
    move-result v4

    .line 118
    goto :goto_1

    .line 119
    :cond_9
    instance-of v6, v4, [C

    .line 120
    .line 121
    if-eqz v6, :cond_a

    .line 122
    .line 123
    instance-of v6, v5, [C

    .line 124
    .line 125
    if-eqz v6, :cond_a

    .line 126
    .line 127
    check-cast v4, [C

    .line 128
    .line 129
    check-cast v5, [C

    .line 130
    .line 131
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->equals([C[C)Z

    .line 132
    .line 133
    .line 134
    move-result v4

    .line 135
    goto :goto_1

    .line 136
    :cond_a
    instance-of v6, v4, [F

    .line 137
    .line 138
    if-eqz v6, :cond_b

    .line 139
    .line 140
    instance-of v6, v5, [F

    .line 141
    .line 142
    if-eqz v6, :cond_b

    .line 143
    .line 144
    check-cast v4, [F

    .line 145
    .line 146
    check-cast v5, [F

    .line 147
    .line 148
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->equals([F[F)Z

    .line 149
    .line 150
    .line 151
    move-result v4

    .line 152
    goto :goto_1

    .line 153
    :cond_b
    instance-of v6, v4, [D

    .line 154
    .line 155
    if-eqz v6, :cond_c

    .line 156
    .line 157
    instance-of v6, v5, [D

    .line 158
    .line 159
    if-eqz v6, :cond_c

    .line 160
    .line 161
    check-cast v4, [D

    .line 162
    .line 163
    check-cast v5, [D

    .line 164
    .line 165
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->equals([D[D)Z

    .line 166
    .line 167
    .line 168
    move-result v4

    .line 169
    goto :goto_1

    .line 170
    :cond_c
    instance-of v6, v4, [Z

    .line 171
    .line 172
    if-eqz v6, :cond_d

    .line 173
    .line 174
    instance-of v6, v5, [Z

    .line 175
    .line 176
    if-eqz v6, :cond_d

    .line 177
    .line 178
    check-cast v4, [Z

    .line 179
    .line 180
    check-cast v5, [Z

    .line 181
    .line 182
    invoke-static {v4, v5}, Lcom/intsig/office/java/util/Arrays;->equals([Z[Z)Z

    .line 183
    .line 184
    .line 185
    move-result v4

    .line 186
    goto :goto_1

    .line 187
    :cond_d
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 188
    .line 189
    .line 190
    move-result v4

    .line 191
    :goto_1
    if-nez v4, :cond_e

    .line 192
    .line 193
    return v1

    .line 194
    :cond_e
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 195
    .line 196
    goto/16 :goto_0

    .line 197
    .line 198
    :cond_f
    return v0

    .line 199
    :cond_10
    :goto_3
    return v1
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public static deepHashCode([Ljava/lang/Object;)I
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p0, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    array-length v1, p0

    .line 6
    const/4 v2, 0x1

    .line 7
    const/4 v3, 0x0

    .line 8
    :goto_0
    if-ge v3, v1, :cond_b

    .line 9
    .line 10
    aget-object v4, p0, v3

    .line 11
    .line 12
    instance-of v5, v4, [Ljava/lang/Object;

    .line 13
    .line 14
    if-eqz v5, :cond_1

    .line 15
    .line 16
    check-cast v4, [Ljava/lang/Object;

    .line 17
    .line 18
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->deepHashCode([Ljava/lang/Object;)I

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    goto :goto_1

    .line 23
    :cond_1
    instance-of v5, v4, [B

    .line 24
    .line 25
    if-eqz v5, :cond_2

    .line 26
    .line 27
    check-cast v4, [B

    .line 28
    .line 29
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->hashCode([B)I

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    goto :goto_1

    .line 34
    :cond_2
    instance-of v5, v4, [S

    .line 35
    .line 36
    if-eqz v5, :cond_3

    .line 37
    .line 38
    check-cast v4, [S

    .line 39
    .line 40
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->hashCode([S)I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    goto :goto_1

    .line 45
    :cond_3
    instance-of v5, v4, [I

    .line 46
    .line 47
    if-eqz v5, :cond_4

    .line 48
    .line 49
    check-cast v4, [I

    .line 50
    .line 51
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->hashCode([I)I

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    goto :goto_1

    .line 56
    :cond_4
    instance-of v5, v4, [J

    .line 57
    .line 58
    if-eqz v5, :cond_5

    .line 59
    .line 60
    check-cast v4, [J

    .line 61
    .line 62
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->hashCode([J)I

    .line 63
    .line 64
    .line 65
    move-result v4

    .line 66
    goto :goto_1

    .line 67
    :cond_5
    instance-of v5, v4, [C

    .line 68
    .line 69
    if-eqz v5, :cond_6

    .line 70
    .line 71
    check-cast v4, [C

    .line 72
    .line 73
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->hashCode([C)I

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    goto :goto_1

    .line 78
    :cond_6
    instance-of v5, v4, [F

    .line 79
    .line 80
    if-eqz v5, :cond_7

    .line 81
    .line 82
    check-cast v4, [F

    .line 83
    .line 84
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->hashCode([F)I

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    goto :goto_1

    .line 89
    :cond_7
    instance-of v5, v4, [D

    .line 90
    .line 91
    if-eqz v5, :cond_8

    .line 92
    .line 93
    check-cast v4, [D

    .line 94
    .line 95
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->hashCode([D)I

    .line 96
    .line 97
    .line 98
    move-result v4

    .line 99
    goto :goto_1

    .line 100
    :cond_8
    instance-of v5, v4, [Z

    .line 101
    .line 102
    if-eqz v5, :cond_9

    .line 103
    .line 104
    check-cast v4, [Z

    .line 105
    .line 106
    invoke-static {v4}, Lcom/intsig/office/java/util/Arrays;->hashCode([Z)I

    .line 107
    .line 108
    .line 109
    move-result v4

    .line 110
    goto :goto_1

    .line 111
    :cond_9
    if-eqz v4, :cond_a

    .line 112
    .line 113
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    goto :goto_1

    .line 118
    :cond_a
    const/4 v4, 0x0

    .line 119
    :goto_1
    mul-int/lit8 v2, v2, 0x1f

    .line 120
    .line 121
    add-int/2addr v2, v4

    .line 122
    add-int/lit8 v3, v3, 0x1

    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_b
    return v2
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public static deepToString([Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 1
    :cond_0
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x14

    .line 2
    array-length v1, p0

    if-eqz v1, :cond_1

    if-gtz v0, :cond_1

    const v0, 0x7fffffff

    .line 3
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 4
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {p0, v1, v0}, Lcom/intsig/office/java/util/Arrays;->deepToString([Ljava/lang/Object;Ljava/lang/StringBuilder;Ljava/util/Set;)V

    .line 5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static deepToString([Ljava/lang/Object;Ljava/lang/StringBuilder;Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/Set<",
            "[",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "null"

    if-nez p0, :cond_0

    .line 6
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    .line 7
    :cond_0
    invoke-interface {p2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x5b

    .line 8
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    .line 9
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_d

    if-eqz v1, :cond_1

    const-string v2, ", "

    .line 10
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    :cond_1
    aget-object v2, p0, v1

    if-nez v2, :cond_2

    .line 12
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 13
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 14
    invoke-virtual {v3}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 15
    const-class v4, [B

    if-ne v3, v4, :cond_3

    .line 16
    check-cast v2, [B

    invoke-static {v2}, Lcom/intsig/office/java/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 17
    :cond_3
    const-class v4, [S

    if-ne v3, v4, :cond_4

    .line 18
    check-cast v2, [S

    invoke-static {v2}, Lcom/intsig/office/java/util/Arrays;->toString([S)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 19
    :cond_4
    const-class v4, [I

    if-ne v3, v4, :cond_5

    .line 20
    check-cast v2, [I

    invoke-static {v2}, Lcom/intsig/office/java/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 21
    :cond_5
    const-class v4, [J

    if-ne v3, v4, :cond_6

    .line 22
    check-cast v2, [J

    invoke-static {v2}, Lcom/intsig/office/java/util/Arrays;->toString([J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 23
    :cond_6
    const-class v4, [C

    if-ne v3, v4, :cond_7

    .line 24
    check-cast v2, [C

    invoke-static {v2}, Lcom/intsig/office/java/util/Arrays;->toString([C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 25
    :cond_7
    const-class v4, [F

    if-ne v3, v4, :cond_8

    .line 26
    check-cast v2, [F

    invoke-static {v2}, Lcom/intsig/office/java/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 27
    :cond_8
    const-class v4, [D

    if-ne v3, v4, :cond_9

    .line 28
    check-cast v2, [D

    invoke-static {v2}, Lcom/intsig/office/java/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 29
    :cond_9
    const-class v4, [Z

    if-ne v3, v4, :cond_a

    .line 30
    check-cast v2, [Z

    invoke-static {v2}, Lcom/intsig/office/java/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 31
    :cond_a
    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    const-string v2, "[...]"

    .line 32
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 33
    :cond_b
    check-cast v2, [Ljava/lang/Object;

    invoke-static {v2, p1, p2}, Lcom/intsig/office/java/util/Arrays;->deepToString([Ljava/lang/Object;Ljava/lang/StringBuilder;Ljava/util/Set;)V

    goto :goto_1

    .line 34
    :cond_c
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_d
    const/16 v0, 0x5d

    .line 35
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 36
    invoke-interface {p2, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public static equals([B[B)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_1

    .line 13
    :cond_1
    array-length v2, p0

    .line 14
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    .line 15
    aget-byte v4, p0, v3

    aget-byte v5, p1, v3

    if-eq v4, v5, :cond_3

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public static equals([C[C)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_1

    .line 10
    :cond_1
    array-length v2, p0

    .line 11
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    .line 12
    aget-char v4, p0, v3

    aget-char v5, p1, v3

    if-eq v4, v5, :cond_3

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public static equals([D[D)Z
    .locals 9

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_1

    .line 19
    :cond_1
    array-length v2, p0

    .line 20
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    .line 21
    aget-wide v4, p0, v3

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    aget-wide v6, p1, v3

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    cmp-long v8, v4, v6

    if-eqz v8, :cond_3

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public static equals([F[F)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_1

    .line 22
    :cond_1
    array-length v2, p0

    .line 23
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    .line 24
    aget v4, p0, v3

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    aget v5, p1, v3

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    if-eq v4, v5, :cond_3

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public static equals([I[I)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_1

    .line 4
    :cond_1
    array-length v2, p0

    .line 5
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    .line 6
    aget v4, p0, v3

    aget v5, p1, v3

    if-eq v4, v5, :cond_3

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public static equals([J[J)Z
    .locals 9

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_1

    .line 1
    :cond_1
    array-length v2, p0

    .line 2
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    .line 3
    aget-wide v4, p0, v3

    aget-wide v6, p1, v3

    cmp-long v8, v4, v6

    if-eqz v8, :cond_3

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public static equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_7

    if-nez p1, :cond_1

    goto :goto_2

    .line 25
    :cond_1
    array-length v2, p0

    .line 26
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_6

    .line 27
    aget-object v4, p0, v3

    .line 28
    aget-object v5, p1, v3

    if-nez v4, :cond_3

    if-nez v5, :cond_4

    goto :goto_1

    .line 29
    :cond_3
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    return v1

    :cond_5
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_6
    return v0

    :cond_7
    :goto_2
    return v1
.end method

.method public static equals([S[S)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_1

    .line 7
    :cond_1
    array-length v2, p0

    .line 8
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    .line 9
    aget-short v4, p0, v3

    aget-short v5, p1, v3

    if-eq v4, v5, :cond_3

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public static equals([Z[Z)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p0, :cond_5

    if-nez p1, :cond_1

    goto :goto_1

    .line 16
    :cond_1
    array-length v2, p0

    .line 17
    array-length v3, p1

    if-eq v3, v2, :cond_2

    return v1

    :cond_2
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    .line 18
    aget-boolean v4, p0, v3

    aget-boolean v5, p1, v3

    if-eq v4, v5, :cond_3

    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public static fill([BB)V
    .locals 2

    .line 13
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->fill([BIIB)V

    return-void
.end method

.method public static fill([BIIB)V
    .locals 1

    .line 14
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 15
    aput-byte p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([CC)V
    .locals 2

    .line 10
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->fill([CIIC)V

    return-void
.end method

.method public static fill([CIIC)V
    .locals 1

    .line 11
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 12
    aput-char p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([DD)V
    .locals 2

    .line 19
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->fill([DIID)V

    return-void
.end method

.method public static fill([DIID)V
    .locals 1

    .line 20
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 21
    aput-wide p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([FF)V
    .locals 2

    .line 22
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->fill([FIIF)V

    return-void
.end method

.method public static fill([FIIF)V
    .locals 1

    .line 23
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 24
    aput p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([II)V
    .locals 2

    .line 4
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->fill([IIII)V

    return-void
.end method

.method public static fill([IIII)V
    .locals 1

    .line 5
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 6
    aput p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([JIIJ)V
    .locals 1

    .line 2
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 3
    aput-wide p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([JJ)V
    .locals 2

    .line 1
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->fill([JIIJ)V

    return-void
.end method

.method public static fill([Ljava/lang/Object;IILjava/lang/Object;)V
    .locals 1

    .line 26
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 27
    aput-object p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .line 25
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    return-void
.end method

.method public static fill([SIIS)V
    .locals 1

    .line 8
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 9
    aput-short p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([SS)V
    .locals 2

    .line 7
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->fill([SIIS)V

    return-void
.end method

.method public static fill([ZIIZ)V
    .locals 1

    .line 17
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    :goto_0
    if-ge p1, p2, :cond_0

    .line 18
    aput-boolean p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fill([ZZ)V
    .locals 2

    .line 16
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/intsig/office/java/util/Arrays;->fill([ZIIZ)V

    return-void
.end method

.method public static hashCode([B)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 5
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-byte v3, p0, v0

    mul-int/lit8 v2, v2, 0x1f

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public static hashCode([C)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 4
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-char v3, p0, v0

    mul-int/lit8 v2, v2, 0x1f

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public static hashCode([D)I
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 9
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-wide v3, p0, v0

    .line 10
    invoke-static {v3, v4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v3

    mul-int/lit8 v2, v2, 0x1f

    const/16 v5, 0x20

    ushr-long v5, v3, v5

    xor-long/2addr v3, v5

    long-to-int v4, v3

    add-int/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public static hashCode([F)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 7
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    :goto_0
    if-ge v0, v1, :cond_1

    aget v3, p0, v0

    mul-int/lit8 v2, v2, 0x1f

    .line 8
    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public static hashCode([I)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 2
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    :goto_0
    if-ge v0, v1, :cond_1

    aget v3, p0, v0

    mul-int/lit8 v2, v2, 0x1f

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public static hashCode([J)I
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 1
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-wide v3, p0, v0

    const/16 v5, 0x20

    ushr-long v5, v3, v5

    xor-long/2addr v3, v5

    long-to-int v4, v3

    mul-int/lit8 v2, v2, 0x1f

    add-int/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public static hashCode([Ljava/lang/Object;)I
    .locals 5

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 11
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, p0, v3

    mul-int/lit8 v2, v2, 0x1f

    if-nez v4, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    .line 12
    :cond_1
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_1
    add-int/2addr v2, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return v2
.end method

.method public static hashCode([S)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 3
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-short v3, p0, v0

    mul-int/lit8 v2, v2, 0x1f

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public static hashCode([Z)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 6
    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    :goto_0
    if-ge v0, v1, :cond_2

    aget-boolean v3, p0, v0

    mul-int/lit8 v2, v2, 0x1f

    if-eqz v3, :cond_1

    const/16 v3, 0x4cf

    goto :goto_1

    :cond_1
    const/16 v3, 0x4d5

    :goto_1
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v2
.end method

.method private static med3([BIII)I
    .locals 2

    .line 5
    aget-byte v0, p0, p1

    aget-byte v1, p0, p2

    if-ge v0, v1, :cond_1

    aget-byte p0, p0, p3

    if-ge v1, p0, :cond_0

    goto :goto_0

    :cond_0
    if-ge v0, p0, :cond_3

    goto :goto_1

    :cond_1
    aget-byte p0, p0, p3

    if-le v1, p0, :cond_2

    :goto_0
    move p1, p2

    goto :goto_2

    :cond_2
    if-le v0, p0, :cond_3

    :goto_1
    move p1, p3

    :cond_3
    :goto_2
    return p1
.end method

.method private static med3([CIII)I
    .locals 2

    .line 4
    aget-char v0, p0, p1

    aget-char v1, p0, p2

    if-ge v0, v1, :cond_1

    aget-char p0, p0, p3

    if-ge v1, p0, :cond_0

    goto :goto_0

    :cond_0
    if-ge v0, p0, :cond_3

    goto :goto_1

    :cond_1
    aget-char p0, p0, p3

    if-le v1, p0, :cond_2

    :goto_0
    move p1, p2

    goto :goto_2

    :cond_2
    if-le v0, p0, :cond_3

    :goto_1
    move p1, p3

    :cond_3
    :goto_2
    return p1
.end method

.method private static med3([DIII)I
    .locals 6

    .line 6
    aget-wide v0, p0, p1

    aget-wide v2, p0, p2

    cmpg-double v4, v0, v2

    if-gez v4, :cond_1

    aget-wide v4, p0, p3

    cmpg-double p0, v2, v4

    if-gez p0, :cond_0

    goto :goto_0

    :cond_0
    cmpg-double p0, v0, v4

    if-gez p0, :cond_3

    goto :goto_1

    :cond_1
    aget-wide v4, p0, p3

    cmpl-double p0, v2, v4

    if-lez p0, :cond_2

    :goto_0
    move p1, p2

    goto :goto_2

    :cond_2
    cmpl-double p0, v0, v4

    if-lez p0, :cond_3

    :goto_1
    move p1, p3

    :cond_3
    :goto_2
    return p1
.end method

.method private static med3([FIII)I
    .locals 3

    .line 7
    aget v0, p0, p1

    aget v1, p0, p2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_1

    aget p0, p0, p3

    cmpg-float v1, v1, p0

    if-gez v1, :cond_0

    goto :goto_0

    :cond_0
    cmpg-float p0, v0, p0

    if-gez p0, :cond_3

    goto :goto_1

    :cond_1
    aget p0, p0, p3

    cmpl-float v1, v1, p0

    if-lez v1, :cond_2

    :goto_0
    move p1, p2

    goto :goto_2

    :cond_2
    cmpl-float p0, v0, p0

    if-lez p0, :cond_3

    :goto_1
    move p1, p3

    :cond_3
    :goto_2
    return p1
.end method

.method private static med3([IIII)I
    .locals 2

    .line 2
    aget v0, p0, p1

    aget v1, p0, p2

    if-ge v0, v1, :cond_1

    aget p0, p0, p3

    if-ge v1, p0, :cond_0

    goto :goto_0

    :cond_0
    if-ge v0, p0, :cond_3

    goto :goto_1

    :cond_1
    aget p0, p0, p3

    if-le v1, p0, :cond_2

    :goto_0
    move p1, p2

    goto :goto_2

    :cond_2
    if-le v0, p0, :cond_3

    :goto_1
    move p1, p3

    :cond_3
    :goto_2
    return p1
.end method

.method private static med3([JIII)I
    .locals 6

    .line 1
    aget-wide v0, p0, p1

    aget-wide v2, p0, p2

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    aget-wide v4, p0, p3

    cmp-long p0, v2, v4

    if-gez p0, :cond_0

    goto :goto_0

    :cond_0
    cmp-long p0, v0, v4

    if-gez p0, :cond_3

    goto :goto_1

    :cond_1
    aget-wide v4, p0, p3

    cmp-long p0, v2, v4

    if-lez p0, :cond_2

    :goto_0
    move p1, p2

    goto :goto_2

    :cond_2
    cmp-long p0, v0, v4

    if-lez p0, :cond_3

    :goto_1
    move p1, p3

    :cond_3
    :goto_2
    return p1
.end method

.method private static med3([SIII)I
    .locals 2

    .line 3
    aget-short v0, p0, p1

    aget-short v1, p0, p2

    if-ge v0, v1, :cond_1

    aget-short p0, p0, p3

    if-ge v1, p0, :cond_0

    goto :goto_0

    :cond_0
    if-ge v0, p0, :cond_3

    goto :goto_1

    :cond_1
    aget-short p0, p0, p3

    if-le v1, p0, :cond_2

    :goto_0
    move p1, p2

    goto :goto_2

    :cond_2
    if-le v0, p0, :cond_3

    :goto_1
    move p1, p3

    :cond_3
    :goto_2
    return p1
.end method

.method private static mergeSort([Ljava/lang/Object;[Ljava/lang/Object;III)V
    .locals 5

    sub-int v0, p3, p2

    const/4 v1, 0x7

    if-ge v0, v1, :cond_2

    move p0, p2

    :goto_0
    if-ge p0, p3, :cond_1

    move p4, p0

    :goto_1
    if-le p4, p2, :cond_0

    add-int/lit8 v0, p4, -0x1

    .line 1
    aget-object v1, p1, v0

    check-cast v1, Ljava/lang/Comparable;

    aget-object v2, p1, p4

    invoke-interface {v1, v2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_0

    .line 2
    invoke-static {p1, p4, v0}, Lcom/intsig/office/java/util/Arrays;->swap([Ljava/lang/Object;II)V

    add-int/lit8 p4, p4, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    add-int v1, p2, p4

    add-int v2, p3, p4

    add-int v3, v1, v2

    ushr-int/lit8 v3, v3, 0x1

    neg-int p4, p4

    .line 3
    invoke-static {p1, p0, v1, v3, p4}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;III)V

    .line 4
    invoke-static {p1, p0, v3, v2, p4}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;III)V

    add-int/lit8 p4, v3, -0x1

    .line 5
    aget-object p4, p0, p4

    check-cast p4, Ljava/lang/Comparable;

    aget-object v4, p0, v3

    invoke-interface {p4, v4}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result p4

    if-gtz p4, :cond_3

    .line 6
    invoke-static {p0, v1, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_3
    move p4, v3

    :goto_2
    if-ge p2, p3, :cond_6

    if-ge p4, v2, :cond_5

    if-ge v1, v3, :cond_4

    .line 7
    aget-object v0, p0, v1

    check-cast v0, Ljava/lang/Comparable;

    aget-object v4, p0, p4

    invoke-interface {v0, v4}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v0, p4, 0x1

    .line 8
    aget-object p4, p0, p4

    aput-object p4, p1, p2

    move p4, v0

    goto :goto_4

    :cond_5
    :goto_3
    add-int/lit8 v0, v1, 0x1

    .line 9
    aget-object v1, p0, v1

    aput-object v1, p1, p2

    move v1, v0

    :goto_4
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_6
    return-void
.end method

.method private static mergeSort([Ljava/lang/Object;[Ljava/lang/Object;IIILjava/util/Comparator;)V
    .locals 10

    sub-int v0, p3, p2

    const/4 v1, 0x7

    if-ge v0, v1, :cond_2

    move p0, p2

    :goto_0
    if-ge p0, p3, :cond_1

    move p4, p0

    :goto_1
    if-le p4, p2, :cond_0

    add-int/lit8 v0, p4, -0x1

    .line 10
    aget-object v1, p1, v0

    aget-object v2, p1, p4

    invoke-interface {p5, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_0

    .line 11
    invoke-static {p1, p4, v0}, Lcom/intsig/office/java/util/Arrays;->swap([Ljava/lang/Object;II)V

    add-int/lit8 p4, p4, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    add-int v7, p2, p4

    add-int v8, p3, p4

    add-int v1, v7, v8

    ushr-int/lit8 v9, v1, 0x1

    neg-int p4, p4

    move-object v1, p1

    move-object v2, p0

    move v3, v7

    move v4, v9

    move v5, p4

    move-object v6, p5

    .line 12
    invoke-static/range {v1 .. v6}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;IIILjava/util/Comparator;)V

    move v3, v9

    move v4, v8

    .line 13
    invoke-static/range {v1 .. v6}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;IIILjava/util/Comparator;)V

    add-int/lit8 p4, v9, -0x1

    .line 14
    aget-object p4, p0, p4

    aget-object v1, p0, v9

    invoke-interface {p5, p4, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    if-gtz p4, :cond_3

    .line 15
    invoke-static {p0, v7, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_3
    move p4, v9

    :goto_2
    if-ge p2, p3, :cond_6

    if-ge p4, v8, :cond_5

    if-ge v7, v9, :cond_4

    .line 16
    aget-object v0, p0, v7

    aget-object v1, p0, p4

    invoke-interface {p5, v0, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v0, p4, 0x1

    .line 17
    aget-object p4, p0, p4

    aput-object p4, p1, p2

    move p4, v0

    goto :goto_4

    :cond_5
    :goto_3
    add-int/lit8 v0, v7, 0x1

    .line 18
    aget-object v1, p0, v7

    aput-object v1, p1, p2

    move v7, v0

    :goto_4
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_6
    return-void
.end method

.method private static rangeCheck(III)V
    .locals 2

    .line 1
    if-gt p1, p2, :cond_2

    .line 2
    .line 3
    if-ltz p1, :cond_1

    .line 4
    .line 5
    if-gt p2, p0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance p0, Ljava/lang/ArrayIndexOutOfBoundsException;

    .line 9
    .line 10
    invoke-direct {p0, p2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    .line 11
    .line 12
    .line 13
    throw p0

    .line 14
    :cond_1
    new-instance p0, Ljava/lang/ArrayIndexOutOfBoundsException;

    .line 15
    .line 16
    invoke-direct {p0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    .line 17
    .line 18
    .line 19
    throw p0

    .line 20
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 21
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v1, "fromIndex("

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string p1, ") > toIndex("

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string p1, ")"

    .line 44
    .line 45
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    throw p0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static sort([B)V
    .locals 2

    .line 13
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([BII)V

    return-void
.end method

.method public static sort([BII)V
    .locals 1

    .line 14
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    sub-int/2addr p2, p1

    .line 15
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->sort1([BII)V

    return-void
.end method

.method public static sort([C)V
    .locals 2

    .line 10
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([CII)V

    return-void
.end method

.method public static sort([CII)V
    .locals 1

    .line 11
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    sub-int/2addr p2, p1

    .line 12
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->sort1([CII)V

    return-void
.end method

.method public static sort([D)V
    .locals 2

    .line 16
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/intsig/office/java/util/Arrays;->sort2([DII)V

    return-void
.end method

.method public static sort([DII)V
    .locals 1

    .line 17
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 18
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->sort2([DII)V

    return-void
.end method

.method public static sort([F)V
    .locals 2

    .line 19
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/intsig/office/java/util/Arrays;->sort2([FII)V

    return-void
.end method

.method public static sort([FII)V
    .locals 1

    .line 20
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 21
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->sort2([FII)V

    return-void
.end method

.method public static sort([I)V
    .locals 2

    .line 4
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([III)V

    return-void
.end method

.method public static sort([III)V
    .locals 1

    .line 5
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    sub-int/2addr p2, p1

    .line 6
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->sort1([III)V

    return-void
.end method

.method public static sort([J)V
    .locals 2

    .line 1
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([JII)V

    return-void
.end method

.method public static sort([JII)V
    .locals 1

    .line 2
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    sub-int/2addr p2, p1

    .line 3
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->sort1([JII)V

    return-void
.end method

.method public static sort([Ljava/lang/Object;)V
    .locals 3

    .line 22
    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 23
    array-length v2, p0

    invoke-static {v0, p0, v1, v2, v1}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;III)V

    return-void
.end method

.method public static sort([Ljava/lang/Object;II)V
    .locals 2

    .line 24
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 25
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    neg-int v1, p1

    .line 26
    invoke-static {v0, p0, p1, p2, v1}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;III)V

    return-void
.end method

.method public static sort([Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;II",
            "Ljava/util/Comparator<",
            "-TT;>;)V"
        }
    .end annotation

    .line 30
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    .line 31
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v1

    if-nez p3, :cond_0

    neg-int p3, p1

    .line 32
    invoke-static {v1, p0, p1, p2, p3}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;III)V

    goto :goto_0

    :cond_0
    neg-int v5, p1

    move-object v2, p0

    move v3, p1

    move v4, p2

    move-object v6, p3

    .line 33
    invoke-static/range {v1 .. v6}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;IIILjava/util/Comparator;)V

    :goto_0
    return-void
.end method

.method public static sort([Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator<",
            "-TT;>;)V"
        }
    .end annotation

    .line 27
    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, [Ljava/lang/Object;

    if-nez p1, :cond_0

    .line 28
    array-length p1, p0

    const/4 v0, 0x0

    invoke-static {v1, p0, v0, p1, v0}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;III)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 29
    array-length v4, p0

    const/4 v5, 0x0

    move-object v2, p0

    move-object v6, p1

    invoke-static/range {v1 .. v6}, Lcom/intsig/office/java/util/Arrays;->mergeSort([Ljava/lang/Object;[Ljava/lang/Object;IIILjava/util/Comparator;)V

    :goto_0
    return-void
.end method

.method public static sort([S)V
    .locals 2

    .line 7
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([SII)V

    return-void
.end method

.method public static sort([SII)V
    .locals 1

    .line 8
    array-length v0, p0

    invoke-static {v0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->rangeCheck(III)V

    sub-int/2addr p2, p1

    .line 9
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->sort1([SII)V

    return-void
.end method

.method private static sort1([BII)V
    .locals 8

    const/4 v0, 0x7

    if-ge p2, v0, :cond_2

    move v0, p1

    :goto_0
    add-int v1, p2, p1

    if-ge v0, v1, :cond_1

    move v1, v0

    :goto_1
    if-le v1, p1, :cond_0

    add-int/lit8 v2, v1, -0x1

    .line 77
    aget-byte v3, p0, v2

    aget-byte v4, p0, v1

    if-le v3, v4, :cond_0

    .line 78
    invoke-static {p0, v1, v2}, Lcom/intsig/office/java/util/Arrays;->swap([BII)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    shr-int/lit8 v1, p2, 0x1

    add-int/2addr v1, p1

    const/4 v2, 0x1

    if-le p2, v0, :cond_4

    add-int v0, p1, p2

    sub-int/2addr v0, v2

    const/16 v3, 0x28

    if-le p2, v3, :cond_3

    .line 79
    div-int/lit8 v3, p2, 0x8

    add-int v4, p1, v3

    mul-int/lit8 v5, v3, 0x2

    add-int v6, p1, v5

    .line 80
    invoke-static {p0, p1, v4, v6}, Lcom/intsig/office/java/util/Arrays;->med3([BIII)I

    move-result v4

    sub-int v6, v1, v3

    add-int v7, v1, v3

    .line 81
    invoke-static {p0, v6, v1, v7}, Lcom/intsig/office/java/util/Arrays;->med3([BIII)I

    move-result v1

    sub-int v5, v0, v5

    sub-int v3, v0, v3

    .line 82
    invoke-static {p0, v5, v3, v0}, Lcom/intsig/office/java/util/Arrays;->med3([BIII)I

    move-result v0

    goto :goto_2

    :cond_3
    move v4, p1

    .line 83
    :goto_2
    invoke-static {p0, v4, v1, v0}, Lcom/intsig/office/java/util/Arrays;->med3([BIII)I

    move-result v1

    .line 84
    :cond_4
    aget-byte v0, p0, v1

    add-int/2addr p2, p1

    add-int/lit8 v1, p2, -0x1

    move v4, p1

    move v5, v4

    move v3, v1

    :goto_3
    if-gt v4, v1, :cond_6

    .line 85
    aget-byte v6, p0, v4

    if-gt v6, v0, :cond_6

    if-ne v6, v0, :cond_5

    add-int/lit8 v6, v5, 0x1

    .line 86
    invoke-static {p0, v5, v4}, Lcom/intsig/office/java/util/Arrays;->swap([BII)V

    move v5, v6

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-lt v1, v4, :cond_8

    .line 87
    aget-byte v6, p0, v1

    if-lt v6, v0, :cond_8

    if-ne v6, v0, :cond_7

    add-int/lit8 v6, v3, -0x1

    .line 88
    invoke-static {p0, v1, v3}, Lcom/intsig/office/java/util/Arrays;->swap([BII)V

    move v3, v6

    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_8
    if-le v4, v1, :cond_b

    sub-int v0, v5, p1

    sub-int v5, v4, v5

    .line 89
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v6, v4, v0

    .line 90
    invoke-static {p0, p1, v6, v0}, Lcom/intsig/office/java/util/Arrays;->vecswap([BIII)V

    sub-int v0, v3, v1

    sub-int v1, p2, v3

    sub-int/2addr v1, v2

    .line 91
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v3, p2, v1

    .line 92
    invoke-static {p0, v4, v3, v1}, Lcom/intsig/office/java/util/Arrays;->vecswap([BIII)V

    if-le v5, v2, :cond_9

    .line 93
    invoke-static {p0, p1, v5}, Lcom/intsig/office/java/util/Arrays;->sort1([BII)V

    :cond_9
    if-le v0, v2, :cond_a

    sub-int/2addr p2, v0

    .line 94
    invoke-static {p0, p2, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([BII)V

    :cond_a
    return-void

    :cond_b
    add-int/lit8 v6, v4, 0x1

    add-int/lit8 v7, v1, -0x1

    .line 95
    invoke-static {p0, v4, v1}, Lcom/intsig/office/java/util/Arrays;->swap([BII)V

    move v4, v6

    move v1, v7

    goto :goto_3
.end method

.method private static sort1([CII)V
    .locals 8

    const/4 v0, 0x7

    if-ge p2, v0, :cond_2

    move v0, p1

    :goto_0
    add-int v1, p2, p1

    if-ge v0, v1, :cond_1

    move v1, v0

    :goto_1
    if-le v1, p1, :cond_0

    add-int/lit8 v2, v1, -0x1

    .line 58
    aget-char v3, p0, v2

    aget-char v4, p0, v1

    if-le v3, v4, :cond_0

    .line 59
    invoke-static {p0, v1, v2}, Lcom/intsig/office/java/util/Arrays;->swap([CII)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    shr-int/lit8 v1, p2, 0x1

    add-int/2addr v1, p1

    const/4 v2, 0x1

    if-le p2, v0, :cond_4

    add-int v0, p1, p2

    sub-int/2addr v0, v2

    const/16 v3, 0x28

    if-le p2, v3, :cond_3

    .line 60
    div-int/lit8 v3, p2, 0x8

    add-int v4, p1, v3

    mul-int/lit8 v5, v3, 0x2

    add-int v6, p1, v5

    .line 61
    invoke-static {p0, p1, v4, v6}, Lcom/intsig/office/java/util/Arrays;->med3([CIII)I

    move-result v4

    sub-int v6, v1, v3

    add-int v7, v1, v3

    .line 62
    invoke-static {p0, v6, v1, v7}, Lcom/intsig/office/java/util/Arrays;->med3([CIII)I

    move-result v1

    sub-int v5, v0, v5

    sub-int v3, v0, v3

    .line 63
    invoke-static {p0, v5, v3, v0}, Lcom/intsig/office/java/util/Arrays;->med3([CIII)I

    move-result v0

    goto :goto_2

    :cond_3
    move v4, p1

    .line 64
    :goto_2
    invoke-static {p0, v4, v1, v0}, Lcom/intsig/office/java/util/Arrays;->med3([CIII)I

    move-result v1

    .line 65
    :cond_4
    aget-char v0, p0, v1

    add-int/2addr p2, p1

    add-int/lit8 v1, p2, -0x1

    move v4, p1

    move v5, v4

    move v3, v1

    :goto_3
    if-gt v4, v1, :cond_6

    .line 66
    aget-char v6, p0, v4

    if-gt v6, v0, :cond_6

    if-ne v6, v0, :cond_5

    add-int/lit8 v6, v5, 0x1

    .line 67
    invoke-static {p0, v5, v4}, Lcom/intsig/office/java/util/Arrays;->swap([CII)V

    move v5, v6

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-lt v1, v4, :cond_8

    .line 68
    aget-char v6, p0, v1

    if-lt v6, v0, :cond_8

    if-ne v6, v0, :cond_7

    add-int/lit8 v6, v3, -0x1

    .line 69
    invoke-static {p0, v1, v3}, Lcom/intsig/office/java/util/Arrays;->swap([CII)V

    move v3, v6

    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_8
    if-le v4, v1, :cond_b

    sub-int v0, v5, p1

    sub-int v5, v4, v5

    .line 70
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v6, v4, v0

    .line 71
    invoke-static {p0, p1, v6, v0}, Lcom/intsig/office/java/util/Arrays;->vecswap([CIII)V

    sub-int v0, v3, v1

    sub-int v1, p2, v3

    sub-int/2addr v1, v2

    .line 72
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v3, p2, v1

    .line 73
    invoke-static {p0, v4, v3, v1}, Lcom/intsig/office/java/util/Arrays;->vecswap([CIII)V

    if-le v5, v2, :cond_9

    .line 74
    invoke-static {p0, p1, v5}, Lcom/intsig/office/java/util/Arrays;->sort1([CII)V

    :cond_9
    if-le v0, v2, :cond_a

    sub-int/2addr p2, v0

    .line 75
    invoke-static {p0, p2, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([CII)V

    :cond_a
    return-void

    :cond_b
    add-int/lit8 v6, v4, 0x1

    add-int/lit8 v7, v1, -0x1

    .line 76
    invoke-static {p0, v4, v1}, Lcom/intsig/office/java/util/Arrays;->swap([CII)V

    move v4, v6

    move v1, v7

    goto :goto_3
.end method

.method private static sort1([DII)V
    .locals 10

    const/4 v0, 0x7

    if-ge p2, v0, :cond_2

    move v0, p1

    :goto_0
    add-int v1, p2, p1

    if-ge v0, v1, :cond_1

    move v1, v0

    :goto_1
    if-le v1, p1, :cond_0

    add-int/lit8 v2, v1, -0x1

    .line 96
    aget-wide v3, p0, v2

    aget-wide v5, p0, v1

    cmpl-double v7, v3, v5

    if-lez v7, :cond_0

    .line 97
    invoke-static {p0, v1, v2}, Lcom/intsig/office/java/util/Arrays;->swap([DII)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    shr-int/lit8 v1, p2, 0x1

    add-int/2addr v1, p1

    const/4 v2, 0x1

    if-le p2, v0, :cond_4

    add-int v0, p1, p2

    sub-int/2addr v0, v2

    const/16 v3, 0x28

    if-le p2, v3, :cond_3

    .line 98
    div-int/lit8 v3, p2, 0x8

    add-int v4, p1, v3

    mul-int/lit8 v5, v3, 0x2

    add-int v6, p1, v5

    .line 99
    invoke-static {p0, p1, v4, v6}, Lcom/intsig/office/java/util/Arrays;->med3([DIII)I

    move-result v4

    sub-int v6, v1, v3

    add-int v7, v1, v3

    .line 100
    invoke-static {p0, v6, v1, v7}, Lcom/intsig/office/java/util/Arrays;->med3([DIII)I

    move-result v1

    sub-int v5, v0, v5

    sub-int v3, v0, v3

    .line 101
    invoke-static {p0, v5, v3, v0}, Lcom/intsig/office/java/util/Arrays;->med3([DIII)I

    move-result v0

    goto :goto_2

    :cond_3
    move v4, p1

    .line 102
    :goto_2
    invoke-static {p0, v4, v1, v0}, Lcom/intsig/office/java/util/Arrays;->med3([DIII)I

    move-result v1

    .line 103
    :cond_4
    aget-wide v0, p0, v1

    add-int/2addr p2, p1

    add-int/lit8 v3, p2, -0x1

    move v5, p1

    move v6, v5

    move v4, v3

    :goto_3
    if-gt v5, v3, :cond_6

    .line 104
    aget-wide v7, p0, v5

    cmpg-double v9, v7, v0

    if-gtz v9, :cond_6

    cmpl-double v9, v7, v0

    if-nez v9, :cond_5

    add-int/lit8 v7, v6, 0x1

    .line 105
    invoke-static {p0, v6, v5}, Lcom/intsig/office/java/util/Arrays;->swap([DII)V

    move v6, v7

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-lt v3, v5, :cond_8

    .line 106
    aget-wide v7, p0, v3

    cmpl-double v9, v7, v0

    if-ltz v9, :cond_8

    cmpl-double v9, v7, v0

    if-nez v9, :cond_7

    add-int/lit8 v7, v4, -0x1

    .line 107
    invoke-static {p0, v3, v4}, Lcom/intsig/office/java/util/Arrays;->swap([DII)V

    move v4, v7

    :cond_7
    add-int/lit8 v3, v3, -0x1

    goto :goto_4

    :cond_8
    if-le v5, v3, :cond_b

    sub-int v0, v6, p1

    sub-int v1, v5, v6

    .line 108
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v6, v5, v0

    .line 109
    invoke-static {p0, p1, v6, v0}, Lcom/intsig/office/java/util/Arrays;->vecswap([DIII)V

    sub-int v0, v4, v3

    sub-int v3, p2, v4

    sub-int/2addr v3, v2

    .line 110
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int v4, p2, v3

    .line 111
    invoke-static {p0, v5, v4, v3}, Lcom/intsig/office/java/util/Arrays;->vecswap([DIII)V

    if-le v1, v2, :cond_9

    .line 112
    invoke-static {p0, p1, v1}, Lcom/intsig/office/java/util/Arrays;->sort1([DII)V

    :cond_9
    if-le v0, v2, :cond_a

    sub-int/2addr p2, v0

    .line 113
    invoke-static {p0, p2, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([DII)V

    :cond_a
    return-void

    :cond_b
    add-int/lit8 v7, v5, 0x1

    add-int/lit8 v8, v3, -0x1

    .line 114
    invoke-static {p0, v5, v3}, Lcom/intsig/office/java/util/Arrays;->swap([DII)V

    move v5, v7

    move v3, v8

    goto :goto_3
.end method

.method private static sort1([FII)V
    .locals 8

    const/4 v0, 0x7

    if-ge p2, v0, :cond_2

    move v0, p1

    :goto_0
    add-int v1, p2, p1

    if-ge v0, v1, :cond_1

    move v1, v0

    :goto_1
    if-le v1, p1, :cond_0

    add-int/lit8 v2, v1, -0x1

    .line 115
    aget v3, p0, v2

    aget v4, p0, v1

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 116
    invoke-static {p0, v1, v2}, Lcom/intsig/office/java/util/Arrays;->swap([FII)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    shr-int/lit8 v1, p2, 0x1

    add-int/2addr v1, p1

    const/4 v2, 0x1

    if-le p2, v0, :cond_4

    add-int v0, p1, p2

    sub-int/2addr v0, v2

    const/16 v3, 0x28

    if-le p2, v3, :cond_3

    .line 117
    div-int/lit8 v3, p2, 0x8

    add-int v4, p1, v3

    mul-int/lit8 v5, v3, 0x2

    add-int v6, p1, v5

    .line 118
    invoke-static {p0, p1, v4, v6}, Lcom/intsig/office/java/util/Arrays;->med3([FIII)I

    move-result v4

    sub-int v6, v1, v3

    add-int v7, v1, v3

    .line 119
    invoke-static {p0, v6, v1, v7}, Lcom/intsig/office/java/util/Arrays;->med3([FIII)I

    move-result v1

    sub-int v5, v0, v5

    sub-int v3, v0, v3

    .line 120
    invoke-static {p0, v5, v3, v0}, Lcom/intsig/office/java/util/Arrays;->med3([FIII)I

    move-result v0

    goto :goto_2

    :cond_3
    move v4, p1

    .line 121
    :goto_2
    invoke-static {p0, v4, v1, v0}, Lcom/intsig/office/java/util/Arrays;->med3([FIII)I

    move-result v1

    .line 122
    :cond_4
    aget v0, p0, v1

    add-int/2addr p2, p1

    add-int/lit8 v1, p2, -0x1

    move v4, p1

    move v5, v4

    move v3, v1

    :goto_3
    if-gt v4, v1, :cond_6

    .line 123
    aget v6, p0, v4

    cmpg-float v7, v6, v0

    if-gtz v7, :cond_6

    cmpl-float v6, v6, v0

    if-nez v6, :cond_5

    add-int/lit8 v6, v5, 0x1

    .line 124
    invoke-static {p0, v5, v4}, Lcom/intsig/office/java/util/Arrays;->swap([FII)V

    move v5, v6

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-lt v1, v4, :cond_8

    .line 125
    aget v6, p0, v1

    cmpl-float v7, v6, v0

    if-ltz v7, :cond_8

    cmpl-float v6, v6, v0

    if-nez v6, :cond_7

    add-int/lit8 v6, v3, -0x1

    .line 126
    invoke-static {p0, v1, v3}, Lcom/intsig/office/java/util/Arrays;->swap([FII)V

    move v3, v6

    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_8
    if-le v4, v1, :cond_b

    sub-int v0, v5, p1

    sub-int v5, v4, v5

    .line 127
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v6, v4, v0

    .line 128
    invoke-static {p0, p1, v6, v0}, Lcom/intsig/office/java/util/Arrays;->vecswap([FIII)V

    sub-int v0, v3, v1

    sub-int v1, p2, v3

    sub-int/2addr v1, v2

    .line 129
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v3, p2, v1

    .line 130
    invoke-static {p0, v4, v3, v1}, Lcom/intsig/office/java/util/Arrays;->vecswap([FIII)V

    if-le v5, v2, :cond_9

    .line 131
    invoke-static {p0, p1, v5}, Lcom/intsig/office/java/util/Arrays;->sort1([FII)V

    :cond_9
    if-le v0, v2, :cond_a

    sub-int/2addr p2, v0

    .line 132
    invoke-static {p0, p2, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([FII)V

    :cond_a
    return-void

    :cond_b
    add-int/lit8 v6, v4, 0x1

    add-int/lit8 v7, v1, -0x1

    .line 133
    invoke-static {p0, v4, v1}, Lcom/intsig/office/java/util/Arrays;->swap([FII)V

    move v4, v6

    move v1, v7

    goto :goto_3
.end method

.method private static sort1([III)V
    .locals 8

    const/4 v0, 0x7

    if-ge p2, v0, :cond_2

    move v0, p1

    :goto_0
    add-int v1, p2, p1

    if-ge v0, v1, :cond_1

    move v1, v0

    :goto_1
    if-le v1, p1, :cond_0

    add-int/lit8 v2, v1, -0x1

    .line 20
    aget v3, p0, v2

    aget v4, p0, v1

    if-le v3, v4, :cond_0

    .line 21
    invoke-static {p0, v1, v2}, Lcom/intsig/office/java/util/Arrays;->swap([III)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    shr-int/lit8 v1, p2, 0x1

    add-int/2addr v1, p1

    const/4 v2, 0x1

    if-le p2, v0, :cond_4

    add-int v0, p1, p2

    sub-int/2addr v0, v2

    const/16 v3, 0x28

    if-le p2, v3, :cond_3

    .line 22
    div-int/lit8 v3, p2, 0x8

    add-int v4, p1, v3

    mul-int/lit8 v5, v3, 0x2

    add-int v6, p1, v5

    .line 23
    invoke-static {p0, p1, v4, v6}, Lcom/intsig/office/java/util/Arrays;->med3([IIII)I

    move-result v4

    sub-int v6, v1, v3

    add-int v7, v1, v3

    .line 24
    invoke-static {p0, v6, v1, v7}, Lcom/intsig/office/java/util/Arrays;->med3([IIII)I

    move-result v1

    sub-int v5, v0, v5

    sub-int v3, v0, v3

    .line 25
    invoke-static {p0, v5, v3, v0}, Lcom/intsig/office/java/util/Arrays;->med3([IIII)I

    move-result v0

    goto :goto_2

    :cond_3
    move v4, p1

    .line 26
    :goto_2
    invoke-static {p0, v4, v1, v0}, Lcom/intsig/office/java/util/Arrays;->med3([IIII)I

    move-result v1

    .line 27
    :cond_4
    aget v0, p0, v1

    add-int/2addr p2, p1

    add-int/lit8 v1, p2, -0x1

    move v4, p1

    move v5, v4

    move v3, v1

    :goto_3
    if-gt v4, v1, :cond_6

    .line 28
    aget v6, p0, v4

    if-gt v6, v0, :cond_6

    if-ne v6, v0, :cond_5

    add-int/lit8 v6, v5, 0x1

    .line 29
    invoke-static {p0, v5, v4}, Lcom/intsig/office/java/util/Arrays;->swap([III)V

    move v5, v6

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-lt v1, v4, :cond_8

    .line 30
    aget v6, p0, v1

    if-lt v6, v0, :cond_8

    if-ne v6, v0, :cond_7

    add-int/lit8 v6, v3, -0x1

    .line 31
    invoke-static {p0, v1, v3}, Lcom/intsig/office/java/util/Arrays;->swap([III)V

    move v3, v6

    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_8
    if-le v4, v1, :cond_b

    sub-int v0, v5, p1

    sub-int v5, v4, v5

    .line 32
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v6, v4, v0

    .line 33
    invoke-static {p0, p1, v6, v0}, Lcom/intsig/office/java/util/Arrays;->vecswap([IIII)V

    sub-int v0, v3, v1

    sub-int v1, p2, v3

    sub-int/2addr v1, v2

    .line 34
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v3, p2, v1

    .line 35
    invoke-static {p0, v4, v3, v1}, Lcom/intsig/office/java/util/Arrays;->vecswap([IIII)V

    if-le v5, v2, :cond_9

    .line 36
    invoke-static {p0, p1, v5}, Lcom/intsig/office/java/util/Arrays;->sort1([III)V

    :cond_9
    if-le v0, v2, :cond_a

    sub-int/2addr p2, v0

    .line 37
    invoke-static {p0, p2, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([III)V

    :cond_a
    return-void

    :cond_b
    add-int/lit8 v6, v4, 0x1

    add-int/lit8 v7, v1, -0x1

    .line 38
    invoke-static {p0, v4, v1}, Lcom/intsig/office/java/util/Arrays;->swap([III)V

    move v4, v6

    move v1, v7

    goto :goto_3
.end method

.method private static sort1([JII)V
    .locals 10

    const/4 v0, 0x7

    if-ge p2, v0, :cond_2

    move v0, p1

    :goto_0
    add-int v1, p2, p1

    if-ge v0, v1, :cond_1

    move v1, v0

    :goto_1
    if-le v1, p1, :cond_0

    add-int/lit8 v2, v1, -0x1

    .line 1
    aget-wide v3, p0, v2

    aget-wide v5, p0, v1

    cmp-long v7, v3, v5

    if-lez v7, :cond_0

    .line 2
    invoke-static {p0, v1, v2}, Lcom/intsig/office/java/util/Arrays;->swap([JII)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    shr-int/lit8 v1, p2, 0x1

    add-int/2addr v1, p1

    const/4 v2, 0x1

    if-le p2, v0, :cond_4

    add-int v0, p1, p2

    sub-int/2addr v0, v2

    const/16 v3, 0x28

    if-le p2, v3, :cond_3

    .line 3
    div-int/lit8 v3, p2, 0x8

    add-int v4, p1, v3

    mul-int/lit8 v5, v3, 0x2

    add-int v6, p1, v5

    .line 4
    invoke-static {p0, p1, v4, v6}, Lcom/intsig/office/java/util/Arrays;->med3([JIII)I

    move-result v4

    sub-int v6, v1, v3

    add-int v7, v1, v3

    .line 5
    invoke-static {p0, v6, v1, v7}, Lcom/intsig/office/java/util/Arrays;->med3([JIII)I

    move-result v1

    sub-int v5, v0, v5

    sub-int v3, v0, v3

    .line 6
    invoke-static {p0, v5, v3, v0}, Lcom/intsig/office/java/util/Arrays;->med3([JIII)I

    move-result v0

    goto :goto_2

    :cond_3
    move v4, p1

    .line 7
    :goto_2
    invoke-static {p0, v4, v1, v0}, Lcom/intsig/office/java/util/Arrays;->med3([JIII)I

    move-result v1

    .line 8
    :cond_4
    aget-wide v0, p0, v1

    add-int/2addr p2, p1

    add-int/lit8 v3, p2, -0x1

    move v5, p1

    move v6, v5

    move v4, v3

    :goto_3
    if-gt v5, v3, :cond_6

    .line 9
    aget-wide v7, p0, v5

    cmp-long v9, v7, v0

    if-gtz v9, :cond_6

    cmp-long v9, v7, v0

    if-nez v9, :cond_5

    add-int/lit8 v7, v6, 0x1

    .line 10
    invoke-static {p0, v6, v5}, Lcom/intsig/office/java/util/Arrays;->swap([JII)V

    move v6, v7

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-lt v3, v5, :cond_8

    .line 11
    aget-wide v7, p0, v3

    cmp-long v9, v7, v0

    if-ltz v9, :cond_8

    cmp-long v9, v7, v0

    if-nez v9, :cond_7

    add-int/lit8 v7, v4, -0x1

    .line 12
    invoke-static {p0, v3, v4}, Lcom/intsig/office/java/util/Arrays;->swap([JII)V

    move v4, v7

    :cond_7
    add-int/lit8 v3, v3, -0x1

    goto :goto_4

    :cond_8
    if-le v5, v3, :cond_b

    sub-int v0, v6, p1

    sub-int v1, v5, v6

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v6, v5, v0

    .line 14
    invoke-static {p0, p1, v6, v0}, Lcom/intsig/office/java/util/Arrays;->vecswap([JIII)V

    sub-int v0, v4, v3

    sub-int v3, p2, v4

    sub-int/2addr v3, v2

    .line 15
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    sub-int v4, p2, v3

    .line 16
    invoke-static {p0, v5, v4, v3}, Lcom/intsig/office/java/util/Arrays;->vecswap([JIII)V

    if-le v1, v2, :cond_9

    .line 17
    invoke-static {p0, p1, v1}, Lcom/intsig/office/java/util/Arrays;->sort1([JII)V

    :cond_9
    if-le v0, v2, :cond_a

    sub-int/2addr p2, v0

    .line 18
    invoke-static {p0, p2, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([JII)V

    :cond_a
    return-void

    :cond_b
    add-int/lit8 v7, v5, 0x1

    add-int/lit8 v8, v3, -0x1

    .line 19
    invoke-static {p0, v5, v3}, Lcom/intsig/office/java/util/Arrays;->swap([JII)V

    move v5, v7

    move v3, v8

    goto :goto_3
.end method

.method private static sort1([SII)V
    .locals 8

    const/4 v0, 0x7

    if-ge p2, v0, :cond_2

    move v0, p1

    :goto_0
    add-int v1, p2, p1

    if-ge v0, v1, :cond_1

    move v1, v0

    :goto_1
    if-le v1, p1, :cond_0

    add-int/lit8 v2, v1, -0x1

    .line 39
    aget-short v3, p0, v2

    aget-short v4, p0, v1

    if-le v3, v4, :cond_0

    .line 40
    invoke-static {p0, v1, v2}, Lcom/intsig/office/java/util/Arrays;->swap([SII)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    shr-int/lit8 v1, p2, 0x1

    add-int/2addr v1, p1

    const/4 v2, 0x1

    if-le p2, v0, :cond_4

    add-int v0, p1, p2

    sub-int/2addr v0, v2

    const/16 v3, 0x28

    if-le p2, v3, :cond_3

    .line 41
    div-int/lit8 v3, p2, 0x8

    add-int v4, p1, v3

    mul-int/lit8 v5, v3, 0x2

    add-int v6, p1, v5

    .line 42
    invoke-static {p0, p1, v4, v6}, Lcom/intsig/office/java/util/Arrays;->med3([SIII)I

    move-result v4

    sub-int v6, v1, v3

    add-int v7, v1, v3

    .line 43
    invoke-static {p0, v6, v1, v7}, Lcom/intsig/office/java/util/Arrays;->med3([SIII)I

    move-result v1

    sub-int v5, v0, v5

    sub-int v3, v0, v3

    .line 44
    invoke-static {p0, v5, v3, v0}, Lcom/intsig/office/java/util/Arrays;->med3([SIII)I

    move-result v0

    goto :goto_2

    :cond_3
    move v4, p1

    .line 45
    :goto_2
    invoke-static {p0, v4, v1, v0}, Lcom/intsig/office/java/util/Arrays;->med3([SIII)I

    move-result v1

    .line 46
    :cond_4
    aget-short v0, p0, v1

    add-int/2addr p2, p1

    add-int/lit8 v1, p2, -0x1

    move v4, p1

    move v5, v4

    move v3, v1

    :goto_3
    if-gt v4, v1, :cond_6

    .line 47
    aget-short v6, p0, v4

    if-gt v6, v0, :cond_6

    if-ne v6, v0, :cond_5

    add-int/lit8 v6, v5, 0x1

    .line 48
    invoke-static {p0, v5, v4}, Lcom/intsig/office/java/util/Arrays;->swap([SII)V

    move v5, v6

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    if-lt v1, v4, :cond_8

    .line 49
    aget-short v6, p0, v1

    if-lt v6, v0, :cond_8

    if-ne v6, v0, :cond_7

    add-int/lit8 v6, v3, -0x1

    .line 50
    invoke-static {p0, v1, v3}, Lcom/intsig/office/java/util/Arrays;->swap([SII)V

    move v3, v6

    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    :cond_8
    if-le v4, v1, :cond_b

    sub-int v0, v5, p1

    sub-int v5, v4, v5

    .line 51
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v6, v4, v0

    .line 52
    invoke-static {p0, p1, v6, v0}, Lcom/intsig/office/java/util/Arrays;->vecswap([SIII)V

    sub-int v0, v3, v1

    sub-int v1, p2, v3

    sub-int/2addr v1, v2

    .line 53
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v3, p2, v1

    .line 54
    invoke-static {p0, v4, v3, v1}, Lcom/intsig/office/java/util/Arrays;->vecswap([SIII)V

    if-le v5, v2, :cond_9

    .line 55
    invoke-static {p0, p1, v5}, Lcom/intsig/office/java/util/Arrays;->sort1([SII)V

    :cond_9
    if-le v0, v2, :cond_a

    sub-int/2addr p2, v0

    .line 56
    invoke-static {p0, p2, v0}, Lcom/intsig/office/java/util/Arrays;->sort1([SII)V

    :cond_a
    return-void

    :cond_b
    add-int/lit8 v6, v4, 0x1

    add-int/lit8 v7, v1, -0x1

    .line 57
    invoke-static {p0, v4, v1}, Lcom/intsig/office/java/util/Arrays;->swap([SII)V

    move v4, v6

    move v1, v7

    goto :goto_3
.end method

.method private static sort2([DII)V
    .locals 12

    const-wide/high16 v0, -0x8000000000000000L

    .line 1
    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const/4 v4, 0x0

    move v5, p1

    const/4 v6, 0x0

    :goto_0
    const-wide/16 v7, 0x0

    if-ge v5, p2, :cond_2

    .line 2
    aget-wide v9, p0, v5

    cmpl-double v11, v9, v9

    if-eqz v11, :cond_0

    add-int/lit8 p2, p2, -0x1

    .line 3
    aget-wide v7, p0, p2

    aput-wide v7, p0, v5

    .line 4
    aput-wide v9, p0, p2

    goto :goto_0

    :cond_0
    cmpl-double v11, v9, v7

    if-nez v11, :cond_1

    .line 5
    invoke-static {v9, v10}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v9

    cmp-long v11, v9, v2

    if-nez v11, :cond_1

    .line 6
    aput-wide v7, p0, v5

    add-int/lit8 v6, v6, 0x1

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    sub-int v2, p2, p1

    .line 7
    invoke-static {p0, p1, v2}, Lcom/intsig/office/java/util/Arrays;->sort1([DII)V

    if-eqz v6, :cond_5

    .line 8
    invoke-static {p0, p1, p2, v7, v8}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([DIID)I

    move-result p1

    :cond_3
    add-int/lit8 p1, p1, -0x1

    if-ltz p1, :cond_4

    .line 9
    aget-wide v2, p0, p1

    cmpl-double p2, v2, v7

    if-eqz p2, :cond_3

    :cond_4
    :goto_1
    if-ge v4, v6, :cond_5

    add-int/lit8 p1, p1, 0x1

    .line 10
    aput-wide v0, p0, p1

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    return-void
.end method

.method private static sort2([FII)V
    .locals 8

    const/high16 v0, -0x80000000

    .line 11
    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    const/4 v2, 0x0

    move v3, p1

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x0

    if-ge v3, p2, :cond_2

    .line 12
    aget v6, p0, v3

    cmpl-float v7, v6, v6

    if-eqz v7, :cond_0

    add-int/lit8 p2, p2, -0x1

    .line 13
    aget v5, p0, p2

    aput v5, p0, v3

    .line 14
    aput v6, p0, p2

    goto :goto_0

    :cond_0
    cmpl-float v7, v6, v5

    if-nez v7, :cond_1

    .line 15
    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v6

    if-ne v6, v1, :cond_1

    .line 16
    aput v5, p0, v3

    add-int/lit8 v4, v4, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    sub-int v1, p2, p1

    .line 17
    invoke-static {p0, p1, v1}, Lcom/intsig/office/java/util/Arrays;->sort1([FII)V

    if-eqz v4, :cond_5

    .line 18
    invoke-static {p0, p1, p2, v5}, Lcom/intsig/office/java/util/Arrays;->binarySearch0([FIIF)I

    move-result p1

    :cond_3
    add-int/lit8 p1, p1, -0x1

    if-ltz p1, :cond_4

    .line 19
    aget p2, p0, p1

    cmpl-float p2, p2, v5

    if-eqz p2, :cond_3

    :cond_4
    :goto_1
    if-ge v2, v4, :cond_5

    add-int/lit8 p1, p1, 0x1

    .line 20
    aput v0, p0, p1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    return-void
.end method

.method private static swap([BII)V
    .locals 2

    .line 13
    aget-byte v0, p0, p1

    .line 14
    aget-byte v1, p0, p2

    aput-byte v1, p0, p1

    .line 15
    aput-byte v0, p0, p2

    return-void
.end method

.method private static swap([CII)V
    .locals 2

    .line 10
    aget-char v0, p0, p1

    .line 11
    aget-char v1, p0, p2

    aput-char v1, p0, p1

    .line 12
    aput-char v0, p0, p2

    return-void
.end method

.method private static swap([DII)V
    .locals 4

    .line 16
    aget-wide v0, p0, p1

    .line 17
    aget-wide v2, p0, p2

    aput-wide v2, p0, p1

    .line 18
    aput-wide v0, p0, p2

    return-void
.end method

.method private static swap([FII)V
    .locals 2

    .line 19
    aget v0, p0, p1

    .line 20
    aget v1, p0, p2

    aput v1, p0, p1

    .line 21
    aput v0, p0, p2

    return-void
.end method

.method private static swap([III)V
    .locals 2

    .line 4
    aget v0, p0, p1

    .line 5
    aget v1, p0, p2

    aput v1, p0, p1

    .line 6
    aput v0, p0, p2

    return-void
.end method

.method private static swap([JII)V
    .locals 4

    .line 1
    aget-wide v0, p0, p1

    .line 2
    aget-wide v2, p0, p2

    aput-wide v2, p0, p1

    .line 3
    aput-wide v0, p0, p2

    return-void
.end method

.method private static swap([Ljava/lang/Object;II)V
    .locals 2

    .line 22
    aget-object v0, p0, p1

    .line 23
    aget-object v1, p0, p2

    aput-object v1, p0, p1

    .line 24
    aput-object v0, p0, p2

    return-void
.end method

.method private static swap([SII)V
    .locals 2

    .line 7
    aget-short v0, p0, p1

    .line 8
    aget-short v1, p0, p2

    aput-short v1, p0, p1

    .line 9
    aput-short v0, p0, p2

    return-void
.end method

.method public static toString([B)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 25
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 26
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 28
    :goto_0
    aget-byte v3, p0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 29
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toString([C)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 19
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 20
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 22
    :goto_0
    aget-char v3, p0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 23
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 24
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toString([D)Ljava/lang/String;
    .locals 5

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 43
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 44
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 46
    :goto_0
    aget-wide v3, p0, v2

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 47
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 48
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toString([F)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 37
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 38
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 40
    :goto_0
    aget v3, p0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 41
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 42
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toString([I)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 7
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 8
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 10
    :goto_0
    aget v3, p0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 12
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toString([J)Ljava/lang/String;
    .locals 5

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 1
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 2
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 4
    :goto_0
    aget-wide v3, p0, v2

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 5
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 6
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toString([Ljava/lang/Object;)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 49
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 50
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 52
    :goto_0
    aget-object v3, p0, v2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 53
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 54
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toString([S)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 13
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 14
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 16
    :goto_0
    aget-short v3, p0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 17
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 18
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toString([Z)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const-string p0, "null"

    return-object p0

    .line 31
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string p0, "[]"

    return-object p0

    .line 32
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    .line 33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    .line 34
    :goto_0
    aget-boolean v3, p0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    if-ne v2, v0, :cond_2

    const/16 p0, 0x5d

    .line 35
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string v3, ", "

    .line 36
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static vecswap([BIII)V
    .locals 1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 5
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->swap([BII)V

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static vecswap([CIII)V
    .locals 1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 4
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->swap([CII)V

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static vecswap([DIII)V
    .locals 1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 6
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->swap([DII)V

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static vecswap([FIII)V
    .locals 1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 7
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->swap([FII)V

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static vecswap([IIII)V
    .locals 1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 2
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->swap([III)V

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static vecswap([JIII)V
    .locals 1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->swap([JII)V

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static vecswap([SIII)V
    .locals 1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 3
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/util/Arrays;->swap([SII)V

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

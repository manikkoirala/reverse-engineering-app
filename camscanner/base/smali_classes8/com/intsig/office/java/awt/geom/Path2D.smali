.class public abstract Lcom/intsig/office/java/awt/geom/Path2D;
.super Ljava/lang/Object;
.source "Path2D.java"

# interfaces
.implements Lcom/intsig/office/java/awt/Shape;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/java/awt/geom/Path2D$Iterator;,
        Lcom/intsig/office/java/awt/geom/Path2D$Double;,
        Lcom/intsig/office/java/awt/geom/Path2D$Float;
    }
.end annotation


# static fields
.field static final EXPAND_MAX:I = 0x1f4

.field static final INIT_SIZE:I = 0x14

.field private static final SEG_CLOSE:B = 0x4t

.field private static final SEG_CUBICTO:B = 0x3t

.field private static final SEG_LINETO:B = 0x1t

.field private static final SEG_MOVETO:B = 0x0t

.field private static final SEG_QUADTO:B = 0x2t

.field private static final SERIAL_PATH_END:B = 0x61t

.field private static final SERIAL_SEG_CLOSE:B = 0x60t

.field private static final SERIAL_SEG_DBL_CUBICTO:B = 0x53t

.field private static final SERIAL_SEG_DBL_LINETO:B = 0x51t

.field private static final SERIAL_SEG_DBL_MOVETO:B = 0x50t

.field private static final SERIAL_SEG_DBL_QUADTO:B = 0x52t

.field private static final SERIAL_SEG_FLT_CUBICTO:B = 0x43t

.field private static final SERIAL_SEG_FLT_LINETO:B = 0x41t

.field private static final SERIAL_SEG_FLT_MOVETO:B = 0x40t

.field private static final SERIAL_SEG_FLT_QUADTO:B = 0x42t

.field private static final SERIAL_STORAGE_DBL_ARRAY:B = 0x31t

.field private static final SERIAL_STORAGE_FLT_ARRAY:B = 0x30t

.field public static final WIND_EVEN_ODD:I = 0x0

.field public static final WIND_NON_ZERO:I = 0x1


# instance fields
.field transient numCoords:I

.field transient numTypes:I

.field transient pointTypes:[B

.field transient windingRule:I


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(II)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/java/awt/geom/Path2D;->setWindingRule(I)V

    .line 4
    new-array p1, p2, [B

    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    return-void
.end method

.method public static contains(Lcom/intsig/office/java/awt/geom/PathIterator;DD)Z
    .locals 2

    const-wide/16 v0, 0x0

    mul-double p1, p1, v0

    mul-double p3, p3, v0

    add-double/2addr p1, p3

    const/4 p3, 0x0

    cmpl-double p4, p1, v0

    if-nez p4, :cond_1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->getWindingRule()I

    move-result p0

    const/4 p1, 0x1

    if-ne p0, p1, :cond_0

    const/4 p0, -0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    :goto_0
    and-int/2addr p0, p3

    if-eqz p0, :cond_1

    const/4 p3, 0x1

    :cond_1
    return p3
.end method

.method public static contains(Lcom/intsig/office/java/awt/geom/PathIterator;DDDD)Z
    .locals 0

    add-double/2addr p1, p5

    .line 7
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result p1

    const/4 p2, 0x0

    if-nez p1, :cond_2

    add-double/2addr p3, p7

    invoke-static {p3, p4}, Ljava/lang/Double;->isNaN(D)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 p3, 0x0

    cmpg-double p1, p5, p3

    if-lez p1, :cond_2

    cmpg-double p1, p7, p3

    if-gtz p1, :cond_1

    goto :goto_0

    .line 8
    :cond_1
    invoke-interface {p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->getWindingRule()I

    :cond_2
    :goto_0
    return p2
.end method

.method public static contains(Lcom/intsig/office/java/awt/geom/PathIterator;Lcom/intsig/office/java/awt/geom/Point2D;)Z
    .locals 4

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-static {p0, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Path2D;->contains(Lcom/intsig/office/java/awt/geom/PathIterator;DD)Z

    move-result p0

    return p0
.end method

.method public static contains(Lcom/intsig/office/java/awt/geom/PathIterator;Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 9

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-static/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Path2D;->contains(Lcom/intsig/office/java/awt/geom/PathIterator;DDDD)Z

    move-result p0

    return p0
.end method

.method public static intersects(Lcom/intsig/office/java/awt/geom/PathIterator;DDDD)Z
    .locals 0

    add-double/2addr p1, p5

    .line 1
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result p1

    const/4 p2, 0x0

    if-nez p1, :cond_2

    add-double/2addr p3, p7

    invoke-static {p3, p4}, Ljava/lang/Double;->isNaN(D)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 p3, 0x0

    cmpg-double p1, p5, p3

    if-lez p1, :cond_2

    cmpg-double p1, p7, p3

    if-gtz p1, :cond_1

    goto :goto_0

    .line 2
    :cond_1
    invoke-interface {p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->getWindingRule()I

    const/4 p0, 0x1

    return p0

    :cond_2
    :goto_0
    return p2
.end method

.method public static intersects(Lcom/intsig/office/java/awt/geom/PathIterator;Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 9

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-static/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Path2D;->intersects(Lcom/intsig/office/java/awt/geom/PathIterator;DDDD)Z

    move-result p0

    return p0
.end method


# virtual methods
.method abstract append(DD)V
.end method

.method abstract append(FF)V
.end method

.method public final append(Lcom/intsig/office/java/awt/Shape;Z)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-interface {p1, v0}, Lcom/intsig/office/java/awt/Shape;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    .line 3
    .line 4
    .line 5
    move-result-object p1

    .line 6
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/java/awt/geom/Path2D;->append(Lcom/intsig/office/java/awt/geom/PathIterator;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public abstract append(Lcom/intsig/office/java/awt/geom/PathIterator;Z)V
.end method

.method public abstract clone()Ljava/lang/Object;
.end method

.method abstract cloneCoordsDouble(Lcom/intsig/office/java/awt/geom/AffineTransform;)[D
.end method

.method abstract cloneCoordsFloat(Lcom/intsig/office/java/awt/geom/AffineTransform;)[F
.end method

.method public final declared-synchronized closePath()V
    .locals 4

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 3
    .line 4
    const/4 v1, 0x4

    .line 5
    const/4 v2, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 9
    .line 10
    sub-int/2addr v0, v2

    .line 11
    aget-byte v0, v3, v0

    .line 12
    .line 13
    if-eq v0, v1, :cond_1

    .line 14
    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    invoke-virtual {p0, v2, v0}, Lcom/intsig/office/java/awt/geom/Path2D;->needRoom(ZI)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 20
    .line 21
    iget v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 22
    .line 23
    add-int/lit8 v3, v2, 0x1

    .line 24
    .line 25
    iput v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 26
    .line 27
    aput-byte v1, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    .line 29
    :cond_1
    monitor-exit p0

    .line 30
    return-void

    .line 31
    :catchall_0
    move-exception v0

    .line 32
    monitor-exit p0

    .line 33
    throw v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public final contains(DD)Z
    .locals 6

    const-wide/16 v0, 0x0

    mul-double v2, p1, v0

    mul-double v4, p3, v0

    add-double/2addr v2, v4

    const/4 v4, 0x0

    cmpl-double v5, v2, v0

    if-nez v5, :cond_2

    .line 3
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return v4

    .line 4
    :cond_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->windingRule:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    .line 5
    :goto_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/Path2D;->pointCrossings(DD)I

    move-result p1

    and-int/2addr p1, v0

    if-eqz p1, :cond_2

    const/4 v4, 0x1

    :cond_2
    return v4
.end method

.method public final contains(DDDD)Z
    .locals 13

    add-double v5, p1, p5

    .line 10
    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    const/4 v9, 0x0

    if-nez v0, :cond_5

    add-double v7, p3, p7

    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    const-wide/16 v0, 0x0

    cmpg-double v2, p5, v0

    if-lez v2, :cond_4

    cmpg-double v2, p7, v0

    if-gtz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v10, p0

    .line 11
    iget v0, v10, Lcom/intsig/office/java/awt/geom/Path2D;->windingRule:I

    const/4 v11, 0x1

    if-ne v0, v11, :cond_2

    const/4 v0, -0x1

    const/4 v12, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    const/4 v12, 0x2

    :goto_0
    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p3

    .line 12
    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Path2D;->rectCrossings(DDDD)I

    move-result v0

    if-eqz v0, :cond_3

    and-int/2addr v0, v12

    if-eqz v0, :cond_3

    const/4 v9, 0x1

    :cond_3
    return v9

    :cond_4
    :goto_1
    move-object v10, p0

    return v9

    :cond_5
    :goto_2
    move-object v10, p0

    return v9
.end method

.method public final contains(Lcom/intsig/office/java/awt/geom/Point2D;)Z
    .locals 4

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Path2D;->contains(DD)Z

    move-result p1

    return p1
.end method

.method public final contains(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 9

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Path2D;->contains(DDDD)Z

    move-result p1

    return p1
.end method

.method public final declared-synchronized createTransformedShape(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/Shape;
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Path2D;->clone()Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    check-cast v0, Lcom/intsig/office/java/awt/geom/Path2D;

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/office/java/awt/geom/Path2D;->transform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11
    .line 12
    .line 13
    :cond_0
    monitor-exit p0

    .line 14
    return-object v0

    .line 15
    :catchall_0
    move-exception p1

    .line 16
    monitor-exit p0

    .line 17
    throw p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public abstract curveTo(DDDDDD)V
.end method

.method public final getBounds()Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    invoke-interface {p0}, Lcom/intsig/office/java/awt/Shape;->getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final declared-synchronized getCurrentPoint()Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 6

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 3
    .line 4
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 5
    .line 6
    const/4 v2, 0x1

    .line 7
    if-lt v1, v2, :cond_5

    .line 8
    .line 9
    if-ge v0, v2, :cond_0

    .line 10
    .line 11
    goto :goto_2

    .line 12
    :cond_0
    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 13
    .line 14
    add-int/lit8 v4, v1, -0x1

    .line 15
    .line 16
    aget-byte v3, v3, v4

    .line 17
    .line 18
    const/4 v4, 0x4

    .line 19
    const/4 v5, 0x2

    .line 20
    if-ne v3, v4, :cond_4

    .line 21
    .line 22
    sub-int/2addr v1, v5

    .line 23
    :goto_0
    if-lez v1, :cond_4

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 26
    .line 27
    aget-byte v3, v3, v1

    .line 28
    .line 29
    if-eqz v3, :cond_4

    .line 30
    .line 31
    if-eq v3, v2, :cond_3

    .line 32
    .line 33
    if-eq v3, v5, :cond_2

    .line 34
    .line 35
    const/4 v4, 0x3

    .line 36
    if-eq v3, v4, :cond_1

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_1
    add-int/lit8 v0, v0, -0x6

    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_2
    add-int/lit8 v0, v0, -0x4

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_3
    add-int/lit8 v0, v0, -0x2

    .line 46
    .line 47
    :goto_1
    add-int/lit8 v1, v1, -0x1

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_4
    sub-int/2addr v0, v5

    .line 51
    invoke-virtual {p0, v0}, Lcom/intsig/office/java/awt/geom/Path2D;->getPoint(I)Lcom/intsig/office/java/awt/geom/Point2D;

    .line 52
    .line 53
    .line 54
    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    monitor-exit p0

    .line 56
    return-object v0

    .line 57
    :cond_5
    :goto_2
    monitor-exit p0

    .line 58
    const/4 v0, 0x0

    .line 59
    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    .line 61
    monitor-exit p0

    .line 62
    throw v0
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;D)Lcom/intsig/office/java/awt/geom/PathIterator;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/FlatteningPathIterator;

    .line 2
    .line 3
    invoke-interface {p0, p1}, Lcom/intsig/office/java/awt/Shape;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/java/awt/geom/FlatteningPathIterator;-><init>(Lcom/intsig/office/java/awt/geom/PathIterator;D)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method abstract getPoint(I)Lcom/intsig/office/java/awt/geom/Point2D;
.end method

.method public final declared-synchronized getWindingRule()I
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->windingRule:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    monitor-exit p0

    .line 5
    return v0

    .line 6
    :catchall_0
    move-exception v0

    .line 7
    monitor-exit p0

    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final intersects(DDDD)Z
    .locals 13

    add-double v5, p1, p5

    .line 4
    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    const/4 v9, 0x0

    if-nez v0, :cond_6

    add-double v7, p3, p7

    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    const-wide/16 v0, 0x0

    cmpg-double v2, p5, v0

    if-lez v2, :cond_5

    cmpg-double v2, p7, v0

    if-gtz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v10, p0

    .line 5
    iget v0, v10, Lcom/intsig/office/java/awt/geom/Path2D;->windingRule:I

    const/4 v11, 0x1

    if-ne v0, v11, :cond_2

    const/4 v0, -0x1

    const/4 v12, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    const/4 v12, 0x2

    :goto_0
    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p3

    .line 6
    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Path2D;->rectCrossings(DDDD)I

    move-result v0

    if-eqz v0, :cond_3

    and-int/2addr v0, v12

    if-eqz v0, :cond_4

    :cond_3
    const/4 v9, 0x1

    :cond_4
    return v9

    :cond_5
    :goto_1
    move-object v10, p0

    return v9

    :cond_6
    :goto_2
    move-object v10, p0

    return v9
.end method

.method public final intersects(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 9

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Path2D;->intersects(DDDD)Z

    move-result p1

    return p1
.end method

.method public abstract lineTo(DD)V
.end method

.method public abstract moveTo(DD)V
.end method

.method abstract needRoom(ZI)V
.end method

.method abstract pointCrossings(DD)I
.end method

.method public abstract quadTo(DDDD)V
.end method

.method final readObject(Ljava/io/ObjectInputStream;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readByte()B

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    :try_start_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readByte()B

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-virtual {p0, v2}, Lcom/intsig/office/java/awt/geom/Path2D;->setWindingRule(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    .line 22
    if-gez v0, :cond_0

    .line 23
    .line 24
    const/16 v2, 0x14

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    move v2, v0

    .line 28
    :goto_0
    new-array v2, v2, [B

    .line 29
    .line 30
    iput-object v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 31
    .line 32
    if-gez v1, :cond_1

    .line 33
    .line 34
    const/16 v1, 0x28

    .line 35
    .line 36
    :cond_1
    if-eqz p2, :cond_2

    .line 37
    .line 38
    move-object p2, p0

    .line 39
    check-cast p2, Lcom/intsig/office/java/awt/geom/Path2D$Double;

    .line 40
    .line 41
    new-array v1, v1, [D

    .line 42
    .line 43
    iput-object v1, p2, Lcom/intsig/office/java/awt/geom/Path2D$Double;->doubleCoords:[D

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_2
    move-object p2, p0

    .line 47
    check-cast p2, Lcom/intsig/office/java/awt/geom/Path2D$Float;

    .line 48
    .line 49
    new-array v1, v1, [F

    .line 50
    .line 51
    iput-object v1, p2, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 52
    .line 53
    :goto_1
    const/4 p2, 0x0

    .line 54
    const/4 v1, 0x0

    .line 55
    :goto_2
    const/16 v2, 0x61

    .line 56
    .line 57
    if-ltz v0, :cond_3

    .line 58
    .line 59
    if-ge v1, v0, :cond_5

    .line 60
    .line 61
    :cond_3
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readByte()B

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    const/16 v4, 0x60

    .line 66
    .line 67
    const/4 v5, 0x1

    .line 68
    if-eq v3, v4, :cond_9

    .line 69
    .line 70
    if-eq v3, v2, :cond_4

    .line 71
    .line 72
    const/4 v2, 0x3

    .line 73
    const/4 v4, 0x2

    .line 74
    packed-switch v3, :pswitch_data_0

    .line 75
    .line 76
    .line 77
    packed-switch v3, :pswitch_data_1

    .line 78
    .line 79
    .line 80
    new-instance p1, Ljava/io/StreamCorruptedException;

    .line 81
    .line 82
    const-string p2, "unrecognized path type"

    .line 83
    .line 84
    invoke-direct {p1, p2}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    throw p1

    .line 88
    :pswitch_0
    const/4 v3, 0x1

    .line 89
    goto :goto_4

    .line 90
    :pswitch_1
    const/4 v2, 0x2

    .line 91
    const/4 v3, 0x1

    .line 92
    goto :goto_8

    .line 93
    :pswitch_2
    const/4 v2, 0x1

    .line 94
    goto :goto_3

    .line 95
    :pswitch_3
    const/4 v2, 0x0

    .line 96
    :goto_3
    const/4 v3, 0x1

    .line 97
    goto :goto_6

    .line 98
    :pswitch_4
    const/4 v3, 0x0

    .line 99
    :goto_4
    const/4 v4, 0x3

    .line 100
    goto :goto_8

    .line 101
    :pswitch_5
    const/4 v2, 0x2

    .line 102
    const/4 v3, 0x0

    .line 103
    goto :goto_8

    .line 104
    :pswitch_6
    const/4 v2, 0x1

    .line 105
    goto :goto_5

    .line 106
    :pswitch_7
    const/4 v2, 0x0

    .line 107
    :goto_5
    const/4 v3, 0x0

    .line 108
    :goto_6
    const/4 v4, 0x1

    .line 109
    goto :goto_8

    .line 110
    :cond_4
    if-gez v0, :cond_8

    .line 111
    .line 112
    :cond_5
    if-ltz v0, :cond_7

    .line 113
    .line 114
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readByte()B

    .line 115
    .line 116
    .line 117
    move-result p1

    .line 118
    if-ne p1, v2, :cond_6

    .line 119
    .line 120
    goto :goto_7

    .line 121
    :cond_6
    new-instance p1, Ljava/io/StreamCorruptedException;

    .line 122
    .line 123
    const-string p2, "missing PATH_END"

    .line 124
    .line 125
    invoke-direct {p1, p2}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    throw p1

    .line 129
    :cond_7
    :goto_7
    return-void

    .line 130
    :cond_8
    new-instance p1, Ljava/io/StreamCorruptedException;

    .line 131
    .line 132
    const-string p2, "unexpected PATH_END"

    .line 133
    .line 134
    invoke-direct {p1, p2}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    throw p1

    .line 138
    :cond_9
    const/4 v2, 0x4

    .line 139
    const/4 v3, 0x0

    .line 140
    const/4 v4, 0x0

    .line 141
    :goto_8
    if-eqz v2, :cond_a

    .line 142
    .line 143
    goto :goto_9

    .line 144
    :cond_a
    const/4 v5, 0x0

    .line 145
    :goto_9
    mul-int/lit8 v6, v4, 0x2

    .line 146
    .line 147
    invoke-virtual {p0, v5, v6}, Lcom/intsig/office/java/awt/geom/Path2D;->needRoom(ZI)V

    .line 148
    .line 149
    .line 150
    if-eqz v3, :cond_b

    .line 151
    .line 152
    :goto_a
    add-int/lit8 v4, v4, -0x1

    .line 153
    .line 154
    if-ltz v4, :cond_c

    .line 155
    .line 156
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readDouble()D

    .line 157
    .line 158
    .line 159
    move-result-wide v5

    .line 160
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readDouble()D

    .line 161
    .line 162
    .line 163
    move-result-wide v7

    .line 164
    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/intsig/office/java/awt/geom/Path2D;->append(DD)V

    .line 165
    .line 166
    .line 167
    goto :goto_a

    .line 168
    :cond_b
    :goto_b
    add-int/lit8 v4, v4, -0x1

    .line 169
    .line 170
    if-ltz v4, :cond_c

    .line 171
    .line 172
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readFloat()F

    .line 173
    .line 174
    .line 175
    move-result v3

    .line 176
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readFloat()F

    .line 177
    .line 178
    .line 179
    move-result v5

    .line 180
    invoke-virtual {p0, v3, v5}, Lcom/intsig/office/java/awt/geom/Path2D;->append(FF)V

    .line 181
    .line 182
    .line 183
    goto :goto_b

    .line 184
    :cond_c
    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 185
    .line 186
    iget v4, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 187
    .line 188
    add-int/lit8 v5, v4, 0x1

    .line 189
    .line 190
    iput v5, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 191
    .line 192
    aput-byte v2, v3, v4

    .line 193
    .line 194
    add-int/lit8 v1, v1, 0x1

    .line 195
    .line 196
    goto/16 :goto_2

    .line 197
    .line 198
    :catch_0
    move-exception p1

    .line 199
    new-instance p2, Ljava/io/InvalidObjectException;

    .line 200
    .line 201
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object p1

    .line 205
    invoke-direct {p2, p1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    .line 206
    .line 207
    .line 208
    throw p2

    .line 209
    :pswitch_data_0
    .packed-switch 0x40
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    :pswitch_data_1
    .packed-switch 0x50
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method abstract rectCrossings(DDDD)I
.end method

.method public final declared-synchronized reset()V
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    const/4 v0, 0x0

    .line 3
    :try_start_0
    iput v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 4
    .line 5
    iput v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    .line 7
    monitor-exit p0

    .line 8
    return-void

    .line 9
    :catchall_0
    move-exception v0

    .line 10
    monitor-exit p0

    .line 11
    throw v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public final setWindingRule(I)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-ne p1, v0, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 8
    .line 9
    const-string v0, "winding rule must be WIND_EVEN_ODD or WIND_NON_ZERO"

    .line 10
    .line 11
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    throw p1

    .line 15
    :cond_1
    :goto_0
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->windingRule:I

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public abstract transform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
.end method

.method final writeObject(Ljava/io/ObjectOutputStream;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p2, :cond_0

    .line 6
    .line 7
    move-object v1, p0

    .line 8
    check-cast v1, Lcom/intsig/office/java/awt/geom/Path2D$Double;

    .line 9
    .line 10
    iget-object v1, v1, Lcom/intsig/office/java/awt/geom/Path2D$Double;->doubleCoords:[D

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v1, p0

    .line 14
    check-cast v1, Lcom/intsig/office/java/awt/geom/Path2D$Float;

    .line 15
    .line 16
    iget-object v1, v1, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 17
    .line 18
    move-object v10, v1

    .line 19
    move-object v1, v0

    .line 20
    move-object v0, v10

    .line 21
    :goto_0
    iget v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 22
    .line 23
    if-eqz p2, :cond_1

    .line 24
    .line 25
    const/16 v3, 0x31

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_1
    const/16 v3, 0x30

    .line 29
    .line 30
    :goto_1
    invoke-virtual {p1, v3}, Ljava/io/ObjectOutputStream;->writeByte(I)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 34
    .line 35
    .line 36
    iget v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 37
    .line 38
    invoke-virtual {p1, v3}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 39
    .line 40
    .line 41
    iget v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->windingRule:I

    .line 42
    .line 43
    int-to-byte v3, v3

    .line 44
    invoke-virtual {p1, v3}, Ljava/io/ObjectOutputStream;->writeByte(I)V

    .line 45
    .line 46
    .line 47
    const/4 v3, 0x0

    .line 48
    const/4 v4, 0x0

    .line 49
    const/4 v5, 0x0

    .line 50
    :goto_2
    if-ge v4, v2, :cond_d

    .line 51
    .line 52
    iget-object v6, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 53
    .line 54
    aget-byte v6, v6, v4

    .line 55
    .line 56
    const/4 v7, 0x1

    .line 57
    if-eqz v6, :cond_9

    .line 58
    .line 59
    if-eq v6, v7, :cond_7

    .line 60
    .line 61
    const/4 v7, 0x2

    .line 62
    if-eq v6, v7, :cond_5

    .line 63
    .line 64
    const/4 v7, 0x3

    .line 65
    if-eq v6, v7, :cond_3

    .line 66
    .line 67
    const/4 v7, 0x4

    .line 68
    if-ne v6, v7, :cond_2

    .line 69
    .line 70
    const/16 v6, 0x60

    .line 71
    .line 72
    const/4 v7, 0x0

    .line 73
    goto :goto_3

    .line 74
    :cond_2
    new-instance p1, Ljava/lang/InternalError;

    .line 75
    .line 76
    const-string p2, "unrecognized path type"

    .line 77
    .line 78
    invoke-direct {p1, p2}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    throw p1

    .line 82
    :cond_3
    if-eqz p2, :cond_4

    .line 83
    .line 84
    const/16 v6, 0x53

    .line 85
    .line 86
    goto :goto_3

    .line 87
    :cond_4
    const/16 v6, 0x43

    .line 88
    .line 89
    goto :goto_3

    .line 90
    :cond_5
    if-eqz p2, :cond_6

    .line 91
    .line 92
    const/16 v6, 0x52

    .line 93
    .line 94
    goto :goto_3

    .line 95
    :cond_6
    const/16 v6, 0x42

    .line 96
    .line 97
    goto :goto_3

    .line 98
    :cond_7
    if-eqz p2, :cond_8

    .line 99
    .line 100
    const/16 v6, 0x51

    .line 101
    .line 102
    goto :goto_3

    .line 103
    :cond_8
    const/16 v6, 0x41

    .line 104
    .line 105
    goto :goto_3

    .line 106
    :cond_9
    if-eqz p2, :cond_a

    .line 107
    .line 108
    const/16 v6, 0x50

    .line 109
    .line 110
    goto :goto_3

    .line 111
    :cond_a
    const/16 v6, 0x40

    .line 112
    .line 113
    :goto_3
    invoke-virtual {p1, v6}, Ljava/io/ObjectOutputStream;->writeByte(I)V

    .line 114
    .line 115
    .line 116
    :goto_4
    add-int/lit8 v7, v7, -0x1

    .line 117
    .line 118
    if-ltz v7, :cond_c

    .line 119
    .line 120
    if-eqz p2, :cond_b

    .line 121
    .line 122
    add-int/lit8 v6, v5, 0x1

    .line 123
    .line 124
    aget-wide v8, v1, v5

    .line 125
    .line 126
    invoke-virtual {p1, v8, v9}, Ljava/io/ObjectOutputStream;->writeDouble(D)V

    .line 127
    .line 128
    .line 129
    add-int/lit8 v5, v6, 0x1

    .line 130
    .line 131
    aget-wide v8, v1, v6

    .line 132
    .line 133
    invoke-virtual {p1, v8, v9}, Ljava/io/ObjectOutputStream;->writeDouble(D)V

    .line 134
    .line 135
    .line 136
    goto :goto_4

    .line 137
    :cond_b
    add-int/lit8 v6, v5, 0x1

    .line 138
    .line 139
    aget v5, v0, v5

    .line 140
    .line 141
    invoke-virtual {p1, v5}, Ljava/io/ObjectOutputStream;->writeFloat(F)V

    .line 142
    .line 143
    .line 144
    add-int/lit8 v5, v6, 0x1

    .line 145
    .line 146
    aget v6, v0, v6

    .line 147
    .line 148
    invoke-virtual {p1, v6}, Ljava/io/ObjectOutputStream;->writeFloat(F)V

    .line 149
    .line 150
    .line 151
    goto :goto_4

    .line 152
    :cond_c
    add-int/lit8 v4, v4, 0x1

    .line 153
    .line 154
    goto :goto_2

    .line 155
    :cond_d
    const/16 p2, 0x61

    .line 156
    .line 157
    invoke-virtual {p1, p2}, Ljava/io/ObjectOutputStream;->writeByte(I)V

    .line 158
    .line 159
    .line 160
    return-void
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

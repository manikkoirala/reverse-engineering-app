.class public Lcom/intsig/office/java/awt/geom/Line2D$Float;
.super Lcom/intsig/office/java/awt/geom/Line2D;
.source "Line2D.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/Line2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x55830373efa192bdL


# instance fields
.field public x1:F

.field public x2:F

.field public y1:F

.field public y2:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Line2D;-><init>()V

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Line2D;-><init>()V

    .line 3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/Line2D$Float;->setLine(FFFF)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;)V
    .locals 0

    .line 4
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Line2D;-><init>()V

    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/java/awt/geom/Line2D;->setLine(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;)V

    return-void
.end method


# virtual methods
.method public getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x1:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x2:F

    .line 4
    .line 5
    cmpg-float v2, v0, v1

    .line 6
    .line 7
    if-gez v2, :cond_0

    .line 8
    .line 9
    sub-float/2addr v1, v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    sub-float/2addr v0, v1

    .line 12
    move v5, v1

    .line 13
    move v1, v0

    .line 14
    move v0, v5

    .line 15
    :goto_0
    iget v2, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y1:F

    .line 16
    .line 17
    iget v3, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y2:F

    .line 18
    .line 19
    cmpg-float v4, v2, v3

    .line 20
    .line 21
    if-gez v4, :cond_1

    .line 22
    .line 23
    sub-float/2addr v3, v2

    .line 24
    goto :goto_1

    .line 25
    :cond_1
    sub-float/2addr v2, v3

    .line 26
    move v5, v3

    .line 27
    move v3, v2

    .line 28
    move v2, v5

    .line 29
    :goto_1
    new-instance v4, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 30
    .line 31
    invoke-direct {v4, v0, v2, v1, v3}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 32
    .line 33
    .line 34
    return-object v4
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getP1()Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x1:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y1:F

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>(FF)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getP2()Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x2:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y2:F

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>(FF)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getX1()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x1:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getX2()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x2:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getY1()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y1:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getY2()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y2:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setLine(DDDD)V
    .locals 0

    double-to-float p1, p1

    .line 1
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x1:F

    double-to-float p1, p3

    .line 2
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y1:F

    double-to-float p1, p5

    .line 3
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x2:F

    double-to-float p1, p7

    .line 4
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y2:F

    return-void
.end method

.method public setLine(FFFF)V
    .locals 0

    .line 5
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x1:F

    .line 6
    iput p2, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y1:F

    .line 7
    iput p3, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->x2:F

    .line 8
    iput p4, p0, Lcom/intsig/office/java/awt/geom/Line2D$Float;->y2:F

    return-void
.end method

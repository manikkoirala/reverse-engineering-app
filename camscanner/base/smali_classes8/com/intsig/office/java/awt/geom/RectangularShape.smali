.class public abstract Lcom/intsig/office/java/awt/geom/RectangularShape;
.super Ljava/lang/Object;
.source "RectangularShape.java"

# interfaces
.implements Lcom/intsig/office/java/awt/Shape;
.implements Ljava/lang/Cloneable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    return-object v0

    .line 6
    :catch_0
    new-instance v0, Ljava/lang/InternalError;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    .line 9
    .line 10
    .line 11
    throw v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public contains(Lcom/intsig/office/java/awt/geom/Point2D;)Z
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-interface {p0, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/Shape;->contains(DD)Z

    move-result p1

    return p1
.end method

.method public contains(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 9

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-interface/range {v0 .. v8}, Lcom/intsig/office/java/awt/Shape;->contains(DDDD)Z

    move-result p1

    return p1
.end method

.method public getBounds()Lcom/intsig/office/java/awt/Rectangle;
    .locals 12

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    const-wide/16 v4, 0x0

    .line 10
    .line 11
    cmpg-double v6, v0, v4

    .line 12
    .line 13
    if-ltz v6, :cond_1

    .line 14
    .line 15
    cmpg-double v6, v2, v4

    .line 16
    .line 17
    if-gez v6, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 25
    .line 26
    .line 27
    move-result-wide v6

    .line 28
    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    .line 29
    .line 30
    .line 31
    move-result-wide v8

    .line 32
    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    .line 33
    .line 34
    .line 35
    move-result-wide v10

    .line 36
    add-double/2addr v4, v0

    .line 37
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    .line 38
    .line 39
    .line 40
    move-result-wide v0

    .line 41
    add-double/2addr v6, v2

    .line 42
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    .line 43
    .line 44
    .line 45
    move-result-wide v2

    .line 46
    new-instance v4, Lcom/intsig/office/java/awt/Rectangle;

    .line 47
    .line 48
    double-to-int v5, v8

    .line 49
    double-to-int v6, v10

    .line 50
    sub-double/2addr v0, v8

    .line 51
    double-to-int v0, v0

    .line 52
    sub-double/2addr v2, v10

    .line 53
    double-to-int v1, v2

    .line 54
    invoke-direct {v4, v5, v6, v0, v1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 55
    .line 56
    .line 57
    return-object v4

    .line 58
    :cond_1
    :goto_0
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 59
    .line 60
    invoke-direct {v0}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 61
    .line 62
    .line 63
    return-object v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getCenterX()D
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 10
    .line 11
    div-double/2addr v2, v4

    .line 12
    add-double/2addr v0, v2

    .line 13
    return-wide v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCenterY()D
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 10
    .line 11
    div-double/2addr v2, v4

    .line 12
    add-double/2addr v0, v2

    .line 13
    return-wide v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFrame()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 4
    .line 5
    .line 6
    move-result-wide v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 8
    .line 9
    .line 10
    move-result-wide v3

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 12
    .line 13
    .line 14
    move-result-wide v5

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 16
    .line 17
    .line 18
    move-result-wide v7

    .line 19
    move-object v0, v9

    .line 20
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    .line 21
    .line 22
    .line 23
    return-object v9
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getHeight()D
.end method

.method public getMaxX()D
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    add-double/2addr v0, v2

    .line 10
    return-wide v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMaxY()D
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    add-double/2addr v0, v2

    .line 10
    return-wide v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMinX()D
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMinY()D
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;D)Lcom/intsig/office/java/awt/geom/PathIterator;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/FlatteningPathIterator;

    .line 2
    .line 3
    invoke-interface {p0, p1}, Lcom/intsig/office/java/awt/Shape;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/java/awt/geom/FlatteningPathIterator;-><init>(Lcom/intsig/office/java/awt/geom/PathIterator;D)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public abstract getWidth()D
.end method

.method public abstract getX()D
.end method

.method public abstract getY()D
.end method

.method public intersects(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 2
    .line 3
    .line 4
    move-result-wide v1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 6
    .line 7
    .line 8
    move-result-wide v3

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 10
    .line 11
    .line 12
    move-result-wide v5

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 14
    .line 15
    .line 16
    move-result-wide v7

    .line 17
    move-object v0, p0

    .line 18
    invoke-interface/range {v0 .. v8}, Lcom/intsig/office/java/awt/Shape;->intersects(DDDD)Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    return p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract setFrame(DDDD)V
.end method

.method public setFrame(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Dimension2D;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v3

    invoke-virtual {p2}, Lcom/intsig/office/java/awt/geom/Dimension2D;->getWidth()D

    move-result-wide v5

    invoke-virtual {p2}, Lcom/intsig/office/java/awt/geom/Dimension2D;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/RectangularShape;->setFrame(DDDD)V

    return-void
.end method

.method public setFrame(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V
    .locals 9

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/RectangularShape;->setFrame(DDDD)V

    return-void
.end method

.method public setFrameFromCenter(DDDD)V
    .locals 13

    sub-double v0, p5, p1

    .line 1
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    sub-double v2, p7, p3

    .line 2
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    sub-double v5, p1, v0

    sub-double v7, p3, v2

    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    mul-double v0, v0, v9

    mul-double v11, v2, v9

    move-object v4, p0

    move-wide v9, v0

    .line 3
    invoke-virtual/range {v4 .. v12}, Lcom/intsig/office/java/awt/geom/RectangularShape;->setFrame(DDDD)V

    return-void
.end method

.method public setFrameFromCenter(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;)V
    .locals 9

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v3

    .line 5
    invoke-virtual {p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v5

    invoke-virtual {p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v7

    move-object v0, p0

    .line 6
    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/RectangularShape;->setFrameFromCenter(DDDD)V

    return-void
.end method

.method public setFrameFromDiagonal(DDDD)V
    .locals 12

    cmpg-double v0, p5, p1

    if-gez v0, :cond_0

    move-wide v0, p1

    move-wide/from16 v2, p5

    goto :goto_0

    :cond_0
    move-wide v2, p1

    move-wide/from16 v0, p5

    :goto_0
    cmpg-double v4, p7, p3

    if-gez v4, :cond_1

    move-wide v4, p3

    move-wide/from16 v6, p7

    goto :goto_1

    :cond_1
    move-wide v6, p3

    move-wide/from16 v4, p7

    :goto_1
    sub-double v8, v0, v2

    sub-double v10, v4, v6

    move-object v1, p0

    move-wide v4, v6

    move-wide v6, v8

    move-wide v8, v10

    .line 1
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/office/java/awt/geom/RectangularShape;->setFrame(DDDD)V

    return-void
.end method

.method public setFrameFromDiagonal(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;)V
    .locals 9

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v3

    invoke-virtual {p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v5

    invoke-virtual {p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/RectangularShape;->setFrameFromDiagonal(DDDD)V

    return-void
.end method

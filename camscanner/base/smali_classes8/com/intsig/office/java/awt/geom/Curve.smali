.class public abstract Lcom/intsig/office/java/awt/geom/Curve;
.super Ljava/lang/Object;
.source "Curve.java"


# static fields
.field public static final DECREASING:I = -0x1

.field public static final INCREASING:I = 0x1

.field public static final RECT_INTERSECTS:I = -0x80000000

.field public static final TMIN:D = 0.001


# instance fields
.field protected direction:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Curve;->direction:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static diffbits(DD)J
    .locals 0

    .line 1
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 2
    .line 3
    .line 4
    move-result-wide p0

    .line 5
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 6
    .line 7
    .line 8
    move-result-wide p2

    .line 9
    sub-long/2addr p0, p2

    .line 10
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    .line 11
    .line 12
    .line 13
    move-result-wide p0

    .line 14
    return-wide p0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static insertCubic(Ljava/util/Vector;DD[D)V
    .locals 19

    .line 1
    const/4 v0, 0x5

    .line 2
    aget-wide v16, p5, v0

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    const/4 v1, 0x2

    .line 6
    const/4 v2, 0x4

    .line 7
    const/4 v4, 0x1

    .line 8
    const/4 v5, 0x3

    .line 9
    cmpl-double v3, p3, v16

    .line 10
    .line 11
    if-lez v3, :cond_0

    .line 12
    .line 13
    aget-wide v2, p5, v2

    .line 14
    .line 15
    aget-wide v6, p5, v1

    .line 16
    .line 17
    aget-wide v8, p5, v5

    .line 18
    .line 19
    aget-wide v10, p5, v0

    .line 20
    .line 21
    aget-wide v12, p5, v4

    .line 22
    .line 23
    const/16 v18, -0x1

    .line 24
    .line 25
    move-object/from16 v0, p0

    .line 26
    .line 27
    move-object/from16 v1, p5

    .line 28
    .line 29
    move-wide/from16 v4, v16

    .line 30
    .line 31
    move-wide/from16 v14, p1

    .line 32
    .line 33
    move-wide/from16 v16, p3

    .line 34
    .line 35
    invoke-static/range {v0 .. v18}, Lcom/intsig/office/java/awt/geom/Order3;->insert(Ljava/util/Vector;[DDDDDDDDDI)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    if-nez v3, :cond_1

    .line 40
    .line 41
    aget-wide v6, p5, v4

    .line 42
    .line 43
    cmpl-double v3, p3, v6

    .line 44
    .line 45
    if-nez v3, :cond_1

    .line 46
    .line 47
    aget-wide v6, p5, v5

    .line 48
    .line 49
    cmpl-double v3, p3, v6

    .line 50
    .line 51
    if-nez v3, :cond_1

    .line 52
    .line 53
    return-void

    .line 54
    :cond_1
    aget-wide v6, p5, v0

    .line 55
    .line 56
    aget-wide v8, p5, v4

    .line 57
    .line 58
    aget-wide v10, p5, v1

    .line 59
    .line 60
    aget-wide v12, p5, v5

    .line 61
    .line 62
    aget-wide v14, p5, v2

    .line 63
    .line 64
    const/16 v18, 0x1

    .line 65
    .line 66
    move-object/from16 v0, p0

    .line 67
    .line 68
    move-object/from16 v1, p5

    .line 69
    .line 70
    move-wide/from16 v2, p1

    .line 71
    .line 72
    move-wide/from16 v4, p3

    .line 73
    .line 74
    invoke-static/range {v0 .. v18}, Lcom/intsig/office/java/awt/geom/Order3;->insert(Ljava/util/Vector;[DDDDDDDDDI)V

    .line 75
    .line 76
    .line 77
    :goto_0
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static insertLine(Ljava/util/Vector;DDDD)V
    .locals 12

    .line 1
    move-object v0, p0

    .line 2
    cmpg-double v1, p3, p7

    .line 3
    .line 4
    if-gez v1, :cond_0

    .line 5
    .line 6
    new-instance v11, Lcom/intsig/office/java/awt/geom/Order1;

    .line 7
    .line 8
    const/4 v10, 0x1

    .line 9
    move-object v1, v11

    .line 10
    move-wide v2, p1

    .line 11
    move-wide v4, p3

    .line 12
    move-wide/from16 v6, p5

    .line 13
    .line 14
    move-wide/from16 v8, p7

    .line 15
    .line 16
    invoke-direct/range {v1 .. v10}, Lcom/intsig/office/java/awt/geom/Order1;-><init>(DDDDI)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    cmpl-double v1, p3, p7

    .line 24
    .line 25
    if-lez v1, :cond_1

    .line 26
    .line 27
    new-instance v11, Lcom/intsig/office/java/awt/geom/Order1;

    .line 28
    .line 29
    const/4 v10, -0x1

    .line 30
    move-object v1, v11

    .line 31
    move-wide/from16 v2, p5

    .line 32
    .line 33
    move-wide/from16 v4, p7

    .line 34
    .line 35
    move-wide v6, p1

    .line 36
    move-wide v8, p3

    .line 37
    invoke-direct/range {v1 .. v10}, Lcom/intsig/office/java/awt/geom/Order1;-><init>(DDDDI)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    :cond_1
    :goto_0
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method public static insertMove(Ljava/util/Vector;DD)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Order0;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/Order0;-><init>(DD)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static insertQuad(Ljava/util/Vector;DD[D)V
    .locals 15

    .line 1
    const/4 v0, 0x3

    .line 2
    aget-wide v12, p5, v0

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    const/4 v1, 0x2

    .line 6
    const/4 v2, 0x1

    .line 7
    cmpl-double v3, p3, v12

    .line 8
    .line 9
    if-lez v3, :cond_0

    .line 10
    .line 11
    aget-wide v3, p5, v1

    .line 12
    .line 13
    aget-wide v6, p5, v0

    .line 14
    .line 15
    aget-wide v8, p5, v2

    .line 16
    .line 17
    const/4 v14, -0x1

    .line 18
    move-object v0, p0

    .line 19
    move-object/from16 v1, p5

    .line 20
    .line 21
    move-wide v2, v3

    .line 22
    move-wide v4, v12

    .line 23
    move-wide/from16 v10, p1

    .line 24
    .line 25
    move-wide/from16 v12, p3

    .line 26
    .line 27
    invoke-static/range {v0 .. v14}, Lcom/intsig/office/java/awt/geom/Order2;->insert(Ljava/util/Vector;[DDDDDDDI)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    if-nez v3, :cond_1

    .line 32
    .line 33
    aget-wide v3, p5, v2

    .line 34
    .line 35
    cmpl-double v5, p3, v3

    .line 36
    .line 37
    if-nez v5, :cond_1

    .line 38
    .line 39
    return-void

    .line 40
    :cond_1
    aget-wide v6, p5, v0

    .line 41
    .line 42
    aget-wide v8, p5, v2

    .line 43
    .line 44
    aget-wide v10, p5, v1

    .line 45
    .line 46
    const/4 v14, 0x1

    .line 47
    move-object v0, p0

    .line 48
    move-object/from16 v1, p5

    .line 49
    .line 50
    move-wide/from16 v2, p1

    .line 51
    .line 52
    move-wide/from16 v4, p3

    .line 53
    .line 54
    invoke-static/range {v0 .. v14}, Lcom/intsig/office/java/awt/geom/Order2;->insert(Ljava/util/Vector;[DDDDDDDI)V

    .line 55
    .line 56
    .line 57
    :goto_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static next(D)D
    .locals 2

    .line 1
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 2
    .line 3
    .line 4
    move-result-wide p0

    .line 5
    const-wide/16 v0, 0x1

    .line 6
    .line 7
    add-long/2addr p0, v0

    .line 8
    invoke-static {p0, p1}, Ljava/lang/Double;->longBitsToDouble(J)D

    .line 9
    .line 10
    .line 11
    move-result-wide p0

    .line 12
    return-wide p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static orderof(DD)I
    .locals 1

    .line 1
    cmpg-double v0, p0, p2

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    const/4 p0, -0x1

    .line 6
    return p0

    .line 7
    :cond_0
    cmpl-double v0, p0, p2

    .line 8
    .line 9
    if-lez v0, :cond_1

    .line 10
    .line 11
    const/4 p0, 0x1

    .line 12
    return p0

    .line 13
    :cond_1
    const/4 p0, 0x0

    .line 14
    return p0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static pointCrossingsForCubic(DDDDDDDDDDI)I
    .locals 42

    .line 1
    move/from16 v0, p20

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    cmpg-double v2, p2, p6

    .line 5
    .line 6
    if-gez v2, :cond_0

    .line 7
    .line 8
    cmpg-double v2, p2, p10

    .line 9
    .line 10
    if-gez v2, :cond_0

    .line 11
    .line 12
    cmpg-double v2, p2, p14

    .line 13
    .line 14
    if-gez v2, :cond_0

    .line 15
    .line 16
    cmpg-double v2, p2, p18

    .line 17
    .line 18
    if-gez v2, :cond_0

    .line 19
    .line 20
    return v1

    .line 21
    :cond_0
    cmpl-double v2, p2, p6

    .line 22
    .line 23
    if-ltz v2, :cond_1

    .line 24
    .line 25
    cmpl-double v3, p2, p10

    .line 26
    .line 27
    if-ltz v3, :cond_1

    .line 28
    .line 29
    cmpl-double v3, p2, p14

    .line 30
    .line 31
    if-ltz v3, :cond_1

    .line 32
    .line 33
    cmpl-double v3, p2, p18

    .line 34
    .line 35
    if-ltz v3, :cond_1

    .line 36
    .line 37
    return v1

    .line 38
    :cond_1
    cmpl-double v3, p0, p4

    .line 39
    .line 40
    if-ltz v3, :cond_2

    .line 41
    .line 42
    cmpl-double v3, p0, p8

    .line 43
    .line 44
    if-ltz v3, :cond_2

    .line 45
    .line 46
    cmpl-double v3, p0, p12

    .line 47
    .line 48
    if-ltz v3, :cond_2

    .line 49
    .line 50
    cmpl-double v3, p0, p16

    .line 51
    .line 52
    if-ltz v3, :cond_2

    .line 53
    .line 54
    return v1

    .line 55
    :cond_2
    const/4 v3, 0x1

    .line 56
    cmpg-double v4, p0, p4

    .line 57
    .line 58
    if-gez v4, :cond_5

    .line 59
    .line 60
    cmpg-double v4, p0, p8

    .line 61
    .line 62
    if-gez v4, :cond_5

    .line 63
    .line 64
    cmpg-double v4, p0, p12

    .line 65
    .line 66
    if-gez v4, :cond_5

    .line 67
    .line 68
    cmpg-double v4, p0, p16

    .line 69
    .line 70
    if-gez v4, :cond_5

    .line 71
    .line 72
    if-ltz v2, :cond_3

    .line 73
    .line 74
    cmpg-double v0, p2, p18

    .line 75
    .line 76
    if-gez v0, :cond_4

    .line 77
    .line 78
    return v3

    .line 79
    :cond_3
    cmpl-double v0, p2, p18

    .line 80
    .line 81
    if-ltz v0, :cond_4

    .line 82
    .line 83
    const/4 v0, -0x1

    .line 84
    return v0

    .line 85
    :cond_4
    return v1

    .line 86
    :cond_5
    const/16 v2, 0x34

    .line 87
    .line 88
    if-le v0, v2, :cond_6

    .line 89
    .line 90
    move-wide/from16 v0, p0

    .line 91
    .line 92
    move-wide/from16 v2, p2

    .line 93
    .line 94
    move-wide/from16 v4, p4

    .line 95
    .line 96
    move-wide/from16 v6, p6

    .line 97
    .line 98
    move-wide/from16 v8, p16

    .line 99
    .line 100
    move-wide/from16 v10, p18

    .line 101
    .line 102
    invoke-static/range {v0 .. v11}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForLine(DDDDDD)I

    .line 103
    .line 104
    .line 105
    move-result v0

    .line 106
    return v0

    .line 107
    :cond_6
    add-double v4, p8, p12

    .line 108
    .line 109
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 110
    .line 111
    div-double/2addr v4, v6

    .line 112
    add-double v8, p10, p14

    .line 113
    .line 114
    div-double/2addr v8, v6

    .line 115
    add-double v10, p4, p8

    .line 116
    .line 117
    div-double/2addr v10, v6

    .line 118
    add-double v12, p6, p10

    .line 119
    .line 120
    div-double/2addr v12, v6

    .line 121
    add-double v14, p12, p16

    .line 122
    .line 123
    div-double v33, v14, v6

    .line 124
    .line 125
    add-double v14, p14, p18

    .line 126
    .line 127
    div-double v35, v14, v6

    .line 128
    .line 129
    add-double v14, v10, v4

    .line 130
    .line 131
    div-double/2addr v14, v6

    .line 132
    add-double v16, v12, v8

    .line 133
    .line 134
    div-double v16, v16, v6

    .line 135
    .line 136
    add-double v4, v4, v33

    .line 137
    .line 138
    div-double v29, v4, v6

    .line 139
    .line 140
    add-double v8, v8, v35

    .line 141
    .line 142
    div-double v31, v8, v6

    .line 143
    .line 144
    add-double v4, v14, v29

    .line 145
    .line 146
    div-double v25, v4, v6

    .line 147
    .line 148
    add-double v4, v16, v31

    .line 149
    .line 150
    div-double v27, v4, v6

    .line 151
    .line 152
    invoke-static/range {v25 .. v26}, Ljava/lang/Double;->isNaN(D)Z

    .line 153
    .line 154
    .line 155
    move-result v2

    .line 156
    if-nez v2, :cond_8

    .line 157
    .line 158
    invoke-static/range {v27 .. v28}, Ljava/lang/Double;->isNaN(D)Z

    .line 159
    .line 160
    .line 161
    move-result v2

    .line 162
    if-eqz v2, :cond_7

    .line 163
    .line 164
    goto :goto_0

    .line 165
    :cond_7
    add-int/lit8 v20, v0, 0x1

    .line 166
    .line 167
    move/from16 v41, v20

    .line 168
    .line 169
    move-wide/from16 v0, p0

    .line 170
    .line 171
    move-wide/from16 v2, p2

    .line 172
    .line 173
    move-wide/from16 v4, p4

    .line 174
    .line 175
    move-wide/from16 v6, p6

    .line 176
    .line 177
    move-wide v8, v10

    .line 178
    move-wide v10, v12

    .line 179
    move-wide v12, v14

    .line 180
    move-wide/from16 v14, v16

    .line 181
    .line 182
    move-wide/from16 v16, v25

    .line 183
    .line 184
    move-wide/from16 v18, v27

    .line 185
    .line 186
    invoke-static/range {v0 .. v20}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForCubic(DDDDDDDDDDI)I

    .line 187
    .line 188
    .line 189
    move-result v0

    .line 190
    move-wide/from16 v21, p0

    .line 191
    .line 192
    move-wide/from16 v23, p2

    .line 193
    .line 194
    move-wide/from16 v37, p16

    .line 195
    .line 196
    move-wide/from16 v39, p18

    .line 197
    .line 198
    invoke-static/range {v21 .. v41}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForCubic(DDDDDDDDDDI)I

    .line 199
    .line 200
    .line 201
    move-result v1

    .line 202
    add-int/2addr v0, v1

    .line 203
    return v0

    .line 204
    :cond_8
    :goto_0
    return v1
.end method

.method public static pointCrossingsForLine(DDDDDD)I
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    cmpg-double v1, p2, p6

    .line 3
    .line 4
    if-gez v1, :cond_0

    .line 5
    .line 6
    cmpg-double v1, p2, p10

    .line 7
    .line 8
    if-gez v1, :cond_0

    .line 9
    .line 10
    return v0

    .line 11
    :cond_0
    cmpl-double v1, p2, p6

    .line 12
    .line 13
    if-ltz v1, :cond_1

    .line 14
    .line 15
    cmpl-double v1, p2, p10

    .line 16
    .line 17
    if-ltz v1, :cond_1

    .line 18
    .line 19
    return v0

    .line 20
    :cond_1
    cmpl-double v1, p0, p4

    .line 21
    .line 22
    if-ltz v1, :cond_2

    .line 23
    .line 24
    cmpl-double v1, p0, p8

    .line 25
    .line 26
    if-ltz v1, :cond_2

    .line 27
    .line 28
    return v0

    .line 29
    :cond_2
    const/4 v1, 0x1

    .line 30
    const/4 v2, -0x1

    .line 31
    cmpg-double v3, p0, p4

    .line 32
    .line 33
    if-gez v3, :cond_4

    .line 34
    .line 35
    cmpg-double v3, p0, p8

    .line 36
    .line 37
    if-gez v3, :cond_4

    .line 38
    .line 39
    cmpg-double p0, p6, p10

    .line 40
    .line 41
    if-gez p0, :cond_3

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_3
    const/4 v1, -0x1

    .line 45
    :goto_0
    return v1

    .line 46
    :cond_4
    sub-double/2addr p2, p6

    .line 47
    sub-double/2addr p8, p4

    .line 48
    mul-double p2, p2, p8

    .line 49
    .line 50
    sub-double p8, p10, p6

    .line 51
    .line 52
    div-double/2addr p2, p8

    .line 53
    add-double/2addr p4, p2

    .line 54
    cmpl-double p2, p0, p4

    .line 55
    .line 56
    if-ltz p2, :cond_5

    .line 57
    .line 58
    return v0

    .line 59
    :cond_5
    cmpg-double p0, p6, p10

    .line 60
    .line 61
    if-gez p0, :cond_6

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_6
    const/4 v1, -0x1

    .line 65
    :goto_1
    return v1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method public static pointCrossingsForPath(Lcom/intsig/office/java/awt/geom/PathIterator;DD)I
    .locals 34

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->isDone()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    return v2

    .line 11
    :cond_0
    const/4 v1, 0x6

    .line 12
    new-array v1, v1, [D

    .line 13
    .line 14
    invoke-interface {v0, v1}, Lcom/intsig/office/java/awt/geom/PathIterator;->currentSegment([D)I

    .line 15
    .line 16
    .line 17
    move-result v3

    .line 18
    if-nez v3, :cond_a

    .line 19
    .line 20
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->next()V

    .line 21
    .line 22
    .line 23
    aget-wide v3, v1, v2

    .line 24
    .line 25
    const/4 v5, 0x1

    .line 26
    aget-wide v6, v1, v5

    .line 27
    .line 28
    move-wide v12, v3

    .line 29
    move-wide v14, v6

    .line 30
    const/16 v29, 0x0

    .line 31
    .line 32
    :goto_0
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->isDone()Z

    .line 33
    .line 34
    .line 35
    move-result v8

    .line 36
    if-nez v8, :cond_8

    .line 37
    .line 38
    invoke-interface {v0, v1}, Lcom/intsig/office/java/awt/geom/PathIterator;->currentSegment([D)I

    .line 39
    .line 40
    .line 41
    move-result v8

    .line 42
    if-eqz v8, :cond_5

    .line 43
    .line 44
    if-eq v8, v5, :cond_4

    .line 45
    .line 46
    const/4 v9, 0x3

    .line 47
    const/4 v10, 0x2

    .line 48
    if-eq v8, v10, :cond_3

    .line 49
    .line 50
    const/4 v11, 0x4

    .line 51
    if-eq v8, v9, :cond_2

    .line 52
    .line 53
    if-eq v8, v11, :cond_1

    .line 54
    .line 55
    goto/16 :goto_2

    .line 56
    .line 57
    :cond_1
    cmpl-double v8, v14, v6

    .line 58
    .line 59
    if-eqz v8, :cond_7

    .line 60
    .line 61
    move-wide/from16 v8, p1

    .line 62
    .line 63
    move-wide/from16 v10, p3

    .line 64
    .line 65
    move-wide/from16 v16, v3

    .line 66
    .line 67
    move-wide/from16 v18, v6

    .line 68
    .line 69
    invoke-static/range {v8 .. v19}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForLine(DDDDDD)I

    .line 70
    .line 71
    .line 72
    move-result v8

    .line 73
    add-int v29, v29, v8

    .line 74
    .line 75
    goto/16 :goto_1

    .line 76
    .line 77
    :cond_2
    aget-wide v30, v1, v11

    .line 78
    .line 79
    move-wide/from16 v24, v30

    .line 80
    .line 81
    const/4 v8, 0x5

    .line 82
    aget-wide v32, v1, v8

    .line 83
    .line 84
    move-wide/from16 v26, v32

    .line 85
    .line 86
    aget-wide v16, v1, v2

    .line 87
    .line 88
    aget-wide v18, v1, v5

    .line 89
    .line 90
    aget-wide v20, v1, v10

    .line 91
    .line 92
    aget-wide v22, v1, v9

    .line 93
    .line 94
    const/16 v28, 0x0

    .line 95
    .line 96
    move-wide/from16 v8, p1

    .line 97
    .line 98
    move-wide/from16 v10, p3

    .line 99
    .line 100
    invoke-static/range {v8 .. v28}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForCubic(DDDDDDDDDDI)I

    .line 101
    .line 102
    .line 103
    move-result v8

    .line 104
    add-int v29, v29, v8

    .line 105
    .line 106
    move-wide/from16 v12, v30

    .line 107
    .line 108
    move-wide/from16 v14, v32

    .line 109
    .line 110
    goto :goto_2

    .line 111
    :cond_3
    aget-wide v25, v1, v10

    .line 112
    .line 113
    move-wide/from16 v20, v25

    .line 114
    .line 115
    aget-wide v27, v1, v9

    .line 116
    .line 117
    move-wide/from16 v22, v27

    .line 118
    .line 119
    aget-wide v16, v1, v2

    .line 120
    .line 121
    aget-wide v18, v1, v5

    .line 122
    .line 123
    const/16 v24, 0x0

    .line 124
    .line 125
    move-wide/from16 v8, p1

    .line 126
    .line 127
    move-wide/from16 v10, p3

    .line 128
    .line 129
    invoke-static/range {v8 .. v24}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForQuad(DDDDDDDDI)I

    .line 130
    .line 131
    .line 132
    move-result v8

    .line 133
    add-int v29, v29, v8

    .line 134
    .line 135
    move-wide/from16 v12, v25

    .line 136
    .line 137
    move-wide/from16 v14, v27

    .line 138
    .line 139
    goto :goto_2

    .line 140
    :cond_4
    aget-wide v20, v1, v2

    .line 141
    .line 142
    aget-wide v22, v1, v5

    .line 143
    .line 144
    move-wide/from16 v8, p1

    .line 145
    .line 146
    move-wide/from16 v10, p3

    .line 147
    .line 148
    move-wide/from16 v16, v20

    .line 149
    .line 150
    move-wide/from16 v18, v22

    .line 151
    .line 152
    invoke-static/range {v8 .. v19}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForLine(DDDDDD)I

    .line 153
    .line 154
    .line 155
    move-result v8

    .line 156
    add-int v29, v29, v8

    .line 157
    .line 158
    move-wide/from16 v12, v20

    .line 159
    .line 160
    move-wide/from16 v14, v22

    .line 161
    .line 162
    goto :goto_2

    .line 163
    :cond_5
    cmpl-double v8, v14, v6

    .line 164
    .line 165
    if-eqz v8, :cond_6

    .line 166
    .line 167
    move-wide/from16 v8, p1

    .line 168
    .line 169
    move-wide/from16 v10, p3

    .line 170
    .line 171
    move-wide/from16 v16, v3

    .line 172
    .line 173
    move-wide/from16 v18, v6

    .line 174
    .line 175
    invoke-static/range {v8 .. v19}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForLine(DDDDDD)I

    .line 176
    .line 177
    .line 178
    move-result v3

    .line 179
    add-int v29, v29, v3

    .line 180
    .line 181
    :cond_6
    aget-wide v3, v1, v2

    .line 182
    .line 183
    aget-wide v6, v1, v5

    .line 184
    .line 185
    :cond_7
    :goto_1
    move-wide v12, v3

    .line 186
    move-wide v14, v6

    .line 187
    :goto_2
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->next()V

    .line 188
    .line 189
    .line 190
    goto/16 :goto_0

    .line 191
    .line 192
    :cond_8
    cmpl-double v0, v14, v6

    .line 193
    .line 194
    if-eqz v0, :cond_9

    .line 195
    .line 196
    move-wide/from16 v8, p1

    .line 197
    .line 198
    move-wide/from16 v10, p3

    .line 199
    .line 200
    move-wide/from16 v16, v3

    .line 201
    .line 202
    move-wide/from16 v18, v6

    .line 203
    .line 204
    invoke-static/range {v8 .. v19}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForLine(DDDDDD)I

    .line 205
    .line 206
    .line 207
    move-result v0

    .line 208
    add-int v29, v29, v0

    .line 209
    .line 210
    :cond_9
    return v29

    .line 211
    :cond_a
    new-instance v0, Lcom/intsig/office/java/awt/geom/IllegalPathStateException;

    .line 212
    .line 213
    const-string v1, "missing initial moveto in path definition"

    .line 214
    .line 215
    invoke-direct {v0, v1}, Lcom/intsig/office/java/awt/geom/IllegalPathStateException;-><init>(Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    throw v0
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
.end method

.method public static pointCrossingsForQuad(DDDDDDDDI)I
    .locals 34

    .line 1
    move/from16 v0, p16

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    cmpg-double v2, p2, p6

    .line 5
    .line 6
    if-gez v2, :cond_0

    .line 7
    .line 8
    cmpg-double v2, p2, p10

    .line 9
    .line 10
    if-gez v2, :cond_0

    .line 11
    .line 12
    cmpg-double v2, p2, p14

    .line 13
    .line 14
    if-gez v2, :cond_0

    .line 15
    .line 16
    return v1

    .line 17
    :cond_0
    cmpl-double v2, p2, p6

    .line 18
    .line 19
    if-ltz v2, :cond_1

    .line 20
    .line 21
    cmpl-double v3, p2, p10

    .line 22
    .line 23
    if-ltz v3, :cond_1

    .line 24
    .line 25
    cmpl-double v3, p2, p14

    .line 26
    .line 27
    if-ltz v3, :cond_1

    .line 28
    .line 29
    return v1

    .line 30
    :cond_1
    cmpl-double v3, p0, p4

    .line 31
    .line 32
    if-ltz v3, :cond_2

    .line 33
    .line 34
    cmpl-double v3, p0, p8

    .line 35
    .line 36
    if-ltz v3, :cond_2

    .line 37
    .line 38
    cmpl-double v3, p0, p12

    .line 39
    .line 40
    if-ltz v3, :cond_2

    .line 41
    .line 42
    return v1

    .line 43
    :cond_2
    const/4 v3, 0x1

    .line 44
    cmpg-double v4, p0, p4

    .line 45
    .line 46
    if-gez v4, :cond_5

    .line 47
    .line 48
    cmpg-double v4, p0, p8

    .line 49
    .line 50
    if-gez v4, :cond_5

    .line 51
    .line 52
    cmpg-double v4, p0, p12

    .line 53
    .line 54
    if-gez v4, :cond_5

    .line 55
    .line 56
    if-ltz v2, :cond_3

    .line 57
    .line 58
    cmpg-double v0, p2, p14

    .line 59
    .line 60
    if-gez v0, :cond_4

    .line 61
    .line 62
    return v3

    .line 63
    :cond_3
    cmpl-double v0, p2, p14

    .line 64
    .line 65
    if-ltz v0, :cond_4

    .line 66
    .line 67
    const/4 v0, -0x1

    .line 68
    return v0

    .line 69
    :cond_4
    return v1

    .line 70
    :cond_5
    const/16 v2, 0x34

    .line 71
    .line 72
    if-le v0, v2, :cond_6

    .line 73
    .line 74
    move-wide/from16 v0, p0

    .line 75
    .line 76
    move-wide/from16 v2, p2

    .line 77
    .line 78
    move-wide/from16 v4, p4

    .line 79
    .line 80
    move-wide/from16 v6, p6

    .line 81
    .line 82
    move-wide/from16 v8, p12

    .line 83
    .line 84
    move-wide/from16 v10, p14

    .line 85
    .line 86
    invoke-static/range {v0 .. v11}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForLine(DDDDDD)I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    return v0

    .line 91
    :cond_6
    add-double v4, p4, p8

    .line 92
    .line 93
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 94
    .line 95
    div-double v8, v4, v6

    .line 96
    .line 97
    add-double v4, p6, p10

    .line 98
    .line 99
    div-double v10, v4, v6

    .line 100
    .line 101
    add-double v4, p8, p12

    .line 102
    .line 103
    div-double v25, v4, v6

    .line 104
    .line 105
    add-double v4, p10, p14

    .line 106
    .line 107
    div-double v27, v4, v6

    .line 108
    .line 109
    add-double v4, v8, v25

    .line 110
    .line 111
    div-double v21, v4, v6

    .line 112
    .line 113
    add-double v4, v10, v27

    .line 114
    .line 115
    div-double v23, v4, v6

    .line 116
    .line 117
    invoke-static/range {v21 .. v22}, Ljava/lang/Double;->isNaN(D)Z

    .line 118
    .line 119
    .line 120
    move-result v2

    .line 121
    if-nez v2, :cond_8

    .line 122
    .line 123
    invoke-static/range {v23 .. v24}, Ljava/lang/Double;->isNaN(D)Z

    .line 124
    .line 125
    .line 126
    move-result v2

    .line 127
    if-eqz v2, :cond_7

    .line 128
    .line 129
    goto :goto_0

    .line 130
    :cond_7
    add-int/lit8 v16, v0, 0x1

    .line 131
    .line 132
    move/from16 v33, v16

    .line 133
    .line 134
    move-wide/from16 v0, p0

    .line 135
    .line 136
    move-wide/from16 v2, p2

    .line 137
    .line 138
    move-wide/from16 v4, p4

    .line 139
    .line 140
    move-wide/from16 v6, p6

    .line 141
    .line 142
    move-wide/from16 v12, v21

    .line 143
    .line 144
    move-wide/from16 v14, v23

    .line 145
    .line 146
    invoke-static/range {v0 .. v16}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForQuad(DDDDDDDDI)I

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    move-wide/from16 v17, p0

    .line 151
    .line 152
    move-wide/from16 v19, p2

    .line 153
    .line 154
    move-wide/from16 v29, p12

    .line 155
    .line 156
    move-wide/from16 v31, p14

    .line 157
    .line 158
    invoke-static/range {v17 .. v33}, Lcom/intsig/office/java/awt/geom/Curve;->pointCrossingsForQuad(DDDDDDDDI)I

    .line 159
    .line 160
    .line 161
    move-result v1

    .line 162
    add-int/2addr v0, v1

    .line 163
    return v0

    .line 164
    :cond_8
    :goto_0
    return v1
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method public static prev(D)D
    .locals 2

    .line 1
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 2
    .line 3
    .line 4
    move-result-wide p0

    .line 5
    const-wide/16 v0, 0x1

    .line 6
    .line 7
    sub-long/2addr p0, v0

    .line 8
    invoke-static {p0, p1}, Ljava/lang/Double;->longBitsToDouble(J)D

    .line 9
    .line 10
    .line 11
    move-result-wide p0

    .line 12
    return-wide p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static rectCrossingsForCubic(IDDDDDDDDDDDDI)I
    .locals 39

    move/from16 v0, p25

    cmpl-double v1, p11, p7

    if-ltz v1, :cond_0

    cmpl-double v2, p15, p7

    if-ltz v2, :cond_0

    cmpl-double v2, p19, p7

    if-ltz v2, :cond_0

    cmpl-double v2, p23, p7

    if-ltz v2, :cond_0

    return p0

    :cond_0
    cmpg-double v2, p11, p3

    if-gtz v2, :cond_1

    cmpg-double v3, p15, p3

    if-gtz v3, :cond_1

    cmpg-double v3, p19, p3

    if-gtz v3, :cond_1

    cmpg-double v3, p23, p3

    if-gtz v3, :cond_1

    return p0

    :cond_1
    cmpg-double v3, p9, p1

    if-gtz v3, :cond_2

    cmpg-double v3, p13, p1

    if-gtz v3, :cond_2

    cmpg-double v3, p17, p1

    if-gtz v3, :cond_2

    cmpg-double v3, p21, p1

    if-gtz v3, :cond_2

    return p0

    :cond_2
    cmpl-double v3, p9, p5

    if-ltz v3, :cond_8

    cmpl-double v3, p13, p5

    if-ltz v3, :cond_8

    cmpl-double v3, p17, p5

    if-ltz v3, :cond_8

    cmpl-double v3, p21, p5

    if-ltz v3, :cond_8

    cmpg-double v0, p11, p23

    if-gez v0, :cond_4

    if-gtz v2, :cond_3

    cmpl-double v0, p23, p3

    if-lez v0, :cond_3

    add-int/lit8 v0, p0, 0x1

    goto :goto_0

    :cond_3
    move/from16 v0, p0

    :goto_0
    cmpg-double v1, p11, p7

    if-gez v1, :cond_7

    cmpl-double v1, p23, p7

    if-ltz v1, :cond_7

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    cmpg-double v0, p23, p11

    if-gez v0, :cond_6

    cmpg-double v0, p23, p3

    if-gtz v0, :cond_5

    cmpl-double v0, p11, p3

    if-lez v0, :cond_5

    add-int/lit8 v0, p0, -0x1

    goto :goto_1

    :cond_5
    move/from16 v0, p0

    :goto_1
    cmpg-double v2, p23, p7

    if-gez v2, :cond_7

    if-ltz v1, :cond_7

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_6
    move/from16 v0, p0

    :cond_7
    :goto_2
    return v0

    :cond_8
    const/high16 v15, -0x80000000

    cmpl-double v1, p9, p1

    if-lez v1, :cond_9

    cmpg-double v1, p9, p5

    if-gez v1, :cond_9

    cmpl-double v1, p11, p3

    if-lez v1, :cond_9

    cmpg-double v1, p11, p7

    if-ltz v1, :cond_a

    :cond_9
    cmpl-double v1, p21, p1

    if-lez v1, :cond_b

    cmpg-double v1, p21, p5

    if-gez v1, :cond_b

    cmpl-double v1, p23, p3

    if-lez v1, :cond_b

    cmpg-double v1, p23, p7

    if-gez v1, :cond_b

    :cond_a
    return v15

    :cond_b
    const/16 v1, 0x34

    if-le v0, v1, :cond_c

    move/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    move-wide/from16 v11, p11

    move-wide/from16 v13, p21

    move-wide/from16 v15, p23

    .line 1
    invoke-static/range {v0 .. v16}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForLine(IDDDDDDDD)I

    move-result v0

    return v0

    :cond_c
    add-double v1, p13, p17

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    add-double v5, p15, p19

    div-double/2addr v5, v3

    add-double v7, p9, p13

    div-double v13, v7, v3

    add-double v7, p11, p15

    div-double v16, v7, v3

    add-double v7, p17, p21

    div-double v26, v7, v3

    add-double v7, p19, p23

    div-double v28, v7, v3

    add-double v7, v13, v1

    div-double v18, v7, v3

    add-double v7, v16, v5

    div-double v20, v7, v3

    add-double v1, v1, v26

    div-double v30, v1, v3

    add-double v5, v5, v28

    div-double v32, v5, v3

    add-double v1, v18, v30

    div-double v34, v1, v3

    add-double v1, v20, v32

    div-double v36, v1, v3

    .line 2
    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static/range {v36 .. v37}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-eqz v1, :cond_d

    goto :goto_3

    :cond_d
    add-int/lit8 v38, v0, 0x1

    move/from16 v25, v38

    move/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    move-wide/from16 v11, p11

    move-wide/from16 v15, v16

    move-wide/from16 v17, v18

    move-wide/from16 v19, v20

    move-wide/from16 v21, v34

    move-wide/from16 v23, v36

    .line 3
    invoke-static/range {v0 .. v25}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForCubic(IDDDDDDDDDDDDI)I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_e

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, v34

    move-wide/from16 v11, v36

    move-wide/from16 v13, v30

    move-wide/from16 v15, v32

    move-wide/from16 v17, v26

    move-wide/from16 v19, v28

    move-wide/from16 v21, p21

    move-wide/from16 v23, p23

    move/from16 v25, v38

    .line 4
    invoke-static/range {v0 .. v25}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForCubic(IDDDDDDDDDDDDI)I

    move-result v0

    :cond_e
    return v0

    :cond_f
    :goto_3
    const/4 v0, 0x0

    return v0
.end method

.method public static rectCrossingsForLine(IDDDDDDDD)I
    .locals 10

    .line 1
    cmpl-double v0, p11, p7

    .line 2
    .line 3
    if-ltz v0, :cond_0

    .line 4
    .line 5
    cmpl-double v1, p15, p7

    .line 6
    .line 7
    if-ltz v1, :cond_0

    .line 8
    .line 9
    return p0

    .line 10
    :cond_0
    cmpg-double v1, p11, p3

    .line 11
    .line 12
    if-gtz v1, :cond_1

    .line 13
    .line 14
    cmpg-double v2, p15, p3

    .line 15
    .line 16
    if-gtz v2, :cond_1

    .line 17
    .line 18
    return p0

    .line 19
    :cond_1
    cmpg-double v2, p9, p1

    .line 20
    .line 21
    if-gtz v2, :cond_2

    .line 22
    .line 23
    cmpg-double v2, p13, p1

    .line 24
    .line 25
    if-gtz v2, :cond_2

    .line 26
    .line 27
    return p0

    .line 28
    :cond_2
    cmpl-double v2, p9, p5

    .line 29
    .line 30
    if-ltz v2, :cond_9

    .line 31
    .line 32
    cmpl-double v2, p13, p5

    .line 33
    .line 34
    if-ltz v2, :cond_9

    .line 35
    .line 36
    cmpg-double v2, p11, p15

    .line 37
    .line 38
    if-gez v2, :cond_4

    .line 39
    .line 40
    if-gtz v1, :cond_3

    .line 41
    .line 42
    add-int/lit8 v0, p0, 0x1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_3
    move v0, p0

    .line 46
    :goto_0
    cmpl-double v1, p15, p7

    .line 47
    .line 48
    if-ltz v1, :cond_8

    .line 49
    .line 50
    add-int/lit8 v0, v0, 0x1

    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_4
    cmpg-double v1, p15, p11

    .line 54
    .line 55
    if-gez v1, :cond_7

    .line 56
    .line 57
    cmpg-double v1, p15, p3

    .line 58
    .line 59
    if-gtz v1, :cond_5

    .line 60
    .line 61
    add-int/lit8 v1, p0, -0x1

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_5
    move v1, p0

    .line 65
    :goto_1
    if-ltz v0, :cond_6

    .line 66
    .line 67
    add-int/lit8 v0, v1, -0x1

    .line 68
    .line 69
    goto :goto_2

    .line 70
    :cond_6
    move v0, v1

    .line 71
    goto :goto_2

    .line 72
    :cond_7
    move v0, p0

    .line 73
    :cond_8
    :goto_2
    return v0

    .line 74
    :cond_9
    const/high16 v2, -0x80000000

    .line 75
    .line 76
    cmpl-double v3, p9, p1

    .line 77
    .line 78
    if-lez v3, :cond_a

    .line 79
    .line 80
    cmpg-double v3, p9, p5

    .line 81
    .line 82
    if-gez v3, :cond_a

    .line 83
    .line 84
    cmpl-double v3, p11, p3

    .line 85
    .line 86
    if-lez v3, :cond_a

    .line 87
    .line 88
    cmpg-double v3, p11, p7

    .line 89
    .line 90
    if-ltz v3, :cond_b

    .line 91
    .line 92
    :cond_a
    cmpl-double v3, p13, p1

    .line 93
    .line 94
    if-lez v3, :cond_c

    .line 95
    .line 96
    cmpg-double v3, p13, p5

    .line 97
    .line 98
    if-gez v3, :cond_c

    .line 99
    .line 100
    cmpl-double v3, p15, p3

    .line 101
    .line 102
    if-lez v3, :cond_c

    .line 103
    .line 104
    cmpg-double v3, p15, p7

    .line 105
    .line 106
    if-gez v3, :cond_c

    .line 107
    .line 108
    :cond_b
    return v2

    .line 109
    :cond_c
    if-gez v1, :cond_d

    .line 110
    .line 111
    sub-double v3, p3, p11

    .line 112
    .line 113
    :goto_3
    sub-double v5, p13, p9

    .line 114
    .line 115
    mul-double v3, v3, v5

    .line 116
    .line 117
    sub-double v5, p15, p11

    .line 118
    .line 119
    div-double/2addr v3, v5

    .line 120
    add-double v3, p9, v3

    .line 121
    .line 122
    goto :goto_4

    .line 123
    :cond_d
    if-lez v0, :cond_e

    .line 124
    .line 125
    sub-double v3, p7, p11

    .line 126
    .line 127
    goto :goto_3

    .line 128
    :cond_e
    move-wide/from16 v3, p9

    .line 129
    .line 130
    :goto_4
    cmpg-double v5, p15, p3

    .line 131
    .line 132
    if-gez v5, :cond_f

    .line 133
    .line 134
    sub-double v6, p3, p15

    .line 135
    .line 136
    :goto_5
    sub-double v8, p9, p13

    .line 137
    .line 138
    mul-double v6, v6, v8

    .line 139
    .line 140
    sub-double v8, p11, p15

    .line 141
    .line 142
    div-double/2addr v6, v8

    .line 143
    add-double v6, p13, v6

    .line 144
    .line 145
    goto :goto_6

    .line 146
    :cond_f
    cmpl-double v6, p15, p7

    .line 147
    .line 148
    if-lez v6, :cond_10

    .line 149
    .line 150
    sub-double v6, p7, p15

    .line 151
    .line 152
    goto :goto_5

    .line 153
    :cond_10
    move-wide/from16 v6, p13

    .line 154
    .line 155
    :goto_6
    cmpg-double v8, v3, p1

    .line 156
    .line 157
    if-gtz v8, :cond_11

    .line 158
    .line 159
    cmpg-double v8, v6, p1

    .line 160
    .line 161
    if-gtz v8, :cond_11

    .line 162
    .line 163
    return p0

    .line 164
    :cond_11
    cmpl-double v8, v3, p5

    .line 165
    .line 166
    if-ltz v8, :cond_18

    .line 167
    .line 168
    cmpl-double v3, v6, p5

    .line 169
    .line 170
    if-ltz v3, :cond_18

    .line 171
    .line 172
    cmpg-double v2, p11, p15

    .line 173
    .line 174
    if-gez v2, :cond_13

    .line 175
    .line 176
    if-gtz v1, :cond_12

    .line 177
    .line 178
    add-int/lit8 v0, p0, 0x1

    .line 179
    .line 180
    goto :goto_7

    .line 181
    :cond_12
    move v0, p0

    .line 182
    :goto_7
    cmpl-double v1, p15, p7

    .line 183
    .line 184
    if-ltz v1, :cond_17

    .line 185
    .line 186
    add-int/lit8 v0, v0, 0x1

    .line 187
    .line 188
    goto :goto_9

    .line 189
    :cond_13
    cmpg-double v1, p15, p11

    .line 190
    .line 191
    if-gez v1, :cond_16

    .line 192
    .line 193
    if-gtz v5, :cond_14

    .line 194
    .line 195
    add-int/lit8 v1, p0, -0x1

    .line 196
    .line 197
    goto :goto_8

    .line 198
    :cond_14
    move v1, p0

    .line 199
    :goto_8
    if-ltz v0, :cond_15

    .line 200
    .line 201
    add-int/lit8 v0, v1, -0x1

    .line 202
    .line 203
    goto :goto_9

    .line 204
    :cond_15
    move v0, v1

    .line 205
    goto :goto_9

    .line 206
    :cond_16
    move v0, p0

    .line 207
    :cond_17
    :goto_9
    return v0

    .line 208
    :cond_18
    return v2
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
.end method

.method public static rectCrossingsForPath(Lcom/intsig/office/java/awt/geom/PathIterator;DDDD)I
    .locals 40

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    cmpg-double v2, p5, p1

    .line 5
    .line 6
    if-lez v2, :cond_f

    .line 7
    .line 8
    cmpg-double v2, p7, p3

    .line 9
    .line 10
    if-gtz v2, :cond_0

    .line 11
    .line 12
    goto/16 :goto_4

    .line 13
    .line 14
    :cond_0
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->isDone()Z

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    if-eqz v2, :cond_1

    .line 19
    .line 20
    return v1

    .line 21
    :cond_1
    const/4 v2, 0x6

    .line 22
    new-array v15, v2, [D

    .line 23
    .line 24
    invoke-interface {v0, v15}, Lcom/intsig/office/java/awt/geom/PathIterator;->currentSegment([D)I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_e

    .line 29
    .line 30
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->next()V

    .line 31
    .line 32
    .line 33
    aget-wide v2, v15, v1

    .line 34
    .line 35
    const/4 v13, 0x1

    .line 36
    aget-wide v4, v15, v13

    .line 37
    .line 38
    move-wide v11, v2

    .line 39
    move-wide/from16 v30, v11

    .line 40
    .line 41
    move-wide/from16 v28, v4

    .line 42
    .line 43
    move-wide/from16 v32, v28

    .line 44
    .line 45
    const/4 v2, 0x0

    .line 46
    :goto_0
    const/high16 v3, -0x80000000

    .line 47
    .line 48
    if-eq v2, v3, :cond_b

    .line 49
    .line 50
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->isDone()Z

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    if-nez v4, :cond_b

    .line 55
    .line 56
    invoke-interface {v0, v15}, Lcom/intsig/office/java/awt/geom/PathIterator;->currentSegment([D)I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    if-eqz v3, :cond_8

    .line 61
    .line 62
    if-eq v3, v13, :cond_7

    .line 63
    .line 64
    const/4 v4, 0x3

    .line 65
    const/4 v5, 0x2

    .line 66
    if-eq v3, v5, :cond_6

    .line 67
    .line 68
    const/4 v6, 0x4

    .line 69
    if-eq v3, v4, :cond_5

    .line 70
    .line 71
    if-eq v3, v6, :cond_2

    .line 72
    .line 73
    move-object/from16 v35, v15

    .line 74
    .line 75
    const/16 v34, 0x1

    .line 76
    .line 77
    goto/16 :goto_3

    .line 78
    .line 79
    :cond_2
    cmpl-double v3, v11, v30

    .line 80
    .line 81
    if-nez v3, :cond_4

    .line 82
    .line 83
    cmpl-double v3, v28, v32

    .line 84
    .line 85
    if-eqz v3, :cond_3

    .line 86
    .line 87
    goto :goto_1

    .line 88
    :cond_3
    move-object/from16 v35, v15

    .line 89
    .line 90
    const/16 v34, 0x1

    .line 91
    .line 92
    goto :goto_2

    .line 93
    :cond_4
    :goto_1
    move-wide/from16 v3, p1

    .line 94
    .line 95
    move-wide/from16 v5, p3

    .line 96
    .line 97
    move-wide/from16 v7, p5

    .line 98
    .line 99
    move-wide/from16 v9, p7

    .line 100
    .line 101
    const/16 v34, 0x1

    .line 102
    .line 103
    move-wide/from16 v13, v28

    .line 104
    .line 105
    move-object/from16 v35, v15

    .line 106
    .line 107
    move-wide/from16 v15, v30

    .line 108
    .line 109
    move-wide/from16 v17, v32

    .line 110
    .line 111
    invoke-static/range {v2 .. v18}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForLine(IDDDDDDDD)I

    .line 112
    .line 113
    .line 114
    move-result v2

    .line 115
    :goto_2
    move-wide/from16 v11, v30

    .line 116
    .line 117
    move-wide/from16 v28, v32

    .line 118
    .line 119
    goto/16 :goto_3

    .line 120
    .line 121
    :cond_5
    move-object/from16 v35, v15

    .line 122
    .line 123
    const/16 v34, 0x1

    .line 124
    .line 125
    aget-wide v36, v35, v6

    .line 126
    .line 127
    move-wide/from16 v23, v36

    .line 128
    .line 129
    const/4 v3, 0x5

    .line 130
    aget-wide v38, v35, v3

    .line 131
    .line 132
    move-wide/from16 v25, v38

    .line 133
    .line 134
    aget-wide v15, v35, v1

    .line 135
    .line 136
    aget-wide v17, v35, v34

    .line 137
    .line 138
    aget-wide v19, v35, v5

    .line 139
    .line 140
    aget-wide v21, v35, v4

    .line 141
    .line 142
    const/16 v27, 0x0

    .line 143
    .line 144
    move-wide/from16 v3, p1

    .line 145
    .line 146
    move-wide/from16 v5, p3

    .line 147
    .line 148
    move-wide/from16 v7, p5

    .line 149
    .line 150
    move-wide/from16 v9, p7

    .line 151
    .line 152
    move-wide/from16 v13, v28

    .line 153
    .line 154
    invoke-static/range {v2 .. v27}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForCubic(IDDDDDDDDDDDDI)I

    .line 155
    .line 156
    .line 157
    move-result v2

    .line 158
    move-wide/from16 v11, v36

    .line 159
    .line 160
    move-wide/from16 v28, v38

    .line 161
    .line 162
    goto/16 :goto_3

    .line 163
    .line 164
    :cond_6
    move-object/from16 v35, v15

    .line 165
    .line 166
    const/16 v34, 0x1

    .line 167
    .line 168
    aget-wide v24, v35, v5

    .line 169
    .line 170
    move-wide/from16 v19, v24

    .line 171
    .line 172
    aget-wide v26, v35, v4

    .line 173
    .line 174
    move-wide/from16 v21, v26

    .line 175
    .line 176
    aget-wide v15, v35, v1

    .line 177
    .line 178
    aget-wide v17, v35, v34

    .line 179
    .line 180
    const/16 v23, 0x0

    .line 181
    .line 182
    move-wide/from16 v3, p1

    .line 183
    .line 184
    move-wide/from16 v5, p3

    .line 185
    .line 186
    move-wide/from16 v7, p5

    .line 187
    .line 188
    move-wide/from16 v9, p7

    .line 189
    .line 190
    move-wide/from16 v13, v28

    .line 191
    .line 192
    invoke-static/range {v2 .. v23}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForQuad(IDDDDDDDDDDI)I

    .line 193
    .line 194
    .line 195
    move-result v2

    .line 196
    move-wide/from16 v11, v24

    .line 197
    .line 198
    move-wide/from16 v28, v26

    .line 199
    .line 200
    goto :goto_3

    .line 201
    :cond_7
    move-object/from16 v35, v15

    .line 202
    .line 203
    const/16 v34, 0x1

    .line 204
    .line 205
    aget-wide v19, v35, v1

    .line 206
    .line 207
    move-wide/from16 v15, v19

    .line 208
    .line 209
    aget-wide v21, v35, v34

    .line 210
    .line 211
    move-wide/from16 v17, v21

    .line 212
    .line 213
    move-wide/from16 v3, p1

    .line 214
    .line 215
    move-wide/from16 v5, p3

    .line 216
    .line 217
    move-wide/from16 v7, p5

    .line 218
    .line 219
    move-wide/from16 v9, p7

    .line 220
    .line 221
    move-wide/from16 v13, v28

    .line 222
    .line 223
    invoke-static/range {v2 .. v18}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForLine(IDDDDDDDD)I

    .line 224
    .line 225
    .line 226
    move-result v2

    .line 227
    move-wide/from16 v11, v19

    .line 228
    .line 229
    move-wide/from16 v28, v21

    .line 230
    .line 231
    goto :goto_3

    .line 232
    :cond_8
    move-object/from16 v35, v15

    .line 233
    .line 234
    const/16 v34, 0x1

    .line 235
    .line 236
    cmpl-double v3, v11, v30

    .line 237
    .line 238
    if-nez v3, :cond_9

    .line 239
    .line 240
    cmpl-double v3, v28, v32

    .line 241
    .line 242
    if-eqz v3, :cond_a

    .line 243
    .line 244
    :cond_9
    move-wide/from16 v3, p1

    .line 245
    .line 246
    move-wide/from16 v5, p3

    .line 247
    .line 248
    move-wide/from16 v7, p5

    .line 249
    .line 250
    move-wide/from16 v9, p7

    .line 251
    .line 252
    move-wide/from16 v13, v28

    .line 253
    .line 254
    move-wide/from16 v15, v30

    .line 255
    .line 256
    move-wide/from16 v17, v32

    .line 257
    .line 258
    invoke-static/range {v2 .. v18}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForLine(IDDDDDDDD)I

    .line 259
    .line 260
    .line 261
    move-result v2

    .line 262
    :cond_a
    aget-wide v11, v35, v1

    .line 263
    .line 264
    aget-wide v28, v35, v34

    .line 265
    .line 266
    move-wide/from16 v30, v11

    .line 267
    .line 268
    move-wide/from16 v32, v28

    .line 269
    .line 270
    :goto_3
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->next()V

    .line 271
    .line 272
    .line 273
    move-object/from16 v15, v35

    .line 274
    .line 275
    const/4 v13, 0x1

    .line 276
    goto/16 :goto_0

    .line 277
    .line 278
    :cond_b
    if-eq v2, v3, :cond_d

    .line 279
    .line 280
    cmpl-double v0, v11, v30

    .line 281
    .line 282
    if-nez v0, :cond_c

    .line 283
    .line 284
    cmpl-double v0, v28, v32

    .line 285
    .line 286
    if-eqz v0, :cond_d

    .line 287
    .line 288
    :cond_c
    move-wide/from16 v3, p1

    .line 289
    .line 290
    move-wide/from16 v5, p3

    .line 291
    .line 292
    move-wide/from16 v7, p5

    .line 293
    .line 294
    move-wide/from16 v9, p7

    .line 295
    .line 296
    move-wide/from16 v13, v28

    .line 297
    .line 298
    move-wide/from16 v15, v30

    .line 299
    .line 300
    move-wide/from16 v17, v32

    .line 301
    .line 302
    invoke-static/range {v2 .. v18}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForLine(IDDDDDDDD)I

    .line 303
    .line 304
    .line 305
    move-result v2

    .line 306
    :cond_d
    return v2

    .line 307
    :cond_e
    new-instance v0, Lcom/intsig/office/java/awt/geom/IllegalPathStateException;

    .line 308
    .line 309
    const-string v1, "missing initial moveto in path definition"

    .line 310
    .line 311
    invoke-direct {v0, v1}, Lcom/intsig/office/java/awt/geom/IllegalPathStateException;-><init>(Ljava/lang/String;)V

    .line 312
    .line 313
    .line 314
    throw v0

    .line 315
    :cond_f
    :goto_4
    return v1
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
.end method

.method public static rectCrossingsForQuad(IDDDDDDDDDDI)I
    .locals 31

    move/from16 v0, p21

    cmpl-double v1, p11, p7

    if-ltz v1, :cond_0

    cmpl-double v2, p15, p7

    if-ltz v2, :cond_0

    cmpl-double v2, p19, p7

    if-ltz v2, :cond_0

    return p0

    :cond_0
    cmpg-double v2, p11, p3

    if-gtz v2, :cond_1

    cmpg-double v3, p15, p3

    if-gtz v3, :cond_1

    cmpg-double v3, p19, p3

    if-gtz v3, :cond_1

    return p0

    :cond_1
    cmpg-double v3, p9, p1

    if-gtz v3, :cond_2

    cmpg-double v3, p13, p1

    if-gtz v3, :cond_2

    cmpg-double v3, p17, p1

    if-gtz v3, :cond_2

    return p0

    :cond_2
    cmpl-double v3, p9, p5

    if-ltz v3, :cond_8

    cmpl-double v3, p13, p5

    if-ltz v3, :cond_8

    cmpl-double v3, p17, p5

    if-ltz v3, :cond_8

    cmpg-double v0, p11, p19

    if-gez v0, :cond_4

    if-gtz v2, :cond_3

    cmpl-double v0, p19, p3

    if-lez v0, :cond_3

    add-int/lit8 v0, p0, 0x1

    goto :goto_0

    :cond_3
    move/from16 v0, p0

    :goto_0
    cmpg-double v1, p11, p7

    if-gez v1, :cond_7

    cmpl-double v1, p19, p7

    if-ltz v1, :cond_7

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    cmpg-double v0, p19, p11

    if-gez v0, :cond_6

    cmpg-double v0, p19, p3

    if-gtz v0, :cond_5

    cmpl-double v0, p11, p3

    if-lez v0, :cond_5

    add-int/lit8 v0, p0, -0x1

    goto :goto_1

    :cond_5
    move/from16 v0, p0

    :goto_1
    cmpg-double v2, p19, p7

    if-gez v2, :cond_7

    if-ltz v1, :cond_7

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_6
    move/from16 v0, p0

    :cond_7
    :goto_2
    return v0

    :cond_8
    const/high16 v15, -0x80000000

    cmpg-double v1, p9, p5

    if-gez v1, :cond_9

    cmpl-double v1, p9, p1

    if-lez v1, :cond_9

    cmpg-double v1, p11, p7

    if-gez v1, :cond_9

    cmpl-double v1, p11, p3

    if-gtz v1, :cond_a

    :cond_9
    cmpg-double v1, p17, p5

    if-gez v1, :cond_b

    cmpl-double v1, p17, p1

    if-lez v1, :cond_b

    cmpg-double v1, p19, p7

    if-gez v1, :cond_b

    cmpl-double v1, p19, p3

    if-lez v1, :cond_b

    :cond_a
    return v15

    :cond_b
    const/16 v1, 0x34

    if-le v0, v1, :cond_c

    move/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    move-wide/from16 v11, p11

    move-wide/from16 v13, p17

    move-wide/from16 v15, p19

    .line 1
    invoke-static/range {v0 .. v16}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForLine(IDDDDDDDD)I

    move-result v0

    return v0

    :cond_c
    add-double v1, p9, p13

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double v13, v1, v3

    add-double v1, p11, p15

    div-double v16, v1, v3

    add-double v1, p13, p17

    div-double v22, v1, v3

    add-double v1, p15, p19

    div-double v24, v1, v3

    add-double v1, v13, v22

    div-double v26, v1, v3

    add-double v1, v16, v24

    div-double v28, v1, v3

    .line 2
    invoke-static/range {v26 .. v27}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-eqz v1, :cond_d

    goto :goto_3

    :cond_d
    add-int/lit8 v30, v0, 0x1

    move/from16 v21, v30

    move/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    move-wide/from16 v11, p11

    move-wide/from16 v15, v16

    move-wide/from16 v17, v26

    move-wide/from16 v19, v28

    .line 3
    invoke-static/range {v0 .. v21}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForQuad(IDDDDDDDDDDI)I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_e

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, v26

    move-wide/from16 v11, v28

    move-wide/from16 v13, v22

    move-wide/from16 v15, v24

    move-wide/from16 v17, p17

    move-wide/from16 v19, p19

    move/from16 v21, v30

    .line 4
    invoke-static/range {v0 .. v21}, Lcom/intsig/office/java/awt/geom/Curve;->rectCrossingsForQuad(IDDDDDDDDDDI)I

    move-result v0

    :cond_e
    return v0

    :cond_f
    :goto_3
    const/4 v0, 0x0

    return v0
.end method

.method public static round(D)D
    .locals 0

    .line 1
    return-wide p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static signeddiffbits(DD)J
    .locals 0

    .line 1
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 2
    .line 3
    .line 4
    move-result-wide p0

    .line 5
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 6
    .line 7
    .line 8
    move-result-wide p2

    .line 9
    sub-long/2addr p0, p2

    .line 10
    return-wide p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public abstract TforY(D)D
.end method

.method public abstract XforT(D)D
.end method

.method public abstract XforY(D)D
.end method

.method public abstract YforT(D)D
.end method

.method public accumulateCrossings(Lcom/intsig/office/java/awt/geom/Crossings;)Z
    .locals 20

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Crossings;->getXHi()D

    .line 4
    .line 5
    .line 6
    move-result-wide v1

    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Curve;->getXMin()D

    .line 8
    .line 9
    .line 10
    move-result-wide v3

    .line 11
    const/4 v5, 0x0

    .line 12
    cmpl-double v6, v3, v1

    .line 13
    .line 14
    if-ltz v6, :cond_0

    .line 15
    .line 16
    return v5

    .line 17
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Crossings;->getXLo()D

    .line 18
    .line 19
    .line 20
    move-result-wide v3

    .line 21
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Crossings;->getYLo()D

    .line 22
    .line 23
    .line 24
    move-result-wide v6

    .line 25
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Crossings;->getYHi()D

    .line 26
    .line 27
    .line 28
    move-result-wide v8

    .line 29
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Curve;->getYTop()D

    .line 30
    .line 31
    .line 32
    move-result-wide v10

    .line 33
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Curve;->getYBot()D

    .line 34
    .line 35
    .line 36
    move-result-wide v12

    .line 37
    cmpg-double v14, v10, v6

    .line 38
    .line 39
    if-gez v14, :cond_2

    .line 40
    .line 41
    cmpg-double v10, v12, v6

    .line 42
    .line 43
    if-gtz v10, :cond_1

    .line 44
    .line 45
    return v5

    .line 46
    :cond_1
    invoke-virtual {v0, v6, v7}, Lcom/intsig/office/java/awt/geom/Curve;->TforY(D)D

    .line 47
    .line 48
    .line 49
    move-result-wide v10

    .line 50
    move-wide v15, v6

    .line 51
    goto :goto_0

    .line 52
    :cond_2
    cmpl-double v6, v10, v8

    .line 53
    .line 54
    if-ltz v6, :cond_3

    .line 55
    .line 56
    return v5

    .line 57
    :cond_3
    const-wide/16 v6, 0x0

    .line 58
    .line 59
    move-wide v15, v10

    .line 60
    move-wide v10, v6

    .line 61
    :goto_0
    cmpl-double v6, v12, v8

    .line 62
    .line 63
    if-lez v6, :cond_4

    .line 64
    .line 65
    invoke-virtual {v0, v8, v9}, Lcom/intsig/office/java/awt/geom/Curve;->TforY(D)D

    .line 66
    .line 67
    .line 68
    move-result-wide v6

    .line 69
    move-wide/from16 v17, v8

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_4
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 73
    .line 74
    move-wide/from16 v17, v12

    .line 75
    .line 76
    :goto_1
    const/4 v8, 0x0

    .line 77
    const/4 v9, 0x0

    .line 78
    :goto_2
    invoke-virtual {v0, v10, v11}, Lcom/intsig/office/java/awt/geom/Curve;->XforT(D)D

    .line 79
    .line 80
    .line 81
    move-result-wide v12

    .line 82
    const/4 v14, 0x1

    .line 83
    cmpg-double v19, v12, v1

    .line 84
    .line 85
    if-gez v19, :cond_7

    .line 86
    .line 87
    if-nez v9, :cond_6

    .line 88
    .line 89
    cmpl-double v8, v12, v3

    .line 90
    .line 91
    if-lez v8, :cond_5

    .line 92
    .line 93
    goto :goto_3

    .line 94
    :cond_5
    const/4 v8, 0x1

    .line 95
    goto :goto_4

    .line 96
    :cond_6
    :goto_3
    return v14

    .line 97
    :cond_7
    if-eqz v8, :cond_8

    .line 98
    .line 99
    return v14

    .line 100
    :cond_8
    const/4 v9, 0x1

    .line 101
    :goto_4
    cmpl-double v12, v10, v6

    .line 102
    .line 103
    if-ltz v12, :cond_a

    .line 104
    .line 105
    if-eqz v8, :cond_9

    .line 106
    .line 107
    iget v1, v0, Lcom/intsig/office/java/awt/geom/Curve;->direction:I

    .line 108
    .line 109
    move-object/from16 v14, p1

    .line 110
    .line 111
    move/from16 v19, v1

    .line 112
    .line 113
    invoke-virtual/range {v14 .. v19}, Lcom/intsig/office/java/awt/geom/Crossings;->record(DDI)V

    .line 114
    .line 115
    .line 116
    :cond_9
    return v5

    .line 117
    :cond_a
    invoke-virtual {v0, v10, v11, v6, v7}, Lcom/intsig/office/java/awt/geom/Curve;->nextVertical(DD)D

    .line 118
    .line 119
    .line 120
    move-result-wide v10

    .line 121
    goto :goto_2
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public compareTo(Lcom/intsig/office/java/awt/geom/Curve;[D)I
    .locals 67

    move-object/from16 v14, p0

    move-object/from16 v0, p1

    const/16 v32, 0x0

    .line 1
    aget-wide v8, p2, v32

    const/16 v33, 0x1

    .line 2
    aget-wide v1, p2, v33

    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Curve;->getYBot()D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Curve;->getYBot()D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    .line 4
    aget-wide v1, p2, v32

    const-string v15, "=>"

    cmpg-double v3, v10, v1

    if-lez v3, :cond_15

    .line 5
    aput-wide v10, p2, v33

    .line 6
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Curve;->getXMax()D

    move-result-wide v1

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Curve;->getXMin()D

    move-result-wide v3

    cmpg-double v5, v1, v3

    if-gtz v5, :cond_1

    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Curve;->getXMin()D

    move-result-wide v1

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Curve;->getXMax()D

    move-result-wide v3

    cmpl-double v0, v1, v3

    if-nez v0, :cond_0

    return v32

    :cond_0
    const/4 v0, -0x1

    return v0

    .line 8
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Curve;->getXMin()D

    move-result-wide v1

    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Curve;->getXMax()D

    move-result-wide v3

    cmpl-double v5, v1, v3

    if-ltz v5, :cond_2

    return v33

    .line 9
    :cond_2
    invoke-virtual {v14, v8, v9}, Lcom/intsig/office/java/awt/geom/Curve;->TforY(D)D

    move-result-wide v2

    .line 10
    invoke-virtual {v14, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    cmpg-double v1, v4, v8

    if-gez v1, :cond_3

    move-object/from16 v1, p0

    move-wide v6, v8

    .line 11
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/office/java/awt/geom/Curve;->refineTforY(DDD)D

    move-result-wide v2

    .line 12
    invoke-virtual {v14, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    :cond_3
    move-wide v12, v2

    move-wide/from16 v16, v4

    .line 13
    invoke-virtual {v14, v10, v11}, Lcom/intsig/office/java/awt/geom/Curve;->TforY(D)D

    move-result-wide v2

    .line 14
    invoke-virtual {v14, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    cmpg-double v1, v4, v8

    if-gez v1, :cond_4

    .line 15
    invoke-virtual {v14, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    move-object/from16 v1, p0

    move-wide v6, v8

    invoke-virtual/range {v1 .. v7}, Lcom/intsig/office/java/awt/geom/Curve;->refineTforY(DDD)D

    move-result-wide v2

    :cond_4
    move-wide v6, v2

    .line 16
    invoke-virtual {v0, v8, v9}, Lcom/intsig/office/java/awt/geom/Curve;->TforY(D)D

    move-result-wide v2

    .line 17
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    cmpg-double v1, v4, v8

    if-gez v1, :cond_5

    move-object/from16 v1, p1

    move-wide/from16 v34, v6

    move-wide v6, v8

    .line 18
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/office/java/awt/geom/Curve;->refineTforY(DDD)D

    move-result-wide v2

    .line 19
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    goto :goto_0

    :cond_5
    move-wide/from16 v34, v6

    :goto_0
    move-wide v6, v2

    move-wide/from16 v18, v4

    .line 20
    invoke-virtual {v0, v10, v11}, Lcom/intsig/office/java/awt/geom/Curve;->TforY(D)D

    move-result-wide v2

    .line 21
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    cmpg-double v1, v4, v8

    if-gez v1, :cond_6

    .line 22
    invoke-virtual {v0, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    move-object/from16 v1, p1

    move-wide/from16 v20, v10

    move-wide v10, v6

    move-wide v6, v8

    invoke-virtual/range {v1 .. v7}, Lcom/intsig/office/java/awt/geom/Curve;->refineTforY(DDD)D

    move-result-wide v2

    goto :goto_1

    :cond_6
    move-wide/from16 v20, v10

    move-wide v10, v6

    :goto_1
    move-wide v4, v2

    .line 23
    invoke-virtual {v14, v12, v13}, Lcom/intsig/office/java/awt/geom/Curve;->XforT(D)D

    move-result-wide v1

    .line 24
    invoke-virtual {v0, v10, v11}, Lcom/intsig/office/java/awt/geom/Curve;->XforT(D)D

    move-result-wide v6

    move-wide/from16 v22, v10

    .line 25
    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    move-wide/from16 v24, v12

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    const-wide v12, 0x3d06849b86a12b9bL    # 1.0E-14

    mul-double v10, v10, v12

    const-wide v12, 0x1a56e1fc2f8f359L    # 1.0E-300

    .line 26
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v12

    .line 27
    invoke-virtual {v14, v1, v2, v6, v7}, Lcom/intsig/office/java/awt/geom/Curve;->fairlyClose(DD)Z

    move-result v3

    const-wide/high16 v36, 0x4000000000000000L    # 2.0

    if-eqz v3, :cond_d

    const-wide v10, 0x42a2309ce5400000L    # 1.0E13

    mul-double v10, v10, v12

    sub-double v26, v20, v8

    const-wide v28, 0x3fb999999999999aL    # 0.1

    move-wide/from16 v30, v1

    mul-double v1, v26, v28

    .line 28
    invoke-static {v10, v11, v1, v2}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    add-double v10, v8, v12

    move-wide/from16 v26, v12

    :goto_2
    cmpg-double v3, v10, v20

    if-gtz v3, :cond_b

    move-wide/from16 v28, v6

    .line 29
    invoke-virtual {v14, v10, v11}, Lcom/intsig/office/java/awt/geom/Curve;->XforY(D)D

    move-result-wide v6

    move-wide/from16 v38, v4

    invoke-virtual {v0, v10, v11}, Lcom/intsig/office/java/awt/geom/Curve;->XforY(D)D

    move-result-wide v3

    invoke-virtual {v14, v6, v7, v3, v4}, Lcom/intsig/office/java/awt/geom/Curve;->fairlyClose(DD)Z

    move-result v3

    if-eqz v3, :cond_8

    mul-double v26, v26, v36

    cmpl-double v3, v26, v1

    if-lez v3, :cond_7

    move-wide/from16 v26, v1

    :cond_7
    add-double v10, v10, v26

    move-wide/from16 v6, v28

    move-wide/from16 v4, v38

    goto :goto_2

    :cond_8
    sub-double v10, v10, v26

    :cond_9
    :goto_3
    div-double v26, v26, v36

    add-double v1, v10, v26

    cmpg-double v3, v1, v10

    if-gtz v3, :cond_a

    goto :goto_4

    .line 30
    :cond_a
    invoke-virtual {v14, v1, v2}, Lcom/intsig/office/java/awt/geom/Curve;->XforY(D)D

    move-result-wide v3

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Curve;->XforY(D)D

    move-result-wide v5

    invoke-virtual {v14, v3, v4, v5, v6}, Lcom/intsig/office/java/awt/geom/Curve;->fairlyClose(DD)Z

    move-result v3

    if-eqz v3, :cond_9

    move-wide v10, v1

    goto :goto_3

    :cond_b
    move-wide/from16 v38, v4

    move-wide/from16 v28, v6

    :goto_4
    cmpl-double v1, v10, v8

    if-lez v1, :cond_e

    cmpg-double v0, v10, v20

    if-gez v0, :cond_c

    .line 31
    aput-wide v10, p2, v33

    :cond_c
    return v32

    :cond_d
    move-wide/from16 v30, v1

    move-wide/from16 v38, v4

    move-wide/from16 v28, v6

    :cond_e
    const-wide/16 v1, 0x0

    cmpg-double v3, v12, v1

    if-gtz v3, :cond_f

    .line 32
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ymin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_f
    move-wide/from16 v4, v16

    move-wide/from16 v2, v18

    move-wide/from16 v10, v22

    move-wide/from16 v8, v24

    move-wide/from16 v40, v28

    move-wide/from16 v6, v34

    move-wide/from16 v34, v30

    :goto_5
    cmpg-double v1, v8, v6

    if-gez v1, :cond_14

    cmpg-double v1, v10, v38

    if-gez v1, :cond_14

    move-wide/from16 v16, v4

    .line 33
    invoke-virtual {v14, v8, v9, v6, v7}, Lcom/intsig/office/java/awt/geom/Curve;->nextVertical(DD)D

    move-result-wide v4

    .line 34
    invoke-virtual {v14, v4, v5}, Lcom/intsig/office/java/awt/geom/Curve;->XforT(D)D

    move-result-wide v42

    move-wide/from16 v20, v6

    move-wide/from16 v18, v8

    .line 35
    invoke-virtual {v14, v4, v5}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v7

    move-wide/from16 v22, v7

    move-object/from16 v24, v15

    move-wide/from16 v7, v38

    .line 36
    invoke-virtual {v0, v10, v11, v7, v8}, Lcom/intsig/office/java/awt/geom/Curve;->nextVertical(DD)D

    move-result-wide v14

    .line 37
    invoke-virtual {v0, v14, v15}, Lcom/intsig/office/java/awt/geom/Curve;->XforT(D)D

    move-result-wide v38

    move-wide/from16 v25, v7

    .line 38
    invoke-virtual {v0, v14, v15}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v7

    const/4 v6, 0x0

    move-wide/from16 v44, v20

    const/4 v1, 0x0

    move-wide/from16 v46, v7

    move-wide/from16 v8, v22

    move-wide/from16 v20, v25

    move v7, v1

    move-object/from16 v1, p0

    move-wide/from16 v48, v2

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v50, v4

    move-wide/from16 v52, v16

    move-wide/from16 v54, v20

    move-wide v4, v12

    move-wide/from16 v58, v8

    move-wide/from16 v56, v18

    move-wide/from16 v8, v56

    move-wide/from16 v60, v10

    move-wide/from16 v10, v34

    move-wide/from16 v62, v12

    move-wide/from16 v12, v52

    move-wide/from16 v64, v14

    move-object/from16 v66, v24

    move-wide/from16 v14, v50

    move-wide/from16 v16, v42

    move-wide/from16 v18, v58

    move-wide/from16 v20, v60

    move-wide/from16 v22, v40

    move-wide/from16 v24, v48

    move-wide/from16 v26, v64

    move-wide/from16 v28, v38

    move-wide/from16 v30, v46

    .line 39
    :try_start_0
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_10

    goto/16 :goto_7

    :cond_10
    move-wide/from16 v3, v46

    move-wide/from16 v1, v58

    cmpg-double v5, v1, v3

    if-gez v5, :cond_12

    .line 40
    aget-wide v3, p2, v32

    cmpl-double v5, v1, v3

    if-lez v5, :cond_11

    .line 41
    aget-wide v3, p2, v33

    cmpg-double v5, v1, v3

    if-gez v5, :cond_14

    .line 42
    aput-wide v1, p2, v33

    goto/16 :goto_7

    :cond_11
    move-wide v4, v1

    move-wide/from16 v34, v42

    move-wide/from16 v2, v48

    move-wide/from16 v8, v50

    move-wide/from16 v10, v60

    goto :goto_6

    .line 43
    :cond_12
    aget-wide v1, p2, v32

    cmpl-double v5, v3, v1

    if-lez v5, :cond_13

    .line 44
    aget-wide v1, p2, v33

    cmpg-double v5, v3, v1

    if-gez v5, :cond_14

    .line 45
    aput-wide v3, p2, v33

    goto/16 :goto_7

    :cond_13
    move-wide v2, v3

    move-wide/from16 v40, v38

    move-wide/from16 v4, v52

    move-wide/from16 v8, v56

    move-wide/from16 v10, v64

    :goto_6
    move-object/from16 v14, p0

    move-wide/from16 v6, v44

    move-wide/from16 v38, v54

    move-wide/from16 v12, v62

    move-object/from16 v15, v66

    goto/16 :goto_5

    :catchall_0
    move-exception v0

    move-wide/from16 v3, v46

    move-wide/from16 v1, v58

    move-object v5, v0

    .line 46
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 47
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "y range was "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v6, p2, v32

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-object/from16 v6, v66

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v7, p2, v33

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 48
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "s y range is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v7, v52

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 49
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t y range is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v7, v48

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 50
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ymin is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v2, v62

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return v32

    .line 51
    :cond_14
    :goto_7
    aget-wide v1, p2, v32

    aget-wide v3, p2, v33

    add-double/2addr v1, v3

    div-double v1, v1, v36

    move-object/from16 v3, p0

    .line 52
    invoke-virtual {v3, v1, v2}, Lcom/intsig/office/java/awt/geom/Curve;->XforY(D)D

    move-result-wide v4

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Curve;->XforY(D)D

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Lcom/intsig/office/java/awt/geom/Curve;->orderof(DD)I

    move-result v0

    return v0

    :cond_15
    move-wide/from16 v20, v10

    move-object v3, v14

    move-object v6, v15

    .line 53
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "this == "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 54
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "that == "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 55
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "target range = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v4, p2, v32

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v4, p2, v33

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 56
    new-instance v0, Ljava/lang/InternalError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "backstepping from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v4, p2, v32

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v4, v20

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public controlPointString()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public crossingsFor(DD)I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getYTop()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    cmpl-double v2, p3, v0

    .line 6
    .line 7
    if-ltz v2, :cond_1

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getYBot()D

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    cmpg-double v2, p3, v0

    .line 14
    .line 15
    if-gez v2, :cond_1

    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getXMax()D

    .line 18
    .line 19
    .line 20
    move-result-wide v0

    .line 21
    cmpg-double v2, p1, v0

    .line 22
    .line 23
    if-gez v2, :cond_1

    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getXMin()D

    .line 26
    .line 27
    .line 28
    move-result-wide v0

    .line 29
    cmpg-double v2, p1, v0

    .line 30
    .line 31
    if-ltz v2, :cond_0

    .line 32
    .line 33
    invoke-virtual {p0, p3, p4}, Lcom/intsig/office/java/awt/geom/Curve;->XforY(D)D

    .line 34
    .line 35
    .line 36
    move-result-wide p3

    .line 37
    cmpg-double v0, p1, p3

    .line 38
    .line 39
    if-gez v0, :cond_1

    .line 40
    .line 41
    :cond_0
    const/4 p1, 0x1

    .line 42
    return p1

    .line 43
    :cond_1
    const/4 p1, 0x0

    .line 44
    return p1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public abstract dXforT(DI)D
.end method

.method public abstract dYforT(DI)D
.end method

.method public abstract enlarge(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V
.end method

.method public fairlyClose(DD)Z
    .locals 2

    .line 1
    sub-double v0, p1, p3

    .line 2
    .line 3
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    .line 8
    .line 9
    .line 10
    move-result-wide p1

    .line 11
    invoke-static {p3, p4}, Ljava/lang/Math;->abs(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide p3

    .line 15
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->max(DD)D

    .line 16
    .line 17
    .line 18
    move-result-wide p1

    .line 19
    const-wide p3, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    mul-double p1, p1, p3

    .line 25
    .line 26
    cmpg-double p3, v0, p1

    .line 27
    .line 28
    if-gez p3, :cond_0

    .line 29
    .line 30
    const/4 p1, 0x1

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 p1, 0x0

    .line 33
    :goto_0
    return p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z
    .locals 46

    move-object/from16 v14, p0

    move-object/from16 v0, p1

    move-wide/from16 v12, p7

    move-wide/from16 v10, p9

    move-wide/from16 v8, p13

    move-wide/from16 v4, p15

    move-wide/from16 v2, p19

    move-wide/from16 v6, p21

    move-wide/from16 v0, p25

    move-wide/from16 v0, p27

    const/16 v32, 0x0

    cmpl-double v15, p11, p29

    if-gtz v15, :cond_14

    cmpl-double v15, p23, p17

    if-lez v15, :cond_0

    goto/16 :goto_1

    .line 1
    :cond_0
    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v15

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v17

    cmpl-double v19, v15, v17

    if-gtz v19, :cond_13

    .line 2
    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v15

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v17

    cmpg-double v19, v15, v17

    if-gez v19, :cond_1

    goto/16 :goto_0

    :cond_1
    sub-double v15, v8, v12

    const-string v0, "no t progress!"

    const-string v1, "t1 = "

    const-string v4, "t0 = "

    const-wide v17, 0x3f50624dd2f1a9fcL    # 0.001

    const-wide/high16 v19, 0x4000000000000000L    # 2.0

    const/16 v33, 0x1

    cmpl-double v5, v15, v17

    if-lez v5, :cond_a

    add-double v15, v12, v8

    div-double v10, v15, v19

    .line 3
    invoke-virtual {v14, v10, v11}, Lcom/intsig/office/java/awt/geom/Curve;->XforT(D)D

    move-result-wide v34

    .line 4
    invoke-virtual {v14, v10, v11}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v36

    cmpl-double v5, v10, v12

    if-eqz v5, :cond_9

    cmpl-double v5, v10, v8

    if-eqz v5, :cond_9

    sub-double v15, p25, v2

    cmpl-double v5, v15, v17

    if-lez v5, :cond_7

    add-double v15, v2, p25

    div-double v14, v15, v19

    move-object/from16 v5, p1

    move-wide/from16 v12, p25

    .line 5
    invoke-virtual {v5, v14, v15}, Lcom/intsig/office/java/awt/geom/Curve;->XforT(D)D

    move-result-wide v38

    .line 6
    invoke-virtual {v5, v14, v15}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v40

    cmpl-double v16, v14, v2

    if-eqz v16, :cond_6

    cmpl-double v16, v14, v12

    if-eqz v16, :cond_6

    cmpl-double v0, v36, p23

    if-ltz v0, :cond_2

    cmpl-double v0, v40, p11

    if-ltz v0, :cond_2

    add-int/lit8 v0, p5, 0x1

    move v6, v0

    add-int/lit8 v7, p6, 0x1

    move-object/from16 v1, p0

    move-wide v3, v2

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object v0, v5

    move-wide/from16 v4, p3

    move-wide/from16 v8, p7

    move-wide/from16 v42, v10

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-wide/from16 v44, v14

    move-wide/from16 v14, v42

    move-wide/from16 v16, v34

    move-wide/from16 v18, v36

    move-wide/from16 v20, p19

    move-wide/from16 v22, p21

    move-wide/from16 v24, p23

    move-wide/from16 v26, v44

    move-wide/from16 v28, v38

    move-wide/from16 v30, v40

    .line 7
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v1

    if-eqz v1, :cond_3

    return v33

    :cond_2
    move-object v0, v5

    move-wide/from16 v42, v10

    move-wide/from16 v44, v14

    :cond_3
    cmpl-double v1, v36, v40

    if-ltz v1, :cond_4

    add-int/lit8 v6, p5, 0x1

    add-int/lit8 v7, p6, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-wide/from16 v14, v42

    move-wide/from16 v16, v34

    move-wide/from16 v18, v36

    move-wide/from16 v20, v44

    move-wide/from16 v22, v38

    move-wide/from16 v24, v40

    move-wide/from16 v26, p25

    move-wide/from16 v28, p27

    move-wide/from16 v30, p29

    .line 8
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v1

    if-eqz v1, :cond_4

    return v33

    :cond_4
    cmpl-double v1, v40, v36

    if-ltz v1, :cond_5

    add-int/lit8 v6, p5, 0x1

    add-int/lit8 v7, p6, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v8, v42

    move-wide/from16 v10, v34

    move-wide/from16 v12, v36

    move-wide/from16 v14, p13

    move-wide/from16 v16, p15

    move-wide/from16 v18, p17

    move-wide/from16 v20, p19

    move-wide/from16 v22, p21

    move-wide/from16 v24, p23

    move-wide/from16 v26, v44

    move-wide/from16 v28, v38

    move-wide/from16 v30, v40

    .line 9
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v1

    if-eqz v1, :cond_5

    return v33

    :cond_5
    cmpl-double v1, p17, v40

    if-ltz v1, :cond_11

    cmpl-double v1, p29, v36

    if-ltz v1, :cond_11

    add-int/lit8 v6, p5, 0x1

    add-int/lit8 v7, p6, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v8, v42

    move-wide/from16 v10, v34

    move-wide/from16 v12, v36

    move-wide/from16 v14, p13

    move-wide/from16 v16, p15

    move-wide/from16 v18, p17

    move-wide/from16 v20, v44

    move-wide/from16 v22, v38

    move-wide/from16 v24, v40

    move-wide/from16 v26, p25

    move-wide/from16 v28, p27

    move-wide/from16 v30, p29

    .line 10
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v0

    if-eqz v0, :cond_11

    return v33

    .line 11
    :cond_6
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v14, p19

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 12
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v12, p25

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 13
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1, v0}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object/from16 v0, p1

    move-wide/from16 v12, p25

    move-wide v14, v2

    move-wide/from16 v42, v10

    cmpl-double v1, v36, p23

    if-ltz v1, :cond_8

    add-int/lit8 v6, p5, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move/from16 v7, p6

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-wide/from16 v14, v42

    move-wide/from16 v16, v34

    move-wide/from16 v18, v36

    move-wide/from16 v20, p19

    move-wide/from16 v22, p21

    move-wide/from16 v24, p23

    move-wide/from16 v26, p25

    move-wide/from16 v28, p27

    move-wide/from16 v30, p29

    .line 14
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v1

    if-eqz v1, :cond_8

    return v33

    :cond_8
    cmpl-double v1, p29, v36

    if-ltz v1, :cond_11

    add-int/lit8 v6, p5, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move/from16 v7, p6

    move-wide/from16 v8, v42

    move-wide/from16 v10, v34

    move-wide/from16 v12, v36

    move-wide/from16 v14, p13

    move-wide/from16 v16, p15

    move-wide/from16 v18, p17

    move-wide/from16 v20, p19

    move-wide/from16 v22, p21

    move-wide/from16 v24, p23

    move-wide/from16 v26, p25

    move-wide/from16 v28, p27

    move-wide/from16 v30, p29

    .line 15
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v0

    if-eqz v0, :cond_11

    return v33

    .line 16
    :cond_9
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "s0 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v12, p7

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 17
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "s1 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v10, p13

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 18
    new-instance v0, Ljava/lang/InternalError;

    const-string v1, "no s progress!"

    invoke-direct {v0, v1}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    move-object/from16 v14, p1

    move-wide/from16 v5, p25

    move-wide v10, v8

    move-wide v8, v2

    sub-double v2, v5, v8

    cmpl-double v7, v2, v17

    if-lez v7, :cond_e

    add-double v2, v8, v5

    div-double v2, v2, v19

    .line 19
    invoke-virtual {v14, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->XforT(D)D

    move-result-wide v34

    .line 20
    invoke-virtual {v14, v2, v3}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v36

    cmpl-double v7, v2, v8

    if-eqz v7, :cond_d

    cmpl-double v7, v2, v5

    if-eqz v7, :cond_d

    cmpl-double v0, v36, p11

    if-ltz v0, :cond_b

    add-int/lit8 v7, p6, 0x1

    move-object/from16 v1, p0

    move-wide/from16 v38, v2

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-object v0, v14

    move-wide/from16 v14, p13

    move-wide/from16 v16, p15

    move-wide/from16 v18, p17

    move-wide/from16 v20, p19

    move-wide/from16 v22, p21

    move-wide/from16 v24, p23

    move-wide/from16 v26, v38

    move-wide/from16 v28, v34

    move-wide/from16 v30, v36

    .line 21
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v1

    if-eqz v1, :cond_c

    return v33

    :cond_b
    move-wide/from16 v38, v2

    move-object v0, v14

    :cond_c
    cmpl-double v1, p17, v36

    if-ltz v1, :cond_11

    add-int/lit8 v7, p6, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-wide/from16 v14, p13

    move-wide/from16 v16, p15

    move-wide/from16 v18, p17

    move-wide/from16 v20, v38

    move-wide/from16 v22, v34

    move-wide/from16 v24, v36

    move-wide/from16 v26, p25

    move-wide/from16 v28, p27

    move-wide/from16 v30, p29

    .line 22
    :try_start_0
    invoke-virtual/range {v1 .. v31}, Lcom/intsig/office/java/awt/geom/Curve;->findIntersect(Lcom/intsig/office/java/awt/geom/Curve;[DDIIDDDDDDDDDDDD)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_11

    return v33

    .line 23
    :cond_d
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v4, p19

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 24
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v4, p25

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 25
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1, v0}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    move-wide v4, v8

    move-object v0, v14

    sub-double v6, p15, p9

    sub-double v8, p17, p11

    sub-double v10, p27, p21

    sub-double v12, p29, p23

    sub-double v17, p21, p9

    sub-double v21, p23, p11

    mul-double v23, v10, v8

    mul-double v25, v12, v6

    sub-double v23, v23, v25

    const-wide/16 v25, 0x0

    cmpl-double v1, v23, v25

    if-eqz v1, :cond_11

    const-wide/high16 v27, 0x3ff0000000000000L    # 1.0

    div-double v23, v27, v23

    mul-double v10, v10, v21

    mul-double v12, v12, v17

    sub-double/2addr v10, v12

    mul-double v10, v10, v23

    mul-double v6, v6, v21

    mul-double v8, v8, v17

    sub-double/2addr v6, v8

    mul-double v6, v6, v23

    cmpl-double v1, v10, v25

    if-ltz v1, :cond_11

    cmpg-double v1, v10, v27

    if-gtz v1, :cond_11

    cmpl-double v1, v6, v25

    if-ltz v1, :cond_11

    cmpg-double v1, v6, v27

    if-gtz v1, :cond_11

    mul-double v10, v10, v15

    add-double v8, p7, v10

    mul-double v6, v6, v2

    add-double v1, v4, v6

    cmpg-double v3, v8, v25

    if-ltz v3, :cond_f

    cmpl-double v3, v8, v27

    if-gtz v3, :cond_f

    cmpg-double v3, v1, v25

    if-ltz v3, :cond_f

    cmpl-double v3, v1, v27

    if-lez v3, :cond_10

    .line 26
    :cond_f
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Uh oh!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_10
    move-object/from16 v3, p0

    .line 27
    invoke-virtual {v3, v8, v9}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v4

    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    move-result-wide v0

    add-double/2addr v4, v0

    div-double v4, v4, v19

    .line 28
    aget-wide v0, p2, v33

    cmpg-double v2, v4, v0

    if-gtz v2, :cond_12

    aget-wide v0, p2, v32

    cmpl-double v2, v4, v0

    if-lez v2, :cond_12

    .line 29
    aput-wide v4, p2, v33

    return v33

    :cond_11
    move-object/from16 v3, p0

    :cond_12
    return v32

    :cond_13
    :goto_0
    move-object v3, v14

    return v32

    :cond_14
    :goto_1
    move-object v3, v14

    return v32

    :catchall_0
    move-exception v0

    move-object v1, v0

    .line 30
    throw v1
.end method

.method public final getDirection()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Curve;->direction:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public abstract getOrder()I
.end method

.method public abstract getReversedCurve()Lcom/intsig/office/java/awt/geom/Curve;
.end method

.method public abstract getSegment([D)I
.end method

.method public getSubCurve(DD)Lcom/intsig/office/java/awt/geom/Curve;
    .locals 6

    .line 1
    iget v5, p0, Lcom/intsig/office/java/awt/geom/Curve;->direction:I

    .line 2
    .line 3
    move-object v0, p0

    .line 4
    move-wide v1, p1

    .line 5
    move-wide v3, p3

    .line 6
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/java/awt/geom/Curve;->getSubCurve(DDI)Lcom/intsig/office/java/awt/geom/Curve;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public abstract getSubCurve(DDI)Lcom/intsig/office/java/awt/geom/Curve;
.end method

.method public final getWithDirection(I)Lcom/intsig/office/java/awt/geom/Curve;
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Curve;->direction:I

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    move-object p1, p0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getReversedCurve()Lcom/intsig/office/java/awt/geom/Curve;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    :goto_0
    return-object p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public abstract getX0()D
.end method

.method public abstract getX1()D
.end method

.method public abstract getXBot()D
.end method

.method public abstract getXMax()D
.end method

.method public abstract getXMin()D
.end method

.method public abstract getXTop()D
.end method

.method public abstract getY0()D
.end method

.method public abstract getY1()D
.end method

.method public abstract getYBot()D
.end method

.method public abstract getYTop()D
.end method

.method public abstract nextVertical(DD)D
.end method

.method public refineTforY(DDD)D
    .locals 5

    .line 1
    const-wide/high16 p3, 0x3ff0000000000000L    # 1.0

    .line 2
    .line 3
    :goto_0
    add-double v0, p1, p3

    .line 4
    .line 5
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 6
    .line 7
    div-double/2addr v0, v2

    .line 8
    cmpl-double v2, v0, p1

    .line 9
    .line 10
    if-eqz v2, :cond_2

    .line 11
    .line 12
    cmpl-double v2, v0, p3

    .line 13
    .line 14
    if-nez v2, :cond_0

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/java/awt/geom/Curve;->YforT(D)D

    .line 18
    .line 19
    .line 20
    move-result-wide v2

    .line 21
    cmpg-double v4, v2, p5

    .line 22
    .line 23
    if-gez v4, :cond_1

    .line 24
    .line 25
    move-wide p1, v0

    .line 26
    goto :goto_0

    .line 27
    :cond_1
    cmpl-double v4, v2, p5

    .line 28
    .line 29
    if-lez v4, :cond_2

    .line 30
    .line 31
    move-wide p3, v0

    .line 32
    goto :goto_0

    .line 33
    :cond_2
    :goto_1
    return-wide p3
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Curve["

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getOrder()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, ", "

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v2, "("

    .line 24
    .line 25
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getX0()D

    .line 29
    .line 30
    .line 31
    move-result-wide v3

    .line 32
    invoke-static {v3, v4}, Lcom/intsig/office/java/awt/geom/Curve;->round(D)D

    .line 33
    .line 34
    .line 35
    move-result-wide v3

    .line 36
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getY0()D

    .line 43
    .line 44
    .line 45
    move-result-wide v3

    .line 46
    invoke-static {v3, v4}, Lcom/intsig/office/java/awt/geom/Curve;->round(D)D

    .line 47
    .line 48
    .line 49
    move-result-wide v3

    .line 50
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v3, "), "

    .line 54
    .line 55
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->controlPointString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getX1()D

    .line 69
    .line 70
    .line 71
    move-result-wide v4

    .line 72
    invoke-static {v4, v5}, Lcom/intsig/office/java/awt/geom/Curve;->round(D)D

    .line 73
    .line 74
    .line 75
    move-result-wide v4

    .line 76
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Curve;->getY1()D

    .line 83
    .line 84
    .line 85
    move-result-wide v1

    .line 86
    invoke-static {v1, v2}, Lcom/intsig/office/java/awt/geom/Curve;->round(D)D

    .line 87
    .line 88
    .line 89
    move-result-wide v1

    .line 90
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Curve;->direction:I

    .line 97
    .line 98
    const/4 v2, 0x1

    .line 99
    if-ne v1, v2, :cond_0

    .line 100
    .line 101
    const-string v1, "D"

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_0
    const-string v1, "U"

    .line 105
    .line 106
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    const-string v1, "]"

    .line 110
    .line 111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    return-object v0
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

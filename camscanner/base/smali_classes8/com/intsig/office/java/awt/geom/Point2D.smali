.class public abstract Lcom/intsig/office/java/awt/geom/Point2D;
.super Ljava/lang/Object;
.source "Point2D.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/java/awt/geom/Point2D$Double;,
        Lcom/intsig/office/java/awt/geom/Point2D$Float;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public static distance(DDDD)D
    .locals 0

    sub-double/2addr p0, p4

    sub-double/2addr p2, p6

    mul-double p0, p0, p0

    mul-double p2, p2, p2

    add-double/2addr p0, p2

    .line 1
    invoke-static {p0, p1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p0

    return-wide p0
.end method

.method public static distanceSq(DDDD)D
    .locals 0

    .line 1
    sub-double/2addr p0, p4

    sub-double/2addr p2, p6

    mul-double p0, p0, p0

    mul-double p2, p2, p2

    add-double/2addr p0, p2

    return-wide p0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    return-object v0

    .line 6
    :catch_0
    new-instance v0, Ljava/lang/InternalError;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    .line 9
    .line 10
    .line 11
    throw v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public distance(DD)D
    .locals 2

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    sub-double/2addr p1, v0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v0

    sub-double/2addr p3, v0

    mul-double p1, p1, p1

    mul-double p3, p3, p3

    add-double/2addr p1, p3

    .line 4
    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p1

    return-wide p1
.end method

.method public distance(Lcom/intsig/office/java/awt/geom/Point2D;)D
    .locals 6

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    sub-double/2addr v2, v4

    mul-double v0, v0, v0

    mul-double v2, v2, v2

    add-double/2addr v0, v2

    .line 7
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public distanceSq(DD)D
    .locals 2

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    sub-double/2addr p1, v0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v0

    sub-double/2addr p3, v0

    mul-double p1, p1, p1

    mul-double p3, p3, p3

    add-double/2addr p1, p3

    return-wide p1
.end method

.method public distanceSq(Lcom/intsig/office/java/awt/geom/Point2D;)D
    .locals 6

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v4

    sub-double/2addr v2, v4

    mul-double v0, v0, v0

    mul-double v2, v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .line 1
    instance-of v0, p1, Lcom/intsig/office/java/awt/geom/Point2D;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/java/awt/geom/Point2D;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 12
    .line 13
    .line 14
    move-result-wide v2

    .line 15
    cmpl-double v4, v0, v2

    .line 16
    .line 17
    if-nez v4, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 20
    .line 21
    .line 22
    move-result-wide v0

    .line 23
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 24
    .line 25
    .line 26
    move-result-wide v2

    .line 27
    cmpl-double p1, v0, v2

    .line 28
    .line 29
    if-nez p1, :cond_0

    .line 30
    .line 31
    const/4 p1, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 p1, 0x0

    .line 34
    :goto_0
    return p1

    .line 35
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    return p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public abstract getX()D
.end method

.method public abstract getY()D
.end method

.method public hashCode()I
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 10
    .line 11
    .line 12
    move-result-wide v2

    .line 13
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 14
    .line 15
    .line 16
    move-result-wide v2

    .line 17
    const-wide/16 v4, 0x1f

    .line 18
    .line 19
    mul-long v2, v2, v4

    .line 20
    .line 21
    xor-long/2addr v0, v2

    .line 22
    long-to-int v2, v0

    .line 23
    const/16 v3, 0x20

    .line 24
    .line 25
    shr-long/2addr v0, v3

    .line 26
    long-to-int v1, v0

    .line 27
    xor-int v0, v2, v1

    .line 28
    .line 29
    return v0
    .line 30
.end method

.method public abstract setLocation(DD)V
.end method

.method public setLocation(Lcom/intsig/office/java/awt/geom/Point2D;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

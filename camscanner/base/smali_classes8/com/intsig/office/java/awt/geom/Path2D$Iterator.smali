.class abstract Lcom/intsig/office/java/awt/geom/Path2D$Iterator;
.super Ljava/lang/Object;
.source "Path2D.java"

# interfaces
.implements Lcom/intsig/office/java/awt/geom/PathIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/Path2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "Iterator"
.end annotation


# static fields
.field static final O8:[I


# instance fields
.field 〇080:I

.field 〇o00〇〇Oo:I

.field 〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->O8:[I

    .line 8
    .line 9
    return-void

    .line 10
    nop

    .line 11
    :array_0
    .array-data 4
        0x2
        0x2
        0x4
        0x6
        0x0
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method constructor <init>(Lcom/intsig/office/java/awt/geom/Path2D;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public getWindingRule()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Path2D;->getWindingRule()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDone()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇080:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;

    .line 4
    .line 5
    iget v1, v1, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 6
    .line 7
    if-lt v0, v1, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public next()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇080:I

    .line 6
    .line 7
    add-int/lit8 v2, v1, 0x1

    .line 8
    .line 9
    iput v2, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇080:I

    .line 10
    .line 11
    aget-byte v0, v0, v1

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o00〇〇Oo:I

    .line 14
    .line 15
    sget-object v2, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->O8:[I

    .line 16
    .line 17
    aget v0, v2, v0

    .line 18
    .line 19
    add-int/2addr v1, v0

    .line 20
    iput v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o00〇〇Oo:I

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class Lcom/intsig/office/java/awt/geom/Path2D$Double$CopyIterator;
.super Lcom/intsig/office/java/awt/geom/Path2D$Iterator;
.source "Path2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/Path2D$Double;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CopyIterator"
.end annotation


# instance fields
.field Oo08:[D


# direct methods
.method constructor <init>(Lcom/intsig/office/java/awt/geom/Path2D$Double;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;-><init>(Lcom/intsig/office/java/awt/geom/Path2D;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p1, Lcom/intsig/office/java/awt/geom/Path2D$Double;->doubleCoords:[D

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Double$CopyIterator;->Oo08:[D

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 5

    .line 4
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;

    iget-object v0, v0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇080:I

    aget-byte v0, v0, v1

    .line 5
    sget-object v1, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->O8:[I

    aget v1, v1, v0

    if-lez v1, :cond_0

    .line 6
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/Path2D$Double$CopyIterator;->Oo08:[D

    iget v3, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o00〇〇Oo:I

    const/4 v4, 0x0

    invoke-static {v2, v3, p1, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    return v0
.end method

.method public currentSegment([F)I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;

    iget-object v0, v0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇080:I

    aget-byte v0, v0, v1

    .line 2
    sget-object v1, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->O8:[I

    aget v1, v1, v0

    if-lez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 3
    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/Path2D$Double$CopyIterator;->Oo08:[D

    iget v4, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o00〇〇Oo:I

    add-int/2addr v4, v2

    aget-wide v4, v3, v4

    double-to-float v3, v4

    aput v3, p1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return v0
.end method

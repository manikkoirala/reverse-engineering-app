.class Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;
.super Lcom/intsig/office/java/awt/geom/Path2D$Iterator;
.source "Path2D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/Path2D$Float;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TxIterator"
.end annotation


# instance fields
.field Oo08:[F

.field o〇0:Lcom/intsig/office/java/awt/geom/AffineTransform;


# direct methods
.method constructor <init>(Lcom/intsig/office/java/awt/geom/Path2D$Float;Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;-><init>(Lcom/intsig/office/java/awt/geom/Path2D;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p1, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;->Oo08:[F

    .line 7
    .line 8
    iput-object p2, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;->o〇0:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 8

    .line 4
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;

    iget-object v0, v0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇080:I

    aget-byte v0, v0, v1

    .line 5
    sget-object v1, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->O8:[I

    aget v1, v1, v0

    if-lez v1, :cond_0

    .line 6
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;->o〇0:Lcom/intsig/office/java/awt/geom/AffineTransform;

    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;->Oo08:[F

    iget v4, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o00〇〇Oo:I

    const/4 v6, 0x0

    div-int/lit8 v7, v1, 0x2

    move-object v5, p1

    invoke-virtual/range {v2 .. v7}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[DII)V

    :cond_0
    return v0
.end method

.method public currentSegment([F)I
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o〇:Lcom/intsig/office/java/awt/geom/Path2D;

    iget-object v0, v0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇080:I

    aget-byte v0, v0, v1

    .line 2
    sget-object v1, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->O8:[I

    aget v1, v1, v0

    if-lez v1, :cond_0

    .line 3
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;->o〇0:Lcom/intsig/office/java/awt/geom/AffineTransform;

    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;->Oo08:[F

    iget v4, p0, Lcom/intsig/office/java/awt/geom/Path2D$Iterator;->〇o00〇〇Oo:I

    const/4 v6, 0x0

    div-int/lit8 v7, v1, 0x2

    move-object v5, p1

    invoke-virtual/range {v2 .. v7}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    :cond_0
    return v0
.end method

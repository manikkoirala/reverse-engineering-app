.class public Lcom/intsig/office/java/awt/geom/Area;
.super Ljava/lang/Object;
.source "Area.java"

# interfaces
.implements Lcom/intsig/office/java/awt/Shape;
.implements Ljava/lang/Cloneable;


# static fields
.field private static EmptyCurves:Ljava/util/Vector;


# instance fields
.field private cachedBounds:Lcom/intsig/office/java/awt/geom/Rectangle2D;

.field private curves:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/Vector;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/office/java/awt/geom/Area;->EmptyCurves:Ljava/util/Vector;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/intsig/office/java/awt/geom/Area;->EmptyCurves:Ljava/util/Vector;

    iput-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Shape;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    instance-of v0, p1, Lcom/intsig/office/java/awt/geom/Area;

    if-eqz v0, :cond_0

    .line 5
    check-cast p1, Lcom/intsig/office/java/awt/geom/Area;

    iget-object p1, p1, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 6
    invoke-interface {p1, v0}, Lcom/intsig/office/java/awt/Shape;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    move-result-object p1

    invoke-static {p1}, Lcom/intsig/office/java/awt/geom/Area;->pathToCurves(Lcom/intsig/office/java/awt/geom/PathIterator;)Ljava/util/Vector;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    :goto_0
    return-void
.end method

.method private getCachedBounds()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->cachedBounds:Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 12
    .line 13
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-lez v1, :cond_1

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/office/java/awt/geom/Curve;

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Curve;->getX0()D

    .line 29
    .line 30
    .line 31
    move-result-wide v2

    .line 32
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Curve;->getY0()D

    .line 33
    .line 34
    .line 35
    move-result-wide v4

    .line 36
    const-wide/16 v6, 0x0

    .line 37
    .line 38
    const-wide/16 v8, 0x0

    .line 39
    .line 40
    move-object v1, v0

    .line 41
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->setRect(DDDD)V

    .line 42
    .line 43
    .line 44
    const/4 v1, 0x1

    .line 45
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    if-ge v1, v2, :cond_1

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 54
    .line 55
    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    check-cast v2, Lcom/intsig/office/java/awt/geom/Curve;

    .line 60
    .line 61
    invoke-virtual {v2, v0}, Lcom/intsig/office/java/awt/geom/Curve;->enlarge(Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 62
    .line 63
    .line 64
    add-int/lit8 v1, v1, 0x1

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    iput-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->cachedBounds:Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 68
    .line 69
    return-object v0
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private invalidateBounds()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->cachedBounds:Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private static pathToCurves(Lcom/intsig/office/java/awt/geom/PathIterator;)Ljava/util/Vector;
    .locals 20

    .line 1
    new-instance v9, Ljava/util/Vector;

    .line 2
    .line 3
    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->getWindingRule()I

    .line 7
    .line 8
    .line 9
    move-result v10

    .line 10
    const/16 v0, 0x17

    .line 11
    .line 12
    new-array v11, v0, [D

    .line 13
    .line 14
    const-wide/16 v0, 0x0

    .line 15
    .line 16
    move-wide v3, v0

    .line 17
    move-wide v12, v3

    .line 18
    move-wide v14, v12

    .line 19
    move-wide v1, v14

    .line 20
    :goto_0
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->isDone()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_5

    .line 25
    .line 26
    move-object/from16 v7, p0

    .line 27
    .line 28
    invoke-interface {v7, v11}, Lcom/intsig/office/java/awt/geom/PathIterator;->currentSegment([D)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    const/16 v16, 0x0

    .line 33
    .line 34
    const/4 v8, 0x1

    .line 35
    if-eqz v0, :cond_4

    .line 36
    .line 37
    if-eq v0, v8, :cond_3

    .line 38
    .line 39
    const/4 v5, 0x3

    .line 40
    const/4 v6, 0x2

    .line 41
    if-eq v0, v6, :cond_2

    .line 42
    .line 43
    const/4 v6, 0x4

    .line 44
    if-eq v0, v5, :cond_1

    .line 45
    .line 46
    if-eq v0, v6, :cond_0

    .line 47
    .line 48
    goto :goto_2

    .line 49
    :cond_0
    move-object v0, v9

    .line 50
    move-wide v5, v12

    .line 51
    move-wide v7, v14

    .line 52
    invoke-static/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Curve;->insertLine(Ljava/util/Vector;DDDD)V

    .line 53
    .line 54
    .line 55
    move-wide v1, v12

    .line 56
    move-wide v3, v14

    .line 57
    goto :goto_2

    .line 58
    :cond_1
    aget-wide v6, v11, v6

    .line 59
    .line 60
    const/4 v0, 0x5

    .line 61
    aget-wide v16, v11, v0

    .line 62
    .line 63
    move-object v0, v9

    .line 64
    move-object v5, v11

    .line 65
    invoke-static/range {v0 .. v5}, Lcom/intsig/office/java/awt/geom/Curve;->insertCubic(Ljava/util/Vector;DD[D)V

    .line 66
    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_2
    aget-wide v6, v11, v6

    .line 70
    .line 71
    aget-wide v16, v11, v5

    .line 72
    .line 73
    move-object v0, v9

    .line 74
    move-object v5, v11

    .line 75
    invoke-static/range {v0 .. v5}, Lcom/intsig/office/java/awt/geom/Curve;->insertQuad(Ljava/util/Vector;DD[D)V

    .line 76
    .line 77
    .line 78
    :goto_1
    move-wide v1, v6

    .line 79
    move-wide/from16 v3, v16

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_3
    aget-wide v16, v11, v16

    .line 83
    .line 84
    aget-wide v18, v11, v8

    .line 85
    .line 86
    move-object v0, v9

    .line 87
    move-wide/from16 v5, v16

    .line 88
    .line 89
    move-wide/from16 v7, v18

    .line 90
    .line 91
    invoke-static/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Curve;->insertLine(Ljava/util/Vector;DDDD)V

    .line 92
    .line 93
    .line 94
    move-wide/from16 v1, v16

    .line 95
    .line 96
    move-wide/from16 v3, v18

    .line 97
    .line 98
    goto :goto_2

    .line 99
    :cond_4
    move-object v0, v9

    .line 100
    move-wide v5, v12

    .line 101
    const/4 v12, 0x1

    .line 102
    move-wide v7, v14

    .line 103
    invoke-static/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Curve;->insertLine(Ljava/util/Vector;DDDD)V

    .line 104
    .line 105
    .line 106
    aget-wide v1, v11, v16

    .line 107
    .line 108
    aget-wide v3, v11, v12

    .line 109
    .line 110
    invoke-static {v9, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/geom/Curve;->insertMove(Ljava/util/Vector;DD)V

    .line 111
    .line 112
    .line 113
    move-wide v12, v1

    .line 114
    move-wide v14, v3

    .line 115
    :goto_2
    invoke-interface/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/PathIterator;->next()V

    .line 116
    .line 117
    .line 118
    goto :goto_0

    .line 119
    :cond_5
    move-object v0, v9

    .line 120
    move-wide v5, v12

    .line 121
    move-wide v7, v14

    .line 122
    invoke-static/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Curve;->insertLine(Ljava/util/Vector;DDDD)V

    .line 123
    .line 124
    .line 125
    if-nez v10, :cond_6

    .line 126
    .line 127
    new-instance v0, Lcom/intsig/office/java/awt/geom/AreaOp$EOWindOp;

    .line 128
    .line 129
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AreaOp$EOWindOp;-><init>()V

    .line 130
    .line 131
    .line 132
    goto :goto_3

    .line 133
    :cond_6
    new-instance v0, Lcom/intsig/office/java/awt/geom/AreaOp$NZWindOp;

    .line 134
    .line 135
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AreaOp$NZWindOp;-><init>()V

    .line 136
    .line 137
    .line 138
    :goto_3
    sget-object v1, Lcom/intsig/office/java/awt/geom/Area;->EmptyCurves:Ljava/util/Vector;

    .line 139
    .line 140
    invoke-virtual {v0, v9, v1}, Lcom/intsig/office/java/awt/geom/AreaOp;->calculate(Ljava/util/Vector;Ljava/util/Vector;)Ljava/util/Vector;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    return-object v0
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public add(Lcom/intsig/office/java/awt/geom/Area;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AreaOp$AddOp;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AreaOp$AddOp;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 7
    .line 8
    iget-object p1, p1, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/java/awt/geom/AreaOp;->calculate(Ljava/util/Vector;Ljava/util/Vector;)Ljava/util/Vector;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->invalidateBounds()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Area;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/java/awt/geom/Area;-><init>(Lcom/intsig/office/java/awt/Shape;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public contains(DD)Z
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->getCachedBounds()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->contains(DD)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    const/4 v2, 0x0

    .line 3
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/office/java/awt/geom/Curve;

    .line 5
    invoke-virtual {v3, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/Curve;->crossingsFor(DD)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    and-int/lit8 p2, v2, 0x1

    if-ne p2, p1, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public contains(DDDD)Z
    .locals 15

    move-wide/from16 v9, p3

    const/4 v11, 0x0

    const-wide/16 v0, 0x0

    cmpg-double v2, p5, v0

    if-ltz v2, :cond_3

    cmpg-double v2, p7, v0

    if-gez v2, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->getCachedBounds()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    move-result-object v0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->contains(DDDD)Z

    move-result v0

    if-nez v0, :cond_1

    return v11

    :cond_1
    move-object v12, p0

    .line 8
    iget-object v0, v12, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    add-double v5, p1, p5

    add-double v13, v9, p7

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide v7, v13

    invoke-static/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Crossings;->findCrossings(Ljava/util/Vector;DDDD)Lcom/intsig/office/java/awt/geom/Crossings;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 9
    invoke-virtual {v0, v9, v10, v13, v14}, Lcom/intsig/office/java/awt/geom/Crossings;->covers(DD)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v11, 0x1

    :cond_2
    return v11

    :cond_3
    :goto_0
    move-object v12, p0

    return v11
.end method

.method public contains(Lcom/intsig/office/java/awt/geom/Point2D;)Z
    .locals 4

    .line 6
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Area;->contains(DD)Z

    move-result p1

    return p1
.end method

.method public contains(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 9

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Area;->contains(DDDD)Z

    move-result p1

    return p1
.end method

.method public createTransformedArea(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/Area;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Area;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/java/awt/geom/Area;-><init>(Lcom/intsig/office/java/awt/Shape;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/office/java/awt/geom/Area;->transform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public equals(Lcom/intsig/office/java/awt/geom/Area;)Z
    .locals 2

    .line 1
    if-ne p1, p0, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    return p1

    .line 5
    :cond_0
    if-nez p1, :cond_1

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return p1

    .line 9
    :cond_1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AreaOp$XorOp;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AreaOp$XorOp;-><init>()V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 15
    .line 16
    iget-object p1, p1, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 17
    .line 18
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/java/awt/geom/AreaOp;->calculate(Ljava/util/Vector;Ljava/util/Vector;)Ljava/util/Vector;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1}, Ljava/util/Vector;->isEmpty()Z

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    return p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public exclusiveOr(Lcom/intsig/office/java/awt/geom/Area;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AreaOp$XorOp;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AreaOp$XorOp;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 7
    .line 8
    iget-object p1, p1, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/java/awt/geom/AreaOp;->calculate(Ljava/util/Vector;Ljava/util/Vector;)Ljava/util/Vector;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->invalidateBounds()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getBounds()Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->getCachedBounds()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->getCachedBounds()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AreaIterator;

    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    invoke-direct {v0, v1, p1}, Lcom/intsig/office/java/awt/geom/AreaIterator;-><init>(Ljava/util/Vector;Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    return-object v0
.end method

.method public getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;D)Lcom/intsig/office/java/awt/geom/PathIterator;
    .locals 1

    .line 2
    new-instance v0, Lcom/intsig/office/java/awt/geom/FlatteningPathIterator;

    invoke-virtual {p0, p1}, Lcom/intsig/office/java/awt/geom/Area;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    move-result-object p1

    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/office/java/awt/geom/FlatteningPathIterator;-><init>(Lcom/intsig/office/java/awt/geom/PathIterator;D)V

    return-object v0
.end method

.method public intersect(Lcom/intsig/office/java/awt/geom/Area;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AreaOp$IntOp;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AreaOp$IntOp;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 7
    .line 8
    iget-object p1, p1, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/java/awt/geom/AreaOp;->calculate(Ljava/util/Vector;Ljava/util/Vector;)Ljava/util/Vector;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->invalidateBounds()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public intersects(DDDD)Z
    .locals 19

    const/4 v9, 0x0

    const-wide/16 v0, 0x0

    cmpg-double v2, p5, v0

    if-ltz v2, :cond_4

    cmpg-double v2, p7, v0

    if-gez v2, :cond_0

    goto :goto_0

    .line 1
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Area;->getCachedBounds()Lcom/intsig/office/java/awt/geom/Rectangle2D;

    move-result-object v0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->intersects(DDDD)Z

    move-result v0

    if-nez v0, :cond_1

    return v9

    :cond_1
    move-object/from16 v0, p0

    .line 2
    iget-object v10, v0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    add-double v15, p1, p5

    add-double v17, p3, p7

    move-wide/from16 v11, p1

    move-wide/from16 v13, p3

    invoke-static/range {v10 .. v18}, Lcom/intsig/office/java/awt/geom/Crossings;->findCrossings(Ljava/util/Vector;DDDD)Lcom/intsig/office/java/awt/geom/Crossings;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 3
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Crossings;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v9, 0x1

    :cond_3
    return v9

    :cond_4
    :goto_0
    move-object/from16 v0, p0

    return v9
.end method

.method public intersects(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 9

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Area;->intersects(DDDD)Z

    move-result p1

    return p1
.end method

.method public isEmpty()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isPolygonal()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x1

    .line 12
    if-eqz v1, :cond_1

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    check-cast v1, Lcom/intsig/office/java/awt/geom/Curve;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Curve;->getOrder()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-le v1, v2, :cond_0

    .line 25
    .line 26
    const/4 v0, 0x0

    .line 27
    return v0

    .line 28
    :cond_1
    return v2
    .line 29
    .line 30
.end method

.method public isRectangular()Z
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    const/4 v2, 0x3

    .line 12
    const/4 v3, 0x0

    .line 13
    if-le v0, v2, :cond_1

    .line 14
    .line 15
    return v3

    .line 16
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/office/java/awt/geom/Curve;

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 25
    .line 26
    const/4 v4, 0x2

    .line 27
    invoke-virtual {v2, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Lcom/intsig/office/java/awt/geom/Curve;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getOrder()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    if-ne v4, v1, :cond_5

    .line 38
    .line 39
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/Curve;->getOrder()I

    .line 40
    .line 41
    .line 42
    move-result v4

    .line 43
    if-eq v4, v1, :cond_2

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getXTop()D

    .line 47
    .line 48
    .line 49
    move-result-wide v4

    .line 50
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getXBot()D

    .line 51
    .line 52
    .line 53
    move-result-wide v6

    .line 54
    cmpl-double v8, v4, v6

    .line 55
    .line 56
    if-nez v8, :cond_5

    .line 57
    .line 58
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/Curve;->getXTop()D

    .line 59
    .line 60
    .line 61
    move-result-wide v4

    .line 62
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/Curve;->getXBot()D

    .line 63
    .line 64
    .line 65
    move-result-wide v6

    .line 66
    cmpl-double v8, v4, v6

    .line 67
    .line 68
    if-eqz v8, :cond_3

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getYTop()D

    .line 72
    .line 73
    .line 74
    move-result-wide v4

    .line 75
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/Curve;->getYTop()D

    .line 76
    .line 77
    .line 78
    move-result-wide v6

    .line 79
    cmpl-double v8, v4, v6

    .line 80
    .line 81
    if-nez v8, :cond_5

    .line 82
    .line 83
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getYBot()D

    .line 84
    .line 85
    .line 86
    move-result-wide v4

    .line 87
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/Curve;->getYBot()D

    .line 88
    .line 89
    .line 90
    move-result-wide v6

    .line 91
    cmpl-double v0, v4, v6

    .line 92
    .line 93
    if-eqz v0, :cond_4

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_4
    return v1

    .line 97
    :cond_5
    :goto_0
    return v3
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public isSingular()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x3

    .line 8
    const/4 v2, 0x1

    .line 9
    if-ge v0, v1, :cond_0

    .line 10
    .line 11
    return v2

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_2

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    check-cast v1, Lcom/intsig/office/java/awt/geom/Curve;

    .line 32
    .line 33
    invoke-virtual {v1}, Lcom/intsig/office/java/awt/geom/Curve;->getOrder()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-nez v1, :cond_1

    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    return v0

    .line 41
    :cond_2
    return v2
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public reset()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/Vector;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->invalidateBounds()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public subtract(Lcom/intsig/office/java/awt/geom/Area;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AreaOp$SubOp;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AreaOp$SubOp;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 7
    .line 8
    iget-object p1, p1, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Lcom/intsig/office/java/awt/geom/AreaOp;->calculate(Ljava/util/Vector;Ljava/util/Vector;)Ljava/util/Vector;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->invalidateBounds()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public transform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/java/awt/geom/Area;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-static {p1}, Lcom/intsig/office/java/awt/geom/Area;->pathToCurves(Lcom/intsig/office/java/awt/geom/PathIterator;)Ljava/util/Vector;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Area;->curves:Ljava/util/Vector;

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Area;->invalidateBounds()V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    .line 18
    .line 19
    const-string v0, "transform must not be null"

    .line 20
    .line 21
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    throw p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

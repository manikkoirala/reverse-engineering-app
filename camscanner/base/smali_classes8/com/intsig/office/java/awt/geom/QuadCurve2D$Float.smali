.class public Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;
.super Lcom/intsig/office/java/awt/geom/QuadCurve2D;
.source "QuadCurve2D.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/QuadCurve2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x761dd0c1ed6d1379L


# instance fields
.field public ctrlx:F

.field public ctrly:F

.field public x1:F

.field public x2:F

.field public y1:F

.field public y2:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;-><init>()V

    return-void
.end method

.method public constructor <init>(FFFFFF)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;-><init>()V

    .line 3
    invoke-virtual/range {p0 .. p6}, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->setCurve(FFFFFF)V

    return-void
.end method


# virtual methods
.method public getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x1:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x2:F

    .line 4
    .line 5
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget v1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    .line 10
    .line 11
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iget v1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y1:F

    .line 16
    .line 17
    iget v2, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y2:F

    .line 18
    .line 19
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    iget v2, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    .line 24
    .line 25
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    iget v2, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x1:F

    .line 30
    .line 31
    iget v3, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x2:F

    .line 32
    .line 33
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    iget v3, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    .line 38
    .line 39
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    iget v3, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y1:F

    .line 44
    .line 45
    iget v4, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y2:F

    .line 46
    .line 47
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    iget v4, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    .line 52
    .line 53
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    new-instance v4, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 58
    .line 59
    sub-float/2addr v2, v0

    .line 60
    sub-float/2addr v3, v1

    .line 61
    invoke-direct {v4, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 62
    .line 63
    .line 64
    return-object v4
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getCtrlPt()Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>(FF)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCtrlX()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCtrlY()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getP1()Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x1:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y1:F

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>(FF)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getP2()Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x2:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y2:F

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>(FF)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getX1()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x1:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getX2()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x2:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getY1()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y1:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getY2()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y2:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setCurve(DDDDDD)V
    .locals 0

    double-to-float p1, p1

    .line 1
    iput p1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x1:F

    double-to-float p1, p3

    .line 2
    iput p1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y1:F

    double-to-float p1, p5

    .line 3
    iput p1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    double-to-float p1, p7

    .line 4
    iput p1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    double-to-float p1, p9

    .line 5
    iput p1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x2:F

    double-to-float p1, p11

    .line 6
    iput p1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y2:F

    return-void
.end method

.method public setCurve(FFFFFF)V
    .locals 0

    .line 7
    iput p1, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x1:F

    .line 8
    iput p2, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y1:F

    .line 9
    iput p3, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrlx:F

    .line 10
    iput p4, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->ctrly:F

    .line 11
    iput p5, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->x2:F

    .line 12
    iput p6, p0, Lcom/intsig/office/java/awt/geom/QuadCurve2D$Float;->y2:F

    return-void
.end method

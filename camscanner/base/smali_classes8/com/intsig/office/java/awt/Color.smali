.class public Lcom/intsig/office/java/awt/Color;
.super Ljava/lang/Object;
.source "Color.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final BLACK:Lcom/intsig/office/java/awt/Color;

.field public static final BLUE:Lcom/intsig/office/java/awt/Color;

.field public static final CYAN:Lcom/intsig/office/java/awt/Color;

.field public static final DARK_GRAY:Lcom/intsig/office/java/awt/Color;

.field private static final FACTOR:D = 0.7

.field public static final GRAY:Lcom/intsig/office/java/awt/Color;

.field public static final GREEN:Lcom/intsig/office/java/awt/Color;

.field public static final LIGHT_GRAY:Lcom/intsig/office/java/awt/Color;

.field public static final MAGENTA:Lcom/intsig/office/java/awt/Color;

.field public static final ORANGE:Lcom/intsig/office/java/awt/Color;

.field public static final PINK:Lcom/intsig/office/java/awt/Color;

.field public static final RED:Lcom/intsig/office/java/awt/Color;

.field public static final WHITE:Lcom/intsig/office/java/awt/Color;

.field public static final YELLOW:Lcom/intsig/office/java/awt/Color;

.field public static final black:Lcom/intsig/office/java/awt/Color;

.field public static final blue:Lcom/intsig/office/java/awt/Color;

.field public static final cyan:Lcom/intsig/office/java/awt/Color;

.field public static final darkGray:Lcom/intsig/office/java/awt/Color;

.field public static final gray:Lcom/intsig/office/java/awt/Color;

.field public static final green:Lcom/intsig/office/java/awt/Color;

.field public static final lightGray:Lcom/intsig/office/java/awt/Color;

.field public static final magenta:Lcom/intsig/office/java/awt/Color;

.field public static final orange:Lcom/intsig/office/java/awt/Color;

.field public static final pink:Lcom/intsig/office/java/awt/Color;

.field public static final red:Lcom/intsig/office/java/awt/Color;

.field private static final serialVersionUID:J = 0x1a51783108f3375L

.field public static final white:Lcom/intsig/office/java/awt/Color;

.field public static final yellow:Lcom/intsig/office/java/awt/Color;


# instance fields
.field private falpha:F

.field private frgbvalue:[F

.field private fvalue:[F

.field private transient pData:J

.field value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    const/16 v1, 0xff

    .line 4
    .line 5
    invoke-direct {v0, v1, v1, v1}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/java/awt/Color;->white:Lcom/intsig/office/java/awt/Color;

    .line 9
    .line 10
    sput-object v0, Lcom/intsig/office/java/awt/Color;->WHITE:Lcom/intsig/office/java/awt/Color;

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 13
    .line 14
    const/16 v2, 0xc0

    .line 15
    .line 16
    invoke-direct {v0, v2, v2, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/intsig/office/java/awt/Color;->lightGray:Lcom/intsig/office/java/awt/Color;

    .line 20
    .line 21
    sput-object v0, Lcom/intsig/office/java/awt/Color;->LIGHT_GRAY:Lcom/intsig/office/java/awt/Color;

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 24
    .line 25
    const/16 v2, 0x80

    .line 26
    .line 27
    invoke-direct {v0, v2, v2, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/office/java/awt/Color;->gray:Lcom/intsig/office/java/awt/Color;

    .line 31
    .line 32
    sput-object v0, Lcom/intsig/office/java/awt/Color;->GRAY:Lcom/intsig/office/java/awt/Color;

    .line 33
    .line 34
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 35
    .line 36
    const/16 v2, 0x40

    .line 37
    .line 38
    invoke-direct {v0, v2, v2, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/office/java/awt/Color;->darkGray:Lcom/intsig/office/java/awt/Color;

    .line 42
    .line 43
    sput-object v0, Lcom/intsig/office/java/awt/Color;->DARK_GRAY:Lcom/intsig/office/java/awt/Color;

    .line 44
    .line 45
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 46
    .line 47
    const/4 v2, 0x0

    .line 48
    invoke-direct {v0, v2, v2, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 49
    .line 50
    .line 51
    sput-object v0, Lcom/intsig/office/java/awt/Color;->black:Lcom/intsig/office/java/awt/Color;

    .line 52
    .line 53
    sput-object v0, Lcom/intsig/office/java/awt/Color;->BLACK:Lcom/intsig/office/java/awt/Color;

    .line 54
    .line 55
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 56
    .line 57
    invoke-direct {v0, v1, v2, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 58
    .line 59
    .line 60
    sput-object v0, Lcom/intsig/office/java/awt/Color;->red:Lcom/intsig/office/java/awt/Color;

    .line 61
    .line 62
    sput-object v0, Lcom/intsig/office/java/awt/Color;->RED:Lcom/intsig/office/java/awt/Color;

    .line 63
    .line 64
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 65
    .line 66
    const/16 v3, 0xaf

    .line 67
    .line 68
    invoke-direct {v0, v1, v3, v3}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 69
    .line 70
    .line 71
    sput-object v0, Lcom/intsig/office/java/awt/Color;->pink:Lcom/intsig/office/java/awt/Color;

    .line 72
    .line 73
    sput-object v0, Lcom/intsig/office/java/awt/Color;->PINK:Lcom/intsig/office/java/awt/Color;

    .line 74
    .line 75
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 76
    .line 77
    const/16 v3, 0xc8

    .line 78
    .line 79
    invoke-direct {v0, v1, v3, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 80
    .line 81
    .line 82
    sput-object v0, Lcom/intsig/office/java/awt/Color;->orange:Lcom/intsig/office/java/awt/Color;

    .line 83
    .line 84
    sput-object v0, Lcom/intsig/office/java/awt/Color;->ORANGE:Lcom/intsig/office/java/awt/Color;

    .line 85
    .line 86
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 87
    .line 88
    invoke-direct {v0, v1, v1, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 89
    .line 90
    .line 91
    sput-object v0, Lcom/intsig/office/java/awt/Color;->yellow:Lcom/intsig/office/java/awt/Color;

    .line 92
    .line 93
    sput-object v0, Lcom/intsig/office/java/awt/Color;->YELLOW:Lcom/intsig/office/java/awt/Color;

    .line 94
    .line 95
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 96
    .line 97
    invoke-direct {v0, v2, v1, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 98
    .line 99
    .line 100
    sput-object v0, Lcom/intsig/office/java/awt/Color;->green:Lcom/intsig/office/java/awt/Color;

    .line 101
    .line 102
    sput-object v0, Lcom/intsig/office/java/awt/Color;->GREEN:Lcom/intsig/office/java/awt/Color;

    .line 103
    .line 104
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 105
    .line 106
    invoke-direct {v0, v1, v2, v1}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 107
    .line 108
    .line 109
    sput-object v0, Lcom/intsig/office/java/awt/Color;->magenta:Lcom/intsig/office/java/awt/Color;

    .line 110
    .line 111
    sput-object v0, Lcom/intsig/office/java/awt/Color;->MAGENTA:Lcom/intsig/office/java/awt/Color;

    .line 112
    .line 113
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 114
    .line 115
    invoke-direct {v0, v2, v1, v1}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 116
    .line 117
    .line 118
    sput-object v0, Lcom/intsig/office/java/awt/Color;->cyan:Lcom/intsig/office/java/awt/Color;

    .line 119
    .line 120
    sput-object v0, Lcom/intsig/office/java/awt/Color;->CYAN:Lcom/intsig/office/java/awt/Color;

    .line 121
    .line 122
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 123
    .line 124
    invoke-direct {v0, v2, v2, v1}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 125
    .line 126
    .line 127
    sput-object v0, Lcom/intsig/office/java/awt/Color;->blue:Lcom/intsig/office/java/awt/Color;

    .line 128
    .line 129
    sput-object v0, Lcom/intsig/office/java/awt/Color;->BLUE:Lcom/intsig/office/java/awt/Color;

    .line 130
    .line 131
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public constructor <init>(FFF)V
    .locals 7

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float v1, p1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    add-double/2addr v1, v3

    double-to-int v1, v1

    mul-float v2, p2, v0

    float-to-double v5, v2

    add-double/2addr v5, v3

    double-to-int v2, v5

    mul-float v0, v0, p3

    float-to-double v5, v0

    add-double/2addr v5, v3

    double-to-int v0, v5

    .line 24
    invoke-direct {p0, v1, v2, v0}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 25
    invoke-static {p1, p2, p3, v0}, Lcom/intsig/office/java/awt/Color;->testColorValueRange(FFFF)V

    const/4 v1, 0x3

    new-array v1, v1, [F

    .line 26
    iput-object v1, p0, Lcom/intsig/office/java/awt/Color;->frgbvalue:[F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 p1, 0x1

    aput p2, v1, p1

    const/4 p1, 0x2

    aput p3, v1, p1

    .line 27
    iput v0, p0, Lcom/intsig/office/java/awt/Color;->falpha:F

    .line 28
    iput-object v1, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 8

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float v1, p1, v0

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    add-double/2addr v1, v3

    double-to-int v1, v1

    mul-float v2, p2, v0

    float-to-double v5, v2

    add-double/2addr v5, v3

    double-to-int v2, v5

    mul-float v5, p3, v0

    float-to-double v5, v5

    add-double/2addr v5, v3

    double-to-int v5, v5

    mul-float v0, v0, p4

    float-to-double v6, v0

    add-double/2addr v6, v3

    double-to-int v0, v6

    .line 29
    invoke-direct {p0, v1, v2, v5, v0}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 30
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->frgbvalue:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 p1, 0x1

    aput p2, v0, p1

    const/4 p1, 0x2

    aput p3, v0, p1

    .line 31
    iput p4, p0, Lcom/intsig/office/java/awt/Color;->falpha:F

    .line 32
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->frgbvalue:[F

    .line 10
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    const/4 v0, 0x0

    .line 11
    iput v0, p0, Lcom/intsig/office/java/awt/Color;->falpha:F

    const/high16 v0, -0x1000000

    or-int/2addr p1, v0

    .line 12
    iput p1, p0, Lcom/intsig/office/java/awt/Color;->value:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 20
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->frgbvalue:[F

    .line 21
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    const/4 v0, 0x0

    .line 22
    iput v0, p0, Lcom/intsig/office/java/awt/Color;->falpha:F

    and-int/lit16 p2, p2, 0xff

    shl-int/lit8 p2, p2, 0x18

    const v0, 0xffffff

    and-int/2addr p1, v0

    or-int/2addr p1, p2

    .line 23
    iput p1, p0, Lcom/intsig/office/java/awt/Color;->value:I

    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    const/16 v0, 0xff

    .line 1
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/intsig/office/java/awt/Color;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->frgbvalue:[F

    .line 4
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/office/java/awt/Color;->falpha:F

    and-int/lit16 v0, p4, 0xff

    shl-int/lit8 v0, v0, 0x18

    and-int/lit16 v1, p1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, p2, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, p3, 0xff

    shl-int/lit8 v1, v1, 0x0

    or-int/2addr v0, v1

    .line 6
    iput v0, p0, Lcom/intsig/office/java/awt/Color;->value:I

    .line 7
    invoke-static {p1, p2, p3, p4}, Lcom/intsig/office/java/awt/Color;->testColorValueRange(IIII)V

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->frgbvalue:[F

    .line 15
    iput-object v0, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    const/4 v0, 0x0

    .line 16
    iput v0, p0, Lcom/intsig/office/java/awt/Color;->falpha:F

    if-eqz p2, :cond_0

    .line 17
    iput p1, p0, Lcom/intsig/office/java/awt/Color;->value:I

    goto :goto_0

    :cond_0
    const/high16 p2, -0x1000000

    or-int/2addr p1, p2

    .line 18
    iput p1, p0, Lcom/intsig/office/java/awt/Color;->value:I

    :goto_0
    return-void
.end method

.method public static HSBtoRGB(FFF)I
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    const/high16 v1, 0x3f000000    # 0.5f

    .line 3
    .line 4
    const/high16 v2, 0x437f0000    # 255.0f

    .line 5
    .line 6
    const/4 v3, 0x0

    .line 7
    cmpl-float v0, p1, v0

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    mul-float p2, p2, v2

    .line 12
    .line 13
    add-float/2addr p2, v1

    .line 14
    float-to-int p0, p2

    .line 15
    move p1, p0

    .line 16
    move p2, p1

    .line 17
    goto/16 :goto_2

    .line 18
    .line 19
    :cond_0
    float-to-double v4, p0

    .line 20
    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    double-to-float v0, v4

    .line 25
    sub-float/2addr p0, v0

    .line 26
    const/high16 v0, 0x40c00000    # 6.0f

    .line 27
    .line 28
    mul-float p0, p0, v0

    .line 29
    .line 30
    float-to-double v4, p0

    .line 31
    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    .line 32
    .line 33
    .line 34
    move-result-wide v4

    .line 35
    double-to-float v0, v4

    .line 36
    sub-float v0, p0, v0

    .line 37
    .line 38
    const/high16 v4, 0x3f800000    # 1.0f

    .line 39
    .line 40
    sub-float v5, v4, p1

    .line 41
    .line 42
    mul-float v5, v5, p2

    .line 43
    .line 44
    mul-float v6, p1, v0

    .line 45
    .line 46
    sub-float v6, v4, v6

    .line 47
    .line 48
    mul-float v6, v6, p2

    .line 49
    .line 50
    sub-float v0, v4, v0

    .line 51
    .line 52
    mul-float p1, p1, v0

    .line 53
    .line 54
    sub-float/2addr v4, p1

    .line 55
    mul-float v4, v4, p2

    .line 56
    .line 57
    float-to-int p0, p0

    .line 58
    if-eqz p0, :cond_6

    .line 59
    .line 60
    const/4 p1, 0x1

    .line 61
    if-eq p0, p1, :cond_5

    .line 62
    .line 63
    const/4 p1, 0x2

    .line 64
    if-eq p0, p1, :cond_4

    .line 65
    .line 66
    const/4 p1, 0x3

    .line 67
    if-eq p0, p1, :cond_3

    .line 68
    .line 69
    const/4 p1, 0x4

    .line 70
    if-eq p0, p1, :cond_2

    .line 71
    .line 72
    const/4 p1, 0x5

    .line 73
    if-eq p0, p1, :cond_1

    .line 74
    .line 75
    const/4 p0, 0x0

    .line 76
    const/4 p1, 0x0

    .line 77
    const/4 p2, 0x0

    .line 78
    goto :goto_2

    .line 79
    :cond_1
    mul-float p2, p2, v2

    .line 80
    .line 81
    add-float/2addr p2, v1

    .line 82
    float-to-int p0, p2

    .line 83
    mul-float v5, v5, v2

    .line 84
    .line 85
    add-float/2addr v5, v1

    .line 86
    float-to-int p1, v5

    .line 87
    mul-float v6, v6, v2

    .line 88
    .line 89
    add-float/2addr v6, v1

    .line 90
    float-to-int p2, v6

    .line 91
    goto :goto_2

    .line 92
    :cond_2
    mul-float v4, v4, v2

    .line 93
    .line 94
    add-float/2addr v4, v1

    .line 95
    float-to-int p0, v4

    .line 96
    mul-float v5, v5, v2

    .line 97
    .line 98
    add-float/2addr v5, v1

    .line 99
    float-to-int p1, v5

    .line 100
    goto :goto_0

    .line 101
    :cond_3
    mul-float v5, v5, v2

    .line 102
    .line 103
    add-float/2addr v5, v1

    .line 104
    float-to-int p0, v5

    .line 105
    mul-float v6, v6, v2

    .line 106
    .line 107
    add-float/2addr v6, v1

    .line 108
    float-to-int p1, v6

    .line 109
    :goto_0
    mul-float p2, p2, v2

    .line 110
    .line 111
    add-float/2addr p2, v1

    .line 112
    float-to-int p2, p2

    .line 113
    goto :goto_2

    .line 114
    :cond_4
    mul-float v5, v5, v2

    .line 115
    .line 116
    add-float/2addr v5, v1

    .line 117
    float-to-int p0, v5

    .line 118
    mul-float p2, p2, v2

    .line 119
    .line 120
    add-float/2addr p2, v1

    .line 121
    float-to-int p1, p2

    .line 122
    mul-float v4, v4, v2

    .line 123
    .line 124
    add-float/2addr v4, v1

    .line 125
    float-to-int p2, v4

    .line 126
    goto :goto_2

    .line 127
    :cond_5
    mul-float v6, v6, v2

    .line 128
    .line 129
    add-float/2addr v6, v1

    .line 130
    float-to-int p0, v6

    .line 131
    mul-float p2, p2, v2

    .line 132
    .line 133
    add-float/2addr p2, v1

    .line 134
    float-to-int p1, p2

    .line 135
    goto :goto_1

    .line 136
    :cond_6
    mul-float p2, p2, v2

    .line 137
    .line 138
    add-float/2addr p2, v1

    .line 139
    float-to-int p0, p2

    .line 140
    mul-float v4, v4, v2

    .line 141
    .line 142
    add-float/2addr v4, v1

    .line 143
    float-to-int p1, v4

    .line 144
    :goto_1
    mul-float v5, v5, v2

    .line 145
    .line 146
    add-float/2addr v5, v1

    .line 147
    float-to-int p2, v5

    .line 148
    :goto_2
    shl-int/lit8 p0, p0, 0x10

    .line 149
    .line 150
    const/high16 v0, -0x1000000

    .line 151
    .line 152
    or-int/2addr p0, v0

    .line 153
    shl-int/lit8 p1, p1, 0x8

    .line 154
    .line 155
    or-int/2addr p0, p1

    .line 156
    shl-int/lit8 p1, p2, 0x0

    .line 157
    .line 158
    or-int/2addr p0, p1

    .line 159
    return p0
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public static RGBtoHSB(III[F)[F
    .locals 7

    .line 1
    if-nez p3, :cond_0

    .line 2
    .line 3
    const/4 p3, 0x3

    .line 4
    new-array p3, p3, [F

    .line 5
    .line 6
    :cond_0
    if-le p0, p1, :cond_1

    .line 7
    .line 8
    move v0, p0

    .line 9
    goto :goto_0

    .line 10
    :cond_1
    move v0, p1

    .line 11
    :goto_0
    if-le p2, v0, :cond_2

    .line 12
    .line 13
    move v0, p2

    .line 14
    :cond_2
    if-ge p0, p1, :cond_3

    .line 15
    .line 16
    move v1, p0

    .line 17
    goto :goto_1

    .line 18
    :cond_3
    move v1, p1

    .line 19
    :goto_1
    if-ge p2, v1, :cond_4

    .line 20
    .line 21
    move v1, p2

    .line 22
    :cond_4
    int-to-float v2, v0

    .line 23
    const/high16 v3, 0x437f0000    # 255.0f

    .line 24
    .line 25
    div-float v3, v2, v3

    .line 26
    .line 27
    const/4 v4, 0x0

    .line 28
    if-eqz v0, :cond_5

    .line 29
    .line 30
    sub-int v5, v0, v1

    .line 31
    .line 32
    int-to-float v5, v5

    .line 33
    div-float/2addr v5, v2

    .line 34
    goto :goto_2

    .line 35
    :cond_5
    const/4 v5, 0x0

    .line 36
    :goto_2
    cmpl-float v2, v5, v4

    .line 37
    .line 38
    if-nez v2, :cond_6

    .line 39
    .line 40
    goto :goto_4

    .line 41
    :cond_6
    sub-int v2, v0, p0

    .line 42
    .line 43
    int-to-float v2, v2

    .line 44
    sub-int v1, v0, v1

    .line 45
    .line 46
    int-to-float v1, v1

    .line 47
    div-float/2addr v2, v1

    .line 48
    sub-int v6, v0, p1

    .line 49
    .line 50
    int-to-float v6, v6

    .line 51
    div-float/2addr v6, v1

    .line 52
    sub-int p2, v0, p2

    .line 53
    .line 54
    int-to-float p2, p2

    .line 55
    div-float/2addr p2, v1

    .line 56
    if-ne p0, v0, :cond_7

    .line 57
    .line 58
    sub-float/2addr p2, v6

    .line 59
    goto :goto_3

    .line 60
    :cond_7
    if-ne p1, v0, :cond_8

    .line 61
    .line 62
    const/high16 p0, 0x40000000    # 2.0f

    .line 63
    .line 64
    add-float/2addr v2, p0

    .line 65
    sub-float p2, v2, p2

    .line 66
    .line 67
    goto :goto_3

    .line 68
    :cond_8
    const/high16 p0, 0x40800000    # 4.0f

    .line 69
    .line 70
    add-float/2addr v6, p0

    .line 71
    sub-float p2, v6, v2

    .line 72
    .line 73
    :goto_3
    const/high16 p0, 0x40c00000    # 6.0f

    .line 74
    .line 75
    div-float/2addr p2, p0

    .line 76
    cmpg-float p0, p2, v4

    .line 77
    .line 78
    if-gez p0, :cond_9

    .line 79
    .line 80
    const/high16 p0, 0x3f800000    # 1.0f

    .line 81
    .line 82
    add-float v4, p2, p0

    .line 83
    .line 84
    goto :goto_4

    .line 85
    :cond_9
    move v4, p2

    .line 86
    :goto_4
    const/4 p0, 0x0

    .line 87
    aput v4, p3, p0

    .line 88
    .line 89
    const/4 p0, 0x1

    .line 90
    aput v5, p3, p0

    .line 91
    .line 92
    const/4 p0, 0x2

    .line 93
    aput v3, p3, p0

    .line 94
    .line 95
    return-object p3
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public static decode(Ljava/lang/String;)Lcom/intsig/office/java/awt/Color;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .line 1
    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 6
    .line 7
    .line 8
    move-result p0

    .line 9
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 10
    .line 11
    shr-int/lit8 v1, p0, 0x10

    .line 12
    .line 13
    and-int/lit16 v1, v1, 0xff

    .line 14
    .line 15
    shr-int/lit8 v2, p0, 0x8

    .line 16
    .line 17
    and-int/lit16 v2, v2, 0xff

    .line 18
    .line 19
    and-int/lit16 p0, p0, 0xff

    .line 20
    .line 21
    invoke-direct {v0, v1, v2, p0}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 22
    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public static getColor(Ljava/lang/String;)Lcom/intsig/office/java/awt/Color;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, v0}, Lcom/intsig/office/java/awt/Color;->getColor(Ljava/lang/String;Lcom/intsig/office/java/awt/Color;)Lcom/intsig/office/java/awt/Color;

    move-result-object p0

    return-object p0
.end method

.method public static getColor(Ljava/lang/String;I)Lcom/intsig/office/java/awt/Color;
    .locals 2

    .line 5
    invoke-static {p0}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 6
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 7
    :cond_0
    new-instance p0, Lcom/intsig/office/java/awt/Color;

    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    shr-int/lit8 p1, p1, 0x0

    and-int/lit16 p1, p1, 0xff

    invoke-direct {p0, v0, v1, p1}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    return-object p0
.end method

.method public static getColor(Ljava/lang/String;Lcom/intsig/office/java/awt/Color;)Lcom/intsig/office/java/awt/Color;
    .locals 2

    .line 2
    invoke-static {p0}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p0

    if-nez p0, :cond_0

    return-object p1

    .line 3
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 4
    new-instance p1, Lcom/intsig/office/java/awt/Color;

    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    and-int/lit16 p0, p0, 0xff

    invoke-direct {p1, v0, v1, p0}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    return-object p1
.end method

.method public static getHSBColor(FFF)Lcom/intsig/office/java/awt/Color;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    invoke-static {p0, p1, p2}, Lcom/intsig/office/java/awt/Color;->HSBtoRGB(FFF)I

    .line 4
    .line 5
    .line 6
    move-result p0

    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/office/java/awt/Color;-><init>(I)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private static native initIDs()V
.end method

.method private static testColorValueRange(FFFF)V
    .locals 9

    float-to-double v0, p3

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const/4 p3, 0x1

    const-string v4, ""

    const-wide/16 v5, 0x0

    cmpg-double v7, v0, v5

    if-ltz v7, :cond_1

    cmpl-double v7, v0, v2

    if-lez v7, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 6
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " Alpha"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    :goto_1
    float-to-double v7, p0

    cmpg-double p0, v7, v5

    if-ltz p0, :cond_2

    cmpl-double p0, v7, v2

    if-lez p0, :cond_3

    .line 7
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " Red"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    :cond_3
    float-to-double p0, p1

    cmpg-double v1, p0, v5

    if-ltz v1, :cond_4

    cmpl-double v1, p0, v2

    if-lez v1, :cond_5

    .line 8
    :cond_4
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " Green"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    :cond_5
    float-to-double p0, p2

    cmpg-double p2, p0, v5

    if-ltz p2, :cond_6

    cmpl-double p2, p0, v2

    if-lez p2, :cond_7

    .line 9
    :cond_6
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " Blue"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x1

    :cond_7
    if-eq v0, p3, :cond_8

    return-void

    .line 10
    :cond_8
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Color parameter outside of expected range:"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static testColorValueRange(IIII)V
    .locals 3

    const/16 v0, 0xff

    const/4 v1, 0x1

    const-string v2, ""

    if-ltz p3, :cond_1

    if-le p3, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    goto :goto_1

    .line 1
    :cond_1
    :goto_0
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Alpha"

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 p3, 0x1

    :goto_1
    if-ltz p0, :cond_2

    if-le p0, v0, :cond_3

    .line 2
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " Red"

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 p3, 0x1

    :cond_3
    if-ltz p1, :cond_4

    if-le p1, v0, :cond_5

    .line 3
    :cond_4
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " Green"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 p3, 0x1

    :cond_5
    if-ltz p2, :cond_6

    if-le p2, v0, :cond_7

    .line 4
    :cond_6
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " Blue"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 p3, 0x1

    :cond_7
    if-eq p3, v1, :cond_8

    return-void

    .line 5
    :cond_8
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Color parameter outside of expected range:"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public brighter()Lcom/intsig/office/java/awt/Color;
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const/4 v3, 0x3

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    if-nez v1, :cond_0

    .line 17
    .line 18
    if-nez v2, :cond_0

    .line 19
    .line 20
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 21
    .line 22
    invoke-direct {v0, v3, v3, v3}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 23
    .line 24
    .line 25
    return-object v0

    .line 26
    :cond_0
    if-lez v0, :cond_1

    .line 27
    .line 28
    if-ge v0, v3, :cond_1

    .line 29
    .line 30
    const/4 v0, 0x3

    .line 31
    :cond_1
    if-lez v1, :cond_2

    .line 32
    .line 33
    if-ge v1, v3, :cond_2

    .line 34
    .line 35
    const/4 v1, 0x3

    .line 36
    :cond_2
    if-lez v2, :cond_3

    .line 37
    .line 38
    if-ge v2, v3, :cond_3

    .line 39
    .line 40
    const/4 v2, 0x3

    .line 41
    :cond_3
    new-instance v3, Lcom/intsig/office/java/awt/Color;

    .line 42
    .line 43
    int-to-double v4, v0

    .line 44
    const-wide v6, 0x3fe6666666666666L    # 0.7

    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    div-double/2addr v4, v6

    .line 50
    double-to-int v0, v4

    .line 51
    const/16 v4, 0xff

    .line 52
    .line 53
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    int-to-double v8, v1

    .line 58
    div-double/2addr v8, v6

    .line 59
    double-to-int v1, v8

    .line 60
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    int-to-double v8, v2

    .line 65
    div-double/2addr v8, v6

    .line 66
    double-to-int v2, v8

    .line 67
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    invoke-direct {v3, v0, v1, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 72
    .line 73
    .line 74
    return-object v3
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public darker()Lcom/intsig/office/java/awt/Color;
    .locals 8

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    int-to-double v1, v1

    .line 8
    const-wide v3, 0x3fe6666666666666L    # 0.7

    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    mul-double v1, v1, v3

    .line 14
    .line 15
    double-to-int v1, v1

    .line 16
    const/4 v2, 0x0

    .line 17
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 22
    .line 23
    .line 24
    move-result v5

    .line 25
    int-to-double v5, v5

    .line 26
    mul-double v5, v5, v3

    .line 27
    .line 28
    double-to-int v5, v5

    .line 29
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    int-to-double v6, v6

    .line 38
    mul-double v6, v6, v3

    .line 39
    .line 40
    double-to-int v3, v6

    .line 41
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    invoke-direct {v0, v1, v5, v2}, Lcom/intsig/office/java/awt/Color;-><init>(III)V

    .line 46
    .line 47
    .line 48
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/java/awt/Color;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/java/awt/Color;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-ne p1, v0, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getAlpha()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    shr-int/lit8 v0, v0, 0x18

    .line 6
    .line 7
    and-int/lit16 v0, v0, 0xff

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBlue()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    shr-int/lit8 v0, v0, 0x0

    .line 6
    .line 7
    and-int/lit16 v0, v0, 0xff

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getColorComponents([F)[F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/java/awt/Color;->getRGBColorComponents([F)[F

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1

    .line 10
    :cond_0
    array-length v0, v0

    .line 11
    if-nez p1, :cond_1

    .line 12
    .line 13
    new-array p1, v0, [F

    .line 14
    .line 15
    :cond_1
    const/4 v1, 0x0

    .line 16
    :goto_0
    if-ge v1, v0, :cond_2

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    .line 19
    .line 20
    aget v2, v2, v1

    .line 21
    .line 22
    aput v2, p1, v1

    .line 23
    .line 24
    add-int/lit8 v1, v1, 0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_2
    return-object p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getComponents([F)[F
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/office/java/awt/Color;->getRGBComponents([F)[F

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1

    .line 10
    :cond_0
    array-length v0, v0

    .line 11
    if-nez p1, :cond_1

    .line 12
    .line 13
    add-int/lit8 p1, v0, 0x1

    .line 14
    .line 15
    new-array p1, p1, [F

    .line 16
    .line 17
    :cond_1
    const/4 v1, 0x0

    .line 18
    :goto_0
    if-ge v1, v0, :cond_2

    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/office/java/awt/Color;->fvalue:[F

    .line 21
    .line 22
    aget v2, v2, v1

    .line 23
    .line 24
    aput v2, p1, v1

    .line 25
    .line 26
    add-int/lit8 v1, v1, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_2
    iget v1, p0, Lcom/intsig/office/java/awt/Color;->falpha:F

    .line 30
    .line 31
    aput v1, p1, v0

    .line 32
    .line 33
    return-object p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getGreen()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    shr-int/lit8 v0, v0, 0x8

    .line 6
    .line 7
    and-int/lit16 v0, v0, 0xff

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRGB()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Color;->value:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getRGBColorComponents([F)[F
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x3

    .line 4
    new-array p1, p1, [F

    .line 5
    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/Color;->frgbvalue:[F

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    const/4 v2, 0x1

    .line 10
    const/4 v3, 0x0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    int-to-float v0, v0

    .line 18
    const/high16 v4, 0x437f0000    # 255.0f

    .line 19
    .line 20
    div-float/2addr v0, v4

    .line 21
    aput v0, p1, v3

    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    int-to-float v0, v0

    .line 28
    div-float/2addr v0, v4

    .line 29
    aput v0, p1, v2

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    int-to-float v0, v0

    .line 36
    div-float/2addr v0, v4

    .line 37
    aput v0, p1, v1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    aget v4, v0, v3

    .line 41
    .line 42
    aput v4, p1, v3

    .line 43
    .line 44
    aget v3, v0, v2

    .line 45
    .line 46
    aput v3, p1, v2

    .line 47
    .line 48
    aget v0, v0, v1

    .line 49
    .line 50
    aput v0, p1, v1

    .line 51
    .line 52
    :goto_0
    return-object p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getRGBComponents([F)[F
    .locals 6

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x4

    .line 4
    new-array p1, p1, [F

    .line 5
    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/Color;->frgbvalue:[F

    .line 7
    .line 8
    const/4 v1, 0x3

    .line 9
    const/4 v2, 0x2

    .line 10
    const/4 v3, 0x1

    .line 11
    const/4 v4, 0x0

    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    int-to-float v0, v0

    .line 19
    const/high16 v5, 0x437f0000    # 255.0f

    .line 20
    .line 21
    div-float/2addr v0, v5

    .line 22
    aput v0, p1, v4

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    int-to-float v0, v0

    .line 29
    div-float/2addr v0, v5

    .line 30
    aput v0, p1, v3

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    int-to-float v0, v0

    .line 37
    div-float/2addr v0, v5

    .line 38
    aput v0, p1, v2

    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getAlpha()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    int-to-float v0, v0

    .line 45
    div-float/2addr v0, v5

    .line 46
    aput v0, p1, v1

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    aget v5, v0, v4

    .line 50
    .line 51
    aput v5, p1, v4

    .line 52
    .line 53
    aget v4, v0, v3

    .line 54
    .line 55
    aput v4, p1, v3

    .line 56
    .line 57
    aget v0, v0, v2

    .line 58
    .line 59
    aput v0, p1, v2

    .line 60
    .line 61
    iget v0, p0, Lcom/intsig/office/java/awt/Color;->falpha:F

    .line 62
    .line 63
    aput v0, p1, v1

    .line 64
    .line 65
    :goto_0
    return-object p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getRed()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRGB()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    shr-int/lit8 v0, v0, 0x10

    .line 6
    .line 7
    and-int/lit16 v0, v0, 0xff

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Color;->value:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, "[r="

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getRed()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v1, ",g="

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getGreen()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string v1, ",b="

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/Color;->getBlue()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v1, "]"

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    return-object v0
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

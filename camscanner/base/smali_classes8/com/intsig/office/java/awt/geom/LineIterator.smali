.class Lcom/intsig/office/java/awt/geom/LineIterator;
.super Ljava/lang/Object;
.source "LineIterator.java"

# interfaces
.implements Lcom/intsig/office/java/awt/geom/PathIterator;


# instance fields
.field 〇080:Lcom/intsig/office/java/awt/geom/Line2D;

.field 〇o00〇〇Oo:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field 〇o〇:I


# direct methods
.method constructor <init>(Lcom/intsig/office/java/awt/geom/Line2D;Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇o00〇〇Oo:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 8

    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/LineIterator;->isDone()Z

    move-result v0

    if-nez v0, :cond_2

    .line 11
    iget v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇o〇:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 12
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Line2D;->getX1()D

    move-result-wide v3

    aput-wide v3, p1, v2

    .line 13
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Line2D;->getY1()D

    move-result-wide v3

    aput-wide v3, p1, v1

    const/4 v1, 0x0

    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Line2D;->getX2()D

    move-result-wide v3

    aput-wide v3, p1, v2

    .line 15
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Line2D;->getY2()D

    move-result-wide v2

    aput-wide v2, p1, v1

    .line 16
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇o00〇〇Oo:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v2, :cond_1

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v3, p1

    move-object v5, p1

    .line 17
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([DI[DII)V

    :cond_1
    return v1

    .line 18
    :cond_2
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "line iterator out of bounds"

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public currentSegment([F)I
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/LineIterator;->isDone()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2
    iget v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇o〇:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Line2D;->getX1()D

    move-result-wide v3

    double-to-float v0, v3

    aput v0, p1, v2

    .line 4
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Line2D;->getY1()D

    move-result-wide v3

    double-to-float v0, v3

    aput v0, p1, v1

    const/4 v1, 0x0

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Line2D;->getX2()D

    move-result-wide v3

    double-to-float v0, v3

    aput v0, p1, v2

    .line 6
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇080:Lcom/intsig/office/java/awt/geom/Line2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Line2D;->getY2()D

    move-result-wide v2

    double-to-float v0, v2

    aput v0, p1, v1

    .line 7
    :goto_0
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇o00〇〇Oo:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v2, :cond_1

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v3, p1

    move-object v5, p1

    .line 8
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    :cond_1
    return v1

    .line 9
    :cond_2
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "line iterator out of bounds"

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getWindingRule()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDone()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇o〇:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-le v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public next()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇o〇:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iput v0, p0, Lcom/intsig/office/java/awt/geom/LineIterator;->〇o〇:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public abstract Lcom/intsig/office/java/awt/geom/Ellipse2D;
.super Lcom/intsig/office/java/awt/geom/RectangularShape;
.source "Ellipse2D.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/java/awt/geom/Ellipse2D$Double;,
        Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public contains(DD)Z
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    cmpg-double v5, v0, v3

    if-gtz v5, :cond_0

    return v2

    .line 2
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v5

    sub-double/2addr p1, v5

    div-double/2addr p1, v0

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    sub-double/2addr p1, v0

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v5

    cmpg-double v7, v5, v3

    if-gtz v7, :cond_1

    return v2

    .line 4
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    sub-double/2addr p3, v3

    div-double/2addr p3, v5

    sub-double/2addr p3, v0

    mul-double p1, p1, p1

    mul-double p3, p3, p3

    add-double/2addr p1, p3

    const-wide/high16 p3, 0x3fd0000000000000L    # 0.25

    cmpg-double v0, p1, p3

    if-gez v0, :cond_2

    const/4 v2, 0x1

    :cond_2
    return v2
.end method

.method public contains(DDDD)Z
    .locals 1

    .line 5
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/Ellipse2D;->contains(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    add-double/2addr p5, p1

    invoke-virtual {p0, p5, p6, p3, p4}, Lcom/intsig/office/java/awt/geom/Ellipse2D;->contains(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    add-double/2addr p3, p7

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/Ellipse2D;->contains(DD)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p5, p6, p3, p4}, Lcom/intsig/office/java/awt/geom/Ellipse2D;->contains(DD)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p1, p0, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/office/java/awt/geom/Ellipse2D;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_2

    .line 9
    .line 10
    check-cast p1, Lcom/intsig/office/java/awt/geom/Ellipse2D;

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 13
    .line 14
    .line 15
    move-result-wide v3

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 17
    .line 18
    .line 19
    move-result-wide v5

    .line 20
    cmpl-double v1, v3, v5

    .line 21
    .line 22
    if-nez v1, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 25
    .line 26
    .line 27
    move-result-wide v3

    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 29
    .line 30
    .line 31
    move-result-wide v5

    .line 32
    cmpl-double v1, v3, v5

    .line 33
    .line 34
    if-nez v1, :cond_1

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 37
    .line 38
    .line 39
    move-result-wide v3

    .line 40
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 41
    .line 42
    .line 43
    move-result-wide v5

    .line 44
    cmpl-double v1, v3, v5

    .line 45
    .line 46
    if-nez v1, :cond_1

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 49
    .line 50
    .line 51
    move-result-wide v3

    .line 52
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 53
    .line 54
    .line 55
    move-result-wide v5

    .line 56
    cmpl-double p1, v3, v5

    .line 57
    .line 58
    if-nez p1, :cond_1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    const/4 v0, 0x0

    .line 62
    :goto_0
    return v0

    .line 63
    :cond_2
    return v2
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/EllipseIterator;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/java/awt/geom/EllipseIterator;-><init>(Lcom/intsig/office/java/awt/geom/Ellipse2D;Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public hashCode()I
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 10
    .line 11
    .line 12
    move-result-wide v2

    .line 13
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 14
    .line 15
    .line 16
    move-result-wide v2

    .line 17
    const-wide/16 v4, 0x25

    .line 18
    .line 19
    mul-long v2, v2, v4

    .line 20
    .line 21
    add-long/2addr v0, v2

    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 23
    .line 24
    .line 25
    move-result-wide v2

    .line 26
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 27
    .line 28
    .line 29
    move-result-wide v2

    .line 30
    const-wide/16 v4, 0x2b

    .line 31
    .line 32
    mul-long v2, v2, v4

    .line 33
    .line 34
    add-long/2addr v0, v2

    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 36
    .line 37
    .line 38
    move-result-wide v2

    .line 39
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 40
    .line 41
    .line 42
    move-result-wide v2

    .line 43
    const-wide/16 v4, 0x2f

    .line 44
    .line 45
    mul-long v2, v2, v4

    .line 46
    .line 47
    add-long/2addr v0, v2

    .line 48
    long-to-int v2, v0

    .line 49
    const/16 v3, 0x20

    .line 50
    .line 51
    shr-long/2addr v0, v3

    .line 52
    long-to-int v1, v0

    .line 53
    xor-int v0, v2, v1

    .line 54
    .line 55
    return v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public intersects(DDDD)Z
    .locals 13

    .line 1
    const/4 v0, 0x0

    .line 2
    const-wide/16 v1, 0x0

    .line 3
    .line 4
    cmpg-double v3, p5, v1

    .line 5
    .line 6
    if-lez v3, :cond_7

    .line 7
    .line 8
    cmpg-double v3, p7, v1

    .line 9
    .line 10
    if-gtz v3, :cond_0

    .line 11
    .line 12
    goto :goto_2

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 14
    .line 15
    .line 16
    move-result-wide v3

    .line 17
    cmpg-double v5, v3, v1

    .line 18
    .line 19
    if-gtz v5, :cond_1

    .line 20
    .line 21
    return v0

    .line 22
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 23
    .line 24
    .line 25
    move-result-wide v5

    .line 26
    sub-double v5, p1, v5

    .line 27
    .line 28
    div-double/2addr v5, v3

    .line 29
    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    .line 30
    .line 31
    sub-double/2addr v5, v7

    .line 32
    div-double v3, p5, v3

    .line 33
    .line 34
    add-double/2addr v3, v5

    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 36
    .line 37
    .line 38
    move-result-wide v9

    .line 39
    cmpg-double v11, v9, v1

    .line 40
    .line 41
    if-gtz v11, :cond_2

    .line 42
    .line 43
    return v0

    .line 44
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 45
    .line 46
    .line 47
    move-result-wide v11

    .line 48
    sub-double v11, p3, v11

    .line 49
    .line 50
    div-double/2addr v11, v9

    .line 51
    sub-double/2addr v11, v7

    .line 52
    div-double v7, p7, v9

    .line 53
    .line 54
    add-double/2addr v7, v11

    .line 55
    cmpl-double v9, v5, v1

    .line 56
    .line 57
    if-lez v9, :cond_3

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_3
    cmpg-double v5, v3, v1

    .line 61
    .line 62
    if-gez v5, :cond_4

    .line 63
    .line 64
    move-wide v5, v3

    .line 65
    goto :goto_0

    .line 66
    :cond_4
    move-wide v5, v1

    .line 67
    :goto_0
    cmpl-double v3, v11, v1

    .line 68
    .line 69
    if-lez v3, :cond_5

    .line 70
    .line 71
    move-wide v1, v11

    .line 72
    goto :goto_1

    .line 73
    :cond_5
    cmpg-double v3, v7, v1

    .line 74
    .line 75
    if-gez v3, :cond_6

    .line 76
    .line 77
    move-wide v1, v7

    .line 78
    :cond_6
    :goto_1
    mul-double v5, v5, v5

    .line 79
    .line 80
    mul-double v1, v1, v1

    .line 81
    .line 82
    add-double/2addr v5, v1

    .line 83
    const-wide/high16 v1, 0x3fd0000000000000L    # 0.25

    .line 84
    .line 85
    cmpg-double v3, v5, v1

    .line 86
    .line 87
    if-gez v3, :cond_7

    .line 88
    .line 89
    const/4 v0, 0x1

    .line 90
    :cond_7
    :goto_2
    return v0
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

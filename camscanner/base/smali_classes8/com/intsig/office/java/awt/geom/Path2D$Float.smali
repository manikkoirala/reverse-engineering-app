.class public Lcom/intsig/office/java/awt/geom/Path2D$Float;
.super Lcom/intsig/office/java/awt/geom/Path2D;
.source "Path2D.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/Path2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;,
        Lcom/intsig/office/java/awt/geom/Path2D$Float$CopyIterator;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x61046d222872ee96L


# instance fields
.field transient floatCoords:[F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0x14

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/java/awt/geom/Path2D$Float;-><init>(II)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    const/16 v0, 0x14

    .line 2
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/java/awt/geom/Path2D;-><init>(II)V

    mul-int/lit8 p2, p2, 0x2

    .line 4
    new-array p1, p2, [F

    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Shape;)V
    .locals 1

    const/4 v0, 0x0

    .line 5
    invoke-direct {p0, p1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;-><init>(Lcom/intsig/office/java/awt/Shape;Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Shape;Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 2

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Path2D;-><init>()V

    .line 7
    instance-of v0, p1, Lcom/intsig/office/java/awt/geom/Path2D;

    if-eqz v0, :cond_0

    .line 8
    check-cast p1, Lcom/intsig/office/java/awt/geom/Path2D;

    .line 9
    iget v0, p1, Lcom/intsig/office/java/awt/geom/Path2D;->windingRule:I

    invoke-virtual {p0, v0}, Lcom/intsig/office/java/awt/geom/Path2D;->setWindingRule(I)V

    .line 10
    iget v0, p1, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    iput v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 11
    iget-object v0, p1, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    array-length v1, v0

    invoke-static {v0, v1}, Lcom/intsig/office/java/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 12
    iget v0, p1, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    iput v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 13
    invoke-virtual {p1, p2}, Lcom/intsig/office/java/awt/geom/Path2D;->cloneCoordsFloat(Lcom/intsig/office/java/awt/geom/AffineTransform;)[F

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    goto :goto_0

    .line 14
    :cond_0
    invoke-interface {p1, p2}, Lcom/intsig/office/java/awt/Shape;->getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;

    move-result-object p1

    .line 15
    invoke-interface {p1}, Lcom/intsig/office/java/awt/geom/PathIterator;->getWindingRule()I

    move-result p2

    invoke-virtual {p0, p2}, Lcom/intsig/office/java/awt/geom/Path2D;->setWindingRule(I)V

    const/16 p2, 0x14

    new-array p2, p2, [B

    .line 16
    iput-object p2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    const/16 p2, 0x28

    new-array p2, p2, [F

    .line 17
    iput-object p2, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    const/4 p2, 0x0

    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->append(Lcom/intsig/office/java/awt/geom/PathIterator;Z)V

    :goto_0
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-super {p0, p1, v0}, Lcom/intsig/office/java/awt/geom/Path2D;->readObject(Ljava/io/ObjectInputStream;Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-super {p0, p1, v0}, Lcom/intsig/office/java/awt/geom/Path2D;->writeObject(Ljava/io/ObjectOutputStream;Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method append(DD)V
    .locals 3

    .line 3
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    double-to-float p1, p1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    .line 4
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    double-to-float p1, p3

    aput p1, v0, v2

    return-void
.end method

.method append(FF)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    .line 2
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    aput p2, v0, v2

    return-void
.end method

.method public final append(Lcom/intsig/office/java/awt/geom/PathIterator;Z)V
    .locals 13

    const/4 v0, 0x6

    new-array v0, v0, [F

    .line 5
    :goto_0
    invoke-interface {p1}, Lcom/intsig/office/java/awt/geom/PathIterator;->isDone()Z

    move-result v1

    if-nez v1, :cond_7

    .line 6
    invoke-interface {p1, v0}, Lcom/intsig/office/java/awt/geom/PathIterator;->currentSegment([F)I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_3

    if-eq v1, v4, :cond_5

    const/4 p2, 0x3

    const/4 v5, 0x2

    if-eq v1, v5, :cond_2

    if-eq v1, p2, :cond_1

    if-eq v1, v2, :cond_0

    goto :goto_2

    .line 7
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Path2D;->closePath()V

    goto :goto_2

    :cond_1
    aget v7, v0, v3

    aget v8, v0, v4

    aget v9, v0, v5

    aget v10, v0, p2

    aget v11, v0, v2

    const/4 p2, 0x5

    aget v12, v0, p2

    move-object v6, p0

    .line 8
    invoke-virtual/range {v6 .. v12}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->curveTo(FFFFFF)V

    goto :goto_2

    :cond_2
    aget v1, v0, v3

    aget v2, v0, v4

    aget v4, v0, v5

    aget p2, v0, p2

    .line 9
    invoke-virtual {p0, v1, v2, v4, p2}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->quadTo(FFFF)V

    goto :goto_2

    :cond_3
    if-eqz p2, :cond_6

    .line 10
    iget p2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    if-lt p2, v4, :cond_6

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    if-ge v1, v4, :cond_4

    goto :goto_1

    .line 11
    :cond_4
    iget-object v5, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    add-int/lit8 p2, p2, -0x1

    aget-byte p2, v5, p2

    if-eq p2, v2, :cond_5

    iget-object p2, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    add-int/lit8 v2, v1, -0x2

    aget v2, p2, v2

    aget v5, v0, v3

    cmpl-float v2, v2, v5

    if-nez v2, :cond_5

    add-int/lit8 v1, v1, -0x1

    aget p2, p2, v1

    aget v1, v0, v4

    cmpl-float p2, p2, v1

    if-nez p2, :cond_5

    goto :goto_2

    :cond_5
    aget p2, v0, v3

    aget v1, v0, v4

    .line 12
    invoke-virtual {p0, p2, v1}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->lineTo(FF)V

    goto :goto_2

    :cond_6
    :goto_1
    aget p2, v0, v3

    aget v1, v0, v4

    .line 13
    invoke-virtual {p0, p2, v1}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->moveTo(FF)V

    .line 14
    :goto_2
    invoke-interface {p1}, Lcom/intsig/office/java/awt/geom/PathIterator;->next()V

    const/4 p2, 0x0

    goto :goto_0

    :cond_7
    return-void
.end method

.method public final clone()Ljava/lang/Object;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Path2D$Float;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;-><init>(Lcom/intsig/office/java/awt/Shape;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method cloneCoordsDouble(Lcom/intsig/office/java/awt/geom/AffineTransform;)[D
    .locals 7

    .line 1
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 2
    .line 3
    array-length v0, v1

    .line 4
    new-array v6, v0, [D

    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    :goto_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 10
    .line 11
    if-ge p1, v0, :cond_1

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 14
    .line 15
    aget v0, v0, p1

    .line 16
    .line 17
    float-to-double v0, v0

    .line 18
    aput-wide v0, v6, p1

    .line 19
    .line 20
    add-int/lit8 p1, p1, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v2, 0x0

    .line 24
    const/4 v4, 0x0

    .line 25
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 26
    .line 27
    div-int/lit8 v5, v0, 0x2

    .line 28
    .line 29
    move-object v0, p1

    .line 30
    move-object v3, v6

    .line 31
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[DII)V

    .line 32
    .line 33
    .line 34
    :cond_1
    return-object v6
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method cloneCoordsFloat(Lcom/intsig/office/java/awt/geom/AffineTransform;)[F
    .locals 7

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 4
    .line 5
    array-length v0, p1

    .line 6
    invoke-static {p1, v0}, Lcom/intsig/office/java/util/Arrays;->copyOf([FI)[F

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 12
    .line 13
    array-length v0, v1

    .line 14
    new-array v6, v0, [F

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    const/4 v4, 0x0

    .line 18
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 19
    .line 20
    div-int/lit8 v5, v0, 0x2

    .line 21
    .line 22
    move-object v0, p1

    .line 23
    move-object v3, v6

    .line 24
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    .line 25
    .line 26
    .line 27
    move-object p1, v6

    .line 28
    :goto_0
    return-object p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final declared-synchronized curveTo(DDDDDD)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x6

    const/4 v1, 0x1

    .line 1
    :try_start_0
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->needRoom(ZI)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    const/4 v2, 0x3

    aput-byte v2, v0, v1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    double-to-float p1, p1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    double-to-float p2, p3

    .line 4
    aput p2, v0, v2

    add-int/lit8 p2, p1, 0x1

    double-to-float p3, p5

    .line 5
    aput p3, v0, p1

    add-int/lit8 p1, p2, 0x1

    double-to-float p3, p7

    .line 6
    aput p3, v0, p2

    add-int/lit8 p2, p1, 0x1

    double-to-float p3, p9

    .line 7
    aput p3, v0, p1

    add-int/lit8 p1, p2, 0x1

    .line 8
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    double-to-float p1, p11

    aput p1, v0, p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized curveTo(FFFFFF)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x6

    const/4 v1, 0x1

    .line 10
    :try_start_0
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->needRoom(ZI)V

    .line 11
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    const/4 v2, 0x3

    aput-byte v2, v0, v1

    .line 12
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    .line 13
    aput p2, v0, v2

    add-int/lit8 p2, p1, 0x1

    .line 14
    aput p3, v0, p1

    add-int/lit8 p1, p2, 0x1

    .line 15
    aput p4, v0, p2

    add-int/lit8 p2, p1, 0x1

    .line 16
    aput p5, v0, p1

    add-int/lit8 p1, p2, 0x1

    .line 17
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    aput p6, v0, p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 8

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 3
    .line 4
    if-lez v0, :cond_4

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 7
    .line 8
    add-int/lit8 v0, v0, -0x1

    .line 9
    .line 10
    aget v2, v1, v0

    .line 11
    .line 12
    add-int/lit8 v0, v0, -0x1

    .line 13
    .line 14
    aget v1, v1, v0

    .line 15
    .line 16
    move v3, v2

    .line 17
    move v4, v3

    .line 18
    move v2, v1

    .line 19
    :cond_0
    :goto_0
    if-lez v0, :cond_5

    .line 20
    .line 21
    iget-object v5, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 22
    .line 23
    add-int/lit8 v0, v0, -0x1

    .line 24
    .line 25
    aget v6, v5, v0

    .line 26
    .line 27
    add-int/lit8 v0, v0, -0x1

    .line 28
    .line 29
    aget v5, v5, v0

    .line 30
    .line 31
    cmpg-float v7, v5, v1

    .line 32
    .line 33
    if-gez v7, :cond_1

    .line 34
    .line 35
    move v1, v5

    .line 36
    :cond_1
    cmpg-float v7, v6, v3

    .line 37
    .line 38
    if-gez v7, :cond_2

    .line 39
    .line 40
    move v3, v6

    .line 41
    :cond_2
    cmpl-float v7, v5, v2

    .line 42
    .line 43
    if-lez v7, :cond_3

    .line 44
    .line 45
    move v2, v5

    .line 46
    :cond_3
    cmpl-float v5, v6, v4

    .line 47
    .line 48
    if-lez v5, :cond_0

    .line 49
    .line 50
    move v4, v6

    .line 51
    goto :goto_0

    .line 52
    :cond_4
    const/4 v1, 0x0

    .line 53
    const/4 v2, 0x0

    .line 54
    const/4 v3, 0x0

    .line 55
    const/4 v4, 0x0

    .line 56
    :cond_5
    new-instance v0, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 57
    .line 58
    sub-float/2addr v2, v1

    .line 59
    sub-float/2addr v4, v3

    .line 60
    invoke-direct {v0, v1, v3, v2, v4}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    .line 62
    .line 63
    monitor-exit p0

    .line 64
    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    .line 66
    monitor-exit p0

    .line 67
    throw v0
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    new-instance p1, Lcom/intsig/office/java/awt/geom/Path2D$Float$CopyIterator;

    .line 4
    .line 5
    invoke-direct {p1, p0}, Lcom/intsig/office/java/awt/geom/Path2D$Float$CopyIterator;-><init>(Lcom/intsig/office/java/awt/geom/Path2D$Float;)V

    .line 6
    .line 7
    .line 8
    return-object p1

    .line 9
    :cond_0
    new-instance v0, Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;

    .line 10
    .line 11
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/java/awt/geom/Path2D$Float$TxIterator;-><init>(Lcom/intsig/office/java/awt/geom/Path2D$Float;Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method getPoint(I)Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 4
    .line 5
    aget v2, v1, p1

    .line 6
    .line 7
    add-int/lit8 p1, p1, 0x1

    .line 8
    .line 9
    aget p1, v1, p1

    .line 10
    .line 11
    invoke-direct {v0, v2, p1}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>(FF)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public final declared-synchronized lineTo(DD)V
    .locals 4

    monitor-enter p0

    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 1
    :try_start_0
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->needRoom(ZI)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    aput-byte v1, v0, v2

    .line 3
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    double-to-float p1, p1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    .line 4
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    double-to-float p1, p3

    aput p1, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized lineTo(FF)V
    .locals 4

    monitor-enter p0

    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 6
    :try_start_0
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->needRoom(ZI)V

    .line 7
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    aput-byte v1, v0, v2

    .line 8
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    .line 9
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    aput p2, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized moveTo(DD)V
    .locals 4

    monitor-enter p0

    .line 1
    :try_start_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    add-int/lit8 v0, v0, -0x1

    aget-byte v0, v1, v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, -0x2

    double-to-float p1, p1

    aput p1, v0, v2

    add-int/lit8 v1, v1, -0x1

    double-to-float p1, p3

    .line 3
    aput p1, v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 4
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->needRoom(ZI)V

    .line 5
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    aput-byte v1, v0, v2

    .line 6
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    double-to-float p1, p1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    .line 7
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    double-to-float p1, p3

    aput p1, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized moveTo(FF)V
    .locals 4

    monitor-enter p0

    .line 9
    :try_start_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    add-int/lit8 v0, v0, -0x1

    aget-byte v0, v1, v0

    if-nez v0, :cond_0

    .line 10
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, -0x2

    aput p1, v0, v2

    add-int/lit8 v1, v1, -0x1

    .line 11
    aput p2, v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 12
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->needRoom(ZI)V

    .line 13
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    aput-byte v1, v0, v2

    .line 14
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    .line 15
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    aput p2, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method needRoom(ZI)V
    .locals 2

    .line 1
    iget p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 4
    .line 5
    array-length v1, v0

    .line 6
    if-lt p1, v1, :cond_1

    .line 7
    .line 8
    const/16 p1, 0x1f4

    .line 9
    .line 10
    if-le v1, p1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move p1, v1

    .line 14
    :goto_0
    add-int/2addr v1, p1

    .line 15
    invoke-static {v0, v1}, Lcom/intsig/office/java/util/Arrays;->copyOf([BI)[B

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    .line 20
    .line 21
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 22
    .line 23
    array-length v0, p1

    .line 24
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 25
    .line 26
    add-int/2addr v1, p2

    .line 27
    if-le v1, v0, :cond_4

    .line 28
    .line 29
    const/16 v1, 0x3e8

    .line 30
    .line 31
    if-le v0, v1, :cond_2

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_2
    move v1, v0

    .line 35
    :goto_1
    if-ge v1, p2, :cond_3

    .line 36
    .line 37
    goto :goto_2

    .line 38
    :cond_3
    move p2, v1

    .line 39
    :goto_2
    add-int/2addr v0, p2

    .line 40
    invoke-static {p1, v0}, Lcom/intsig/office/java/util/Arrays;->copyOf([FI)[F

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 45
    .line 46
    :cond_4
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method pointCrossings(DD)I
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 2
    .line 3
    const/4 p2, 0x0

    .line 4
    aget p3, p1, p2

    .line 5
    .line 6
    const/4 p3, 0x1

    .line 7
    aget p1, p1, p3

    .line 8
    .line 9
    return p2
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final declared-synchronized quadTo(DDDD)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x4

    const/4 v1, 0x1

    .line 1
    :try_start_0
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->needRoom(ZI)V

    .line 2
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    const/4 v2, 0x2

    aput-byte v2, v0, v1

    .line 3
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    double-to-float p1, p1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    double-to-float p2, p3

    .line 4
    aput p2, v0, v2

    add-int/lit8 p2, p1, 0x1

    double-to-float p3, p5

    .line 5
    aput p3, v0, p1

    add-int/lit8 p1, p2, 0x1

    .line 6
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    double-to-float p1, p7

    aput p1, v0, p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized quadTo(FFFF)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x4

    const/4 v1, 0x1

    .line 8
    :try_start_0
    invoke-virtual {p0, v1, v0}, Lcom/intsig/office/java/awt/geom/Path2D$Float;->needRoom(ZI)V

    .line 9
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->pointTypes:[B

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numTypes:I

    const/4 v2, 0x2

    aput-byte v2, v0, v1

    .line 10
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    iget v1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    add-int/lit8 v2, v1, 0x1

    aput p1, v0, v1

    add-int/lit8 p1, v2, 0x1

    .line 11
    aput p2, v0, v2

    add-int/lit8 p2, p1, 0x1

    .line 12
    aput p3, v0, p1

    add-int/lit8 p1, p2, 0x1

    .line 13
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    aput p4, v0, p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 14
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method rectCrossings(DDDD)I
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 2
    .line 3
    const/4 p2, 0x0

    .line 4
    aget p3, p1, p2

    .line 5
    .line 6
    const/4 p3, 0x1

    .line 7
    aget p1, p1, p3

    .line 8
    .line 9
    return p2
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public final transform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 6

    .line 1
    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/Path2D$Float;->floatCoords:[F

    .line 2
    .line 3
    const/4 v2, 0x0

    .line 4
    const/4 v4, 0x0

    .line 5
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Path2D;->numCoords:I

    .line 6
    .line 7
    div-int/lit8 v5, v0, 0x2

    .line 8
    .line 9
    move-object v0, p1

    .line 10
    move-object v1, v3

    .line 11
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

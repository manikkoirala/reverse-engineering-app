.class Lcom/intsig/office/java/awt/geom/RectIterator;
.super Ljava/lang/Object;
.source "RectIterator.java"

# interfaces
.implements Lcom/intsig/office/java/awt/geom/PathIterator;


# instance fields
.field O8:D

.field Oo08:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field o〇0:I

.field 〇080:D

.field 〇o00〇〇Oo:D

.field 〇o〇:D


# direct methods
.method constructor <init>(Lcom/intsig/office/java/awt/geom/Rectangle2D;Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 5
    .line 6
    .line 7
    move-result-wide v0

    .line 8
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇080:D

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 11
    .line 12
    .line 13
    move-result-wide v0

    .line 14
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇o00〇〇Oo:D

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇o〇:D

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 23
    .line 24
    .line 25
    move-result-wide v0

    .line 26
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->O8:D

    .line 27
    .line 28
    iput-object p2, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->Oo08:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 29
    .line 30
    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇o〇:D

    .line 31
    .line 32
    const-wide/16 v2, 0x0

    .line 33
    .line 34
    cmpg-double v4, p1, v2

    .line 35
    .line 36
    if-ltz v4, :cond_0

    .line 37
    .line 38
    cmpg-double p1, v0, v2

    .line 39
    .line 40
    if-gez p1, :cond_1

    .line 41
    .line 42
    :cond_0
    const/4 p1, 0x6

    .line 43
    iput p1, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->o〇0:I

    .line 44
    .line 45
    :cond_1
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 13

    .line 11
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectIterator;->isDone()Z

    move-result v0

    if-nez v0, :cond_7

    .line 12
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->o〇0:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 p1, 0x4

    return p1

    .line 13
    :cond_0
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇080:D

    const/4 v3, 0x0

    aput-wide v1, p1, v3

    .line 14
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇o00〇〇Oo:D

    const/4 v6, 0x1

    aput-wide v4, p1, v6

    const/4 v7, 0x2

    if-eq v0, v6, :cond_1

    if-ne v0, v7, :cond_2

    .line 15
    :cond_1
    iget-wide v8, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇o〇:D

    add-double/2addr v1, v8

    aput-wide v1, p1, v3

    :cond_2
    if-eq v0, v7, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 16
    :cond_3
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->O8:D

    add-double/2addr v4, v0

    aput-wide v4, p1, v6

    .line 17
    :cond_4
    iget-object v7, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->Oo08:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v7, :cond_5

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object v8, p1

    move-object v10, p1

    .line 18
    invoke-virtual/range {v7 .. v12}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([DI[DII)V

    .line 19
    :cond_5
    iget p1, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->o〇0:I

    if-nez p1, :cond_6

    goto :goto_0

    :cond_6
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 20
    :cond_7
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "rect iterator out of bounds"

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public currentSegment([F)I
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectIterator;->isDone()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->o〇0:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 p1, 0x4

    return p1

    .line 3
    :cond_0
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇080:D

    double-to-float v1, v1

    const/4 v2, 0x0

    aput v1, p1, v2

    .line 4
    iget-wide v3, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇o00〇〇Oo:D

    double-to-float v3, v3

    const/4 v4, 0x1

    aput v3, p1, v4

    const/4 v5, 0x2

    if-eq v0, v4, :cond_1

    if-ne v0, v5, :cond_2

    .line 5
    :cond_1
    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->〇o〇:D

    double-to-float v6, v6

    add-float/2addr v1, v6

    aput v1, p1, v2

    :cond_2
    if-eq v0, v5, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 6
    :cond_3
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->O8:D

    double-to-float v0, v0

    add-float/2addr v3, v0

    aput v3, p1, v4

    .line 7
    :cond_4
    iget-object v5, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->Oo08:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v5, :cond_5

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v6, p1

    move-object v8, p1

    .line 8
    invoke-virtual/range {v5 .. v10}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    .line 9
    :cond_5
    iget p1, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->o〇0:I

    if-nez p1, :cond_6

    goto :goto_0

    :cond_6
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 10
    :cond_7
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "rect iterator out of bounds"

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getWindingRule()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDone()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->o〇0:I

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    if-le v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public next()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->o〇0:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iput v0, p0, Lcom/intsig/office/java/awt/geom/RectIterator;->o〇0:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

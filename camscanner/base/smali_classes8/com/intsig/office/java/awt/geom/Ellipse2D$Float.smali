.class public Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;
.super Lcom/intsig/office/java/awt/geom/Ellipse2D;
.source "Ellipse2D.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/Ellipse2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x5c0fdab63c697049L


# instance fields
.field public height:F

.field public width:F

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Ellipse2D;-><init>()V

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Ellipse2D;-><init>()V

    .line 3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->setFrame(FFFF)V

    return-void
.end method


# virtual methods
.method public getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->x:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->y:F

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->width:F

    .line 8
    .line 9
    iget v4, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->height:F

    .line 10
    .line 11
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeight()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->height:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWidth()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->width:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getX()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->x:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getY()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->y:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isEmpty()Z
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->width:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    const-wide/16 v2, 0x0

    .line 5
    .line 6
    cmpg-double v4, v0, v2

    .line 7
    .line 8
    if-lez v4, :cond_1

    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->height:F

    .line 11
    .line 12
    float-to-double v0, v0

    .line 13
    cmpg-double v4, v0, v2

    .line 14
    .line 15
    if-gtz v4, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 21
    :goto_1
    return v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setFrame(DDDD)V
    .locals 0

    double-to-float p1, p1

    .line 5
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->x:F

    double-to-float p1, p3

    .line 6
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->y:F

    double-to-float p1, p5

    .line 7
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->width:F

    double-to-float p1, p7

    .line 8
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->height:F

    return-void
.end method

.method public setFrame(FFFF)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->x:F

    .line 2
    iput p2, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->y:F

    .line 3
    iput p3, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->width:F

    .line 4
    iput p4, p0, Lcom/intsig/office/java/awt/geom/Ellipse2D$Float;->height:F

    return-void
.end method

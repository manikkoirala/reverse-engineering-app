.class public Lcom/intsig/office/java/awt/Rectangle;
.super Lcom/intsig/office/java/awt/geom/Rectangle2D;
.source "Rectangle.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x3c4f95fae535958cL


# instance fields
.field public height:I

.field public width:I

.field public x:I

.field public y:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0, v0, v0, v0}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0, v0, v0, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/Rectangle2D;-><init>()V

    .line 4
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 5
    iput p2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 6
    iput p3, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 7
    iput p4, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Point;)V
    .locals 2

    .line 10
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1, v1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Point;Lcom/intsig/office/java/awt/Dimension;)V
    .locals 2

    .line 9
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    iget v1, p2, Lcom/intsig/office/java/awt/Dimension;->width:I

    iget p2, p2, Lcom/intsig/office/java/awt/Dimension;->height:I

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Dimension;)V
    .locals 2

    .line 11
    iget v0, p1, Lcom/intsig/office/java/awt/Dimension;->width:I

    iget p1, p1, Lcom/intsig/office/java/awt/Dimension;->height:I

    const/4 v1, 0x0

    invoke-direct {p0, v1, v1, v0, p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 3

    .line 2
    iget v0, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    iget v2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    return-void
.end method

.method private static clip(DZ)I
    .locals 3

    .line 1
    const-wide/high16 v0, -0x3e20000000000000L    # -2.147483648E9

    .line 2
    .line 3
    cmpg-double v2, p0, v0

    .line 4
    .line 5
    if-gtz v2, :cond_0

    .line 6
    .line 7
    const/high16 p0, -0x80000000

    .line 8
    .line 9
    return p0

    .line 10
    :cond_0
    const-wide v0, 0x41dfffffffc00000L    # 2.147483647E9

    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    cmpl-double v2, p0, v0

    .line 16
    .line 17
    if-ltz v2, :cond_1

    .line 18
    .line 19
    const p0, 0x7fffffff

    .line 20
    .line 21
    .line 22
    return p0

    .line 23
    :cond_1
    if-eqz p2, :cond_2

    .line 24
    .line 25
    invoke-static {p0, p1}, Ljava/lang/Math;->ceil(D)D

    .line 26
    .line 27
    .line 28
    move-result-wide p0

    .line 29
    goto :goto_0

    .line 30
    :cond_2
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    .line 31
    .line 32
    .line 33
    move-result-wide p0

    .line 34
    :goto_0
    double-to-int p0, p0

    .line 35
    return p0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static native initIDs()V
.end method


# virtual methods
.method public add(II)V
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    or-int v2, v0, v1

    if-gez v2, :cond_0

    .line 2
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 3
    iput p2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    const/4 p1, 0x0

    .line 4
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    return-void

    .line 5
    :cond_0
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 6
    iget v3, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    int-to-long v4, v0

    int-to-long v0, v1

    int-to-long v6, v2

    add-long/2addr v4, v6

    int-to-long v6, v3

    add-long/2addr v0, v6

    if-le v2, p1, :cond_1

    move v2, p1

    :cond_1
    if-le v3, p2, :cond_2

    move v3, p2

    :cond_2
    int-to-long v6, p1

    cmp-long p1, v4, v6

    if-gez p1, :cond_3

    move-wide v4, v6

    :cond_3
    int-to-long p1, p2

    cmp-long v6, v0, p1

    if-gez v6, :cond_4

    move-wide v0, p1

    :cond_4
    int-to-long p1, v2

    sub-long/2addr v4, p1

    int-to-long p1, v3

    sub-long/2addr v0, p1

    const-wide/32 p1, 0x7fffffff

    cmp-long v6, v4, p1

    if-lez v6, :cond_5

    move-wide v4, p1

    :cond_5
    cmp-long v6, v0, p1

    if-lez v6, :cond_6

    move-wide v0, p1

    :cond_6
    long-to-int p1, v4

    long-to-int p2, v0

    .line 7
    invoke-virtual {p0, v2, v3, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;->reshape(IIII)V

    return-void
.end method

.method public add(Landroid/graphics/Point;)V
    .locals 1

    .line 8
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/java/awt/Rectangle;->add(II)V

    return-void
.end method

.method public add(Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 13

    .line 9
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    int-to-long v0, v0

    .line 10
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    int-to-long v2, v2

    or-long v4, v0, v2

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-gez v8, :cond_0

    .line 11
    iget v4, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    iget v5, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    iget v8, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    iget v9, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v4, v5, v8, v9}, Lcom/intsig/office/java/awt/Rectangle;->reshape(IIII)V

    .line 12
    :cond_0
    iget v4, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    int-to-long v4, v4

    .line 13
    iget v8, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    int-to-long v8, v8

    or-long v10, v4, v8

    cmp-long v12, v10, v6

    if-gez v12, :cond_1

    return-void

    .line 14
    :cond_1
    iget v6, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 15
    iget v7, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    int-to-long v10, v6

    add-long/2addr v0, v10

    int-to-long v10, v7

    add-long/2addr v2, v10

    .line 16
    iget v10, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 17
    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    int-to-long v11, v10

    add-long/2addr v4, v11

    int-to-long v11, p1

    add-long/2addr v8, v11

    if-le v6, v10, :cond_2

    move v6, v10

    :cond_2
    if-le v7, p1, :cond_3

    move v7, p1

    :cond_3
    cmp-long p1, v0, v4

    if-gez p1, :cond_4

    move-wide v0, v4

    :cond_4
    cmp-long p1, v2, v8

    if-gez p1, :cond_5

    move-wide v2, v8

    :cond_5
    int-to-long v4, v6

    sub-long/2addr v0, v4

    int-to-long v4, v7

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long p1, v0, v4

    if-lez p1, :cond_6

    move-wide v0, v4

    :cond_6
    cmp-long p1, v2, v4

    if-lez p1, :cond_7

    move-wide v2, v4

    :cond_7
    long-to-int p1, v0

    long-to-int v0, v2

    .line 18
    invoke-virtual {p0, v6, v7, p1, v0}, Lcom/intsig/office/java/awt/Rectangle;->reshape(IIII)V

    return-void
.end method

.method public contains(II)Z
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;->inside(II)Z

    move-result p1

    return p1
.end method

.method public contains(IIII)Z
    .locals 5

    .line 4
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 5
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    or-int v2, v0, v1

    or-int/2addr v2, p3

    or-int/2addr v2, p4

    const/4 v3, 0x0

    if-gez v2, :cond_0

    return v3

    .line 6
    :cond_0
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 7
    iget v4, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    if-lt p1, v2, :cond_8

    if-ge p2, v4, :cond_1

    goto :goto_0

    :cond_1
    add-int/2addr v0, v2

    add-int/2addr p3, p1

    if-gt p3, p1, :cond_3

    if-ge v0, v2, :cond_2

    if-le p3, v0, :cond_4

    :cond_2
    return v3

    :cond_3
    if-lt v0, v2, :cond_4

    if-le p3, v0, :cond_4

    return v3

    :cond_4
    add-int/2addr v1, v4

    add-int/2addr p4, p2

    if-gt p4, p2, :cond_6

    if-ge v1, v4, :cond_5

    if-le p4, v1, :cond_7

    :cond_5
    return v3

    :cond_6
    if-lt v1, v4, :cond_7

    if-le p4, v1, :cond_7

    return v3

    :cond_7
    const/4 p1, 0x1

    return p1

    :cond_8
    :goto_0
    return v3
.end method

.method public contains(Landroid/graphics/Point;)Z
    .locals 1

    .line 1
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/java/awt/Rectangle;->contains(II)Z

    move-result p1

    return p1
.end method

.method public contains(Lcom/intsig/office/java/awt/Rectangle;)Z
    .locals 3

    .line 3
    iget v0, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    iget v2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/intsig/office/java/awt/Rectangle;->contains(IIII)Z

    move-result p1

    return p1
.end method

.method public createIntersection(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/java/awt/Rectangle;->intersection(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1

    .line 12
    :cond_0
    new-instance v0, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    .line 13
    .line 14
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-static {p0, p1, v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->intersect(Lcom/intsig/office/java/awt/geom/Rectangle2D;Lcom/intsig/office/java/awt/geom/Rectangle2D;Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 18
    .line 19
    .line 20
    return-object v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public createUnion(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/office/java/awt/Rectangle;->union(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1

    .line 12
    :cond_0
    new-instance v0, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    .line 13
    .line 14
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-static {p0, p1, v0}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->union(Lcom/intsig/office/java/awt/geom/Rectangle2D;Lcom/intsig/office/java/awt/geom/Rectangle2D;Lcom/intsig/office/java/awt/geom/Rectangle2D;)V

    .line 18
    .line 19
    .line 20
    return-object v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 6
    .line 7
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 8
    .line 9
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 10
    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 14
    .line 15
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 16
    .line 17
    if-ne v0, v1, :cond_0

    .line 18
    .line 19
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 20
    .line 21
    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 22
    .line 23
    if-ne v0, v1, :cond_0

    .line 24
    .line 25
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 26
    .line 27
    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 28
    .line 29
    if-ne v0, p1, :cond_0

    .line 30
    .line 31
    const/4 p1, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 p1, 0x0

    .line 34
    :goto_0
    return p1

    .line 35
    :cond_1
    invoke-super {p0, p1}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->equals(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    return p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getBounds()Lcom/intsig/office/java/awt/Rectangle;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 8
    .line 9
    iget v4, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 10
    .line 11
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 8
    .line 9
    iget v4, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 10
    .line 11
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeight()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 2
    .line 3
    int-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getLocation()Landroid/graphics/Point;
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Point;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSize()Lcom/intsig/office/java/awt/Dimension;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/Dimension;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/office/java/awt/Dimension;-><init>(II)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWidth()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 2
    .line 3
    int-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getX()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 2
    .line 3
    int-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getY()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 2
    .line 3
    int-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public grow(II)V
    .locals 11

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 2
    .line 3
    int-to-long v0, v0

    .line 4
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 5
    .line 6
    int-to-long v2, v2

    .line 7
    iget v4, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 8
    .line 9
    int-to-long v4, v4

    .line 10
    iget v6, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 11
    .line 12
    int-to-long v6, v6

    .line 13
    add-long/2addr v4, v0

    .line 14
    add-long/2addr v6, v2

    .line 15
    int-to-long v8, p1

    .line 16
    sub-long/2addr v0, v8

    .line 17
    int-to-long p1, p2

    .line 18
    sub-long/2addr v2, p1

    .line 19
    add-long/2addr v4, v8

    .line 20
    add-long/2addr v6, p1

    .line 21
    const-wide/32 p1, 0x7fffffff

    .line 22
    .line 23
    .line 24
    const-wide/32 v8, -0x80000000

    .line 25
    .line 26
    .line 27
    cmp-long v10, v4, v0

    .line 28
    .line 29
    if-gez v10, :cond_2

    .line 30
    .line 31
    sub-long/2addr v4, v0

    .line 32
    cmp-long v10, v4, v8

    .line 33
    .line 34
    if-gez v10, :cond_0

    .line 35
    .line 36
    move-wide v4, v8

    .line 37
    :cond_0
    cmp-long v10, v0, v8

    .line 38
    .line 39
    if-gez v10, :cond_1

    .line 40
    .line 41
    move-wide v0, v8

    .line 42
    goto :goto_1

    .line 43
    :cond_1
    cmp-long v10, v0, p1

    .line 44
    .line 45
    if-lez v10, :cond_6

    .line 46
    .line 47
    move-wide v0, p1

    .line 48
    goto :goto_1

    .line 49
    :cond_2
    cmp-long v10, v0, v8

    .line 50
    .line 51
    if-gez v10, :cond_3

    .line 52
    .line 53
    move-wide v0, v8

    .line 54
    goto :goto_0

    .line 55
    :cond_3
    cmp-long v10, v0, p1

    .line 56
    .line 57
    if-lez v10, :cond_4

    .line 58
    .line 59
    move-wide v0, p1

    .line 60
    :cond_4
    :goto_0
    sub-long/2addr v4, v0

    .line 61
    cmp-long v10, v4, v8

    .line 62
    .line 63
    if-gez v10, :cond_5

    .line 64
    .line 65
    move-wide v4, v8

    .line 66
    goto :goto_1

    .line 67
    :cond_5
    cmp-long v10, v4, p1

    .line 68
    .line 69
    if-lez v10, :cond_6

    .line 70
    .line 71
    move-wide v4, p1

    .line 72
    :cond_6
    :goto_1
    cmp-long v10, v6, v2

    .line 73
    .line 74
    if-gez v10, :cond_a

    .line 75
    .line 76
    sub-long/2addr v6, v2

    .line 77
    cmp-long v10, v6, v8

    .line 78
    .line 79
    if-gez v10, :cond_7

    .line 80
    .line 81
    move-wide v6, v8

    .line 82
    :cond_7
    cmp-long v10, v2, v8

    .line 83
    .line 84
    if-gez v10, :cond_8

    .line 85
    .line 86
    move-wide p1, v6

    .line 87
    move-wide v2, v8

    .line 88
    goto :goto_3

    .line 89
    :cond_8
    cmp-long v8, v2, p1

    .line 90
    .line 91
    if-lez v8, :cond_9

    .line 92
    .line 93
    move-wide v2, p1

    .line 94
    :cond_9
    move-wide p1, v6

    .line 95
    goto :goto_3

    .line 96
    :cond_a
    cmp-long v10, v2, v8

    .line 97
    .line 98
    if-gez v10, :cond_b

    .line 99
    .line 100
    move-wide v2, v8

    .line 101
    goto :goto_2

    .line 102
    :cond_b
    cmp-long v10, v2, p1

    .line 103
    .line 104
    if-lez v10, :cond_c

    .line 105
    .line 106
    move-wide v2, p1

    .line 107
    :cond_c
    :goto_2
    sub-long/2addr v6, v2

    .line 108
    cmp-long v10, v6, v8

    .line 109
    .line 110
    if-gez v10, :cond_d

    .line 111
    .line 112
    move-wide p1, v8

    .line 113
    goto :goto_3

    .line 114
    :cond_d
    cmp-long v8, v6, p1

    .line 115
    .line 116
    if-lez v8, :cond_9

    .line 117
    .line 118
    :goto_3
    long-to-int v1, v0

    .line 119
    long-to-int v0, v2

    .line 120
    long-to-int v2, v4

    .line 121
    long-to-int p2, p1

    .line 122
    invoke-virtual {p0, v1, v0, v2, p2}, Lcom/intsig/office/java/awt/Rectangle;->reshape(IIII)V

    .line 123
    .line 124
    .line 125
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public inside(II)Z
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 4
    .line 5
    or-int v2, v0, v1

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    if-gez v2, :cond_0

    .line 9
    .line 10
    return v3

    .line 11
    :cond_0
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 12
    .line 13
    iget v4, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 14
    .line 15
    if-lt p1, v2, :cond_4

    .line 16
    .line 17
    if-ge p2, v4, :cond_1

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_1
    add-int/2addr v0, v2

    .line 21
    add-int/2addr v1, v4

    .line 22
    if-lt v0, v2, :cond_2

    .line 23
    .line 24
    if-le v0, p1, :cond_4

    .line 25
    .line 26
    :cond_2
    if-lt v1, v4, :cond_3

    .line 27
    .line 28
    if-le v1, p2, :cond_4

    .line 29
    .line 30
    :cond_3
    const/4 v3, 0x1

    .line 31
    :cond_4
    :goto_0
    return v3
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public intersection(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 14

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 4
    .line 5
    iget v2, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 6
    .line 7
    iget v3, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 8
    .line 9
    int-to-long v4, v0

    .line 10
    iget v6, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 11
    .line 12
    int-to-long v6, v6

    .line 13
    add-long/2addr v4, v6

    .line 14
    int-to-long v6, v1

    .line 15
    iget v8, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 16
    .line 17
    int-to-long v8, v8

    .line 18
    add-long/2addr v6, v8

    .line 19
    int-to-long v8, v2

    .line 20
    iget v10, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 21
    .line 22
    int-to-long v10, v10

    .line 23
    add-long/2addr v8, v10

    .line 24
    int-to-long v10, v3

    .line 25
    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 26
    .line 27
    int-to-long v12, p1

    .line 28
    add-long/2addr v10, v12

    .line 29
    if-ge v0, v2, :cond_0

    .line 30
    .line 31
    move v0, v2

    .line 32
    :cond_0
    if-ge v1, v3, :cond_1

    .line 33
    .line 34
    move v1, v3

    .line 35
    :cond_1
    cmp-long p1, v4, v8

    .line 36
    .line 37
    if-lez p1, :cond_2

    .line 38
    .line 39
    move-wide v4, v8

    .line 40
    :cond_2
    cmp-long p1, v6, v10

    .line 41
    .line 42
    if-lez p1, :cond_3

    .line 43
    .line 44
    move-wide v6, v10

    .line 45
    :cond_3
    int-to-long v2, v0

    .line 46
    sub-long/2addr v4, v2

    .line 47
    int-to-long v2, v1

    .line 48
    sub-long/2addr v6, v2

    .line 49
    const-wide/32 v2, -0x80000000

    .line 50
    .line 51
    .line 52
    cmp-long p1, v4, v2

    .line 53
    .line 54
    if-gez p1, :cond_4

    .line 55
    .line 56
    move-wide v4, v2

    .line 57
    :cond_4
    cmp-long p1, v6, v2

    .line 58
    .line 59
    if-gez p1, :cond_5

    .line 60
    .line 61
    move-wide v6, v2

    .line 62
    :cond_5
    new-instance p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 63
    .line 64
    long-to-int v2, v4

    .line 65
    long-to-int v3, v6

    .line 66
    invoke-direct {p1, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 67
    .line 68
    .line 69
    return-object p1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public intersects(Lcom/intsig/office/java/awt/Rectangle;)Z
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 4
    .line 5
    iget v2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 6
    .line 7
    iget v3, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    if-lez v2, :cond_5

    .line 11
    .line 12
    if-lez v3, :cond_5

    .line 13
    .line 14
    if-lez v0, :cond_5

    .line 15
    .line 16
    if-gtz v1, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget v5, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 20
    .line 21
    iget v6, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 22
    .line 23
    iget v7, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 24
    .line 25
    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 26
    .line 27
    add-int/2addr v2, v7

    .line 28
    add-int/2addr v3, p1

    .line 29
    add-int/2addr v0, v5

    .line 30
    add-int/2addr v1, v6

    .line 31
    if-lt v2, v7, :cond_1

    .line 32
    .line 33
    if-le v2, v5, :cond_5

    .line 34
    .line 35
    :cond_1
    if-lt v3, p1, :cond_2

    .line 36
    .line 37
    if-le v3, v6, :cond_5

    .line 38
    .line 39
    :cond_2
    if-lt v0, v5, :cond_3

    .line 40
    .line 41
    if-le v0, v7, :cond_5

    .line 42
    .line 43
    :cond_3
    if-lt v1, v6, :cond_4

    .line 44
    .line 45
    if-le v1, p1, :cond_5

    .line 46
    .line 47
    :cond_4
    const/4 v4, 0x1

    .line 48
    :cond_5
    :goto_0
    return v4
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public isEmpty()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 2
    .line 3
    if-lez v0, :cond_1

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 6
    .line 7
    if-gtz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public move(II)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public outcode(DD)I
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 2
    .line 3
    if-gtz v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x5

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 8
    .line 9
    int-to-double v2, v1

    .line 10
    cmpg-double v4, p1, v2

    .line 11
    .line 12
    if-gez v4, :cond_1

    .line 13
    .line 14
    const/4 p1, 0x1

    .line 15
    goto :goto_0

    .line 16
    :cond_1
    int-to-double v1, v1

    .line 17
    int-to-double v3, v0

    .line 18
    add-double/2addr v1, v3

    .line 19
    cmpl-double v0, p1, v1

    .line 20
    .line 21
    if-lez v0, :cond_2

    .line 22
    .line 23
    const/4 p1, 0x4

    .line 24
    goto :goto_0

    .line 25
    :cond_2
    const/4 p1, 0x0

    .line 26
    :goto_0
    iget p2, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 27
    .line 28
    if-gtz p2, :cond_3

    .line 29
    .line 30
    or-int/lit8 p1, p1, 0xa

    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_3
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 34
    .line 35
    int-to-double v1, v0

    .line 36
    cmpg-double v3, p3, v1

    .line 37
    .line 38
    if-gez v3, :cond_4

    .line 39
    .line 40
    or-int/lit8 p1, p1, 0x2

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_4
    int-to-double v0, v0

    .line 44
    int-to-double v2, p2

    .line 45
    add-double/2addr v0, v2

    .line 46
    cmpl-double p2, p3, v0

    .line 47
    .line 48
    if-lez p2, :cond_5

    .line 49
    .line 50
    or-int/lit8 p1, p1, 0x8

    .line 51
    .line 52
    :cond_5
    :goto_1
    return p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public reshape(IIII)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 4
    .line 5
    iput p3, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 6
    .line 7
    iput p4, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public resize(II)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setBounds(IIII)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/Rectangle;->reshape(IIII)V

    return-void
.end method

.method public setBounds(Lcom/intsig/office/java/awt/Rectangle;)V
    .locals 3

    .line 1
    iget v0, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    iget v1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    iget v2, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/intsig/office/java/awt/Rectangle;->setBounds(IIII)V

    return-void
.end method

.method public setLocation(II)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;->move(II)V

    return-void
.end method

.method public setLocation(Landroid/graphics/Point;)V
    .locals 1

    .line 1
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/java/awt/Rectangle;->setLocation(II)V

    return-void
.end method

.method public setRect(DDDD)V
    .locals 15

    .line 1
    move-wide/from16 v0, p1

    .line 2
    .line 3
    move-wide/from16 v2, p3

    .line 4
    .line 5
    const/4 v4, 0x1

    .line 6
    const/4 v5, -0x1

    .line 7
    const v6, 0x7fffffff

    .line 8
    .line 9
    .line 10
    const-wide/16 v7, 0x0

    .line 11
    .line 12
    const/4 v9, 0x0

    .line 13
    const-wide v10, 0x41efffffffc00000L    # 4.294967294E9

    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    cmpl-double v12, v0, v10

    .line 19
    .line 20
    if-lez v12, :cond_0

    .line 21
    .line 22
    const/4 v0, -0x1

    .line 23
    const v12, 0x7fffffff

    .line 24
    .line 25
    .line 26
    goto :goto_2

    .line 27
    :cond_0
    invoke-static {v0, v1, v9}, Lcom/intsig/office/java/awt/Rectangle;->clip(DZ)I

    .line 28
    .line 29
    .line 30
    move-result v12

    .line 31
    cmpl-double v13, p5, v7

    .line 32
    .line 33
    if-ltz v13, :cond_1

    .line 34
    .line 35
    int-to-double v13, v12

    .line 36
    sub-double/2addr v0, v13

    .line 37
    add-double v0, p5, v0

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    move-wide/from16 v0, p5

    .line 41
    .line 42
    :goto_0
    cmpl-double v13, v0, v7

    .line 43
    .line 44
    if-ltz v13, :cond_2

    .line 45
    .line 46
    const/4 v13, 0x1

    .line 47
    goto :goto_1

    .line 48
    :cond_2
    const/4 v13, 0x0

    .line 49
    :goto_1
    invoke-static {v0, v1, v13}, Lcom/intsig/office/java/awt/Rectangle;->clip(DZ)I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    :goto_2
    cmpl-double v1, v2, v10

    .line 54
    .line 55
    if-lez v1, :cond_3

    .line 56
    .line 57
    :goto_3
    move-object v1, p0

    .line 58
    goto :goto_6

    .line 59
    :cond_3
    invoke-static {v2, v3, v9}, Lcom/intsig/office/java/awt/Rectangle;->clip(DZ)I

    .line 60
    .line 61
    .line 62
    move-result v6

    .line 63
    cmpl-double v1, p7, v7

    .line 64
    .line 65
    if-ltz v1, :cond_4

    .line 66
    .line 67
    int-to-double v10, v6

    .line 68
    sub-double v1, v2, v10

    .line 69
    .line 70
    add-double v1, p7, v1

    .line 71
    .line 72
    goto :goto_4

    .line 73
    :cond_4
    move-wide/from16 v1, p7

    .line 74
    .line 75
    :goto_4
    cmpl-double v3, v1, v7

    .line 76
    .line 77
    if-ltz v3, :cond_5

    .line 78
    .line 79
    goto :goto_5

    .line 80
    :cond_5
    const/4 v4, 0x0

    .line 81
    :goto_5
    invoke-static {v1, v2, v4}, Lcom/intsig/office/java/awt/Rectangle;->clip(DZ)I

    .line 82
    .line 83
    .line 84
    move-result v5

    .line 85
    goto :goto_3

    .line 86
    :goto_6
    invoke-virtual {p0, v12, v6, v0, v5}, Lcom/intsig/office/java/awt/Rectangle;->reshape(IIII)V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public setSize(II)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/java/awt/Rectangle;->resize(II)V

    return-void
.end method

.method public setSize(Lcom/intsig/office/java/awt/Dimension;)V
    .locals 1

    .line 1
    iget v0, p1, Lcom/intsig/office/java/awt/Dimension;->width:I

    iget p1, p1, Lcom/intsig/office/java/awt/Dimension;->height:I

    invoke-virtual {p0, v0, p1}, Lcom/intsig/office/java/awt/Rectangle;->setSize(II)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, "[x="

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v1, ",y="

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v1, ",width="

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string v1, ",height="

    .line 48
    .line 49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    iget v1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 53
    .line 54
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    const-string v1, "]"

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public translate(II)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 2
    .line 3
    add-int v1, v0, p1

    .line 4
    .line 5
    const/high16 v2, -0x80000000

    .line 6
    .line 7
    const v3, 0x7fffffff

    .line 8
    .line 9
    .line 10
    if-gez p1, :cond_1

    .line 11
    .line 12
    if-le v1, v0, :cond_3

    .line 13
    .line 14
    iget p1, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 15
    .line 16
    if-ltz p1, :cond_0

    .line 17
    .line 18
    sub-int/2addr v1, v2

    .line 19
    add-int/2addr p1, v1

    .line 20
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 21
    .line 22
    :cond_0
    const/high16 v1, -0x80000000

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    if-ge v1, v0, :cond_3

    .line 26
    .line 27
    iget p1, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 28
    .line 29
    if-ltz p1, :cond_2

    .line 30
    .line 31
    sub-int/2addr v1, v3

    .line 32
    add-int/2addr p1, v1

    .line 33
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 34
    .line 35
    if-gez p1, :cond_2

    .line 36
    .line 37
    iput v3, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 38
    .line 39
    :cond_2
    const v1, 0x7fffffff

    .line 40
    .line 41
    .line 42
    :cond_3
    :goto_0
    iput v1, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 43
    .line 44
    iget p1, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 45
    .line 46
    add-int v0, p1, p2

    .line 47
    .line 48
    if-gez p2, :cond_4

    .line 49
    .line 50
    if-le v0, p1, :cond_6

    .line 51
    .line 52
    iget p1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 53
    .line 54
    if-ltz p1, :cond_7

    .line 55
    .line 56
    sub-int/2addr v0, v2

    .line 57
    add-int/2addr p1, v0

    .line 58
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_4
    if-ge v0, p1, :cond_6

    .line 62
    .line 63
    iget p1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 64
    .line 65
    if-ltz p1, :cond_5

    .line 66
    .line 67
    sub-int/2addr v0, v3

    .line 68
    add-int/2addr p1, v0

    .line 69
    iput p1, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 70
    .line 71
    if-gez p1, :cond_5

    .line 72
    .line 73
    iput v3, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 74
    .line 75
    :cond_5
    const v2, 0x7fffffff

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_6
    move v2, v0

    .line 80
    :cond_7
    :goto_1
    iput v2, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
.end method

.method public union(Lcom/intsig/office/java/awt/Rectangle;)Lcom/intsig/office/java/awt/Rectangle;
    .locals 13

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 2
    .line 3
    int-to-long v0, v0

    .line 4
    iget v2, p0, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 5
    .line 6
    int-to-long v2, v2

    .line 7
    or-long v4, v0, v2

    .line 8
    .line 9
    const-wide/16 v6, 0x0

    .line 10
    .line 11
    cmp-long v8, v4, v6

    .line 12
    .line 13
    if-gez v8, :cond_0

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/office/java/awt/Rectangle;

    .line 16
    .line 17
    invoke-direct {v0, p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 18
    .line 19
    .line 20
    return-object v0

    .line 21
    :cond_0
    iget v4, p1, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 22
    .line 23
    int-to-long v4, v4

    .line 24
    iget v8, p1, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 25
    .line 26
    int-to-long v8, v8

    .line 27
    or-long v10, v4, v8

    .line 28
    .line 29
    cmp-long v12, v10, v6

    .line 30
    .line 31
    if-gez v12, :cond_1

    .line 32
    .line 33
    new-instance p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 34
    .line 35
    invoke-direct {p1, p0}, Lcom/intsig/office/java/awt/Rectangle;-><init>(Lcom/intsig/office/java/awt/Rectangle;)V

    .line 36
    .line 37
    .line 38
    return-object p1

    .line 39
    :cond_1
    iget v6, p0, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 40
    .line 41
    iget v7, p0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 42
    .line 43
    int-to-long v10, v6

    .line 44
    add-long/2addr v0, v10

    .line 45
    int-to-long v10, v7

    .line 46
    add-long/2addr v2, v10

    .line 47
    iget v10, p1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 48
    .line 49
    iget p1, p1, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 50
    .line 51
    int-to-long v11, v10

    .line 52
    add-long/2addr v4, v11

    .line 53
    int-to-long v11, p1

    .line 54
    add-long/2addr v8, v11

    .line 55
    if-le v6, v10, :cond_2

    .line 56
    .line 57
    move v6, v10

    .line 58
    :cond_2
    if-le v7, p1, :cond_3

    .line 59
    .line 60
    move v7, p1

    .line 61
    :cond_3
    cmp-long p1, v0, v4

    .line 62
    .line 63
    if-gez p1, :cond_4

    .line 64
    .line 65
    move-wide v0, v4

    .line 66
    :cond_4
    cmp-long p1, v2, v8

    .line 67
    .line 68
    if-gez p1, :cond_5

    .line 69
    .line 70
    move-wide v2, v8

    .line 71
    :cond_5
    int-to-long v4, v6

    .line 72
    sub-long/2addr v0, v4

    .line 73
    int-to-long v4, v7

    .line 74
    sub-long/2addr v2, v4

    .line 75
    const-wide/32 v4, 0x7fffffff

    .line 76
    .line 77
    .line 78
    cmp-long p1, v0, v4

    .line 79
    .line 80
    if-lez p1, :cond_6

    .line 81
    .line 82
    move-wide v0, v4

    .line 83
    :cond_6
    cmp-long p1, v2, v4

    .line 84
    .line 85
    if-lez p1, :cond_7

    .line 86
    .line 87
    move-wide v2, v4

    .line 88
    :cond_7
    new-instance p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 89
    .line 90
    long-to-int v1, v0

    .line 91
    long-to-int v0, v2

    .line 92
    invoke-direct {p1, v6, v7, v1, v0}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 93
    .line 94
    .line 95
    return-object p1
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

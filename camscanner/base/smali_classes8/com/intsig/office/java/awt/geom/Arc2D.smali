.class public abstract Lcom/intsig/office/java/awt/geom/Arc2D;
.super Lcom/intsig/office/java/awt/geom/RectangularShape;
.source "Arc2D.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/java/awt/geom/Arc2D$Double;,
        Lcom/intsig/office/java/awt/geom/Arc2D$Float;
    }
.end annotation


# static fields
.field public static final CHORD:I = 0x1

.field public static final OPEN:I = 0x0

.field public static final PIE:I = 0x2


# instance fields
.field private type:I


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/office/java/awt/geom/Arc2D;-><init>(I)V

    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;-><init>()V

    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->setArcType(I)V

    return-void
.end method

.method private contains(DDDDLcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 19

    move-object/from16 v0, p0

    move-wide/from16 v2, p1

    move-wide/from16 v4, p3

    .line 18
    invoke-virtual/range {p0 .. p4}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DD)Z

    move-result v1

    const/4 v10, 0x0

    if-eqz v1, :cond_5

    add-double v6, v2, p5

    invoke-virtual {v0, v6, v7, v4, v5}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DD)Z

    move-result v1

    if-eqz v1, :cond_5

    add-double v8, v4, p7

    invoke-virtual {v0, v2, v3, v8, v9}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DD)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DD)Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 19
    :cond_0
    iget v1, v0, Lcom/intsig/office/java/awt/geom/Arc2D;->type:I

    const/4 v6, 0x2

    const/4 v11, 0x1

    if-ne v1, v6, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x4066800000000000L    # 180.0

    cmpg-double v1, v6, v8

    if-gtz v1, :cond_1

    goto/16 :goto_1

    :cond_1
    if-nez p9, :cond_2

    .line 20
    new-instance v12, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    move-object v1, v12

    move-wide/from16 v2, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    goto :goto_0

    :cond_2
    move-object/from16 v12, p9

    .line 21
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v1

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    .line 22
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v5

    div-double/2addr v5, v3

    .line 23
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v3

    add-double/2addr v3, v1

    .line 24
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v7

    add-double/2addr v7, v5

    .line 25
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    move-result-wide v13

    neg-double v13, v13

    invoke-static {v13, v14}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v13

    .line 26
    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v15

    mul-double v15, v15, v1

    add-double/2addr v15, v3

    .line 27
    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v17

    mul-double v17, v17, v5

    add-double v17, v7, v17

    move-object/from16 p1, v12

    move-wide/from16 p2, v3

    move-wide/from16 p4, v7

    move-wide/from16 p6, v15

    move-wide/from16 p8, v17

    .line 28
    invoke-virtual/range {p1 .. p9}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->intersectsLine(DDDD)Z

    move-result v9

    if-eqz v9, :cond_3

    return v10

    .line 29
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    move-result-wide v9

    neg-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v9

    add-double/2addr v13, v9

    .line 30
    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v9

    mul-double v1, v1, v9

    add-double/2addr v1, v3

    .line 31
    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    mul-double v5, v5, v9

    add-double/2addr v5, v7

    move-object/from16 p1, v12

    move-wide/from16 p2, v3

    move-wide/from16 p4, v7

    move-wide/from16 p6, v1

    move-wide/from16 p8, v5

    .line 32
    invoke-virtual/range {p1 .. p9}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->intersectsLine(DDDD)Z

    move-result v1

    xor-int/2addr v1, v11

    return v1

    :cond_4
    :goto_1
    return v11

    :cond_5
    :goto_2
    return v10
.end method

.method static normalizeDegrees(D)D
    .locals 9

    .line 1
    const-wide v0, -0x3f99800000000000L    # -180.0

    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    const-wide v2, 0x4076800000000000L    # 360.0

    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    const-wide v4, 0x4066800000000000L    # 180.0

    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    cmpl-double v6, p0, v4

    .line 17
    .line 18
    if-lez v6, :cond_1

    .line 19
    .line 20
    const-wide v6, 0x4080e00000000000L    # 540.0

    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    cmpg-double v8, p0, v6

    .line 26
    .line 27
    if-gtz v8, :cond_0

    .line 28
    .line 29
    sub-double/2addr p0, v2

    .line 30
    goto :goto_1

    .line 31
    :cond_0
    invoke-static {p0, p1, v2, v3}, Ljava/lang/Math;->IEEEremainder(DD)D

    .line 32
    .line 33
    .line 34
    move-result-wide p0

    .line 35
    cmpl-double v2, p0, v0

    .line 36
    .line 37
    if-nez v2, :cond_3

    .line 38
    .line 39
    :goto_0
    move-wide p0, v4

    .line 40
    goto :goto_1

    .line 41
    :cond_1
    cmpg-double v6, p0, v0

    .line 42
    .line 43
    if-gtz v6, :cond_3

    .line 44
    .line 45
    const-wide v6, -0x3f7f200000000000L    # -540.0

    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    cmpl-double v8, p0, v6

    .line 51
    .line 52
    if-lez v8, :cond_2

    .line 53
    .line 54
    add-double/2addr p0, v2

    .line 55
    goto :goto_1

    .line 56
    :cond_2
    invoke-static {p0, p1, v2, v3}, Ljava/lang/Math;->IEEEremainder(DD)D

    .line 57
    .line 58
    .line 59
    move-result-wide p0

    .line 60
    cmpl-double v2, p0, v0

    .line 61
    .line 62
    if-nez v2, :cond_3

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_3
    :goto_1
    return-wide p0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method


# virtual methods
.method public contains(DD)Z
    .locals 29

    move-object/from16 v0, p0

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v1

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    cmpg-double v6, v1, v4

    if-gtz v6, :cond_0

    return v3

    .line 2
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v6

    sub-double v6, p1, v6

    div-double/2addr v6, v1

    const-wide/high16 v1, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v6, v1

    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v8

    cmpg-double v10, v8, v4

    if-gtz v10, :cond_1

    return v3

    .line 4
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v4

    sub-double v4, p3, v4

    div-double/2addr v4, v8

    sub-double/2addr v4, v1

    mul-double v1, v6, v6

    mul-double v8, v4, v4

    add-double/2addr v1, v8

    const-wide/high16 v8, 0x3fd0000000000000L    # 0.25

    cmpl-double v10, v1, v8

    if-ltz v10, :cond_2

    return v3

    .line 5
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    const-wide v8, 0x4076800000000000L    # 360.0

    const/4 v10, 0x1

    cmpl-double v11, v1, v8

    if-ltz v11, :cond_3

    return v10

    .line 6
    :cond_3
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v8

    neg-double v8, v8

    invoke-virtual {v0, v8, v9}, Lcom/intsig/office/java/awt/geom/Arc2D;->containsAngle(D)Z

    move-result v8

    .line 7
    iget v9, v0, Lcom/intsig/office/java/awt/geom/Arc2D;->type:I

    const/4 v11, 0x2

    if-ne v9, v11, :cond_4

    return v8

    :cond_4
    const-wide v11, 0x4066800000000000L    # 180.0

    if-eqz v8, :cond_5

    cmpl-double v9, v1, v11

    if-ltz v9, :cond_6

    return v10

    :cond_5
    cmpg-double v9, v1, v11

    if-gtz v9, :cond_6

    return v3

    .line 8
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    move-result-wide v1

    neg-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    .line 9
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v23

    .line 10
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v25

    .line 11
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    move-result-wide v11

    neg-double v11, v11

    invoke-static {v11, v12}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v11

    add-double/2addr v1, v11

    .line 12
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v27

    .line 13
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    mul-double v19, v6, v11

    mul-double v21, v4, v11

    move-wide/from16 v11, v23

    move-wide/from16 v13, v25

    move-wide/from16 v15, v27

    move-wide/from16 v17, v1

    .line 14
    invoke-static/range {v11 .. v22}, Lcom/intsig/office/java/awt/geom/Line2D;->relativeCCW(DDDDDD)I

    move-result v4

    const-wide/16 v19, 0x0

    const-wide/16 v21, 0x0

    .line 15
    invoke-static/range {v11 .. v22}, Lcom/intsig/office/java/awt/geom/Line2D;->relativeCCW(DDDDDD)I

    move-result v1

    mul-int v4, v4, v1

    if-ltz v4, :cond_7

    const/4 v1, 0x1

    goto :goto_0

    :cond_7
    const/4 v1, 0x0

    :goto_0
    if-eqz v8, :cond_8

    if-nez v1, :cond_9

    const/4 v3, 0x1

    goto :goto_1

    :cond_8
    move v3, v1

    :cond_9
    :goto_1
    return v3
.end method

.method public contains(DDDD)Z
    .locals 10

    const/4 v9, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    move-wide/from16 v7, p7

    .line 16
    invoke-direct/range {v0 .. v9}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DDDDLcom/intsig/office/java/awt/geom/Rectangle2D;)Z

    move-result v0

    return v0
.end method

.method public contains(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
    .locals 10

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DDDDLcom/intsig/office/java/awt/geom/Rectangle2D;)Z

    move-result p1

    return p1
.end method

.method public containsAngle(D)Z
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x1

    .line 7
    const-wide/16 v4, 0x0

    .line 8
    .line 9
    cmpg-double v6, v0, v4

    .line 10
    .line 11
    if-gez v6, :cond_0

    .line 12
    .line 13
    const/4 v6, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v6, 0x0

    .line 16
    :goto_0
    if-eqz v6, :cond_1

    .line 17
    .line 18
    neg-double v0, v0

    .line 19
    :cond_1
    const-wide v7, 0x4076800000000000L    # 360.0

    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    cmpl-double v9, v0, v7

    .line 25
    .line 26
    if-ltz v9, :cond_2

    .line 27
    .line 28
    return v3

    .line 29
    :cond_2
    invoke-static {p1, p2}, Lcom/intsig/office/java/awt/geom/Arc2D;->normalizeDegrees(D)D

    .line 30
    .line 31
    .line 32
    move-result-wide p1

    .line 33
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 34
    .line 35
    .line 36
    move-result-wide v9

    .line 37
    invoke-static {v9, v10}, Lcom/intsig/office/java/awt/geom/Arc2D;->normalizeDegrees(D)D

    .line 38
    .line 39
    .line 40
    move-result-wide v9

    .line 41
    sub-double/2addr p1, v9

    .line 42
    if-eqz v6, :cond_3

    .line 43
    .line 44
    neg-double p1, p1

    .line 45
    :cond_3
    cmpg-double v6, p1, v4

    .line 46
    .line 47
    if-gez v6, :cond_4

    .line 48
    .line 49
    add-double/2addr p1, v7

    .line 50
    :cond_4
    cmpl-double v6, p1, v4

    .line 51
    .line 52
    if-ltz v6, :cond_5

    .line 53
    .line 54
    cmpg-double v4, p1, v0

    .line 55
    .line 56
    if-gez v4, :cond_5

    .line 57
    .line 58
    const/4 v2, 0x1

    .line 59
    :cond_5
    return v2
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p1, p0, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/office/java/awt/geom/Arc2D;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_2

    .line 9
    .line 10
    check-cast p1, Lcom/intsig/office/java/awt/geom/Arc2D;

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 13
    .line 14
    .line 15
    move-result-wide v3

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 17
    .line 18
    .line 19
    move-result-wide v5

    .line 20
    cmpl-double v1, v3, v5

    .line 21
    .line 22
    if-nez v1, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 25
    .line 26
    .line 27
    move-result-wide v3

    .line 28
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 29
    .line 30
    .line 31
    move-result-wide v5

    .line 32
    cmpl-double v1, v3, v5

    .line 33
    .line 34
    if-nez v1, :cond_1

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 37
    .line 38
    .line 39
    move-result-wide v3

    .line 40
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 41
    .line 42
    .line 43
    move-result-wide v5

    .line 44
    cmpl-double v1, v3, v5

    .line 45
    .line 46
    if-nez v1, :cond_1

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 49
    .line 50
    .line 51
    move-result-wide v3

    .line 52
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 53
    .line 54
    .line 55
    move-result-wide v5

    .line 56
    cmpl-double v1, v3, v5

    .line 57
    .line 58
    if-nez v1, :cond_1

    .line 59
    .line 60
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 61
    .line 62
    .line 63
    move-result-wide v3

    .line 64
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 65
    .line 66
    .line 67
    move-result-wide v5

    .line 68
    cmpl-double v1, v3, v5

    .line 69
    .line 70
    if-nez v1, :cond_1

    .line 71
    .line 72
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 73
    .line 74
    .line 75
    move-result-wide v3

    .line 76
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 77
    .line 78
    .line 79
    move-result-wide v5

    .line 80
    cmpl-double v1, v3, v5

    .line 81
    .line 82
    if-nez v1, :cond_1

    .line 83
    .line 84
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getArcType()I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->getArcType()I

    .line 89
    .line 90
    .line 91
    move-result p1

    .line 92
    if-ne v1, p1, :cond_1

    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_1
    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    .line 97
    :cond_2
    return v2
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public abstract getAngleExtent()D
.end method

.method public abstract getAngleStart()D
.end method

.method public getArcType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/Arc2D;->type:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 20

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->isEmpty()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 12
    .line 13
    .line 14
    move-result-wide v4

    .line 15
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 16
    .line 17
    .line 18
    move-result-wide v6

    .line 19
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 20
    .line 21
    .line 22
    move-result-wide v8

    .line 23
    move-object/from16 v1, p0

    .line 24
    .line 25
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/office/java/awt/geom/Arc2D;->makeBounds(DDDD)Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    return-object v0

    .line 30
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getArcType()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    const/4 v1, 0x2

    .line 35
    const-wide/16 v2, 0x0

    .line 36
    .line 37
    if-ne v0, v1, :cond_1

    .line 38
    .line 39
    move-wide v0, v2

    .line 40
    move-wide v4, v0

    .line 41
    goto :goto_0

    .line 42
    :cond_1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 43
    .line 44
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    .line 45
    .line 46
    :goto_0
    const/4 v6, 0x0

    .line 47
    move-wide v6, v4

    .line 48
    move-wide v8, v6

    .line 49
    const/4 v10, 0x0

    .line 50
    move-wide v4, v2

    .line 51
    move-wide v2, v0

    .line 52
    :goto_1
    const/4 v11, 0x6

    .line 53
    if-ge v10, v11, :cond_5

    .line 54
    .line 55
    const/4 v11, 0x4

    .line 56
    if-ge v10, v11, :cond_2

    .line 57
    .line 58
    const-wide v11, 0x4056800000000000L    # 90.0

    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    add-double/2addr v4, v11

    .line 64
    move-object/from16 v14, p0

    .line 65
    .line 66
    invoke-virtual {v14, v4, v5}, Lcom/intsig/office/java/awt/geom/Arc2D;->containsAngle(D)Z

    .line 67
    .line 68
    .line 69
    move-result v11

    .line 70
    if-nez v11, :cond_4

    .line 71
    .line 72
    goto :goto_3

    .line 73
    :cond_2
    move-object/from16 v14, p0

    .line 74
    .line 75
    if-ne v10, v11, :cond_3

    .line 76
    .line 77
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 78
    .line 79
    .line 80
    move-result-wide v4

    .line 81
    goto :goto_2

    .line 82
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 83
    .line 84
    .line 85
    move-result-wide v11

    .line 86
    add-double/2addr v4, v11

    .line 87
    :cond_4
    :goto_2
    neg-double v11, v4

    .line 88
    invoke-static {v11, v12}, Ljava/lang/Math;->toRadians(D)D

    .line 89
    .line 90
    .line 91
    move-result-wide v11

    .line 92
    move-wide v15, v4

    .line 93
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    .line 94
    .line 95
    .line 96
    move-result-wide v4

    .line 97
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    .line 98
    .line 99
    .line 100
    move-result-wide v11

    .line 101
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(DD)D

    .line 102
    .line 103
    .line 104
    move-result-wide v0

    .line 105
    invoke-static {v2, v3, v11, v12}, Ljava/lang/Math;->min(DD)D

    .line 106
    .line 107
    .line 108
    move-result-wide v2

    .line 109
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(DD)D

    .line 110
    .line 111
    .line 112
    move-result-wide v4

    .line 113
    invoke-static {v8, v9, v11, v12}, Ljava/lang/Math;->max(DD)D

    .line 114
    .line 115
    .line 116
    move-result-wide v6

    .line 117
    move-wide v8, v6

    .line 118
    move-wide v6, v4

    .line 119
    move-wide v4, v15

    .line 120
    :goto_3
    add-int/lit8 v10, v10, 0x1

    .line 121
    .line 122
    goto :goto_1

    .line 123
    :cond_5
    move-object/from16 v14, p0

    .line 124
    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 126
    .line 127
    .line 128
    move-result-wide v4

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 130
    .line 131
    .line 132
    move-result-wide v10

    .line 133
    sub-double/2addr v6, v0

    .line 134
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    .line 135
    .line 136
    mul-double v6, v6, v12

    .line 137
    .line 138
    mul-double v16, v6, v4

    .line 139
    .line 140
    sub-double/2addr v8, v2

    .line 141
    mul-double v8, v8, v12

    .line 142
    .line 143
    mul-double v18, v8, v10

    .line 144
    .line 145
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 146
    .line 147
    .line 148
    move-result-wide v6

    .line 149
    mul-double v0, v0, v12

    .line 150
    .line 151
    add-double/2addr v0, v12

    .line 152
    mul-double v0, v0, v4

    .line 153
    .line 154
    add-double/2addr v0, v6

    .line 155
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 156
    .line 157
    .line 158
    move-result-wide v4

    .line 159
    mul-double v2, v2, v12

    .line 160
    .line 161
    add-double/2addr v2, v12

    .line 162
    mul-double v2, v2, v10

    .line 163
    .line 164
    add-double/2addr v2, v4

    .line 165
    move-object/from16 v11, p0

    .line 166
    .line 167
    move-wide v12, v0

    .line 168
    move-wide v14, v2

    .line 169
    invoke-virtual/range {v11 .. v19}, Lcom/intsig/office/java/awt/geom/Arc2D;->makeBounds(DDDD)Lcom/intsig/office/java/awt/geom/Rectangle2D;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    return-object v0
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method public getEndPoint()Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    neg-double v0, v0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 7
    .line 8
    .line 9
    move-result-wide v2

    .line 10
    sub-double/2addr v0, v2

    .line 11
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 16
    .line 17
    .line 18
    move-result-wide v2

    .line 19
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    .line 20
    .line 21
    .line 22
    move-result-wide v4

    .line 23
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 24
    .line 25
    mul-double v4, v4, v6

    .line 26
    .line 27
    add-double/2addr v4, v6

    .line 28
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 29
    .line 30
    .line 31
    move-result-wide v8

    .line 32
    mul-double v4, v4, v8

    .line 33
    .line 34
    add-double/2addr v2, v4

    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 36
    .line 37
    .line 38
    move-result-wide v4

    .line 39
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    .line 40
    .line 41
    .line 42
    move-result-wide v0

    .line 43
    mul-double v0, v0, v6

    .line 44
    .line 45
    add-double/2addr v0, v6

    .line 46
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 47
    .line 48
    .line 49
    move-result-wide v6

    .line 50
    mul-double v0, v0, v6

    .line 51
    .line 52
    add-double/2addr v4, v0

    .line 53
    new-instance v0, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 54
    .line 55
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>(DD)V

    .line 56
    .line 57
    .line 58
    return-object v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/ArcIterator;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/office/java/awt/geom/ArcIterator;-><init>(Lcom/intsig/office/java/awt/geom/Arc2D;Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getStartPoint()Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    neg-double v0, v0

    .line 6
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 11
    .line 12
    .line 13
    move-result-wide v2

    .line 14
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    .line 15
    .line 16
    .line 17
    move-result-wide v4

    .line 18
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 19
    .line 20
    mul-double v4, v4, v6

    .line 21
    .line 22
    add-double/2addr v4, v6

    .line 23
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 24
    .line 25
    .line 26
    move-result-wide v8

    .line 27
    mul-double v4, v4, v8

    .line 28
    .line 29
    add-double/2addr v2, v4

    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 31
    .line 32
    .line 33
    move-result-wide v4

    .line 34
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    .line 35
    .line 36
    .line 37
    move-result-wide v0

    .line 38
    mul-double v0, v0, v6

    .line 39
    .line 40
    add-double/2addr v0, v6

    .line 41
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 42
    .line 43
    .line 44
    move-result-wide v6

    .line 45
    mul-double v0, v0, v6

    .line 46
    .line 47
    add-double/2addr v4, v0

    .line 48
    new-instance v0, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    .line 49
    .line 50
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>(DD)V

    .line 51
    .line 52
    .line 53
    return-object v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public hashCode()I
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 10
    .line 11
    .line 12
    move-result-wide v2

    .line 13
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 14
    .line 15
    .line 16
    move-result-wide v2

    .line 17
    const-wide/16 v4, 0x25

    .line 18
    .line 19
    mul-long v2, v2, v4

    .line 20
    .line 21
    add-long/2addr v0, v2

    .line 22
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 23
    .line 24
    .line 25
    move-result-wide v2

    .line 26
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 27
    .line 28
    .line 29
    move-result-wide v2

    .line 30
    const-wide/16 v4, 0x2b

    .line 31
    .line 32
    mul-long v2, v2, v4

    .line 33
    .line 34
    add-long/2addr v0, v2

    .line 35
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 36
    .line 37
    .line 38
    move-result-wide v2

    .line 39
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 40
    .line 41
    .line 42
    move-result-wide v2

    .line 43
    const-wide/16 v4, 0x2f

    .line 44
    .line 45
    mul-long v2, v2, v4

    .line 46
    .line 47
    add-long/2addr v0, v2

    .line 48
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 49
    .line 50
    .line 51
    move-result-wide v2

    .line 52
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 53
    .line 54
    .line 55
    move-result-wide v2

    .line 56
    const-wide/16 v4, 0x35

    .line 57
    .line 58
    mul-long v2, v2, v4

    .line 59
    .line 60
    add-long/2addr v0, v2

    .line 61
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 62
    .line 63
    .line 64
    move-result-wide v2

    .line 65
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 66
    .line 67
    .line 68
    move-result-wide v2

    .line 69
    const-wide/16 v4, 0x3b

    .line 70
    .line 71
    mul-long v2, v2, v4

    .line 72
    .line 73
    add-long/2addr v0, v2

    .line 74
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getArcType()I

    .line 75
    .line 76
    .line 77
    move-result v2

    .line 78
    mul-int/lit8 v2, v2, 0x3d

    .line 79
    .line 80
    int-to-long v2, v2

    .line 81
    add-long/2addr v0, v2

    .line 82
    long-to-int v2, v0

    .line 83
    const/16 v3, 0x20

    .line 84
    .line 85
    shr-long/2addr v0, v3

    .line 86
    long-to-int v1, v0

    .line 87
    xor-int v0, v2, v1

    .line 88
    .line 89
    return v0
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public intersects(DDDD)Z
    .locals 42

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-wide/from16 v10, p1

    .line 4
    .line 5
    move-wide/from16 v12, p3

    .line 6
    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 8
    .line 9
    .line 10
    move-result-wide v1

    .line 11
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 12
    .line 13
    .line 14
    move-result-wide v3

    .line 15
    const/4 v14, 0x0

    .line 16
    const-wide/16 v5, 0x0

    .line 17
    .line 18
    cmpg-double v7, p5, v5

    .line 19
    .line 20
    if-lez v7, :cond_f

    .line 21
    .line 22
    cmpg-double v7, p7, v5

    .line 23
    .line 24
    if-lez v7, :cond_f

    .line 25
    .line 26
    cmpg-double v7, v1, v5

    .line 27
    .line 28
    if-lez v7, :cond_f

    .line 29
    .line 30
    cmpg-double v7, v3, v5

    .line 31
    .line 32
    if-gtz v7, :cond_0

    .line 33
    .line 34
    goto/16 :goto_3

    .line 35
    .line 36
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 37
    .line 38
    .line 39
    move-result-wide v15

    .line 40
    cmpl-double v7, v15, v5

    .line 41
    .line 42
    if-nez v7, :cond_1

    .line 43
    .line 44
    return v14

    .line 45
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 46
    .line 47
    .line 48
    move-result-wide v7

    .line 49
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 50
    .line 51
    .line 52
    move-result-wide v17

    .line 53
    add-double/2addr v1, v7

    .line 54
    add-double v3, v17, v3

    .line 55
    .line 56
    move-wide/from16 v19, v15

    .line 57
    .line 58
    add-double v14, v10, p5

    .line 59
    .line 60
    add-double v5, v12, p7

    .line 61
    .line 62
    cmpl-double v9, v10, v1

    .line 63
    .line 64
    if-gez v9, :cond_e

    .line 65
    .line 66
    cmpl-double v9, v12, v3

    .line 67
    .line 68
    if-gez v9, :cond_e

    .line 69
    .line 70
    cmpg-double v9, v14, v7

    .line 71
    .line 72
    if-lez v9, :cond_e

    .line 73
    .line 74
    cmpg-double v9, v5, v17

    .line 75
    .line 76
    if-gtz v9, :cond_2

    .line 77
    .line 78
    goto/16 :goto_2

    .line 79
    .line 80
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterX()D

    .line 81
    .line 82
    .line 83
    move-result-wide v32

    .line 84
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterY()D

    .line 85
    .line 86
    .line 87
    move-result-wide v34

    .line 88
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getStartPoint()Lcom/intsig/office/java/awt/geom/Point2D;

    .line 89
    .line 90
    .line 91
    move-result-object v9

    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getEndPoint()Lcom/intsig/office/java/awt/geom/Point2D;

    .line 93
    .line 94
    .line 95
    move-result-object v16

    .line 96
    invoke-virtual {v9}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 97
    .line 98
    .line 99
    move-result-wide v28

    .line 100
    invoke-virtual {v9}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 101
    .line 102
    .line 103
    move-result-wide v30

    .line 104
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 105
    .line 106
    .line 107
    move-result-wide v36

    .line 108
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 109
    .line 110
    .line 111
    move-result-wide v38

    .line 112
    move-wide/from16 v23, v3

    .line 113
    .line 114
    const-wide v3, 0x4066800000000000L    # 180.0

    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    const/16 v16, 0x1

    .line 120
    .line 121
    cmpl-double v9, v34, v12

    .line 122
    .line 123
    if-ltz v9, :cond_5

    .line 124
    .line 125
    cmpg-double v25, v34, v5

    .line 126
    .line 127
    if-gtz v25, :cond_5

    .line 128
    .line 129
    cmpg-double v25, v28, v14

    .line 130
    .line 131
    if-gez v25, :cond_3

    .line 132
    .line 133
    cmpg-double v25, v36, v14

    .line 134
    .line 135
    if-gez v25, :cond_3

    .line 136
    .line 137
    cmpg-double v25, v32, v14

    .line 138
    .line 139
    if-gez v25, :cond_3

    .line 140
    .line 141
    cmpl-double v25, v1, v10

    .line 142
    .line 143
    if-lez v25, :cond_3

    .line 144
    .line 145
    const-wide/16 v1, 0x0

    .line 146
    .line 147
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Arc2D;->containsAngle(D)Z

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    if-nez v1, :cond_4

    .line 152
    .line 153
    :cond_3
    cmpl-double v1, v28, v10

    .line 154
    .line 155
    if-lez v1, :cond_5

    .line 156
    .line 157
    cmpl-double v1, v36, v10

    .line 158
    .line 159
    if-lez v1, :cond_5

    .line 160
    .line 161
    cmpl-double v1, v32, v10

    .line 162
    .line 163
    if-lez v1, :cond_5

    .line 164
    .line 165
    cmpg-double v1, v7, v14

    .line 166
    .line 167
    if-gez v1, :cond_5

    .line 168
    .line 169
    invoke-virtual {v0, v3, v4}, Lcom/intsig/office/java/awt/geom/Arc2D;->containsAngle(D)Z

    .line 170
    .line 171
    .line 172
    move-result v1

    .line 173
    if-eqz v1, :cond_5

    .line 174
    .line 175
    :cond_4
    return v16

    .line 176
    :cond_5
    cmpl-double v1, v32, v10

    .line 177
    .line 178
    if-ltz v1, :cond_8

    .line 179
    .line 180
    cmpg-double v1, v32, v14

    .line 181
    .line 182
    if-gtz v1, :cond_8

    .line 183
    .line 184
    cmpl-double v1, v30, v12

    .line 185
    .line 186
    if-lez v1, :cond_6

    .line 187
    .line 188
    cmpl-double v1, v38, v12

    .line 189
    .line 190
    if-lez v1, :cond_6

    .line 191
    .line 192
    if-lez v9, :cond_6

    .line 193
    .line 194
    cmpg-double v1, v17, v5

    .line 195
    .line 196
    if-gez v1, :cond_6

    .line 197
    .line 198
    const-wide v1, 0x4056800000000000L    # 90.0

    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Arc2D;->containsAngle(D)Z

    .line 204
    .line 205
    .line 206
    move-result v1

    .line 207
    if-nez v1, :cond_7

    .line 208
    .line 209
    :cond_6
    cmpg-double v1, v30, v5

    .line 210
    .line 211
    if-gez v1, :cond_8

    .line 212
    .line 213
    cmpg-double v1, v38, v5

    .line 214
    .line 215
    if-gez v1, :cond_8

    .line 216
    .line 217
    cmpg-double v1, v34, v5

    .line 218
    .line 219
    if-gez v1, :cond_8

    .line 220
    .line 221
    cmpl-double v1, v23, v12

    .line 222
    .line 223
    if-lez v1, :cond_8

    .line 224
    .line 225
    const-wide v1, 0x4070e00000000000L    # 270.0

    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    invoke-virtual {v0, v1, v2}, Lcom/intsig/office/java/awt/geom/Arc2D;->containsAngle(D)Z

    .line 231
    .line 232
    .line 233
    move-result v1

    .line 234
    if-eqz v1, :cond_8

    .line 235
    .line 236
    :cond_7
    return v16

    .line 237
    :cond_8
    new-instance v17, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    .line 238
    .line 239
    move-object/from16 v1, v17

    .line 240
    .line 241
    move-wide/from16 v21, v3

    .line 242
    .line 243
    move-wide/from16 v2, p1

    .line 244
    .line 245
    move-wide v8, v5

    .line 246
    move-wide/from16 v4, p3

    .line 247
    .line 248
    move-wide/from16 v6, p5

    .line 249
    .line 250
    move-wide/from16 v40, v8

    .line 251
    .line 252
    move-wide/from16 v8, p7

    .line 253
    .line 254
    invoke-direct/range {v1 .. v9}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    .line 255
    .line 256
    .line 257
    iget v1, v0, Lcom/intsig/office/java/awt/geom/Arc2D;->type:I

    .line 258
    .line 259
    const/4 v2, 0x2

    .line 260
    if-eq v1, v2, :cond_a

    .line 261
    .line 262
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->abs(D)D

    .line 263
    .line 264
    .line 265
    move-result-wide v1

    .line 266
    cmpl-double v3, v1, v21

    .line 267
    .line 268
    if-lez v3, :cond_9

    .line 269
    .line 270
    goto :goto_0

    .line 271
    :cond_9
    move-object/from16 v23, v17

    .line 272
    .line 273
    move-wide/from16 v24, v28

    .line 274
    .line 275
    move-wide/from16 v26, v30

    .line 276
    .line 277
    move-wide/from16 v28, v36

    .line 278
    .line 279
    move-wide/from16 v30, v38

    .line 280
    .line 281
    invoke-virtual/range {v23 .. v31}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->intersectsLine(DDDD)Z

    .line 282
    .line 283
    .line 284
    move-result v1

    .line 285
    if-eqz v1, :cond_b

    .line 286
    .line 287
    return v16

    .line 288
    :cond_a
    :goto_0
    move-object/from16 v23, v17

    .line 289
    .line 290
    move-wide/from16 v24, v32

    .line 291
    .line 292
    move-wide/from16 v26, v34

    .line 293
    .line 294
    invoke-virtual/range {v23 .. v31}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->intersectsLine(DDDD)Z

    .line 295
    .line 296
    .line 297
    move-result v1

    .line 298
    if-nez v1, :cond_d

    .line 299
    .line 300
    move-object/from16 v23, v17

    .line 301
    .line 302
    move-wide/from16 v24, v32

    .line 303
    .line 304
    move-wide/from16 v26, v34

    .line 305
    .line 306
    move-wide/from16 v28, v36

    .line 307
    .line 308
    move-wide/from16 v30, v38

    .line 309
    .line 310
    invoke-virtual/range {v23 .. v31}, Lcom/intsig/office/java/awt/geom/Rectangle2D;->intersectsLine(DDDD)Z

    .line 311
    .line 312
    .line 313
    move-result v1

    .line 314
    if-eqz v1, :cond_b

    .line 315
    .line 316
    goto :goto_1

    .line 317
    :cond_b
    invoke-virtual/range {p0 .. p4}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DD)Z

    .line 318
    .line 319
    .line 320
    move-result v1

    .line 321
    if-nez v1, :cond_d

    .line 322
    .line 323
    invoke-virtual {v0, v14, v15, v12, v13}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DD)Z

    .line 324
    .line 325
    .line 326
    move-result v1

    .line 327
    if-nez v1, :cond_d

    .line 328
    .line 329
    move-wide/from16 v1, v40

    .line 330
    .line 331
    invoke-virtual {v0, v10, v11, v1, v2}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DD)Z

    .line 332
    .line 333
    .line 334
    move-result v3

    .line 335
    if-nez v3, :cond_d

    .line 336
    .line 337
    invoke-virtual {v0, v14, v15, v1, v2}, Lcom/intsig/office/java/awt/geom/Arc2D;->contains(DD)Z

    .line 338
    .line 339
    .line 340
    move-result v1

    .line 341
    if-eqz v1, :cond_c

    .line 342
    .line 343
    goto :goto_1

    .line 344
    :cond_c
    const/4 v1, 0x0

    .line 345
    return v1

    .line 346
    :cond_d
    :goto_1
    return v16

    .line 347
    :cond_e
    :goto_2
    const/4 v1, 0x0

    .line 348
    return v1

    .line 349
    :cond_f
    :goto_3
    const/4 v1, 0x0

    .line 350
    return v1
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method protected abstract makeBounds(DDDD)Lcom/intsig/office/java/awt/geom/Rectangle2D;
.end method

.method public abstract setAngleExtent(D)V
.end method

.method public abstract setAngleStart(D)V
.end method

.method public setAngleStart(Lcom/intsig/office/java/awt/geom/Point2D;)V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterX()D

    .line 10
    .line 11
    .line 12
    move-result-wide v4

    .line 13
    sub-double/2addr v2, v4

    .line 14
    mul-double v0, v0, v2

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 17
    .line 18
    .line 19
    move-result-wide v2

    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterY()D

    .line 25
    .line 26
    .line 27
    move-result-wide v6

    .line 28
    sub-double/2addr v4, v6

    .line 29
    mul-double v2, v2, v4

    .line 30
    .line 31
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    .line 32
    .line 33
    .line 34
    move-result-wide v0

    .line 35
    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    .line 36
    .line 37
    .line 38
    move-result-wide v0

    .line 39
    neg-double v0, v0

    .line 40
    invoke-virtual {p0, v0, v1}, Lcom/intsig/office/java/awt/geom/Arc2D;->setAngleStart(D)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public setAngles(DDDD)V
    .locals 13

    move-object v0, p0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterX()D

    move-result-wide v1

    .line 2
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getCenterY()D

    move-result-wide v3

    .line 3
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    sub-double v9, v3, p3

    mul-double v9, v9, v5

    sub-double v11, p1, v1

    mul-double v11, v11, v7

    .line 5
    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v9

    sub-double v3, v3, p7

    mul-double v5, v5, v3

    sub-double v1, p5, v1

    mul-double v7, v7, v1

    .line 6
    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v1

    sub-double/2addr v1, v9

    const-wide/16 v3, 0x0

    cmpg-double v5, v1, v3

    if-gtz v5, :cond_0

    const-wide v3, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v1, v3

    .line 7
    :cond_0
    invoke-static {v9, v10}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/intsig/office/java/awt/geom/Arc2D;->setAngleStart(D)V

    .line 8
    invoke-static {v1, v2}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/intsig/office/java/awt/geom/Arc2D;->setAngleExtent(D)V

    return-void
.end method

.method public setAngles(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;)V
    .locals 9

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v3

    invoke-virtual {p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v5

    invoke-virtual {p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Arc2D;->setAngles(DDDD)V

    return-void
.end method

.method public abstract setArc(DDDDDDI)V
.end method

.method public setArc(Lcom/intsig/office/java/awt/geom/Arc2D;)V
    .locals 14

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    move-result-wide v9

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    move-result-wide v11

    iget v13, p1, Lcom/intsig/office/java/awt/geom/Arc2D;->type:I

    move-object v0, p0

    .line 5
    invoke-virtual/range {v0 .. v13}, Lcom/intsig/office/java/awt/geom/Arc2D;->setArc(DDDDDDI)V

    return-void
.end method

.method public setArc(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Dimension2D;DDI)V
    .locals 14

    .line 1
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v3

    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/Dimension2D;->getWidth()D

    move-result-wide v5

    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/Dimension2D;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    move-wide/from16 v9, p3

    move-wide/from16 v11, p5

    move/from16 v13, p7

    invoke-virtual/range {v0 .. v13}, Lcom/intsig/office/java/awt/geom/Arc2D;->setArc(DDDDDDI)V

    return-void
.end method

.method public setArc(Lcom/intsig/office/java/awt/geom/Rectangle2D;DDI)V
    .locals 14

    .line 2
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v7

    move-object v0, p0

    move-wide/from16 v9, p2

    move-wide/from16 v11, p4

    move/from16 v13, p6

    invoke-virtual/range {v0 .. v13}, Lcom/intsig/office/java/awt/geom/Arc2D;->setArc(DDDDDDI)V

    return-void
.end method

.method public setArcByCenter(DDDDDI)V
    .locals 14

    .line 1
    sub-double v1, p1, p5

    .line 2
    .line 3
    sub-double v3, p3, p5

    .line 4
    .line 5
    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    .line 6
    .line 7
    mul-double v7, p5, v5

    .line 8
    .line 9
    move-object v0, p0

    .line 10
    move-wide v5, v7

    .line 11
    move-wide/from16 v9, p7

    .line 12
    .line 13
    move-wide/from16 v11, p9

    .line 14
    .line 15
    move/from16 v13, p11

    .line 16
    .line 17
    invoke-virtual/range {v0 .. v13}, Lcom/intsig/office/java/awt/geom/Arc2D;->setArc(DDDDDDI)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
.end method

.method public setArcByTangent(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;D)V
    .locals 20

    .line 1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    sub-double/2addr v0, v2

    .line 10
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 11
    .line 12
    .line 13
    move-result-wide v2

    .line 14
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 15
    .line 16
    .line 17
    move-result-wide v4

    .line 18
    sub-double/2addr v2, v4

    .line 19
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    .line 20
    .line 21
    .line 22
    move-result-wide v0

    .line 23
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 24
    .line 25
    .line 26
    move-result-wide v2

    .line 27
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 28
    .line 29
    .line 30
    move-result-wide v4

    .line 31
    sub-double/2addr v2, v4

    .line 32
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 33
    .line 34
    .line 35
    move-result-wide v4

    .line 36
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 37
    .line 38
    .line 39
    move-result-wide v6

    .line 40
    sub-double/2addr v4, v6

    .line 41
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    .line 42
    .line 43
    .line 44
    move-result-wide v2

    .line 45
    sub-double v4, v2, v0

    .line 46
    .line 47
    const-wide v6, 0x400921fb54442d18L    # Math.PI

    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    const-wide v8, 0x401921fb54442d18L    # 6.283185307179586

    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    cmpl-double v10, v4, v6

    .line 58
    .line 59
    if-lez v10, :cond_0

    .line 60
    .line 61
    sub-double/2addr v2, v8

    .line 62
    goto :goto_0

    .line 63
    :cond_0
    const-wide v6, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    cmpg-double v10, v4, v6

    .line 69
    .line 70
    if-gez v10, :cond_1

    .line 71
    .line 72
    add-double/2addr v2, v8

    .line 73
    :cond_1
    :goto_0
    add-double v4, v0, v2

    .line 74
    .line 75
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 76
    .line 77
    div-double/2addr v4, v6

    .line 78
    sub-double v6, v2, v4

    .line 79
    .line 80
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    .line 81
    .line 82
    .line 83
    move-result-wide v6

    .line 84
    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    .line 85
    .line 86
    .line 87
    move-result-wide v6

    .line 88
    div-double v6, p4, v6

    .line 89
    .line 90
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    .line 91
    .line 92
    .line 93
    move-result-wide v8

    .line 94
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    .line 95
    .line 96
    .line 97
    move-result-wide v10

    .line 98
    mul-double v10, v10, v6

    .line 99
    .line 100
    add-double v9, v8, v10

    .line 101
    .line 102
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    .line 103
    .line 104
    .line 105
    move-result-wide v11

    .line 106
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    .line 107
    .line 108
    .line 109
    move-result-wide v4

    .line 110
    mul-double v6, v6, v4

    .line 111
    .line 112
    add-double/2addr v11, v6

    .line 113
    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    cmpg-double v6, v0, v2

    .line 119
    .line 120
    if-gez v6, :cond_2

    .line 121
    .line 122
    sub-double/2addr v0, v4

    .line 123
    add-double/2addr v2, v4

    .line 124
    goto :goto_1

    .line 125
    :cond_2
    add-double/2addr v0, v4

    .line 126
    sub-double/2addr v2, v4

    .line 127
    :goto_1
    neg-double v0, v0

    .line 128
    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    .line 129
    .line 130
    .line 131
    move-result-wide v15

    .line 132
    neg-double v0, v2

    .line 133
    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    .line 134
    .line 135
    .line 136
    move-result-wide v0

    .line 137
    sub-double/2addr v0, v15

    .line 138
    const-wide/16 v2, 0x0

    .line 139
    .line 140
    const-wide v4, 0x4076800000000000L    # 360.0

    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    cmpg-double v6, v0, v2

    .line 146
    .line 147
    if-gez v6, :cond_3

    .line 148
    .line 149
    add-double/2addr v0, v4

    .line 150
    goto :goto_2

    .line 151
    :cond_3
    sub-double/2addr v0, v4

    .line 152
    :goto_2
    move-wide/from16 v17, v0

    .line 153
    .line 154
    move-object/from16 v0, p0

    .line 155
    .line 156
    iget v1, v0, Lcom/intsig/office/java/awt/geom/Arc2D;->type:I

    .line 157
    .line 158
    move-object/from16 v8, p0

    .line 159
    .line 160
    move-wide/from16 v13, p4

    .line 161
    .line 162
    move/from16 v19, v1

    .line 163
    .line 164
    invoke-virtual/range {v8 .. v19}, Lcom/intsig/office/java/awt/geom/Arc2D;->setArcByCenter(DDDDDI)V

    .line 165
    .line 166
    .line 167
    return-void
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public setArcType(I)V
    .locals 3

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x2

    .line 4
    if-gt p1, v0, :cond_0

    .line 5
    .line 6
    iput p1, p0, Lcom/intsig/office/java/awt/geom/Arc2D;->type:I

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 10
    .line 11
    new-instance v1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v2, "invalid type for Arc: "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    throw v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setFrame(DDDD)V
    .locals 15

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 2
    .line 3
    .line 4
    move-result-wide v9

    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 6
    .line 7
    .line 8
    move-result-wide v11

    .line 9
    move-object v14, p0

    .line 10
    iget v13, v14, Lcom/intsig/office/java/awt/geom/Arc2D;->type:I

    .line 11
    .line 12
    move-object v0, p0

    .line 13
    move-wide/from16 v1, p1

    .line 14
    .line 15
    move-wide/from16 v3, p3

    .line 16
    .line 17
    move-wide/from16 v5, p5

    .line 18
    .line 19
    move-wide/from16 v7, p7

    .line 20
    .line 21
    invoke-virtual/range {v0 .. v13}, Lcom/intsig/office/java/awt/geom/Arc2D;->setArc(DDDDDDI)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

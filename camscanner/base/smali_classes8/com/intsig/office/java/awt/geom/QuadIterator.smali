.class Lcom/intsig/office/java/awt/geom/QuadIterator;
.super Ljava/lang/Object;
.source "QuadIterator.java"

# interfaces
.implements Lcom/intsig/office/java/awt/geom/PathIterator;


# instance fields
.field 〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

.field 〇o00〇〇Oo:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field 〇o〇:I


# direct methods
.method constructor <init>(Lcom/intsig/office/java/awt/geom/QuadCurve2D;Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o00〇〇Oo:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 10

    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/QuadIterator;->isDone()Z

    move-result v0

    if-nez v0, :cond_3

    .line 13
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o〇:I

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_0

    .line 14
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getX1()D

    move-result-wide v4

    aput-wide v4, p1, v3

    .line 15
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getY1()D

    move-result-wide v4

    aput-wide v4, p1, v2

    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getCtrlX()D

    move-result-wide v4

    aput-wide v4, p1, v3

    .line 17
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getCtrlY()D

    move-result-wide v3

    aput-wide v3, p1, v2

    .line 18
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getX2()D

    move-result-wide v3

    aput-wide v3, p1, v1

    .line 19
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getY2()D

    move-result-wide v3

    const/4 v0, 0x3

    aput-wide v3, p1, v0

    const/4 v3, 0x2

    .line 20
    :goto_0
    iget-object v4, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o00〇〇Oo:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v4, :cond_2

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 21
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o〇:I

    if-nez v0, :cond_1

    const/4 v9, 0x1

    goto :goto_1

    :cond_1
    const/4 v9, 0x2

    :goto_1
    move-object v5, p1

    move-object v7, p1

    invoke-virtual/range {v4 .. v9}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([DI[DII)V

    :cond_2
    return v3

    .line 22
    :cond_3
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "quad iterator iterator out of bounds"

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public currentSegment([F)I
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/QuadIterator;->isDone()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o〇:I

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getX1()D

    move-result-wide v4

    double-to-float v0, v4

    aput v0, p1, v3

    .line 4
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getY1()D

    move-result-wide v4

    double-to-float v0, v4

    aput v0, p1, v2

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getCtrlX()D

    move-result-wide v4

    double-to-float v0, v4

    aput v0, p1, v3

    .line 6
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getCtrlY()D

    move-result-wide v3

    double-to-float v0, v3

    aput v0, p1, v2

    .line 7
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getX2()D

    move-result-wide v3

    double-to-float v0, v3

    aput v0, p1, v1

    .line 8
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇080:Lcom/intsig/office/java/awt/geom/QuadCurve2D;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/QuadCurve2D;->getY2()D

    move-result-wide v3

    double-to-float v0, v3

    const/4 v3, 0x3

    aput v0, p1, v3

    const/4 v3, 0x2

    .line 9
    :goto_0
    iget-object v4, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o00〇〇Oo:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v4, :cond_2

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 10
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o〇:I

    if-nez v0, :cond_1

    const/4 v9, 0x1

    goto :goto_1

    :cond_1
    const/4 v9, 0x2

    :goto_1
    move-object v5, p1

    move-object v7, p1

    invoke-virtual/range {v4 .. v9}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    :cond_2
    return v3

    .line 11
    :cond_3
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "quad iterator iterator out of bounds"

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getWindingRule()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDone()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o〇:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-le v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public next()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o〇:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iput v0, p0, Lcom/intsig/office/java/awt/geom/QuadIterator;->〇o〇:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class Lcom/intsig/office/java/awt/geom/AreaIterator;
.super Ljava/lang/Object;
.source "Area.java"

# interfaces
.implements Lcom/intsig/office/java/awt/geom/PathIterator;


# instance fields
.field private O8:Lcom/intsig/office/java/awt/geom/Curve;

.field private Oo08:Lcom/intsig/office/java/awt/geom/Curve;

.field private 〇080:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field private 〇o00〇〇Oo:Ljava/util/Vector;

.field private 〇o〇:I


# direct methods
.method public constructor <init>(Ljava/util/Vector;Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->〇o00〇〇Oo:Ljava/util/Vector;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->〇080:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    const/4 v0, 0x1

    .line 13
    if-lt p2, v0, :cond_0

    .line 14
    .line 15
    const/4 p2, 0x0

    .line 16
    invoke-virtual {p1, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    check-cast p1, Lcom/intsig/office/java/awt/geom/Curve;

    .line 21
    .line 22
    iput-object p1, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 9

    .line 3
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->O8:Lcom/intsig/office/java/awt/geom/Curve;

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    .line 4
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getOrder()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getX0()D

    move-result-wide v2

    const/4 v0, 0x0

    aput-wide v2, p1, v0

    .line 6
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getY0()D

    move-result-wide v2

    aput-wide v2, p1, v1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x4

    return p1

    .line 7
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    if-eqz v0, :cond_5

    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/office/java/awt/geom/Curve;->getSegment([D)I

    move-result v0

    .line 9
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/Curve;->getOrder()I

    move-result v2

    move v1, v0

    if-nez v2, :cond_3

    :goto_1
    const/4 v8, 0x1

    goto :goto_2

    :cond_3
    move v8, v2

    .line 10
    :goto_2
    iget-object v3, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->〇080:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v3, :cond_4

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v4, p1

    move-object v6, p1

    .line 11
    invoke-virtual/range {v3 .. v8}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([DI[DII)V

    :cond_4
    return v1

    .line 12
    :cond_5
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "area iterator out of bounds"

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public currentSegment([F)I
    .locals 7

    const/4 v0, 0x6

    new-array v0, v0, [D

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/java/awt/geom/AreaIterator;->currentSegment([D)I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-ne v1, v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    if-ne v1, v4, :cond_1

    const/4 v2, 0x2

    goto :goto_0

    :cond_1
    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    :goto_0
    mul-int/lit8 v5, v2, 0x2

    if-ge v3, v5, :cond_3

    .line 2
    aget-wide v5, v0, v3

    double-to-float v5, v5

    aput v5, p1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method

.method public getWindingRule()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDone()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->O8:Lcom/intsig/office/java/awt/geom/Curve;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public next()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->O8:Lcom/intsig/office/java/awt/geom/Curve;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iput-object v1, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->O8:Lcom/intsig/office/java/awt/geom/Curve;

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->O8:Lcom/intsig/office/java/awt/geom/Curve;

    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->〇o〇:I

    .line 14
    .line 15
    add-int/lit8 v0, v0, 0x1

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->〇o〇:I

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->〇o00〇〇Oo:Ljava/util/Vector;

    .line 20
    .line 21
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-ge v0, v2, :cond_1

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->〇o00〇〇Oo:Ljava/util/Vector;

    .line 28
    .line 29
    iget v2, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->〇o〇:I

    .line 30
    .line 31
    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Lcom/intsig/office/java/awt/geom/Curve;

    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getOrder()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->O8:Lcom/intsig/office/java/awt/geom/Curve;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getX1()D

    .line 48
    .line 49
    .line 50
    move-result-wide v2

    .line 51
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getX0()D

    .line 54
    .line 55
    .line 56
    move-result-wide v4

    .line 57
    cmpl-double v0, v2, v4

    .line 58
    .line 59
    if-nez v0, :cond_2

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->O8:Lcom/intsig/office/java/awt/geom/Curve;

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getY1()D

    .line 64
    .line 65
    .line 66
    move-result-wide v2

    .line 67
    iget-object v0, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    .line 68
    .line 69
    invoke-virtual {v0}, Lcom/intsig/office/java/awt/geom/Curve;->getY0()D

    .line 70
    .line 71
    .line 72
    move-result-wide v4

    .line 73
    cmpl-double v0, v2, v4

    .line 74
    .line 75
    if-nez v0, :cond_2

    .line 76
    .line 77
    iput-object v1, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->O8:Lcom/intsig/office/java/awt/geom/Curve;

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_1
    iput-object v1, p0, Lcom/intsig/office/java/awt/geom/AreaIterator;->Oo08:Lcom/intsig/office/java/awt/geom/Curve;

    .line 81
    .line 82
    :cond_2
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

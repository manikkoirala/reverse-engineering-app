.class public Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;
.super Lcom/intsig/office/java/awt/geom/RoundRectangle2D;
.source "RoundRectangle2D.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/RoundRectangle2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Float"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x2f817959ce0672aaL


# instance fields
.field public archeight:F

.field public arcwidth:F

.field public height:F

.field public width:F

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D;-><init>()V

    return-void
.end method

.method public constructor <init>(FFFFFF)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D;-><init>()V

    .line 3
    invoke-virtual/range {p0 .. p6}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->setRoundRect(FFFFFF)V

    return-void
.end method


# virtual methods
.method public getArcHeight()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->archeight:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getArcWidth()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->arcwidth:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->x:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->y:F

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->width:F

    .line 8
    .line 9
    iget v4, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->height:F

    .line 10
    .line 11
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Float;-><init>(FFFF)V

    .line 12
    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeight()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->height:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWidth()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->width:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getX()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->x:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getY()D
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->y:F

    .line 2
    .line 3
    float-to-double v0, v0

    .line 4
    return-wide v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isEmpty()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->width:F

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    cmpg-float v0, v0, v1

    .line 5
    .line 6
    if-lez v0, :cond_1

    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->height:F

    .line 9
    .line 10
    cmpg-float v0, v0, v1

    .line 11
    .line 12
    if-gtz v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    goto :goto_1

    .line 17
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 18
    :goto_1
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setRoundRect(DDDDDD)V
    .locals 0

    double-to-float p1, p1

    .line 7
    iput p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->x:F

    double-to-float p1, p3

    .line 8
    iput p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->y:F

    double-to-float p1, p5

    .line 9
    iput p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->width:F

    double-to-float p1, p7

    .line 10
    iput p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->height:F

    double-to-float p1, p9

    .line 11
    iput p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->arcwidth:F

    double-to-float p1, p11

    .line 12
    iput p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->archeight:F

    return-void
.end method

.method public setRoundRect(FFFFFF)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->x:F

    .line 2
    iput p2, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->y:F

    .line 3
    iput p3, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->width:F

    .line 4
    iput p4, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->height:F

    .line 5
    iput p5, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->arcwidth:F

    .line 6
    iput p6, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->archeight:F

    return-void
.end method

.method public setRoundRect(Lcom/intsig/office/java/awt/geom/RoundRectangle2D;)V
    .locals 2

    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->x:F

    .line 14
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->y:F

    .line 15
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->width:F

    .line 16
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->height:F

    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D;->getArcWidth()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->arcwidth:F

    .line 18
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D;->getArcHeight()D

    move-result-wide v0

    double-to-float p1, v0

    iput p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Float;->archeight:F

    return-void
.end method

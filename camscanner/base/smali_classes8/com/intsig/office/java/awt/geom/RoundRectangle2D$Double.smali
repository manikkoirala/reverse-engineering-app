.class public Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;
.super Lcom/intsig/office/java/awt/geom/RoundRectangle2D;
.source "RoundRectangle2D.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/office/java/awt/geom/RoundRectangle2D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Double"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0xe8e94c42b521a65L


# instance fields
.field public archeight:D

.field public arcwidth:D

.field public height:D

.field public width:D

.field public x:D

.field public y:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D;-><init>()V

    return-void
.end method

.method public constructor <init>(DDDDDD)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D;-><init>()V

    .line 3
    invoke-virtual/range {p0 .. p12}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->setRoundRect(DDDDDD)V

    return-void
.end method


# virtual methods
.method public getArcHeight()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->archeight:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getArcWidth()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->arcwidth:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->x:D

    .line 4
    .line 5
    iget-wide v3, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->y:D

    .line 6
    .line 7
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->width:D

    .line 8
    .line 9
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->height:D

    .line 10
    .line 11
    move-object v0, v9

    .line 12
    invoke-direct/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/Rectangle2D$Double;-><init>(DDDD)V

    .line 13
    .line 14
    .line 15
    return-object v9
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHeight()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->height:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getWidth()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->width:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getX()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->x:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getY()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->y:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isEmpty()Z
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->width:D

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmpg-double v4, v0, v2

    .line 6
    .line 7
    if-lez v4, :cond_1

    .line 8
    .line 9
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->height:D

    .line 10
    .line 11
    cmpg-double v4, v0, v2

    .line 12
    .line 13
    if-gtz v4, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 19
    :goto_1
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setRoundRect(DDDDDD)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->x:D

    .line 2
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->y:D

    .line 3
    iput-wide p5, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->width:D

    .line 4
    iput-wide p7, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->height:D

    .line 5
    iput-wide p9, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->arcwidth:D

    .line 6
    iput-wide p11, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->archeight:D

    return-void
.end method

.method public setRoundRect(Lcom/intsig/office/java/awt/geom/RoundRectangle2D;)V
    .locals 2

    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->x:D

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->y:D

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->width:D

    .line 10
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->height:D

    .line 11
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D;->getArcWidth()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->arcwidth:D

    .line 12
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RoundRectangle2D;->getArcHeight()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/RoundRectangle2D$Double;->archeight:D

    return-void
.end method

.class Lcom/intsig/office/java/awt/geom/ArcIterator;
.super Ljava/lang/Object;
.source "ArcIterator.java"

# interfaces
.implements Lcom/intsig/office/java/awt/geom/PathIterator;


# instance fields
.field O8:D

.field OO0o〇〇〇〇0:I

.field Oo08:D

.field oO80:Lcom/intsig/office/java/awt/geom/AffineTransform;

.field o〇0:D

.field 〇080:D

.field 〇80〇808〇O:I

.field 〇8o8o〇:I

.field 〇o00〇〇Oo:D

.field 〇o〇:D

.field 〇〇888:D


# direct methods
.method constructor <init>(Lcom/intsig/office/java/awt/geom/Arc2D;Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 9

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getWidth()D

    .line 5
    .line 6
    .line 7
    move-result-wide v0

    .line 8
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 9
    .line 10
    div-double/2addr v0, v2

    .line 11
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getHeight()D

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    div-double/2addr v0, v2

    .line 18
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getX()D

    .line 21
    .line 22
    .line 23
    move-result-wide v0

    .line 24
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    .line 25
    .line 26
    add-double/2addr v0, v2

    .line 27
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/RectangularShape;->getY()D

    .line 30
    .line 31
    .line 32
    move-result-wide v0

    .line 33
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    .line 34
    .line 35
    add-double/2addr v0, v2

    .line 36
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleStart()D

    .line 39
    .line 40
    .line 41
    move-result-wide v0

    .line 42
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    .line 43
    .line 44
    .line 45
    move-result-wide v0

    .line 46
    neg-double v0, v0

    .line 47
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->Oo08:D

    .line 48
    .line 49
    iput-object p2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->oO80:Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->getAngleExtent()D

    .line 52
    .line 53
    .line 54
    move-result-wide v0

    .line 55
    neg-double v0, v0

    .line 56
    const-wide v2, 0x4076800000000000L    # 360.0

    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    const/4 p2, 0x0

    .line 62
    const-wide/16 v4, 0x0

    .line 63
    .line 64
    cmpl-double v6, v0, v2

    .line 65
    .line 66
    if-gez v6, :cond_1

    .line 67
    .line 68
    const-wide v2, -0x3f89800000000000L    # -360.0

    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    cmpg-double v6, v0, v2

    .line 74
    .line 75
    if-gtz v6, :cond_0

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    .line 79
    .line 80
    .line 81
    move-result-wide v2

    .line 82
    const-wide v6, 0x4056800000000000L    # 90.0

    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    div-double/2addr v2, v6

    .line 88
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    .line 89
    .line 90
    .line 91
    move-result-wide v2

    .line 92
    double-to-int v2, v2

    .line 93
    iput v2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->OO0o〇〇〇〇0:I

    .line 94
    .line 95
    int-to-double v2, v2

    .line 96
    div-double/2addr v0, v2

    .line 97
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    .line 98
    .line 99
    .line 100
    move-result-wide v0

    .line 101
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->o〇0:D

    .line 102
    .line 103
    invoke-static {v0, v1}, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080(D)D

    .line 104
    .line 105
    .line 106
    move-result-wide v0

    .line 107
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇〇888:D

    .line 108
    .line 109
    cmpl-double v2, v0, v4

    .line 110
    .line 111
    if-nez v2, :cond_2

    .line 112
    .line 113
    iput p2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->OO0o〇〇〇〇0:I

    .line 114
    .line 115
    goto :goto_1

    .line 116
    :cond_1
    :goto_0
    const/4 v2, 0x4

    .line 117
    iput v2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->OO0o〇〇〇〇0:I

    .line 118
    .line 119
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->o〇0:D

    .line 125
    .line 126
    const-wide v6, 0x3fe1ac5111534a21L    # 0.5522847498307933

    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    iput-wide v6, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇〇888:D

    .line 132
    .line 133
    cmpg-double v8, v0, v4

    .line 134
    .line 135
    if-gez v8, :cond_2

    .line 136
    .line 137
    neg-double v0, v2

    .line 138
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->o〇0:D

    .line 139
    .line 140
    neg-double v0, v6

    .line 141
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇〇888:D

    .line 142
    .line 143
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Arc2D;->getArcType()I

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    if-eqz p1, :cond_5

    .line 148
    .line 149
    const/4 p2, 0x1

    .line 150
    if-eq p1, p2, :cond_4

    .line 151
    .line 152
    const/4 p2, 0x2

    .line 153
    if-eq p1, p2, :cond_3

    .line 154
    .line 155
    goto :goto_2

    .line 156
    :cond_3
    iput p2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇8o8o〇:I

    .line 157
    .line 158
    goto :goto_2

    .line 159
    :cond_4
    iput p2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇8o8o〇:I

    .line 160
    .line 161
    goto :goto_2

    .line 162
    :cond_5
    iput p2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇8o8o〇:I

    .line 163
    .line 164
    :goto_2
    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    .line 165
    .line 166
    cmpg-double v0, p1, v4

    .line 167
    .line 168
    if-ltz v0, :cond_6

    .line 169
    .line 170
    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    .line 171
    .line 172
    cmpg-double v0, p1, v4

    .line 173
    .line 174
    if-gez v0, :cond_7

    .line 175
    .line 176
    :cond_6
    const/4 p1, -0x1

    .line 177
    iput p1, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇8o8o〇:I

    .line 178
    .line 179
    iput p1, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->OO0o〇〇〇〇0:I

    .line 180
    .line 181
    :cond_7
    return-void
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method private static 〇080(D)D
    .locals 4

    .line 1
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    .line 2
    .line 3
    div-double/2addr p0, v0

    .line 4
    const-wide v0, 0x3ff5555555555555L    # 1.3333333333333333

    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    invoke-static {p0, p1}, Ljava/lang/Math;->sin(D)D

    .line 10
    .line 11
    .line 12
    move-result-wide v2

    .line 13
    mul-double v2, v2, v0

    .line 14
    .line 15
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 16
    .line 17
    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    .line 18
    .line 19
    .line 20
    move-result-wide p0

    .line 21
    add-double/2addr p0, v0

    .line 22
    div-double/2addr v2, p0

    .line 23
    return-wide v2
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public currentSegment([D)I
    .locals 17

    move-object/from16 v0, p0

    .line 29
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/ArcIterator;->isDone()Z

    move-result v1

    if-nez v1, :cond_6

    .line 30
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->Oo08:D

    .line 31
    iget v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇80〇808〇O:I

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-nez v3, :cond_1

    .line 32
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    mul-double v5, v5, v9

    add-double/2addr v3, v5

    aput-wide v3, p1, v7

    .line 33
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    mul-double v1, v1, v5

    add-double/2addr v3, v1

    aput-wide v3, p1, v8

    .line 34
    iget-object v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->oO80:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v2, p1

    move-object/from16 v4, p1

    .line 35
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([DI[DII)V

    :cond_0
    return v7

    .line 36
    :cond_1
    iget v4, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->OO0o〇〇〇〇0:I

    const/4 v5, 0x4

    if-le v3, v4, :cond_4

    .line 37
    iget v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇8o8o〇:I

    add-int/2addr v4, v1

    if-ne v3, v4, :cond_2

    return v5

    .line 38
    :cond_2
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    aput-wide v1, p1, v7

    .line 39
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    aput-wide v1, p1, v8

    .line 40
    iget-object v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->oO80:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v1, :cond_3

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v2, p1

    move-object/from16 v4, p1

    .line 41
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([DI[DII)V

    :cond_3
    return v8

    .line 42
    :cond_4
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->o〇0:D

    sub-int/2addr v3, v8

    int-to-double v3, v3

    mul-double v9, v9, v3

    add-double/2addr v1, v9

    .line 43
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    .line 44
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    .line 45
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇〇888:D

    mul-double v15, v13, v9

    sub-double v15, v3, v15

    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    mul-double v15, v15, v5

    add-double/2addr v11, v15

    aput-wide v11, p1, v7

    .line 46
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    mul-double v13, v13, v3

    add-double/2addr v9, v13

    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    mul-double v9, v9, v3

    add-double/2addr v5, v9

    aput-wide v5, p1, v8

    .line 47
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->o〇0:D

    add-double/2addr v1, v3

    .line 48
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    .line 49
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    .line 50
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇〇888:D

    mul-double v9, v7, v1

    add-double/2addr v9, v3

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    mul-double v9, v9, v11

    add-double/2addr v9, v5

    const/4 v13, 0x2

    aput-wide v9, p1, v13

    .line 51
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    mul-double v7, v7, v3

    sub-double v7, v1, v7

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    mul-double v7, v7, v13

    add-double/2addr v7, v9

    const/4 v15, 0x3

    aput-wide v7, p1, v15

    mul-double v3, v3, v11

    add-double/2addr v5, v3

    const/4 v3, 0x4

    .line 52
    aput-wide v5, p1, v3

    mul-double v1, v1, v13

    add-double/2addr v9, v1

    const/4 v1, 0x5

    .line 53
    aput-wide v9, p1, v1

    .line 54
    iget-object v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->oO80:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v1, :cond_5

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x3

    move-object/from16 v2, p1

    move-object/from16 v4, p1

    .line 55
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([DI[DII)V

    :cond_5
    return v15

    .line 56
    :cond_6
    new-instance v1, Ljava/util/NoSuchElementException;

    const-string v2, "arc iterator out of bounds"

    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public currentSegment([F)I
    .locals 17

    move-object/from16 v0, p0

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/ArcIterator;->isDone()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->Oo08:D

    .line 3
    iget v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇80〇808〇O:I

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-nez v3, :cond_1

    .line 4
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    mul-double v5, v5, v9

    add-double/2addr v3, v5

    double-to-float v3, v3

    aput v3, p1, v7

    .line 5
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    mul-double v1, v1, v5

    add-double/2addr v3, v1

    double-to-float v1, v3

    aput v1, p1, v8

    .line 6
    iget-object v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->oO80:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v2, p1

    move-object/from16 v4, p1

    .line 7
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    :cond_0
    return v7

    .line 8
    :cond_1
    iget v4, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->OO0o〇〇〇〇0:I

    const/4 v5, 0x4

    if-le v3, v4, :cond_4

    .line 9
    iget v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇8o8o〇:I

    add-int/2addr v4, v1

    if-ne v3, v4, :cond_2

    return v5

    .line 10
    :cond_2
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    double-to-float v1, v1

    aput v1, p1, v7

    .line 11
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    double-to-float v1, v1

    aput v1, p1, v8

    .line 12
    iget-object v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->oO80:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v1, :cond_3

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v2, p1

    move-object/from16 v4, p1

    .line 13
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    :cond_3
    return v8

    .line 14
    :cond_4
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->o〇0:D

    sub-int/2addr v3, v8

    int-to-double v3, v3

    mul-double v9, v9, v3

    add-double/2addr v1, v9

    .line 15
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    .line 16
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    .line 17
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇〇888:D

    mul-double v15, v13, v9

    sub-double v15, v3, v15

    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    mul-double v15, v15, v5

    add-double/2addr v11, v15

    double-to-float v5, v11

    aput v5, p1, v7

    .line 18
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    mul-double v13, v13, v3

    add-double/2addr v9, v13

    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    mul-double v9, v9, v3

    add-double/2addr v5, v9

    double-to-float v3, v5

    aput v3, p1, v8

    .line 19
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->o〇0:D

    add-double/2addr v1, v3

    .line 20
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    .line 21
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    .line 22
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇080:D

    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇〇888:D

    mul-double v9, v7, v1

    add-double/2addr v9, v3

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o〇:D

    mul-double v9, v9, v11

    add-double/2addr v9, v5

    double-to-float v9, v9

    const/4 v10, 0x2

    aput v9, p1, v10

    .line 23
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇o00〇〇Oo:D

    mul-double v7, v7, v3

    sub-double v7, v1, v7

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->O8:D

    mul-double v7, v7, v13

    add-double/2addr v7, v9

    double-to-float v7, v7

    const/4 v8, 0x3

    aput v7, p1, v8

    mul-double v3, v3, v11

    add-double/2addr v5, v3

    double-to-float v3, v5

    const/4 v4, 0x4

    .line 24
    aput v3, p1, v4

    mul-double v1, v1, v13

    add-double/2addr v9, v1

    double-to-float v1, v9

    const/4 v2, 0x5

    .line 25
    aput v1, p1, v2

    .line 26
    iget-object v1, v0, Lcom/intsig/office/java/awt/geom/ArcIterator;->oO80:Lcom/intsig/office/java/awt/geom/AffineTransform;

    if-eqz v1, :cond_5

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x3

    move-object/from16 v2, p1

    move-object/from16 v4, p1

    .line 27
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->transform([FI[FII)V

    :cond_5
    return v8

    .line 28
    :cond_6
    new-instance v1, Ljava/util/NoSuchElementException;

    const-string v2, "arc iterator out of bounds"

    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getWindingRule()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isDone()Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇80〇808〇O:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->OO0o〇〇〇〇0:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇8o8o〇:I

    .line 6
    .line 7
    add-int/2addr v1, v2

    .line 8
    if-le v0, v1, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public next()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇80〇808〇O:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iput v0, p0, Lcom/intsig/office/java/awt/geom/ArcIterator;->〇80〇808〇O:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

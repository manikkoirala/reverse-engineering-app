.class public interface abstract Lcom/intsig/office/java/awt/Shape;
.super Ljava/lang/Object;
.source "Shape.java"


# virtual methods
.method public abstract contains(DD)Z
.end method

.method public abstract contains(DDDD)Z
.end method

.method public abstract contains(Lcom/intsig/office/java/awt/geom/Point2D;)Z
.end method

.method public abstract contains(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
.end method

.method public abstract getBounds()Lcom/intsig/office/java/awt/Rectangle;
.end method

.method public abstract getBounds2D()Lcom/intsig/office/java/awt/geom/Rectangle2D;
.end method

.method public abstract getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;)Lcom/intsig/office/java/awt/geom/PathIterator;
.end method

.method public abstract getPathIterator(Lcom/intsig/office/java/awt/geom/AffineTransform;D)Lcom/intsig/office/java/awt/geom/PathIterator;
.end method

.method public abstract intersects(DDDD)Z
.end method

.method public abstract intersects(Lcom/intsig/office/java/awt/geom/Rectangle2D;)Z
.end method

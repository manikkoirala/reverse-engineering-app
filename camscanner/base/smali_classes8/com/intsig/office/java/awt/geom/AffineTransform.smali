.class public Lcom/intsig/office/java/awt/geom/AffineTransform;
.super Ljava/lang/Object;
.source "AffineTransform.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/io/Serializable;


# static fields
.field static final APPLY_IDENTITY:I = 0x0

.field static final APPLY_SCALE:I = 0x2

.field static final APPLY_SHEAR:I = 0x4

.field static final APPLY_TRANSLATE:I = 0x1

.field private static final HI_IDENTITY:I = 0x0

.field private static final HI_SCALE:I = 0x10

.field private static final HI_SHEAR:I = 0x20

.field private static final HI_SHIFT:I = 0x3

.field private static final HI_TRANSLATE:I = 0x8

.field public static final TYPE_FLIP:I = 0x40

.field public static final TYPE_GENERAL_ROTATION:I = 0x10

.field public static final TYPE_GENERAL_SCALE:I = 0x4

.field public static final TYPE_GENERAL_TRANSFORM:I = 0x20

.field public static final TYPE_IDENTITY:I = 0x0

.field public static final TYPE_MASK_ROTATION:I = 0x18

.field public static final TYPE_MASK_SCALE:I = 0x6

.field public static final TYPE_QUADRANT_ROTATION:I = 0x8

.field public static final TYPE_TRANSLATION:I = 0x1

.field public static final TYPE_UNIFORM_SCALE:I = 0x2

.field private static final TYPE_UNKNOWN:I = -0x1

.field private static final rot90conversion:[I

.field private static final serialVersionUID:J = 0x127891154ad5ff62L


# instance fields
.field m00:D

.field m01:D

.field m02:D

.field m10:D

.field m11:D

.field m12:D

.field transient state:I

.field private transient type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [I

    .line 4
    .line 5
    fill-array-data v0, :array_0

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->rot90conversion:[I

    .line 9
    .line 10
    return-void

    .line 11
    :array_0
    .array-data 4
        0x4
        0x5
        0x4
        0x5
        0x2
        0x3
        0x6
        0x7
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public constructor <init>()V
    .locals 2

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 11
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    return-void
.end method

.method public constructor <init>(DDDDDD)V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 40
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 41
    iput-wide p5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 42
    iput-wide p7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 43
    iput-wide p9, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 44
    iput-wide p11, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 45
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    return-void
.end method

.method private constructor <init>(DDDDDDI)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 3
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 4
    iput-wide p5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 5
    iput-wide p7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 6
    iput-wide p9, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 7
    iput-wide p11, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 8
    iput p13, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    const/4 p1, -0x1

    .line 9
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    return-void
.end method

.method public constructor <init>(FFFFFF)V
    .locals 2

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    float-to-double v0, p1

    .line 22
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    float-to-double p1, p2

    .line 23
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    float-to-double p1, p3

    .line 24
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    float-to-double p1, p4

    .line 25
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    float-to-double p1, p5

    .line 26
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    float-to-double p1, p6

    .line 27
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 28
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 2

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 14
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 15
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 16
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 17
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 18
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 19
    iget v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 20
    iget p1, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    return-void
.end method

.method public constructor <init>([D)V
    .locals 4

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 47
    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    const/4 v0, 0x1

    .line 48
    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    const/4 v0, 0x2

    .line 49
    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    const/4 v0, 0x3

    .line 50
    aget-wide v0, p1, v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 51
    array-length v0, p1

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    const/4 v0, 0x4

    .line 52
    aget-wide v2, p1, v0

    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 53
    aget-wide v0, p1, v1

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    return-void
.end method

.method public constructor <init>([F)V
    .locals 4

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 30
    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    const/4 v0, 0x1

    .line 31
    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    const/4 v0, 0x2

    .line 32
    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    const/4 v0, 0x3

    .line 33
    aget v0, p1, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 34
    array-length v0, p1

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    const/4 v0, 0x4

    .line 35
    aget v0, p1, v0

    float-to-double v2, v0

    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 36
    aget p1, p1, v1

    float-to-double v0, p1

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    return-void
.end method

.method private static _matround(D)D
    .locals 2

    .line 1
    const-wide v0, 0x430c6bf526340000L    # 1.0E15

    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    mul-double p0, p0, v0

    .line 7
    .line 8
    invoke-static {p0, p1}, Ljava/lang/Math;->rint(D)D

    .line 9
    .line 10
    .line 11
    move-result-wide p0

    .line 12
    div-double/2addr p0, v0

    .line 13
    return-wide p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private calculateType()V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    .line 4
    .line 5
    .line 6
    iget v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 7
    .line 8
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 9
    .line 10
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 11
    .line 12
    const-wide/16 v6, 0x0

    .line 13
    .line 14
    packed-switch v1, :pswitch_data_0

    .line 15
    .line 16
    .line 17
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 18
    .line 19
    .line 20
    goto/16 :goto_9

    .line 21
    .line 22
    :pswitch_0
    const/4 v1, 0x0

    .line 23
    goto/16 :goto_a

    .line 24
    .line 25
    :pswitch_1
    const/4 v1, 0x1

    .line 26
    goto :goto_0

    .line 27
    :pswitch_2
    const/4 v1, 0x0

    .line 28
    :goto_0
    iget-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 29
    .line 30
    cmpl-double v12, v10, v6

    .line 31
    .line 32
    if-ltz v12, :cond_0

    .line 33
    .line 34
    const/4 v12, 0x1

    .line 35
    goto :goto_1

    .line 36
    :cond_0
    const/4 v12, 0x0

    .line 37
    :goto_1
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 38
    .line 39
    cmpl-double v15, v13, v6

    .line 40
    .line 41
    if-ltz v15, :cond_1

    .line 42
    .line 43
    const/4 v8, 0x1

    .line 44
    goto :goto_2

    .line 45
    :cond_1
    const/4 v8, 0x0

    .line 46
    :goto_2
    if-eq v12, v8, :cond_4

    .line 47
    .line 48
    neg-double v6, v13

    .line 49
    cmpl-double v8, v10, v6

    .line 50
    .line 51
    if-eqz v8, :cond_2

    .line 52
    .line 53
    :goto_3
    or-int/lit8 v8, v1, 0xc

    .line 54
    .line 55
    goto/16 :goto_f

    .line 56
    .line 57
    :cond_2
    cmpl-double v6, v10, v4

    .line 58
    .line 59
    if-eqz v6, :cond_3

    .line 60
    .line 61
    cmpl-double v4, v10, v2

    .line 62
    .line 63
    if-eqz v4, :cond_3

    .line 64
    .line 65
    goto :goto_7

    .line 66
    :cond_3
    or-int/lit8 v8, v1, 0x8

    .line 67
    .line 68
    goto/16 :goto_f

    .line 69
    .line 70
    :cond_4
    cmpl-double v2, v10, v13

    .line 71
    .line 72
    if-nez v2, :cond_5

    .line 73
    .line 74
    or-int/lit8 v8, v1, 0x4a

    .line 75
    .line 76
    goto/16 :goto_f

    .line 77
    .line 78
    :cond_5
    or-int/lit8 v8, v1, 0x4c

    .line 79
    .line 80
    goto/16 :goto_f

    .line 81
    .line 82
    :pswitch_3
    const/4 v1, 0x1

    .line 83
    goto :goto_4

    .line 84
    :pswitch_4
    const/4 v1, 0x0

    .line 85
    :goto_4
    iget-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 86
    .line 87
    cmpl-double v12, v10, v6

    .line 88
    .line 89
    if-ltz v12, :cond_6

    .line 90
    .line 91
    const/4 v12, 0x1

    .line 92
    goto :goto_5

    .line 93
    :cond_6
    const/4 v12, 0x0

    .line 94
    :goto_5
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 95
    .line 96
    cmpl-double v15, v13, v6

    .line 97
    .line 98
    if-ltz v15, :cond_7

    .line 99
    .line 100
    const/4 v8, 0x1

    .line 101
    goto :goto_6

    .line 102
    :cond_7
    const/4 v8, 0x0

    .line 103
    :goto_6
    if-ne v12, v8, :cond_b

    .line 104
    .line 105
    if-eqz v12, :cond_9

    .line 106
    .line 107
    cmpl-double v2, v10, v13

    .line 108
    .line 109
    if-nez v2, :cond_8

    .line 110
    .line 111
    or-int/lit8 v8, v1, 0x2

    .line 112
    .line 113
    goto/16 :goto_f

    .line 114
    .line 115
    :cond_8
    or-int/lit8 v8, v1, 0x4

    .line 116
    .line 117
    goto/16 :goto_f

    .line 118
    .line 119
    :cond_9
    cmpl-double v4, v10, v13

    .line 120
    .line 121
    if-eqz v4, :cond_a

    .line 122
    .line 123
    goto :goto_3

    .line 124
    :cond_a
    cmpl-double v4, v10, v2

    .line 125
    .line 126
    if-eqz v4, :cond_3

    .line 127
    .line 128
    :goto_7
    or-int/lit8 v8, v1, 0xa

    .line 129
    .line 130
    goto/16 :goto_f

    .line 131
    .line 132
    :cond_b
    neg-double v6, v13

    .line 133
    cmpl-double v8, v10, v6

    .line 134
    .line 135
    if-nez v8, :cond_e

    .line 136
    .line 137
    cmpl-double v6, v10, v4

    .line 138
    .line 139
    if-eqz v6, :cond_d

    .line 140
    .line 141
    cmpl-double v4, v10, v2

    .line 142
    .line 143
    if-nez v4, :cond_c

    .line 144
    .line 145
    goto :goto_8

    .line 146
    :cond_c
    or-int/lit8 v8, v1, 0x42

    .line 147
    .line 148
    goto/16 :goto_f

    .line 149
    .line 150
    :cond_d
    :goto_8
    or-int/lit8 v8, v1, 0x40

    .line 151
    .line 152
    goto/16 :goto_f

    .line 153
    .line 154
    :cond_e
    or-int/lit8 v8, v1, 0x44

    .line 155
    .line 156
    goto/16 :goto_f

    .line 157
    .line 158
    :pswitch_5
    const/4 v8, 0x1

    .line 159
    goto/16 :goto_f

    .line 160
    .line 161
    :pswitch_6
    const/4 v8, 0x0

    .line 162
    goto :goto_f

    .line 163
    :goto_9
    :pswitch_7
    const/4 v1, 0x1

    .line 164
    :goto_a
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 165
    .line 166
    iget-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 167
    .line 168
    mul-double v12, v2, v10

    .line 169
    .line 170
    iget-wide v14, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 171
    .line 172
    iget-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 173
    .line 174
    mul-double v16, v14, v8

    .line 175
    .line 176
    add-double v12, v12, v16

    .line 177
    .line 178
    cmpl-double v16, v12, v6

    .line 179
    .line 180
    if-eqz v16, :cond_f

    .line 181
    .line 182
    const/16 v1, 0x20

    .line 183
    .line 184
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 185
    .line 186
    return-void

    .line 187
    :cond_f
    cmpl-double v12, v2, v6

    .line 188
    .line 189
    if-ltz v12, :cond_10

    .line 190
    .line 191
    const/4 v12, 0x1

    .line 192
    goto :goto_b

    .line 193
    :cond_10
    const/4 v12, 0x0

    .line 194
    :goto_b
    cmpl-double v13, v8, v6

    .line 195
    .line 196
    if-ltz v13, :cond_11

    .line 197
    .line 198
    const/4 v6, 0x1

    .line 199
    goto :goto_c

    .line 200
    :cond_11
    const/4 v6, 0x0

    .line 201
    :goto_c
    if-ne v12, v6, :cond_15

    .line 202
    .line 203
    cmpl-double v6, v2, v8

    .line 204
    .line 205
    if-nez v6, :cond_14

    .line 206
    .line 207
    neg-double v6, v14

    .line 208
    cmpl-double v12, v10, v6

    .line 209
    .line 210
    if-eqz v12, :cond_12

    .line 211
    .line 212
    goto :goto_d

    .line 213
    :cond_12
    mul-double v2, v2, v8

    .line 214
    .line 215
    mul-double v10, v10, v14

    .line 216
    .line 217
    sub-double/2addr v2, v10

    .line 218
    cmpl-double v6, v2, v4

    .line 219
    .line 220
    if-eqz v6, :cond_13

    .line 221
    .line 222
    or-int/lit8 v8, v1, 0x12

    .line 223
    .line 224
    goto :goto_f

    .line 225
    :cond_13
    or-int/lit8 v8, v1, 0x10

    .line 226
    .line 227
    goto :goto_f

    .line 228
    :cond_14
    :goto_d
    or-int/lit8 v8, v1, 0x14

    .line 229
    .line 230
    goto :goto_f

    .line 231
    :cond_15
    neg-double v6, v8

    .line 232
    cmpl-double v12, v2, v6

    .line 233
    .line 234
    if-nez v12, :cond_18

    .line 235
    .line 236
    cmpl-double v6, v10, v14

    .line 237
    .line 238
    if-eqz v6, :cond_16

    .line 239
    .line 240
    goto :goto_e

    .line 241
    :cond_16
    mul-double v2, v2, v8

    .line 242
    .line 243
    mul-double v10, v10, v14

    .line 244
    .line 245
    sub-double/2addr v2, v10

    .line 246
    cmpl-double v6, v2, v4

    .line 247
    .line 248
    if-eqz v6, :cond_17

    .line 249
    .line 250
    or-int/lit8 v8, v1, 0x52

    .line 251
    .line 252
    goto :goto_f

    .line 253
    :cond_17
    or-int/lit8 v8, v1, 0x50

    .line 254
    .line 255
    goto :goto_f

    .line 256
    :cond_18
    :goto_e
    or-int/lit8 v8, v1, 0x54

    .line 257
    .line 258
    :goto_f
    iput v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 259
    .line 260
    return-void

    .line 261
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method public static getQuadrantRotateInstance(I)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AffineTransform;

    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 2
    invoke-virtual {v0, p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToQuadrantRotation(I)V

    return-object v0
.end method

.method public static getQuadrantRotateInstance(IDD)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 7

    .line 3
    new-instance v6, Lcom/intsig/office/java/awt/geom/AffineTransform;

    invoke-direct {v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    move-object v0, v6

    move v1, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 4
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToQuadrantRotation(IDD)V

    return-object v6
.end method

.method public static getRotateInstance(D)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AffineTransform;

    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 2
    invoke-virtual {v0, p0, p1}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToRotation(D)V

    return-object v0
.end method

.method public static getRotateInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 1

    .line 5
    new-instance v0, Lcom/intsig/office/java/awt/geom/AffineTransform;

    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 6
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToRotation(DD)V

    return-object v0
.end method

.method public static getRotateInstance(DDD)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 8

    .line 3
    new-instance v7, Lcom/intsig/office/java/awt/geom/AffineTransform;

    invoke-direct {v7}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    move-object v0, v7

    move-wide v1, p0

    move-wide v3, p2

    move-wide v5, p4

    .line 4
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToRotation(DDD)V

    return-object v7
.end method

.method public static getRotateInstance(DDDD)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 10

    .line 7
    new-instance v9, Lcom/intsig/office/java/awt/geom/AffineTransform;

    invoke-direct {v9}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    move-object v0, v9

    move-wide v1, p0

    move-wide v3, p2

    move-wide v5, p4

    move-wide/from16 v7, p6

    .line 8
    invoke-virtual/range {v0 .. v8}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToRotation(DDDD)V

    return-object v9
.end method

.method public static getScaleInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToScale(DD)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static getShearInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToShear(DD)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static getTranslateInstance(DD)Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToTranslation(DD)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private final rotate180()V
    .locals 8

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 2
    .line 3
    neg-double v0, v0

    .line 4
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 5
    .line 6
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 7
    .line 8
    neg-double v2, v2

    .line 9
    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 10
    .line 11
    iget v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 12
    .line 13
    and-int/lit8 v5, v4, 0x4

    .line 14
    .line 15
    if-eqz v5, :cond_0

    .line 16
    .line 17
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 18
    .line 19
    neg-double v0, v0

    .line 20
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 21
    .line 22
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 23
    .line 24
    neg-double v0, v0

    .line 25
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 29
    .line 30
    cmpl-double v7, v0, v5

    .line 31
    .line 32
    if-nez v7, :cond_1

    .line 33
    .line 34
    cmpl-double v0, v2, v5

    .line 35
    .line 36
    if-nez v0, :cond_1

    .line 37
    .line 38
    and-int/lit8 v0, v4, -0x3

    .line 39
    .line 40
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    or-int/lit8 v0, v4, 0x2

    .line 44
    .line 45
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 46
    .line 47
    :goto_0
    const/4 v0, -0x1

    .line 48
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private final rotate270()V
    .locals 8

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 2
    .line 3
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 4
    .line 5
    neg-double v2, v2

    .line 6
    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 7
    .line 8
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 9
    .line 10
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 11
    .line 12
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 13
    .line 14
    neg-double v4, v4

    .line 15
    iput-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 16
    .line 17
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 18
    .line 19
    sget-object v4, Lcom/intsig/office/java/awt/geom/AffineTransform;->rot90conversion:[I

    .line 20
    .line 21
    iget v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 22
    .line 23
    aget v4, v4, v5

    .line 24
    .line 25
    and-int/lit8 v5, v4, 0x6

    .line 26
    .line 27
    const/4 v6, 0x2

    .line 28
    if-ne v5, v6, :cond_0

    .line 29
    .line 30
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 31
    .line 32
    cmpl-double v7, v2, v5

    .line 33
    .line 34
    if-nez v7, :cond_0

    .line 35
    .line 36
    cmpl-double v2, v0, v5

    .line 37
    .line 38
    if-nez v2, :cond_0

    .line 39
    .line 40
    add-int/lit8 v4, v4, -0x2

    .line 41
    .line 42
    :cond_0
    iput v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 43
    .line 44
    const/4 v0, -0x1

    .line 45
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private final rotate90()V
    .locals 8

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 2
    .line 3
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 4
    .line 5
    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 6
    .line 7
    neg-double v0, v0

    .line 8
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 9
    .line 10
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 11
    .line 12
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 13
    .line 14
    iput-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 15
    .line 16
    neg-double v0, v0

    .line 17
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 18
    .line 19
    sget-object v4, Lcom/intsig/office/java/awt/geom/AffineTransform;->rot90conversion:[I

    .line 20
    .line 21
    iget v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 22
    .line 23
    aget v4, v4, v5

    .line 24
    .line 25
    and-int/lit8 v5, v4, 0x6

    .line 26
    .line 27
    const/4 v6, 0x2

    .line 28
    if-ne v5, v6, :cond_0

    .line 29
    .line 30
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 31
    .line 32
    cmpl-double v7, v2, v5

    .line 33
    .line 34
    if-nez v7, :cond_0

    .line 35
    .line 36
    cmpl-double v2, v0, v5

    .line 37
    .line 38
    if-nez v2, :cond_0

    .line 39
    .line 40
    add-int/lit8 v4, v4, -0x2

    .line 41
    .line 42
    :cond_0
    iput v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 43
    .line 44
    const/4 v0, -0x1

    .line 45
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private stateError()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/InternalError;

    .line 2
    .line 3
    const-string v1, "missing case in transform state switch"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .line 1
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    return-object v0

    .line 6
    :catch_0
    new-instance v0, Ljava/lang/InternalError;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    .line 9
    .line 10
    .line 11
    throw v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public concatenate(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 22

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    iget v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 6
    .line 7
    iget v3, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 8
    .line 9
    shl-int/lit8 v4, v3, 0x3

    .line 10
    .line 11
    or-int/2addr v4, v2

    .line 12
    const/16 v5, 0x30

    .line 13
    .line 14
    if-eq v4, v5, :cond_1

    .line 15
    .line 16
    const/16 v5, 0x38

    .line 17
    .line 18
    if-eq v4, v5, :cond_0

    .line 19
    .line 20
    packed-switch v4, :pswitch_data_0

    .line 21
    .line 22
    .line 23
    const-wide/16 v6, 0x0

    .line 24
    .line 25
    packed-switch v4, :pswitch_data_1

    .line 26
    .line 27
    .line 28
    iget-wide v6, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 29
    .line 30
    iget-wide v8, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 31
    .line 32
    iget-wide v10, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 33
    .line 34
    iget-wide v12, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 35
    .line 36
    iget-wide v14, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 37
    .line 38
    move-wide/from16 v16, v6

    .line 39
    .line 40
    iget-wide v5, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 41
    .line 42
    packed-switch v2, :pswitch_data_2

    .line 43
    .line 44
    .line 45
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 46
    .line 47
    .line 48
    goto :goto_1

    .line 49
    :pswitch_0
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 50
    .line 51
    mul-double v12, v12, v1

    .line 52
    .line 53
    iput-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 54
    .line 55
    mul-double v14, v14, v1

    .line 56
    .line 57
    iput-wide v14, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 58
    .line 59
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 60
    .line 61
    mul-double v5, v5, v1

    .line 62
    .line 63
    add-double/2addr v3, v5

    .line 64
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 65
    .line 66
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 67
    .line 68
    mul-double v6, v16, v1

    .line 69
    .line 70
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 71
    .line 72
    mul-double v8, v8, v1

    .line 73
    .line 74
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 75
    .line 76
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 77
    .line 78
    mul-double v10, v10, v1

    .line 79
    .line 80
    add-double/2addr v3, v10

    .line 81
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :pswitch_1
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 85
    .line 86
    mul-double v3, v16, v1

    .line 87
    .line 88
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 89
    .line 90
    mul-double v8, v8, v1

    .line 91
    .line 92
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 93
    .line 94
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 95
    .line 96
    mul-double v10, v10, v1

    .line 97
    .line 98
    add-double/2addr v3, v10

    .line 99
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 100
    .line 101
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 102
    .line 103
    mul-double v12, v12, v1

    .line 104
    .line 105
    iput-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 106
    .line 107
    mul-double v14, v14, v1

    .line 108
    .line 109
    iput-wide v14, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 110
    .line 111
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 112
    .line 113
    mul-double v5, v5, v1

    .line 114
    .line 115
    add-double/2addr v3, v5

    .line 116
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 117
    .line 118
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    .line 119
    .line 120
    .line 121
    return-void

    .line 122
    :pswitch_2
    move-wide/from16 v1, v16

    .line 123
    .line 124
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 125
    .line 126
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 127
    .line 128
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 129
    .line 130
    add-double/2addr v1, v10

    .line 131
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 132
    .line 133
    iput-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 134
    .line 135
    iput-wide v14, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 136
    .line 137
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 138
    .line 139
    add-double/2addr v1, v5

    .line 140
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 141
    .line 142
    or-int/lit8 v1, v3, 0x1

    .line 143
    .line 144
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 145
    .line 146
    const/4 v1, -0x1

    .line 147
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 148
    .line 149
    return-void

    .line 150
    :goto_1
    :pswitch_3
    or-int v1, v2, v3

    .line 151
    .line 152
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 153
    .line 154
    :pswitch_4
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 155
    .line 156
    move-wide/from16 v18, v5

    .line 157
    .line 158
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 159
    .line 160
    mul-double v6, v16, v1

    .line 161
    .line 162
    mul-double v20, v12, v4

    .line 163
    .line 164
    add-double v6, v6, v20

    .line 165
    .line 166
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 167
    .line 168
    mul-double v6, v8, v1

    .line 169
    .line 170
    mul-double v20, v14, v4

    .line 171
    .line 172
    add-double v6, v6, v20

    .line 173
    .line 174
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 175
    .line 176
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 177
    .line 178
    mul-double v1, v1, v10

    .line 179
    .line 180
    mul-double v3, v18, v4

    .line 181
    .line 182
    add-double/2addr v1, v3

    .line 183
    add-double/2addr v6, v1

    .line 184
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 185
    .line 186
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 187
    .line 188
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 189
    .line 190
    mul-double v6, v16, v1

    .line 191
    .line 192
    mul-double v12, v12, v3

    .line 193
    .line 194
    add-double/2addr v6, v12

    .line 195
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 196
    .line 197
    mul-double v8, v8, v1

    .line 198
    .line 199
    mul-double v14, v14, v3

    .line 200
    .line 201
    add-double/2addr v8, v14

    .line 202
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 203
    .line 204
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 205
    .line 206
    mul-double v10, v10, v1

    .line 207
    .line 208
    mul-double v1, v18, v3

    .line 209
    .line 210
    add-double/2addr v10, v1

    .line 211
    add-double/2addr v5, v10

    .line 212
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 213
    .line 214
    const/4 v1, -0x1

    .line 215
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 216
    .line 217
    return-void

    .line 218
    :pswitch_5
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 219
    .line 220
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 221
    .line 222
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 223
    .line 224
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 225
    .line 226
    goto :goto_2

    .line 227
    :pswitch_6
    iget-wide v2, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 228
    .line 229
    iget-wide v5, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 230
    .line 231
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 232
    .line 233
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 234
    .line 235
    mul-double v9, v9, v5

    .line 236
    .line 237
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 238
    .line 239
    mul-double v7, v7, v2

    .line 240
    .line 241
    iput-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 242
    .line 243
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 244
    .line 245
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 246
    .line 247
    mul-double v9, v9, v5

    .line 248
    .line 249
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 250
    .line 251
    mul-double v7, v7, v2

    .line 252
    .line 253
    iput-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 254
    .line 255
    const/4 v1, -0x1

    .line 256
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 257
    .line 258
    return-void

    .line 259
    :pswitch_7
    iget-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 260
    .line 261
    iget-wide v10, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 262
    .line 263
    mul-double v8, v8, v10

    .line 264
    .line 265
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 266
    .line 267
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 268
    .line 269
    iget-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 270
    .line 271
    iget-wide v10, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 272
    .line 273
    mul-double v8, v8, v10

    .line 274
    .line 275
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 276
    .line 277
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 278
    .line 279
    xor-int/lit8 v1, v2, 0x6

    .line 280
    .line 281
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 282
    .line 283
    const/4 v1, -0x1

    .line 284
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 285
    .line 286
    return-void

    .line 287
    :pswitch_8
    iget-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 288
    .line 289
    iget-wide v10, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 290
    .line 291
    mul-double v8, v8, v10

    .line 292
    .line 293
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 294
    .line 295
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 296
    .line 297
    iget-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 298
    .line 299
    iget-wide v10, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 300
    .line 301
    mul-double v8, v8, v10

    .line 302
    .line 303
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 304
    .line 305
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 306
    .line 307
    xor-int/lit8 v1, v2, 0x6

    .line 308
    .line 309
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 310
    .line 311
    const/4 v2, -0x1

    .line 312
    iput v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 313
    .line 314
    return-void

    .line 315
    :pswitch_9
    const/4 v2, -0x1

    .line 316
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 317
    .line 318
    iget-wide v3, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 319
    .line 320
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 321
    .line 322
    iget-wide v3, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 323
    .line 324
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 325
    .line 326
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 327
    .line 328
    const/4 v1, 0x5

    .line 329
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 330
    .line 331
    iput v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 332
    .line 333
    return-void

    .line 334
    :goto_2
    :pswitch_a
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 335
    .line 336
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 337
    .line 338
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 339
    .line 340
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 341
    .line 342
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 343
    .line 344
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 345
    .line 346
    iput v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 347
    .line 348
    iget v1, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 349
    .line 350
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 351
    .line 352
    return-void

    .line 353
    :pswitch_b
    iget-wide v2, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 354
    .line 355
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 356
    .line 357
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/intsig/office/java/awt/geom/AffineTransform;->scale(DD)V

    .line 358
    .line 359
    .line 360
    return-void

    .line 361
    :pswitch_c
    iget-wide v2, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 362
    .line 363
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 364
    .line 365
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 366
    .line 367
    .line 368
    :pswitch_d
    return-void

    .line 369
    :cond_0
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 370
    .line 371
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 372
    .line 373
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 374
    .line 375
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 376
    .line 377
    :pswitch_e
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 378
    .line 379
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 380
    .line 381
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 382
    .line 383
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 384
    .line 385
    :pswitch_f
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 386
    .line 387
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 388
    .line 389
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 390
    .line 391
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 392
    .line 393
    iput v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 394
    .line 395
    iget v1, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 396
    .line 397
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 398
    .line 399
    return-void

    .line 400
    :cond_1
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 401
    .line 402
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 403
    .line 404
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 405
    .line 406
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 407
    .line 408
    :pswitch_10
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 409
    .line 410
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 411
    .line 412
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 413
    .line 414
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 415
    .line 416
    iput v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 417
    .line 418
    iget v1, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 419
    .line 420
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 421
    .line 422
    return-void

    .line 423
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_f
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_10
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_e
    .end packed-switch

    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    :pswitch_data_1
    .packed-switch 0x20
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_5
    .end packed-switch

    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public createInverse()Lcom/intsig/office/java/awt/geom/AffineTransform;
    .locals 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 4
    .line 5
    const-string v2, "Determinant is "

    .line 6
    .line 7
    const-wide/16 v3, 0x1

    .line 8
    .line 9
    const-string v5, "Determinant is 0"

    .line 10
    .line 11
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 12
    .line 13
    const-wide/16 v8, 0x0

    .line 14
    .line 15
    packed-switch v1, :pswitch_data_0

    .line 16
    .line 17
    .line 18
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 19
    .line 20
    .line 21
    goto/16 :goto_0

    .line 22
    .line 23
    :pswitch_0
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 24
    .line 25
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 26
    .line 27
    mul-double v5, v5, v7

    .line 28
    .line 29
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 30
    .line 31
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 32
    .line 33
    mul-double v7, v7, v9

    .line 34
    .line 35
    sub-double/2addr v5, v7

    .line 36
    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    .line 37
    .line 38
    .line 39
    move-result-wide v7

    .line 40
    cmpg-double v1, v7, v3

    .line 41
    .line 42
    if-lez v1, :cond_0

    .line 43
    .line 44
    new-instance v1, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 45
    .line 46
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 47
    .line 48
    div-double v8, v2, v5

    .line 49
    .line 50
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 51
    .line 52
    neg-double v2, v2

    .line 53
    div-double v10, v2, v5

    .line 54
    .line 55
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 56
    .line 57
    neg-double v2, v2

    .line 58
    div-double v12, v2, v5

    .line 59
    .line 60
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 61
    .line 62
    div-double v14, v2, v5

    .line 63
    .line 64
    const-wide/16 v16, 0x0

    .line 65
    .line 66
    const-wide/16 v18, 0x0

    .line 67
    .line 68
    const/16 v20, 0x6

    .line 69
    .line 70
    move-object v7, v1

    .line 71
    invoke-direct/range {v7 .. v20}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>(DDDDDDI)V

    .line 72
    .line 73
    .line 74
    return-object v1

    .line 75
    :cond_0
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 76
    .line 77
    new-instance v3, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    invoke-direct {v1, v2}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    throw v1

    .line 96
    :pswitch_1
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 97
    .line 98
    cmpl-double v3, v1, v8

    .line 99
    .line 100
    if-eqz v3, :cond_1

    .line 101
    .line 102
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 103
    .line 104
    cmpl-double v10, v3, v8

    .line 105
    .line 106
    if-eqz v10, :cond_1

    .line 107
    .line 108
    new-instance v5, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 109
    .line 110
    const-wide/16 v12, 0x0

    .line 111
    .line 112
    div-double v14, v6, v1

    .line 113
    .line 114
    div-double v16, v6, v3

    .line 115
    .line 116
    const-wide/16 v18, 0x0

    .line 117
    .line 118
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 119
    .line 120
    neg-double v6, v6

    .line 121
    div-double v20, v6, v3

    .line 122
    .line 123
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 124
    .line 125
    neg-double v3, v3

    .line 126
    div-double v22, v3, v1

    .line 127
    .line 128
    const/16 v24, 0x5

    .line 129
    .line 130
    move-object v11, v5

    .line 131
    invoke-direct/range {v11 .. v24}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>(DDDDDDI)V

    .line 132
    .line 133
    .line 134
    return-object v5

    .line 135
    :cond_1
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 136
    .line 137
    invoke-direct {v1, v5}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    throw v1

    .line 141
    :pswitch_2
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 142
    .line 143
    cmpl-double v3, v1, v8

    .line 144
    .line 145
    if-eqz v3, :cond_2

    .line 146
    .line 147
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 148
    .line 149
    cmpl-double v10, v3, v8

    .line 150
    .line 151
    if-eqz v10, :cond_2

    .line 152
    .line 153
    new-instance v5, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 154
    .line 155
    const-wide/16 v12, 0x0

    .line 156
    .line 157
    div-double v14, v6, v1

    .line 158
    .line 159
    div-double v16, v6, v3

    .line 160
    .line 161
    const-wide/16 v18, 0x0

    .line 162
    .line 163
    const-wide/16 v20, 0x0

    .line 164
    .line 165
    const-wide/16 v22, 0x0

    .line 166
    .line 167
    const/16 v24, 0x4

    .line 168
    .line 169
    move-object v11, v5

    .line 170
    invoke-direct/range {v11 .. v24}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>(DDDDDDI)V

    .line 171
    .line 172
    .line 173
    return-object v5

    .line 174
    :cond_2
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 175
    .line 176
    invoke-direct {v1, v5}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    throw v1

    .line 180
    :pswitch_3
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 181
    .line 182
    cmpl-double v3, v1, v8

    .line 183
    .line 184
    if-eqz v3, :cond_3

    .line 185
    .line 186
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 187
    .line 188
    cmpl-double v10, v3, v8

    .line 189
    .line 190
    if-eqz v10, :cond_3

    .line 191
    .line 192
    new-instance v5, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 193
    .line 194
    div-double v12, v6, v1

    .line 195
    .line 196
    const-wide/16 v14, 0x0

    .line 197
    .line 198
    const-wide/16 v16, 0x0

    .line 199
    .line 200
    div-double v18, v6, v3

    .line 201
    .line 202
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 203
    .line 204
    neg-double v6, v6

    .line 205
    div-double v20, v6, v1

    .line 206
    .line 207
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 208
    .line 209
    neg-double v1, v1

    .line 210
    div-double v22, v1, v3

    .line 211
    .line 212
    const/16 v24, 0x3

    .line 213
    .line 214
    move-object v11, v5

    .line 215
    invoke-direct/range {v11 .. v24}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>(DDDDDDI)V

    .line 216
    .line 217
    .line 218
    return-object v5

    .line 219
    :cond_3
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 220
    .line 221
    invoke-direct {v1, v5}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    throw v1

    .line 225
    :pswitch_4
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 226
    .line 227
    cmpl-double v3, v1, v8

    .line 228
    .line 229
    if-eqz v3, :cond_4

    .line 230
    .line 231
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 232
    .line 233
    cmpl-double v10, v3, v8

    .line 234
    .line 235
    if-eqz v10, :cond_4

    .line 236
    .line 237
    new-instance v5, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 238
    .line 239
    div-double v12, v6, v1

    .line 240
    .line 241
    const-wide/16 v14, 0x0

    .line 242
    .line 243
    const-wide/16 v16, 0x0

    .line 244
    .line 245
    div-double v18, v6, v3

    .line 246
    .line 247
    const-wide/16 v20, 0x0

    .line 248
    .line 249
    const-wide/16 v22, 0x0

    .line 250
    .line 251
    const/16 v24, 0x2

    .line 252
    .line 253
    move-object v11, v5

    .line 254
    invoke-direct/range {v11 .. v24}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>(DDDDDDI)V

    .line 255
    .line 256
    .line 257
    return-object v5

    .line 258
    :cond_4
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 259
    .line 260
    invoke-direct {v1, v5}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 261
    .line 262
    .line 263
    throw v1

    .line 264
    :pswitch_5
    new-instance v1, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 265
    .line 266
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    .line 267
    .line 268
    const-wide/16 v9, 0x0

    .line 269
    .line 270
    const-wide/16 v11, 0x0

    .line 271
    .line 272
    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    .line 273
    .line 274
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 275
    .line 276
    neg-double v2, v2

    .line 277
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 278
    .line 279
    neg-double v4, v4

    .line 280
    const/16 v19, 0x1

    .line 281
    .line 282
    move-object v6, v1

    .line 283
    move-wide v15, v2

    .line 284
    move-wide/from16 v17, v4

    .line 285
    .line 286
    invoke-direct/range {v6 .. v19}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>(DDDDDDI)V

    .line 287
    .line 288
    .line 289
    return-object v1

    .line 290
    :pswitch_6
    new-instance v1, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 291
    .line 292
    invoke-direct {v1}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>()V

    .line 293
    .line 294
    .line 295
    return-object v1

    .line 296
    :goto_0
    :pswitch_7
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 297
    .line 298
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 299
    .line 300
    mul-double v5, v5, v7

    .line 301
    .line 302
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 303
    .line 304
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 305
    .line 306
    mul-double v7, v7, v9

    .line 307
    .line 308
    sub-double/2addr v5, v7

    .line 309
    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    .line 310
    .line 311
    .line 312
    move-result-wide v7

    .line 313
    cmpg-double v1, v7, v3

    .line 314
    .line 315
    if-lez v1, :cond_5

    .line 316
    .line 317
    new-instance v1, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 318
    .line 319
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 320
    .line 321
    div-double v8, v2, v5

    .line 322
    .line 323
    iget-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 324
    .line 325
    neg-double v12, v10

    .line 326
    div-double/2addr v12, v5

    .line 327
    iget-wide v14, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 328
    .line 329
    move-wide/from16 v16, v12

    .line 330
    .line 331
    neg-double v12, v14

    .line 332
    div-double/2addr v12, v5

    .line 333
    move-wide/from16 v18, v12

    .line 334
    .line 335
    iget-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 336
    .line 337
    div-double v20, v12, v5

    .line 338
    .line 339
    move-wide/from16 v22, v8

    .line 340
    .line 341
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 342
    .line 343
    mul-double v14, v14, v7

    .line 344
    .line 345
    move-wide/from16 v24, v7

    .line 346
    .line 347
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 348
    .line 349
    mul-double v2, v2, v7

    .line 350
    .line 351
    sub-double/2addr v14, v2

    .line 352
    div-double v2, v14, v5

    .line 353
    .line 354
    mul-double v10, v10, v7

    .line 355
    .line 356
    mul-double v12, v12, v24

    .line 357
    .line 358
    sub-double/2addr v10, v12

    .line 359
    div-double v4, v10, v5

    .line 360
    .line 361
    const/4 v6, 0x7

    .line 362
    move-object v7, v1

    .line 363
    move-wide/from16 v8, v22

    .line 364
    .line 365
    move-wide/from16 v10, v16

    .line 366
    .line 367
    move-wide/from16 v12, v18

    .line 368
    .line 369
    move-wide/from16 v14, v20

    .line 370
    .line 371
    move-wide/from16 v16, v2

    .line 372
    .line 373
    move-wide/from16 v18, v4

    .line 374
    .line 375
    move/from16 v20, v6

    .line 376
    .line 377
    invoke-direct/range {v7 .. v20}, Lcom/intsig/office/java/awt/geom/AffineTransform;-><init>(DDDDDDI)V

    .line 378
    .line 379
    .line 380
    return-object v1

    .line 381
    :cond_5
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 382
    .line 383
    new-instance v3, Ljava/lang/StringBuilder;

    .line 384
    .line 385
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 386
    .line 387
    .line 388
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    .line 390
    .line 391
    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 392
    .line 393
    .line 394
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 395
    .line 396
    .line 397
    move-result-object v2

    .line 398
    invoke-direct {v1, v2}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 399
    .line 400
    .line 401
    throw v1

    .line 402
    nop

    .line 403
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method public createTransformedShape(Lcom/intsig/office/java/awt/Shape;)Lcom/intsig/office/java/awt/Shape;
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return-object p1

    .line 5
    :cond_0
    new-instance v0, Lcom/intsig/office/java/awt/geom/Path2D$Double;

    .line 6
    .line 7
    invoke-direct {v0, p1, p0}, Lcom/intsig/office/java/awt/geom/Path2D$Double;-><init>(Lcom/intsig/office/java/awt/Shape;Lcom/intsig/office/java/awt/geom/AffineTransform;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public deltaTransform(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;)Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 8

    if-nez p2, :cond_1

    .line 1
    instance-of p2, p1, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    if-eqz p2, :cond_0

    .line 2
    new-instance p2, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    invoke-direct {p2}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>()V

    goto :goto_0

    .line 3
    :cond_0
    new-instance p2, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    invoke-direct {p2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>()V

    .line 4
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    .line 6
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    packed-switch p1, :pswitch_data_0

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto :goto_1

    .line 8
    :pswitch_0
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v2, v2, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v0, v0, v4

    invoke-virtual {p2, v2, v3, v0, v1}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 9
    :pswitch_1
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v0, v0, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v2, v2, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 10
    :pswitch_2
    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 11
    :goto_1
    :pswitch_3
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v4, v4, v0

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v6, v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v0, v0, v6

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v2, v2, v6

    add-double/2addr v0, v2

    invoke-virtual {p2, v4, v5, v0, v1}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public deltaTransform([DI[DII)V
    .locals 23

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    if-ne v3, v1, :cond_0

    if-le v4, v2, :cond_0

    mul-int/lit8 v5, p5, 0x2

    add-int v6, v2, v5

    if-ge v4, v6, :cond_0

    .line 12
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v4

    .line 13
    :cond_0
    iget v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    packed-switch v5, :pswitch_data_0

    .line 14
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto :goto_2

    .line 15
    :pswitch_0
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 16
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    move v9, v2

    move/from16 v2, p5

    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_1

    add-int/lit8 v10, v9, 0x1

    .line 17
    aget-wide v11, v1, v9

    add-int/lit8 v9, v4, 0x1

    add-int/lit8 v13, v10, 0x1

    .line 18
    aget-wide v14, v1, v10

    mul-double v14, v14, v5

    aput-wide v14, v3, v4

    add-int/lit8 v4, v9, 0x1

    mul-double v11, v11, v7

    .line 19
    aput-wide v11, v3, v9

    move v9, v13

    goto :goto_0

    :cond_1
    return-void

    .line 20
    :pswitch_1
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 21
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move v9, v2

    move/from16 v2, p5

    :goto_1
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_2

    add-int/lit8 v10, v4, 0x1

    add-int/lit8 v11, v9, 0x1

    .line 22
    aget-wide v12, v1, v9

    mul-double v12, v12, v5

    aput-wide v12, v3, v4

    add-int/lit8 v4, v10, 0x1

    add-int/lit8 v9, v11, 0x1

    .line 23
    aget-wide v11, v1, v11

    mul-double v11, v11, v7

    aput-wide v11, v3, v10

    goto :goto_1

    :cond_2
    return-void

    :pswitch_2
    if-ne v1, v3, :cond_3

    if-eq v2, v4, :cond_4

    :cond_3
    mul-int/lit8 v5, p5, 0x2

    .line 24
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    return-void

    .line 25
    :goto_2
    :pswitch_3
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 26
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 27
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 28
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move v13, v2

    move/from16 v2, p5

    :goto_3
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_5

    add-int/lit8 v14, v13, 0x1

    .line 29
    aget-wide v15, v1, v13

    add-int/lit8 v13, v14, 0x1

    .line 30
    aget-wide v17, v1, v14

    add-int/lit8 v14, v4, 0x1

    mul-double v19, v15, v5

    mul-double v21, v17, v7

    add-double v19, v19, v21

    .line 31
    aput-wide v19, v3, v4

    add-int/lit8 v4, v14, 0x1

    mul-double v15, v15, v9

    mul-double v17, v17, v11

    add-double v15, v15, v17

    .line 32
    aput-wide v15, v3, v14

    goto :goto_3

    :cond_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .line 1
    instance-of v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    check-cast p1, Lcom/intsig/office/java/awt/geom/AffineTransform;

    .line 8
    .line 9
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 10
    .line 11
    iget-wide v4, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 12
    .line 13
    cmpl-double v0, v2, v4

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 18
    .line 19
    iget-wide v4, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 20
    .line 21
    cmpl-double v0, v2, v4

    .line 22
    .line 23
    if-nez v0, :cond_1

    .line 24
    .line 25
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 26
    .line 27
    iget-wide v4, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 28
    .line 29
    cmpl-double v0, v2, v4

    .line 30
    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 34
    .line 35
    iget-wide v4, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 36
    .line 37
    cmpl-double v0, v2, v4

    .line 38
    .line 39
    if-nez v0, :cond_1

    .line 40
    .line 41
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 42
    .line 43
    iget-wide v4, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 44
    .line 45
    cmpl-double v0, v2, v4

    .line 46
    .line 47
    if-nez v0, :cond_1

    .line 48
    .line 49
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 50
    .line 51
    iget-wide v4, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 52
    .line 53
    cmpl-double p1, v2, v4

    .line 54
    .line 55
    if-nez p1, :cond_1

    .line 56
    .line 57
    const/4 v1, 0x1

    .line 58
    :cond_1
    return v1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getDeterminant()D
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 2
    .line 3
    packed-switch v0, :pswitch_data_0

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 7
    .line 8
    .line 9
    goto :goto_0

    .line 10
    :pswitch_0
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 11
    .line 12
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 13
    .line 14
    mul-double v0, v0, v2

    .line 15
    .line 16
    neg-double v0, v0

    .line 17
    return-wide v0

    .line 18
    :pswitch_1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 19
    .line 20
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 21
    .line 22
    mul-double v0, v0, v2

    .line 23
    .line 24
    return-wide v0

    .line 25
    :pswitch_2
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 26
    .line 27
    return-wide v0

    .line 28
    :goto_0
    :pswitch_3
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 29
    .line 30
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 31
    .line 32
    mul-double v0, v0, v2

    .line 33
    .line 34
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 35
    .line 36
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 37
    .line 38
    mul-double v2, v2, v4

    .line 39
    .line 40
    sub-double/2addr v0, v2

    .line 41
    return-wide v0

    .line 42
    nop

    .line 43
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getMatrix([D)V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 3
    .line 4
    aput-wide v1, p1, v0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 8
    .line 9
    aput-wide v1, p1, v0

    .line 10
    .line 11
    const/4 v0, 0x2

    .line 12
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 13
    .line 14
    aput-wide v1, p1, v0

    .line 15
    .line 16
    const/4 v0, 0x3

    .line 17
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 18
    .line 19
    aput-wide v1, p1, v0

    .line 20
    .line 21
    array-length v0, p1

    .line 22
    const/4 v1, 0x5

    .line 23
    if-le v0, v1, :cond_0

    .line 24
    .line 25
    const/4 v0, 0x4

    .line 26
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 27
    .line 28
    aput-wide v2, p1, v0

    .line 29
    .line 30
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 31
    .line 32
    aput-wide v2, p1, v1

    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public getScaleX()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getScaleY()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShearX()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShearY()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTranslateX()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTranslateY()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getType()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->calculateType()V

    .line 7
    .line 8
    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 10
    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public hashCode()I
    .locals 6

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 2
    .line 3
    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    const-wide/16 v2, 0x1f

    .line 8
    .line 9
    mul-long v0, v0, v2

    .line 10
    .line 11
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 12
    .line 13
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 14
    .line 15
    .line 16
    move-result-wide v4

    .line 17
    add-long/2addr v0, v4

    .line 18
    mul-long v0, v0, v2

    .line 19
    .line 20
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 21
    .line 22
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 23
    .line 24
    .line 25
    move-result-wide v4

    .line 26
    add-long/2addr v0, v4

    .line 27
    mul-long v0, v0, v2

    .line 28
    .line 29
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 30
    .line 31
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 32
    .line 33
    .line 34
    move-result-wide v4

    .line 35
    add-long/2addr v0, v4

    .line 36
    mul-long v0, v0, v2

    .line 37
    .line 38
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 39
    .line 40
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 41
    .line 42
    .line 43
    move-result-wide v4

    .line 44
    add-long/2addr v0, v4

    .line 45
    mul-long v0, v0, v2

    .line 46
    .line 47
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 48
    .line 49
    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    .line 50
    .line 51
    .line 52
    move-result-wide v2

    .line 53
    add-long/2addr v0, v2

    .line 54
    long-to-int v2, v0

    .line 55
    const/16 v3, 0x20

    .line 56
    .line 57
    shr-long/2addr v0, v3

    .line 58
    long-to-int v1, v0

    .line 59
    xor-int v0, v2, v1

    .line 60
    .line 61
    return v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public inverseTransform(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;)Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;
        }
    .end annotation

    if-nez p2, :cond_1

    .line 1
    instance-of p2, p1, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    if-eqz p2, :cond_0

    .line 2
    new-instance p2, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    invoke-direct {p2}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>()V

    goto :goto_0

    .line 3
    :cond_0
    new-instance p2, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    invoke-direct {p2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>()V

    .line 4
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    .line 6
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    const-string v4, "Determinant is 0"

    const-wide/16 v5, 0x0

    packed-switch p1, :pswitch_data_0

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto :goto_1

    .line 8
    :pswitch_0
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    sub-double/2addr v0, v7

    .line 9
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    sub-double/2addr v2, v7

    .line 10
    :pswitch_1
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    cmpl-double p1, v7, v5

    if-eqz p1, :cond_2

    iget-wide v9, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    cmpl-double p1, v9, v5

    if-eqz p1, :cond_2

    div-double/2addr v2, v9

    div-double/2addr v0, v7

    .line 11
    invoke-virtual {p2, v2, v3, v0, v1}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 12
    :cond_2
    new-instance p1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    invoke-direct {p1, v4}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 13
    :pswitch_2
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    sub-double/2addr v0, v7

    .line 14
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    sub-double/2addr v2, v7

    .line 15
    :pswitch_3
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    cmpl-double p1, v7, v5

    if-eqz p1, :cond_3

    iget-wide v9, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    cmpl-double p1, v9, v5

    if-eqz p1, :cond_3

    div-double/2addr v0, v7

    div-double/2addr v2, v9

    .line 16
    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 17
    :cond_3
    new-instance p1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    invoke-direct {p1, v4}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 18
    :pswitch_4
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    sub-double/2addr v0, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    sub-double/2addr v2, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 19
    :pswitch_5
    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 20
    :goto_1
    :pswitch_6
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    sub-double/2addr v0, v4

    .line 21
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    sub-double/2addr v2, v4

    .line 22
    :pswitch_7
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v4, v4, v6

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    iget-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v6, v6, v8

    sub-double/2addr v4, v6

    .line 23
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide/16 v8, 0x1

    cmpg-double p1, v6, v8

    if-lez p1, :cond_4

    .line 24
    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v6, v6, v0

    iget-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v8, v8, v2

    sub-double/2addr v6, v8

    div-double/2addr v6, v4

    iget-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v2, v2, v8

    iget-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v0, v0, v8

    sub-double/2addr v2, v0

    div-double/2addr v2, v4

    invoke-virtual {p2, v6, v7, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 25
    :cond_4
    new-instance p1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Determinant is "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public inverseTransform([DI[DII)V
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    if-ne v3, v1, :cond_0

    if-le v4, v2, :cond_0

    mul-int/lit8 v5, p5, 0x2

    add-int v6, v2, v5

    if-ge v4, v6, :cond_0

    .line 26
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v4

    .line 27
    :cond_0
    iget v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    const-string v6, "Determinant is "

    const-string v9, "Determinant is 0"

    const-wide/16 v10, 0x0

    packed-switch v5, :pswitch_data_0

    .line 28
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto/16 :goto_6

    .line 29
    :pswitch_0
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 30
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 31
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 32
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v17, v9, v7

    mul-double v19, v11, v13

    sub-double v4, v17, v19

    .line 33
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v17

    const-wide/16 v15, 0x1

    cmpg-double v19, v17, v15

    if-lez v19, :cond_2

    move/from16 v6, p5

    move v15, v2

    move/from16 v2, p4

    :goto_0
    add-int/lit8 v6, v6, -0x1

    if-ltz v6, :cond_1

    add-int/lit8 v16, v15, 0x1

    .line 34
    aget-wide v17, v1, v15

    add-int/lit8 v15, v16, 0x1

    .line 35
    aget-wide v19, v1, v16

    add-int/lit8 v16, v2, 0x1

    mul-double v21, v17, v7

    mul-double v23, v19, v11

    sub-double v21, v21, v23

    div-double v21, v21, v4

    .line 36
    aput-wide v21, v3, v2

    add-int/lit8 v2, v16, 0x1

    mul-double v19, v19, v9

    mul-double v17, v17, v13

    sub-double v19, v19, v17

    div-double v19, v19, v4

    .line 37
    aput-wide v19, v3, v16

    goto :goto_0

    :cond_1
    return-void

    .line 38
    :cond_2
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 39
    :pswitch_1
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 40
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 41
    iget-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 42
    iget-wide v14, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    cmpl-double v8, v4, v10

    if-eqz v8, :cond_4

    cmpl-double v8, v12, v10

    if-eqz v8, :cond_4

    move/from16 v8, p5

    move v9, v2

    move/from16 v2, p4

    :goto_1
    add-int/lit8 v8, v8, -0x1

    if-ltz v8, :cond_3

    add-int/lit8 v10, v9, 0x1

    .line 43
    aget-wide v16, v1, v9

    sub-double v16, v16, v6

    add-int/lit8 v9, v2, 0x1

    add-int/lit8 v11, v10, 0x1

    .line 44
    aget-wide v18, v1, v10

    sub-double v18, v18, v14

    div-double v18, v18, v12

    aput-wide v18, v3, v2

    add-int/lit8 v2, v9, 0x1

    div-double v16, v16, v4

    .line 45
    aput-wide v16, v3, v9

    move v9, v11

    goto :goto_1

    :cond_3
    return-void

    .line 46
    :cond_4
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    invoke-direct {v1, v9}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :pswitch_2
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 48
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    cmpl-double v8, v4, v10

    if-eqz v8, :cond_6

    cmpl-double v8, v6, v10

    if-eqz v8, :cond_6

    move/from16 v8, p5

    move v9, v2

    move/from16 v2, p4

    :goto_2
    add-int/lit8 v8, v8, -0x1

    if-ltz v8, :cond_5

    add-int/lit8 v10, v9, 0x1

    .line 49
    aget-wide v11, v1, v9

    add-int/lit8 v9, v2, 0x1

    add-int/lit8 v13, v10, 0x1

    .line 50
    aget-wide v14, v1, v10

    div-double/2addr v14, v6

    aput-wide v14, v3, v2

    add-int/lit8 v2, v9, 0x1

    div-double/2addr v11, v4

    .line 51
    aput-wide v11, v3, v9

    move v9, v13

    goto :goto_2

    :cond_5
    return-void

    .line 52
    :cond_6
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    invoke-direct {v1, v9}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :pswitch_3
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 54
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 55
    iget-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 56
    iget-wide v14, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    cmpl-double v8, v4, v10

    if-eqz v8, :cond_8

    cmpl-double v8, v12, v10

    if-eqz v8, :cond_8

    move/from16 v8, p5

    move v9, v2

    move/from16 v2, p4

    :goto_3
    add-int/lit8 v8, v8, -0x1

    if-ltz v8, :cond_7

    add-int/lit8 v10, v2, 0x1

    add-int/lit8 v11, v9, 0x1

    .line 57
    aget-wide v16, v1, v9

    sub-double v16, v16, v6

    div-double v16, v16, v4

    aput-wide v16, v3, v2

    add-int/lit8 v2, v10, 0x1

    add-int/lit8 v9, v11, 0x1

    .line 58
    aget-wide v16, v1, v11

    sub-double v16, v16, v14

    div-double v16, v16, v12

    aput-wide v16, v3, v10

    goto :goto_3

    :cond_7
    return-void

    .line 59
    :cond_8
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    invoke-direct {v1, v9}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 60
    :pswitch_4
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 61
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    cmpl-double v8, v4, v10

    if-eqz v8, :cond_a

    cmpl-double v8, v6, v10

    if-eqz v8, :cond_a

    move/from16 v8, p5

    move v9, v2

    move/from16 v2, p4

    :goto_4
    add-int/lit8 v8, v8, -0x1

    if-ltz v8, :cond_9

    add-int/lit8 v10, v2, 0x1

    add-int/lit8 v11, v9, 0x1

    .line 62
    aget-wide v12, v1, v9

    div-double/2addr v12, v4

    aput-wide v12, v3, v2

    add-int/lit8 v2, v10, 0x1

    add-int/lit8 v9, v11, 0x1

    .line 63
    aget-wide v11, v1, v11

    div-double/2addr v11, v6

    aput-wide v11, v3, v10

    goto :goto_4

    :cond_9
    return-void

    .line 64
    :cond_a
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    invoke-direct {v1, v9}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 65
    :pswitch_5
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 66
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v8, p5

    move v9, v2

    move/from16 v2, p4

    :goto_5
    add-int/lit8 v8, v8, -0x1

    if-ltz v8, :cond_b

    add-int/lit8 v10, v2, 0x1

    add-int/lit8 v11, v9, 0x1

    .line 67
    aget-wide v12, v1, v9

    sub-double/2addr v12, v4

    aput-wide v12, v3, v2

    add-int/lit8 v2, v10, 0x1

    add-int/lit8 v9, v11, 0x1

    .line 68
    aget-wide v11, v1, v11

    sub-double/2addr v11, v6

    aput-wide v11, v3, v10

    goto :goto_5

    :cond_b
    return-void

    :pswitch_6
    move/from16 v4, p4

    if-ne v1, v3, :cond_c

    if-eq v2, v4, :cond_d

    :cond_c
    mul-int/lit8 v5, p5, 0x2

    .line 69
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    return-void

    .line 70
    :goto_6
    :pswitch_7
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 71
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 72
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 73
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 74
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move/from16 v17, v2

    .line 75
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    mul-double v18, v7, v4

    mul-double v20, v9, v13

    move-wide/from16 v22, v13

    sub-double v13, v18, v20

    .line 76
    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v18

    const-wide/16 v15, 0x1

    cmpg-double v20, v18, v15

    if-lez v20, :cond_f

    move/from16 v6, p4

    move/from16 v15, p5

    :goto_7
    add-int/lit8 v15, v15, -0x1

    if-ltz v15, :cond_e

    add-int/lit8 v16, v17, 0x1

    .line 77
    aget-wide v17, v1, v17

    sub-double v17, v17, v11

    add-int/lit8 v19, v16, 0x1

    .line 78
    aget-wide v20, v1, v16

    sub-double v20, v20, v2

    add-int/lit8 v16, v6, 0x1

    mul-double v24, v17, v4

    mul-double v26, v20, v9

    sub-double v24, v24, v26

    div-double v24, v24, v13

    .line 79
    aput-wide v24, p3, v6

    add-int/lit8 v6, v16, 0x1

    mul-double v20, v20, v7

    mul-double v17, v17, v22

    sub-double v20, v20, v17

    div-double v20, v20, v13

    .line 80
    aput-wide v20, p3, v16

    move/from16 v17, v19

    goto :goto_7

    :cond_e
    return-void

    .line 81
    :cond_f
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public invert()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 4
    .line 5
    const-string v2, "Determinant is "

    .line 6
    .line 7
    const-wide/16 v3, 0x1

    .line 8
    .line 9
    const-string v5, "Determinant is 0"

    .line 10
    .line 11
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 12
    .line 13
    const-wide/16 v8, 0x0

    .line 14
    .line 15
    packed-switch v1, :pswitch_data_0

    .line 16
    .line 17
    .line 18
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 19
    .line 20
    .line 21
    goto/16 :goto_0

    .line 22
    .line 23
    :pswitch_0
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 24
    .line 25
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 26
    .line 27
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 28
    .line 29
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 30
    .line 31
    mul-double v13, v5, v11

    .line 32
    .line 33
    mul-double v15, v7, v9

    .line 34
    .line 35
    sub-double/2addr v13, v15

    .line 36
    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    .line 37
    .line 38
    .line 39
    move-result-wide v15

    .line 40
    cmpg-double v1, v15, v3

    .line 41
    .line 42
    if-lez v1, :cond_0

    .line 43
    .line 44
    div-double/2addr v11, v13

    .line 45
    iput-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 46
    .line 47
    neg-double v1, v9

    .line 48
    div-double/2addr v1, v13

    .line 49
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 50
    .line 51
    neg-double v1, v7

    .line 52
    div-double/2addr v1, v13

    .line 53
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 54
    .line 55
    div-double/2addr v5, v13

    .line 56
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 57
    .line 58
    goto/16 :goto_1

    .line 59
    .line 60
    :cond_0
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 61
    .line 62
    new-instance v3, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v3, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-direct {v1, v2}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    throw v1

    .line 81
    :pswitch_1
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 82
    .line 83
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 84
    .line 85
    iget-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 86
    .line 87
    iget-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 88
    .line 89
    cmpl-double v14, v1, v8

    .line 90
    .line 91
    if-eqz v14, :cond_1

    .line 92
    .line 93
    cmpl-double v14, v10, v8

    .line 94
    .line 95
    if-eqz v14, :cond_1

    .line 96
    .line 97
    div-double v8, v6, v1

    .line 98
    .line 99
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 100
    .line 101
    div-double/2addr v6, v10

    .line 102
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 103
    .line 104
    neg-double v5, v12

    .line 105
    div-double/2addr v5, v10

    .line 106
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 107
    .line 108
    neg-double v3, v3

    .line 109
    div-double/2addr v3, v1

    .line 110
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 111
    .line 112
    goto/16 :goto_1

    .line 113
    .line 114
    :cond_1
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 115
    .line 116
    invoke-direct {v1, v5}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    throw v1

    .line 120
    :pswitch_2
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 121
    .line 122
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 123
    .line 124
    cmpl-double v10, v1, v8

    .line 125
    .line 126
    if-eqz v10, :cond_2

    .line 127
    .line 128
    cmpl-double v10, v3, v8

    .line 129
    .line 130
    if-eqz v10, :cond_2

    .line 131
    .line 132
    div-double v1, v6, v1

    .line 133
    .line 134
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 135
    .line 136
    div-double/2addr v6, v3

    .line 137
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 138
    .line 139
    goto/16 :goto_1

    .line 140
    .line 141
    :cond_2
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 142
    .line 143
    invoke-direct {v1, v5}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    throw v1

    .line 147
    :pswitch_3
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 148
    .line 149
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 150
    .line 151
    iget-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 152
    .line 153
    iget-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 154
    .line 155
    cmpl-double v14, v1, v8

    .line 156
    .line 157
    if-eqz v14, :cond_3

    .line 158
    .line 159
    cmpl-double v14, v10, v8

    .line 160
    .line 161
    if-eqz v14, :cond_3

    .line 162
    .line 163
    div-double v8, v6, v1

    .line 164
    .line 165
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 166
    .line 167
    div-double/2addr v6, v10

    .line 168
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 169
    .line 170
    neg-double v3, v3

    .line 171
    div-double/2addr v3, v1

    .line 172
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 173
    .line 174
    neg-double v1, v12

    .line 175
    div-double/2addr v1, v10

    .line 176
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 177
    .line 178
    goto :goto_1

    .line 179
    :cond_3
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 180
    .line 181
    invoke-direct {v1, v5}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    throw v1

    .line 185
    :pswitch_4
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 186
    .line 187
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 188
    .line 189
    cmpl-double v10, v1, v8

    .line 190
    .line 191
    if-eqz v10, :cond_4

    .line 192
    .line 193
    cmpl-double v10, v3, v8

    .line 194
    .line 195
    if-eqz v10, :cond_4

    .line 196
    .line 197
    div-double v1, v6, v1

    .line 198
    .line 199
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 200
    .line 201
    div-double/2addr v6, v3

    .line 202
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 203
    .line 204
    goto :goto_1

    .line 205
    :cond_4
    new-instance v1, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 206
    .line 207
    invoke-direct {v1, v5}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 208
    .line 209
    .line 210
    throw v1

    .line 211
    :pswitch_5
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 212
    .line 213
    neg-double v1, v1

    .line 214
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 215
    .line 216
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 217
    .line 218
    neg-double v1, v1

    .line 219
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 220
    .line 221
    goto :goto_1

    .line 222
    :goto_0
    :pswitch_6
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 223
    .line 224
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 225
    .line 226
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 227
    .line 228
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 229
    .line 230
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 231
    .line 232
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 233
    .line 234
    mul-double v17, v5, v13

    .line 235
    .line 236
    mul-double v19, v7, v11

    .line 237
    .line 238
    move-object/from16 v21, v2

    .line 239
    .line 240
    sub-double v1, v17, v19

    .line 241
    .line 242
    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    .line 243
    .line 244
    .line 245
    move-result-wide v17

    .line 246
    const-wide/16 v15, 0x1

    .line 247
    .line 248
    cmpg-double v19, v17, v15

    .line 249
    .line 250
    if-lez v19, :cond_5

    .line 251
    .line 252
    move-wide v15, v9

    .line 253
    div-double v9, v13, v1

    .line 254
    .line 255
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 256
    .line 257
    neg-double v9, v11

    .line 258
    div-double/2addr v9, v1

    .line 259
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 260
    .line 261
    neg-double v9, v7

    .line 262
    div-double/2addr v9, v1

    .line 263
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 264
    .line 265
    div-double v9, v5, v1

    .line 266
    .line 267
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 268
    .line 269
    mul-double v7, v7, v3

    .line 270
    .line 271
    mul-double v13, v13, v15

    .line 272
    .line 273
    sub-double/2addr v7, v13

    .line 274
    div-double/2addr v7, v1

    .line 275
    iput-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 276
    .line 277
    mul-double v11, v11, v15

    .line 278
    .line 279
    mul-double v5, v5, v3

    .line 280
    .line 281
    sub-double/2addr v11, v5

    .line 282
    div-double/2addr v11, v1

    .line 283
    iput-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 284
    .line 285
    :goto_1
    :pswitch_7
    return-void

    .line 286
    :cond_5
    new-instance v3, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;

    .line 287
    .line 288
    new-instance v4, Ljava/lang/StringBuilder;

    .line 289
    .line 290
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 291
    .line 292
    .line 293
    move-object/from16 v5, v21

    .line 294
    .line 295
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    .line 297
    .line 298
    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 299
    .line 300
    .line 301
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 302
    .line 303
    .line 304
    move-result-object v1

    .line 305
    invoke-direct {v3, v1}, Lcom/intsig/office/java/awt/geom/NoninvertibleTransformException;-><init>(Ljava/lang/String;)V

    .line 306
    .line 307
    .line 308
    throw v3

    .line 309
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_6
    .end packed-switch
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
.end method

.method public isIdentity()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->getType()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 15
    :goto_1
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public preConcatenate(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 24

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    iget v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 6
    .line 7
    iget v3, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 8
    .line 9
    shl-int/lit8 v4, v3, 0x3

    .line 10
    .line 11
    or-int/2addr v4, v2

    .line 12
    packed-switch v4, :pswitch_data_0

    .line 13
    .line 14
    .line 15
    packed-switch v4, :pswitch_data_1

    .line 16
    .line 17
    .line 18
    iget-wide v6, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 19
    .line 20
    iget-wide v8, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 21
    .line 22
    iget-wide v10, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 23
    .line 24
    iget-wide v12, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 25
    .line 26
    iget-wide v14, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 27
    .line 28
    move-wide/from16 v16, v6

    .line 29
    .line 30
    iget-wide v5, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 31
    .line 32
    packed-switch v2, :pswitch_data_2

    .line 33
    .line 34
    .line 35
    move-wide/from16 v18, v5

    .line 36
    .line 37
    move-wide/from16 v4, v16

    .line 38
    .line 39
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 40
    .line 41
    .line 42
    goto/16 :goto_1

    .line 43
    .line 44
    :pswitch_0
    move-wide/from16 v18, v5

    .line 45
    .line 46
    move-wide/from16 v4, v16

    .line 47
    .line 48
    goto/16 :goto_1

    .line 49
    .line 50
    :pswitch_1
    move-wide v1, v5

    .line 51
    move-wide/from16 v4, v16

    .line 52
    .line 53
    goto/16 :goto_2

    .line 54
    .line 55
    :pswitch_2
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 56
    .line 57
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 58
    .line 59
    mul-double v18, v1, v16

    .line 60
    .line 61
    mul-double v20, v3, v8

    .line 62
    .line 63
    add-double v18, v18, v20

    .line 64
    .line 65
    add-double v10, v10, v18

    .line 66
    .line 67
    mul-double v1, v1, v12

    .line 68
    .line 69
    mul-double v3, v3, v14

    .line 70
    .line 71
    add-double/2addr v1, v3

    .line 72
    add-double/2addr v5, v1

    .line 73
    :pswitch_3
    iput-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 74
    .line 75
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 76
    .line 77
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 78
    .line 79
    mul-double v8, v8, v1

    .line 80
    .line 81
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 82
    .line 83
    mul-double v1, v1, v14

    .line 84
    .line 85
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 86
    .line 87
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 88
    .line 89
    mul-double v6, v1, v16

    .line 90
    .line 91
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 92
    .line 93
    mul-double v1, v1, v12

    .line 94
    .line 95
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 96
    .line 97
    goto/16 :goto_3

    .line 98
    .line 99
    :pswitch_4
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 100
    .line 101
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 102
    .line 103
    mul-double v18, v1, v16

    .line 104
    .line 105
    mul-double v20, v3, v8

    .line 106
    .line 107
    add-double v18, v18, v20

    .line 108
    .line 109
    add-double v10, v10, v18

    .line 110
    .line 111
    mul-double v1, v1, v12

    .line 112
    .line 113
    mul-double v3, v3, v14

    .line 114
    .line 115
    add-double/2addr v1, v3

    .line 116
    add-double/2addr v5, v1

    .line 117
    :pswitch_5
    iput-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 118
    .line 119
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 120
    .line 121
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 122
    .line 123
    mul-double v6, v1, v16

    .line 124
    .line 125
    iput-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 126
    .line 127
    mul-double v1, v1, v12

    .line 128
    .line 129
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 130
    .line 131
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 132
    .line 133
    mul-double v8, v8, v1

    .line 134
    .line 135
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 136
    .line 137
    mul-double v1, v1, v14

    .line 138
    .line 139
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 140
    .line 141
    goto :goto_3

    .line 142
    :pswitch_6
    move-wide/from16 v18, v5

    .line 143
    .line 144
    iget-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 145
    .line 146
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 147
    .line 148
    mul-double v20, v4, v16

    .line 149
    .line 150
    mul-double v22, v6, v8

    .line 151
    .line 152
    add-double v20, v20, v22

    .line 153
    .line 154
    add-double v10, v10, v20

    .line 155
    .line 156
    mul-double v4, v4, v12

    .line 157
    .line 158
    mul-double v6, v6, v14

    .line 159
    .line 160
    add-double/2addr v4, v6

    .line 161
    add-double v5, v18, v4

    .line 162
    .line 163
    goto :goto_0

    .line 164
    :pswitch_7
    move-wide/from16 v18, v5

    .line 165
    .line 166
    :goto_0
    iput-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 167
    .line 168
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 169
    .line 170
    move-wide/from16 v4, v16

    .line 171
    .line 172
    iput-wide v4, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 173
    .line 174
    iput-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 175
    .line 176
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 177
    .line 178
    iput-wide v14, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 179
    .line 180
    or-int v1, v2, v3

    .line 181
    .line 182
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 183
    .line 184
    const/4 v1, -0x1

    .line 185
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 186
    .line 187
    return-void

    .line 188
    :goto_1
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 189
    .line 190
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 191
    .line 192
    mul-double v16, v1, v4

    .line 193
    .line 194
    mul-double v20, v6, v8

    .line 195
    .line 196
    add-double v16, v16, v20

    .line 197
    .line 198
    add-double v10, v10, v16

    .line 199
    .line 200
    mul-double v1, v1, v12

    .line 201
    .line 202
    mul-double v6, v6, v14

    .line 203
    .line 204
    add-double/2addr v1, v6

    .line 205
    add-double v1, v18, v1

    .line 206
    .line 207
    :goto_2
    iput-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 208
    .line 209
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 210
    .line 211
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 212
    .line 213
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 214
    .line 215
    mul-double v10, v1, v4

    .line 216
    .line 217
    mul-double v16, v6, v8

    .line 218
    .line 219
    add-double v10, v10, v16

    .line 220
    .line 221
    iput-wide v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 222
    .line 223
    mul-double v1, v1, v12

    .line 224
    .line 225
    mul-double v6, v6, v14

    .line 226
    .line 227
    add-double/2addr v1, v6

    .line 228
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 229
    .line 230
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 231
    .line 232
    iget-wide v6, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 233
    .line 234
    mul-double v3, v1, v4

    .line 235
    .line 236
    mul-double v8, v8, v6

    .line 237
    .line 238
    add-double/2addr v3, v8

    .line 239
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 240
    .line 241
    mul-double v1, v1, v12

    .line 242
    .line 243
    mul-double v6, v6, v14

    .line 244
    .line 245
    add-double/2addr v1, v6

    .line 246
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 247
    .line 248
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    .line 249
    .line 250
    .line 251
    return-void

    .line 252
    :pswitch_8
    or-int/lit8 v2, v2, 0x2

    .line 253
    .line 254
    :pswitch_9
    xor-int/lit8 v2, v2, 0x4

    .line 255
    .line 256
    iput v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 257
    .line 258
    :pswitch_a
    iget-wide v2, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 259
    .line 260
    iget-wide v5, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 261
    .line 262
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 263
    .line 264
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 265
    .line 266
    mul-double v9, v9, v2

    .line 267
    .line 268
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 269
    .line 270
    mul-double v7, v7, v5

    .line 271
    .line 272
    iput-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 273
    .line 274
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 275
    .line 276
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 277
    .line 278
    mul-double v9, v9, v2

    .line 279
    .line 280
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 281
    .line 282
    mul-double v7, v7, v5

    .line 283
    .line 284
    iput-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 285
    .line 286
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 287
    .line 288
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 289
    .line 290
    mul-double v9, v9, v2

    .line 291
    .line 292
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 293
    .line 294
    mul-double v7, v7, v5

    .line 295
    .line 296
    iput-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 297
    .line 298
    const/4 v1, -0x1

    .line 299
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 300
    .line 301
    return-void

    .line 302
    :pswitch_b
    or-int/lit8 v3, v2, 0x2

    .line 303
    .line 304
    iput v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 305
    .line 306
    :pswitch_c
    iget-wide v5, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 307
    .line 308
    iget-wide v7, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 309
    .line 310
    and-int/lit8 v1, v2, 0x4

    .line 311
    .line 312
    if-eqz v1, :cond_0

    .line 313
    .line 314
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 315
    .line 316
    mul-double v9, v9, v5

    .line 317
    .line 318
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 319
    .line 320
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 321
    .line 322
    mul-double v9, v9, v7

    .line 323
    .line 324
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 325
    .line 326
    and-int/lit8 v1, v2, 0x2

    .line 327
    .line 328
    if-eqz v1, :cond_1

    .line 329
    .line 330
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 331
    .line 332
    mul-double v9, v9, v5

    .line 333
    .line 334
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 335
    .line 336
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 337
    .line 338
    mul-double v9, v9, v7

    .line 339
    .line 340
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 341
    .line 342
    goto :goto_4

    .line 343
    :cond_0
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 344
    .line 345
    mul-double v9, v9, v5

    .line 346
    .line 347
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 348
    .line 349
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 350
    .line 351
    mul-double v9, v9, v7

    .line 352
    .line 353
    iput-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 354
    .line 355
    :cond_1
    :goto_4
    and-int/lit8 v1, v2, 0x1

    .line 356
    .line 357
    if-eqz v1, :cond_2

    .line 358
    .line 359
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 360
    .line 361
    mul-double v1, v1, v5

    .line 362
    .line 363
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 364
    .line 365
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 366
    .line 367
    mul-double v1, v1, v7

    .line 368
    .line 369
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 370
    .line 371
    :cond_2
    const/4 v1, -0x1

    .line 372
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 373
    .line 374
    return-void

    .line 375
    :pswitch_d
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 376
    .line 377
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 378
    .line 379
    add-double/2addr v2, v4

    .line 380
    iput-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 381
    .line 382
    iget-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 383
    .line 384
    iget-wide v4, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 385
    .line 386
    add-double/2addr v2, v4

    .line 387
    iput-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 388
    .line 389
    return-void

    .line 390
    :pswitch_e
    iget-wide v3, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 391
    .line 392
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 393
    .line 394
    iget-wide v3, v1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 395
    .line 396
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 397
    .line 398
    or-int/lit8 v1, v2, 0x1

    .line 399
    .line 400
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 401
    .line 402
    iget v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 403
    .line 404
    or-int/lit8 v1, v1, 0x1

    .line 405
    .line 406
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 407
    .line 408
    :pswitch_f
    return-void

    .line 409
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_e
        :pswitch_d
        :pswitch_e
        :pswitch_d
        :pswitch_e
        :pswitch_d
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
    .end packed-switch

    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    :pswitch_data_1
    .packed-switch 0x20
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_a
        :pswitch_a
    .end packed-switch

    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
.end method

.method public quadrantRotate(I)V
    .locals 2

    const/4 v0, 0x3

    and-int/2addr p1, v0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate270()V

    goto :goto_0

    .line 2
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate180()V

    goto :goto_0

    .line 3
    :cond_2
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate90()V

    :goto_0
    return-void
.end method

.method public quadrantRotate(IDD)V
    .locals 10

    const/4 v0, 0x3

    and-int/2addr p1, v0

    if-eqz p1, :cond_4

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v2, 0x2

    if-eq p1, v2, :cond_1

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    add-double v8, v4, v6

    mul-double v8, v8, p2

    sub-double/2addr v6, v4

    mul-double v6, v6, p4

    add-double/2addr v8, v6

    add-double/2addr v2, v8

    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 5
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    add-double v8, v4, v6

    mul-double p2, p2, v8

    sub-double/2addr v6, v4

    mul-double p4, p4, v6

    add-double/2addr p2, p4

    add-double/2addr v2, p2

    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate270()V

    goto :goto_0

    .line 7
    :cond_1
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    add-double/2addr v4, v4

    mul-double v4, v4, p2

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    add-double/2addr v6, v6

    mul-double v6, v6, p4

    add-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 8
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    add-double/2addr v4, v4

    mul-double p2, p2, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    add-double/2addr v4, v4

    mul-double p4, p4, v4

    add-double/2addr p2, p4

    add-double/2addr v2, p2

    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 9
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate180()V

    goto :goto_0

    .line 10
    :cond_2
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    sub-double v8, v4, v6

    mul-double v8, v8, p2

    add-double/2addr v6, v4

    mul-double v6, v6, p4

    add-double/2addr v8, v6

    add-double/2addr v2, v8

    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 11
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    sub-double v8, v4, v6

    mul-double p2, p2, v8

    add-double/2addr v6, v4

    mul-double p4, p4, v6

    add-double/2addr p2, p4

    add-double/2addr v2, p2

    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 12
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate90()V

    .line 13
    :goto_0
    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    const-wide/16 p3, 0x0

    cmpl-double p5, p1, p3

    if-nez p5, :cond_3

    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    cmpl-double p5, p1, p3

    if-nez p5, :cond_3

    .line 14
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    and-int/lit8 p1, p1, -0x2

    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    goto :goto_1

    .line 15
    :cond_3
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    or-int/2addr p1, v1

    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    :cond_4
    :goto_1
    return-void
.end method

.method public rotate(D)V
    .locals 10

    .line 1
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v0, v2

    if-nez v4, :cond_0

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate90()V

    goto :goto_0

    :cond_0
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v6, v0, v4

    if-nez v6, :cond_1

    .line 3
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate270()V

    goto :goto_0

    .line 4
    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide p1

    cmpl-double v6, p1, v4

    if-nez v6, :cond_2

    .line 5
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate180()V

    goto :goto_0

    :cond_2
    cmpl-double v4, p1, v2

    if-eqz v4, :cond_3

    .line 6
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 7
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v6, p1, v2

    mul-double v8, v0, v4

    add-double/2addr v6, v8

    .line 8
    iput-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    neg-double v6, v0

    mul-double v2, v2, v6

    mul-double v4, v4, p1

    add-double/2addr v2, v4

    .line 9
    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 10
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 11
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v8, p1, v2

    mul-double v0, v0, v4

    add-double/2addr v8, v0

    .line 12
    iput-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v6, v6, v2

    mul-double p1, p1, v4

    add-double/2addr v6, p1

    .line 13
    iput-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 14
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    :cond_3
    :goto_0
    return-void
.end method

.method public rotate(DD)V
    .locals 8

    const-wide/16 v0, 0x0

    cmpl-double v2, p3, v0

    if-nez v2, :cond_0

    cmpg-double p3, p1, v0

    if-gez p3, :cond_3

    .line 18
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate180()V

    goto :goto_0

    :cond_0
    cmpl-double v3, p1, v0

    if-nez v3, :cond_2

    if-lez v2, :cond_1

    .line 19
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate90()V

    goto :goto_0

    .line 20
    :cond_1
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate270()V

    goto :goto_0

    :cond_2
    mul-double v0, p1, p1

    mul-double v2, p3, p3

    add-double/2addr v0, v2

    .line 21
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    div-double/2addr p3, v0

    div-double/2addr p1, v0

    .line 22
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 23
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v4, p1, v0

    mul-double v6, p3, v2

    add-double/2addr v4, v6

    .line 24
    iput-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    neg-double v4, p3

    mul-double v0, v0, v4

    mul-double v2, v2, p1

    add-double/2addr v0, v2

    .line 25
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 26
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 27
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v6, p1, v0

    mul-double p3, p3, v2

    add-double/2addr v6, p3

    .line 28
    iput-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v4, v4, v0

    mul-double p1, p1, v2

    add-double/2addr v4, p1

    .line 29
    iput-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 30
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    :cond_3
    :goto_0
    return-void
.end method

.method public rotate(DDD)V
    .locals 0

    .line 15
    invoke-virtual {p0, p3, p4, p5, p6}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate(D)V

    neg-double p1, p3

    neg-double p3, p5

    .line 17
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    return-void
.end method

.method public rotate(DDDD)V
    .locals 0

    .line 31
    invoke-virtual {p0, p5, p6, p7, p8}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    .line 32
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/AffineTransform;->rotate(DD)V

    neg-double p1, p5

    neg-double p3, p7

    .line 33
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/AffineTransform;->translate(DD)V

    return-void
.end method

.method public scale(DD)V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, -0x1

    .line 5
    const/4 v3, 0x1

    .line 6
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 7
    .line 8
    packed-switch v0, :pswitch_data_0

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 12
    .line 13
    .line 14
    goto :goto_2

    .line 15
    :pswitch_0
    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 16
    .line 17
    mul-double v6, v6, p1

    .line 18
    .line 19
    iput-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 20
    .line 21
    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 22
    .line 23
    mul-double p1, p1, p3

    .line 24
    .line 25
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 26
    .line 27
    cmpl-double p3, v6, v4

    .line 28
    .line 29
    if-nez p3, :cond_1

    .line 30
    .line 31
    cmpl-double p3, p1, v4

    .line 32
    .line 33
    if-nez p3, :cond_1

    .line 34
    .line 35
    and-int/lit8 p1, v0, 0x1

    .line 36
    .line 37
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 38
    .line 39
    if-nez p1, :cond_0

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const/4 v1, 0x1

    .line 43
    :goto_0
    iput v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_1
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 47
    .line 48
    :goto_1
    return-void

    .line 49
    :pswitch_1
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 50
    .line 51
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 52
    .line 53
    cmpl-double v1, p1, v4

    .line 54
    .line 55
    if-nez v1, :cond_2

    .line 56
    .line 57
    cmpl-double p1, p3, v4

    .line 58
    .line 59
    if-eqz p1, :cond_3

    .line 60
    .line 61
    :cond_2
    or-int/lit8 p1, v0, 0x2

    .line 62
    .line 63
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 64
    .line 65
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 66
    .line 67
    :cond_3
    return-void

    .line 68
    :goto_2
    :pswitch_2
    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 69
    .line 70
    mul-double v6, v6, p1

    .line 71
    .line 72
    iput-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 73
    .line 74
    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 75
    .line 76
    mul-double v6, v6, p3

    .line 77
    .line 78
    iput-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 79
    .line 80
    :pswitch_3
    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 81
    .line 82
    mul-double v6, v6, p3

    .line 83
    .line 84
    iput-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 85
    .line 86
    iget-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 87
    .line 88
    mul-double p3, p3, p1

    .line 89
    .line 90
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 91
    .line 92
    const-wide/16 p1, 0x0

    .line 93
    .line 94
    cmpl-double v8, v6, p1

    .line 95
    .line 96
    if-nez v8, :cond_6

    .line 97
    .line 98
    cmpl-double v6, p3, p1

    .line 99
    .line 100
    if-nez v6, :cond_6

    .line 101
    .line 102
    and-int/lit8 p1, v0, 0x1

    .line 103
    .line 104
    iget-wide p2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 105
    .line 106
    cmpl-double p4, p2, v4

    .line 107
    .line 108
    if-nez p4, :cond_5

    .line 109
    .line 110
    iget-wide p2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 111
    .line 112
    cmpl-double p4, p2, v4

    .line 113
    .line 114
    if-nez p4, :cond_5

    .line 115
    .line 116
    if-nez p1, :cond_4

    .line 117
    .line 118
    goto :goto_3

    .line 119
    :cond_4
    const/4 v1, 0x1

    .line 120
    :goto_3
    iput v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 121
    .line 122
    goto :goto_4

    .line 123
    :cond_5
    or-int/lit8 p1, p1, 0x2

    .line 124
    .line 125
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 126
    .line 127
    :goto_4
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 128
    .line 129
    :cond_6
    return-void

    .line 130
    nop

    .line 131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
    .end packed-switch
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public setToIdentity()V
    .locals 2

    .line 1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 2
    .line 3
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 4
    .line 5
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 6
    .line 7
    const-wide/16 v0, 0x0

    .line 8
    .line 9
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 10
    .line 11
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 12
    .line 13
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 14
    .line 15
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 19
    .line 20
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setToQuadrantRotation(I)V
    .locals 10

    const/4 v0, 0x3

    and-int/2addr p1, v0

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    const-wide/16 v3, 0x0

    if-eqz p1, :cond_3

    const/4 v5, 0x1

    const/4 v6, 0x4

    const/16 v7, 0x8

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    if-eq p1, v5, :cond_2

    const/4 v5, 0x2

    if-eq p1, v5, :cond_1

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1
    :cond_0
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 2
    iput-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 3
    iput-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 4
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 5
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 6
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 7
    iput v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 8
    iput v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 9
    :cond_1
    iput-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 10
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 11
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 12
    iput-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 13
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 14
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 15
    iput v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 16
    iput v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 17
    :cond_2
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 18
    iput-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 19
    iput-wide v8, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 20
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 21
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 22
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 23
    iput v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 24
    iput v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 25
    :cond_3
    iput-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 26
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 27
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 28
    iput-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 29
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 30
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    const/4 p1, 0x0

    .line 31
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 32
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    :goto_0
    return-void
.end method

.method public setToQuadrantRotation(IDD)V
    .locals 14

    move-object v0, p0

    const/4 v1, 0x3

    and-int/lit8 v2, p1, 0x3

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v5, 0x0

    if-eqz v2, :cond_6

    const/4 v7, 0x1

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, 0x8

    const/16 v11, 0x9

    const-wide/high16 v12, -0x4010000000000000L    # -1.0

    if-eq v2, v7, :cond_4

    const/4 v7, 0x2

    if-eq v2, v7, :cond_2

    if-eq v2, v1, :cond_0

    goto/16 :goto_0

    .line 33
    :cond_0
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 34
    iput-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 35
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 36
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    sub-double v1, p2, p4

    .line 37
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double v3, p4, p2

    .line 38
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    cmpl-double v7, v1, v5

    if-nez v7, :cond_1

    cmpl-double v1, v3, v5

    if-nez v1, :cond_1

    .line 39
    iput v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 40
    iput v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 41
    :cond_1
    iput v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 42
    iput v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 43
    :cond_2
    iput-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 44
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 45
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 46
    iput-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    add-double v2, p2, p2

    .line 47
    iput-wide v2, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double v8, p4, p4

    .line 48
    iput-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    cmpl-double v4, v2, v5

    if-nez v4, :cond_3

    cmpl-double v2, v8, v5

    if-nez v2, :cond_3

    .line 49
    iput v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 50
    iput v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 51
    :cond_3
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 52
    iput v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 53
    :cond_4
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 54
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 55
    iput-wide v12, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 56
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    add-double v1, p2, p4

    .line 57
    iput-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    sub-double v3, p4, p2

    .line 58
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    cmpl-double v7, v1, v5

    if-nez v7, :cond_5

    cmpl-double v1, v3, v5

    if-nez v1, :cond_5

    .line 59
    iput v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 60
    iput v10, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 61
    :cond_5
    iput v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 62
    iput v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    .line 63
    :cond_6
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 64
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 65
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 66
    iput-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 67
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 68
    iput-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    const/4 v1, 0x0

    .line 69
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 70
    iput v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    :goto_0
    return-void
.end method

.method public setToRotation(D)V
    .locals 10

    .line 1
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    const/16 v2, 0x8

    const-wide/16 v3, 0x0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    cmpl-double v7, v0, v5

    if-eqz v7, :cond_3

    const-wide/high16 v7, -0x4010000000000000L    # -1.0

    cmpl-double v9, v0, v7

    if-nez v9, :cond_0

    goto :goto_1

    .line 2
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide p1

    cmpl-double v9, p1, v7

    if-nez v9, :cond_1

    const/4 v0, 0x2

    .line 3
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 4
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    :goto_0
    move-wide v0, v3

    goto :goto_2

    :cond_1
    cmpl-double v2, p1, v5

    if-nez v2, :cond_2

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 6
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    :cond_2
    const/4 v2, 0x6

    .line 7
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    const/16 v2, 0x10

    .line 8
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p1, 0x4

    .line 9
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 10
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    move-wide p1, v3

    .line 11
    :goto_2
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 12
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    neg-double v0, v0

    .line 13
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 14
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 15
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 16
    iput-wide v3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    return-void
.end method

.method public setToRotation(DD)V
    .locals 11

    const/16 v0, 0x8

    const-wide/high16 v1, -0x4010000000000000L    # -1.0

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v5, 0x0

    cmpl-double v7, p3, v5

    if-nez v7, :cond_1

    cmpg-double p3, p1, v5

    if-gez p3, :cond_0

    const/4 p1, 0x2

    .line 24
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 25
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 26
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 27
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    move-wide v1, v3

    :goto_0
    move-wide p1, v5

    goto :goto_1

    :cond_1
    cmpl-double v8, p1, v5

    if-nez v8, :cond_3

    if-lez v7, :cond_2

    move-wide v1, v3

    :cond_2
    const/4 p1, 0x4

    .line 28
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 29
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    move-wide p1, v1

    move-wide v1, v5

    goto :goto_1

    :cond_3
    mul-double v0, p1, p1

    mul-double v2, p3, p3

    add-double/2addr v0, v2

    .line 30
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    div-double/2addr p1, v0

    div-double v1, p3, v0

    const/4 p3, 0x6

    .line 31
    iput p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    const/16 p3, 0x10

    .line 32
    iput p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    move-wide v9, p1

    move-wide p1, v1

    move-wide v1, v9

    .line 33
    :goto_1
    iput-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 34
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    neg-double p1, p1

    .line 35
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 36
    iput-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 37
    iput-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 38
    iput-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    return-void
.end method

.method public setToRotation(DDD)V
    .locals 6

    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToRotation(D)V

    .line 18
    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 19
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    sub-double/2addr v0, v2

    mul-double v2, p3, v0

    mul-double v4, p5, p1

    add-double/2addr v2, v4

    .line 20
    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    mul-double p5, p5, v0

    mul-double p3, p3, p1

    sub-double/2addr p5, p3

    .line 21
    iput-wide p5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    const-wide/16 p1, 0x0

    cmpl-double p3, v2, p1

    if-nez p3, :cond_0

    cmpl-double p3, p5, p1

    if-eqz p3, :cond_1

    .line 22
    :cond_0
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 23
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    :cond_1
    return-void
.end method

.method public setToRotation(DDDD)V
    .locals 4

    .line 39
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/office/java/awt/geom/AffineTransform;->setToRotation(DD)V

    .line 40
    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    const-wide/high16 p3, 0x3ff0000000000000L    # 1.0

    .line 41
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    sub-double/2addr p3, v0

    mul-double v0, p5, p3

    mul-double v2, p7, p1

    add-double/2addr v0, v2

    .line 42
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    mul-double p7, p7, p3

    mul-double p5, p5, p1

    sub-double/2addr p7, p5

    .line 43
    iput-wide p7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    const-wide/16 p1, 0x0

    cmpl-double p3, v0, p1

    if-nez p3, :cond_0

    cmpl-double p3, p7, p1

    if-eqz p3, :cond_1

    .line 44
    :cond_0
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 45
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    :cond_1
    return-void
.end method

.method public setToScale(DD)V
    .locals 3

    .line 1
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 2
    .line 3
    const-wide/16 v0, 0x0

    .line 4
    .line 5
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 6
    .line 7
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 8
    .line 9
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 10
    .line 11
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 12
    .line 13
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 14
    .line 15
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 16
    .line 17
    cmpl-double v2, p1, v0

    .line 18
    .line 19
    if-nez v2, :cond_1

    .line 20
    .line 21
    cmpl-double p1, p3, v0

    .line 22
    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const/4 p1, 0x0

    .line 27
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 28
    .line 29
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    :goto_0
    const/4 p1, 0x2

    .line 33
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 34
    .line 35
    const/4 p1, -0x1

    .line 36
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 37
    .line 38
    :goto_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setToShear(DD)V
    .locals 3

    .line 1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 2
    .line 3
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 4
    .line 5
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 6
    .line 7
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 8
    .line 9
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 10
    .line 11
    const-wide/16 v0, 0x0

    .line 12
    .line 13
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 14
    .line 15
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 16
    .line 17
    cmpl-double v2, p1, v0

    .line 18
    .line 19
    if-nez v2, :cond_1

    .line 20
    .line 21
    cmpl-double p1, p3, v0

    .line 22
    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const/4 p1, 0x0

    .line 27
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 28
    .line 29
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    :goto_0
    const/4 p1, 0x6

    .line 33
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 34
    .line 35
    const/4 p1, -0x1

    .line 36
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 37
    .line 38
    :goto_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setToTranslation(DD)V
    .locals 4

    .line 1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 2
    .line 3
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 4
    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 8
    .line 9
    iput-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 10
    .line 11
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 12
    .line 13
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 14
    .line 15
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 16
    .line 17
    cmpl-double v0, p1, v2

    .line 18
    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    cmpl-double p1, p3, v2

    .line 22
    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const/4 p1, 0x0

    .line 27
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 28
    .line 29
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 33
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 34
    .line 35
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 36
    .line 37
    :goto_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public setTransform(DDDDDD)V
    .locals 0

    .line 9
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 10
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 11
    iput-wide p5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 12
    iput-wide p7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 13
    iput-wide p9, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 14
    iput-wide p11, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 15
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    return-void
.end method

.method public setTransform(Lcom/intsig/office/java/awt/geom/AffineTransform;)V
    .locals 2

    .line 1
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 2
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 3
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 4
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 5
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 6
    iget-wide v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 7
    iget v0, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 8
    iget p1, p1, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    return-void
.end method

.method public shear(DD)V
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    const-wide/16 v2, 0x0

    .line 5
    .line 6
    packed-switch v0, :pswitch_data_0

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :pswitch_0
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 14
    .line 15
    mul-double v4, v4, p3

    .line 16
    .line 17
    iput-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 18
    .line 19
    iget-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 20
    .line 21
    mul-double p3, p3, p1

    .line 22
    .line 23
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 24
    .line 25
    cmpl-double p1, v4, v2

    .line 26
    .line 27
    if-nez p1, :cond_0

    .line 28
    .line 29
    cmpl-double p1, p3, v2

    .line 30
    .line 31
    if-eqz p1, :cond_1

    .line 32
    .line 33
    :cond_0
    or-int/lit8 p1, v0, 0x2

    .line 34
    .line 35
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 36
    .line 37
    :cond_1
    iput v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 38
    .line 39
    return-void

    .line 40
    :pswitch_1
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 41
    .line 42
    mul-double v4, v4, p1

    .line 43
    .line 44
    iput-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 45
    .line 46
    iget-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 47
    .line 48
    mul-double p1, p1, p3

    .line 49
    .line 50
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 51
    .line 52
    cmpl-double p3, v4, v2

    .line 53
    .line 54
    if-nez p3, :cond_2

    .line 55
    .line 56
    cmpl-double p3, p1, v2

    .line 57
    .line 58
    if-eqz p3, :cond_3

    .line 59
    .line 60
    :cond_2
    or-int/lit8 p1, v0, 0x4

    .line 61
    .line 62
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 63
    .line 64
    :cond_3
    iput v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 65
    .line 66
    return-void

    .line 67
    :pswitch_2
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 68
    .line 69
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 70
    .line 71
    cmpl-double v4, p1, v2

    .line 72
    .line 73
    if-nez v4, :cond_4

    .line 74
    .line 75
    cmpl-double p1, p3, v2

    .line 76
    .line 77
    if-eqz p1, :cond_5

    .line 78
    .line 79
    :cond_4
    or-int/lit8 p1, v0, 0x2

    .line 80
    .line 81
    or-int/lit8 p1, p1, 0x4

    .line 82
    .line 83
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 84
    .line 85
    iput v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 86
    .line 87
    :cond_5
    return-void

    .line 88
    :goto_0
    :pswitch_3
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 89
    .line 90
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 91
    .line 92
    mul-double v4, v2, p3

    .line 93
    .line 94
    add-double/2addr v4, v0

    .line 95
    iput-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 96
    .line 97
    mul-double v0, v0, p1

    .line 98
    .line 99
    add-double/2addr v0, v2

    .line 100
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 101
    .line 102
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 103
    .line 104
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 105
    .line 106
    mul-double p3, p3, v2

    .line 107
    .line 108
    add-double/2addr p3, v0

    .line 109
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 110
    .line 111
    mul-double v0, v0, p1

    .line 112
    .line 113
    add-double/2addr v0, v2

    .line 114
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 115
    .line 116
    invoke-virtual {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->updateState()V

    .line 117
    .line 118
    .line 119
    return-void

    .line 120
    nop

    .line 121
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "AffineTransform[["

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 12
    .line 13
    invoke-static {v1, v2}, Lcom/intsig/office/java/awt/geom/AffineTransform;->_matround(D)D

    .line 14
    .line 15
    .line 16
    move-result-wide v1

    .line 17
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v1, ", "

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 26
    .line 27
    invoke-static {v2, v3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->_matround(D)D

    .line 28
    .line 29
    .line 30
    move-result-wide v2

    .line 31
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 38
    .line 39
    invoke-static {v2, v3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->_matround(D)D

    .line 40
    .line 41
    .line 42
    move-result-wide v2

    .line 43
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v2, "], ["

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 52
    .line 53
    invoke-static {v2, v3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->_matround(D)D

    .line 54
    .line 55
    .line 56
    move-result-wide v2

    .line 57
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 64
    .line 65
    invoke-static {v2, v3}, Lcom/intsig/office/java/awt/geom/AffineTransform;->_matround(D)D

    .line 66
    .line 67
    .line 68
    move-result-wide v2

    .line 69
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    iget-wide v1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 76
    .line 77
    invoke-static {v1, v2}, Lcom/intsig/office/java/awt/geom/AffineTransform;->_matround(D)D

    .line 78
    .line 79
    .line 80
    move-result-wide v1

    .line 81
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    const-string v1, "]]"

    .line 85
    .line 86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    return-object v0
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public transform(Lcom/intsig/office/java/awt/geom/Point2D;Lcom/intsig/office/java/awt/geom/Point2D;)Lcom/intsig/office/java/awt/geom/Point2D;
    .locals 8

    if-nez p2, :cond_1

    .line 1
    instance-of p2, p1, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    if-eqz p2, :cond_0

    .line 2
    new-instance p2, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    invoke-direct {p2}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>()V

    goto :goto_0

    .line 3
    :cond_0
    new-instance p2, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    invoke-direct {p2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>()V

    .line 4
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v2

    .line 6
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    packed-switch p1, :pswitch_data_0

    .line 7
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto :goto_1

    .line 8
    :pswitch_0
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v4, v4, v0

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v6, v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v0, v0, v6

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v2, v2, v6

    add-double/2addr v0, v2

    invoke-virtual {p2, v4, v5, v0, v1}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 9
    :pswitch_1
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v2, v2, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v2, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v0, v0, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v0, v4

    invoke-virtual {p2, v2, v3, v0, v1}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 10
    :pswitch_2
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v2, v2, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v0, v0, v4

    invoke-virtual {p2, v2, v3, v0, v1}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 11
    :pswitch_3
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v0, v0, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v0, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v2, v2, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v2, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 12
    :pswitch_4
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v0, v0, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v2, v2, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 13
    :pswitch_5
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v0, v4

    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v2, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 14
    :pswitch_6
    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    .line 15
    :goto_1
    :pswitch_7
    iget-wide v4, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v4, v4, v0

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v6, v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v4, v6

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v0, v0, v6

    iget-wide v6, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v2, v2, v6

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v0, v2

    invoke-virtual {p2, v4, v5, v0, v1}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    return-object p2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public transform([DI[DII)V
    .locals 27

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    if-ne v3, v1, :cond_0

    if-le v4, v2, :cond_0

    mul-int/lit8 v5, p5, 0x2

    add-int v6, v2, v5

    if-ge v4, v6, :cond_0

    .line 82
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v4

    .line 83
    :cond_0
    iget v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    packed-switch v5, :pswitch_data_0

    .line 84
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto/16 :goto_6

    .line 85
    :pswitch_0
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 86
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 87
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 88
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move v13, v2

    move/from16 v2, p5

    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_1

    add-int/lit8 v14, v13, 0x1

    .line 89
    aget-wide v15, v1, v13

    add-int/lit8 v13, v14, 0x1

    .line 90
    aget-wide v17, v1, v14

    add-int/lit8 v14, v4, 0x1

    mul-double v19, v5, v15

    mul-double v21, v7, v17

    add-double v19, v19, v21

    .line 91
    aput-wide v19, v3, v4

    add-int/lit8 v4, v14, 0x1

    mul-double v15, v15, v9

    mul-double v17, v17, v11

    add-double v15, v15, v17

    .line 92
    aput-wide v15, v3, v14

    goto :goto_0

    :cond_1
    return-void

    .line 93
    :pswitch_1
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 94
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 95
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 96
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move v13, v2

    move/from16 v2, p5

    :goto_1
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_2

    add-int/lit8 v14, v13, 0x1

    .line 97
    aget-wide v15, v1, v13

    add-int/lit8 v13, v4, 0x1

    add-int/lit8 v17, v14, 0x1

    .line 98
    aget-wide v18, v1, v14

    mul-double v18, v18, v5

    add-double v18, v18, v7

    aput-wide v18, v3, v4

    add-int/lit8 v4, v13, 0x1

    mul-double v15, v15, v9

    add-double/2addr v15, v11

    .line 99
    aput-wide v15, v3, v13

    move/from16 v13, v17

    goto :goto_1

    :cond_2
    return-void

    .line 100
    :pswitch_2
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 101
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    move v9, v2

    move/from16 v2, p5

    :goto_2
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_3

    add-int/lit8 v10, v9, 0x1

    .line 102
    aget-wide v11, v1, v9

    add-int/lit8 v9, v4, 0x1

    add-int/lit8 v13, v10, 0x1

    .line 103
    aget-wide v14, v1, v10

    mul-double v14, v14, v5

    aput-wide v14, v3, v4

    add-int/lit8 v4, v9, 0x1

    mul-double v11, v11, v7

    .line 104
    aput-wide v11, v3, v9

    move v9, v13

    goto :goto_2

    :cond_3
    return-void

    .line 105
    :pswitch_3
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 106
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 107
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 108
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move v13, v2

    move/from16 v2, p5

    :goto_3
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_4

    add-int/lit8 v14, v4, 0x1

    add-int/lit8 v15, v13, 0x1

    .line 109
    aget-wide v16, v1, v13

    mul-double v16, v16, v5

    add-double v16, v16, v7

    aput-wide v16, v3, v4

    add-int/lit8 v4, v14, 0x1

    add-int/lit8 v13, v15, 0x1

    .line 110
    aget-wide v15, v1, v15

    mul-double v15, v15, v9

    add-double/2addr v15, v11

    aput-wide v15, v3, v14

    goto :goto_3

    :cond_4
    return-void

    .line 111
    :pswitch_4
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 112
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move v9, v2

    move/from16 v2, p5

    :goto_4
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_5

    add-int/lit8 v10, v4, 0x1

    add-int/lit8 v11, v9, 0x1

    .line 113
    aget-wide v12, v1, v9

    mul-double v12, v12, v5

    aput-wide v12, v3, v4

    add-int/lit8 v4, v10, 0x1

    add-int/lit8 v9, v11, 0x1

    .line 114
    aget-wide v11, v1, v11

    mul-double v11, v11, v7

    aput-wide v11, v3, v10

    goto :goto_4

    :cond_5
    return-void

    .line 115
    :pswitch_5
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 116
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move v9, v2

    move/from16 v2, p5

    :goto_5
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_6

    add-int/lit8 v10, v4, 0x1

    add-int/lit8 v11, v9, 0x1

    .line 117
    aget-wide v12, v1, v9

    add-double/2addr v12, v5

    aput-wide v12, v3, v4

    add-int/lit8 v4, v10, 0x1

    add-int/lit8 v9, v11, 0x1

    .line 118
    aget-wide v11, v1, v11

    add-double/2addr v11, v7

    aput-wide v11, v3, v10

    goto :goto_5

    :cond_6
    return-void

    :pswitch_6
    if-ne v1, v3, :cond_7

    if-eq v2, v4, :cond_8

    :cond_7
    mul-int/lit8 v5, p5, 0x2

    .line 119
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    return-void

    .line 120
    :goto_6
    :pswitch_7
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 121
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 122
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 123
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 124
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move-wide v15, v13

    .line 125
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v17, v2

    move/from16 v2, p5

    :goto_7
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_9

    add-int/lit8 v18, v17, 0x1

    .line 126
    aget-wide v19, v1, v17

    add-int/lit8 v17, v18, 0x1

    .line 127
    aget-wide v21, v1, v18

    add-int/lit8 v18, v4, 0x1

    mul-double v23, v5, v19

    mul-double v25, v7, v21

    add-double v23, v23, v25

    add-double v23, v23, v9

    .line 128
    aput-wide v23, v3, v4

    add-int/lit8 v4, v18, 0x1

    mul-double v19, v19, v11

    mul-double v21, v21, v15

    add-double v19, v19, v21

    add-double v19, v19, v13

    .line 129
    aput-wide v19, v3, v18

    goto :goto_7

    :cond_9
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public transform([DI[FII)V
    .locals 25

    move-object/from16 v0, p0

    .line 178
    iget v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    packed-switch v1, :pswitch_data_0

    .line 179
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto/16 :goto_7

    .line 180
    :pswitch_0
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 181
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 182
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 183
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move/from16 v9, p2

    move/from16 v10, p4

    move/from16 v11, p5

    :goto_0
    add-int/lit8 v11, v11, -0x1

    if-ltz v11, :cond_0

    add-int/lit8 v12, v9, 0x1

    .line 184
    aget-wide v13, p1, v9

    add-int/lit8 v9, v12, 0x1

    .line 185
    aget-wide v15, p1, v12

    add-int/lit8 v12, v10, 0x1

    mul-double v17, v1, v13

    mul-double v19, v3, v15

    move-wide/from16 v21, v1

    add-double v1, v17, v19

    double-to-float v1, v1

    .line 186
    aput v1, p3, v10

    add-int/lit8 v10, v12, 0x1

    mul-double v13, v13, v5

    mul-double v15, v15, v7

    add-double/2addr v13, v15

    double-to-float v1, v13

    .line 187
    aput v1, p3, v12

    move-wide/from16 v1, v21

    goto :goto_0

    :cond_0
    return-void

    .line 188
    :pswitch_1
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 189
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 190
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 191
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v9, p2

    move/from16 v10, p4

    move/from16 v11, p5

    :goto_1
    add-int/lit8 v11, v11, -0x1

    if-ltz v11, :cond_1

    add-int/lit8 v12, v9, 0x1

    .line 192
    aget-wide v13, p1, v9

    add-int/lit8 v9, v10, 0x1

    add-int/lit8 v15, v12, 0x1

    .line 193
    aget-wide v16, p1, v12

    mul-double v16, v16, v1

    move-wide/from16 v18, v1

    add-double v1, v16, v3

    double-to-float v1, v1

    aput v1, p3, v10

    add-int/lit8 v10, v9, 0x1

    mul-double v13, v13, v5

    add-double/2addr v13, v7

    double-to-float v1, v13

    .line 194
    aput v1, p3, v9

    move v9, v15

    move-wide/from16 v1, v18

    goto :goto_1

    :cond_1
    return-void

    .line 195
    :pswitch_2
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 196
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    move/from16 v5, p2

    move/from16 v6, p4

    move/from16 v7, p5

    :goto_2
    add-int/lit8 v7, v7, -0x1

    if-ltz v7, :cond_2

    add-int/lit8 v8, v5, 0x1

    .line 197
    aget-wide v9, p1, v5

    add-int/lit8 v5, v6, 0x1

    add-int/lit8 v11, v8, 0x1

    .line 198
    aget-wide v12, p1, v8

    mul-double v12, v12, v1

    double-to-float v8, v12

    aput v8, p3, v6

    add-int/lit8 v6, v5, 0x1

    mul-double v9, v9, v3

    double-to-float v8, v9

    .line 199
    aput v8, p3, v5

    move v5, v11

    goto :goto_2

    :cond_2
    return-void

    .line 200
    :pswitch_3
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 201
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 202
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 203
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v9, p2

    move/from16 v10, p4

    move/from16 v11, p5

    :goto_3
    add-int/lit8 v11, v11, -0x1

    if-ltz v11, :cond_3

    add-int/lit8 v12, v10, 0x1

    add-int/lit8 v13, v9, 0x1

    .line 204
    aget-wide v14, p1, v9

    mul-double v14, v14, v1

    add-double/2addr v14, v3

    double-to-float v9, v14

    aput v9, p3, v10

    add-int/lit8 v10, v12, 0x1

    add-int/lit8 v9, v13, 0x1

    .line 205
    aget-wide v13, p1, v13

    mul-double v13, v13, v5

    add-double/2addr v13, v7

    double-to-float v13, v13

    aput v13, p3, v12

    goto :goto_3

    :cond_3
    return-void

    .line 206
    :pswitch_4
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 207
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move/from16 v5, p2

    move/from16 v6, p4

    move/from16 v7, p5

    :goto_4
    add-int/lit8 v7, v7, -0x1

    if-ltz v7, :cond_4

    add-int/lit8 v8, v6, 0x1

    add-int/lit8 v9, v5, 0x1

    .line 208
    aget-wide v10, p1, v5

    mul-double v10, v10, v1

    double-to-float v5, v10

    aput v5, p3, v6

    add-int/lit8 v6, v8, 0x1

    add-int/lit8 v5, v9, 0x1

    .line 209
    aget-wide v9, p1, v9

    mul-double v9, v9, v3

    double-to-float v9, v9

    aput v9, p3, v8

    goto :goto_4

    :cond_4
    return-void

    .line 210
    :pswitch_5
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 211
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v5, p2

    move/from16 v6, p4

    move/from16 v7, p5

    :goto_5
    add-int/lit8 v7, v7, -0x1

    if-ltz v7, :cond_5

    add-int/lit8 v8, v6, 0x1

    add-int/lit8 v9, v5, 0x1

    .line 212
    aget-wide v10, p1, v5

    add-double/2addr v10, v1

    double-to-float v5, v10

    aput v5, p3, v6

    add-int/lit8 v6, v8, 0x1

    add-int/lit8 v5, v9, 0x1

    .line 213
    aget-wide v9, p1, v9

    add-double/2addr v9, v3

    double-to-float v9, v9

    aput v9, p3, v8

    goto :goto_5

    :cond_5
    return-void

    :pswitch_6
    move/from16 v1, p2

    move/from16 v2, p4

    move/from16 v3, p5

    :goto_6
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_6

    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v1, 0x1

    .line 214
    aget-wide v6, p1, v1

    double-to-float v1, v6

    aput v1, p3, v2

    add-int/lit8 v2, v4, 0x1

    add-int/lit8 v1, v5, 0x1

    .line 215
    aget-wide v5, p1, v5

    double-to-float v5, v5

    aput v5, p3, v4

    goto :goto_6

    :cond_6
    return-void

    .line 216
    :goto_7
    :pswitch_7
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 217
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 218
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 219
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 220
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 221
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v13, p2

    move/from16 v14, p4

    move/from16 v15, p5

    :goto_8
    add-int/lit8 v15, v15, -0x1

    if-ltz v15, :cond_7

    add-int/lit8 v16, v13, 0x1

    .line 222
    aget-wide v17, p1, v13

    add-int/lit8 v13, v16, 0x1

    .line 223
    aget-wide v19, p1, v16

    add-int/lit8 v16, v14, 0x1

    mul-double v21, v1, v17

    mul-double v23, v3, v19

    add-double v21, v21, v23

    move-wide/from16 v23, v1

    add-double v0, v21, v5

    double-to-float v0, v0

    .line 224
    aput v0, p3, v14

    add-int/lit8 v14, v16, 0x1

    mul-double v17, v17, v7

    mul-double v19, v19, v9

    add-double v17, v17, v19

    add-double v0, v17, v11

    double-to-float v0, v0

    .line 225
    aput v0, p3, v16

    move-object/from16 v0, p0

    move-wide/from16 v1, v23

    goto :goto_8

    :cond_7
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public transform([FI[DII)V
    .locals 26

    move-object/from16 v0, p0

    .line 130
    iget v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    packed-switch v1, :pswitch_data_0

    .line 131
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto/16 :goto_7

    .line 132
    :pswitch_0
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 133
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 134
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 135
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move/from16 v9, p2

    move/from16 v10, p4

    move/from16 v11, p5

    :goto_0
    add-int/lit8 v11, v11, -0x1

    if-ltz v11, :cond_0

    add-int/lit8 v12, v9, 0x1

    .line 136
    aget v9, p1, v9

    float-to-double v13, v9

    add-int/lit8 v9, v12, 0x1

    .line 137
    aget v12, p1, v12

    move/from16 p2, v11

    float-to-double v11, v12

    add-int/lit8 v15, v10, 0x1

    mul-double v16, v1, v13

    mul-double v18, v3, v11

    add-double v16, v16, v18

    .line 138
    aput-wide v16, p3, v10

    add-int/lit8 v10, v15, 0x1

    mul-double v13, v13, v5

    mul-double v11, v11, v7

    add-double/2addr v13, v11

    .line 139
    aput-wide v13, p3, v15

    move/from16 v11, p2

    goto :goto_0

    :cond_0
    return-void

    .line 140
    :pswitch_1
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 141
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 142
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 143
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v9, p2

    move/from16 v10, p4

    move/from16 v11, p5

    :goto_1
    add-int/lit8 v11, v11, -0x1

    if-ltz v11, :cond_1

    add-int/lit8 v12, v9, 0x1

    .line 144
    aget v9, p1, v9

    float-to-double v13, v9

    add-int/lit8 v9, v10, 0x1

    add-int/lit8 v15, v12, 0x1

    .line 145
    aget v12, p1, v12

    move/from16 p2, v11

    float-to-double v11, v12

    mul-double v11, v11, v1

    add-double/2addr v11, v3

    aput-wide v11, p3, v10

    add-int/lit8 v10, v9, 0x1

    mul-double v13, v13, v5

    add-double/2addr v13, v7

    .line 146
    aput-wide v13, p3, v9

    move/from16 v11, p2

    move v9, v15

    goto :goto_1

    :cond_1
    return-void

    .line 147
    :pswitch_2
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 148
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    move/from16 v5, p2

    move/from16 v6, p4

    move/from16 v7, p5

    :goto_2
    add-int/lit8 v7, v7, -0x1

    if-ltz v7, :cond_2

    add-int/lit8 v8, v5, 0x1

    .line 149
    aget v5, p1, v5

    float-to-double v9, v5

    add-int/lit8 v5, v6, 0x1

    add-int/lit8 v11, v8, 0x1

    .line 150
    aget v8, p1, v8

    float-to-double v12, v8

    mul-double v12, v12, v1

    aput-wide v12, p3, v6

    add-int/lit8 v6, v5, 0x1

    mul-double v9, v9, v3

    .line 151
    aput-wide v9, p3, v5

    move v5, v11

    goto :goto_2

    :cond_2
    return-void

    .line 152
    :pswitch_3
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 153
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 154
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 155
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v9, p2

    move/from16 v10, p4

    move/from16 v11, p5

    :goto_3
    add-int/lit8 v11, v11, -0x1

    if-ltz v11, :cond_3

    add-int/lit8 v12, v10, 0x1

    add-int/lit8 v13, v9, 0x1

    .line 156
    aget v9, p1, v9

    float-to-double v14, v9

    mul-double v14, v14, v1

    add-double/2addr v14, v3

    aput-wide v14, p3, v10

    add-int/lit8 v10, v12, 0x1

    add-int/lit8 v9, v13, 0x1

    .line 157
    aget v13, p1, v13

    float-to-double v13, v13

    mul-double v13, v13, v5

    add-double/2addr v13, v7

    aput-wide v13, p3, v12

    goto :goto_3

    :cond_3
    return-void

    .line 158
    :pswitch_4
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 159
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move/from16 v5, p2

    move/from16 v6, p4

    move/from16 v7, p5

    :goto_4
    add-int/lit8 v7, v7, -0x1

    if-ltz v7, :cond_4

    add-int/lit8 v8, v6, 0x1

    add-int/lit8 v9, v5, 0x1

    .line 160
    aget v5, p1, v5

    float-to-double v10, v5

    mul-double v10, v10, v1

    aput-wide v10, p3, v6

    add-int/lit8 v6, v8, 0x1

    add-int/lit8 v5, v9, 0x1

    .line 161
    aget v9, p1, v9

    float-to-double v9, v9

    mul-double v9, v9, v3

    aput-wide v9, p3, v8

    goto :goto_4

    :cond_4
    return-void

    .line 162
    :pswitch_5
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 163
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v5, p2

    move/from16 v6, p4

    move/from16 v7, p5

    :goto_5
    add-int/lit8 v7, v7, -0x1

    if-ltz v7, :cond_5

    add-int/lit8 v8, v6, 0x1

    add-int/lit8 v9, v5, 0x1

    .line 164
    aget v5, p1, v5

    float-to-double v10, v5

    add-double/2addr v10, v1

    aput-wide v10, p3, v6

    add-int/lit8 v6, v8, 0x1

    add-int/lit8 v5, v9, 0x1

    .line 165
    aget v9, p1, v9

    float-to-double v9, v9

    add-double/2addr v9, v3

    aput-wide v9, p3, v8

    goto :goto_5

    :cond_5
    return-void

    :pswitch_6
    move/from16 v1, p2

    move/from16 v2, p4

    move/from16 v3, p5

    :goto_6
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_6

    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v1, 0x1

    .line 166
    aget v1, p1, v1

    float-to-double v6, v1

    aput-wide v6, p3, v2

    add-int/lit8 v2, v4, 0x1

    add-int/lit8 v1, v5, 0x1

    .line 167
    aget v5, p1, v5

    float-to-double v5, v5

    aput-wide v5, p3, v4

    goto :goto_6

    :cond_6
    return-void

    .line 168
    :goto_7
    :pswitch_7
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 169
    iget-wide v3, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 170
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 171
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 172
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 173
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v13, p2

    move/from16 v14, p4

    move/from16 v15, p5

    :goto_8
    add-int/lit8 v15, v15, -0x1

    if-ltz v15, :cond_7

    add-int/lit8 v16, v13, 0x1

    .line 174
    aget v13, p1, v13

    move-wide/from16 v17, v11

    float-to-double v11, v13

    add-int/lit8 v13, v16, 0x1

    .line 175
    aget v0, p1, v16

    move-wide/from16 v20, v9

    float-to-double v9, v0

    add-int/lit8 v0, v14, 0x1

    mul-double v22, v1, v11

    mul-double v24, v3, v9

    add-double v22, v22, v24

    add-double v22, v22, v5

    .line 176
    aput-wide v22, p3, v14

    add-int/lit8 v14, v0, 0x1

    mul-double v11, v11, v7

    mul-double v9, v9, v20

    add-double/2addr v11, v9

    add-double v11, v11, v17

    .line 177
    aput-wide v11, p3, v0

    move-object/from16 v0, p0

    move-wide/from16 v11, v17

    move-wide/from16 v9, v20

    goto :goto_8

    :cond_7
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public transform([FI[FII)V
    .locals 25

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    if-ne v3, v1, :cond_0

    if-le v4, v2, :cond_0

    mul-int/lit8 v5, p5, 0x2

    add-int v6, v2, v5

    if-ge v4, v6, :cond_0

    .line 34
    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v4

    .line 35
    :cond_0
    iget v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    packed-switch v5, :pswitch_data_0

    move-object v10, v1

    .line 36
    invoke-direct/range {p0 .. p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto/16 :goto_6

    :pswitch_0
    move-object v10, v1

    goto/16 :goto_6

    .line 37
    :pswitch_1
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 38
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 39
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 40
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move v13, v2

    move/from16 v2, p5

    :goto_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_1

    add-int/lit8 v14, v13, 0x1

    .line 41
    aget v13, v1, v13

    move-wide v15, v11

    float-to-double v11, v13

    add-int/lit8 v13, v14, 0x1

    .line 42
    aget v14, v1, v14

    move/from16 p2, v13

    float-to-double v13, v14

    add-int/lit8 v17, v4, 0x1

    mul-double v18, v5, v11

    mul-double v20, v7, v13

    move-wide/from16 v22, v5

    add-double v5, v18, v20

    double-to-float v5, v5

    .line 43
    aput v5, v3, v4

    add-int/lit8 v4, v17, 0x1

    mul-double v11, v11, v9

    mul-double v5, v15, v13

    add-double/2addr v11, v5

    double-to-float v5, v11

    .line 44
    aput v5, v3, v17

    move/from16 v13, p2

    move-wide v11, v15

    move-wide/from16 v5, v22

    goto :goto_0

    :cond_1
    return-void

    .line 45
    :pswitch_2
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 46
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 47
    iget-wide v9, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 48
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move v13, v2

    move/from16 v2, p5

    :goto_1
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_2

    add-int/lit8 v14, v13, 0x1

    .line 49
    aget v13, v1, v13

    move-wide v15, v11

    float-to-double v11, v13

    add-int/lit8 v13, v4, 0x1

    add-int/lit8 v17, v14, 0x1

    .line 50
    aget v14, v1, v14

    move/from16 p2, v2

    float-to-double v1, v14

    mul-double v1, v1, v5

    add-double/2addr v1, v7

    double-to-float v1, v1

    aput v1, v3, v4

    add-int/lit8 v4, v13, 0x1

    mul-double v11, v11, v9

    add-double/2addr v11, v15

    double-to-float v1, v11

    .line 51
    aput v1, v3, v13

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-wide v11, v15

    move/from16 v13, v17

    goto :goto_1

    :cond_2
    return-void

    .line 52
    :pswitch_3
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 53
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    move/from16 v1, p5

    :goto_2
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_3

    add-int/lit8 v9, v2, 0x1

    move-object/from16 v10, p1

    .line 54
    aget v2, v10, v2

    float-to-double v11, v2

    add-int/lit8 v2, v4, 0x1

    add-int/lit8 v13, v9, 0x1

    .line 55
    aget v9, v10, v9

    float-to-double v14, v9

    mul-double v14, v14, v5

    double-to-float v9, v14

    aput v9, v3, v4

    add-int/lit8 v4, v2, 0x1

    mul-double v11, v11, v7

    double-to-float v9, v11

    .line 56
    aput v9, v3, v2

    move v2, v13

    goto :goto_2

    :cond_3
    return-void

    :pswitch_4
    move-object v10, v1

    .line 57
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 58
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 59
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 60
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v1, p5

    :goto_3
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_4

    add-int/lit8 v9, v4, 0x1

    add-int/lit8 v15, v2, 0x1

    .line 61
    aget v2, v10, v2

    move/from16 p2, v1

    float-to-double v1, v2

    mul-double v1, v1, v5

    add-double/2addr v1, v7

    double-to-float v1, v1

    aput v1, v3, v4

    add-int/lit8 v4, v9, 0x1

    add-int/lit8 v2, v15, 0x1

    .line 62
    aget v1, v10, v15

    move/from16 p4, v2

    float-to-double v1, v1

    mul-double v1, v1, v11

    add-double/2addr v1, v13

    double-to-float v1, v1

    aput v1, v3, v9

    move/from16 v1, p2

    move/from16 v2, p4

    goto :goto_3

    :cond_4
    return-void

    :pswitch_5
    move-object v10, v1

    .line 63
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 64
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move/from16 v1, p5

    :goto_4
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_5

    add-int/lit8 v9, v4, 0x1

    add-int/lit8 v11, v2, 0x1

    .line 65
    aget v2, v10, v2

    float-to-double v12, v2

    mul-double v12, v12, v5

    double-to-float v2, v12

    aput v2, v3, v4

    add-int/lit8 v4, v9, 0x1

    add-int/lit8 v2, v11, 0x1

    .line 66
    aget v11, v10, v11

    float-to-double v11, v11

    mul-double v11, v11, v7

    double-to-float v11, v11

    aput v11, v3, v9

    goto :goto_4

    :cond_5
    return-void

    :pswitch_6
    move-object v10, v1

    .line 67
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 68
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v1, p5

    :goto_5
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_6

    add-int/lit8 v9, v4, 0x1

    add-int/lit8 v11, v2, 0x1

    .line 69
    aget v2, v10, v2

    float-to-double v12, v2

    add-double/2addr v12, v5

    double-to-float v2, v12

    aput v2, v3, v4

    add-int/lit8 v4, v9, 0x1

    add-int/lit8 v2, v11, 0x1

    .line 70
    aget v11, v10, v11

    float-to-double v11, v11

    add-double/2addr v11, v7

    double-to-float v11, v11

    aput v11, v3, v9

    goto :goto_5

    :cond_6
    return-void

    :pswitch_7
    move-object v10, v1

    if-ne v10, v3, :cond_7

    if-eq v2, v4, :cond_8

    :cond_7
    mul-int/lit8 v1, p5, 0x2

    .line 71
    invoke-static {v10, v2, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    return-void

    .line 72
    :goto_6
    iget-wide v5, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 73
    iget-wide v7, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 74
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 75
    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    move v9, v2

    .line 76
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    move-wide v15, v1

    .line 77
    iget-wide v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    move/from16 v17, v9

    move/from16 v9, p5

    :goto_7
    add-int/lit8 v9, v9, -0x1

    if-ltz v9, :cond_9

    add-int/lit8 v18, v17, 0x1

    .line 78
    aget v0, v10, v17

    move-wide/from16 v19, v1

    float-to-double v0, v0

    add-int/lit8 v17, v18, 0x1

    .line 79
    aget v2, v10, v18

    move/from16 p2, v9

    float-to-double v9, v2

    add-int/lit8 v2, v4, 0x1

    mul-double v21, v5, v0

    mul-double v23, v7, v9

    add-double v21, v21, v23

    move-wide/from16 v23, v5

    add-double v5, v21, v11

    double-to-float v5, v5

    .line 80
    aput v5, v3, v4

    add-int/lit8 v4, v2, 0x1

    mul-double v0, v0, v13

    mul-double v5, v15, v9

    add-double/2addr v0, v5

    add-double v0, v0, v19

    double-to-float v0, v0

    .line 81
    aput v0, v3, v2

    move-object/from16 v0, p0

    move-object/from16 v10, p1

    move/from16 v9, p2

    move-wide/from16 v1, v19

    move-wide/from16 v5, v23

    goto :goto_7

    :cond_9
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public transform([Lcom/intsig/office/java/awt/geom/Point2D;I[Lcom/intsig/office/java/awt/geom/Point2D;II)V
    .locals 15

    move-object v0, p0

    .line 16
    iget v1, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    move/from16 v2, p2

    move/from16 v3, p4

    move/from16 v4, p5

    :goto_0
    add-int/lit8 v4, v4, -0x1

    if-ltz v4, :cond_2

    add-int/lit8 v5, v2, 0x1

    .line 17
    aget-object v2, p1, v2

    .line 18
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/Point2D;->getX()D

    move-result-wide v6

    .line 19
    invoke-virtual {v2}, Lcom/intsig/office/java/awt/geom/Point2D;->getY()D

    move-result-wide v8

    add-int/lit8 v10, v3, 0x1

    .line 20
    aget-object v3, p3, v3

    if-nez v3, :cond_1

    .line 21
    instance-of v2, v2, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    if-eqz v2, :cond_0

    .line 22
    new-instance v2, Lcom/intsig/office/java/awt/geom/Point2D$Double;

    invoke-direct {v2}, Lcom/intsig/office/java/awt/geom/Point2D$Double;-><init>()V

    goto :goto_1

    .line 23
    :cond_0
    new-instance v2, Lcom/intsig/office/java/awt/geom/Point2D$Float;

    invoke-direct {v2}, Lcom/intsig/office/java/awt/geom/Point2D$Float;-><init>()V

    :goto_1
    move-object v3, v2

    add-int/lit8 v2, v10, -0x1

    .line 24
    aput-object v3, p3, v2

    :cond_1
    packed-switch v1, :pswitch_data_0

    .line 25
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    goto :goto_2

    .line 26
    :pswitch_0
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v11, v11, v6

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v13, v13, v8

    add-double/2addr v11, v13

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v6, v6, v13

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v8, v8, v13

    add-double/2addr v6, v8

    invoke-virtual {v3, v11, v12, v6, v7}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    goto :goto_3

    .line 27
    :pswitch_1
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v8, v8, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v8, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v6, v6, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v6, v11

    invoke-virtual {v3, v8, v9, v6, v7}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    goto :goto_3

    .line 28
    :pswitch_2
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v8, v8, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v6, v6, v11

    invoke-virtual {v3, v8, v9, v6, v7}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    goto :goto_3

    .line 29
    :pswitch_3
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v6, v6, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v6, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v8, v8, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v8, v11

    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    goto :goto_3

    .line 30
    :pswitch_4
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v6, v6, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v8, v8, v11

    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    goto :goto_3

    .line 31
    :pswitch_5
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v6, v11

    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v8, v11

    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    goto :goto_3

    .line 32
    :pswitch_6
    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    goto :goto_3

    .line 33
    :goto_2
    :pswitch_7
    iget-wide v11, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    mul-double v11, v11, v6

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    mul-double v13, v13, v8

    add-double/2addr v11, v13

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    add-double/2addr v11, v13

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    mul-double v6, v6, v13

    iget-wide v13, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    mul-double v8, v8, v13

    add-double/2addr v6, v8

    iget-wide v8, v0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    add-double/2addr v6, v8

    invoke-virtual {v3, v11, v12, v6, v7}, Lcom/intsig/office/java/awt/geom/Point2D;->setLocation(DD)V

    :goto_3
    move v2, v5

    move v3, v10

    goto/16 :goto_0

    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public translate(DD)V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    const/4 v2, 0x1

    .line 5
    const-wide/16 v3, 0x0

    .line 6
    .line 7
    packed-switch v0, :pswitch_data_0

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/office/java/awt/geom/AffineTransform;->stateError()V

    .line 11
    .line 12
    .line 13
    goto/16 :goto_0

    .line 14
    .line 15
    :pswitch_0
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 16
    .line 17
    mul-double v0, v0, p1

    .line 18
    .line 19
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 20
    .line 21
    mul-double v5, v5, p3

    .line 22
    .line 23
    add-double/2addr v0, v5

    .line 24
    iput-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 25
    .line 26
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 27
    .line 28
    mul-double p1, p1, v5

    .line 29
    .line 30
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 31
    .line 32
    mul-double p3, p3, v5

    .line 33
    .line 34
    add-double/2addr p1, p3

    .line 35
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 36
    .line 37
    cmpl-double p3, v0, v3

    .line 38
    .line 39
    if-nez p3, :cond_0

    .line 40
    .line 41
    cmpl-double p3, p1, v3

    .line 42
    .line 43
    if-eqz p3, :cond_1

    .line 44
    .line 45
    :cond_0
    const/4 p1, 0x7

    .line 46
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 47
    .line 48
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 49
    .line 50
    or-int/2addr p1, v2

    .line 51
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 52
    .line 53
    :cond_1
    return-void

    .line 54
    :pswitch_1
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 55
    .line 56
    mul-double p3, p3, v5

    .line 57
    .line 58
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 59
    .line 60
    add-double/2addr p3, v5

    .line 61
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 62
    .line 63
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 64
    .line 65
    mul-double p1, p1, v5

    .line 66
    .line 67
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 68
    .line 69
    add-double/2addr p1, v5

    .line 70
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 71
    .line 72
    cmpl-double v0, p3, v3

    .line 73
    .line 74
    if-nez v0, :cond_2

    .line 75
    .line 76
    cmpl-double p3, p1, v3

    .line 77
    .line 78
    if-nez p3, :cond_2

    .line 79
    .line 80
    const/4 p1, 0x4

    .line 81
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 82
    .line 83
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 84
    .line 85
    if-eq p1, v1, :cond_2

    .line 86
    .line 87
    sub-int/2addr p1, v2

    .line 88
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 89
    .line 90
    :cond_2
    return-void

    .line 91
    :pswitch_2
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 92
    .line 93
    mul-double p3, p3, v0

    .line 94
    .line 95
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 96
    .line 97
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 98
    .line 99
    mul-double p1, p1, v0

    .line 100
    .line 101
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 102
    .line 103
    cmpl-double v0, p3, v3

    .line 104
    .line 105
    if-nez v0, :cond_3

    .line 106
    .line 107
    cmpl-double p3, p1, v3

    .line 108
    .line 109
    if-eqz p3, :cond_4

    .line 110
    .line 111
    :cond_3
    const/4 p1, 0x5

    .line 112
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 113
    .line 114
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 115
    .line 116
    or-int/2addr p1, v2

    .line 117
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 118
    .line 119
    :cond_4
    return-void

    .line 120
    :pswitch_3
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 121
    .line 122
    mul-double p1, p1, v5

    .line 123
    .line 124
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 125
    .line 126
    add-double/2addr p1, v5

    .line 127
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 128
    .line 129
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 130
    .line 131
    mul-double p3, p3, v5

    .line 132
    .line 133
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 134
    .line 135
    add-double/2addr p3, v5

    .line 136
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 137
    .line 138
    cmpl-double v0, p1, v3

    .line 139
    .line 140
    if-nez v0, :cond_5

    .line 141
    .line 142
    cmpl-double p1, p3, v3

    .line 143
    .line 144
    if-nez p1, :cond_5

    .line 145
    .line 146
    const/4 p1, 0x2

    .line 147
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 148
    .line 149
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 150
    .line 151
    if-eq p1, v1, :cond_5

    .line 152
    .line 153
    sub-int/2addr p1, v2

    .line 154
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 155
    .line 156
    :cond_5
    return-void

    .line 157
    :pswitch_4
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 158
    .line 159
    mul-double p1, p1, v0

    .line 160
    .line 161
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 162
    .line 163
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 164
    .line 165
    mul-double p3, p3, v0

    .line 166
    .line 167
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 168
    .line 169
    cmpl-double v0, p1, v3

    .line 170
    .line 171
    if-nez v0, :cond_6

    .line 172
    .line 173
    cmpl-double p1, p3, v3

    .line 174
    .line 175
    if-eqz p1, :cond_7

    .line 176
    .line 177
    :cond_6
    const/4 p1, 0x3

    .line 178
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 179
    .line 180
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 181
    .line 182
    or-int/2addr p1, v2

    .line 183
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 184
    .line 185
    :cond_7
    return-void

    .line 186
    :pswitch_5
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 187
    .line 188
    add-double/2addr p1, v0

    .line 189
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 190
    .line 191
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 192
    .line 193
    add-double/2addr p3, v0

    .line 194
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 195
    .line 196
    cmpl-double v0, p1, v3

    .line 197
    .line 198
    if-nez v0, :cond_8

    .line 199
    .line 200
    cmpl-double p1, p3, v3

    .line 201
    .line 202
    if-nez p1, :cond_8

    .line 203
    .line 204
    const/4 p1, 0x0

    .line 205
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 206
    .line 207
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 208
    .line 209
    :cond_8
    return-void

    .line 210
    :pswitch_6
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 211
    .line 212
    iput-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 213
    .line 214
    cmpl-double v0, p1, v3

    .line 215
    .line 216
    if-nez v0, :cond_9

    .line 217
    .line 218
    cmpl-double p1, p3, v3

    .line 219
    .line 220
    if-eqz p1, :cond_a

    .line 221
    .line 222
    :cond_9
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 223
    .line 224
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 225
    .line 226
    :cond_a
    return-void

    .line 227
    :goto_0
    :pswitch_7
    iget-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 228
    .line 229
    mul-double v5, v5, p1

    .line 230
    .line 231
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 232
    .line 233
    mul-double v7, v7, p3

    .line 234
    .line 235
    add-double/2addr v5, v7

    .line 236
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 237
    .line 238
    add-double/2addr v5, v7

    .line 239
    iput-wide v5, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 240
    .line 241
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 242
    .line 243
    mul-double p1, p1, v7

    .line 244
    .line 245
    iget-wide v7, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 246
    .line 247
    mul-double p3, p3, v7

    .line 248
    .line 249
    add-double/2addr p1, p3

    .line 250
    iget-wide p3, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 251
    .line 252
    add-double/2addr p1, p3

    .line 253
    iput-wide p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 254
    .line 255
    cmpl-double p3, v5, v3

    .line 256
    .line 257
    if-nez p3, :cond_b

    .line 258
    .line 259
    cmpl-double p3, p1, v3

    .line 260
    .line 261
    if-nez p3, :cond_b

    .line 262
    .line 263
    const/4 p1, 0x6

    .line 264
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 265
    .line 266
    iget p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 267
    .line 268
    if-eq p1, v1, :cond_b

    .line 269
    .line 270
    sub-int/2addr p1, v2

    .line 271
    iput p1, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 272
    .line 273
    :cond_b
    return-void

    .line 274
    nop

    .line 275
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method updateState()V
    .locals 8

    .line 1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m01:D

    .line 2
    .line 3
    const/4 v2, -0x1

    .line 4
    const-wide/16 v3, 0x0

    .line 5
    .line 6
    cmpl-double v5, v0, v3

    .line 7
    .line 8
    if-nez v5, :cond_3

    .line 9
    .line 10
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m10:D

    .line 11
    .line 12
    cmpl-double v5, v0, v3

    .line 13
    .line 14
    if-nez v5, :cond_3

    .line 15
    .line 16
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 17
    .line 18
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 19
    .line 20
    cmpl-double v7, v0, v5

    .line 21
    .line 22
    if-nez v7, :cond_1

    .line 23
    .line 24
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 25
    .line 26
    cmpl-double v7, v0, v5

    .line 27
    .line 28
    if-nez v7, :cond_1

    .line 29
    .line 30
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 31
    .line 32
    cmpl-double v2, v0, v3

    .line 33
    .line 34
    if-nez v2, :cond_0

    .line 35
    .line 36
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 37
    .line 38
    cmpl-double v2, v0, v3

    .line 39
    .line 40
    if-nez v2, :cond_0

    .line 41
    .line 42
    const/4 v0, 0x0

    .line 43
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 44
    .line 45
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    const/4 v0, 0x1

    .line 49
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 50
    .line 51
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 55
    .line 56
    cmpl-double v5, v0, v3

    .line 57
    .line 58
    if-nez v5, :cond_2

    .line 59
    .line 60
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 61
    .line 62
    cmpl-double v5, v0, v3

    .line 63
    .line 64
    if-nez v5, :cond_2

    .line 65
    .line 66
    const/4 v0, 0x2

    .line 67
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 68
    .line 69
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_2
    const/4 v0, 0x3

    .line 73
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 74
    .line 75
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_3
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m00:D

    .line 79
    .line 80
    cmpl-double v5, v0, v3

    .line 81
    .line 82
    if-nez v5, :cond_5

    .line 83
    .line 84
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m11:D

    .line 85
    .line 86
    cmpl-double v5, v0, v3

    .line 87
    .line 88
    if-nez v5, :cond_5

    .line 89
    .line 90
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 91
    .line 92
    cmpl-double v5, v0, v3

    .line 93
    .line 94
    if-nez v5, :cond_4

    .line 95
    .line 96
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 97
    .line 98
    cmpl-double v5, v0, v3

    .line 99
    .line 100
    if-nez v5, :cond_4

    .line 101
    .line 102
    const/4 v0, 0x4

    .line 103
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 104
    .line 105
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_4
    const/4 v0, 0x5

    .line 109
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 110
    .line 111
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 112
    .line 113
    goto :goto_0

    .line 114
    :cond_5
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m02:D

    .line 115
    .line 116
    cmpl-double v5, v0, v3

    .line 117
    .line 118
    if-nez v5, :cond_6

    .line 119
    .line 120
    iget-wide v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->m12:D

    .line 121
    .line 122
    cmpl-double v5, v0, v3

    .line 123
    .line 124
    if-nez v5, :cond_6

    .line 125
    .line 126
    const/4 v0, 0x6

    .line 127
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 128
    .line 129
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 130
    .line 131
    goto :goto_0

    .line 132
    :cond_6
    const/4 v0, 0x7

    .line 133
    iput v0, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->state:I

    .line 134
    .line 135
    iput v2, p0, Lcom/intsig/office/java/awt/geom/AffineTransform;->type:I

    .line 136
    .line 137
    :goto_0
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

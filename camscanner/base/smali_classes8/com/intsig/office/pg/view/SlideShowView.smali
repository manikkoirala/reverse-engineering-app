.class public Lcom/intsig/office/pg/view/SlideShowView;
.super Ljava/lang/Object;
.source "SlideShowView.java"


# instance fields
.field private animDuration:I

.field private animShapeArea:Landroid/graphics/Rect;

.field private animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

.field private bgRect:Landroid/graphics/Rect;

.field private pageAnimation:Lcom/intsig/office/pg/animate/IAnimation;

.field private paint:Landroid/graphics/Paint;

.field private presentation:Lcom/intsig/office/pg/control/Presentation;

.field private shapeVisible:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/pg/animate/IAnimation;",
            ">;>;"
        }
    .end annotation
.end field

.field private slide:Lcom/intsig/office/pg/model/PGSlide;

.field private slideshowStep:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/pg/control/Presentation;Lcom/intsig/office/pg/model/PGSlide;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 6
    .line 7
    const/16 v0, 0x4b0

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->animDuration:I

    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 12
    .line 13
    iput-object p2, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 14
    .line 15
    new-instance p1, Landroid/graphics/Paint;

    .line 16
    .line 17
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->paint:Landroid/graphics/Paint;

    .line 21
    .line 22
    const/4 p2, 0x1

    .line 23
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->paint:Landroid/graphics/Paint;

    .line 27
    .line 28
    sget-object p2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    .line 29
    .line 30
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->paint:Landroid/graphics/Paint;

    .line 34
    .line 35
    const/high16 p2, 0x41c00000    # 24.0f

    .line 36
    .line 37
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 38
    .line 39
    .line 40
    new-instance p1, Landroid/graphics/Rect;

    .line 41
    .line 42
    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 43
    .line 44
    .line 45
    iput-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->bgRect:Landroid/graphics/Rect;

    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private removeAnimation()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    new-instance v0, Ljava/util/HashMap;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 15
    .line 16
    .line 17
    iput v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 18
    .line 19
    :goto_0
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 20
    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/pg/animate/AnimationManager;->stopAnimation()V

    .line 24
    .line 25
    .line 26
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/PGEditor;->clearAnimation()V

    .line 41
    .line 42
    .line 43
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 44
    .line 45
    if-eqz v0, :cond_3

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    :goto_1
    if-ge v1, v0, :cond_3

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 54
    .line 55
    invoke-virtual {v2, v1}, Lcom/intsig/office/pg/model/PGSlide;->getShape(I)Lcom/intsig/office/common/shape/IShape;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-direct {p0, v2}, Lcom/intsig/office/pg/view/SlideShowView;->removeShapeAnimation(Lcom/intsig/office/common/shape/IShape;)V

    .line 60
    .line 61
    .line 62
    add-int/lit8 v1, v1, 0x1

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_3
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private removeShapeAnimation(Lcom/intsig/office/common/shape/IShape;)V
    .locals 3

    .line 1
    instance-of v0, p1, Lcom/intsig/office/common/shape/GroupShape;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/office/common/shape/GroupShape;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    array-length v0, p1

    .line 12
    const/4 v1, 0x0

    .line 13
    :goto_0
    if-ge v1, v0, :cond_1

    .line 14
    .line 15
    aget-object v2, p1, v1

    .line 16
    .line 17
    invoke-direct {p0, v2}, Lcom/intsig/office/pg/view/SlideShowView;->removeShapeAnimation(Lcom/intsig/office/common/shape/IShape;)V

    .line 18
    .line 19
    .line 20
    add-int/lit8 v1, v1, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-interface {p1}, Lcom/intsig/office/common/shape/IShape;->getAnimation()Lcom/intsig/office/pg/animate/IAnimation;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    const/4 v1, 0x0

    .line 30
    invoke-interface {p1, v1}, Lcom/intsig/office/common/shape/IShape;->setAnimation(Lcom/intsig/office/pg/animate/IAnimation;)V

    .line 31
    .line 32
    .line 33
    invoke-interface {v0}, Lcom/intsig/office/pg/animate/IAnimation;->dispose()V

    .line 34
    .line 35
    .line 36
    :cond_1
    return-void
    .line 37
    .line 38
.end method

.method private setShapeAnimation(ILcom/intsig/office/pg/animate/IAnimation;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    .line 2
    iget-object v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    invoke-virtual {v2, v1}, Lcom/intsig/office/pg/model/PGSlide;->getShape(I)Lcom/intsig/office/common/shape/IShape;

    move-result-object v2

    .line 3
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    move-result v3

    if-eq v3, p1, :cond_0

    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getGroupShapeID()I

    move-result v3

    if-ne v3, p1, :cond_1

    :cond_0
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getAnimation()Lcom/intsig/office/pg/animate/IAnimation;

    move-result-object v3

    if-nez v3, :cond_1

    .line 4
    invoke-direct {p0, v2, p2}, Lcom/intsig/office/pg/view/SlideShowView;->setShapeAnimation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/pg/animate/IAnimation;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private setShapeAnimation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/pg/animate/IAnimation;)V
    .locals 3

    .line 5
    instance-of v0, p1, Lcom/intsig/office/common/shape/GroupShape;

    if-eqz v0, :cond_0

    .line 6
    check-cast p1, Lcom/intsig/office/common/shape/GroupShape;

    invoke-virtual {p1}, Lcom/intsig/office/common/shape/GroupShape;->getShapes()[Lcom/intsig/office/common/shape/IShape;

    move-result-object p1

    .line 7
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    .line 8
    invoke-direct {p0, v2, p2}, Lcom/intsig/office/pg/view/SlideShowView;->setShapeAnimation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/pg/animate/IAnimation;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9
    :cond_0
    invoke-interface {p1, p2}, Lcom/intsig/office/common/shape/IShape;->setAnimation(Lcom/intsig/office/pg/animate/IAnimation;)V

    :cond_1
    return-void
.end method

.method private updateShapeAnimation(ILcom/intsig/office/pg/animate/IAnimation;Z)V
    .locals 4

    .line 10
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    invoke-virtual {v0, p2}, Lcom/intsig/office/pg/animate/AnimationManager;->setAnimation(Lcom/intsig/office/pg/animate/IAnimation;)V

    .line 11
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    .line 12
    iget-object v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    invoke-virtual {v2, v1}, Lcom/intsig/office/pg/model/PGSlide;->getShape(I)Lcom/intsig/office/common/shape/IShape;

    move-result-object v2

    .line 13
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    move-result v3

    if-eq v3, p1, :cond_0

    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getGroupShapeID()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 14
    :cond_0
    invoke-direct {p0, v2, p2}, Lcom/intsig/office/pg/view/SlideShowView;->setShapeAnimation(Lcom/intsig/office/common/shape/IShape;Lcom/intsig/office/pg/animate/IAnimation;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    .line 15
    iget-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    const/16 p3, 0x3e8

    invoke-interface {p2}, Lcom/intsig/office/pg/animate/IAnimation;->getFPS()I

    move-result p2

    div-int/2addr p3, p2

    invoke-virtual {p1, p3}, Lcom/intsig/office/pg/animate/AnimationManager;->beginAnimation(I)V

    goto :goto_1

    .line 16
    :cond_3
    iget-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    invoke-virtual {p1}, Lcom/intsig/office/pg/animate/AnimationManager;->stopAnimation()V

    :goto_1
    return-void
.end method

.method private updateShapeAnimation(IZ)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getSlideShowAnimation()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    .line 2
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getShapeID()I

    move-result v0

    iget-object v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    invoke-virtual {v2}, Lcom/intsig/office/pg/control/Presentation;->getZoom()F

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/intsig/office/pg/view/SlideShowView;->updateShapeArea(IF)V

    .line 4
    invoke-virtual {p1}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getAnimationType()B

    move-result v0

    if-eq v0, v1, :cond_0

    .line 5
    new-instance v0, Lcom/intsig/office/pg/animate/FadeAnimation;

    iget v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animDuration:I

    invoke-direct {v0, p1, v1}, Lcom/intsig/office/pg/animate/FadeAnimation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;I)V

    goto :goto_0

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/office/pg/animate/EmphanceAnimation;

    iget v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animDuration:I

    invoke-direct {v0, p1, v1}, Lcom/intsig/office/pg/animate/EmphanceAnimation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;I)V

    .line 7
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getShapeID()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 8
    invoke-virtual {p1}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getParagraphBegin()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    invoke-virtual {p1}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getShapeID()I

    move-result p1

    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/office/pg/view/SlideShowView;->updateShapeAnimation(ILcom/intsig/office/pg/animate/IAnimation;Z)V

    :cond_1
    return-void
.end method

.method private updateShapeArea(IF)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, v0, :cond_2

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 11
    .line 12
    invoke-virtual {v2, v1}, Lcom/intsig/office/pg/model/PGSlide;->getShape(I)Lcom/intsig/office/common/shape/IShape;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    if-ne v3, p1, :cond_1

    .line 21
    .line 22
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    if-eqz v2, :cond_1

    .line 27
    .line 28
    iget p1, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 29
    .line 30
    int-to-float p1, p1

    .line 31
    mul-float p1, p1, p2

    .line 32
    .line 33
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    iget v0, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 38
    .line 39
    int-to-float v0, v0

    .line 40
    mul-float v0, v0, p2

    .line 41
    .line 42
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    iget v1, v2, Lcom/intsig/office/java/awt/Rectangle;->width:I

    .line 47
    .line 48
    int-to-float v1, v1

    .line 49
    mul-float v1, v1, p2

    .line 50
    .line 51
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->height:I

    .line 56
    .line 57
    int-to-float v2, v2

    .line 58
    mul-float v2, v2, p2

    .line 59
    .line 60
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 61
    .line 62
    .line 63
    move-result p2

    .line 64
    iget-object v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->animShapeArea:Landroid/graphics/Rect;

    .line 65
    .line 66
    if-nez v2, :cond_0

    .line 67
    .line 68
    new-instance v2, Landroid/graphics/Rect;

    .line 69
    .line 70
    add-int/2addr v1, p1

    .line 71
    add-int/2addr p2, v0

    .line 72
    invoke-direct {v2, p1, v0, v1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 73
    .line 74
    .line 75
    iput-object v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->animShapeArea:Landroid/graphics/Rect;

    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_0
    add-int/2addr v1, p1

    .line 79
    add-int/2addr p2, v0

    .line 80
    invoke-virtual {v2, p1, v0, v1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 81
    .line 82
    .line 83
    :goto_1
    return-void

    .line 84
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_2
    const/4 p1, 0x0

    .line 88
    iput-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animShapeArea:Landroid/graphics/Rect;

    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method


# virtual methods
.method public animationStoped()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/pg/animate/AnimationManager;->hasStoped()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0

    .line 10
    :cond_0
    const/4 v0, 0x1

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public changeSlide(Lcom/intsig/office/pg/model/PGSlide;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->paint:Landroid/graphics/Paint;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/office/pg/animate/AnimationManager;->dispose()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 16
    .line 17
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 25
    .line 26
    :cond_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public drawSlide(Landroid/graphics/Canvas;FLcom/intsig/office/system/beans/CalloutView/CalloutView;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v8, p1

    .line 4
    .line 5
    move-object/from16 v9, p3

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/office/pg/view/SlideShowView;->pageAnimation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 8
    .line 9
    const/4 v10, 0x2

    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    invoke-interface {v1}, Lcom/intsig/office/pg/animate/IAnimation;->getAnimationStatus()B

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eq v1, v10, :cond_1

    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/office/pg/view/SlideShowView;->pageAnimation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 19
    .line 20
    invoke-interface {v1}, Lcom/intsig/office/pg/animate/IAnimation;->getCurrentAnimationInfor()Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->getProgress()F

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    mul-float v1, v1, p2

    .line 29
    .line 30
    const v2, 0x3a83126f    # 0.001f

    .line 31
    .line 32
    .line 33
    cmpg-float v2, v1, v2

    .line 34
    .line 35
    if-gtz v2, :cond_0

    .line 36
    .line 37
    return-void

    .line 38
    :cond_0
    move v11, v1

    .line 39
    goto :goto_0

    .line 40
    :cond_1
    move/from16 v11, p2

    .line 41
    .line 42
    :goto_0
    iget-object v1, v0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 43
    .line 44
    invoke-virtual {v1}, Lcom/intsig/office/pg/control/Presentation;->getPageSize()Lcom/intsig/office/java/awt/Dimension;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    iget v2, v1, Lcom/intsig/office/java/awt/Dimension;->width:I

    .line 49
    .line 50
    int-to-float v2, v2

    .line 51
    mul-float v2, v2, v11

    .line 52
    .line 53
    float-to-int v12, v2

    .line 54
    iget v1, v1, Lcom/intsig/office/java/awt/Dimension;->height:I

    .line 55
    .line 56
    int-to-float v1, v1

    .line 57
    mul-float v1, v1, v11

    .line 58
    .line 59
    float-to-int v13, v1

    .line 60
    iget-object v1, v0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/intsig/office/pg/control/Presentation;->getmWidth()I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    sub-int/2addr v1, v12

    .line 67
    div-int/lit8 v14, v1, 0x2

    .line 68
    .line 69
    iget-object v1, v0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 70
    .line 71
    invoke-virtual {v1}, Lcom/intsig/office/pg/control/Presentation;->getmHeight()I

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    sub-int/2addr v1, v13

    .line 76
    div-int/lit8 v15, v1, 0x2

    .line 77
    .line 78
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 79
    .line 80
    .line 81
    int-to-float v1, v14

    .line 82
    int-to-float v2, v15

    .line 83
    invoke-virtual {v8, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 84
    .line 85
    .line 86
    const/4 v7, 0x0

    .line 87
    invoke-virtual {v8, v7, v7, v12, v13}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 88
    .line 89
    .line 90
    iget-object v1, v0, Lcom/intsig/office/pg/view/SlideShowView;->bgRect:Landroid/graphics/Rect;

    .line 91
    .line 92
    invoke-virtual {v1, v7, v7, v12, v13}, Landroid/graphics/Rect;->set(IIII)V

    .line 93
    .line 94
    .line 95
    invoke-static {}, Lcom/intsig/office/pg/view/SlideDrawKit;->instance()Lcom/intsig/office/pg/view/SlideDrawKit;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    iget-object v2, v0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 100
    .line 101
    invoke-virtual {v2}, Lcom/intsig/office/pg/control/Presentation;->getPGModel()Lcom/intsig/office/pg/model/PGModel;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    iget-object v2, v0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 106
    .line 107
    invoke-virtual {v2}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 108
    .line 109
    .line 110
    move-result-object v4

    .line 111
    iget-object v5, v0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 112
    .line 113
    iget-object v6, v0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 114
    .line 115
    move-object/from16 v2, p1

    .line 116
    .line 117
    move-object/from16 v16, v6

    .line 118
    .line 119
    move v6, v11

    .line 120
    move-object/from16 v7, v16

    .line 121
    .line 122
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/office/pg/view/SlideDrawKit;->drawSlide(Landroid/graphics/Canvas;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/control/PGEditor;Lcom/intsig/office/pg/model/PGSlide;FLjava/util/Map;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 126
    .line 127
    .line 128
    if-eqz v9, :cond_3

    .line 129
    .line 130
    iget-object v1, v0, Lcom/intsig/office/pg/view/SlideShowView;->pageAnimation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 131
    .line 132
    if-eqz v1, :cond_2

    .line 133
    .line 134
    invoke-interface {v1}, Lcom/intsig/office/pg/animate/IAnimation;->getAnimationStatus()B

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    if-eq v1, v10, :cond_2

    .line 139
    .line 140
    const/4 v1, 0x4

    .line 141
    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    .line 142
    .line 143
    .line 144
    goto :goto_1

    .line 145
    :cond_2
    invoke-virtual {v9, v11}, Lcom/intsig/office/system/beans/CalloutView/CalloutView;->setZoom(F)V

    .line 146
    .line 147
    .line 148
    add-int/2addr v12, v14

    .line 149
    add-int/2addr v13, v15

    .line 150
    invoke-virtual {v9, v14, v15, v12, v13}, Landroid/view/View;->layout(IIII)V

    .line 151
    .line 152
    .line 153
    const/4 v1, 0x0

    .line 154
    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    .line 155
    .line 156
    .line 157
    :cond_3
    :goto_1
    return-void
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public drawSlideForToPicture(Landroid/graphics/Canvas;FII)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-ne v1, p3, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eq v1, p4, :cond_1

    .line 16
    .line 17
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    int-to-float v1, v1

    .line 22
    int-to-float p3, p3

    .line 23
    div-float/2addr v1, p3

    .line 24
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    int-to-float p3, p3

    .line 29
    int-to-float p4, p4

    .line 30
    div-float/2addr p3, p4

    .line 31
    invoke-static {v1, p3}, Ljava/lang/Math;->min(FF)F

    .line 32
    .line 33
    .line 34
    move-result p3

    .line 35
    mul-float p2, p2, p3

    .line 36
    .line 37
    :cond_1
    move v5, p2

    .line 38
    invoke-static {}, Lcom/intsig/office/pg/view/SlideDrawKit;->instance()Lcom/intsig/office/pg/view/SlideDrawKit;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    iget-object p2, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 43
    .line 44
    invoke-virtual {p2}, Lcom/intsig/office/pg/control/Presentation;->getPGModel()Lcom/intsig/office/pg/model/PGModel;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    iget-object p2, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 49
    .line 50
    invoke-virtual {p2}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    iget-object v4, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 55
    .line 56
    iget-object v6, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 57
    .line 58
    move-object v1, p1

    .line 59
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/office/pg/view/SlideDrawKit;->drawSlide(Landroid/graphics/Canvas;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/control/PGEditor;Lcom/intsig/office/pg/model/PGSlide;FLjava/util/Map;)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public endSlideShow()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/pg/view/SlideShowView;->removeAnimation()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDrawingRect()Landroid/graphics/Rect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->bgRect:Landroid/graphics/Rect;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSlideshowToImage(Lcom/intsig/office/pg/model/PGSlide;I)Landroid/graphics/Bitmap;
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, p1, v0}, Lcom/intsig/office/pg/view/SlideShowView;->initSlideShow(Lcom/intsig/office/pg/model/PGSlide;Z)V

    .line 5
    .line 6
    .line 7
    :goto_0
    iget v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 8
    .line 9
    add-int/lit8 v2, p2, -0x1

    .line 10
    .line 11
    if-ge v1, v2, :cond_0

    .line 12
    .line 13
    add-int/lit8 v1, v1, 0x1

    .line 14
    .line 15
    iput v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 16
    .line 17
    invoke-direct {p0, v1, v0}, Lcom/intsig/office/pg/view/SlideShowView;->updateShapeAnimation(IZ)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-static {}, Lcom/intsig/office/pg/view/SlideDrawKit;->instance()Lcom/intsig/office/pg/view/SlideDrawKit;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getPGModel()Lcom/intsig/office/pg/model/PGModel;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 32
    .line 33
    invoke-virtual {v1}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    iget-object v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 38
    .line 39
    invoke-virtual {p2, v0, v1, p1, v2}, Lcom/intsig/office/pg/view/SlideDrawKit;->slideToImage(Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/pg/control/PGEditor;Lcom/intsig/office/pg/model/PGSlide;Ljava/util/Map;)Landroid/graphics/Bitmap;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-direct {p0}, Lcom/intsig/office/pg/view/SlideShowView;->removeAnimation()V

    .line 44
    .line 45
    .line 46
    return-object p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public gotoLastAction()V
    .locals 2

    .line 1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/office/pg/view/SlideShowView;->gotoNextSlide()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 8
    .line 9
    add-int/lit8 v0, v0, 0x1

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/pg/view/SlideShowView;->updateShapeAnimation(IZ)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public gotoNextSlide()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getSlideShowAnimation()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    iget v2, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-lt v2, v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v1, 0x0

    .line 20
    :cond_1
    :goto_0
    return v1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public gotopreviousSlide()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getSlideShowAnimation()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 11
    .line 12
    if-gtz v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v1, 0x0

    .line 16
    :cond_1
    :goto_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public initSlideShow(Lcom/intsig/office/pg/model/PGSlide;Z)V
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/pg/view/SlideShowView;->removeAnimation()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGSlide;->getSlideShowAnimation()Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v1, 0x0

    .line 14
    if-eqz v0, :cond_5

    .line 15
    .line 16
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    const/4 v3, 0x0

    .line 21
    :goto_0
    if-ge v3, v2, :cond_5

    .line 22
    .line 23
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v4

    .line 27
    check-cast v4, Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 28
    .line 29
    iget-object v5, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 30
    .line 31
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getShapeID()I

    .line 32
    .line 33
    .line 34
    move-result v6

    .line 35
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 36
    .line 37
    .line 38
    move-result-object v6

    .line 39
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v5

    .line 43
    check-cast v5, Ljava/util/Map;

    .line 44
    .line 45
    if-nez v5, :cond_1

    .line 46
    .line 47
    new-instance v5, Ljava/util/HashMap;

    .line 48
    .line 49
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 50
    .line 51
    .line 52
    iget-object v6, p0, Lcom/intsig/office/pg/view/SlideShowView;->shapeVisible:Ljava/util/Map;

    .line 53
    .line 54
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getShapeID()I

    .line 55
    .line 56
    .line 57
    move-result v7

    .line 58
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 59
    .line 60
    .line 61
    move-result-object v7

    .line 62
    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    :cond_1
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getParagraphBegin()I

    .line 66
    .line 67
    .line 68
    move-result v6

    .line 69
    :goto_1
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getParagraphEnd()I

    .line 70
    .line 71
    .line 72
    move-result v7

    .line 73
    if-gt v6, v7, :cond_4

    .line 74
    .line 75
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 76
    .line 77
    .line 78
    move-result-object v7

    .line 79
    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    .line 81
    .line 82
    move-result-object v7

    .line 83
    check-cast v7, Lcom/intsig/office/pg/animate/IAnimation;

    .line 84
    .line 85
    if-nez v7, :cond_3

    .line 86
    .line 87
    new-instance v6, Lcom/intsig/office/pg/animate/FadeAnimation;

    .line 88
    .line 89
    iget v7, p0, Lcom/intsig/office/pg/view/SlideShowView;->animDuration:I

    .line 90
    .line 91
    invoke-direct {v6, v4, v7}, Lcom/intsig/office/pg/animate/FadeAnimation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;I)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getParagraphBegin()I

    .line 95
    .line 96
    .line 97
    move-result v7

    .line 98
    :goto_2
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getParagraphEnd()I

    .line 99
    .line 100
    .line 101
    move-result v8

    .line 102
    if-gt v7, v8, :cond_2

    .line 103
    .line 104
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 105
    .line 106
    .line 107
    move-result-object v8

    .line 108
    invoke-interface {v5, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    add-int/lit8 v7, v7, 0x1

    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_2
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getShapeID()I

    .line 115
    .line 116
    .line 117
    move-result v4

    .line 118
    invoke-direct {p0, v4, v6}, Lcom/intsig/office/pg/view/SlideShowView;->setShapeAnimation(ILcom/intsig/office/pg/animate/IAnimation;)V

    .line 119
    .line 120
    .line 121
    goto :goto_3

    .line 122
    :cond_3
    add-int/lit8 v6, v6, 0x1

    .line 123
    .line 124
    goto :goto_1

    .line 125
    :cond_4
    :goto_3
    add-int/lit8 v3, v3, 0x1

    .line 126
    .line 127
    goto :goto_0

    .line 128
    :cond_5
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 129
    .line 130
    if-nez v0, :cond_6

    .line 131
    .line 132
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 133
    .line 134
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getControl()Lcom/intsig/office/system/IControl;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/intsig/office/system/SysKit;->getAnimationManager()Lcom/intsig/office/pg/animate/AnimationManager;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    iput-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 147
    .line 148
    :cond_6
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGSlide;->hasTransition()Z

    .line 149
    .line 150
    .line 151
    move-result p1

    .line 152
    if-eqz p1, :cond_9

    .line 153
    .line 154
    iget-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->pageAnimation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 155
    .line 156
    if-nez p1, :cond_7

    .line 157
    .line 158
    new-instance p1, Lcom/intsig/office/pg/animate/FadeAnimation;

    .line 159
    .line 160
    new-instance v0, Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 161
    .line 162
    const/4 v2, -0x3

    .line 163
    invoke-direct {v0, v2, v1}, Lcom/intsig/office/pg/animate/ShapeAnimation;-><init>(IB)V

    .line 164
    .line 165
    .line 166
    iget v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animDuration:I

    .line 167
    .line 168
    invoke-direct {p1, v0, v1}, Lcom/intsig/office/pg/animate/FadeAnimation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;I)V

    .line 169
    .line 170
    .line 171
    iput-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->pageAnimation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 172
    .line 173
    goto :goto_4

    .line 174
    :cond_7
    iget v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->animDuration:I

    .line 175
    .line 176
    invoke-interface {p1, v0}, Lcom/intsig/office/pg/animate/IAnimation;->setDuration(I)V

    .line 177
    .line 178
    .line 179
    :goto_4
    iget-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 180
    .line 181
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->pageAnimation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 182
    .line 183
    invoke-virtual {p1, v0}, Lcom/intsig/office/pg/animate/AnimationManager;->setAnimation(Lcom/intsig/office/pg/animate/IAnimation;)V

    .line 184
    .line 185
    .line 186
    if-eqz p2, :cond_8

    .line 187
    .line 188
    iget-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 189
    .line 190
    iget-object p2, p0, Lcom/intsig/office/pg/view/SlideShowView;->pageAnimation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 191
    .line 192
    invoke-interface {p2}, Lcom/intsig/office/pg/animate/IAnimation;->getFPS()I

    .line 193
    .line 194
    .line 195
    move-result p2

    .line 196
    const/16 v0, 0x3e8

    .line 197
    .line 198
    div-int/2addr v0, p2

    .line 199
    invoke-virtual {p1, v0}, Lcom/intsig/office/pg/animate/AnimationManager;->beginAnimation(I)V

    .line 200
    .line 201
    .line 202
    goto :goto_5

    .line 203
    :cond_8
    iget-object p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animationMgr:Lcom/intsig/office/pg/animate/AnimationManager;

    .line 204
    .line 205
    invoke-virtual {p1}, Lcom/intsig/office/pg/animate/AnimationManager;->stopAnimation()V

    .line 206
    .line 207
    .line 208
    :cond_9
    :goto_5
    return-void
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public isExitSlideShow()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public nextActionSlideShow()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    add-int/2addr v0, v1

    .line 5
    iput v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 6
    .line 7
    invoke-direct {p0, v0, v1}, Lcom/intsig/office/pg/view/SlideShowView;->updateShapeAnimation(IZ)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public previousActionSlideShow()V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, -0x1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slide:Lcom/intsig/office/pg/model/PGSlide;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-virtual {p0, v1, v2}, Lcom/intsig/office/pg/view/SlideShowView;->initSlideShow(Lcom/intsig/office/pg/model/PGSlide;Z)V

    .line 9
    .line 10
    .line 11
    :goto_0
    iget v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 12
    .line 13
    if-ge v1, v0, :cond_0

    .line 14
    .line 15
    add-int/lit8 v1, v1, 0x1

    .line 16
    .line 17
    iput v1, p0, Lcom/intsig/office/pg/view/SlideShowView;->slideshowStep:I

    .line 18
    .line 19
    invoke-direct {p0, v1, v2}, Lcom/intsig/office/pg/view/SlideShowView;->updateShapeAnimation(IZ)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setAnimationDuration(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/office/pg/view/SlideShowView;->animDuration:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.class public Lcom/intsig/office/pg/animate/Animation;
.super Ljava/lang/Object;
.source "Animation.java"

# interfaces
.implements Lcom/intsig/office/pg/animate/IAnimation;


# static fields
.field public static final AnimStatus_Animating:B = 0x1t

.field public static final AnimStatus_End:B = 0x2t

.field public static final AnimStatus_NotStarted:B = 0x0t

.field public static final Duration:I = 0x4b0

.field private static final FPS:I = 0xf

.field public static final FadeIn:B = 0x0t

.field public static final FadeOut:B = 0x1t


# instance fields
.field protected begin:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

.field protected current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

.field protected delay:I

.field protected duration:F

.field protected end:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

.field protected fps:I

.field protected shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

.field protected status:B


# direct methods
.method public constructor <init>(Lcom/intsig/office/pg/animate/ShapeAnimation;)V
    .locals 2

    const/16 v0, 0x4b0

    const/16 v1, 0xf

    .line 1
    invoke-direct {p0, p1, v0, v1}, Lcom/intsig/office/pg/animate/Animation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;II)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/pg/animate/ShapeAnimation;I)V
    .locals 1

    const/16 v0, 0xf

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/office/pg/animate/Animation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;II)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/pg/animate/ShapeAnimation;II)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/intsig/office/pg/animate/Animation;->shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

    int-to-float p1, p2

    .line 5
    iput p1, p0, Lcom/intsig/office/pg/animate/Animation;->duration:F

    .line 6
    iput p3, p0, Lcom/intsig/office/pg/animate/Animation;->fps:I

    const/16 p1, 0x3e8

    .line 7
    div-int/2addr p1, p3

    iput p1, p0, Lcom/intsig/office/pg/animate/Animation;->delay:I

    const/4 p1, 0x0

    .line 8
    iput-byte p1, p0, Lcom/intsig/office/pg/animate/Animation;->status:B

    return-void
.end method


# virtual methods
.method public animation(I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/pg/animate/Animation;->begin:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->begin:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 12
    .line 13
    :cond_0
    iget-object v1, p0, Lcom/intsig/office/pg/animate/Animation;->end:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 14
    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->dispose()V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->end:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 21
    .line 22
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 23
    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->dispose()V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 30
    .line 31
    :cond_2
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getAnimationStatus()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/pg/animate/Animation;->status:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCurrentAnimationInfor()Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDuration()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/animate/Animation;->duration:F

    .line 2
    .line 3
    float-to-int v0, v0

    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFPS()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/animate/Animation;->fps:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShapeAnimation()Lcom/intsig/office/pg/animate/ShapeAnimation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setDuration(I)V
    .locals 0

    .line 1
    int-to-float p1, p1

    .line 2
    iput p1, p0, Lcom/intsig/office/pg/animate/Animation;->duration:F

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public start()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-byte v0, p0, Lcom/intsig/office/pg/animate/Animation;->status:B

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public stop()V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    iput-byte v0, p0, Lcom/intsig/office/pg/animate/Animation;->status:B

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.class public Lcom/intsig/office/pg/animate/ShapeAnimation;
.super Ljava/lang/Object;
.source "ShapeAnimation.java"


# static fields
.field public static final Para_All:I = -0x2

.field public static final Para_BG:I = -0x1

.field public static final SA_EMPH:B = 0x1t

.field public static final SA_ENTR:B = 0x0t

.field public static final SA_EXIT:B = 0x2t

.field public static final SA_MEDIACALL:B = 0x5t

.field public static final SA_PATH:B = 0x3t

.field public static final SA_VERB:B = 0x4t

.field public static final Slide:I = -0x3


# instance fields
.field private animType:B

.field private paraBegin:I

.field private paraEnd:I

.field private shapeID:I


# direct methods
.method public constructor <init>(IB)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x2

    .line 2
    iput v0, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->paraBegin:I

    .line 3
    iput v0, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->paraEnd:I

    .line 4
    iput p1, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->shapeID:I

    .line 5
    iput-byte p2, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->animType:B

    return-void
.end method

.method public constructor <init>(IBII)V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput p1, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->shapeID:I

    .line 8
    iput-byte p2, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->animType:B

    .line 9
    iput p3, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->paraBegin:I

    .line 10
    iput p4, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->paraEnd:I

    return-void
.end method


# virtual methods
.method public getAnimationType()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->animType:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getParagraphBegin()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->paraBegin:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getParagraphEnd()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->paraEnd:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getShapeID()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/animate/ShapeAnimation;->shapeID:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

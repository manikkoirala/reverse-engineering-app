.class public Lcom/intsig/office/pg/animate/AnimationManager;
.super Ljava/lang/Object;
.source "AnimationManager.java"

# interfaces
.implements Lcom/intsig/office/system/ITimerListener;


# instance fields
.field private actionIndex:I

.field private animation:Lcom/intsig/office/pg/animate/IAnimation;

.field private control:Lcom/intsig/office/system/IControl;

.field private timer:Lcom/intsig/office/system/beans/ATimer;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public actionPerformed()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/office/pg/animate/IAnimation;->getAnimationStatus()B

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v3, 0x2

    .line 12
    if-eq v0, v3, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 15
    .line 16
    iget v3, p0, Lcom/intsig/office/pg/animate/AnimationManager;->actionIndex:I

    .line 17
    .line 18
    add-int/2addr v3, v2

    .line 19
    iput v3, p0, Lcom/intsig/office/pg/animate/AnimationManager;->actionIndex:I

    .line 20
    .line 21
    invoke-interface {v0, v3}, Lcom/intsig/office/pg/animate/IAnimation;->animation(I)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 25
    .line 26
    const v2, 0x50000001

    .line 27
    .line 28
    .line 29
    invoke-interface {v0, v2, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 33
    .line 34
    if-eqz v0, :cond_3

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/ATimer;->restart()V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 41
    .line 42
    if-eqz v0, :cond_1

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/ATimer;->stop()V

    .line 45
    .line 46
    .line 47
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 48
    .line 49
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    if-eqz v0, :cond_2

    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 56
    .line 57
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-interface {v0, v2}, Lcom/intsig/office/common/IOfficeToPicture;->setModeType(B)V

    .line 62
    .line 63
    .line 64
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 65
    .line 66
    const v2, 0x2000000a

    .line 67
    .line 68
    .line 69
    invoke-interface {v0, v2, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 70
    .line 71
    .line 72
    :cond_3
    :goto_0
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public beginAnimation(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/office/system/beans/ATimer;

    .line 6
    .line 7
    invoke-direct {v0, p1, p0}, Lcom/intsig/office/system/beans/ATimer;-><init>(ILcom/intsig/office/system/ITimerListener;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 11
    .line 12
    :cond_0
    iget-object p1, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 13
    .line 14
    if-eqz p1, :cond_1

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->actionIndex:I

    .line 18
    .line 19
    invoke-interface {p1}, Lcom/intsig/office/pg/animate/IAnimation;->start()V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/office/system/beans/ATimer;->start()V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 28
    .line 29
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    if-eqz p1, :cond_1

    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 36
    .line 37
    invoke-interface {p1}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-interface {p1, v0}, Lcom/intsig/office/common/IOfficeToPicture;->setModeType(B)V

    .line 42
    .line 43
    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/office/system/beans/ATimer;->dispose()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public hasStoped()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-interface {v0}, Lcom/intsig/office/pg/animate/IAnimation;->getAnimationStatus()B

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v2, 0x2

    .line 11
    if-ne v0, v2, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v1, 0x0

    .line 15
    :cond_1
    :goto_0
    return v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public killAnimationTimer()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/ATimer;->stop()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public restartAnimationTimer()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/ATimer;->restart()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setAnimation(Lcom/intsig/office/pg/animate/IAnimation;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/ATimer;->isRunning()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/ATimer;->stop()V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 21
    .line 22
    invoke-interface {v0}, Lcom/intsig/office/pg/animate/IAnimation;->stop()V

    .line 23
    .line 24
    .line 25
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public stopAnimation()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->timer:Lcom/intsig/office/system/beans/ATimer;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/system/beans/ATimer;->stop()V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->animation:Lcom/intsig/office/pg/animate/IAnimation;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/office/pg/animate/IAnimation;->stop()V

    .line 17
    .line 18
    .line 19
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 20
    .line 21
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 28
    .line 29
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const/4 v1, 0x1

    .line 34
    invoke-interface {v0, v1}, Lcom/intsig/office/common/IOfficeToPicture;->setModeType(B)V

    .line 35
    .line 36
    .line 37
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/pg/animate/AnimationManager;->control:Lcom/intsig/office/system/IControl;

    .line 38
    .line 39
    const v1, 0x50000001

    .line 40
    .line 41
    .line 42
    const/4 v2, 0x0

    .line 43
    invoke-interface {v0, v1, v2}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 44
    .line 45
    .line 46
    :cond_3
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

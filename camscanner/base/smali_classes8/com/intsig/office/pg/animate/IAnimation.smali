.class public interface abstract Lcom/intsig/office/pg/animate/IAnimation;
.super Ljava/lang/Object;
.source "IAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;
    }
.end annotation


# virtual methods
.method public abstract animation(I)V
.end method

.method public abstract dispose()V
.end method

.method public abstract getAnimationStatus()B
.end method

.method public abstract getCurrentAnimationInfor()Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;
.end method

.method public abstract getDuration()I
.end method

.method public abstract getFPS()I
.end method

.method public abstract getShapeAnimation()Lcom/intsig/office/pg/animate/ShapeAnimation;
.end method

.method public abstract setDuration(I)V
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method

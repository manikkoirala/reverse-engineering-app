.class public Lcom/intsig/office/pg/animate/FadeAnimation;
.super Lcom/intsig/office/pg/animate/Animation;
.source "FadeAnimation.java"


# direct methods
.method public constructor <init>(Lcom/intsig/office/pg/animate/ShapeAnimation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/office/pg/animate/Animation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;)V

    .line 2
    invoke-direct {p0}, Lcom/intsig/office/pg/animate/FadeAnimation;->initAnimationKeyPoint()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/pg/animate/ShapeAnimation;I)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/intsig/office/pg/animate/Animation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;I)V

    .line 4
    invoke-direct {p0}, Lcom/intsig/office/pg/animate/FadeAnimation;->initAnimationKeyPoint()V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/office/pg/animate/ShapeAnimation;II)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/office/pg/animate/Animation;-><init>(Lcom/intsig/office/pg/animate/ShapeAnimation;II)V

    .line 6
    invoke-direct {p0}, Lcom/intsig/office/pg/animate/FadeAnimation;->initAnimationKeyPoint()V

    return-void
.end method

.method private fadeIn(I)V
    .locals 2

    .line 1
    int-to-float p1, p1

    .line 2
    iget v0, p0, Lcom/intsig/office/pg/animate/Animation;->duration:F

    .line 3
    .line 4
    cmpg-float v1, p1, v0

    .line 5
    .line 6
    if-gez v1, :cond_0

    .line 7
    .line 8
    div-float/2addr p1, v0

    .line 9
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setProgress(F)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 15
    .line 16
    const/high16 v1, 0x437f0000    # 255.0f

    .line 17
    .line 18
    mul-float p1, p1, v1

    .line 19
    .line 20
    float-to-int p1, p1

    .line 21
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setAlpha(I)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 p1, 0x2

    .line 26
    iput-byte p1, p0, Lcom/intsig/office/pg/animate/Animation;->status:B

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 29
    .line 30
    const/high16 v0, 0x3f800000    # 1.0f

    .line 31
    .line 32
    invoke-virtual {p1, v0}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setProgress(F)V

    .line 33
    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 36
    .line 37
    const/16 v0, 0xff

    .line 38
    .line 39
    invoke-virtual {p1, v0}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setAlpha(I)V

    .line 40
    .line 41
    .line 42
    :goto_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private fadeOut(I)V
    .locals 3

    .line 1
    int-to-float p1, p1

    .line 2
    iget v0, p0, Lcom/intsig/office/pg/animate/Animation;->duration:F

    .line 3
    .line 4
    const/high16 v1, 0x3f800000    # 1.0f

    .line 5
    .line 6
    cmpg-float v2, p1, v0

    .line 7
    .line 8
    if-gez v2, :cond_0

    .line 9
    .line 10
    div-float/2addr p1, v0

    .line 11
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setProgress(F)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 17
    .line 18
    const/high16 v2, 0x437f0000    # 255.0f

    .line 19
    .line 20
    sub-float/2addr v1, p1

    .line 21
    mul-float v1, v1, v2

    .line 22
    .line 23
    float-to-int p1, v1

    .line 24
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setAlpha(I)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 p1, 0x2

    .line 29
    iput-byte p1, p0, Lcom/intsig/office/pg/animate/Animation;->status:B

    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 32
    .line 33
    invoke-virtual {p1, v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setProgress(F)V

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 37
    .line 38
    const/4 v0, 0x0

    .line 39
    invoke-virtual {p1, v0}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setAlpha(I)V

    .line 40
    .line 41
    .line 42
    :goto_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private initAnimationKeyPoint()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 2
    .line 3
    const/16 v1, 0xff

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x0

    .line 7
    if-eqz v0, :cond_3

    .line 8
    .line 9
    new-instance v4, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getAnimationType()B

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/16 v0, 0xff

    .line 20
    .line 21
    :goto_0
    invoke-direct {v4, v2, v0, v3}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;-><init>(Landroid/graphics/Rect;II)V

    .line 22
    .line 23
    .line 24
    iput-object v4, p0, Lcom/intsig/office/pg/animate/Animation;->begin:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 25
    .line 26
    new-instance v0, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 27
    .line 28
    iget-object v4, p0, Lcom/intsig/office/pg/animate/Animation;->shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 29
    .line 30
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getAnimationType()B

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    if-nez v4, :cond_1

    .line 35
    .line 36
    const/16 v4, 0xff

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_1
    const/4 v4, 0x0

    .line 40
    :goto_1
    invoke-direct {v0, v2, v4, v3}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;-><init>(Landroid/graphics/Rect;II)V

    .line 41
    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->end:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 44
    .line 45
    new-instance v0, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 46
    .line 47
    iget-object v4, p0, Lcom/intsig/office/pg/animate/Animation;->shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 48
    .line 49
    invoke-virtual {v4}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getAnimationType()B

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    if-nez v4, :cond_2

    .line 54
    .line 55
    const/4 v1, 0x0

    .line 56
    :cond_2
    invoke-direct {v0, v2, v1, v3}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;-><init>(Landroid/graphics/Rect;II)V

    .line 57
    .line 58
    .line 59
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 60
    .line 61
    goto :goto_2

    .line 62
    :cond_3
    new-instance v0, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 63
    .line 64
    invoke-direct {v0, v2, v3, v3}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;-><init>(Landroid/graphics/Rect;II)V

    .line 65
    .line 66
    .line 67
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->begin:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 68
    .line 69
    new-instance v0, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 70
    .line 71
    invoke-direct {v0, v2, v1, v3}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;-><init>(Landroid/graphics/Rect;II)V

    .line 72
    .line 73
    .line 74
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->end:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 75
    .line 76
    new-instance v0, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 77
    .line 78
    invoke-direct {v0, v2, v3, v3}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;-><init>(Landroid/graphics/Rect;II)V

    .line 79
    .line 80
    .line 81
    iput-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 82
    .line 83
    :goto_2
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method


# virtual methods
.method public animation(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 6
    .line 7
    if-eqz v1, :cond_3

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getAnimationType()B

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_2

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    if-eq v0, v1, :cond_1

    .line 17
    .line 18
    const/4 v1, 0x2

    .line 19
    if-eq v0, v1, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget v0, p0, Lcom/intsig/office/pg/animate/Animation;->delay:I

    .line 23
    .line 24
    mul-int p1, p1, v0

    .line 25
    .line 26
    invoke-direct {p0, p1}, Lcom/intsig/office/pg/animate/FadeAnimation;->fadeOut(I)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    iget v0, p0, Lcom/intsig/office/pg/animate/Animation;->delay:I

    .line 31
    .line 32
    mul-int p1, p1, v0

    .line 33
    .line 34
    invoke-direct {p0, p1}, Lcom/intsig/office/pg/animate/FadeAnimation;->fadeIn(I)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_2
    iget v0, p0, Lcom/intsig/office/pg/animate/Animation;->delay:I

    .line 39
    .line 40
    mul-int p1, p1, v0

    .line 41
    .line 42
    invoke-direct {p0, p1}, Lcom/intsig/office/pg/animate/FadeAnimation;->fadeIn(I)V

    .line 43
    .line 44
    .line 45
    :cond_3
    :goto_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public start()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/office/pg/animate/Animation;->start()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setProgress(F)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public stop()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/office/pg/animate/Animation;->stop()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 5
    .line 6
    if-eqz v0, :cond_2

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setAngle(I)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 13
    .line 14
    const/high16 v2, 0x3f800000    # 1.0f

    .line 15
    .line 16
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setProgress(F)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->shapeAnim:Lcom/intsig/office/pg/animate/ShapeAnimation;

    .line 20
    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/pg/animate/ShapeAnimation;->getAnimationType()B

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    const/4 v2, 0x2

    .line 30
    if-eq v0, v2, :cond_0

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setAlpha(I)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/pg/animate/Animation;->current:Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;

    .line 40
    .line 41
    const/16 v1, 0xff

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/animate/IAnimation$AnimationInformation;->setAlpha(I)V

    .line 44
    .line 45
    .line 46
    :cond_2
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

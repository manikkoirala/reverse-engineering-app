.class public interface abstract Lcom/intsig/office/pg/control/PageListViewDelegate;
.super Ljava/lang/Object;
.source "PageListViewDelegate.java"


# virtual methods
.method public abstract dispose()V
.end method

.method public abstract exportImage(Lcom/intsig/office/system/beans/pagelist/APageListItem;Landroid/graphics/Bitmap;)V
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getCurrentPageNumber()I
.end method

.method public abstract getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;
.end method

.method public abstract getFitSizeState()I
.end method

.method public abstract getFitZoom()F
.end method

.method public abstract getHeight()I
.end method

.method public abstract getModel()Ljava/lang/Object;
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public abstract getWidth()I
.end method

.method public abstract getZoom()F
.end method

.method public abstract gotoPage(I)V
.end method

.method public abstract isPointVisibleOnScreen(II)Z
.end method

.method public abstract measureNeedZoom()Z
.end method

.method public abstract nextPageView()V
.end method

.method public abstract notifyDataChange()V
.end method

.method public abstract postRepaint(Lcom/intsig/office/system/beans/pagelist/APageListItem;)V
.end method

.method public abstract previousPageview()V
.end method

.method public abstract setBackgroundColor(I)V
.end method

.method public abstract setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setBackgroundResource(I)V
.end method

.method public abstract setFitSize(I)V
.end method

.method public abstract setItemPointVisibleOnScreen(II)V
.end method

.method public abstract setVisibility(I)V
.end method

.method public abstract setZoom(FII)V
.end method

.method public abstract showPDFPageForIndex(I)V
.end method

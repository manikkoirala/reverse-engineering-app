.class public Lcom/intsig/office/pg/control/PGControl;
.super Lcom/intsig/office/system/AbstractControl;
.source "PGControl.java"


# instance fields
.field private isDispose:Z

.field private isShowingProgressDlg:Z

.field private mainControl:Lcom/intsig/office/system/IControl;

.field private pgView:Lcom/intsig/office/pg/control/Presentation;

.field private progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Lcom/intsig/office/system/IControl;Lcom/intsig/office/pg/model/PGModel;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/system/AbstractControl;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 5
    .line 6
    new-instance p1, Lcom/intsig/office/pg/control/Presentation;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 9
    .line 10
    .line 11
    move-result-object p3

    .line 12
    invoke-interface {p3}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 13
    .line 14
    .line 15
    move-result-object p3

    .line 16
    invoke-direct {p1, p3, p2, p0}, Lcom/intsig/office/pg/control/Presentation;-><init>(Landroid/app/Activity;Lcom/intsig/office/pg/model/PGModel;Lcom/intsig/office/system/IControl;)V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method static bridge synthetic O8(Lcom/intsig/office/pg/control/PGControl;)Landroid/app/ProgressDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/pg/control/PGControl;->progressDialog:Landroid/app/ProgressDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic Oo08(Lcom/intsig/office/pg/control/PGControl;Landroid/app/ProgressDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->progressDialog:Landroid/app/ProgressDialog;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private pagesCountChanged()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/pg/control/PGControl;->isShowingProgressDlg:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->showLoadingSlide()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/intsig/office/pg/control/PGControl;->isShowingProgressDlg:Z

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 17
    .line 18
    new-instance v1, Lcom/intsig/office/pg/control/PGControl$5;

    .line 19
    .line 20
    invoke-direct {v1, p0}, Lcom/intsig/office/pg/control/PGControl$5;-><init>(Lcom/intsig/office/pg/control/PGControl;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 24
    .line 25
    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method static bridge synthetic 〇080(Lcom/intsig/office/pg/control/PGControl;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/office/pg/control/PGControl;->isDispose:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/office/pg/control/PGControl;)Lcom/intsig/office/system/IControl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/office/pg/control/PGControl;)Lcom/intsig/office/pg/control/Presentation;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public actionEvent(ILjava/lang/Object;)V
    .locals 7

    .line 1
    const/4 v0, 0x2

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x1

    .line 4
    sparse-switch p1, :sswitch_data_0

    .line 5
    .line 6
    .line 7
    goto/16 :goto_1

    .line 8
    .line 9
    :sswitch_0
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 10
    .line 11
    check-cast p2, Ljava/lang/Integer;

    .line 12
    .line 13
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 14
    .line 15
    .line 16
    move-result p2

    .line 17
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->setAnimationDuration(I)V

    .line 18
    .line 19
    .line 20
    goto/16 :goto_1

    .line 21
    .line 22
    :sswitch_1
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 23
    .line 24
    const/4 p2, 0x3

    .line 25
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->slideShow(B)V

    .line 26
    .line 27
    .line 28
    goto/16 :goto_1

    .line 29
    .line 30
    :sswitch_2
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 31
    .line 32
    invoke-virtual {p1, v0}, Lcom/intsig/office/pg/control/Presentation;->slideShow(B)V

    .line 33
    .line 34
    .line 35
    goto/16 :goto_1

    .line 36
    .line 37
    :sswitch_3
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->endSlideShow()V

    .line 40
    .line 41
    .line 42
    goto/16 :goto_1

    .line 43
    .line 44
    :sswitch_4
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-interface {p1, v2}, Lcom/intsig/office/system/IMainFrame;->fullScreen(Z)V

    .line 49
    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 52
    .line 53
    if-nez p2, :cond_0

    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 56
    .line 57
    .line 58
    move-result p2

    .line 59
    add-int/2addr p2, v2

    .line 60
    goto :goto_0

    .line 61
    :cond_0
    check-cast p2, Ljava/lang/Integer;

    .line 62
    .line 63
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 64
    .line 65
    .line 66
    move-result p2

    .line 67
    :goto_0
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->beginSlideShow(I)V

    .line 68
    .line 69
    .line 70
    goto/16 :goto_1

    .line 71
    .line 72
    :sswitch_5
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->isSlideShow()Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    if-nez p1, :cond_4

    .line 79
    .line 80
    check-cast p2, Ljava/lang/Integer;

    .line 81
    .line 82
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 83
    .line 84
    .line 85
    move-result p1

    .line 86
    invoke-virtual {p0, p1}, Lcom/intsig/office/pg/control/PGControl;->showSlide(I)V

    .line 87
    .line 88
    .line 89
    goto/16 :goto_1

    .line 90
    .line 91
    :sswitch_6
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getCurrentSlide()Lcom/intsig/office/pg/model/PGSlide;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGSlide;->getNotes()Lcom/intsig/office/pg/model/PGNotes;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/intsig/office/pg/model/PGNotes;->getNotes()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    new-instance v4, Ljava/util/Vector;

    .line 106
    .line 107
    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 108
    .line 109
    .line 110
    invoke-virtual {v4, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 111
    .line 112
    .line 113
    new-instance p1, Lcom/intsig/office/pg/dialog/NotesDialog;

    .line 114
    .line 115
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 116
    .line 117
    .line 118
    move-result-object p2

    .line 119
    invoke-interface {p2}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    const/4 v3, 0x0

    .line 124
    const/16 v5, 0x8

    .line 125
    .line 126
    move-object v0, p1

    .line 127
    move-object v1, p0

    .line 128
    invoke-direct/range {v0 .. v5}, Lcom/intsig/office/pg/dialog/NotesDialog;-><init>(Lcom/intsig/office/system/IControl;Landroid/content/Context;Lcom/intsig/office/system/IDialogAction;Ljava/util/Vector;I)V

    .line 129
    .line 130
    .line 131
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 132
    .line 133
    .line 134
    goto/16 :goto_1

    .line 135
    .line 136
    :sswitch_7
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 137
    .line 138
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->initCalloutView()V

    .line 139
    .line 140
    .line 141
    goto/16 :goto_1

    .line 142
    .line 143
    :sswitch_8
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 144
    .line 145
    check-cast p2, Ljava/lang/Integer;

    .line 146
    .line 147
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 148
    .line 149
    .line 150
    move-result p2

    .line 151
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->setFitSize(I)V

    .line 152
    .line 153
    .line 154
    goto/16 :goto_1

    .line 155
    .line 156
    :sswitch_9
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGControl;->pagesCountChanged()V

    .line 157
    .line 158
    .line 159
    goto/16 :goto_1

    .line 160
    .line 161
    :sswitch_a
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 162
    .line 163
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->isSlideShow()Z

    .line 164
    .line 165
    .line 166
    move-result p1

    .line 167
    if-eqz p1, :cond_1

    .line 168
    .line 169
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 170
    .line 171
    const/4 p2, 0x5

    .line 172
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->slideShow(B)V

    .line 173
    .line 174
    .line 175
    goto/16 :goto_1

    .line 176
    .line 177
    :cond_1
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 178
    .line 179
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 180
    .line 181
    .line 182
    move-result p1

    .line 183
    iget-object p2, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 184
    .line 185
    invoke-virtual {p2}, Lcom/intsig/office/pg/control/Presentation;->getRealSlideCount()I

    .line 186
    .line 187
    .line 188
    move-result p2

    .line 189
    sub-int/2addr p2, v2

    .line 190
    if-ge p1, p2, :cond_4

    .line 191
    .line 192
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 193
    .line 194
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 195
    .line 196
    .line 197
    move-result p2

    .line 198
    add-int/2addr p2, v2

    .line 199
    invoke-virtual {p1, p2, v1}, Lcom/intsig/office/pg/control/Presentation;->showSlide(IZ)V

    .line 200
    .line 201
    .line 202
    goto/16 :goto_1

    .line 203
    .line 204
    :sswitch_b
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 205
    .line 206
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->isSlideShow()Z

    .line 207
    .line 208
    .line 209
    move-result p1

    .line 210
    if-eqz p1, :cond_2

    .line 211
    .line 212
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 213
    .line 214
    const/4 p2, 0x4

    .line 215
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->slideShow(B)V

    .line 216
    .line 217
    .line 218
    goto/16 :goto_1

    .line 219
    .line 220
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 221
    .line 222
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 223
    .line 224
    .line 225
    move-result p1

    .line 226
    if-lez p1, :cond_4

    .line 227
    .line 228
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 229
    .line 230
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 231
    .line 232
    .line 233
    move-result p2

    .line 234
    sub-int/2addr p2, v2

    .line 235
    invoke-virtual {p1, p2, v1}, Lcom/intsig/office/pg/control/Presentation;->showSlide(IZ)V

    .line 236
    .line 237
    .line 238
    goto/16 :goto_1

    .line 239
    .line 240
    :sswitch_c
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 241
    .line 242
    new-instance p2, Lcom/intsig/office/pg/control/PGControl$4;

    .line 243
    .line 244
    invoke-direct {p2, p0}, Lcom/intsig/office/pg/control/PGControl$4;-><init>(Lcom/intsig/office/pg/control/PGControl;)V

    .line 245
    .line 246
    .line 247
    invoke-virtual {p1, p2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 248
    .line 249
    .line 250
    goto/16 :goto_1

    .line 251
    .line 252
    :sswitch_d
    check-cast p2, Lcom/intsig/office/common/hyperlink/Hyperlink;

    .line 253
    .line 254
    invoke-virtual {p2}, Lcom/intsig/office/common/hyperlink/Hyperlink;->getAddress()Ljava/lang/String;

    .line 255
    .line 256
    .line 257
    move-result-object p1

    .line 258
    if-eqz p1, :cond_4

    .line 259
    .line 260
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    const/4 v1, 0x0

    .line 265
    const/4 v2, 0x0

    .line 266
    const/4 v3, 0x0

    .line 267
    const/high16 v4, -0x40800000    # -1.0f

    .line 268
    .line 269
    const/high16 v5, -0x40800000    # -1.0f

    .line 270
    .line 271
    const/16 v6, 0xe

    .line 272
    .line 273
    invoke-interface/range {v0 .. v6}, Lcom/intsig/office/system/IMainFrame;->onEventMethod(Landroid/view/View;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FFB)Z

    .line 274
    .line 275
    .line 276
    new-instance p2, Landroid/content/Intent;

    .line 277
    .line 278
    const-string v0, "android.intent.action.VIEW"

    .line 279
    .line 280
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 281
    .line 282
    .line 283
    move-result-object p1

    .line 284
    invoke-direct {p2, v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 285
    .line 286
    .line 287
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 288
    .line 289
    .line 290
    move-result-object p1

    .line 291
    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 292
    .line 293
    .line 294
    move-result-object p1

    .line 295
    invoke-virtual {p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    .line 297
    .line 298
    goto/16 :goto_1

    .line 299
    .line 300
    :sswitch_e
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 301
    .line 302
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->isSlideShow()Z

    .line 303
    .line 304
    .line 305
    move-result p1

    .line 306
    if-nez p1, :cond_4

    .line 307
    .line 308
    check-cast p2, [I

    .line 309
    .line 310
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 311
    .line 312
    aget v1, p2, v1

    .line 313
    .line 314
    int-to-float v1, v1

    .line 315
    const v3, 0x461c4000    # 10000.0f

    .line 316
    .line 317
    .line 318
    div-float/2addr v1, v3

    .line 319
    aget v2, p2, v2

    .line 320
    .line 321
    aget p2, p2, v0

    .line 322
    .line 323
    invoke-virtual {p1, v1, v2, p2}, Lcom/intsig/office/pg/control/Presentation;->setZoom(FII)V

    .line 324
    .line 325
    .line 326
    goto :goto_1

    .line 327
    :sswitch_f
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 328
    .line 329
    .line 330
    move-result-object p1

    .line 331
    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 332
    .line 333
    .line 334
    move-result-object p1

    .line 335
    const-string p2, "clipboard"

    .line 336
    .line 337
    invoke-virtual {p1, p2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 338
    .line 339
    .line 340
    move-result-object p1

    .line 341
    check-cast p1, Landroid/text/ClipboardManager;

    .line 342
    .line 343
    iget-object p2, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 344
    .line 345
    invoke-virtual {p2}, Lcom/intsig/office/pg/control/Presentation;->getSelectedText()Ljava/lang/String;

    .line 346
    .line 347
    .line 348
    move-result-object p2

    .line 349
    invoke-virtual {p1, p2}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 350
    .line 351
    .line 352
    goto :goto_1

    .line 353
    :sswitch_10
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 354
    .line 355
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 356
    .line 357
    .line 358
    move-result-object p1

    .line 359
    if-eqz p1, :cond_3

    .line 360
    .line 361
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 362
    .line 363
    new-instance v0, Lcom/intsig/office/pg/control/PGControl$2;

    .line 364
    .line 365
    invoke-direct {v0, p0, p2}, Lcom/intsig/office/pg/control/PGControl$2;-><init>(Lcom/intsig/office/pg/control/PGControl;Ljava/lang/Object;)V

    .line 366
    .line 367
    .line 368
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 369
    .line 370
    .line 371
    goto :goto_1

    .line 372
    :cond_3
    new-instance p1, Lcom/intsig/office/pg/control/PGControl$3;

    .line 373
    .line 374
    invoke-direct {p1, p0, p2}, Lcom/intsig/office/pg/control/PGControl$3;-><init>(Lcom/intsig/office/pg/control/PGControl;Ljava/lang/Object;)V

    .line 375
    .line 376
    .line 377
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    .line 378
    .line 379
    .line 380
    goto :goto_1

    .line 381
    :sswitch_11
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 382
    .line 383
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 384
    .line 385
    .line 386
    move-result-object p1

    .line 387
    if-eqz p1, :cond_4

    .line 388
    .line 389
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 390
    .line 391
    new-instance v0, Lcom/intsig/office/pg/control/PGControl$1;

    .line 392
    .line 393
    invoke-direct {v0, p0, p2}, Lcom/intsig/office/pg/control/PGControl$1;-><init>(Lcom/intsig/office/pg/control/PGControl;Ljava/lang/Object;)V

    .line 394
    .line 395
    .line 396
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 397
    .line 398
    .line 399
    goto :goto_1

    .line 400
    :sswitch_12
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->isAutoTest()Z

    .line 401
    .line 402
    .line 403
    move-result p1

    .line 404
    if-eqz p1, :cond_4

    .line 405
    .line 406
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 407
    .line 408
    .line 409
    move-result-object p1

    .line 410
    invoke-interface {p1}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 411
    .line 412
    .line 413
    move-result-object p1

    .line 414
    invoke-virtual {p1}, Landroid/app/Activity;->onBackPressed()V

    .line 415
    .line 416
    .line 417
    goto :goto_1

    .line 418
    :sswitch_13
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 419
    .line 420
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->init()V

    .line 421
    .line 422
    .line 423
    goto :goto_1

    .line 424
    :sswitch_14
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 425
    .line 426
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 427
    .line 428
    .line 429
    :catch_0
    :cond_4
    :goto_1
    return-void

    .line 430
    nop

    .line 431
    :sswitch_data_0
    .sparse-switch
        -0x10000000 -> :sswitch_14
        0x13 -> :sswitch_13
        0x16 -> :sswitch_12
        0x1a -> :sswitch_11
        0x1b -> :sswitch_10
        0x10000002 -> :sswitch_f
        0x20000005 -> :sswitch_e
        0x20000008 -> :sswitch_d
        0x2000000a -> :sswitch_c
        0x2000000d -> :sswitch_b
        0x2000000e -> :sswitch_a
        0x2000000f -> :sswitch_9
        0x20000015 -> :sswitch_8
        0x2000001e -> :sswitch_7
        0x50000000 -> :sswitch_6
        0x50000001 -> :sswitch_14
        0x50000002 -> :sswitch_5
        0x51000001 -> :sswitch_4
        0x51000002 -> :sswitch_3
        0x51000003 -> :sswitch_2
        0x51000004 -> :sswitch_1
        0x51000007 -> :sswitch_0
    .end sparse-switch
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public canBackLayout()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/office/pg/control/PGControl;->isDispose:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->dispose()V

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getActionValue(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .line 1
    const/4 v0, 0x2

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x1

    .line 4
    const/4 v3, 0x0

    .line 5
    sparse-switch p1, :sswitch_data_0

    .line 6
    .line 7
    .line 8
    goto/16 :goto_4

    .line 9
    .line 10
    :sswitch_0
    instance-of p1, p2, [I

    .line 11
    .line 12
    if-eqz p1, :cond_7

    .line 13
    .line 14
    check-cast p2, [I

    .line 15
    .line 16
    array-length p1, p2

    .line 17
    if-lt p1, v0, :cond_1

    .line 18
    .line 19
    aget p1, p2, v2

    .line 20
    .line 21
    if-gtz p1, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 25
    .line 26
    aget p2, p2, v3

    .line 27
    .line 28
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/pg/control/Presentation;->getSlideshowToImage(II)Landroid/graphics/Bitmap;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    return-object p1

    .line 33
    :cond_1
    :goto_0
    return-object v1

    .line 34
    :sswitch_1
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 35
    .line 36
    check-cast p2, Ljava/lang/Integer;

    .line 37
    .line 38
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->getSlideAnimationSteps(I)I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    return-object p1

    .line 51
    :sswitch_2
    check-cast p2, Ljava/lang/Integer;

    .line 52
    .line 53
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    iget-object p2, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 58
    .line 59
    invoke-virtual {p2}, Lcom/intsig/office/pg/control/Presentation;->getRealSlideCount()I

    .line 60
    .line 61
    .line 62
    move-result p2

    .line 63
    if-gt p1, p2, :cond_2

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_2
    const/4 v2, 0x0

    .line 67
    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    return-object p1

    .line 72
    :sswitch_3
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->hasNextAction_Slideshow()Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    return-object p1

    .line 83
    :sswitch_4
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 84
    .line 85
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->hasPreviousAction_Slideshow()Z

    .line 86
    .line 87
    .line 88
    move-result p1

    .line 89
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    return-object p1

    .line 94
    :sswitch_5
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 95
    .line 96
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->isSlideShow()Z

    .line 97
    .line 98
    .line 99
    move-result p1

    .line 100
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    return-object p1

    .line 105
    :sswitch_6
    check-cast p2, Ljava/lang/Integer;

    .line 106
    .line 107
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 108
    .line 109
    .line 110
    move-result p1

    .line 111
    if-lez p1, :cond_4

    .line 112
    .line 113
    iget-object p2, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 114
    .line 115
    invoke-virtual {p2}, Lcom/intsig/office/pg/control/Presentation;->getSlideCount()I

    .line 116
    .line 117
    .line 118
    move-result p2

    .line 119
    if-le p1, p2, :cond_3

    .line 120
    .line 121
    goto :goto_2

    .line 122
    :cond_3
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 123
    .line 124
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getPageSize()Lcom/intsig/office/java/awt/Dimension;

    .line 125
    .line 126
    .line 127
    move-result-object p1

    .line 128
    new-instance p2, Lcom/intsig/office/java/awt/Rectangle;

    .line 129
    .line 130
    iget v0, p1, Lcom/intsig/office/java/awt/Dimension;->width:I

    .line 131
    .line 132
    iget p1, p1, Lcom/intsig/office/java/awt/Dimension;->height:I

    .line 133
    .line 134
    invoke-direct {p2, v3, v3, v0, p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>(IIII)V

    .line 135
    .line 136
    .line 137
    return-object p2

    .line 138
    :cond_4
    :goto_2
    return-object v1

    .line 139
    :sswitch_7
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 140
    .line 141
    check-cast p2, Ljava/lang/Integer;

    .line 142
    .line 143
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 144
    .line 145
    .line 146
    move-result p2

    .line 147
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->getSlideNote(I)Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object p1

    .line 151
    return-object p1

    .line 152
    :sswitch_8
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 153
    .line 154
    check-cast p2, Ljava/lang/Integer;

    .line 155
    .line 156
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 157
    .line 158
    .line 159
    move-result p2

    .line 160
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->slideToImage(I)Landroid/graphics/Bitmap;

    .line 161
    .line 162
    .line 163
    move-result-object p1

    .line 164
    return-object p1

    .line 165
    :sswitch_9
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 166
    .line 167
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getFitZoom()F

    .line 168
    .line 169
    .line 170
    move-result p1

    .line 171
    const/high16 p2, 0x40400000    # 3.0f

    .line 172
    .line 173
    mul-float p1, p1, p2

    .line 174
    .line 175
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    return-object p1

    .line 180
    :sswitch_a
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 181
    .line 182
    if-eqz p1, :cond_7

    .line 183
    .line 184
    check-cast p2, Landroid/graphics/Bitmap;

    .line 185
    .line 186
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/Presentation;->getSnapshot(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 187
    .line 188
    .line 189
    move-result-object p1

    .line 190
    return-object p1

    .line 191
    :sswitch_b
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 192
    .line 193
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getRealSlideCount()I

    .line 194
    .line 195
    .line 196
    move-result p1

    .line 197
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 198
    .line 199
    .line 200
    move-result-object p1

    .line 201
    return-object p1

    .line 202
    :sswitch_c
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 203
    .line 204
    if-eqz p1, :cond_7

    .line 205
    .line 206
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getFitSizeState()I

    .line 207
    .line 208
    .line 209
    move-result p1

    .line 210
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 211
    .line 212
    .line 213
    move-result-object p1

    .line 214
    return-object p1

    .line 215
    :sswitch_d
    instance-of p1, p2, [I

    .line 216
    .line 217
    if-eqz p1, :cond_7

    .line 218
    .line 219
    check-cast p2, [I

    .line 220
    .line 221
    if-eqz p2, :cond_7

    .line 222
    .line 223
    array-length p1, p2

    .line 224
    const/4 v4, 0x7

    .line 225
    if-ne p1, v4, :cond_7

    .line 226
    .line 227
    iget-object v5, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 228
    .line 229
    aget v6, p2, v3

    .line 230
    .line 231
    aget v7, p2, v2

    .line 232
    .line 233
    aget v8, p2, v0

    .line 234
    .line 235
    const/4 p1, 0x3

    .line 236
    aget v9, p2, p1

    .line 237
    .line 238
    const/4 p1, 0x4

    .line 239
    aget v10, p2, p1

    .line 240
    .line 241
    const/4 p1, 0x5

    .line 242
    aget v11, p2, p1

    .line 243
    .line 244
    const/4 p1, 0x6

    .line 245
    aget v12, p2, p1

    .line 246
    .line 247
    invoke-virtual/range {v5 .. v12}, Lcom/intsig/office/pg/control/Presentation;->slideAreaToImage(IIIIIII)Landroid/graphics/Bitmap;

    .line 248
    .line 249
    .line 250
    move-result-object p1

    .line 251
    return-object p1

    .line 252
    :sswitch_e
    instance-of p1, p2, [I

    .line 253
    .line 254
    if-eqz p1, :cond_7

    .line 255
    .line 256
    check-cast p2, [I

    .line 257
    .line 258
    array-length p1, p2

    .line 259
    if-lt p1, v0, :cond_6

    .line 260
    .line 261
    aget p1, p2, v2

    .line 262
    .line 263
    if-gtz p1, :cond_5

    .line 264
    .line 265
    goto :goto_3

    .line 266
    :cond_5
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 267
    .line 268
    aget p2, p2, v3

    .line 269
    .line 270
    int-to-float p1, p1

    .line 271
    const v1, 0x461c4000    # 10000.0f

    .line 272
    .line 273
    .line 274
    div-float/2addr p1, v1

    .line 275
    invoke-virtual {v0, p2, p1}, Lcom/intsig/office/pg/control/Presentation;->getThumbnail(IF)Landroid/graphics/Bitmap;

    .line 276
    .line 277
    .line 278
    move-result-object p1

    .line 279
    return-object p1

    .line 280
    :cond_6
    :goto_3
    return-object v1

    .line 281
    :sswitch_f
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 282
    .line 283
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->hasNextSlide_Slideshow()Z

    .line 284
    .line 285
    .line 286
    move-result p1

    .line 287
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 288
    .line 289
    .line 290
    move-result-object p1

    .line 291
    return-object p1

    .line 292
    :sswitch_10
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 293
    .line 294
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->hasPreviousSlide_Slideshow()Z

    .line 295
    .line 296
    .line 297
    move-result p1

    .line 298
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 299
    .line 300
    .line 301
    move-result-object p1

    .line 302
    return-object p1

    .line 303
    :sswitch_11
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 304
    .line 305
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 306
    .line 307
    .line 308
    move-result p1

    .line 309
    add-int/2addr p1, v2

    .line 310
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 311
    .line 312
    .line 313
    move-result-object p1

    .line 314
    return-object p1

    .line 315
    :sswitch_12
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 316
    .line 317
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getSlideCount()I

    .line 318
    .line 319
    .line 320
    move-result p1

    .line 321
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 322
    .line 323
    .line 324
    move-result-object p1

    .line 325
    return-object p1

    .line 326
    :sswitch_13
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 327
    .line 328
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getFitZoom()F

    .line 329
    .line 330
    .line 331
    move-result p1

    .line 332
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 333
    .line 334
    .line 335
    move-result-object p1

    .line 336
    return-object p1

    .line 337
    :sswitch_14
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 338
    .line 339
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getZoom()F

    .line 340
    .line 341
    .line 342
    move-result p1

    .line 343
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 344
    .line 345
    .line 346
    move-result-object p1

    .line 347
    return-object p1

    .line 348
    :cond_7
    :goto_4
    return-object v1

    .line 349
    :sswitch_data_0
    .sparse-switch
        0x20000005 -> :sswitch_14
        0x20000006 -> :sswitch_13
        0x2000000b -> :sswitch_12
        0x2000000c -> :sswitch_11
        0x2000000d -> :sswitch_10
        0x2000000e -> :sswitch_f
        0x20000010 -> :sswitch_e
        0x20000013 -> :sswitch_d
        0x20000016 -> :sswitch_c
        0x20000017 -> :sswitch_b
        0x20000018 -> :sswitch_a
        0x2000001f -> :sswitch_9
        0x50000003 -> :sswitch_8
        0x50000004 -> :sswitch_7
        0x50000005 -> :sswitch_6
        0x51000000 -> :sswitch_5
        0x51000005 -> :sswitch_4
        0x51000006 -> :sswitch_3
        0x51000008 -> :sswitch_2
        0x51000009 -> :sswitch_1
        0x5100000a -> :sswitch_0
    .end sparse-switch
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Lcom/intsig/office/system/IMainFrame;->getCurActivity()Landroid/app/Activity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getApplicationType()B
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCurrentViewIndex()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-int/lit8 v0, v0, 0x1

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getCustomDialog()Lcom/intsig/office/common/ICustomDialog;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getCustomDialog()Lcom/intsig/office/common/ICustomDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return-object p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getFind()Lcom/intsig/office/system/IFind;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getFind()Lcom/intsig/office/pg/control/PGFind;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getMainFrame()Lcom/intsig/office/system/IMainFrame;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getOfficeToPicture()Lcom/intsig/office/common/IOfficeToPicture;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSlideImages()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/office/pg/control/Presentation;->getSlideCount()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v2, 0x1

    .line 13
    :goto_0
    if-gt v2, v1, :cond_0

    .line 14
    .line 15
    iget-object v3, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 16
    .line 17
    invoke-virtual {v3, v2}, Lcom/intsig/office/pg/control/Presentation;->slideToImage(I)Landroid/graphics/Bitmap;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .line 23
    .line 24
    add-int/lit8 v2, v2, 0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception v1

    .line 28
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 29
    .line 30
    .line 31
    :cond_0
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public getSlideShow()Lcom/intsig/office/common/ISlideShow;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSlideShow()Lcom/intsig/office/common/ISlideShow;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getSysKit()Lcom/intsig/office/system/SysKit;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->getSysKit()Lcom/intsig/office/system/SysKit;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isAutoTest()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/office/system/IControl;->isAutoTest()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSlideShow()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->isSlideShow()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public layoutView(IIII)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method public setLayoutThreadDied(Z)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setStopDraw(Z)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public showSlide(I)V
    .locals 5

    .line 1
    if-ltz p1, :cond_3

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getSlideCount()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lt p1, v0, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    iput-boolean v0, p0, Lcom/intsig/office/pg/control/PGControl;->isShowingProgressDlg:Z

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/office/pg/control/Presentation;->getRealSlideCount()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-lt p1, v1, :cond_2

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    iput-boolean v1, p0, Lcom/intsig/office/pg/control/PGControl;->isShowingProgressDlg:Z

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGControl;->getMainFrame()Lcom/intsig/office/system/IMainFrame;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-interface {v1}, Lcom/intsig/office/system/IMainFrame;->isShowProgressBar()Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-eqz v1, :cond_1

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 37
    .line 38
    new-instance v2, Lcom/intsig/office/pg/control/PGControl$6;

    .line 39
    .line 40
    invoke-direct {v2, p0}, Lcom/intsig/office/pg/control/PGControl$6;-><init>(Lcom/intsig/office/pg/control/PGControl;)V

    .line 41
    .line 42
    .line 43
    const-wide/16 v3, 0xc8

    .line 44
    .line 45
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGControl;->mainControl:Lcom/intsig/office/system/IControl;

    .line 50
    .line 51
    invoke-interface {v1}, Lcom/intsig/office/system/IControl;->getCustomDialog()Lcom/intsig/office/common/ICustomDialog;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    if-eqz v1, :cond_2

    .line 56
    .line 57
    const/4 v2, 0x2

    .line 58
    invoke-interface {v1, v2}, Lcom/intsig/office/common/ICustomDialog;->showDialog(B)V

    .line 59
    .line 60
    .line 61
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGControl;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 62
    .line 63
    invoke-virtual {v1, p1, v0}, Lcom/intsig/office/pg/control/Presentation;->showSlide(IZ)V

    .line 64
    .line 65
    .line 66
    :cond_3
    :goto_1
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.class public Lcom/intsig/office/pg/control/PGFind;
.super Ljava/lang/Object;
.source "PGFind.java"

# interfaces
.implements Lcom/intsig/office/system/IFind;


# instance fields
.field private isSetPointToVisible:Z

.field private final mHighLightList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/office/simpletext/model/HighLightWord;",
            ">;"
        }
    .end annotation
.end field

.field private presentation:Lcom/intsig/office/pg/control/Presentation;

.field private query:Ljava/lang/String;

.field protected rect:Lcom/intsig/office/java/awt/Rectangle;

.field private shapeIndex:I

.field private slideIndex:I

.field private startOffset:I


# direct methods
.method public constructor <init>(Lcom/intsig/office/pg/control/Presentation;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->mHighLightList:Ljava/util/ArrayList;

    .line 10
    .line 11
    const/4 v0, -0x1

    .line 12
    iput v0, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 13
    .line 14
    iput v0, p0, Lcom/intsig/office/pg/control/PGFind;->slideIndex:I

    .line 15
    .line 16
    iput v0, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 19
    .line 20
    new-instance p1, Lcom/intsig/office/java/awt/Rectangle;

    .line 21
    .line 22
    invoke-direct {p1}, Lcom/intsig/office/java/awt/Rectangle;-><init>()V

    .line 23
    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method private addHighlightList(Lcom/intsig/office/simpletext/model/SectionElement;ILjava/lang/String;I)V
    .locals 8

    .line 1
    if-lez p2, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    int-to-long p1, p2

    .line 8
    add-long v3, v0, p1

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->mHighLightList:Ljava/util/ArrayList;

    .line 11
    .line 12
    new-instance p2, Lcom/intsig/office/simpletext/model/HighLightWord;

    .line 13
    .line 14
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    .line 15
    .line 16
    .line 17
    move-result p3

    .line 18
    int-to-long v0, p3

    .line 19
    add-long v5, v3, v0

    .line 20
    .line 21
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object v7

    .line 25
    move-object v2, p2

    .line 26
    invoke-direct/range {v2 .. v7}, Lcom/intsig/office/simpletext/model/HighLightWord;-><init>(JJLjava/lang/Integer;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
.end method

.method private addHighlights(Lcom/intsig/office/simpletext/model/SectionElement;Ljava/lang/String;I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getRenderersDoc()Lcom/intsig/office/simpletext/model/IDocument;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p1, v0}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-gez v1, :cond_0

    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    invoke-direct {p0, p1, v1, p2, p3}, Lcom/intsig/office/pg/control/PGFind;->addHighlightList(Lcom/intsig/office/simpletext/model/SectionElement;ILjava/lang/String;I)V

    .line 19
    .line 20
    .line 21
    :goto_0
    const/4 v2, -0x1

    .line 22
    if-eq v1, v2, :cond_1

    .line 23
    .line 24
    add-int/lit8 v1, v1, 0x1

    .line 25
    .line 26
    invoke-virtual {v0, p2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    invoke-direct {p0, p1, v1, p2, p3}, Lcom/intsig/office/pg/control/PGFind;->addHighlightList(Lcom/intsig/office/simpletext/model/SectionElement;ILjava/lang/String;I)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method private checkAddAllHighLight()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->mHighLightList:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/office/pg/control/PGFind;->slideIndex:I

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/control/Presentation;->getSlide(I)Lcom/intsig/office/pg/model/PGSlide;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const/4 v1, 0x0

    .line 15
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCountForFind()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    if-ge v1, v2, :cond_2

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/office/pg/model/PGSlide;->getShapeForFind(I)Lcom/intsig/office/common/shape/IShape;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    if-eqz v2, :cond_1

    .line 26
    .line 27
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    const/4 v4, 0x1

    .line 32
    if-ne v3, v4, :cond_1

    .line 33
    .line 34
    move-object v3, v2

    .line 35
    check-cast v3, Lcom/intsig/office/common/shape/TextBox;

    .line 36
    .line 37
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    if-eqz v3, :cond_1

    .line 42
    .line 43
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 44
    .line 45
    .line 46
    move-result-wide v4

    .line 47
    invoke-virtual {v3}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 48
    .line 49
    .line 50
    move-result-wide v6

    .line 51
    sub-long/2addr v4, v6

    .line 52
    const-wide/16 v6, 0x0

    .line 53
    .line 54
    cmp-long v8, v4, v6

    .line 55
    .line 56
    if-nez v8, :cond_0

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_0
    iget-object v4, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 60
    .line 61
    invoke-interface {v2}, Lcom/intsig/office/common/shape/IShape;->getShapeID()I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    invoke-direct {p0, v3, v4, v2}, Lcom/intsig/office/pg/control/PGFind;->addHighlights(Lcom/intsig/office/simpletext/model/SectionElement;Ljava/lang/String;I)V

    .line 66
    .line 67
    .line 68
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->mHighLightList:Ljava/util/ArrayList;

    .line 72
    .line 73
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    if-nez v0, :cond_3

    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 80
    .line 81
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/PGEditor;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGFind;->mHighLightList:Ljava/util/ArrayList;

    .line 90
    .line 91
    invoke-interface {v0, v1}, Lcom/intsig/office/simpletext/control/IHighlight;->addHighlights(Ljava/util/ArrayList;)V

    .line 92
    .line 93
    .line 94
    :cond_3
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method private findSlideBackward(I)Z
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/control/Presentation;->getSlide(I)Lcom/intsig/office/pg/model/PGSlide;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    iget v2, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    if-ltz v2, :cond_1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCountForFind()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    sub-int/2addr v2, v3

    .line 22
    :goto_0
    if-ltz v2, :cond_7

    .line 23
    .line 24
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/PGSlide;->getShapeForFind(I)Lcom/intsig/office/common/shape/IShape;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    if-eqz v4, :cond_6

    .line 29
    .line 30
    invoke-interface {v4}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    if-ne v5, v3, :cond_6

    .line 35
    .line 36
    iget v5, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 37
    .line 38
    if-ne v5, v2, :cond_2

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGFind;->getCurrentIndex()I

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    if-ne v5, p1, :cond_2

    .line 45
    .line 46
    iget v5, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_2
    const/4 v5, -0x1

    .line 50
    :goto_1
    check-cast v4, Lcom/intsig/office/common/shape/TextBox;

    .line 51
    .line 52
    invoke-virtual {v4}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 53
    .line 54
    .line 55
    move-result-object v6

    .line 56
    if-eqz v6, :cond_6

    .line 57
    .line 58
    if-ltz v5, :cond_3

    .line 59
    .line 60
    iget-object v7, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 61
    .line 62
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    .line 63
    .line 64
    .line 65
    move-result v7

    .line 66
    if-lt v5, v7, :cond_6

    .line 67
    .line 68
    :cond_3
    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 69
    .line 70
    .line 71
    move-result-wide v7

    .line 72
    invoke-virtual {v6}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 73
    .line 74
    .line 75
    move-result-wide v9

    .line 76
    sub-long/2addr v7, v9

    .line 77
    const-wide/16 v9, 0x0

    .line 78
    .line 79
    cmp-long v11, v7, v9

    .line 80
    .line 81
    if-nez v11, :cond_4

    .line 82
    .line 83
    goto :goto_3

    .line 84
    :cond_4
    if-ltz v5, :cond_5

    .line 85
    .line 86
    iget-object v5, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 87
    .line 88
    invoke-virtual {v5}, Lcom/intsig/office/pg/control/Presentation;->getRenderersDoc()Lcom/intsig/office/simpletext/model/IDocument;

    .line 89
    .line 90
    .line 91
    move-result-object v5

    .line 92
    invoke-virtual {v6, v5}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v5

    .line 96
    iget-object v6, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 97
    .line 98
    iget v7, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 99
    .line 100
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    .line 101
    .line 102
    .line 103
    move-result v8

    .line 104
    sub-int/2addr v7, v8

    .line 105
    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    .line 106
    .line 107
    .line 108
    move-result v7

    .line 109
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    .line 110
    .line 111
    .line 112
    move-result v5

    .line 113
    goto :goto_2

    .line 114
    :cond_5
    iget-object v5, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 115
    .line 116
    invoke-virtual {v5}, Lcom/intsig/office/pg/control/Presentation;->getRenderersDoc()Lcom/intsig/office/simpletext/model/IDocument;

    .line 117
    .line 118
    .line 119
    move-result-object v5

    .line 120
    invoke-virtual {v6, v5}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v5

    .line 124
    iget-object v6, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 125
    .line 126
    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 127
    .line 128
    .line 129
    move-result v5

    .line 130
    :goto_2
    if-ltz v5, :cond_6

    .line 131
    .line 132
    iput v5, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 133
    .line 134
    iput v2, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 135
    .line 136
    invoke-virtual {p0, p1, v4}, Lcom/intsig/office/pg/control/PGFind;->addHighlight(ILcom/intsig/office/common/shape/TextBox;)V

    .line 137
    .line 138
    .line 139
    return v3

    .line 140
    :cond_6
    :goto_3
    add-int/lit8 v2, v2, -0x1

    .line 141
    .line 142
    goto :goto_0

    .line 143
    :cond_7
    return v1
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private findSlideForward(I)Z
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/office/pg/control/Presentation;->getSlide(I)Lcom/intsig/office/pg/model/PGSlide;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    iget v2, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCountForFind()I

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    if-lt v2, v3, :cond_1

    .line 18
    .line 19
    return v1

    .line 20
    :cond_1
    iget v2, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 21
    .line 22
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCountForFind()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    if-ge v2, v3, :cond_6

    .line 31
    .line 32
    invoke-virtual {v0, v2}, Lcom/intsig/office/pg/model/PGSlide;->getShapeForFind(I)Lcom/intsig/office/common/shape/IShape;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    if-eqz v3, :cond_5

    .line 37
    .line 38
    invoke-interface {v3}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    const/4 v5, 0x1

    .line 43
    if-ne v4, v5, :cond_5

    .line 44
    .line 45
    check-cast v3, Lcom/intsig/office/common/shape/TextBox;

    .line 46
    .line 47
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    if-eqz v4, :cond_5

    .line 52
    .line 53
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 54
    .line 55
    .line 56
    move-result-wide v6

    .line 57
    invoke-virtual {v4}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 58
    .line 59
    .line 60
    move-result-wide v8

    .line 61
    sub-long/2addr v6, v8

    .line 62
    const-wide/16 v8, 0x0

    .line 63
    .line 64
    cmp-long v10, v6, v8

    .line 65
    .line 66
    if-nez v10, :cond_2

    .line 67
    .line 68
    goto :goto_3

    .line 69
    :cond_2
    iget v6, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 70
    .line 71
    if-ne v6, v2, :cond_3

    .line 72
    .line 73
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGFind;->getCurrentIndex()I

    .line 74
    .line 75
    .line 76
    move-result v6

    .line 77
    if-ne v6, p1, :cond_3

    .line 78
    .line 79
    iget v6, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_3
    const/4 v6, -0x1

    .line 83
    :goto_1
    if-ltz v6, :cond_4

    .line 84
    .line 85
    iget-object v6, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 86
    .line 87
    invoke-virtual {v6}, Lcom/intsig/office/pg/control/Presentation;->getRenderersDoc()Lcom/intsig/office/simpletext/model/IDocument;

    .line 88
    .line 89
    .line 90
    move-result-object v6

    .line 91
    invoke-virtual {v4, v6}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    iget-object v6, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 96
    .line 97
    iget v7, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 98
    .line 99
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    .line 100
    .line 101
    .line 102
    move-result v8

    .line 103
    add-int/2addr v7, v8

    .line 104
    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    goto :goto_2

    .line 109
    :cond_4
    iget-object v6, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 110
    .line 111
    invoke-virtual {v6}, Lcom/intsig/office/pg/control/Presentation;->getRenderersDoc()Lcom/intsig/office/simpletext/model/IDocument;

    .line 112
    .line 113
    .line 114
    move-result-object v6

    .line 115
    invoke-virtual {v4, v6}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v4

    .line 119
    iget-object v6, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 120
    .line 121
    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 122
    .line 123
    .line 124
    move-result v4

    .line 125
    :goto_2
    if-ltz v4, :cond_5

    .line 126
    .line 127
    iput v4, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 128
    .line 129
    iput v2, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 130
    .line 131
    invoke-virtual {p0, p1, v3}, Lcom/intsig/office/pg/control/PGFind;->addHighlight(ILcom/intsig/office/common/shape/TextBox;)V

    .line 132
    .line 133
    .line 134
    return v5

    .line 135
    :cond_5
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 136
    .line 137
    goto :goto_0

    .line 138
    :cond_6
    return v1
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method private getCurrentIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/control/PGFind;->slideIndex:I

    .line 2
    .line 3
    if-ltz v0, :cond_0

    .line 4
    .line 5
    return v0

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public addHighlight(ILcom/intsig/office/common/shape/TextBox;)V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGFind;->getCurrentIndex()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x1

    .line 8
    if-eq p1, v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 11
    .line 12
    invoke-virtual {v0, p1, v3}, Lcom/intsig/office/pg/control/Presentation;->showSlide(IZ)V

    .line 13
    .line 14
    .line 15
    iput-boolean v3, p0, Lcom/intsig/office/pg/control/PGFind;->isSetPointToVisible:Z

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/intsig/office/java/awt/Rectangle;->setBounds(IIII)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iget v4, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 30
    .line 31
    int-to-long v4, v4

    .line 32
    iget-object v6, p0, Lcom/intsig/office/pg/control/PGFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 33
    .line 34
    invoke-virtual {v0, v4, v5, v6, v2}, Lcom/intsig/office/pg/control/PGEditor;->modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getPrintMode()Lcom/intsig/office/pg/control/PGPrintMode;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/PGPrintMode;->getListView()Lcom/intsig/office/pg/control/PageListViewDelegate;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iget-object v2, p0, Lcom/intsig/office/pg/control/PGFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 48
    .line 49
    iget v4, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 50
    .line 51
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 52
    .line 53
    invoke-interface {v0, v4, v2}, Lcom/intsig/office/pg/control/PageListViewDelegate;->isPointVisibleOnScreen(II)Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-nez v0, :cond_1

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getPrintMode()Lcom/intsig/office/pg/control/PGPrintMode;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/PGPrintMode;->getListView()Lcom/intsig/office/pg/control/PageListViewDelegate;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    iget-object v2, p0, Lcom/intsig/office/pg/control/PGFind;->rect:Lcom/intsig/office/java/awt/Rectangle;

    .line 70
    .line 71
    iget v4, v2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 72
    .line 73
    iget v2, v2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 74
    .line 75
    invoke-interface {v0, v4, v2}, Lcom/intsig/office/pg/control/PageListViewDelegate;->setItemPointVisibleOnScreen(II)V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 80
    .line 81
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getPrintMode()Lcom/intsig/office/pg/control/PGPrintMode;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    iget-object v2, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 86
    .line 87
    invoke-virtual {v2}, Lcom/intsig/office/pg/control/Presentation;->getPrintMode()Lcom/intsig/office/pg/control/PGPrintMode;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    invoke-virtual {v2}, Lcom/intsig/office/pg/control/PGPrintMode;->getListView()Lcom/intsig/office/pg/control/PageListViewDelegate;

    .line 92
    .line 93
    .line 94
    move-result-object v2

    .line 95
    invoke-interface {v2}, Lcom/intsig/office/pg/control/PageListViewDelegate;->getCurrentPageView()Lcom/intsig/office/system/beans/pagelist/APageListItem;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    invoke-virtual {v0, v2, v1}, Lcom/intsig/office/pg/control/PGPrintMode;->exportImage(Lcom/intsig/office/system/beans/pagelist/APageListItem;Landroid/graphics/Bitmap;)V

    .line 100
    .line 101
    .line 102
    :goto_0
    const/4 v2, 0x1

    .line 103
    :goto_1
    iput p1, p0, Lcom/intsig/office/pg/control/PGFind;->slideIndex:I

    .line 104
    .line 105
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGFind;->checkAddAllHighLight()V

    .line 106
    .line 107
    .line 108
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    invoke-virtual {p1, p2}, Lcom/intsig/office/pg/control/PGEditor;->setEditorTextBox(Lcom/intsig/office/common/shape/TextBox;)V

    .line 115
    .line 116
    .line 117
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 118
    .line 119
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/PGEditor;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    iget p2, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 128
    .line 129
    int-to-long v3, p2

    .line 130
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 131
    .line 132
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    add-int/2addr p2, v0

    .line 137
    int-to-long v5, p2

    .line 138
    invoke-interface {p1, v3, v4, v5, v6}, Lcom/intsig/office/simpletext/control/IHighlight;->addHighlight(JJ)V

    .line 139
    .line 140
    .line 141
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 142
    .line 143
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getControl()Lcom/intsig/office/system/IControl;

    .line 144
    .line 145
    .line 146
    move-result-object p1

    .line 147
    const/16 p2, 0x14

    .line 148
    .line 149
    invoke-interface {p1, p2, v1}, Lcom/intsig/office/system/IControl;->actionEvent(ILjava/lang/Object;)V

    .line 150
    .line 151
    .line 152
    if-eqz v2, :cond_2

    .line 153
    .line 154
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 155
    .line 156
    invoke-virtual {p1}, Landroid/view/View;->postInvalidate()V

    .line 157
    .line 158
    .line 159
    :cond_2
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 160
    .line 161
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->notifyDataChange()V

    .line 162
    .line 163
    .line 164
    return-void
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
.end method

.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public find(Ljava/lang/String;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 6
    .line 7
    const/4 p1, -0x1

    .line 8
    iput p1, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 9
    .line 10
    iput p1, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/office/pg/control/PGFind;->removeSearch()V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/office/pg/control/Presentation;->getCurrentIndex()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/office/pg/control/PGFind;->findSlideForward(I)Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_2

    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    return p1

    .line 29
    :cond_2
    add-int/lit8 p1, p1, 0x1

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 32
    .line 33
    invoke-virtual {v1}, Lcom/intsig/office/pg/control/Presentation;->getRealSlideCount()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-ne p1, v1, :cond_3

    .line 38
    .line 39
    const/4 p1, 0x0

    .line 40
    :cond_3
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGFind;->getCurrentIndex()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-ne p1, v1, :cond_1

    .line 45
    .line 46
    return v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public findBackward()Z
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/pg/control/PGFind;->findBackward(Z)Z

    move-result v0

    return v0
.end method

.method public findBackward(Z)Z
    .locals 5

    .line 2
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 3
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGFind;->getCurrentIndex()I

    move-result v0

    .line 4
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGFind;->getCurrentIndex()I

    move-result v2

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    .line 5
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getRealSlideCount()I

    move-result v0

    sub-int/2addr v0, v3

    :cond_1
    if-ne v0, v2, :cond_2

    .line 6
    iget v4, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    if-lez v4, :cond_2

    add-int/lit8 v4, v4, -0x1

    .line 7
    iput v4, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 8
    :cond_2
    invoke-direct {p0, v0}, Lcom/intsig/office/pg/control/PGFind;->findSlideBackward(I)Z

    move-result v4

    if-eqz v4, :cond_3

    return v3

    :cond_3
    const/4 v4, -0x1

    .line 9
    iput v4, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 10
    iput v4, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_1

    if-nez p1, :cond_4

    .line 11
    invoke-virtual {p0, v3}, Lcom/intsig/office/pg/control/PGFind;->findBackward(Z)Z

    move-result p1

    return p1

    :cond_4
    return v1
.end method

.method public findForward()Z
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0}, Lcom/intsig/office/pg/control/PGFind;->findForward(Z)Z

    move-result v0

    return v0
.end method

.method public findForward(Z)Z
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 3
    :cond_0
    invoke-direct {p0}, Lcom/intsig/office/pg/control/PGFind;->getCurrentIndex()I

    move-result v0

    if-eqz p1, :cond_1

    .line 4
    iput v1, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    const/4 v0, 0x0

    .line 5
    :cond_1
    iget v2, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    if-ltz v2, :cond_2

    add-int/lit8 v2, v2, 0x1

    .line 6
    iput v2, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 7
    :cond_2
    invoke-direct {p0, v0}, Lcom/intsig/office/pg/control/PGFind;->findSlideForward(I)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    return v3

    :cond_3
    const/4 v2, -0x1

    .line 8
    iput v2, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 9
    iput v2, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    add-int/lit8 v0, v0, 0x1

    .line 10
    iget-object v2, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    invoke-virtual {v2}, Lcom/intsig/office/pg/control/Presentation;->getRealSlideCount()I

    move-result v2

    if-ne v0, v2, :cond_1

    if-nez p1, :cond_4

    if-lez v0, :cond_4

    .line 11
    invoke-virtual {p0, v3}, Lcom/intsig/office/pg/control/PGFind;->findForward(Z)Z

    move-result p1

    return p1

    :cond_4
    return v1
.end method

.method public getPageIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/office/pg/control/PGFind;->slideIndex:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public isSetPointToVisible()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/office/pg/control/PGFind;->isSetPointToVisible:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public onConfigurationChanged()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getCurrentSlide()Lcom/intsig/office/pg/model/PGSlide;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget v1, p0, Lcom/intsig/office/pg/control/PGFind;->shapeIndex:I

    .line 8
    .line 9
    if-ltz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/office/pg/model/PGSlide;->getShapeCountForFind()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-ge v1, v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/PGEditor;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    iget v1, p0, Lcom/intsig/office/pg/control/PGFind;->startOffset:I

    .line 28
    .line 29
    int-to-long v2, v1

    .line 30
    iget-object v4, p0, Lcom/intsig/office/pg/control/PGFind;->query:Ljava/lang/String;

    .line 31
    .line 32
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    .line 33
    .line 34
    .line 35
    move-result v4

    .line 36
    add-int/2addr v1, v4

    .line 37
    int-to-long v4, v1

    .line 38
    invoke-interface {v0, v2, v3, v4, v5}, Lcom/intsig/office/simpletext/control/IHighlight;->addHighlight(JJ)V

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 42
    .line 43
    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 44
    .line 45
    .line 46
    :cond_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
.end method

.method public removeSearch()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getEditor()Lcom/intsig/office/pg/control/PGEditor;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/PGEditor;->getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0}, Lcom/intsig/office/simpletext/control/IHighlight;->removeHighlight()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->mHighLightList:Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGFind;->presentation:Lcom/intsig/office/pg/control/Presentation;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->notifyDataChange()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public resetSearchResult()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setSetPointToVisible(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/office/pg/control/PGFind;->isSetPointToVisible:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

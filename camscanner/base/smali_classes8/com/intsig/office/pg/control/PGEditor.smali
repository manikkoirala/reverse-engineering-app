.class public Lcom/intsig/office/pg/control/PGEditor;
.super Ljava/lang/Object;
.source "PGEditor.java"

# interfaces
.implements Lcom/intsig/office/simpletext/control/IWord;


# instance fields
.field private editorTextBox:Lcom/intsig/office/common/shape/TextBox;

.field private highlight:Lcom/intsig/office/simpletext/control/IHighlight;

.field private paraAnimation:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/pg/animate/IAnimation;",
            ">;"
        }
    .end annotation
.end field

.field private pgView:Lcom/intsig/office/pg/control/Presentation;


# direct methods
.method public constructor <init>(Lcom/intsig/office/pg/control/Presentation;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGEditor;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 5
    .line 6
    new-instance p1, Lcom/intsig/office/simpletext/control/Highlight;

    .line 7
    .line 8
    invoke-direct {p1, p0}, Lcom/intsig/office/simpletext/control/Highlight;-><init>(Lcom/intsig/office/simpletext/control/IWord;)V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGEditor;->highlight:Lcom/intsig/office/simpletext/control/IHighlight;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method


# virtual methods
.method public clearAnimation()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->paraAnimation:Ljava/util/Map;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public dispose()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->editorTextBox:Lcom/intsig/office/common/shape/TextBox;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGEditor;->highlight:Lcom/intsig/office/simpletext/control/IHighlight;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-interface {v1}, Lcom/intsig/office/simpletext/control/IHighlight;->dispose()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->highlight:Lcom/intsig/office/simpletext/control/IHighlight;

    .line 12
    .line 13
    :cond_0
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/office/pg/control/PGEditor;->paraAnimation:Ljava/util/Map;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->paraAnimation:Ljava/util/Map;

    .line 23
    .line 24
    :cond_1
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getControl()Lcom/intsig/office/system/IControl;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getControl()Lcom/intsig/office/system/IControl;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getDocument()Lcom/intsig/office/simpletext/model/IDocument;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEditType()B
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getEditorTextBox()Lcom/intsig/office/common/shape/TextBox;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->editorTextBox:Lcom/intsig/office/common/shape/TextBox;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getHighlight()Lcom/intsig/office/simpletext/control/IHighlight;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->highlight:Lcom/intsig/office/simpletext/control/IHighlight;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getPGView()Lcom/intsig/office/pg/control/Presentation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getParagraphAnimation(I)Lcom/intsig/office/pg/animate/IAnimation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->paraAnimation:Ljava/util/Map;

    .line 6
    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Lcom/intsig/office/pg/animate/IAnimation;

    .line 18
    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGEditor;->paraAnimation:Ljava/util/Map;

    .line 22
    .line 23
    const/4 v0, -0x2

    .line 24
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    check-cast p1, Lcom/intsig/office/pg/animate/IAnimation;

    .line 33
    .line 34
    :cond_0
    if-nez p1, :cond_1

    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/office/pg/control/PGEditor;->paraAnimation:Ljava/util/Map;

    .line 37
    .line 38
    const/4 v0, -0x1

    .line 39
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    check-cast p1, Lcom/intsig/office/pg/animate/IAnimation;

    .line 48
    .line 49
    :cond_1
    return-object p1

    .line 50
    :cond_2
    const/4 p1, 0x0

    .line 51
    return-object p1
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
.end method

.method public getText(JJ)Ljava/lang/String;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->editorTextBox:Lcom/intsig/office/common/shape/TextBox;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/TextBox;->getElement()Lcom/intsig/office/simpletext/model/SectionElement;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 11
    .line 12
    .line 13
    move-result-wide v2

    .line 14
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 15
    .line 16
    .line 17
    move-result-wide v4

    .line 18
    sub-long/2addr v2, v4

    .line 19
    const-wide/16 v4, 0x0

    .line 20
    .line 21
    cmp-long v6, v2, v4

    .line 22
    .line 23
    if-lez v6, :cond_0

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/office/simpletext/model/SectionElement;->getText(Lcom/intsig/office/simpletext/model/IDocument;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    if-eqz v2, :cond_0

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getStartOffset()J

    .line 32
    .line 33
    .line 34
    move-result-wide v3

    .line 35
    invoke-static {p1, p2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    .line 36
    .line 37
    .line 38
    move-result-wide p1

    .line 39
    long-to-int p2, p1

    .line 40
    invoke-virtual {v0}, Lcom/intsig/office/simpletext/model/AbstractElement;->getEndOffset()J

    .line 41
    .line 42
    .line 43
    move-result-wide v0

    .line 44
    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    .line 45
    .line 46
    .line 47
    move-result-wide p3

    .line 48
    long-to-int p1, p3

    .line 49
    invoke-virtual {v2, p2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    return-object p1

    .line 54
    :cond_0
    return-object v1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getTextBox()Lcom/intsig/office/common/shape/IShape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->editorTextBox:Lcom/intsig/office/common/shape/TextBox;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->editorTextBox:Lcom/intsig/office/common/shape/TextBox;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/office/common/shape/TextBox;->getRootView()Lcom/intsig/office/simpletext/view/STRoot;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/office/simpletext/view/STRoot;->modelToView(JLcom/intsig/office/java/awt/Rectangle;Z)Lcom/intsig/office/java/awt/Rectangle;

    .line 12
    .line 13
    .line 14
    :cond_0
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 15
    .line 16
    iget-object p2, p0, Lcom/intsig/office/pg/control/PGEditor;->editorTextBox:Lcom/intsig/office/common/shape/TextBox;

    .line 17
    .line 18
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    iget p2, p2, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 23
    .line 24
    add-int/2addr p1, p2

    .line 25
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 26
    .line 27
    iget p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 28
    .line 29
    iget-object p2, p0, Lcom/intsig/office/pg/control/PGEditor;->editorTextBox:Lcom/intsig/office/common/shape/TextBox;

    .line 30
    .line 31
    invoke-virtual {p2}, Lcom/intsig/office/common/shape/AbstractShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    iget p2, p2, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 36
    .line 37
    add-int/2addr p1, p2

    .line 38
    iput p1, p3, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 39
    .line 40
    :cond_1
    return-object p3
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.method public setEditorTextBox(Lcom/intsig/office/common/shape/TextBox;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGEditor;->editorTextBox:Lcom/intsig/office/common/shape/TextBox;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setShapeAnimation(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/office/pg/animate/IAnimation;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/control/PGEditor;->paraAnimation:Ljava/util/Map;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public viewToModel(IIZ)J
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/control/PGEditor;->pgView:Lcom/intsig/office/pg/control/Presentation;

    .line 2
    .line 3
    const-wide/16 v1, -0x1

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-wide v1

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/office/pg/control/Presentation;->getCurrentSlide()Lcom/intsig/office/pg/model/PGSlide;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0, p1, p2}, Lcom/intsig/office/pg/model/PGSlide;->getShape(II)Lcom/intsig/office/common/shape/IShape;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-interface {v0}, Lcom/intsig/office/common/shape/IShape;->getType()S

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    const/4 v4, 0x1

    .line 23
    if-ne v3, v4, :cond_1

    .line 24
    .line 25
    move-object v3, v0

    .line 26
    check-cast v3, Lcom/intsig/office/common/shape/TextBox;

    .line 27
    .line 28
    invoke-virtual {v3}, Lcom/intsig/office/common/shape/TextBox;->getRootView()Lcom/intsig/office/simpletext/view/STRoot;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    if-eqz v3, :cond_1

    .line 33
    .line 34
    invoke-interface {v0}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    iget v1, v1, Lcom/intsig/office/java/awt/Rectangle;->x:I

    .line 39
    .line 40
    sub-int/2addr p1, v1

    .line 41
    invoke-interface {v0}, Lcom/intsig/office/common/shape/IShape;->getBounds()Lcom/intsig/office/java/awt/Rectangle;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    iget v0, v0, Lcom/intsig/office/java/awt/Rectangle;->y:I

    .line 46
    .line 47
    sub-int/2addr p2, v0

    .line 48
    invoke-virtual {v3, p1, p2, p3}, Lcom/intsig/office/simpletext/view/STRoot;->viewToModel(IIZ)J

    .line 49
    .line 50
    .line 51
    move-result-wide p1

    .line 52
    return-wide p1

    .line 53
    :cond_1
    return-wide v1
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
.end method

.class public Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;
.super Ljava/lang/Object;
.source "TableCellStyle.java"


# instance fields
.field private bgFill:Lcom/intsig/office/fc/dom4j/Element;

.field private cellBorders:Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;

.field private fontAttr:Lcom/intsig/office/simpletext/model/IAttributeSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->cellBorders:Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->bgFill:Lcom/intsig/office/fc/dom4j/Element;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->fontAttr:Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getFontAttributeSet()Lcom/intsig/office/simpletext/model/IAttributeSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->fontAttr:Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTableCellBgFill()Lcom/intsig/office/fc/dom4j/Element;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->bgFill:Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public getTableCellBorders()Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->cellBorders:Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
.end method

.method public setFontAttributeSet(Lcom/intsig/office/simpletext/model/IAttributeSet;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->fontAttr:Lcom/intsig/office/simpletext/model/IAttributeSet;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTableCellBgFill(Lcom/intsig/office/fc/dom4j/Element;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->bgFill:Lcom/intsig/office/fc/dom4j/Element;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method

.method public setTableCellBorders(Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/office/pg/model/tableStyle/TableCellStyle;->cellBorders:Lcom/intsig/office/pg/model/tableStyle/TableCellBorders;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
.end method
